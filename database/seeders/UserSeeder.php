<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('users')->truncate();
        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('users')->insert([
            ['name' => 'Super Admin', 'email' => 'sadmin@admin.com', 'role_id' => 1, 'password' =>  bcrypt('password')],
            ['name' => 'Super Admin', 'email' => 'admin@admin.com', 'role_id' => 2, 'password' =>  bcrypt('password')],
            ['name' => 'Service', 'email' => 'service@admin.com', 'role_id' => 4, 'password' =>  bcrypt('password')]
        ]);
    }
}
