<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('fields')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('fields')->insert([
            ['name' => 'text', 'type' => 'input'],
            ['name' => 'number', 'type' => 'input'],
            ['name' => 'tel', 'type' => 'input'],
            ['name' => 'email', 'type' => 'input'],
            ['name' => 'password', 'type' => 'input'],
            ['name' => 'checkbox', 'type' => 'input'],
            ['name' => 'radio', 'type' => 'input'],
            ['name' => 'file', 'type' => 'input'],
            ['name' => 'select', 'type' => 'select'],
            ['name' => 'textarea', 'type' => 'textarea']
        ]);
    }
}
