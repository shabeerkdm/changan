<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class attGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('attribute_groups')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('attribute_groups')->insert([
            ['group_name' => 'Driving Performance & Suspension'],
            ['group_name' => 'Vehicle Dimension'],
            ['group_name' => 'Tires & Wheels'],
            ['group_name' => 'Exterior Features'],
            ['group_name' => 'Exterior Lighting'],
            ['group_name' => 'Interior Features'],
            ['group_name' => 'Conveinience'],
            ['group_name' => 'Infotainment'],
            ['group_name' => 'Safety']
        ]);
    }
}
