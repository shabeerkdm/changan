<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class FieldPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('field_page')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('field_page')->insert([
            ['page_id' => 1, 'field_id' => 1, 'name' => 'title', 'label' => 'Page Title', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 1, 'field_id' => 9, 'name' => 'slider_id', 'label' => 'Slider', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode(['method' => 'get', 'url' => 'sliders'])],
            ['page_id' => 1, 'field_id' => 8, 'name' => 'image_1', 'label' => 'Video Background (1214px X 1079px)', 'class' => 'col-md-4', 'validation' => '{}', 'options' => json_encode([])],
            ['page_id' => 1, 'field_id' => 1, 'name' => 'video_1', 'label' => 'Video URL', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 1, 'field_id' => 8, 'name' => 'image_2', 'label' => 'Compare Changan Models (580px X 353px)', 'class' => 'col-md-4', 'validation' => '{}', 'options' => json_encode([])],
            ['page_id' => 1, 'field_id' => 8, 'name' => 'image_3', 'label' => 'Discover Your Changan (580px X 353px)', 'class' => 'col-md-4', 'validation' => '{}', 'options' => json_encode([])],
            ['page_id' => 1, 'field_id' => 8, 'name' => 'image_4', 'label' => 'Innovation with care (580px X 517px)', 'class' => 'col-md-4', 'validation' => '{}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 1, 'name' => 'title', 'label' => 'Page Title', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 9, 'name' => 'slider_id', 'label' => 'Slider', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode(['method' => 'get', 'url' => 'sliders'])],
            ['page_id' => 2, 'field_id' => 1, 'name' => 'intro_title', 'label' => 'Introduction Title', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 10, 'name' => 'block_1', 'label' => 'Introduction Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 8, 'name' => 'image_1', 'label' => 'Background (2000px X 862px)', 'class' => 'col-md-4', 'validation' => '{}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 1, 'name' => 'title_2', 'label' => 'Title', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 10, 'name' => 'block_2', 'label' => 'Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 1, 'name' => 'success_title', 'label' => 'Our Success', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 2, 'field_id' => 10, 'name' => 'block_3', 'label' => 'Our Success Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 1, 'name' => 'title', 'label' => 'Page Title', 'class' => 'col-md-6', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 1, 'name' => 'title_1', 'label' => 'Intro', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 10, 'name' => 'block_1', 'label' => 'Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 1, 'name' => 'title_2', 'label' => 'Period', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 10, 'name' => 'block_2', 'label' => 'Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 1, 'name' => 'title_3', 'label' => 'Terms', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
            ['page_id' => 3, 'field_id' => 10, 'name' => 'block_3', 'label' => 'Body', 'class' => 'col-md-12', 'validation' => '{required:true}', 'options' => json_encode([])],
        ]);
    }
}
