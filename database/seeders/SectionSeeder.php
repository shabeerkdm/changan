<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('sections')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('sections')->insert([
            ['name' => 'Page'],
            ['name' => 'Model'],
            ['name' => 'Post'],
            ['name' => 'Offer']
        ]);
    }
}
