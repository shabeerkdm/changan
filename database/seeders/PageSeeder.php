<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('pages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('pages')->insert([
            ['title' => 'Home', 'slug' => 'home', 'type' => 'pages'],
            ['title' => 'About', 'slug' => 'about', 'type' => 'pages'],
            ['title' => 'Warranty', 'slug' => 'warranty', 'type' => 'pages']
        ]);
    }
}
