<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FieldSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(FieldPageSeeder::class);
        $this->call(attGroupSeeder::class);
        $this->call(SectionSeeder::class);
    }
}
