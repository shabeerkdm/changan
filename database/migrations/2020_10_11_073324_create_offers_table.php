<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 256);
            $table->string('title', 256);
            $table->string('type', 256);
            $table->unsignedBigInteger('model_id')->nullable();
            $table->unsignedBigInteger('grade_id')->nullable();
            $table->string('image', 256);
            $table->string('image_1', 256)->nullable();
            $table->text('description')->nullable();
            $table->text('body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
