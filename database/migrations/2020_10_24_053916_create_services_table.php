<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('fname', 150);
            $table->string('lname', 150);
            $table->string('email', 150);
            $table->string('phone', 150);
            $table->string('types', 255);
            $table->string('plate_number', 255);
            $table->date('date')->nullable();
            $table->text('comments')->nullable();
            $table->text('status')->nullable();
            $table->unsignedBigInteger('model_id')->default(0);
            $table->foreign('model_id')->references('id')->on('models')->onDelete('cascade');
            $table->unsignedBigInteger('contact_id')->nullable();
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
