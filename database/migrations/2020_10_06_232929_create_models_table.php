<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 256);
            $table->unsignedBigInteger('slider_id');
            $table->foreign('slider_id')->references('id')->on('sliders');
            $table->string('name', 256);
            $table->string('title', 256);
            $table->string('slogan', 256);
            $table->string('image', 256)->nullable();
            $table->string('video_bg', 256)->nullable();
            $table->string('video_1', 256)->nullable();
            $table->string('file', 256)->nullable();
            $table->string('engine', 256)->nullable();
            $table->string('transmission', 256)->nullable();
            $table->string('torque', 256)->nullable();
            $table->string('gallery_bg', 256)->nullable();
            $table->string('brochure', 256)->nullable();
            $table->text('view', 256)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models');
    }
}
