@extends('layouts.base')
@include('layouts.seo')
@section('content')
    <section class="background-cover slick">
        @foreach ($model->slider->slides as $slide)
            <a href="{{ url('buy-online/' . $model->slug) }}">
                <img class="img-fluid desktop" src="{{ storage($slide->image) }}" alt="" style="width:100% !important">
                <img class="img-fluid mobile" src="{{ storage($slide->m_image) }}" alt="">
            </a>
        @endforeach
    </section>
    <section class="py-5 mobile__pt_0">
        <div class="container">
            <div class="primary__title text-center">
                <h2 class="mb-2">{{ $model->title }}</h2>
                <p>{{ $model->slogan }}</p>
            </div>
            {!! $model->view !!}
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row mb-0">
                <div class="col-sm-4 my-2">
                    <div class="qlty-block bg__grey">
                        <div class="qlty-block__icon">
                            <svg class="icon">
                                <use xlink:href="#engine" />
                            </svg>
                        </div>
                        <div class="qlty-block__value">
                            {{ $model->engine }}
                        </div>
                        <h5>Engine</h5>
                    </div>
                </div>
                <div class="col-sm-4 my-2">
                    <div class="qlty-block bg__grey">
                        <div class="qlty-block__icon">
                            <svg class="icon">
                                <use xlink:href="#manual-transmission" />
                            </svg>
                        </div>
                        <div class="qlty-block__value">
                            {{ $model->transmission }}
                        </div>
                        <h5>Transmission</h5>
                    </div>
                </div>
                <div class="col-sm-4 my-2">
                    <div class="qlty-block bg__grey">
                        <div class="qlty-block__icon">
                            <svg class="icon">
                                <use xlink:href="#repair" />
                            </svg>
                        </div>
                        <div class="qlty-block__value">
                            {{ $model->torque }}
                        </div>
                        <h5>Maximum Torque (N-m)</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-5 ">
        <div class="container">
            <div class="specification">
                <div class="specification__top">
                    <div class="primary__title text-left mb-0">
                        <h2>Specifications</h2>
                    </div>
                    <div class="tabs_links">
                        <ul class="tabs">
                            <li class="tab-link current" data-tab="tab-1">Power and Performance</li>
                            <li class="tab-link" data-tab="tab-2">Exterior</li>
                            <li class="tab-link" data-tab="tab-3">Interior</li>
                            <li class="tab-link" data-tab="tab-4">Safety</li>
                        </ul>
                    </div>
                </div>
                <div class="specification__bottom">
                    <div id="tab-1" class="tab-content current">
                        <div class="row">
                            @foreach ($specs['power'] as $spec)
                                <div class="col-sm-4">
                                    <div class="news">
                                        <div class="news__image">
                                            <img class="img-fluid" src="{{ storage($spec->image) }}" alt="">
                                        </div>
                                        <div class="news__title">
                                            <h5 class="text-uppercase font-size__small">{{ $spec->title }}</h5>
                                        </div>
                                        <div class="news__details">
                                            {!! $spec->body !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="tab-2" class="tab-content">

                        <div class="row">
                            @foreach ($specs['exterior'] as $spec)
                                <div class="col-sm-4">
                                    <div class="news">
                                        <div class="news__image">
                                            <img class="img-fluid" src="{{ storage($spec->image) }}" alt="">
                                        </div>
                                        <div class="news__title">
                                            <h5 class="text-uppercase font-size__small">{{ $spec->title }}</h5>
                                        </div>
                                        <div class="news__details">
                                            {!! $spec->body !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="tab-3" class="tab-content">
                        <div class="row">
                            @foreach ($specs['interior'] as $spec)
                                <div class="col-sm-4">
                                    <div class="news">
                                        <div class="news__image">
                                            <img class="img-fluid" src="{{ storage($spec->image) }}" alt="">
                                        </div>
                                        <div class="news__title">
                                            <h5 class="text-uppercase font-size__small">{{ $spec->title }}</h5>
                                        </div>
                                        <div class="news__details">
                                            {!! $spec->body !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="tab-4" class="tab-content">
                        <div class="row">
                            @foreach ($specs['safety'] as $spec)
                                <div class="col-sm-4">
                                    <div class="news">
                                        <div class="news__image">
                                            <img class="img-fluid" src="{{ storage($spec->image) }}" alt="">
                                        </div>
                                        <div class="news__title">
                                            <h5 class="text-uppercase font-size__small">{{ $spec->title }}</h5>
                                        </div>
                                        <div class="news__details">
                                            {!! $spec->body !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
            <div class="download mt-4 text-center">
                @if ($model->brochure)
                    <a href="{{ storage($model->brochure) }}" download class="download__btn">Download Brochure
                        {{ $model->price }}
                        <span class="download__btn_icon"></span></a>
                @endif
            </div>
        </div>
    </section>
    @if (count($grades) != 0)
        <section class="py-5 mt-5 bg__grey">
            <div class="container">
                <div class="primary__title text-center">
                    <h2 class="mb-2">Explore <span>{{ $model->name }}</span></h2>
                </div>
                <div class="exploremore">
                    <div class="row mb-0 justify-content-center">
                        @foreach ($grades as $grade)
                            <div class="col-sm-4">
                                <a href="{{ url('buy-online/' . $model->slug . '?grade=' . $grade->id) }}">
                                    <div class="exploremore__car">
                                        <img src="{{ storage($grade->image) }}" alt="">
                                    </div>
                                    <div class="exploremore__info">
                                        <h4>{{ $model->name }} <span>{{ $grade->name }}</span></h4>
                                        {{-- <h6 class="text-center mt-1">Online Price: AED {{ number_format($grade->price) }}
                                            <small class="ml-1">Exclusive
                                                of
                                                vat</small> </h6> --}}
                                    </div>
                                </a>
                                <div class="key-features">
                                    <h5>Key features</h5>
                                    {!! $grade->body !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="download mt-4 text-center">
                    <a href="{{ url('compare/grades/' . $model->slug) }}" class="download__btn">Compare Grades<span
                            class="download__btn_icon icon-plus"></span></a>
                    <a href="{{ url('book-test-drive?model=' . $model->slug) }}"
                        class="download__btn download__btn-outline">Book a
                        test drive<span class="download__btn_icon icon-testdrive"></span></a>
                </div>
            </div>
        </section>
    @endif
    <section class="pt-5 mobile__pt_0">
        <div class="container">
            <div class="compare">
                <div class="col-lg-6 p-0">
                    <div class="compare__bg">
                        <img class="img-fluid" src="{{ asset('public/images/compare_bg.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 p-3 compare__content">
                    <h3>Compare {{ $model->name }} to other
                        changan models
                    </h3>
                    <p> A tailor- made functionality which provides you an option to compare and view CHANGAN models
                        with just a click on basis of performance, features and cost combinations to choose your best
                        suited vehicle.
                    </p>
                    <div class="c-button ">
                        <a href="{{ url('compare/models/' . $model->slug) }}" class="btn c-button__theme">COMPARE MODELS &
                            GRADES<span class="c-button__theme_arrow anim"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5 mobile__pt_0">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="two-block">

                        <div class="two-block__img overlay overlay_50">
                            <a id="play-video" class="video-play-button popup-youtube" href="{{ $model->video_1 }}">
                                <span></span>
                            </a>
                            <img class="img-fluid" src="{{ storage($model->video_bg) }}" alt="">
                        </div>
                        <div class="two-block__text">
                            <h5>Video Gallery</h5>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="two-block gallery-link">
                        <a href="#">
                            <div class="two-block__img">
                                <img class="img-fluid" src="{{ storage($model->gallery_bg) }}" alt="">
                            </div>
                            <div class="two-block__text">
                                <h5>Photo Gallery</h5>
                            </div>
                        </a>
                    </div>
                    <div class="gallery">
                        @foreach ($gallery as $image)
                            <a href="{{ storage($image->image) }}"></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.contact',['models'=>$models,'_model'=>$model->id])
@endsection
@push('script')
    <script src="{{ asset('public/dist/js/360.js') }}"></script>
@endpush
