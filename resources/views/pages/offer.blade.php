@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    {{-- <h1>Offers</h1> --}}
</div>
<section class="py-5">

    <div class="container">
        <div class="offer-card offer-card__popup">
            <div class="offer-card__image">
                <img class="img-fluid" src="{{storage($offer->image)}}" alt="">
            </div>
            <div class="offer-card__content">
                <div class="offer-card__content--title">
                    <h3>{{$offer->title}}</h3>
                    {!!$offer->body!!}
                </div>
                <section class="pt-4">
                    <div class="container">

                        <div class="text-center">
                            <h1 class="title__semibolod font-size__big">
                                Please complete the information below. <br class="hide-sm">We will contact you
                                shortly.
                            </h1>
                        </div>
                        <div class="content mt-2">
                            <div class="btd">
                                <div class="offset-sm-3 col-sm-6">
                                    <form action="" id="offer-form">
                                        <input type="hidden" name="source" value="offer-page">
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <input id="first_name" name="fname" type="text" class="validate">
                                                <label for="first_name">First Name</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="last_name" name="lname" type="text" class="validate">
                                                <label for="last_name">Last Name</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <input id="email" name="email" type="email" class="validate">
                                                <label for="email">Email</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input id="phone" name="phone" type="number" class="validate">
                                                <label for="phone">Mobile Number</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="model_id">
                                                    <option value="" disabled selected>Select you car model</option>
                                                    @foreach ($models as $model)
                                                    <option value="{{$model->id}}">{{$model->name}}</option>
                                                    @endforeach
                                                </select>
                                                <label>Select Preferred Model</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <select name="contact_id">
                                                    <option value="" disabled selected>Nearest Location</option>
                                                    @foreach ($locations as $location)
                                                    <option value="{{$location->id}}">{{$location->address}}</option>
                                                    @endforeach
                                                </select>
                                                <label>Preferred Location</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea id="textarea1" name="message" class="materialize-textarea"></textarea>
                                                <label for="textarea1">How did you hear about us?</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <label>
                                                    <input type="checkbox" />
                                                    <span>I'd like to receive marketing communication from
                                                        changan</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="download mt-3 text-center">
                                            <a href="#" onclick="$('#offer-form').submit()" class="download__btn">ENQUIRE NOW<span
                                                    class="download__btn_icon icon-send"></span></a>
                                        </div>
                                    </form>
                                    <div class="message"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
@endsection

@push('script')
    <script>
        $('#offer-form').on('submit', function(e) {
            e.preventDefault()
        })
        $('#offer-form').validate({
            rules: {
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true
                },
                contact_id:{
                    required:true
                },
                model_id:{
                    required:true
                }
            },
            ignore: "",
            submitHandler: function(form) {
                $('.message').html('Please wait...')
                var formData = $(form).serializeArray();
                $.ajax({
                    type: 'post',
                    url: url + '/contact',
                    dataType: "json",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function(data) {
                        $('.message').html(
                            `<span>Thank you for getting in touch! <br/>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!<br/>Have a great day!</span>`
                            )
                        setTimeout(() => {
                            location.replace(url+'/thank-you')
                        }, 3000);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            }
        })

    </script>
@endpush