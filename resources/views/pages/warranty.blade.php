@extends('layouts.base')
@include('layouts.seo')
@push('style')
<style>
    .container .aboutus ul:not(.browser-default)>li {
        list-style-type: disc !important;
    }
    .container .aboutus ul:not(.browser-default)>li ul>li{
        list-style-type: circle !important;
        margin-left: 20px
    }
</style>
@endpush
@section('content')
<div class="bg-breadcrumb">
    <h1>{{$page->title}} </h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <article class="mb-4">
                <div class="heading__wrap_800">
                    <h2 class="title__semibolod font-size__big">
                        {{$page->fields[1]->pivot->value}}
                    </h2>
                </div>
                <div class="content mt-2">
                    {!!$page->fields[2]->pivot->value!!}
                </div>
            </article>
            <article class="mb-4">
                <div class="heading__wrap_800">
                    <h2 class="title__semibolod font-size__big">
                        {{$page->fields[3]->pivot->value}}
                    </h2>
                </div>
                <div class="content mt-2">
                    {!!$page->fields[4]->pivot->value!!}
                </div>
            </article>
            <article>
                <div class="heading__wrap_800">
                    <h2 class="title__semibolod font-size__big">
                        {{$page->fields[5]->pivot->value}}
                    </h2>
                </div>
                <div class="content mt-2">
                    {!!$page->fields[6]->pivot->value!!}
                </div>
                <div class="download mt-3 text-center">
                    <a href="{{url('service-booking')}}" class="download__btn mr-1">Book a service<span
                            class="download__btn_icon icon-service"></span></a>
                    <a href="{{url('findlocation')}}" class="download__btn mr-1">Find Our Locations<span
                            class="download__btn_icon icon-track-white"></span></a>
                    <a href="{{url('enquire')}}" class="download__btn">Contact us<span
                            class="download__btn_icon icon-send"></span></a>
                </div>
            </article>

        </div>
    </div>
</section>
@endsection