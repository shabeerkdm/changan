@extends('layouts.base')
@include('layouts.seo')
@section('content')
    <section class="breadcrumb-section">
        <img class="img-fluid" src="{{ asset('public/images/contact_banner.jpg') }}" alt="">
        <div class="breadcrumb-overlay">
            <div class="container">
                <h1>Contact us</h1>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="row ">
                <div class="col-sm-3">
                    <div class="primary__title">
                        <h2 class="mb-1">Our Locations</h2>
                        <h6>Schedule your visit with us.</h6>
                    </div>

                </div>
                @foreach ($contacts as $contact)
                    <div class="col-sm-3">
                        <div class="locations__city">
                            <h4>{{ $contact->location }}</h4>
                        </div>
                        <div class="locations__info">
                            <div class="locations__info_address">
                                <div>{{ $contact->company }} </div>
                            </div>
                            <div class="locations__info_address">
                                <div>{{ $contact->address }}</div>
                            </div>
                            @if($contact->po_box)
                            <div class="locations__info_address">
                                <div>
                                    PO.BOX : {{ $contact->po_box }}
                                </div>
                            </div>
                            @endif
                            <div class="locations__info_phone">
                                <ul>
                                    <li>{{ $contact->phone }}</li>
                                </ul>
                            </div>
                            <div class="locations__info_email">
                                <ul>
                                    <li>{{ $contact->email }}</li>
                                </ul>
                            </div>
                            <h6 class="mt-3"><strong>Timings </strong></h6>
                            <div>Sales: {{$contact->timing_sales}}</div>
                            <div>Services: {{$contact->timing_services}}</div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row mb-0">
                <div class="offset-sm-3 col-sm-9">
                    <div class="download mt-3 text-left">
                        <a href="{{ url('book-test-drive') }}" class="download__btn mr-1">Book a test drive<span
                                class="download__btn_icon icon-testdrive-white"></span></a>
                        <a href="{{ url('findlocation') }}" class="download__btn mr-1">Find Our Locations<span
                                class="download__btn_icon icon-track-white"></span></a>
                        <a href="{{ url('enquire') }}" class="download__btn">Request a quote<span
                                class="download__btn_icon icon-send"></span></a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <section class="bg__grey py-5">
        <div class="container">
            <form action="" id="contact-form">

                <input type="hidden" name="source" value="contact-page">
                <div class="row mb-0">
                    <div class="col-sm-3">
                        <div class="primary__title">
                            <h2 class="mb-1">Work inquiries</h2>
                            <h6>Fill in this form or <a href="">send us an e-mail </a> swith your inquiry.</h6>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <input id="fname" type="text" name="fname" class="validate">
                                    <label for="fname">First Name</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <input id="lname" type="text" name="lname" class="validate">
                                    <label for="lname">Last Name</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <input id="email" type="email" name="email" class="validate">
                                    <label for="email">Email </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <input id="phone" type="number" name="phone" class="validate">
                                    <label for="phone">Phone</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <input id="subject" type="text" name="subject" class="validate">
                                    <label for="subject">Subject (optional)</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-field">
                                    <select name="model_id">
                                        <option value="" disabled selected>Please select your car</option>
                                        @foreach ($models as $model)
                                            <option value="{{ $model->id }}">{{ $model->name }}</option>
                                        @endforeach
                                    </select>
                                    <label>Choose your car (optional)</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-field">
                                    <textarea id="textarea1" name="message" class="materialize-textarea"></textarea>
                                    <label for="textarea1">Message</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="c-button text-left">
                                    <a href="javascript:;" onclick="$('#contact-form').submit()"
                                        class="btn c-button__theme">Send a message<span
                                            class="c-button__theme_arrow anim"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="message"></div>
        </div>
    </section>
@endsection

@push('script')
    <script>
        $('#contact-form').on('submit', function(e) {
            e.preventDefault()
        })
        $('#contact-form').validate({
            rules: {
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true
                }
            },
            ignore: "",
            submitHandler: function(form) {
                $('.message').html('Please wait...')
                var formData = $(form).serializeArray();
                $.ajax({
                    type: 'post',
                    url: url + '/contact',
                    dataType: "json",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function(data) {
                        $('.message').html(
                            `<span>Thank you for getting in touch! <br/>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!<br/>Have a great day!</span>`
                            )
                        setTimeout(() => {
                            location.replace(url+'/thank-you')
                        }, 3000);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            }
        })

    </script>
@endpush
