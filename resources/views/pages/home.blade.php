@extends('layouts.base')
@include('layouts.seo')
@section('content')
    @include('layouts.slider',['slider'=>$page->slider])
    <div class="video-banner d-none d-sm-block">
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
            <source src="{{ storage($page->fields[7]->pivot->value) }}" type="video/mp4">
        </video>
        <div class="video-banner__overlay">
            <div class="container">

                <h1 class="title__semibolod font-size__big text-center">
                    {{$page->fields[8]->pivot->value}}
                </h1>
                <h3>{{$page->fields[9]->pivot->value}}
                </h3>
            </div>

        </div>
    </div>
    <section class="py-5">
        <div class="container">
            <div class="primary__title text-center">
                <h2>Discover your perfect Changan</h2>
            </div>
            <div class="cars-slider cars mt-4">
                @foreach ($models as $model)
                    @if($model->category == 1)
                    <div class="cars__block">
                        <a href="{{ url('vehicle/' . $model->slug) }}">
                            <div class="cars__block_img">
                                <img src="{{ storage($model->image) }}" alt="">
                            </div>
                            <div class="cars__block_text">
                                {{ $model->name }}
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
    <section class="pb-5 ">
        <div class="container">
            <div class="primary__title text-center">
                <h2>What would you like to do </h2>
            </div>
            <div class="feature">
                <div class="row">

                    <div class="col">
                        <a href="{{ url('/book-test-drive') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-steering-wheel"></i>
                                </div>
                                <div class="feature__col_title">
                                    Book a test DRIVE
                                </div>
                            </div>
                        </a>
                    </div>

                    {{-- <div class="col">
                        <a href="{{ url('/car-online') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-booking"></i>
                                </div>
                                <div class="feature__col_title">
                                    Buy or Reserve Online
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="{{ url('/service-booking') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-wrench"></i>
                                </div>
                                <div class="feature__col_title">
                                    Book a service
                                </div>
                            </div>
                        </a>
                    </div> --}}

                    <div class="col">
                        <a href="{{ url('/findlocation') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-pin"></i>
                                </div>
                                <div class="feature__col_title">
                                    FIND location
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('/enquire') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-discount"></i>
                                </div>
                                <div class="feature__col_title">
                                    Request a quote
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="uni_home_section pb-5">
        <div class="container">
            <div class="uni_car_box">
                <div class="uni_text">
                    <div class="uni_logo">
                        <img src="{{asset('public/images/uni/uni-logo.png')}}" class="img-fluid" alt="">
                    </div>
                    <h2>Go beyond the distance
                        with the latest UNI series</h2>
                </div>
                <div class="uni_item">
                    <ul>
                        @foreach ($models as $model)
                            @if($model->category == 2)
                            <li>
                                <div class="cars__block">
                                    <a href="{{ url('vehicle/' . $model->slug) }}">
                                        <div class="cars__block_img">
                                            <img src="{{ storage($model->image) }}" alt="" class="img-fluid">
                                        </div>
                                        <div class="cars__block_text">
                                            {{ $model->name }}
                                        </div>
                                    </a>
                                </div>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="no-gutter two-column-block">
            <div class="no-gutter__2 two-column-block__left bg__grey p-5">
                <div class="primary__title">
                    <h2>Grab your favourite <br>
                        Changan online
                    </h2>
                </div>

                <div class="process">
                    <div class="process__wrapper">
                        <div class="process__wrapper_icon">
                            <svg class="icon">
                                <use xlink:href="#car" />
                            </svg>
                        </div>
                        <div class="process__wrapper_content">
                            <div class="process__wrapper_content_title">Explore the automobile world from anywhere, anytime
                            </div>
                            <div class="process__wrapper_content_info">
                                <p>Look for your favorite models on the website with the aid of video brochures and product
                                    descriptions at your convenience.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="process__wrapper">
                        <div class="process__wrapper_icon">
                            <svg class="icon">
                                <use xlink:href="#credit-card" />
                            </svg>
                        </div>
                        <div class="process__wrapper_content">
                            <div class="process__wrapper_content_title">Reserve your Changan from the safety of your home
                            </div>
                            <div class="process__wrapper_content_info">
                                <p>Select the dealership that you prefer from the options provided and pay the booking
                                    amount,
                                    and they will call you right back.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="process__wrapper">
                        <div class="process__wrapper_icon">
                            <svg class="icon">
                                <use xlink:href="#shopping-bags" />
                            </svg>
                        </div>
                        <div class="process__wrapper_content">
                            <div class="process__wrapper_content_title">Flexible payment options and hassle-free delivery
                            </div>
                            <div class="process__wrapper_content_info">
                                <p>Multiple financial options offered to ease the burden of heavy payment. Simply pick up at
                                    the
                                    dealership or get delivered to your home.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="c-button mt-4">
                        {{-- <a href="{{ url('car-online') }}" class="btn c-button__theme">Buy or Reserve Online<span
                                class="c-button__theme_arrow anim"></span>
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="no-gutter__2 two-column-block__right">
                <div class="video__img h-100">
                    <img src="{{ storage($page->fields[5]->pivot->value) }}" alt="">
                </div>
                <div class="video__link">
                    <div class="video__link_icon">
                        <a id="play-video" class="video-play-button popup-youtube"
                            href="{{ $page->fields[3]->pivot->value }}">
                            <span></span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="py-5 mobile__pt_0">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{ url('compare') }}">
                        <div class="two-block">
                            <div class="two-block__img">
                                <img class="img-fluid" src="{{ storage($page->fields[4]->pivot->value) }}" alt="">
                            </div>
                            <div class="two-block__text">
                                <h5>Compare Changan Models</h5>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="{{ url('car-online') }}">
                        <div class="two-block">
                            <div class="two-block__img">
                                <img class="img-fluid" src="{{ storage($page->fields[5]->pivot->value) }}" alt="">
                            </div>
                            <div class="two-block__text">
                                <h5>Discover your Favourite Changan</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-5">
        <div class="container">
            <div class="primary__title text-center">
                <h2 class="mb-2">Why Changan </h2>
                <p>Changan with its high-quality service, wide model range with advanced innovative technology,
                    guaranteed reliable customer service and exceptional professional maintenance support team has
                    earned an enviable reputation for being one of the trusted automobile companies in UAE.
                </p>
            </div>
            <div class="feature">
                <div class="row">
                    <div class="col">
                        <a href="{{ url('design') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-car"></i>
                                </div>
                                <div class="feature__col_title">
                                    Design
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('innovations') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-security"></i>
                                </div>
                                <div class="feature__col_title">
                                    INNOVATION
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('intelligence') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-reward"></i>
                                </div>
                                <div class="feature__col_title">
                                    SAFETY
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('quality') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <img style="width:40px;" src="{{ asset('public/images/idea.svg') }}" alt="">
                                </div>
                                <div class="feature__col_title">
                                    QUALITY
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col">
                        <a href="{{ url('sustainable') }}">
                            <div class="feature__col">
                                <div class="feature__col_icon">
                                    <i class="fi flaticon-shuttle"></i>
                                </div>
                                <div class="feature__col_title">
                                    SUSTAINABLE
                                    DEVELOPMENT
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ------ -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="primary__title text-left">
                        <h2>Innovation with care</h2>
                    </div>
                    <p>Changan is one of the most trusted Chinese brands in the world when it comes to automobile
                        manufacturing. For
                        the last 34 years, we have been reflecting on the values represented by our brand name itself-
                        ‘Enduring
                        Safety’. And now we are going to revolutionize the auto industry by introducing online automobile
                        retailing.
                    </p>
                    <div class="about_features">
                        <ul>
                            <li><span><svg class="icon">
                                        <use xlink:href="#checkmark" />
                                    </svg></span>Best selling Chinese car company across the globe</li>
                            <li><svg class="icon">
                                    <use xlink:href="#checkmark" />
                                </svg>8,500+ consumers buy a new Changan each day</li>
                            <li><svg class="icon">
                                    <use xlink:href="#checkmark" />
                                </svg>6,000 sales and service centers in 60 countries</li>
                        </ul>
                    </div>

                    <div class="c-button mt-4">
                        <a href="{{ url('innovations') }}" class="btn c-button__theme">Read More<span
                                class="c-button__theme_arrow anim"></span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 offset-md-1 about_grphic">
                    <img class="desktop img-fluid" src="{{ storage($page->fields[6]->pivot->value) }}" alt="">
                    <img class="mobile img-fluid" src="{{ asset('public/images/about_1_mobile.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <div class="primary__title text-center">
                <h2>News / Media</h2>
            </div>
            <div class="row">
                @foreach ($news as $post)
                    <div class="col-sm-4">
                        <div class="news-block">
                            <a href="{{ url('news/' . $post->slug) }}">
                                <div class="news-block__image news__image">
                                    <img class="img-fluid" src="{{ storage($post->image) }}" alt="">
                                </div>
                                <div class="news__title">
                                    <h5>{{ $post->title }}</h5>
                                </div>
                                <div class="news__details">
                                    {!! $post->description !!}
                                </div>
                        </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="c-button text-center mt-4">
                <a href="{{ url('news') }}" class="btn c-button__theme">Read More<span
                        class="c-button__theme_arrow anim"></span>
                </a>
            </div>
        </div>
    </section>
    @include('layouts.contact',['models'=>$models])
@endsection
