@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Service Booking</h1>
</div>
<section>
    <img class="img-fluid" src="{{asset('public/images/tracking_banner.jpg')}}" alt="">
</section>
<section class="py-5">
    <div class="container">

        <div class="text-center">
            <h1 class="title__semibolod font-size__big">
                Sign up for maintenance and service
            </h1>
        </div>
        <div class="content mt-2">
            <div class="btd">
                <div class="offset-sm-3 col-sm-6">
                    <form action="" id="service-form">
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" type="text" name="fname" class="validate">
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" type="text" name="lname" class="validate">
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="email" type="email" name="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="phone" type="text" name="phone" class="validate">
                                <label for="phone">Mobile Number</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <select name="model_id">
                                    <option value="" disabled selected>Select you car model</option>
                                    @foreach ($models as $model)
                                    <option value="{{$model->id}}">{{$model->name}}</option>
                                    @endforeach
                                </select>
                                <label>Select Model</label>
                            </div>
                            <div class="input-field col s6">
                                <select name="contact_id">
                                    <option value="" disabled selected>Preferred Workshop</option>
                                    @foreach ($locations as $location)
                                    <option value="{{$location->id}}">{{$location->address}}</option>
                                    @endforeach
                                </select>
                                <label>Preferred Location</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <select id="serviceType" type="text" name="types" class="validate">
                                    <option value="Regular Maintenance">Regular Maintenance</option>
                                    <option value="Check up">Check up</option>
                                </select>
                                <label for="serviceType">Type of Service Required</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="plateNumber" type="text" name="plate_number" class="validate">
                                <label for="plateNumber">Plate Number</label>
                            </div>
                            <div class="input-field col s6">
                                <ion-icon class="material-icons prefix" name="calendar-outline"></ion-icon>
                                <input type="text" placeholder="Select Date" name="date" class="datepicker" />
                                <label>Select Date</label>
                            </div>
                            {{-- <div class="input-field col s6">
                                <ion-icon class="material-icons prefix" name="time-outline"></ion-icon>
                                <input type="text" placeholder="Select Time" class="timepicker" />
                                <label>Prefered Time</label>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="textarea1" name="comments" class="materialize-textarea"></textarea>
                                <label for="textarea1">Comments</label>
                            </div>
                        </div>
                        <div class="download mt-3 text-center">
                            <a href="javascript:;" onclick="$('#service-form').submit()" class="download__btn">ENQUIRE NOW<span
                                    class="download__btn_icon icon-send"></span></a>
                            <a href="#" class="download__btn download__btn-outline">Track your
                                service<span class="download__btn_icon icon-track"></span></a>
                        </div>
                    </form>
                    <div id="message"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script src="{{user_js('service.js')}}"></script>
@endpush