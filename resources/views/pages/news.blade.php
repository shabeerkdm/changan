@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>News & Events</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="row" id="posts">
            @foreach ($posts as $post)
            <div class="col-sm-4">
                <div class="news-block">
                    <a href="{{url('news/'.$post->slug)}}">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{storage($post->image)}}" alt="">
                        </div>
                        <div class="news__title">
                            <h5>{{$post->title}}</h5>
                        </div>
                        <div class="news__details">
                            {!!$post->description!!}
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="c-button text-center mt-4">
            <a href="javascript:;" id="load-more" class="btn c-button__theme">Load More<span
                    class="c-button__theme_arrow anim"></span>
            </a>
        </div>
    </div>
</section>
<input type="hidden" name="url" value="{{url('')}}">
<input type="hidden" name="storage" value="{{storage('')}}">
@endsection
@push('script')
<script src="{{user_js('news.js')}}"></script>
@endpush