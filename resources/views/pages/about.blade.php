@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>{{$page->fields[0]->pivot->value}}</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_500">

                <h1 class="title__semibolod font-size__big">
                    {{$page->fields[2]->pivot->value}}
                </h1>
            </div>
            <div class="content mt-2">
                {!!$page->fields[3]->pivot->value!!}
            </div>
        </div>
    </div>
</section>
<section class="background">
    <div class="spacer__40"></div>
    <div class="container overlay__info">
        <div class="row mb-0 flex-row-reverse">
            <div class="col-sm-3 pl-0">
                <div class="brand-info brand-info__theme">
                    <div class="brand-info__count count">
                        <div class="count__icon"></div>
                        <div class="count__number">
                            #1
                        </div>
                    </div>
                    <h6 class="mb-0">CHINESE <br> Brand</h6>
                </div>
            </div>
            <div class="col-sm-3 pr-0">
                <div class="brand-info brand-info__alt">
                    <div class="brand-info__count count">
                        <div class="count__icon"></div>
                        <div class="count__number">
                            65+
                        </div>
                    </div>
                    <h6 class="mb-0">countries</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="background__paralax" style="background-image: url('{{storage($page->fields[4]->pivot->value)}}')" data-bottom-top="top: -30%;" data-top-bottom="top: 0%;">
    </div>
</section>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_500">
                <h1 class="title__semibolod font-size__big">
                    {{$page->fields[5]->pivot->value}}
                </h1>
            </div>
            <div class="content mt-2">
                {!!$page->fields[6]->pivot->value!!}
            </div>
        </div>
    </div>
</section>
<section class="pb-5">
    <div class="container">
        <div class="sales-slider">
            @if(count($page->slider->slides) > 0)
            @foreach ($page->slider->slides as $slide)
            <div class="sales-slider__item  ">
                <img class="img-fluid" src="{{storage($slide->image)}}" alt="">
            </div>
            @endforeach
            @endif
        </div>
        <div class="heading__wrap_500 mt-4">
            <p class="subtitle">OUR SUCCESS</p>
            <h1 class="title__semibolod font-size__big">
                {{$page->fields[7]->pivot->value}}
            </h1>
        </div>
        <div class="content mt-2">
            {!!$page->fields[8]->pivot->value!!}
        </div>
    </div>
</section>

<section class="py-5 bg__grey">
    <div class="container">
        <div class="about-slider">
            <div class="carousel">
                <div class="carousel-item">
                    <div class="news-block about-info-block">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{asset('public/images/about_block_1.jpg')}}" alt="">
                        </div>
                        <div class="news__details">
                            <p>In the Chinese language, CHANG means lasting and AN means safety. Therefore, CHANG AN
                                together represents the idea of “Lasting Safety”. We sincerely hope that Changan will
                                bring you both safety and happiness. </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="news-block about-info-block">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{asset('public/images/about_block_2.jpg')}}" alt="">
                        </div>
                        <div class="news__details">
                            <p>As one of the top four automobile groups in China and the top selling domestic Chinese
                                automotive brand, Changan Automobile boasts an industrial history stretching back 157
                                years. With 35 years of experience in building and selling passenger vehicles, Changan
                                is an early leader in the Chinese auto industry. </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="news-block about-info-block">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{asset('public/images/about_block_3.jpg')}}" alt="">
                        </div>
                        <div class="news__details">
                            <p>Each day, more than 8,500 consumers buy a new Changan. With 6,000 sales and service
                                facilities in more than 60 countries, and more than 150,000 professional team members
                                globally, we stand ready to provide you considerate and prompt service 24 hours a day.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="news-block about-info-block">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{asset('public/images/about_block_4.jpg')}}" alt="">
                        </div>
                        <div class="news__details">
                            <p>Changan invests extensively in advanced research and innovative new technologies. Part of
                                this commitment has resulted in the formation of a global R&D organization. Our
                                international teams work at facilities around the world to bring you advanced, high-tech
                                products, trend-setting vehicle designs, innovative safety technologies, and strong
                                performance. </p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="news-block about-info-block">
                        <div class="news-block__image news__image">
                            <img class="img-fluid" src="{{asset('public/images/about_block_5.jpg')}}" alt="">
                        </div>
                        <div class="news__details">
                            <p>Building cars and trucks that please our customers is what motivates us in everything we
                                do. As we introduce Changan to new markets around the world, we strive to always meet
                                the ever changing needs of our customers no matter where they live.
        
                                Changan - Driving forward with You!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5 ">
    <div class="container">
        <div class="cta-block">
            <div class="cta-block__details">
                <h3 class="title__semibolod font-size__big">
                    To know more about <br class="hide-sm"> our cars & booking
                </h3>
            </div>
            <div class="cta-block__action">
                <div class="c-button text-left">
                    <a href="{{url('book-test-drive')}}" class="btn c-button__theme c-button__light">Book a dest drive<span
                            class="c-button__theme_arrow arrow-white anim"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection