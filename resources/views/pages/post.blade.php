@extends('layouts.base')
@include('layouts.seo')
@section('content')
<section class="py-5 border__top_1">
    <div class="container">
        <div class="news">
            <div class="news__title_wrap">
                <h1 class="title__semibolod font-size__big">
                    {{$post->title}}
                </h1>
            </div>
            <div class="news__content mt-2">
                {!!$post->body!!}
            </div>
        <!--    <div class="news__recent mt-4">-->
        <!--        <h3>More News</h3>-->
        <!--        <div class="row">-->
        <!--            @foreach ($recent_posts as $post)    -->
        <!--            <div class="col-sm-4">-->
        <!--                <div class="news-block">-->
        <!--                    <a href="{{url('news/'.$post->slug)}}">-->
        <!--                        <div class="news-block__image news__image">-->
        <!--                            <img class="img-fluid" src="{{storage($post->image)}}" alt="">-->
        <!--                        </div>-->
        <!--                        <div class="news__title">-->
        <!--                            <h5>{{$post->title}}</h5>-->
        <!--                        </div>-->
        <!--                        <div class="news__details">-->
        <!--                            {!!$post->description!!}-->
        <!--                        </div>-->
        <!--                </div>-->
        <!--                </a>-->
        <!--            </div>-->
        <!--            @endforeach-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
</section>
@endsection
