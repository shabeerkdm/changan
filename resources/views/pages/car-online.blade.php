@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Buy your car online</h1>
</div>
<section class="pt-5">
    <div class="container">
        <div class="row mb-0">
            <div class="col-sm-4">
                <label>Please select car</label>
                <select class="browser-default" id="select-model">
                    <option value="0" selected>All Models</option>
                    @foreach ($models as $model)
                    <option value="{{$model->slug}}">{{$model->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4">
                <label>Please select Grades</label>
                <select class="browser-default" id="select-grade">
                    <option value="0" selected>All Grades</option>
                </select>
            </div>
            {{-- <div class="col-sm-4">
                <label>Price Range</label>
                <div class="pricerange">
                    <div id="pricerange" data-min="{{$range[0]->min / 5000}}" data-max="{{$range[0]->max / 5000}}"></div>
                    <div class="pricerange__result">
                        AED &nbsp;
                        <span id="lower-value"></span> -
                        <span id="upper-value"></span>
                    </div>
                </div>

            </div> --}}
        </div>
    </div>
</section>
<section class="pt-4 pb-5">
    <div class="container-fluid">
        <div class="row" id="vehicles">
            @foreach ($grades as $grade)
            <div class="col-md-3 col-sm-6 vehicle" data-model="{{$grade->slug}}" data-grade="{{$grade->id}}"
                data-price="{{$grade->price}}">
                <div class="offer-card">
                    <div class="offer-card__image">
                        <img class="img-fluid" src="{{storage($grade->image)}}" alt="">
                    </div>
                    <div class="offer-card__content bg__grey">
                        <div class="offer-card__content--title">
                            <h3>{{$grade->model_name}} <span>{{$grade->name}}</span></h3>
                            {{-- <h5>Online Price: AED {{number_format($grade->price)}} <small>exclusive of vat</small></h5> --}}
                        </div>
                        <div class="offer-card__content--offer">
                            {!!$grade->body!!}
                        </div>
                        <div class="offer-card__content--btn">
                            <a href="{{url('buy-online/'.$grade->slug.'?grade='.$grade->id)}}"
                                class="download__btn open-popup-link">Buy or reserve</a>
                            <a href="{{url('vehicle/'.$grade->slug)}}"
                                class="download__btn download__btn-outline open-popup-link">Discover
                                More</a>

                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
@push('script')
<script src="{{user_js('car-online.js')}}"></script>
@endpush