@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>BUY OR RESERVE</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                @if(count($grade->Clrs) != 0)
                <div class="car-pic car-1"
                    style="background-image: url('{{storage($grade->Clrs[0]->image)}}');background-size: 100%;">
                </div>
                <div class="car-colors">
                    @if(count($grade->Clrs) != 0)
                    @foreach ($grade->Clrs as $color)
                    @if($color->stock)
                    <span class="@if ($loop->first) active @endif"
                        style="background-color: {{$color->color}};color:{{$color->color}}"
                        data-pic="{{storage($color->image)}}" data-id="{{$color->id}}"></span>
                    @endif
                    @endforeach
                    @endif
                </div>
                @endif
                <div class="pr-4 mt-5">
                    <div class="buy-online mt-3">
                        <ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                    role="tab" aria-controls="pills-home" aria-selected="true">Vehicle Loan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                                    role="tab" aria-controls="pills-profile" aria-selected="false">Reserve Vehicle</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">Buy Vehicle</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                aria-labelledby="pills-home-tab">
                                <div id="widget">
                                    {{-- <div class="form-group">
                                        <label for="loan-amount">Down Payment: </label> <span
                                            id="selected-amount"></span>
                                        <input type="range" id="loan-amount" min="1000" max="{{$grade->price}}"
                                            step="1000" value="1000">
                                    </div>

                                    <div class="form-group">
                                        <label for="loan-duration">Months:</label><span id="selected-duration"></span>
                                        <input type="range" id="loan-duration" min="12" max="60" step="6" value="12">
                                    </div>
                                    <input type="hidden" name="interest" value="{{$grade->intrest}}">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Vehicle Cost</td>
                                                <td><span id="total-price">AED {{number_format($grade->price)}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Down payment:</td>
                                                <td><span id="down-payment"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Loan amount</td>
                                                <td><span id="total-loan"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Term (Months)</td>
                                                <td><span id="total-term"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Total Interest</td>
                                                <td><span id="total-interest"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Sum Total</td>
                                                <td><span id="total-payable"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Monthly Installments</td>
                                                <td><span id="monthly-payment"></span></td>
                                            </tr>
                                        </tbody>

                                    </table> --}}
                                    <div class="mt-2">
                                        <p>The {{$grade->intrest}}% annual flat interest rate is the calculations shown
                                            above. The calculations for the monthly payments are just for references
                                            only and are shown as an estimate, Interest rate may vary depending on the
                                            customer profile. For more information, kindly contact 800 Changan
                                            (2426426).</p>
                                    </div>
                                </div>
                                <div class="loan__actions mt-3">
                                    <ul>
                                        <li> <a class="btn btn-primary mb-1"
                                                href="{{url('book-test-drive?grade='.$grade->id)}}">BOOK A TEST
                                                DRIVE</a></li>
                                        <li> <a class="btn btn-primary"
                                                href="{{url('enquire?grade='.$grade->id)}}">ENQUIRE NOW</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                aria-labelledby="pills-profile-tab">
                                <div class="deposit">AED 1,000</div>
                                <p class="text-center">Reserve your Changan car today and enjoy free cancellation.Terms
                                    and Conditions apply
                                </p>
                                <div class="loan__actions mt-3">
                                    @if($grade->stock)
                                    <ul>
                                        <li> <a class="btn btn-primary" href="javascript:;"
                                                onclick="$('#reserve-form').submit()">BOOK
                                                NOW</a></li>
                                    </ul>
                                    @else
                                    <h3>Out of stock!</h3>
                                    @endif
                                </div>
                                {{-- <div class="deposit">This option currently unavalible,Please contact us to know more
                                    </small></div>
                                <!-- <p class="text-center">Pay the full amount online.
                                Includes VAT charges.</p> -->
                                <div class="loan__actions mt-3">
                                    <ul class="justify-content-center">
                                        <li> <a class="btn btn-primary"
                                                href="{{url('enquire?grade='.$grade->id)}}">Contact Us</a></li>
                                </ul>
                            </div>--}}
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                            aria-labelledby="pills-contact-tab">
                            <div class="deposit">This option currently unavalible,Please contact us to know more
                                </small></div>
                            <!-- <p class="text-center">Pay the full amount online.
                                    Includes VAT charges.</p> -->
                            <div class="loan__actions mt-3">
                                <ul class="justify-content-center">
                                    <li> <a class="btn btn-primary" href="{{url('enquire?grade='.$grade->id)}}">Contact
                                            Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-5">
            <div class="car-details">
                <h3 class="car-name">{{$grade->model_name}} <small>{{$grade->name}}</small></h3>
                <div class="car-details__spec">
                    <div class="cardetails-accordion">
                        @foreach ($groups as $group)
                        <div class="cardetails__block">
                            <div class="car-details__spec_head">{{$group->group_name}} <span class="acc-arrow"></span>
                            </div>
                            <div class="cardetails">
                                <table class="table table-bordered">
                                    <tbody>
                                        @foreach ($group->Atts as $att)
                                        <tr>
                                            <td>{{$att->attribute}}</td>
                                            <td>{{$att->value}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <form action="{{url('reserve')}}" method="POST" class="d-none" id="reserve-form">
        @csrf
        <input type="hidden" name="grade_id" value="{{$grade->id}}">
        <input type="hidden" name="grade_stock" value="{{$grade->stock}}">
        @if(count($grade->Clrs) != 0)
        <input type="hidden" name="color_id" value="{{$grade->Clrs[0]->id}}">
        @endif
    </form>
</section>
@endsection
@push('script')
<script src="{{user_js('buy-online.js')}}"></script>
@endpush