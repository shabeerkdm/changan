@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Offers</h1>
</div>

<section class="pt-5 pb-5">
    <div class="container offer-tab">
        <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                    aria-selected="true">Sales</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                    aria-controls="profile" aria-selected="false">After Sales</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="offer-filter mt-3">
                    <div class="row">
                        <div class="col-sm-3">
                            <select class="browser-default" id="model-filter">
                                <option value="0">All</option>
                                @foreach ($models as $model)
                                <option value="{{$model->id}}">{{$model->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-9 d-none" id="filter">
                            <div class="offer-filter__results filter-results">
                                <div class="filter-results__title">
                                    Filter Results
                                </div>
                                <div class="filter-results__col">
                                    <div class="off-car-name"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="vehicles">
                    @foreach ($offers as $offer)
                    @if ($offer->type == 1)
                    <div class="col-sm-4 vehicle" data-model="model_{{$offer->model_id}}">
                        <div class="offer-card">
                            <div class="offer-card__image">
                                <img class="img-fluid" src="{{storage($offer->image)}}" alt="">
                            </div>
                            <div class="offer-card__content">
                                <div class="offer-card__content--title">
                                    <h3>{{$offer->title}}</h3>
                                </div>
                                {!!$offer->description!!}
                                <div class="offer-card__content--btn">
                                    <a href="#offer-popup" class="download__btn offer-popup" data-effect="mfp-3d-unfold"
                                        data-id="{{$offer->id}}">View Details
                                    </a>
                                    <a href="{{url('enquire')}}" class="download__btn download__btn-outline">Enquire Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row mt-4">
                    @foreach ($offers as $offer)
                    @if ($offer->type == 2)
                    <div class="col-sm-4">
                        <div class="offer-card">
                            <div class="offer-card__image">
                                <img class="img-fluid" src="{{storage($offer->image)}}" alt="">
                            </div>
                            <div class="offer-card__content">
                                <div class="offer-card__content--title">
                                    <h3>{{$offer->title}}</h3>
                                </div>
                                <div class="offer-card__content--btn">
                                    <a href="{{url('sales/'.$offer->slug)}}" class="download__btn ">Discover the offer
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" name="storage" value="{{storage('')}}">
<div id="offer-popup" class="white-popup mfp-with-anim mfp-hide">
    <div class="offer-card offer-card__popup">   
    </div>
</div>
@endsection

@push('script')
<script src="{{user_js('offer.js')}}"></script>
@endpush