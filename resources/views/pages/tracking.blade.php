@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Service Tracking</h1>
</div>
<section>
    <img class="img-fluid" src="{{asset('public/images/tracking_banner.jpg')}}" alt="">
</section>
<section class="pt-5 pb-3">
    <div class="container">
        <div class="row">
            <div class="offset-sm-3 col-sm-6">
                <form action="" id="tracking-form">
                    <div class="tracking__group mb-3">
                        <input class="tracking__group_input" name="plate_number" placeholder="Enter your plate number" type="text">
                        {{-- <button class="tracking__group_btn">Track service</button> --}}
                    </div>
                    <div class="tracking__group mb-3">
                        <input class="tracking__group_input" name="phone" placeholder="Enter your mobile number" type="text">
                        <button class="tracking__group_btn">Track service</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="py-5 bg__grey d-none status">
    <div class="container">
        <div class="mb-3">
            <h3 class="text-center">Your car service status</h3>
        </div>
        <div id="status"></div>
    </div>
</section>
@endsection
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="{{user_js('tracking.js')}}"></script>
@endpush