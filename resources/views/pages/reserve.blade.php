@extends('layouts.base')
@include('layouts.seo')
@push('style')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
    label.error{
        position: relative;
        margin-top: 5px
    }
     .btn-otp {
        width: 100%;
    }

    .btn-otp a {
        font-size: 9px !important;
        background: #03448d !important;
        height: 46px !important;
        margin-left: -21px !important;
    }

    @media (max-width:800px) {


        .btn-otp a {
            height: 43px !important;
        }
    }

</style>
@endpush
@section('content')
<div class="bg-breadcrumb">
    <h1>Reserve your car

    </h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="reservenow">
                    <div class="reservenow__img">
                        <img class="img-fluid" src="{{storage($color->image)}}" alt="">
                    </div>
                    <div class="reservenow__info">
                        <h3>Your Car Information</h3>
                        <div class="payamount car-details">
                            <h6>Pay Deposit</h6>
                            <h3 class="car-name ">AED 1,000</h3>
                        </div>
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Car Model</td>
                                    <td>{{$grade->model_name}}</td>
                                </tr>
                                <tr>
                                    <td>Grade</td>
                                    <td>{{$grade->name}}</td>
                                </tr>
                                <tr>
                                    <td>Color</td>
                                    <td>
                                        <div class="d-flex align-items-center"><span class="selected-color"
                                                style="background-color: {{$color->color}};color:{{$color->color}}">
                                            </span> {{$color->name}}</div>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <td>Suspensions (F/R)</td>
                                    <td>McPherson Independent / Torsion beam non-independent </td>
                                </tr>
                                <tr>
                                    <td>Brakes System (F/R)</td>
                                    <td> Ventilated Discs / Drum </td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class=" col-sm-5 offset-md-1">
                <div class="car-details">
                    <h3 class="car-name ">Submit your Information</h3>
                    <div class="car-tag">Please provide a few personal details</div>
                    <div class="car-details__spec">
                        <div class="cardetails">
                            <div class="cardetails__block">
                                <div class="cardetails">
                                    <form action="" id="reserve-form">
                                        @csrf
                                        <input type="hidden" name="grade_id" value="{{$grade->id}}">
                                        <input type="hidden" name="color_id" value="{{$color->id}}">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="fname" type="text" name="fname" class="validate">
                                                    <label for="fname">First
                                                        Name</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="lname" type="text" name="lname" class="validate">
                                                    <label for="lname">Last Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-field">
                                                    <input id="email" type="email" name="email" class="validate">
                                                    <label for="email">Email
                                                        Address*</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-8">
                                                <div class="input-field">
                                                    <input id="number" type="tel" name="phone" class="validate">
                                                    <!--<a href="javascript:;" onclick="sendOtp()"-->
                                                    <!--    class="button__arrow">Click</a>-->
                                                    <label for="number">Contact
                                                        Number* <span style="font-size: 8px">(Format. Eg:- 971XXXXXXXXX)</span></label>

                                                    <span class="helper-text" style="margin-top: 10px">Varification
                                                        Required</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-4">
                                                <div class="input-field">
                                                    <div class="loan__actions mt-2">
                                                        <ul class="text-center">
                                                            <li class="btn-otp"> <a class="btn btn-primary" href="javascript:;" onclick="sendOtp()">SEND
                                                                    OTP</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="input-field">
                                                    <input id="otp" type="number" name="otp" placeholder="OTP" class="validate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="bulding" type="text" name="address_1" class="validate">
                                                    <label for="bulding">Building/App/Villa*</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="area" type="text" name="address_2" class="validate">
                                                    <label for="area">Area/Street/Landmark*</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="password" type="password" name="password"
                                                        class="validate">
                                                    <label for="password">Enter
                                                        password</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="c-password" type="password" name="password_confirm"
                                                        class="validate">
                                                    <label for="c-password">Confirm
                                                        Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="loan__actions mt-3">
                                            <ul>
                                                <li> <a class="btn btn-primary" href="javascript:;"
                                                        onclick="$('#reserve-form').submit()">PROCEED
                                                        TO PAYMENT</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </form>
                                    <div id="message"></div>
                                </div>
                            </div>
                            {{-- <div class="cardetails__block">
                                <div class="car-details__spec_head" id="payment">PAYMENT
                                    INFORMATION<span class="acc-arrow"></span>
                                </div>
                                <div class="cardetails">
                                    <form action="" id="payment-form">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-field">
                                                    <input id="cname" type="text" class="cname" class="validate">
                                                    <label for="cname">Card Holder
                                                        Name</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-field">
                                                    <input id="cnumber" type="text" name="cnumber" class="validate">
                                                    <label for="cnumber">Card
                                                        Number</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="expm" type="text" name="expm" class="validate">
                                                    <label for="expm">Exp.Month</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="input-field">
                                                    <input id="expy" type="text" name="expy" class="validate">
                                                    <label for="expy">Exp.Year</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="input-field">
                                                    <input id="ccv" type="text" name="ccv" class="validate">
                                                    <label for="ccv">CCV Number</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="loan__actions mt-3">
                                            <ul>
                                                <li> <a class="btn btn-primary" href="javascript:;"
                                                        onclick="$('#payment-form').submit()">PROCEED</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script src="{{user_js('reserve.js')}}"></script>
@endpush