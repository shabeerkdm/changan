@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Compare cars</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="compare-block">
            <div class="compare-block__header">
                <div class="row">
                    <div class="col-sm-3 d-flex align-items-center">
                        <h3>Specifications</h3>
                    </div>
                    @if($data && $type == 'grades')
                    <div class="col-sm-3 middle-block vehicle" id="car-1">
                        <div class="compare-block__header--car">
                            <img class="img-fluid add-{{$type}}" data-id="{{$data[0]->id}}" src="{{storage($data[0]->image)}}" alt="">
                        </div>
                        <div class="compare-block__header--carname">
                            {{$data[0]->model_name}}<small>{{$data[0]->name}}</small>
                        </div>
                    </div>
                    @else
                    <div class="col-sm-3 middle-block vehicle add-car" id="car-1">
                        <a href="#" class="download__btn add-{{$type}}">ADD VEHICLE<span
                                class="download__btn_icon icon-plus"></span></a>
                    </div>
                    @endif
                    <div class="col-sm-3 middle-block vehicle add-car" id="car-2">
                        <a href="#" class="download__btn add-{{$type}}">ADD VEHICLE<span
                                class="download__btn_icon icon-plus"></span></a>

                    </div>
                    <div class="col-sm-3 vehicle add-car" id="car-3">
                        <a href="#" class="download__btn add-{{$type}}">ADD VEHICLE<span
                                class="download__btn_icon icon-plus"></span></a>

                    </div>
                </div>
            </div>

            <div class="compare-block__content">
                <div class="car-details__spec mt-0">
                    <div class="cardetails-accordion compare-car">
                        @foreach ($groups as $group)
                        <div class="cardetails__block" id="group_{{$group->id}}">
                            <div class="car-details__spec_head">{{$group->group_name}} <span class="acc-arrow"></span>
                            </div>
                            <div class="cardetails">
                                <table class="table table-bordered table-hover">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach

                        <div class="compare-block__footer">
                            <table class="table table-bordered">
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" name="car-1" value="{{count($data) ? $data[0]->id:0}}">
<input type="hidden" name="type" value="{{$type}}">
<input type="hidden" name="total" value="{{($data && $type == 'grades') ? count($data):count($models)}}">
<input type="hidden" name="storage" value="{{storage('')}}">

<div id="grades" class="modal" style="width: 50%;height:inherit">
    <div class="modal-content">
        <h4 class="modal-title text-center">Choose Grade</h4>
        <hr />
        <div class="row mb-0 justify-content-center" id="grades-content">
            @if($data)
            @foreach ($data as $grade)
            <div class="col-sm-4 choose-grade" data-id={{$grade->id}}>
                <div class="exploremore__car">
                    <img src="{{storage($grade->image)}}" alt="">
                </div>
                <div class="exploremore__info">
                    <h4>{{$data[0]->model_name}} <span>{{$grade->name}}</span></h4>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>

<div id="models" class="modal" style="width: 50%;height:inherit">
    <div class="modal-content">
        <h4 class="modal-title text-center">Choose Vehicle</h4>
        <hr style="margin: 1rem; border: 0;" />
        <div class="row mb-0 justify-content-center" id="vehicles">
            @foreach ($models as $model)
            @if($model->category == 1)
            <div class="col-sm-4 choose-model" data-slug={{$model->slug}}>
                <div class="exploremore__car">
                    <img src="{{storage($model->image)}}" alt="">
                </div>
                <div class="exploremore__info">
                    <h4>{{$model->name}}</h4>
                </div>
            </div>
            @endif
            @endforeach
        </div>
        <hr style="margin: 1rem; border: 0;"/>
        <h4 class="modal-title text-center">Compare with Uni Changan</h4>
        <hr style="margin: 1rem; border: 0;"/>
        <div class="row mb-0 justify-content-center" id="vehicles">
            @foreach ($models as $model)
            @if($model->category == 2)
            <div class="col-sm-4 choose-model" data-slug={{$model->slug}}>
                <div class="exploremore__car">
                    <img src="{{storage($model->image)}}" alt="">
                </div>
                <div class="exploremore__info">
                    <h4>{{$model->name}}</h4>
                </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>

@endsection
@push('script')
<script src="{{ user_js('compare.js') }}"></script>
@endpush