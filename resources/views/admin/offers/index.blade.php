@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Offers']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>Offers</span>
            <a href="{{url('/admin/offers/add')}}"><button class="btn btn-primary">Add Offer</button></a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Type</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($offers as $offer)
                <tr>
                    <td>{{$offer->title}}</td>
                    <td>{{$offer->type== 1 ? 'Sales':'After Sales'}}</td>
                    <td><a href="{{url('admin/offers/'.$offer->id)}}"><button class="btn btn-warning action"><i
                                    class="fa fa-edit fa-sm"></i></button></a>
                        <button class="btn btn-danger action delete" data-model="offer" data-id="{{ $offer->id }}"><i
                                class="fa fa-trash fa-sm"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
@endpush