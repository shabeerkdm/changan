@extends('admin.layouts.base')
@section('bread-crumb')
    @include('admin.layouts.bread-crumb', ['links' => ['Home', ($offer ? 'Edit' : 'Add') . ' offer']])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>{{ $offer ? 'Edit' : 'Add' }} offer</span>
                        <button onclick="$('#offer-form').submit()" class="btn btn-primary">{{ $offer ? 'Update' : 'Save' }}
                            offer</button>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url($offer ? 'admin/offers/' . $offer->id : 'admin/offers') }}" id="offer-form"
                        class="row" enctype="multipart/form-data">
                        @if ($offer)
                            @method('patch')
                        @endif
                        <div class="form-group col-8">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" value="{{ $offer->title ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-4">
                            <label for="thumb">Thumbnail (600px X 338px)</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="thumb">
                                <label class="custom-file-label" for="thumb"></label>
                            </div>
                        </div>
                        <div class="form-group col-8">
                            <label for="slug">Slug</label>
                            <input type="text" name="slug" id="slug" value="{{ $offer->slug ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-4">
                            <label for="image">Feature Image (900px X 507px)</label>
                            <div class="custom-file">
                                <input type="file" name="image_1" class="custom-file-input" id="image">
                                <label class="custom-file-label" for="image"></label>
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="type">Type</label>
                            <select name="type" id="type" class="form-control">
                                <option value="1"
                                    @isset($offer->type)@if ($offer->type == 1) selected @endif @endisset>
                                    Sales</option>
                                <option value="2"
                                    @isset($offer->type)@if ($offer->type == 2) selected @endif @endisset>
                                    After Sales</option>
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="model">Model</label>
                            <select name="model_id" id="model" class="form-control" onchange="setGrade(this)">
                                <option value="0" disabled selected>Choose Model</option>
                                @foreach ($models as $model)
                                    <option value="{{ $model->id }}"
                                        @if ($offer) {{ $offer->model_id == $model->id ? 'selected' : '' }} @endif>
                                        {{ $model->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4 @if (count($grades) == 0) d-none @endif">
                            <label for="grade">Grade</label>
                            <select name="grade_id" id="grade" class="form-control">
                                @foreach ($grades as $grade)
                                    <option value="{{ $grade->id }}"
                                        @if ($offer) {{ $offer->grade_id == $grade->id ? 'selected' : '' }} @endif>
                                        {{ $grade->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control editor" cols="30" rows="20">{{ $offer->description ?? '' }}</textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="body">Body</label>
                            <textarea name="body" id="body" class="form-control editor" cols="30" rows="20" required>{{ $offer->body ?? '' }}</textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>Add Images</span>
                    </div>
                </div>
                <form id="add-image" enctype="multipart/form-data">
                    <input type="hidden" name="section_id" value="4">
                    <div class="card-body row">
                        <div class="form-group col">
                            <label for="image">Image</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="image">
                                <label class="custom-file-label" for="image"></label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <button class="btn btn-primary" type="submit" type="button" data-dismiss="modal">Add</button>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>Images</span>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm" id="image-media">
                        <thead>
                            <tr>
                                <th>Preview</th>
                                <th width="150">Copy</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        document.querySelectorAll('.editor').forEach((editorElement) => {
            ClassicEditor.create(editorElement)
                .then((editor) => {
                    editorElement.editorInstance = editor; // Store the editor instance
                })
                .catch((error) => {
                    console.error('Error initializing CKEditor:', error);
                });
        });
    </script>
    <script src="{{ admin_js('media.js') }}"></script>
    <script src="{{ admin_js('offer.js') }}"></script>
@endpush
