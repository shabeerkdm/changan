@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Orders']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>Orders</span>
        </div>
    </div>
    <div class="card-body">

        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Model</th>
                    <th>Payment</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                <tr>
                    <td>{{$order->order_id}}</td>
                    <td>{{$order->customer->fname}} {{$order->customer->lname}}</td>
                    <td>{{$order->customer->email}}</td>
                    <td>{{$order->customer->contact}}</td>
                    <td>{{$order->grade->model->name}} {{$order->grade->name}}</td>
                    <td>{{$order->is_success ? 'Success' : 'Failed'}}</td>
                    <td>
                        <button class="btn btn-info action view" data-toggle="modal" data-target="#orderPopup"
                            data-id="{{ $order->id }}"><i class="fa fa-eye fa-sm"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $orders->links() }}
    </div>
</div>
@endsection
@push('modal')
<div class="modal fade" id="orderPopup" tabindex="-1" role="dialog" aria-labelledby="orderPopupLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">View Order</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form action="" class="row" id="change-status">
                    @method('patch')
                    <input type="hidden" name="order_id" value="">
                    <div class="form-group col-md-10">
                        <label for="status">Status</label>
                        <input type="text" class="form-control" name="status" id="status">
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary" style="margin-top: 27px">Update</button>
                    </div>
                </form>
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
<script src="{{admin_js('order.js')}}"></script>
@endpush