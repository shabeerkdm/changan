@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','SEO']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>SEO</span>
            <button class="btn btn-primary" onclick="$('#seo-form').submit()"
                class="btn btn-primary">{{($seo ? 'Update':'Save')}}</button>
        </div>
    </div>
    <div class="card-body">
        <form action="{{url($seo ? 'admin/seo/'.$seo->id : 'admin/seo')}}" id="seo-form">
            @if ($seo)
            @method('patch')
            @endif
            <div class="row">
                <div class="form-group col-md-5">
                    <label for="page">Page</label>
                    <input type="text" name="page" value="{{$seo->page ?? ''}}" class="form-control" id="page">
                </div>
                <div class="form-group col-md-5">
                    <label for="slug">Slug</label>
                    <input type="text" name="slug" value="{{$seo->slug ?? ''}}" class="form-control" id="slug">
                </div>
                <div class="form-group col-md-2">
                    <label for="slug">Type</label>
                    <select name="type" class="form-control" id="type">
                        <option value="1">Page</option>
                        <option value="2">Vehicle</option>
                        <option value="3">Post</option>
                        <option value="4">Offer</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    <label for="title">Title</label>
                    <input type="text" name="title" value="{{$seo->title ?? ''}}" id="title" class="form-control">
                </div>
                <div class="form-group col-12">
                    <label for="description">Description</label>
                    <input type="text" name="description" value="{{$seo->description ?? ''}}" id="description"
                        class="form-control">
                </div>
                <div class="form-group col-12">
                    <label for="key_words">Key Words</label>
                    <input type="text" name="key_words" value="{{$seo->key_words ?? ''}}" id="key_words"
                        class="form-control">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>SEO</span>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Page</th>
                    <th>Slug</th>
                    <th>Title</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($seos as $s)
                <tr>
                    <td>{{$s->page}}</td>
                    <td>{{$s->slug}}</td>
                    <td>{{$s->title}}</td>
                    <td><a href="{{url('admin/seo/'.$s->id)}}"><button class="btn btn-warning action"><i
                                    class="fa fa-edit fa-sm"></i></button></a>
                        <button class="btn btn-danger action delete" data-model="seo" data-id="{{ $s->id }}"><i
                                class="fa fa-trash fa-sm"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this,'seo')
    })
</script>
<script src="{{admin_js('seo.js')}}"></script>
@endpush