@extends('admin.layouts.base')
@section('bread-crumb')
    @include('admin.layouts.bread-crumb',['links'=>['Home','Settings']])
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row d-flex justify-content-between align-items-center px-3">
                <span>Site Settings</span>
                <button onclick="$('#site_options').submit()" class="btn btn-primary">Update</button>
            </div>
        </div>
        <div class="card-body">
            <form id="site_options">
                <ul class="nav nav-tabs" id="myTab1" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                            aria-controls="home" aria-selected="true">Social Media</a></li>
                    <li class="nav-item"><a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                            aria-controls="contact" aria-selected="false">Contact</a></li>
                    <li class="nav-item"><a class="nav-link" id="sms-tab" data-toggle="tab" href="#sms" role="tab"
                            aria-controls="sms" aria-selected="false">SMS</a></li>
                </ul>
                <div class="tab-content" id="myTab1Content">
                    <div class="tab-pane pt-3 fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card">
                            <div class="card-header">
                                Social Media Links
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="facebook">Facebook</label>
                                        <input type="text" name="facebook" id="facebook"
                                            value="{{ $options['facebook'] ?? '' }}" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="twitter">Twitter</label>
                                        <input type="text" name="twitter" id="twitter"
                                            value="{{ $options['twitter'] ?? '' }}" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="youtube">Youtube</label>
                                        <input type="text" name="youtube" id="youtube"
                                            value="{{ $options['youtube'] ?? '' }}" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="instagram">Instagram</label>
                                        <input type="text" name="instagram" id="instagram"
                                            value="{{ $options['instagram'] ?? '' }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane pt-3 fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="tab-pane pt-3 fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card">
                                <div class="card-header">
                                    Contact form Images
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="left-image">Left Image (694px X 730px)</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_l" class="custom-file-input" id="left-image"
                                                    required>
                                                <label class="custom-file-label" for="left-image"></label>
                                            </div>
                                        </div>
                                        @isset($options['image_r'])
                                            <div class="col-md-2">
                                                <img src="{{ storage($options['image_r']) ?? '' }}" alt="" width="50%"
                                                    height="50%" style="margin-top:25px ">
                                            </div>
                                            @endif
                                            <div class="form-group col-md-4">
                                                <label for="right-image">Right Image (694px X 730px)</label>
                                                <div class="custom-file">
                                                    <input type="file" name="image_r" class="custom-file-input" id="right-image"
                                                        required>
                                                    <label class="custom-file-label" for="right-image"></label>
                                                </div>
                                            </div>
                                            @isset($options['image_l'])
                                                <div class="col-md-2">
                                                    <img src="{{ storage($options['image_l']) ?? '' }}" alt="" width="50%"
                                                        height="50%" style="margin-top:25px ">
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane pt-3 fade" id="sms" role="tabpanel" aria-labelledby="sms-tab">
                                <div class="tab-pane pt-3 fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="card">
                                        <div class="card-header">
                                            SMS format
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <label for="opt-sms">OTP message (use [OTP] as otp field)</label>
                                                    <textarea name="otp_sms" id="opt-sms" class="form-control" cols="30"
                                                        rows="10">{{ $options['otp_sms'] ?? '' }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        @endsection
        @push('script')
            <script src="{{ admin_js('options.js') }}"></script>
        @endpush
