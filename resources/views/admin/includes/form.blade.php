@php
    $rules = '';
@endphp
<form action="{{ $action }}" method="@if ($method == 'get') get @else post @endif" class="row"
    id="master-form" enctype="multipart/form-data">
    @csrf
    @if ($method != 'get' && $method != 'post')
        @method($method)
    @endif
    @foreach ($fields as $i => $field)
        <div class="form-group {{ $field->pivot->class }}">
            @if ($field->name == 'file')
                <label for="field_{{ $i + 1 }}">{{ $field->pivot->label }}</label>
                <div class="custom-file">
                    <input type="file" name="{{ $field->pivot->name }}" class="custom-file-input"
                        id="field_{{ $i + 1 }}">
                    <label class="custom-file-label" for="field_{{ $i + 1 }}"></label>
                </div>
                @php
                    $rules .=
                        $field->pivot->name .
                        ':' .
                        ($field->pivot->validation ? $field->pivot->validation : '{}') .
                        (!$loop->last ? ',' : '');
                @endphp
            @else
                @if ($field->type == 'select')
                    <label class="field_{{ $i + 1 }}" for="">{{ $field->pivot->label }}</label>
                    <select name="{{ $field->pivot->name }}" class="form-control" id="field_{{ $i + 1 }}"
                        data-options="{{ $field->pivot->options }}" data-value="{{ $field->pivot->value }}"></select>
                    @php
                        $rules .=
                            $field->pivot->name .
                            ':' .
                            ($field->pivot->validation ? $field->pivot->validation : '{}') .
                            (!$loop->last ? ',' : '');
                    @endphp
                @elseif($field->type == 'textarea')
                    <textarea name="{{ $field->pivot->name }}" id="field_{{ $i + 1 }}" class="form-control editor" cols="30"
                        rows="10">{{ $field->pivot->value }}</textarea>
                    @php
                        $rules .=
                            $field->pivot->name .
                            ':' .
                            ($field->pivot->validation
                                ? '{required:function () { const editor = document.querySelector("#field_' .
                                    ($i + 1) .
                                    '").editorInstance; return editor ? editor.getData().trim() === "" : true;}}'
                                : '{}') .
                            ($loop->last ? '' : ',');
                    @endphp
                @else
                    <label class="field_{{ $i + 1 }}" for="">{{ $field->pivot->label }}</label>
                    <{{ $field->type }} type="{{ $field->name }}" name="{{ $field->pivot->name }}"
                        value="{{ $field->pivot->value }}" class="form-control" id="field_{{ $i + 1 }}">
                        </{{ $field->type }}>
                        @php
                            $rules .=
                                $field->pivot->name .
                                ':' .
                                ($field->pivot->validation ? $field->pivot->validation : '{}') .
                                (!$loop->last ? ',' : '');
                        @endphp
                @endif
            @endif
        </div>
        @if ($field->name == 'file' && $field->pivot->value)
            <div class="col-md-2">
                <img src="{{ storage($field->pivot->value) }}" alt="" width="50%" height="50%"
                    style="margin-top:25px ">
            </div>
        @endif
    @endforeach
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary">{{ $button }}</button>
    </div>
</form>
@push('script')
    <script>
        document.querySelectorAll('.editor').forEach((editorElement) => {
            ClassicEditor.create(editorElement)
                .then((editor) => {
                    editorElement.editorInstance = editor; // Store the editor instance
                })
                .catch((error) => {
                    console.error('Error initializing CKEditor:', error);
                });
        });

        $(document).ready(function() {
            $('#master-form').validate({
                rules: {
                    {!! $rules !!}
                },
                ignore: " ",
                onfocusout: function(element) {
                    if ($(element).hasClass('ck-editor__editable')) {
                        return false;
                    }
                    this.element(element);
                },
                submitHandler: function(form) {
                    document.querySelectorAll('.editor').forEach((editorElement) => {
                        const editor = editorElement.editorInstance;
                        if (editor) {
                            editor.updateSourceElement();
                        }
                    });

                    var form_data = new FormData($(form)[0]);
                    var url = $(form).attr('action');
                    form_data.append('id', {{ $id }});
                    $.ajax({
                        type: '{{ $method }}',
                        url: url,
                        dataType: "JSON",
                        data: form_data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            if (result.status) {
                                swal(result.message, {
                                    icon: "success",
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 1500);
                            }
                        },
                        error: function(error) {
                            console.error(error);
                        }
                    });
                }
            });

            // Load select options dynamically
            $('select').each(function() {
                var self = $(this);
                var options = self.data('options');
                var value = self.data('value');
                var selected = 0;
                $.ajax({
                    type: options.method,
                    url: url + '/admin/' + options.url + '-list',
                    dataType: "JSON",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                    },
                    success: function(result) {
                        if (result.status) {
                            if (result.data.length > 0) {
                                result.data.forEach((element, i) => {
                                    self.append(new Option(element.name, element.id,
                                        element.id == value, true));
                                    if (element.id == value) {
                                        selected = i;
                                    }
                                    self[0].selectedIndex = selected;
                                });
                            }
                        }
                    },
                    error: function() {}
                });
            });
        });
    </script>
@endpush
