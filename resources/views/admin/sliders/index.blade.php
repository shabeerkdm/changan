@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Sliders']])
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Add New Slider</div>
            <div class="card-body">
                <form action="{{ url('admin/sliders') }}" method="post" id="master-form">
                    @csrf
                    <div class="form-group">
                        <label for="name">Slider Name</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">Add New Slide</div>
            <div class="card-body">
                <form action="{{ url('admin/slide') }}" method="post" id="slide-form" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Slider Name</label>
                        <select name="slider_id" id="" class="form-control">
                            @foreach ($sliders as $slider)
                            <option value="{{ $slider->id }}">{{ $slider->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="slide">Slide Image (1920px X 650px)</label>
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="slide">
                            <label class="custom-file-label" for="slide"></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="m_slide">Mobile Image (1125px X 1319px)</label>
                        <div class="custom-file">
                            <input type="file" name="m_image" class="custom-file-input" id="m_slide">
                            <label class="custom-file-label" for="m_slide"></label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Sliders</div>
            <div class="card-body">
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th width="150">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sliders as $slider)
                        <tr>
                            <td>{{ $slider->name }}</td>
                            <td>
                                <button class="btn btn-info action view" data-toggle="modal" data-target="#slidesPopup"
                                    data-id="{{ $slider->id }}"><i class="fa fa-eye fa-sm"></i></button>
                                <button class="btn btn-warning action edit" data-id="{{ $slider->id }}"><i
                                        class="fa fa-edit fa-sm"></i></button>
                                <button class="btn btn-danger action delete" data-model="slider"
                                    data-id="{{ $slider->id }}"><i class="fa fa-trash fa-sm"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $sliders->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@push('modal')
<div class="modal fade" id="slidesPopup" tabindex="-1" role="dialog" aria-labelledby="slidesPopupLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">View Slides</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-responsive-sm table-bordered table-striped table-sm" id="slides">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th width="150">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content-->
    </div>
    <!-- /.modal-dialog-->
</div>
@endpush
@push('script')
<script src="{{admin_js('slider.js')}}"></script>
@endpush