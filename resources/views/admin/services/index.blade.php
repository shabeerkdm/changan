@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Services']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>Services</span>
            <div class="col-4">
                <form action="" id="search-service">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search by Plate number"
                            aria-label="Search" aria-describedby="basic-addon2" value="{{$search ?? ''}}">
                        <div class="input-group-append">
                            <span class="input-group-text" onclick="$('#search-service').submit()" id="basic-addon2"><i
                                    class="fa fa-search"></i></span>
                        </div>
                        <button type="button" class="btn btn-primary ml-2"
                            onclick="location.replace('{{url('/admin/services')}}')">Clear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Model</th>
                    <th>Plate Number</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($services as $service)
                <tr>
                    <td>{{$service->fname}}</td>
                    <td>{{$service->email}}</td>
                    <td>{{$service->phone}}</td>
                    <td>{{$service->model->name}}</td>
                    <td>{{$service->plate_number}}</td>
                    <td>
                        <button class="btn btn-info action view" data-toggle="modal" data-target="#servicePopup"
                            data-id="{{ $service->id }}"><i class="fa fa-eye fa-sm"></i></button>
                        <button class="btn btn-warning action view" data-toggle="modal" data-target="#serviceStatus"
                            data-id="{{ $service->id }}"><i class="fa fa-plus fa-sm"></i></button>
                    </td>
                <tr>
                    @endforeach
            </tbody>
        </table>
        {{ $services->links() }}
    </div>
</div>
@endsection
@push('modal')
<div class="modal fade" id="servicePopup" tabindex="-1" role="dialog" aria-labelledby="servicePopupLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">View Service</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="serviceStatus" tabindex="-1" role="dialog" aria-labelledby="serviceStatusLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Service Status</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="service_id" value="">
                <form action="" class="row" id="status-form">
                    <input type="hidden" name="index" value="">
                    <div class="form-group col-md-4">
                        <label for="date">Status</label>
                        <input type="date" class="form-control" name="date" id="date">
                    </div>
                    <div class="form-group col-md-7">
                        <label for="status">Status</label>
                        <input type="text" class="form-control" name="status" id="status">
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary" type="button" style="margin-top: 27px"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </form>
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="button" data-dismiss="modal" onclick="update()">Update</button>
            </div>
        </div>
    </div>
</div>
@endpush
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
<script src="{{admin_js('service.js')}}"></script>
@endpush