@extends('admin.layouts.base')
@section('bread-crumb')
    @include('admin.layouts.bread-crumb', ['links' => ['Home', ($post ? 'Edit' : 'Add') . ' news']])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>{{ $post ? 'Edit' : 'Add' }} news</span>
                        <button onclick="$('#post-form').submit()" class="btn btn-primary">{{ $post ? 'Update' : 'Save' }}
                            News</button>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url($post ? 'admin/posts/' . $post->id : 'admin/posts') }}" id="post-form" class="row"
                        enctype="multipart/form-data">
                        @if ($post)
                            @method('patch')
                        @endif
                        <div class="form-group col-12">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" value="{{ $post->title ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-8">
                            <label for="slug">Slug</label>
                            <input type="text" name="slug" id="slug" value="{{ $post->slug ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-4">
                            <label for="thumb">Thumbnail (452px X 277px)</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="thumb">
                                <label class="custom-file-label" for="thumb"></label>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control editor" cols="30" rows="20">{{ $post->description ?? '' }}</textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="body">Body</label>
                            <textarea name="body" id="body" class="form-control editor" cols="30" rows="20">{{ $post->body ?? '' }}</textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>Add Images</span>
                    </div>
                </div>
                <form id="add-image" enctype="multipart/form-data">
                    <input type="hidden" name="section_id" value="3">
                    <div class="card-body row">
                        <div class="form-group col">
                            <label for="image">Image</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="image">
                                <label class="custom-file-label" for="image"></label>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end">
                        <button class="btn btn-primary" type="submit" type="button" data-dismiss="modal">Add</button>
                    </div>
                </form>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>Images</span>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm" id="image-media">
                        <thead>
                            <tr>
                                <th>Preview</th>
                                <th width="150">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        document.querySelectorAll('.editor').forEach((editorElement) => {
            ClassicEditor.create(editorElement)
                .then((editor) => {
                    editorElement.editorInstance = editor; // Store the editor instance
                })
                .catch((error) => {
                    console.error('Error initializing CKEditor:', error);
                });
        });
    </script>
    <script src="{{ admin_js('media.js') }}"></script>
    <script src="{{ admin_js('post.js') }}"></script>
@endpush
