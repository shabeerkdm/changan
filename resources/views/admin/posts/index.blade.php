@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','News']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>News</span>
            <a href="{{url('/admin/posts/add')}}"><button class="btn btn-primary">Add News</button></a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Title</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                    <td>{{$post->title}}</td>
                    <td><a href="{{url('admin/posts/'.$post->id)}}"><button class="btn btn-warning action"><i
                                    class="fa fa-edit fa-sm"></i></button></a>
                        <button class="btn btn-danger action delete" data-model="post" data-id="{{ $post->id }}"><i
                                class="fa fa-trash fa-sm"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
@endpush