<header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
        data-class="c-sidebar-show">
        <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
        </svg>
    </button><a class="c-header-brand d-lg-none" href="{{url('admin')}}">
        <img src="{{asset('public/images/logo.jpg')}}" alt="" width="180px">
    </a>
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
        data-class="c-sidebar-lg-show" responsive="true">
        <svg class="c-icon c-icon-lg">
            <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-menu')}}"></use>
        </svg>
    </button>
    <ul class="c-header-nav d-md-down-none">
        {{-- <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Dashboard</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Users</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Settings</a></li> --}}
    </ul>
    <ul class="c-header-nav ml-auto mr-4">
        {{-- <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                    <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-bell')}}"></use>
        </svg></a></li>
        <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="#">
                <svg class="c-icon">
                    <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-list-rich')}}"></use>
                </svg></a></li>--}}
        @if(Auth::user()->hasRole('ADMIN'))
        <li class="c-header-nav-item d-md-down-none mx-2"><a class="c-header-nav-link" href="{{url('')}}"
                target="_blank">
                <svg class="c-icon">
                    <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-home')}}"></use>
                </svg>&nbsp;| View Site</a></li>
        @endif
        <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#"
                role="button" aria-haspopup="true" aria-expanded="false">
                <div class="c-avatar"><img class="c-avatar-img" src="{{asset('public/assets/img/avatars/6.jpg')}}"
                        alt="user@email.com">
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                <form action="{{url('logout')}}" method="post">
                    @csrf
                    <button class="dropdown-item">
                        <svg class="c-icon mr-2">
                            <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-account-logout')}}"></use>
                        </svg> Logout
                    </button>
                </form>
            </div>
        </li>
    </ul>
    @yield('bread-crumb')
</header>