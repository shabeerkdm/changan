<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
  <div class="c-sidebar-brand d-lg-down-none">
    <a href="{{url('admin')}}">
      <img src="{{asset('public/images/logo.jpg')}}" alt="" width="100%" style="border-right: 1px solid #d8dbe0">
    </a>
  </div>
  <ul class="c-sidebar-nav">
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/dashboard')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-speedometer')}}"></use>
        </svg> Dashboard</a></li>
    @if(Auth::user()->hasRole('ADMIN'))
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/leads')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
        </svg> Leads</a></li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/services')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-weightlifitng')}}"></use>
        </svg> Services</a></li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/orders')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-cart')}}"></use>
        </svg> Orders</a></li>
    <li class="c-sidebar-nav-divider"></li>
    <li class="c-sidebar-nav-title">Base</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/sliders')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-flip')}}"></use>
        </svg> Sliders</a>
    </li>
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle"
        href="#">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-file')}}"></use>
        </svg> Pages</a>
      <ul class="c-sidebar-nav-dropdown-items">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/page/home')}}" target="_top">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-home')}}"></use>
            </svg> Home</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/page/about')}}" target="_top">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-info')}}"></use>
            </svg> About</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/page/warranty')}}"
            target="_top">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-shield-alt')}}"></use>
            </svg> Warranty</a></li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/contact')}}" target="_top">
            <svg class="c-sidebar-nav-icon">
              <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-contact')}}"></use>
            </svg> Contact</a></li>
      </ul>
    </li>
    <li class="c-sidebar-nav-title">Vehicles</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/models')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-car-alt')}}"></use>
        </svg> Models</a></li>
    <li class="c-sidebar-nav-title">Blog</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/posts')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-airplay')}}"></use>
        </svg> News & Media</a></li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/offers')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-gift')}}"></use>
        </svg> Offers</a></li>
    <li class="c-sidebar-nav-title">Options</li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/seo')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-find-in-page')}}"></use>
        </svg> SEO</a>
    </li>
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/settings')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-settings')}}"></use>
        </svg> Settings</a>
    </li>
    @elseif(Auth::user()->hasRole('SERVICE'))
    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{url('admin/services')}}">
        <svg class="c-sidebar-nav-icon">
          <use xlink:href="{{asset('public/vendors/@coreui/icons/svg/free.svg#cil-weightlifitng')}}"></use>
        </svg> Services</a></li>
    @endif
  </ul>
  <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
    data-class="c-sidebar-minimized"></button>
</div>