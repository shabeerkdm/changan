<div class="c-subheader px-3">
    <ol class="breadcrumb border-0 m-0">
        @foreach ($links as $link)
        <li class="breadcrumb-item @if($loop->last) active @endif">{{$link}}</li>
        @endforeach
    </ol>
</div>