<!DOCTYPE html>

<html lang="en">

<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} | @yield('title','Dashboard')</title>
    <link rel="shortcut icon" href="{{asset('public/favicon.ico')}}" type="image/png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('public/assets/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- Main styles for this application-->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="{{asset('public/css/style.css')}}" rel="stylesheet">
    {{-- <link href="{{asset('public/vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet"> --}}
</head>

<body class="c-app">
    @include('admin.layouts.sidebar')
    <div class="c-wrapper c-fixed-components">
        @include('admin.layouts.header')
        <div class="c-body">
            <main class="c-main">
                <div class="container-fluid">
                    <div class="fade-in">
                        @yield('content')
                    </div>
                </div>
            </main>
            @include('admin.layouts.footer')
        </div>
    </div>
    @stack('modal')
    <input type="hidden" name="site_url" value="{{url('/')}}">
    <input type="hidden" name="storage" value="{{storage('')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- CoreUI and necessary plugins-->
    <script src="{{asset('public/vendors/@coreui/coreui/js/coreui.bundle.min.js')}}"></script>
    <!--[if IE]><!-->
    <script src="{{asset('public/vendors/@coreui/icons/js/svgxuse.min.js')}}"></script>
    <!--<![endif]-->
    <!-- Plugins and scripts required by this view-->
    {{-- <script src="{{asset('public/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js')}}"></script> --}}
    <script src="{{asset('public/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    {{-- <script src="{{asset('public/assets/js/main.js')}}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/39.0.1/classic/ckeditor.js"></script>
    <script src="{{ admin_js('script.js') }}"></script>
    @stack('script')
</body>

</html>