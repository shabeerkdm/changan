@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Offers']])
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <span>Location</span>
            </div>
            <div class="card-body">
                <form action="" class="row" id="contact-form">
                    <div class="form-group col-12">
                        <label for="location">Company Name</label>
                        <input type="text" class="form-control" name="company" id="company">
                    </div>
                    <div class="form-group col-12">
                        <label for="location">Location</label>
                        <input type="text" class="form-control" name="location" id="location">
                    </div>
                    <div class="form-group col-12">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" name="description" id="description">
                    </div>
                    <div class="form-group col-12">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" id="address">
                    </div>
                    <div class="form-group col-12">
                        <label for="address">P.O Box</label>
                        <input type="text" class="form-control" name="po_box" id="po_box">
                    </div>
                    <div class="form-group col-12">
                        <label for="phone">Phone</label>
                        <input type="tel" class="form-control" name="phone" id="phone">
                    </div>
                    <div class="form-group col-12">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email">
                    </div>
                    <div class="form-group col-12">
                        <label for="timing_sales">Timing Sales</label>
                        <input type="text" class="form-control" name="timing_sales" id="timing_sales">
                    </div>
                    <div class="form-group col-12">
                        <label for="timing_services">Timing Services</label>
                        <input type="text" class="form-control" name="timing_services" id="timing_services">
                    </div>
                    <div class="form-group col-12">
                        <button type="submit" id="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <span>Locations</span>
            </div>
            <div class="card-body">
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Location</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($contacts as $contact)
                        <tr>
                            <td>{{$contact->location}}</td>
                            <td>{{$contact->phone}}</td>
                            <td>{{$contact->email}}</td>
                            <td>
                                <button class="btn btn-warning action" onclick="editContact({{$contact}})"><i
                                        class="fa fa-edit fa-sm"></i></button></a>
                                <button class="btn btn-danger action delete" data-model="contact"
                                    data-id="{{ $contact->id }}"><i class="fa fa-trash fa-sm"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="{{admin_js('contact.js')}}"></script>
@endpush