@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Models']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center px-3">
            <span>Models</span>
            <a href="{{url('/admin/models/add')}}"><button class="btn btn-primary">Add Model</button></a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Name</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($models as $model)
                <tr>
                    <td>{{$model->name}}</td>
                    <td><a href="{{url('admin/models/'.$model->id)}}"><button class="btn btn-warning action"><i
                                    class="fa fa-edit fa-sm"></i></button></a>
                        <button class="btn btn-danger action delete" data-model="model" data-id="{{ $model->id }}"><i
                                class="fa fa-trash fa-sm"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
@endpush