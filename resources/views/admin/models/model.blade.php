@extends('admin.layouts.base')
@section('bread-crumb')
    @include('admin.layouts.bread-crumb', ['links' => ['Home', 'Models', $model ? 'Edit' : 'Add']])
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row d-flex justify-content-between align-items-center px-3">
                        <span>{{ $model ? 'Edit' : 'Add' }} Model</span>
                        <button onclick="$('#model-form').submit()" class="btn btn-primary">{{ $model ? 'Update' : 'Save' }}
                            Model</button>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url($model ? 'admin/models/' . $model->id : 'admin/models') }}" method="post"
                        id="model-form" class="row" enctype="multipart/form-data">
                        @if ($model)
                            @method('patch')
                        @endif
                        <div class="form-group col-md-4">
                            <label for="name">Model Name</label>
                            <input type="text" name="name" id="name" value="{{ $model->name ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="slider">Slider</label>
                            <select name="slider_id" id="slider" class="form-control">
                                @foreach ($sliders as $slider)
                                    <option value="{{ $slider->id }}"
                                        @if ($model) {{ $model->slider_id == $slider->id ? 'selected' : '' }} @endif>
                                        {{ $slider->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="slug">Slug</label>
                            <input type="text" name="slug" id="slug" value="{{ $model->slug ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="title">Page Title</label>
                            <input type="text" name="title" id="title" value="{{ $model->title ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="slogan">Page Slogan</label>
                            <input type="text" name="slogan" id="slogan" value="{{ $model->slogan ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="engine">Engine</label>
                            <input type="text" name="engine" id="engine" value="{{ $model->engine ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="transmission">Transmission</label>
                            <input type="text" name="transmission" id="transmission"
                                value="{{ $model->transmission ?? '' }}" class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="torque">Maximum Torque</label>
                            <input type="text" name="torque" id="torque" value="{{ $model->torque ?? '' }}"
                                class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="category">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option value="1"
                                    @if ($model) {{ $model->category == 1 ? 'selected' : '' }} @endif>
                                    Changan</option>
                                <option value="2"
                                    @if ($model) {{ $model->category == 2 ? 'selected' : '' }} @endif>
                                    UNI</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="active">Active</label>
                            <input type="checkbox" name="active" id="active" value="{{ $model->active ?? '' }}"
                                {{ $model && $model->active ? 'checked' : '' }}>
                        </div>
                    </form>
                </div>
            </div>
            @if ($model)
                <div class="accordion" id="accordion">
                    <div class="card">
                        <div class="card-header" id="sepcifications" data-toggle="collapse" data-target="#specs"
                            aria-expanded="true" aria-controls="specs">
                            <h5 class="mb-0">
                                <h4>
                                    Specifications
                                </h4>
                            </h5>
                        </div>
                        <div id="specs" class="collapse" aria-labelledby="sepcifications" data-parent="#accordion"
                            style="border-bottom: 1px solid #d8dbe0">
                            <div class="card-body">
                                <form class="row" class="row" id="specs-form" enctype="multipart/form-data">
                                    <input type="hidden" name="index" value="">
                                    <div class="form-group col-md-6">
                                        <label for="spec-title">Title</label>
                                        <input type="text" name="title" id="spec-title" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="spec-type">Type</label>
                                        <select name="type" id="spec-type" class="form-control">
                                            <option value="power">Power and Performance</option>
                                            <option value="exterior">Exterior</option>
                                            <option value="interior">Interior</option>
                                            <option value="safety">Safety</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="spec-image">Image (453px X 381px)</label>
                                        <div class="custom-file">
                                            <input type="file" name="image" class="custom-file-input"
                                                id="spec-image">
                                            <label class="custom-file-label" for="image"></label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="body">Body</label>
                                        <textarea name="body" id="body" class="form-control editor" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <button type="submit" class="btn btn-primary"
                                            style="margin-top: 28px">Add</button>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-responsive-sm table-bordered table-striped table-sm"
                                            id="model-specs">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th width="150">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header" id="attributes" data-toggle="collapse" data-target="#attrs"
                            aria-expanded="true" aria-controls="attrs">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="mb-0">
                                        <h4>
                                            Attributes
                                        </h4>
                                    </h5>
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                        </div>
                        <div id="attrs" class="collapse" aria-labelledby="attributes" data-parent="#accordion">
                            <div class="card-body">
                                <form id="att-form" enctype="multipart/form-data">
                                    <input type="hidden" name="index" value="">
                                    <div class="row">
                                        <div class="form-group col-md-8">
                                            <label for="grade">Grade</label>
                                            <div class="input-group">
                                                <select class="custom-select" name="grade_id" id="grade"
                                                    onchange="setAtts()">
                                                    @if (count($grades) == 0)
                                                        <option value="0" disabled selected>Create a grade</option>
                                                    @else
                                                        @foreach ($grades as $grade)
                                                            <option value="{{ $grade->id }}">{{ $grade->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-success" type="button" data-toggle="modal"
                                                        data-target="#addGrade"><i class="fa fa-plus"></i></button>
                                                </div>
                                                <div class="input-group-append d-none" id="grade-edit">
                                                    <button class="btn btn-warning grade-edit" type="button"
                                                        data-toggle="modal" data-target="#addGrade"><i
                                                            class="fa fa-edit"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if (count($grades) != 0)
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label for="attribute">Attribute</label>
                                                <input type="text" name="attribute" id="attribute"
                                                    class="form-control">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="attribute_group">Attribute Group</label>
                                                <select type="text" name="group" id="attribute_group"
                                                    class="form-control">
                                                    @foreach ($att_groups as $group)
                                                        <option value="{{ $group->id }}">{{ $group->group_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="value">Value</label>
                                                <input type="text" name="value" id="value"
                                                    class="form-control">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <button type="submit" class="btn btn-primary"
                                                    style="margin-top: 28px">Add</button>
                                            </div>
                                        </div>
                                    @endif
                                </form>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-responsive-sm table-bordered table-striped table-sm"
                                            id="model-atts">
                                            <thead>
                                                <tr>
                                                    <th>Attribute</th>
                                                    <th>Value</th>
                                                    <th width="150">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header" id="color-section" data-toggle="collapse" data-target="#colors"
                            aria-expanded="true" aria-controls="colors">
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="mb-0">
                                        <h4>
                                            Colors
                                        </h4>
                                    </h5>
                                </div>
                                <div class="col-6">
                                </div>
                            </div>
                        </div>
                        <div id="colors" class="collapse" aria-labelledby="color-section" data-parent="#accordion">
                            <div class="card-body">
                                <form id="color-form" class="row" enctype="multipart/form-data">
                                    <input type="hidden" name="index" value="">
                                    <div class="form-group col-md-3">
                                        <label for="color-name">Name</label>
                                        <input type="text" name="name" id="color-name" class="form-control">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="color">Color</label>
                                        <input type="color" name="color" id="color" class="form-control">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="c_stock">In Stock</label>
                                        <input type="checkbox" name="c_stock" id="c_stock" class="form-control">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="color-image">Color Image (716px X 230px)</label>
                                        <div class="custom-file">
                                            <input type="file" name="image" class="custom-file-input"
                                                id="color-image">
                                            <label class="custom-file-label" for="image"></label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <button type="submit" class="btn btn-primary"
                                            style="margin-top: 28px">Add</button>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-responsive-sm table-bordered table-striped table-sm"
                                            id="model-colors">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Color</th>
                                                    <th>In Stock</th>
                                                    <th width="150">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <span>Gallery</span>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/models/' . $model->id) }}" method="post" class="row"
                            id="gallery-form">
                            @method('patch')
                            <div class="form-group col-md-10">
                                <label for="vid_bg">Video Background (580px X 353px)</label>
                                <div class="custom-file">
                                    <input type="file" name="video_bg" class="custom-file-input" id="vid_bg">
                                    <label class="custom-file-label" for="vid_bg"></label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                @if ($model->video_bg)
                                    <img src="{{ storage($model->video_bg ?? '') }}" alt="" width="50%"
                                        height="50%" style="margin-top:25px ">
                                @endif
                            </div>
                            <div class="form-group col-md-12">
                                <label for="video">Video URL</label>
                                <input type="text" name="video_1" value="{{ $model->video_1 }}" class="form-control"
                                    id="video">
                            </div>
                            <div class="form-group col-md-10">
                                <label for="gal_bg">Photo gallery background (580px X 353px)</label>
                                <div class="custom-file">
                                    <input type="file" name="gallery_bg" class="custom-file-input" id="gal_bg">
                                    <label class="custom-file-label" for="gal_bg"></label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                @if ($model->video_bg)
                                    <img src="{{ storage($model->video_bg ?? '') }}" alt="" width="50%"
                                        height="50%" style="margin-top:25px ">
                                @endif
                            </div>
                            <div class="form-group col-md-10">
                                <label for="f_image">Featured Image (268px X 96px)</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="f_image">
                                    <label class="custom-file-label" for="image"></label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                @if ($model->video_bg)
                                    <img src="{{ storage($model->video_bg ?? '') }}" alt="" width="50%"
                                        height="50%" style="margin-top:25px ">
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="brochure">Brochure</label>
                                <div class="custom-file">
                                    <input type="file" name="brochure" class="custom-file-input" id="brochure">
                                    <label class="custom-file-label" for="brochure"></label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <span>360 View</span>
                    </div>
                    <div class="card-body">
                        <label for="360_view">360 Code</label>
                        <textarea name="view" class="form-control" id="360_view" cols="30" rows="10">{{ $model->view }}</textarea>
                    </div>
                </div>
            @endif
            <input type="hidden" name="is_edit" value="{{ $model ? $model->id : false }}">
        </div>
        @if ($model)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <div class="row d-flex justify-content-between align-items-center px-3">
                            <span>Add Images to gallery</span>
                        </div>
                    </div>
                    <form id="add-image" enctype="multipart/form-data">
                        <input type="hidden" name="section_id" value="2">
                        <input type="hidden" name="reference_id" value="{{ $model->id }}">
                        <div class="card-body row">
                            <div class="form-group col">
                                <label for="g_image">Image (900px X 600px)</label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" id="g_image">
                                    <label class="custom-file-label" for="image"></label>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button class="btn btn-primary" type="submit" type="button"
                                data-dismiss="modal">Add</button>
                        </div>
                    </form>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="row d-flex justify-content-between align-items-center px-3">
                            <span>Images</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered table-striped table-sm" id="image-media">
                            <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th width="150">Delete</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <!-- Modal -->
    <div class="modal fade" id="addGrade" tabindex="-1" role="dialog" aria-labelledby="addGradeTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addGradeTitle">Add Grade</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('admin/grade') }}" method="POST" id="add-grade-form"
                    enctype="multipart/form-data">
                    <input type="hidden" name="model_id" value="{{ $model->id ?? '' }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="grade-name">Name</label>
                                    <input type="text" name="name" id="grade-name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="grade-image">Image</label>
                                    <div class="custom-file">
                                        <input type="file" name="image" class="custom-file-input" id="grade-image"
                                            required>
                                        <label class="custom-file-label" for="grade-image"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="price">Price</label>
                                <input type="number" name="price" id="price" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="intrest">Loan Intrest</label>
                                <input type="number" name="intrest" id="intrest" class="form-control" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="stock">In Stock</label>
                                <input type="checkbox" name="stock" id="stock" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="features">Key features</label>
                                    <textarea name="features" id="features" class="form-control editor" cols="30" rows="20"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>
        document.querySelectorAll('.editor').forEach((editorElement) => {
            ClassicEditor.create(editorElement)
                .then((editor) => {
                    editorElement.editorInstance = editor; // Store the editor instance
                })
                .catch((error) => {
                    console.error('Error initializing CKEditor:', error);
                });
        });
    </script>
    <script src="{{ admin_js('media.js') }}"></script>
    <script src="{{ admin_js('model.js') }}"></script>
@endpush
