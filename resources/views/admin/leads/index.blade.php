@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Leads']])
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row d-flex justify-content-between align-items-center pl-3">
            <span>Leads</span>
            <div class="col-4">
                <select name="filter" class="form-control" id="filter">
                    <option value="0">All</option>
                    @foreach ($filters as $key=>$value)
                    <option value="{{$key}}" @if($filter == $key) selected @endif>{{$value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive-sm table-bordered table-striped table-sm">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Source</th>
                    <th>Date and Time</th>
                    <th width="150">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($leads as $lead)
                <tr>
                    <td>{{$lead->name}}</td>
                    <td>{{$lead->email}}</td>
                    <td>{{$lead->phone}}</td>
                    <td>{{$lead->source}}</td>
                    <td>{{date('d-m-Y h:m a',strtotime($lead->created_at))}}</td>
                    <td>
                        <button class="btn btn-info action view" data-toggle="modal" data-target="#leadPopup"
                        data-id="{{ $lead->id }}"><i class="fa fa-eye fa-sm"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $leads->links() }}
    </div>
</div>
@endsection
@push('modal')
<div class="modal fade" id="leadPopup" tabindex="-1" role="dialog" aria-labelledby="leadPopupLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">View Lead</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-responsive-sm table-bordered table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endpush
@push('script')
<script>
    $('.delete').on('click', function () {
        deleteFn(this)
    })
</script>
<script src="{{admin_js('lead.js')}}"></script>
@endpush