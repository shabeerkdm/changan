@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Pages']])
@endsection
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{$page->title}}</div>
        <div class="card-body">
            @include('admin.includes.form',['action' => url('/admin/page/'.$page->slug), 'method' => 'post', 'fields' =>
            $page->fields,'button'=>'Update','id'=>$page->id])
        </div>
    </div>
</div>
@endsection