@extends('admin.layouts.base')
@section('bread-crumb')
@include('admin.layouts.bread-crumb',['links'=>['Home','Dashboard']])
@endsection
@section('content')
@if(Auth::user()->hasRole('ADMIN'))
<div class="row">
    @foreach ($counts as $key=>$value)
    <div class="col-sm-6 col-lg-3">
        <div class="card text-white bg-gradient-primary">
            <div class="card-body card-body pb-0 d-flex justify-content-between align-items-start">
                <div>
                    <div class="text-value-lg">{{$value}}</div>
                    <div>{{$key}}</div>
                </div>
            </div>
            <div class="c-chart-wrapper mt-3 mx-3" style="height:70px;">
                <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                        <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                        <div class=""></div>
                    </div>
                </div>
                <canvas class="chart chartjs-render-monitor" id="card-chart1" height="70" style="display: block;"
                    width="206"></canvas>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Leads &amp; Orders</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Recent leads</h4>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    {{-- <th>Email</th> --}}
                                    <th>Phone</th>
                                    <th>Source</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leads as $lead)
                                <tr>
                                    <td>{{$lead->name}}</td>
                                    {{-- <td>{{$lead->email}}</td> --}}
                                    <td>{{$lead->phone}}</td>
                                    <td>{{$lead->source}}</td>
                                    <td>{{date('d-m-Y h:m a',strtotime($lead->created_at))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <h4>Service leads</h4>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                <tr>
                                    <td>{{$service->fname}} {{$service->lname}}</td>
                                    <td>{{$service->email}}</td>
                                    <td>{{$service->phone}}</td>
                                    <td>{{date('d-m-Y h:m a',strtotime($service->created_at))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Recent orders</h4>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Model</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $order)
                                <tr>
                                    <td>{{$order->customer->fname}} {{$order->customer->lname}}</td>
                                    <td>{{$order->customer->email}}</td>
                                    <td>{{$order->customer->contact}}</td>
                                    <td>{{$order->grade->model->name}} {{$order->grade->name}}</td>
                                    <td>{{date('d-m-Y h:m a',strtotime($order->created_at))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Service Leads</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Service leads</h4>
                        <table class="table table-responsive-sm table-hover table-outline mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                <tr>
                                    <td>{{$service->fname}} {{$service->lname}}</td>
                                    <td>{{$service->email}}</td>
                                    <td>{{$service->phone}}</td>
                                    <td>{{date('d-m-Y h:m a',strtotime($service->created_at))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection