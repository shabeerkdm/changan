@extends('layouts.base')
@section('content')
<div class="bg-breadcrumb">
    <h1>Thank You</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Thank you!
                </h1>
            </div>
            <div class="content mt-2">
                <p>Your request has been sent successfully.</p>
                <p>One of our colleagues will get back in touch with you soon!<br />
                    Have a great day!</span></p>
                    <a href="{{url('')}}" class="download__btn">Go to Home Page</a>
            </div>
        </div>
    </div>
</section>
@endsection