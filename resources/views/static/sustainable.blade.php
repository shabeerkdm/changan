@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Sustainable Development</h1>
</div>
<section>
    <img class="img-fluid" src="{{asset('public/images/sustainable_banner.jpg')}}" alt="">
</section>
<section class="py-5">
    <div class="container">
        <div class="creative-section mt-0">
            <div class="row justify-content-center">
                <div class="col-sm-6">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/static/image/continued-banner-bg_0120180524.jpg"
                            alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                New-energy Strategy <br class="hide-sm"> ”Shangri-La Plan”
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>In 2020, complete 3 major new energy special platforms.
                                In 2025, stop selling traditional fuel vehicles and realize complete electrification
                                on
                                all
                                products .
                                “ Four Strategic Actions”：100 Billion Plan、10000 Staff of R& D、Partnership
                                Plan、Amazing
                                Experience.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/static/image/continued-text-bg_0220180524.jpg"
                            alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                New Energy<br class="hide-sm">Technology R&D
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>2 R&D Bases have been established in Chongqing and Beijing.Achievements made in new
                                energy
                                vehicle manufacturing.Changan is capable of manufacturing new energy vehicles and
                                has
                                launched 16 new energy models.In 2017, Changan sold more than 61,000 units of new
                                energy
                                vehicles with a 180.9% year-on-year growth.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/static/image/discover-bg_0920180524.jpg" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Cooperation in <br class="hide-sm"> new energy applications
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>In 2016, launched 16 models of new energy vehicles.For full cooperation in building
                                electric
                                vehicle recharging network, operation and new energy vehicle technology.

                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
