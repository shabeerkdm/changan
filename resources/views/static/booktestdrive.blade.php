@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>BOOK A TEST DRIVE

    </h1>
</div>

<section class="py-5">
    <div class="container">

        <div class="text-center">
            <h1 class="title__semibolod font-size__big">
                Test drives available at home or at changan showrooms
            </h1>
        </div>
        <div class="content mt-2">
            <p class="text-center"></p>
            <div class="btd mt-5">
                <div class="offset-sm-3 col-sm-6">
                    <form action="" id="test-drive-form">
                        <input type="hidden" name="source" value="book-test-drive">
                        <div class="select-car-image">
                            <img id="my_changing_image" src="" />
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input type="hidden" name="model_id" value="{{$model_id}}">
                                <select id="my_select_box">
                                    <option value="" disabled selected>Please select</option>
                                    @foreach ($models as $model)
                                    @if ($model->engine != 'false')
                                    <option value="{{$model->image}}" data-id="{{$model->id}}" data-image=""
                                        @if($model->id == $model_id) selected @endif>{{$model->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <label>Preferred Model</label>
                            </div>

                            <div class="input-field col s6">
                                <select name="type" disabled>
                                    <!--<option value="" disabled selected>Please Select type</option>-->
                                    <!--<option value="Home">Home Test Drive</option>-->
                                    <option value="Showroom" selected>Showroom Test drive</option>
                                </select>
                                <label>Test drive Type</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <select name="contact_id">
                                    <option value="" disabled selected>Nearest Location</option>
                                    @foreach ($locations as $location)
                                    <option value="{{$location->id}}">{{$location->address}}</option>
                                    @endforeach
                                </select>
                                <label>Preferred Location</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <ion-icon class="material-icons prefix" name="calendar-outline"></ion-icon>
                                <input type="text" name="date" placeholder="Select Date" class="datepicker" />
                                <label>Select Date</label>
                            </div>
                            <div class="input-field col s6">
                                <ion-icon class="material-icons prefix" name="time-outline"></ion-icon>
                                <input type="text" name="time" placeholder="Select Time" class="timepicker" />
                                <label>Prefered Time</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="first_name" name="fname" type="text" class="validate">
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" name="lname" type="text" class="validate">
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input id="email" name="email" type="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="phone" name="phone" type="number" class="validate">
                                <label for="phone">Mobile Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <label>
                                    <input type="checkbox" name="is_subscribe" />
                                    <span>I'd like to receive marketing communication from changan</span>
                                </label>
                            </div>
                        </div>
                        <div class="download mt-3 text-center">
                            <a href="javascript:;" onclick="$('#test-drive-form').submit()" class="download__btn">Book a
                                test drive<span class="download__btn_icon icon-send"></span></a>
                        </div>
                    </form>
                    <div class="message"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<input type="hidden" name="storage" value="{{storage('')}}">
@endsection
@push('script')
<script>
    var storage = $('[name="storage"]').val()
    if($('#my_select_box').val())
        $('#my_changing_image').attr('src', storage+$('#my_select_box').val());
    $('#my_select_box').change(function() {
        $('#my_changing_image').attr('src', storage+$('#my_select_box').val());
        $('[name="model_id"]').val($(this).find(':selected').attr('data-id'))
    });
    $('#test-drive-form').on('submit',function(e){
        e.preventDefault()
    })
    $('#test-drive-form').validate({
        rules:{
            model_id:{
                required:true
            },
            type:{
                required:true
            },
            contact_id:{
                required:true
            },
            date:{
                required:true
            },
            time:{
                required:true
            },
            fname:{
                required:true
            },
            lname:{
                required:true
            },
            email:{
                required:true,
                email:true
            },
            phone:{
                required:true
            }
        },
        ignore:"",
        submitHandler:function(form){
            $('.message').html('Please wait...')
            var formData = $(form).serializeArray();
            $.ajax({
                type: 'post',
                url: url + '/contact',
                dataType: "json",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (data) {
                    $('.message').html(`<span>Thank you for getting in touch! <br/>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!<br/>Have a great day!</span>`)
                    setTimeout(() => {
                        location.replace(url+'/thank-you')
                    }, 3000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
</script>
@endpush