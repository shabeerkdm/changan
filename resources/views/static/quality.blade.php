@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Quality</h1>
</div>
<section>
    <img class="img-fluid" src="{{asset('public/images/quality_banner_1.jpg')}}" alt="">
</section>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Making constant efforts to build the world's first class R&D Capablity
                </h1>
            </div>
            <div class="content mt-2">
                
            </div>
        </div>
    </div>
</section>
<section class="py-5 bg__grey">
    <div class="container">
        <div class="row mb-0">
            <div class="col-sm-3 my-2">
                <div class="qlty-block">
                    <div class="qlty-block__icon">
                        <svg class="icon">
                            <use xlink:href="#snowflake" />
                        </svg>
                    </div>
                    <div class="qlty-block__value">
                        -50℃
                    </div>
                    <h5>Extremely Cold Russian Snowfield</h5>
                </div>
            </div>
            <div class="col-sm-3 my-2">
                <div class="qlty-block">
                    <div class="qlty-block__icon">
                        <svg class="icon">
                            <use xlink:href="#temperature" />
                        </svg>
                    </div>
                    <div class="qlty-block__value">
                        50℃
                    </div>
                    <h5>Extremely Hot Middle East Desert</h5>
                </div>
            </div>
            <div class="col-sm-3 my-2">
                <div class="qlty-block">
                    <div class="qlty-block__icon">
                        <svg class="icon">
                            <use xlink:href="#mountain" />
                        </svg>
                    </div>
                    <div class="qlty-block__value">
                        5,200
                    </div>
                    <h5>Meter-high Qianghai-Titbet Plateau</h5>
                </div>
            </div>
            <div class="col-sm-3 my-2">
                <div class="qlty-block">
                    <div class="qlty-block__icon">
                        <svg class="icon">
                            <use xlink:href="#cactus" />
                        </svg>
                    </div>
                    <div class="qlty-block__value">
                        120 mg/cm³
                    </div>
                    <h5>Dusty Turpan desert</h5>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-5">
    <div class="container">

        <div class="creative-section mt-0">
            <div class="row">
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid" src="{{asset('public/images/qlty_1.jpg')}}" alt="">
                    </div>
                    <div class="creative-section__content p-3 bg__grey">
                        <div class="heading__wrap_500">
                            <h2 class="title__semibolod font-size__big">
                                Changan Automobile Product Development System: CA-PDS
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>Core that covers the product’s full life cycle with 14 milestones and 3,500 work
                                documents to
                                accurately guide product development to meet the customer demand.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid" src="{{asset('public/images/qlty_2.jpg')}}" alt="">
                    </div>
                    <div class="creative-section__content p-3 bg__grey">
                        <div class="heading__wrap_500">
                            <h2 class="title__semibolod font-size__big">
                                Changan Automobile’ s Product Test and Verification System: CA-TVS
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>With more than 4 million km in test mileage, each new model is to undergo 4,500 tests
                                in
                                15
                                areas including environment, strength, safety, etc. We are committed to make sure
                                our
                                product can provide safe and enjoyable ride to every customer.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="py-5 bg__grey">
    <div class="container">
        <div class="heading__wrap_800">
            <h1 class="title__semibolod font-size__big">
                National awards
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="news-block about-info-block h-100">
                    <div class="news-block__image news__image">
                        <img class="img-fluid" src="{{asset('public/images/qua-honor-img1.png')}}" alt="">
                    </div>
                    <div class="news__details">
                        <p>State Key Laboratory of Automotive Noise Vibration and Safety Technology.The State Key
                            Laboratory of Vehicle NVH and Safety Technology – The Ministry of Science and Technology
                            of the People's Republic of China (2015)

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="news-block about-info-block h-100">
                    <div class="news-block__image news__image">
                        <img class="img-fluid" src="{{asset('public/images/qua-honor-img2.png')}}" alt="">
                    </div>
                    <div class="news__details">
                        <p>Standardized Good Behavior Enterprise.
                            A Company of Good Conduct on Standardization – Chongqing Municipal Quality and Technical
                            Supervision Bureau (2015)

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="news-block about-info-block h-100">
                    <div class="news-block__image news__image">
                        <img class="img-fluid" src="{{asset('public/images/qua-honor-img3.png')}}" alt="">
                    </div>
                    <div class="news__details">
                        <p>Hybrid Passenger Car National Local Joint Engineering Laboratory.National & Local Joint
                            Engineering Laboratory for Hybrid Passenger Vehicle – The National Development and
                            Reform Commission of the People's Republic of China (2013)

                        </p>
                    </div>
                </div>
            </div>



        </div>
    </div>
</section>
@endsection