@extends('layouts.base')
@include('layouts.seo')
@section('content')
    <div class="bg-breadcrumb">
        <h1>Terms and Conditions</h1>
    </div>
    <section class="py-5">
        <div class="container">
            <div class="privacy">
                <div class="content mt-2">
                    <p>Welcome to Changan.ae, owned and operated by Union Motors LLC. The content on this site is
                        maintained by Changan or its affiliated companies or affiliates or third parties that post on the
                        website, but for whom Changan (“Changan”, “us”, “we” or “our”) assumes no direct responsibility.</p>
                    <h2>1 . Introduction</h2>
                    <p>1.1 The terms and conditions on this page will govern your use of this website, chanagan.ae and by
                        entering this website you need to agree to abide by them. If you do not accept these, kindly leave
                        the
                        website.</p>

                    <p>1.2 This Website is provided to you by Union Motors LLC, a company registered
                        in
                        the United Arab Emirates with its registered office at Dubai al khabaisi, Deira, United Arab
                        Emirates.</p>

                    <p>1.3 We have all the right to change or update the following terms and conditions at any time, without
                        any advanced notice. As a user, it is your own responsibility to review the Website terms each time
                        you
                        enter the website to ensure you are aware of our latest terms and conditions. Your use of this
                        website
                        after a change has been made signifies your acceptance of the revised terms.</p>
                    <p>1.4 United Arab of Emirates is our country of domicile” and stipulate that the governing law is the
                        local law. All disputes arising in connection therewith shall be heard only by a court of competent
                        jurisdiction in U.A.E</p>
                    <p>1.5 “Union Motors LLC” maintains the http://www.changan.ae/ Website ("Changan").</p>
                    <h2>2. Using Our Website</h2>
                    <p>2.1 As a user/visitor you (“you” “your” and “yourself”), can utilize our website for your own
                        personal
                        non-commercial use.</p>

                    <p>2.2 We have all the rights to amend or withdraw the services on this website without any notice.
                        Also,
                        access to the website is allowed on a temporary basis. As a user, you are responsible for making all
                        necessary arrangements for accessing our website. You will be held responsible for all individuals
                        and
                        their activities accessing our website through your internet connection.</p>

                    <p>2.3 You are not allowed to extract the content or any data from our website using any software or an
                        automated system. Except, if you have entered a written agreement permitting such activity with us.
                    </p>
                    <p>2.4 Visa or MasterCard debit and credit cards in AED will be accepted for payment</p>

                    <p>2.5 We will not trade with or provide any services to OFAC and sanctioned countries</p>

                    <p>2.6 Customer using the website who are Minor /under the age of 18 shall not register as a User of the
                        website and shall not transact on or use the website</p>

                    <p>2.7 Cardholder must retain a copy of transaction records and Merchant policies and rules. User is
                        responsible for maintaining the confidentiality of his account</p>
                    <h2>3. Risks and Liabilities</h2>
                    <p>3.1 By entering this website you acknowledge that we do not guarantee our website will:</p>

                    <p>(a) remain the same. We might change, remove, or update or make it subjected to access by paid
                        registration.</p>

                    <p>(b) be compatible with the software or hardware accessories you are using</p>

                    <p>(c) be available all the time</p>

                    <p>(d) be accurate and up to date with the latest information</p>

                    <p>(e) be free of viruses or other malicious components. It is entirely your responsibility to avoid
                        such circumstances.</p>

                    <p>3.2 You also acknowledge that:

                    <p>(a) we cannot guarantee the speed or security of the Website; and</p>

                    <p>(b) we will not be responsible for any direct or indirect damages or losses due to malicious attacks
                        that can be traced back to our website.</p>
                    <h2>4. Products and Prices</h2>
                    <p>4.1 Changan is constantly striving to improve our products and services. All the information provided
                        on
                        this website is for general information purposes only and can be subjected to change at any time.
                    </p>

                    <p>4.2 For updated information or further guidance or advice related to our company, products or
                        services
                        kindly contact your local authorized Changan dealer, or fleet manager, or a fleet customer contact
                        center, before taking any action based on the information available on this website.</p>

                    <p>4.3 We will not be held responsible for any loss or damage resulting from your dependence on any
                        information available on our website to the fullest extent permissible by law.</p>

                    <p>4.4 Kindly note that all the colors shown on the website may not match the actual colors.</p>

                    <p>4.5 The prices for all cars listed on the website are exclusive of VAT and that of commercial
                        vehicles
                        is exclusive of VAT. Prices on the fleet website are clarified by specific disclaimers.</p>
                    <p>4.6 The specifications displayed may vary from actual vehicles in the Changan branches in the UAE, for more accurate details, please check the cars in our branches.</p>
                    <p>4.7 the service contract pricing subject to change anytime without notice. It might take some time to upload the new package pricing on the website, for accurate details, please check the pricing with your service advisor.</p>
                    <h2>5. Requesting A Test Drive</h2>
                    <p>5.1 You may use our website contact information to reach out to a Changan dealer to arrange to test
                        drive.

                    <p>5.2 However we cannot guarantee that all the models you are interested in will be available to test
                        drive at your preferred dealership. So, for specific information on availability contact directly.
                    </p>

                    <p>5.3 Kindly note that you need to follow additional terms and conditions in order to qualify to test
                        drive any Changan model. These terms are available at your preferred Chanagan dealership.</p>

                    <p>5.4 Kindly note that our dealerships reserve all the rights to allow or refuse a test drive to any
                        applicant, without reasons or notice. And we and our dealers will not be responsible for any
                        resulting
                        loss or damage to the fullest extent permissible by law.</p>

                    <p>5.5 kindly note that home test drive option is subject to vehicle availability, for more details,
                        please
                        check your nearest Changan showroom.</p>
                    <h2>6. Intellectual Property</h2>
                    <p>6.1 All the content and information available on our website are protected by certain rights,
                        including all patents, rights to inventions, moral rights, trademarks, business names and domain
                        names, copyrights, goodwill, and the right to sue for passing off or unfair competition, rights in
                        designs and all other intellectual property rights, all registered or unregistered, licensed to our
                        parent company. </p>

                    <p>6.2 You have no right to copy any content or info from this website without our permission. As a
                        user, you can view or print the content for your personal, or non-commercial use.</p>

                    You should not:

                    <p>(a) change, copy or re-publish the content and information on this website</p>

                    <p>(b) remove the copyright or other proprietary notices included in the content</p>

                    <p>(c)use the content on our website to violate rights of us or the third party associated with it</p>

                    <p>(d) exploit, republish, alter, publish, distribute, broadcast, frame, or communicate to the public or
                        third party our website content or information in any way, without our prior written consent by us.
                    </p>

                    <p>6.3 use the materials on our website in a way of invasion of our Rights or the Rights of our
                        Licensors. In case of such events, we reserve all the rights to execute all rights and remedies
                        available in respect of any infringement of rights on the website or the content on it.</p>
                    <h2>7. Delivery information</h2>

                    <p>7.1 After the online booking through changan,ae. The title and registration of the vehicle shall not
                        pass to the buyer without full payment. The seller should receive full payment from the buyer, the
                        buyer is required to pay full price of the vehicle and related services included in the transaction.
                    </p>

                    <p>7.2 Post registration of the vehicle, Union motors LLC will not accept return of the vehicle from the
                        buyer. </p>
                    <h2>8. Payment Confirmation</h2>
                    <p>8.1 Once the payment is processed, the successful completion of the online transaction will be sent to the client via email within 24 hours upon receipt of payment, post which the customer can contact or visit Union Motors LLC to progress further formalities. </p>
                    <p>8.2 Within 24hours the customer will receive a booking acceptance form the Union Motors LLC after
                        confirming the reservation, after the confirmation the customer can visit or contact the Union
                        Motors
                        LLC after the successful completion of the online transaction to progress further formalities.</p>

                    <p>8.3 After online booking and an authorized online payment transaction, the system generates a booking
                        reference number for the receipt payments towards the booking amount. The customer will utilize the
                        booking reference number to communicate with Union Motors LLC as well as for any future interchanges
                        whether as per these terms and conditions or otherwise.</p>

                    <p>8.4 The booking confirmation will be subject to the vehicle availability</p>
                    <h2>9. Refund or Cancellation Policy</h2>

                    <p>9.1 Union Motors LLC shall use all reasonable attempts to secure the selected Changan cars without
                        bias
                        to any other terms and conditions, where the customer does not make contact with the significant
                        Union
                        Motors LLC within 7 days from the successful booking payment date.</p>

                    <p>9.2 Union Motors LLC reserves the authority if needed to cancel any booking at any point of time and
                        Union Motors LLC will, after making the relevant allowances, utilize all reasonable endeavors to
                        refund
                        the remaining booking amount to the customer.</p>

                    <p>9.3 For completing the significant booking and subsequent sale or cancellation and refund of the
                        booking
                        amount, the customer will be needed to give details mentioned by Union Motors LLC during the time of
                        closing. The customer must provide accurate information when providing any details.</p>
                    <p>9.4 Union Motors LLC shall use all reasonable endeavors to process cancellation refunds which will take upto 45 days to complete the transfer.</p>	

                    <p>9.5 Customer can cancel their order within 24 hours; refunds will be made back to the payment solution used initially by the customer. Please allow for up to 45days for the refund transfer to be completed. </p>

                    <p>9.6 Bookings cannot be cancelled, and no refund shall be payable where the Customer has paid the
                        Balance
                        Price.</p>

                    <p>9.7 Refunds will be done only through the original mode of payment.</p>
                    <h2>10. Trademarks</h2>
                    <p>We reserve all rights in and to the ‘changan.ae’ domain name, all trademarks, and logos displayed on
                        this Site are owned or used under license by Changan. These trademarks include product brand names,
                        vehicle model names, slogans, and emblems. The unauthorized use of any trademark displayed on this
                        website is strictly forbidden.</p>
                    <h2>11. Privacy of Users</h2>
                    <p>Changan value the privacy and security of your personal data. Check out our Privacy Policy, for
                        details on how we process your personal data.</p>
                    <h2>12. Our Use Of Cookies</h2>
                    <p>This Website uses cookies. Kindly check out our Cookie Policy, for further details.</p>
                    <h2>13 .Third Party Content</h2>
                    <p>13.1 The content and information on your website may contain links to websites of affiliated and
                        non-affiliated third parties. Changan does not manage or edit these websites.</p>

                    <p>13.2 Changan shall not be liable for any content or information displayed on third party websites.
                        Any access or use of any affiliated or non-affiliated website is subjected to your own Terms of Use
                        and privacy policies.
                    </p>
                    <h2>14. Access From Outside UAE</h2>
                    <p>14.1 All the product descriptions or specifications displayed on this website are for the UAE market
                        only, unless stated otherwise. </p>

                    <p>14.2 All the information and other materials contained on our website may not comply with the laws in
                        countries outside the UAE.</p>
                    <h2>15. Linking To Our Website</h2>
                    <p>15.1 Linking to our page is permitted under the following conditions:</p>
                    <ol type="a">
                        <li>for non-commercial purposes </li>
                        <li>fair and legal way</li>
                        <li>damage our reputation or take advantage of it</li>
                    </ol>
                    <p>15.2 As a user, you should not use a link to our website suggesting any form of association,
                        advertisement, or endorsement, without any written agreement with us.</p>

                    <p>15.3 If you would like to link to our website for commercial purposes or any purpose not included
                        above, please contact us directly.</p>

                    <p>15.4 We own all rights to withdraw linking permission at any point in time and without notice.</p>
                    <h2>16. Enquiries And Complaint</h2>
                    <p>Incase of any queries or complaints regarding the content or information on this website, contact our
                        customer services team.</p>
                    <h2>17. General</h2>
                    <p>17.1 You may not assign, sub-license, or transfer any of your rights under these terms.</p>

                    <p>17.2 In case any provision of these terms is found to be invalid, the invalidity of that provision
                        will not affect the remaining provisions of these terms.</p>

                    <p>17.3 Failure by us or our associates to exercise any right or remedy under these terms does not
                        constitute a rejection of that right or remedy.</p>
                    <h2>18. Governing Law</h2>
                    <p>18.1 By entering our website you acknowledge that UAE law governs your use of the Website and your
                        relationship with Changan. You also agree that the UAE courts will have exclusive jurisdiction over
                        any
                        claim arising from or related to this Website.</p>

                    <p>Additional terms and conditions may be applied by specific Changan dealerships. Please refer to your
                        nearest or preferred Changan dealership for details.</p>
                    <h2>Contact Us</h2>
                    <p>For queries and complaints call us on <strong>800 Changan (2426426).</strong></p>
                </div>
            </div>
        </div>
    </section>
@stop
