@extends('layouts.base')
@include('layouts.seo')
@section('content')
    <div class="bg-breadcrumb">
        <h1>Privacy Policy</h1>
    </div>
    <section class="py-5">
        <div class="container">
            <div class="privacy">
                <div class="content mt-2">
                    <ul class="dotted">
                        <li>The company keeps the security of its user’s data safe and secure and is fully committed to providing maximum privacy rights. All the data are handled with the utmost care through proper legal and fair measures. The company is dedicated to transparency regarding data usage and the kind of data collected. </li>
                        <li>The Website Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore, the Customers’ are encouraged to frequently visit these sections to be updated about the changes on the website. Modifications will be effective on the day they are posted</li>
                        <li>The policy applies to all the scenarios of web portal visits and provides information related
                            to:</li>
                        <h2>Topics covered:</h2>
                        <ul class="dotted">
                            <li>What personal data is collected?</li>
                            <li>How personal data is used</li>
                            <li>Reasons to share personal data </li>
                            <li>Marketing</li>
                            <li>User rights</li>
                            <li>Notice to end-users  </li>
                            <li>Cookies</li>
                            <li>Disclosures</li>
                            <li>External advertising</li>
                            <li>Security of your personal information</li>
                            <li>Policy amendments</li>
                            <li>Updating information</li>
                            <li>How to contact </li>
                            <li>Credit </li>
                        </ul>
                    </ul>
                    <h2>List of Data collected</h2>
                    <h4>The company may collect the following information about users:</h4>
                    <ul class="dotted">
                        <li>Name, age/date of birth and gender</li>
                        <li>Contact details: postal address and billing addresses, telephone numbers (including mobile
                            numbers) and e-mail address</li>
                        <li>Online browsing activities on Company websites</li>
                        <li>Password(s)</li>
                        <li>Debit/Credit card details given by the user to place the order</li>
                        <li>Interests, preferences, feedback and survey responses</li>
                        <li>Location</li>
                        <li>All the documents related to communication with the company</li>
                        <li>Personal data, including sources from the social media platform</li>
                        <li>Our websites are not intended for children and we do not knowingly collect data relating to
                            children.</li>
                        <li>The company web portal is not designed for children and hence the company will never knowingly
                            use the data pertaining to them.</li>
                    </ul>
                    <h2>How we use personal data</h2>
                    <h4>The company uses personal data:</h4>
                    <ul class="dotted">
                        <li>So that users have access to the services available on the website</li>
                        <li>to customize website according to the interests of the users</li>
                        <li>To manage users registered account(s) </li>
                        <li>To verify a user’s identity</li>
                        <li>For website security and prevention of fraud.</li>
                        <li>Send newsletters and other marketing communications which the user has full right to unsubscribe
                            at any time</li>
                        <li>To deliver bills and statements and collect payments </li>
                        <li>For having a better idea of the customer’s interest</li>
                        <li>To effectively carry out customer service interactions </li>
                        <li>To produce it, in case of any legal dispute with the users </li>
                        <li>To send notifications via email especially concerning the user’s request</li>
                        <li>To maintain and check the company’s compliance with the terms and conditions governing the use
                            of the website.</li>
                        <li>To understand the customer concerns and improve the customer experience</li>
                        <li>The company does not disclose any personal information of its users to any third party</li>
                        <li>The website financial transactions are handled through Telr. </li>
                        <li>For Telr privacy policy visit <a href="https://telr.com/legal/privacy-policy" target="_blank">https://telr.com/legal/privacy-policy</a></li>
                        <li>The company shares the information with these payment providers for the purposes of processing
                            payments that take place through the website including refunds and complaints and queries
                            relating to such payments and refunds.</li>
                    </ul>
                    <h4>Newsletter & Marketing</h4>
                    <ul class="dotted">
                        <li>In case the user has subscribed to the company newsletters, they will receive marketing messages
                            through e-mail. The user can unsubscribe to this service anytime through a link that is always
                            available with the e-mail message. </li>
                    </ul>
                    <h4>Cookies</h4>
                    <ul class="dotted">
                        <li>The company web portal uses cookies. Cookies refer to the small files placed by websites on the
                            user’s computer and may be used for websites and to collect data. </li>
                        <li>The user agrees to give consent to the company’s use of cookies in accordance with the terms of
                            this policy.</li>
                    </ul>
                    <h4>Disclosures</h4>
                    <ul class="dotted">
                        <li>
                            The company reserves the right to disclose a user’s personal information, in accordance with the
                            purposes set out in this privacy policy.
                        </li>
                    </ul>
                    <p>The company also reserves the right to disclose personal information:</p>
                    <ul class="dotted">
                        <li>within the limits permitted by law</li>
                        <li>in accordance with an ongoing and/ or prospective legal proceedings;</li>
                        <li>To avail the company’s legal rights to provide data to a third party to prevent fraud and
                            eliminate credit risk</li>
                        <li>Other than the conditions stated in this privacy policy, the company shall not use or share the
                            user data to third parties.</li>
                        </li>
                        <li>All credit/debit cards’ details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties</li>
                        <li><a href="https://www.changan.ae">www.changan.ae</a> will not pass any debit/credit card details to third parties
                        </li>
                    </ul>
                    <h4>External Advertising</h4>
                    <ul class="dotted">
                        <li>The company may use the information from the user’s visit to the website for “Remarketing” or
                            “Retargeting” purposes by the use of Cookies.</li>
                        <li>Therefore, the user may see products or adverts on a third-party website. The user can opt-out
                            of Google advertising cookies at any time by visiting http://www.google.com/settings/ads to stop
                            viewing these adverts</li>
                    </ul>
                    <h4>Security of user’s personal information</h4>
                    <ul class="dotted">
                        <li>The company has the authority to take proper technical and organizational precautions so as to
                            prevent any loss or missus or alteration of the user’s personal information.
                            The personal information of the users is secured through a password and firewall-protected
                            servers.
                            The user acknowledges that a data transfer over the internet is insecure in its nature and
                            therefore the company cannot guarantee the security of data sent over the internet.
                            The user takes the responsibility to keep their login details confidential. The company, under
                            no circumstances, other than while logging in, shall approach its user for their password.
                            Policy amendments</li>
                        <li>www.changan.ae takes appropriate steps to ensure data privacy and security including through various hardware and software methodologies. However, www.changan.ae cannot guarantee the security of any information that is disclosed online</li>
                        <li>The www.changan.ae is not responsible for the privacy policies of websites to which it links. If you provide any information to such third parties different rules regarding the collection and use of your personal information may apply. You should contact these entities directly if you have any questions about their use of the information that they collect.</li>
                    </ul>
                    <h4>Updating information</h4>
                    <ul>
                        <li>In case the user wants to update the information that the company holds, they can contact the
                            company using the contact details mentioned herein -
                        </li>
                    </ul>
                    <h4>How to contact</h4>
                    <p>In case of any queries related to this document pertaining to the terms and conditions contact us via email to: <a href="mailto:info@unionmotors.com">info@unionmotors.com</a>, or Call us <a href="tel:042608080">04-2608080</a></p>
                </div>
            </div>
        </div>
    </section>
@stop
