@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Payment</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Success
                </h1>
            </div>
            <div class="content mt-2">
                <p>Your payment has been success, We have a created an account and sent a mail with the account
                    creadentials to your registered E-mail. Kindly check your inbox</p>
                <p>One of our colleagues will get back in touch with you soon!<br />
                    Have a great day!</span></p>
            </div>
        </div>
    </div>
</section>
@endsection