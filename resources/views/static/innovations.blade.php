@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Innovations</h1>
</div>
<section class="background">
    <div class="spacer__40"></div>
    <div class="container overlay__info">
        <div class="row mb-0">
            <div class="col-sm-3 pr-0">
                <div class="brand-info brand-info__theme">
                    <div class="brand-info__count count">
                        <div class="count__icon"></div>
                        <div class="count__number">
                            8000+
                        </div>
                    </div>
                    <h6 class="mb-0">New customers <br> daily</h6>
                </div>
            </div>
            <div class="col-sm-3 pl-0">
                <div class="brand-info brand-info__alt">
                    <div class="brand-info__count count">
                        <div class="count__icon"></div>
                        <div class="count__number">
                            12,000,000 +
                        </div>
                    </div>
                    <h6 class="mb-0">Vechicles <br> Sold</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="background__paralax background__image_2" data-bottom-top="top: -30%;" data-top-bottom="top: 0%;">
    </div>
</section>


<section class="pt-5 pb-4">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Making constant efforts to build the world's first class R&D Capablity
                </h1>
            </div>
            <div class="content mt-2">
                <p>Each year Changan invests 5% of its annual sales revenue in product R&D and the total investment
                    has amounted to RMB 49.6 billion during the 11th and 12th five-year plan periods. Changan
                    invested 2 billion RMB and built an international standard vehicle test field in Chongqing
                    Dianjiang District.</p>
            </div>
        </div>
    </div>
</section>

<section class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 ">
                <div class="creative-section__image">
                    <img class="img-fluid" src="{{asset('public/images/innovation_block_1.jpg')}}" alt="">
                </div>
                <div class="creative-section__content mb-3 p-3 bg__grey">
                    <div class="heading__wrap_500">

                        <h2 class="title__semibolod font-size__big">
                            Global R&D Network
                        </h2>
                    </div>
                    <div class="content mt-2">
                        <p>A global collaborative R&D pattern with different emphases has been established, covering
                            nine regions of six countries, including Chongqing City, Beijing City, Hebei Province,
                            Hefei City, Turin of Italy, Yokohama of Japan, Birmingham of UK, Detroit of US and
                            Munich of Germany.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="creative-section__image">
                    <img class="img-fluid" src="{{asset('public/images/innovation_block_2.jpg')}}" alt="">
                </div>
                <div class="creative-section__content mb-3 p-3 bg__grey">
                    <div class="heading__wrap_500">

                        <h2 class="title__semibolod font-size__big">
                            Global R&D Centre
                        </h2>
                    </div>
                    <div class="content mt-2">
                        <p> “Open and Shared, Global Collaboration” R&D 4.0 Era
                            Creating an Open and Shared Smart Research and
                            Development Platform for the Whole Society

                        </p>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="creative-section__image">
                    <img class="img-fluid" src="{{asset('public/images/innovation_block_3.jpg')}}" alt="">
                </div>
                <div class="creative-section__content mb-3 p-3 bg__grey">
                    <div class="heading__wrap_500">

                        <h2 class="title__semibolod font-size__big">
                            Investment In R&D
                        </h2>
                    </div>
                    <div class="content mt-2">
                        <p> Each year Changan invests 5% of its annual sales revenue in product R&D and the total
                            investment has amounted to RMB 49.6 billion during the 11th and 12th five-year plan
                            periods. Changan invested 2 billion RMB and built an international standard vehicle test
                            field in Chongqing Dianjiang District.

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="creative-section__image">
                    <img class="img-fluid" src="{{asset('public/images/innovation_block_4.jpg')}}" alt="">
                </div>
                <div class="creative-section__content mb-3 p-3 bg__grey">
                    <div class="heading__wrap_500">

                        <h2 class="title__semibolod font-size__big">
                            Taking the lead in R&D capability
                        </h2>
                    </div>
                    <div class="content mt-2">
                        <p> According to an assessment made by the NDFC (National Development and Reform Commission,
                            the state-acknowledged technical center), Changan scored 93.6, ranking third of the
                            automotive industry and its R&D capability taking the first position of the industry
                            continuously for ten years. Changan has applied for a total of 10,311 patents including
                            2,918 patens of invention (as of 2018.04).

                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-sm-6 ">
                <div class="creative-section__image">
                    <img class="img-fluid" src="{{asset('public/images/innovation_block_5.jpg')}}" alt="">
                </div>
                <div class="creative-section__content mb-3 p-3 bg__grey">
                    <div class="heading__wrap_500">

                        <h2 class="title__semibolod font-size__big">
                            3+N R&D Concept
                        </h2>
                    </div>
                    <div class="content mt-2">
                        <p> Three green technical labels of fashion, intelligence and green plus numerous
                            fundamental technologies, including crash safety, NVH, CAE, testing, body, chassis,
                            electrics, light weight, etc.

                        </p>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
@endsection