@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Intelligence</h1>
</div>
<section>
    <img class="img-fluid" src="{{asset('public/images/intelligence_banner_1.jpg')}}" alt="">
</section>
<section class="py-5">
    <div class="container">
        <div class="heading__wrap_800">

            <h1 class="title__semibolod font-size__big">
                Intelligent R&D
            </h1>
        </div>
        <div class="content mt-2">
            <p>Supported by intelligent driving, intelligent networking and intelligent interaction, Changan <br
                    class="hide-sm">
                Automobile is planning to build intelligent vehicle platform through different stages.</p>
        </div>
    </div>
    <div class="creative-section mt-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/upload/focus/1805/24/5b0691c01482f.jpg" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Intelligent <br class="hide-sm"> Interconnection
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>In China, Changan is the company first to put the intelligent human-vehicle life
                                functionality
                                into mass production which is realized by establishing a cloud-based background
                                interconnection
                                between Changan, JD and IFLYTEK.
                                Combining human, vehicle and home together into one intelligent unit via 3 voice
                                portals
                                (phone,
                                car, smart home) to create an enjoyable and intelligent lifestyle.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/upload/focus/1805/24/5b069e4f891ff.jpg" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Smart <br class="hide-sm"> Interaction
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>Intelligent audio control system can easily operate active start-up, Personalization,
                                audio
                                intervention, and other features via Xiaoan, your audio secretary which has been
                                carried out
                                on several models, such as CS95, CS75 launched in 2017.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid"
                            src="http://www.globalchangan.com/data/upload/focus/1805/25/5b07d6432e909.jpg" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey ">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Transformation and <br class="hide-sm"> Upgrading
                            </h2>
                        </div>
                        <div class="content mt-2 eq__height_1">
                            <p>Customizing products to meet personal needs and quickly meeting diversified demands.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 ">
                    <div class="creative-section__image">
                        <img class="img-fluid" src="{{asset('public/images/rd-3.jpg')}}" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey ">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Highly Automatic Driving
                            </h2>
                        </div>
                        <div class="content mt-2 eq__height_1">
                            <p>Raeton completed a 2,000 km autonomous test drive from Chongqing to Beijing in April
                                2016. It signified that the intelligent driving technology of Changan has completely
                                reached level III of intelligent driving—conditional automatic driving. It is
                                expected
                                to apply on mass production vehicles in 2020.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="creative-section__image">
                        <img class="img-fluid" src="{{asset('public/images/rd-2.jpg')}}" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey eq__height">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Semiautomatic Driving
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>Fully independent development of APA 4.0 (automatic parking system)</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="creative-section__image">
                        <img class="img-fluid" src="{{asset('public/images/rd-1.jpg')}}" alt="">
                    </div>
                    <div class="creative-section__content mb-3 p-3 bg__grey eq__height">
                        <div class="heading__wrap_500">

                            <h2 class="title__semibolod font-size__big">
                                Assisted Driving
                            </h2>
                        </div>
                        <div class="content mt-2">
                            <p>Standardized application of the intelligent driving level I product on full-series
                                models
                                in 2017.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row Ends here !!!! -->
        </div>
    </div>
    </div>
</section>
@endsection