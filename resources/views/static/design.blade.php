@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Design</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Appeal to Your Sense of Style
                </h1>
            </div>
            <div class="content mt-2">
                <p>More than 100 prominent designers from around the world have created more than 10 concept
                    vehicles and 20 mass-production vehicles over the past 10 years. Changan has earned 20 design
                    awards at home and abroad, leading trends in styling.


                </p>
            </div>
        </div>
    </div>
</section>

<section class="pt-0">
    <div class="creative-section mt-0">
        <div class="row">
            <div class="col-sm-6 p-0">
                <div class="creative-section__image">
                    <img class="img-fluid"
                        src="http://www.globalchangan.com/data/upload/about/1805/25/5b07bcb5d70c7.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 creative-section__content p-3 bg__grey">
                <div class="heading__wrap_500">

                    <h2 class="title__semibolod font-size__big">
                        Concept Cars
                    </h2>
                </div>
                <div class="content mt-2">
                    <p>Integrating eastern and western cultures and gathering global design inspirations, Changan
                        concept cars incorporate environmentally friendly design concepts into cutting edge models.
                    </p>
                    <div class="c-button ">
                        <a href="#" class="btn c-button__theme gallery-link">View all models<span
                                class="c-button__theme_arrow anim"></span>
                        </a>
                        <div class="gallery">
                            <a href="http://www.globalchangan.com/data/upload/about/1805/25/5b07bcb5d70c7.jpg"></a>
                            <a href="http://www.globalchangan.com/data/upload/about/1805/25/5b07bc1652d9f.jpg"></a>
                            <a href="http://www.globalchangan.com/data/upload/about/1805/25/5b07be3174492.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection