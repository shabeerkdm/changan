@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Our Locations</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mb-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d194719.88951692864!2d55.1162635!3d25.152685!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f694adc174429%3A0x833d2612739eb0d0!2sCHANGAN%20Service%20Centre!5e1!3m2!1sen!2sin!4v1734448739818!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-sm-4 mb-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6122.916836079011!2d54.491996177029066!3d24.381192378245146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e41172c8469af%3A0x20e9eeae78ec6eca!2sChangan%20Showroom%20%26%20Service%20Centre!5e1!3m2!1sen!2sin!4v1736867106171!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-sm-4 mb-3">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6056.350984293001!2d55.864766577054425!3d25.721112977377462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ef675223eb41927%3A0x47a17966acf0106c!2sCHANGAN%20Showroom!5e1!3m2!1sen!2sin!4v1736867160158!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            @foreach ($contacts as $contact)
            <div class="col-sm-4">
                <div class="our-location">
                    <div class="our-location__info mt-1">
                        <div class="locations__city">
                            <h4 class="mb-1">{{$contact->location}} </h4>
                            <h6 class="semibold">{{$contact->description}}</h6>
                        </div>
                        <div class="locations__info">
                            <div class="locations__info_address">
                                <div>{{$contact->address}}</div>
                            </div>
                            <div class="locations__info_phone">
                                <ul>
                                    <li>{{$contact->phone}}</li>
                                </ul>
                            </div>
                            <div class="locations__info_email">
                                <ul>
                                    <li>{{$contact->email}}</li>
                                </ul>
                            </div>
                            <h6 class="mt-3"><strong>Timings </strong></h6>
                            <div>Sales: {{$contact->timing_sales}}</div>
                            <div>Services: {{$contact->timing_services}}</div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="time-info">
            <!--*All Changan showrooms are closed on Friday.-->
        </div>
        <div class="download mt-3 text-center">
            <a href="{{url('contact')}}" class="download__btn">Contact Us<span
                    class="download__btn_icon icon-send"></span></a>

        </div>
    </div>
    </div>
</section>
@endsection