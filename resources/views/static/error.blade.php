@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Payment</h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Cancelled
                </h1>
            </div>
            <div class="content mt-2">
                <p>Your payment has been failed, Erro code {{$status['code']}} : {{$status['text']}}</p>
            </div>
        </div>
    </div>
</section>
@endsection