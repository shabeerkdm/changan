@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>History</h1>
</div>
<!-- history -->

<section class="pt-5 pb-4">
    <div class="container">
        <div class="heading__wrap_500">
            <p class="subtitle">History</p>
            <h1 class="title__semibolod font-size__big">
                62 Years of Experience In Making Automobiles
            </h1>
        </div>
        <div class="content mt-2 mb-2">
            <p>In 1958, made China’s first jeep. As of 1963, turned out a total of 1,390 “Yangtze 46” jeeps,
                filling in the blank of China's automobile industry.
            </p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <figure>
                    <img class="img-fluid" src="{{asset('public/images/history_0')}}1.jpg" alt="">
                </figure>
            </div>
            <div class="col-sm-6">
                <figure>
                    <img class="img-fluid" src="{{asset('public/images/history_0')}}2.jpg" alt="">
                </figure>
            </div>
        </div>

    </div>

</section>


<section class="pb-5">
    <div class="container">
        <div class="heading__wrap_600 text-center mx-auto mb-4">
            <h1 class="title__semibolod font-size__big">
                158 Years of History & Development
            </h1>
        </div>
    </div>
    <div class="history-slider">
        <div class="h-slider">
            <div class="slider__timeline">
                <div class="container">
                    <div class="timeline">
                        <div class="owl-carousel">
                            <div class="item" data-year="1862">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>1862</h2>
                                        <h3>IN 1862 SHANGHAI</h3>
                                        <div class="text">
                                            <p>Originated from Shanghai Western Gun Bureau in Shanghai, one of the
                                                pioneers of Chinese modern industry.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_1.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="1863">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>1863</h2>
                                        <h3>IN 1863 SUZHOU</h3>
                                        <div class="text">
                                            <p>Renamed as Suzhou Western Gun Bureau.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_3.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="1865">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>1865</h2>
                                        <h3>IN 1865 NANJING</h3>
                                        <div class="text">
                                            <p>Renamed as Jinling Manufacturing Bureau.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_2.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="1937">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>1937</h2>
                                        <h3>IN 1937 CHONGQING</h3>
                                        <div class="text">
                                            <p>Renamed as the 21st Weapon Factory, the largest ordnance manufacturer
                                                in China at that time, provided 60% ordnance for Anti-Japanese War.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_4.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="1984">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>1984</h2>

                                        <div class="text">
                                            <p>Entered auto industry by technological & trade cooperation, and
                                                started to produce mini car.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_5.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="2003">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>2003</h2>

                                        <div class="text">
                                            <p>Italian Design Center was established, Changan’s global R&D footprint
                                                was started.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_6.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>

                            <div class="item" data-year="2009">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>2009</h2>

                                        <div class="text">
                                            <p>Became one of the four major auto enterprise groups in China.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_7.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <div class="item" data-year="21 centure">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>21 centure</h2>

                                        <div class="text">
                                            <p>With global marketing, products have been sold in 50 countries across
                                                the world.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <picture>
                                            <img src="{{asset('public/images/timeline_8.jpg')}}" alt="image">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection