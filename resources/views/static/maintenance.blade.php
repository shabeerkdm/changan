@extends('layouts.base')
@include('layouts.seo')
@section('content')
<div class="bg-breadcrumb">
    <h1>Maintenance</h1>
</div>
<img class="img-fluid" src="{{asset('public/images/maintance_1.jpg')}}" alt="">
<section class="py-5">
    <div class="container">
        <div class="aboutus">
            <div class="heading__wrap_800">
                <h1 class="title__semibolod font-size__big">
                    Genuine service parts
                </h1>
            </div>
            <div class="content mt-2">
                <p>Changan genuine service parts are manufactured according to exacting standards specifically for
                    your vehicle. Changan parts are extensively tested to guarantee your car’s durability, safety
                    and performance.</p>
                <p>Changan genuine parts can only be found at authorized Changan dealers. When your car’s
                    performance and your safety are involved, accept no substitutes. Changan Genuine Service Parts.

                </p>
                <div class="about_features">
                    <ul>
                        <li><span><svg class="icon">
                                    <use xlink:href="#checkmark"></use>
                                </svg></span>Periodic Maintenance must be done every 10,000 km or 6 months.</li>
                        <li><svg class="icon">
                                <use xlink:href="#checkmark"></use>
                            </svg>Brake pads, Wiper blades, Drive belt will be replaces if needed. Cost of parts and
                            labor will be an extra charge.</li>
                        <li><svg class="icon">
                                <use xlink:href="#checkmark"></use>
                            </svg>Prices above are valid from 1 Jan 2020 and it may change without prior notice.
                        </li>


                    </ul>
                </div>
                <div class="download mt-5 text-center">
                    <a href="#" class="download__btn">Book your Services<span
                            class="download__btn_icon icon-send"></span></a>
                    <a href="#" class="download__btn download__btn-outline">Track your
                        service<span class="download__btn_icon icon-track"></span></a>
                </div>
                <div class="service-cost">
                    <div class="mt-5">
                        <h1 class="title__semibolod font-size__big">
                            Service and Maintenance Contract (SMC) Package Price & Details
                        </h1>
                    </div>
                    <img class="img-fluid" src="{{asset('public/images/smc.jpg')}}" alt="">
                </div>
                <div class="service-cost">
                    <div class="mt-5">
                        <h1 class="title__semibolod font-size__big">
                            Service Contract (SC) Package Price & Details
                        </h1>
                    </div>
                    <img class="img-fluid" src="{{asset('public/images/sc.jpg')}}" alt="">
                </div>


            </div>
        </div>
    </div>

</section>
<section class="pb-5 d-none">

    <div class="container mt-5">


        <div class="heading__wrap_800">
            <p class="subtitle">Bibendum est ultricies </p>
            <h1 class="title__semibolod font-size__big">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </h1>
        </div>
        <div class="content mt-2">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore magna aliqua. Pellentesque massa placerat duis ultricies lacus sed turpis
                tincidunt id.</p>
        </div>

    </div>
</section>
@endsection
