<section class="background-cover slick d-block d-sm-none">
    <div class="mainbanner">
        @if(count($slider->slides) > 0)
        @foreach ($slider->slides as $slide)
        <a href="{{url('car-online')}}">
            <div class="slide slide--2" style="background-image: url('{{storage($slide->image)}}')">
                <div class="slide__content d-none">
                    <div class="slide__content_title">
                        <h1>Grab your favourite <br>
                            Changan online
                        </h1>
                    </div>
                    <div class="slide__content_desc">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas explicabo delectus
                            inventore ea
                            <br>
                            impedit deserunt. Maxime, corrupti adipisci dolor nulla perspiciatis pariatur ipsam totam.
                        </p>
                    </div>
                </div>
            </div>
        </a>
        @endforeach
        @endif
    </div>
</section>
