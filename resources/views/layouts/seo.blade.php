@if($seo)
@section('title'){{ $seo->title }}@endsection
@section('description'){{ $seo->description }}@endsection
@section('keywords'){{ $seo->key_words }}@endsection
@endif