<section>
    <div class="no-gutter ">
        <div class="no-gutter__3 object-fit c-dn">
            <img src="{{storage($options['image_l'] ?? '')}}" alt="">
        </div>
        <div class="no-gutter__3 contact bg__grey p-4">
            <div class="primary__title text-center">
                <h2>Book a test drive</h2>
            </div>
            <div class="contact__wrapper">
                <form action="" id="contact-form">
                    <input type="hidden" name="source" value="book-test-drive">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-field">
                                <input id="name" type="text" name="name" class="validate">
                                <label for="name">Full Name</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-field">
                                <input id="email" type="email" name="email" class="validate">
                                <label for="email">Your email</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-field">
                                <input id="phone" type="number" name="phone" class="validate">
                                <label for="phone">Phone number</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-field">
                                <select name="model_id">
                                    <option value="" disabled selected>Please select your car</option>
                                    @foreach ($models as $model)
                                    <option value="{{$model->id}}" @isset($_model)@if($_model == $model->id) selected @endif @endisset>{{$model->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 mt-3">
                            <div class="c-button text-left">
                                <a href="javascript:;" class="btn c-button__theme"
                                    onclick="$('#contact-form').submit()">Send a message<span
                                        class="c-button__theme_arrow anim"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="message"></div>
            </div>
        </div>
        <div class="no-gutter__3 object-fit c-dn">
            <img src="{{storage($options['image_r'] ?? '')}}" alt="">
        </div>
    </div>
</section>
@push('script')
<script>
    $(function(){
        $('#contact-form').on('submit',function(e){
            e.preventDefault()
        })
        $('#contact-form').validate({
            rules:{
                name:{
                    required:true
                },
                email:{
                    required:true,
                    email:true
                },
                phone:{
                    required:true
                },
                model_id:{
                    required:true
                }
            },
            ignore:"",
            submitHandler:function(form){
                $('.message').html('Please wait...')
                var formData = $(form).serializeArray();
                $.ajax({
                    type: 'post',
                    url: url + '/contact',
                    dataType: "json",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function (data) {
                        $('.message').html(`<span>Thank you for getting in touch! <br/>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!<br/>Have a great day!</span>`)
                    setTimeout(() => {
                        location.replace(url+'/thank-you')
                    }, 3000);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                })
            }
        })
    })
</script>
@endpush