<footer id="footer" class="footer">
    <div class="container">
        <div class="social">
            <h4>Changan on the social web:</h4>
            <ul class="social__icons">
                <li><a target="blank" href="{{$options['facebook'] ?? '#'}}"><i
                            class="fab fa-facebook-f"></i></a></li>
                <li><a target="blank" href="{{$options['twitter'] ?? '#'}}"><i class="fab fa-twitter"></i></a></li>
                <li><a target="blank" href="{{$options['youtube'] ?? '#'}}"><i
                            class="fab fa-youtube"></i></a></li>
                <li><a target="blank" href="{{$options['instagram'] ?? '#'}}"><i
                            class="fab fa-instagram"></i></a></li>
        </div>
        <div class="footer__menu">
            <div class="row" id="accordion">
                <div class="col ">
                    <h5 class="mobile__menu">Purchase <span class="mobile__menu_btn"></span></h5>
                    <ul>
                        {{-- <li><a href="{{url('car-online')}}">Buy your car online</a></li> --}}
                        <li><a href="{{url('compare')}}">Compare Changan Models</a></li>
                        <li><a href="{{url('book-test-drive')}}">Book a test drive</a></li>
                        <li><a href="{{url('enquire')}}">Request a quote</a></li>
                        <li><a href="{{url('offers')}}"> Offers</a></li>
                    </ul>
                </div>
                <div class="col ">
                    <h5 class="mobile__menu">About Us <span class="mobile__menu_btn"></span></h5>
                    <ul>
                        <li><a href="{{url('about')}}">About Changan</a></li>
                        <li><a href="{{url('history')}}"> History</a></li>
                        <li><a href="{{url('news')}}"> News & media</a></li>

                    </ul>
                </div>
                <div class="col ">
                    <h5 class="mobile__menu">Why Changan <span class="mobile__menu_btn"></span></h5>
                    <ul>
                        <li><a href="{{url('innovations')}}"> Innovation</a></li>
                        <li><a href="{{url('intelligence')}}"> Intelligence</a></li>
                        <li><a href="{{url('quality')}}"> Quality</a></li>
                        <li><a href="{{url('design')}}"> Design</a></li>
                        <li><a href="{{url('sustainable')}}"> Sustainable Development</a></li>
                    </ul>
                </div>
                <div class="col ">
                    <h5 class="mobile__menu">After sales <span class="mobile__menu_btn"></span></h5>
                    <ul>
                        <li><a href="{{url('maintenance')}}"> Maintenance </a></li>
                        <!--<li><a href="{{url('service-booking')}}"> Service Booking </a></li>-->
                        <li><a href="{{url('tracking')}}"> Service Tracking </a></li>
                        <li><a href="{{url('warranty')}}"> Warranty </a></li>
                    </ul>
                </div>
                <div class="col ">
                    <h5 class="mobile__menu">More Links <span class="mobile__menu_btn"></span></h5>
                    <ul>
                        <li><a href="{{url('car-online')}}"> Vehicles</a></li>
                        <li><a href="{{url('contact')}}"> Contact Us</a></li>
                        <li><a href="{{url('privacy')}}"> Privcy Policy </a></li>
                        <li><a href="{{url('terms')}}"> Terms & Conditions </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer__copyright">
            <div class="footer__copyright_info">
                &copy <?php echo date("Y"); ?> Changan UAE. All Rights Reserved.
            </div>
            <div class="footer__copyright_payment d-flex align-items-center">
                <span class="mr-1">we accept</span> <span><img src="{{asset('public/images/payment-ico.png')}}" alt=""></span>
            </div>
        </div>
    </div>
    </div>
</footer>