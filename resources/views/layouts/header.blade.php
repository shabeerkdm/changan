<header id="header" class="c-header">
    <div id="logoNav" class="container-fluid px-3">
        <div class="c-header__navbar">
            <div class="c-header__navbar-brand-wrapper">
                <!-- Mega menu Starts -->
                <div class="megamenu">

                    <nav class="navbar navbar-expand-lg navbar">
                        <a class="navbar-brand c-header__navbar-brand" href="{{url('/')}}" aria-label="Space">
                            <img class="c-header__navbar-brand-default" src="{{asset('public/images/logo_05.png')}}" alt="Logo">
                        </a>
                        <button class="mobile-menu navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            @include('layouts.menu')
                        </div>
                    </nav>

                </div>
                <!-- Mega menu Ends -->
                <div class="c-header__navbar-findlocation findlocation">
                    <div class="findlocation__wrapper">
                        {{-- @auth
                        @if(Auth::user()->hasRole('USER'))
                        <a href="{{url('account')}}" class="anim mr-2">
                            <div class="findlocation__wrapper_text">My Account</div>
                        </a>
                        @else
                        <a href="{{url('login')}}" class="anim mr-2">
                            <div class="findlocation__wrapper_text">Login</div>
                        </a>
                        @endif
                        @else
                        <a href="{{url('login')}}" class="anim mr-2">
                            <div class="findlocation__wrapper_text">Login</div>
                        </a>
                        @endauth --}}
                        <!-- <div class="findlocation__wrapper_icon">
                            <svg width="18" height="18" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round">
                                <use xlink:href="{{asset('public/images/feather-sprite.svg#map-pin')}}" />
                            </svg>
                        </div> -->


                        {{-- <a href="#">
                            <div class="findlocation__wrapper_icon bg__grey">
                                <svg class="dark" width="18" height="18" fill="none" stroke="#121212" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <use xlink:href="{{asset('public/images/feather-sprite.svg#shopping-cart')}}" />
                                </svg>
                            </div>
                        </a> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


<!-- Floating Social Media bar Starts -->
<div class="float-sm">
    <div class="fl-fl float-position__1">
        <i class="fi flaticon-discount"></i>
        <a href="{{url('enquire')}}">
            Request a quote
        </a>
    </div>
    <div class="fl-fl float-position__2">
        <i class="fi flaticon-steering-wheel"></i>
        <a href="{{url('book-test-drive')}}"> Book a test drive</a>
    </div>
    {{-- <div class="fl-fl float-position__3">
        <i class="fi flaticon-booking"></i>
        <a href="{{url('car-online')}}"> Reserve Online
        </a>
    </div>
    <div class="fl-fl float-position__4">
        <i class="fi flaticon-wrench"></i>
        <a href="{{url('service-booking')}}"> Book a service
        </a>
    </div> --}}
    <div class="fl-fl float-position__3">
        <i class="fi flaticon-pin"></i>
        <a href="{{url('findlocation')}}"> FIND location
        </a>
    </div>
</div>
<!-- Floating Social Media bar Ends -->