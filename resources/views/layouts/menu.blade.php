<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link active" href="{{url('/')}}">Home</a>
    </li>
    <li class="nav-item dropdown has-mega-menu" style="position:static;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">VEHICLE</a>
        <div class="dropdown-menu">
            <div class="row mb-0">
                @foreach ($models as $model)
                    @if($model->category == 1)
                    <div class="col-md-3">
                        <a href="{{url('/vehicle/'.$model->slug)}}" tabindex="0">
                            <div class="cars__block_img">
                                <img src="{{storage($model->image)}}" alt="">
                            </div>
                            <div class="cars__block_text">
                                {{$model->name}}
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </li>
    <li class="nav-item dropdown has-mega-menu" style="position:static;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false"> <img src="{{asset('public/images/uni/uni-logo.png')}}" width="50px"></a>
        <div class="dropdown-menu">
            <div class="row mb-0">
                @foreach ($models as $model)
                    @if($model->category == 2)
                    <div class="col-md-3">
                        <a href="{{url('/vehicle/'.$model->slug)}}" tabindex="0">
                            <div class="cars__block_img">
                                <img src="{{storage($model->image)}}" alt="">
                            </div>
                            <div class="cars__block_text">
                                {{$model->name}}
                            </div>
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </li>
    <li class="nav-item dropdown " style="position:relative;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">PURCHASE</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            {{-- <a class="dropdown-item" href="{{url('/car-online')}}">Buy your car online</a> --}}
            <a class="dropdown-item" href="{{url('/book-test-drive')}}">Book a test drive</a>
            <a class="dropdown-item" href="{{url('/enquire')}}">Request a quote</a>
        </div>
    </li>
    <li>
        <a class="nav-link" href="{{url('/compare')}}">Compare</a>
    </li>

    <li class="nav-item dropdown " style="position:relative;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">About Us</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{url('/about')}}">About Changan</a>
            <a class="dropdown-item" href="{{url('/history')}}">History</a>
            <a class="dropdown-item" href="{{url('/news')}}">News & Media</a>
        </div>
    </li>
    {{-- <li class="nav-item">
        <a class="nav-link" href="{{url('/offers')}}">Offers</a>
    </li> --}}
    <li class="nav-item dropdown " style="position:relative;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">Why Changan</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{url('/innovations')}}">Innovation</a>
            <a class="dropdown-item" href="{{url('/intelligence')}}">Intelligence</a>
            <a class="dropdown-item" href="{{url('/quality')}}">Quality</a>
            <a class="dropdown-item" href="{{url('/design')}}">Design</a>
            <a class="dropdown-item" href="{{url('/sustainable')}}">Sustainable Development</a>
        </div>
    </li>

    <li class="nav-item dropdown " style="position:relative;">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">After sales</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            {{-- <a class="dropdown-item" href="{{url('/maintenance')}}">Maintenance</a> --}}
            <!--<a class="dropdown-item" href="{{url('/service-booking')}}">Service Booking</a>-->
            <a class="dropdown-item" href="{{url('/tracking')}}">Service Tracking</a>
            <a class="dropdown-item" href="{{url('/warranty')}}">Warranty</a>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{url('/contact')}}">Contact us</a>
    </li>

</ul>