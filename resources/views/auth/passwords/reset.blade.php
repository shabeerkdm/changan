@extends('layouts.base')
@push('style')
<style>
    label.error {
        position: relative;
        margin-top: 5px
    }

    .download__btn:focus {
        background: #333
    }
</style>
@endpush
@section('content')
<div class="bg-breadcrumb">
    <h1>RESET PASSWORD </h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="text-center">
            <h2>Sign in to changan my account</h2>
            <h5>Enter your email new passwords</h5>
        </div>
        <div class="login">
            <div class="row">
                <form class="col s12" action="" id="reset-form">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group">
                        <label for="email" class="">{{ __('E-Mail Address') }}</label>


                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                    <div class="form-group">
                        <label for="password" class="">{{ __('Password') }}</label>


                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror

                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirm"
                            required autocomplete="new-password">
                    </div>

                    <div class="form-group mb-0">
                        <div class="col-md-6 offset-md-3">
                            <button type="submit" class="download__btn btn-block justify-content-center">
                                {{ __('Reset Password') }}
                            </button>
                        </div>
                    </div>
                </form>
                <div id="message"></div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <input type="hidden" name="url" value="{{url('')}}">
    @endsection
    @push('script')
    <script>
        $(document).ready(function () {
        var url = $('[name="url"]').val();
        $('#reset-form').submit(function(e){
            e.preventDefault()
        })
        $('#reset-form').validate({
        rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required:true,
                    minlength: 5
                },
                password_confirm: {
                    required:true,
                    minlength: 5,
                    equalTo: "#password"
                },
                token:{
                    required:true
                }
        },

        ignore: ' ',
    submitHandler: function (form) {
        var form_data = new FormData($('#reset-form')[0]);
        $.ajax({
            type: "POST",
            url: url + "/password/reset",
            dataType: 'JSON',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if(result.status){
                    $('#message').text('Your password has been reset successfully...')
                    setTimeout(() => {
                        location.replace(url+'/login') 
                    }, 3000);
                }
            },
            error: function (response) {
                console.log(response);
            }
        })

    }
});
    })
    </script>
    @endpush