@extends('layouts.base')
@push('style')
<style>
    label.error{
        position: relative;
        margin-top: 5px
    }
    .download__btn:focus{
        background: #333
    }
</style>
@endpush
@section('content')
<div class="bg-breadcrumb">
    <h1>Login </h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="text-center">
            <h2>Sign in to changan my account</h2>
            <h5>Enter your email to sent reselt link:</h5>
        </div>
        <div class="login">
            <div class="row">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <form class="col s12" id="reset-form">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                            <label for="email" class="">{{ __('E-Mail Address') }}</label>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-3">
                            <button type="submit" class="download__btn btn-block justify-content-center">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="message"></div>
        </div>
    </div>
</section>
<input type="hidden" name="url" value="{{url('')}}">
@endsection
@push('script')
<script>
    $(document).ready(function () {
        var url = $('[name="url"]').val();
        $('#reset-form').submit(function(e){
            e.preventDefault()
        })
        $('#reset-form').validate({
            rules: {
                email: {
                    required: true,
                    email:true
                }
            },
        ignore: ' ',
        submitHandler: function (form) {
        $('#message').text('Please wait...');
        var form_data = new FormData($('#reset-form')[0]);
        $.ajax({
            type: "POST",
            url: url + "/password/email",
            dataType: 'JSON',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if(result.status){
                    $('#message').text('Your password reset link has been sent successfully..., Kindly check your inbox')
                }else{

                }
            },
            error: function (response) {
                console.log(response);
            }
        })
    }
});
    })
</script>
@endpush