@extends('layouts.base')

@section('content')
<div class="bg-breadcrumb">
    <h1>My account Details</h1>
</div>
<section class="py-5 my-account bg__grey">
    <div class="container ">
        <h2>Welcome, <span>{{$customer->fname}} {{$customer->lname}}</span></h2>
        <div><a href="javascript:;" onclick="$('#logout').submit()">Sign out</a></div>
        <form action="{{url('logout')}}" method="post" id="logout" class="d-none">
            @csrf
        </form>
        <div class="my-account__tabs mt-3">
            <div class="row">
                <div class="col-sm-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home"
                            role="tab" aria-controls="v-pills-home" aria-selected="true">Orders</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">Account Details</a>
                        {{-- <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
                            role="tab" aria-controls="v-pills-settings" aria-selected="false">Address Book</a> --}}
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                            role="tab" aria-controls="v-pills-messages" aria-selected="false">Update Password</a>

                    </div>
                </div>
                <div class="col-sm-9 pl-3">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                            aria-labelledby="v-pills-home-tab">

                            <h3>Orders</h3>
                            <h6>See how your orders are managed and check the latest status on your order</h6>
                            <div class="order-status">
                                <table class="responsive-table highlight">
                                    <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Order Number</th>
                                            <th>Order Status</th>
                                            <th>Order Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($customer->orders) !=0)
                                        @foreach ($customer->orders as $order)
                                        @if($order->is_success)
                                        <tr>
                                            <td>{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                            <td>{{$order->order_id}}</td>
                                            <td>{{$order->status ?? 'Processing'}}</td>
                                            <td>1000 AED</td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="4">No Items to display.</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">

                            <h3>Account Details</h3>
                            <h6>To change your name , click in the appropriate fields.</h6>
                            <div class="form-block">
                                <form action="" id="account-form">
                                    <input type="hidden" name="customer_id" value="{{$customer->id}}">
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input value="{{$customer->fname}}" id="first_name" type="text" name="fname"
                                                class="validate">
                                            <label for="first_name" class="active">First Name</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="last_name" type="text" value="{{$customer->lname}}" name="lname"
                                                class="validate">
                                            <label for="last_name" class="active">Last Name</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input value="{{$customer->email}}" id="email" type="text" name="email"
                                                class="validate">
                                            <label for="email" class="active">Email Address</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="phone" type="number" name="phone" value="{{$customer->contact}}"
                                                class="validate">
                                            <label for="phone" class="active">Phone Number</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <p>
                                            <label>
                                                <input type="checkbox" checked="checked" />
                                                <span>I want to get email updates.</span>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="row">
                                        <div class="download text-left">
                                            <a href="javascript:;" onclick="$('#account-form').submit()"
                                                class="download__btn mr-1">Save</a>
                                            {{-- <a href="" class="download__btn download__btn-outline ">Cancel</a> --}}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                            aria-labelledby="v-pills-messages-tab">
                            <h3>Update Password</h3>
                            <h6>Use the form below to change the password for your account.</h6>
                            <div class="form-block">
                                <form action="" id="password-form">
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input id="password" type="password" name="password" class="validate">
                                            <label for="password" class="active">Enter new password</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input id="c-password" type="password" name="password_confirm"
                                                class="validate">
                                            <label for="c-password" class="active">Confirm Password</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="download text-left">
                                            <a href="javascript:;" onclick="$('#password-form').submit()"
                                                class="download__btn mr-1">Save</a>
                                            {{-- <a href="" class="download__btn download__btn-outline ">Cancel</a> --}}
                                        </div>
                                    </div>
                                </form>
                                <div id="message"></div>
                            </div>
                        </div>
                        {{-- <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                            aria-labelledby="v-pills-settings-tab">
                            <h3>Addresses</h3>
                            <h6>Add, remove and select preferred addresses</h6>

                            <div class="order-status address-table">
                                <table class="responsive-table highlight">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Phone Number</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Jibin Jacob</td>
                                            <td class="address-info">Al shaibani building, 701, 54 - Amman St <br>
                                                Al Nahda 2 - Al Nahda,
                                                Dubai, United Arab Emirates</td>
                                            <td class="status">+971504163041 <span> <img src="images/verified.svg"
                                                        alt="">
                                                </span>
                                                <!--  images/unverified.svg -->
                                            </td>
                                            <td class="update">
                                                <a href="">Delete</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
@push('script')
<script src="{{user_js('account.js')}}"></script>
@endpush