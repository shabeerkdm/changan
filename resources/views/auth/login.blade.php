@extends('layouts.base')

@section('content')
<div class="bg-breadcrumb">
    <h1>Login </h1>
</div>
<section class="py-5">
    <div class="container">
        <div class="text-center">
            <h2>Sign in to changan my account</h2>
            <h5>Enter your details to login to your account:</h5>
        </div>
        <div class="login">
            <div class="row">
                <form class="col s12" id="login-form">
                    @csrf
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email" type="email" name="email" class="validate" required>
                            <label for="email">Email</label>

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="password" type="password" name="password" class="validate" required>
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <a href="{{url('password/reset')}}">Forget Password ?</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button type="submit" class="download__btn btn-block justify-content-center">Sign
                                In</button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="message"></div>
        </div>
    </div>
</section>
@stop

@push('script')
<script src="{{ user_js('login.js') }}"></script>
@endpush