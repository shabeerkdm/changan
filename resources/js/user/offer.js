var storage = $('[name="storage"]').val()
$('.open-popup-link').magnificPopup({
    type: 'inline',
    midClick: true,
    mainClass: 'mfp-fade'
});
$(document).ready(function () {
    $('.offer-popup').on('click', function () {
        getOffer($(this).attr('data-id'), function (data) {
            $('#offer-popup').find('.offer-card').html(`
            <div class="offer-card__image">
            <img class="img-fluid" src="${storage + data.image_1}" alt="">
        </div>
        <div class="offer-card__content">
            <div class="offer-card__content--title">
                ${data.body}
            </div>
        </div>
        <div class="download mt-3 text-left">
            <a href="${url + '/vehicle/' + data.m_slug}" class="download__btn mr-1">Discover ${data.m_name}<span
                    class="download__btn_icon icon-arrow__right"></span></a>
            <a href="#" class="download__btn download__btn-outline mr-1">DOWNLOAD BROCHURE<span
                    class="download__btn_icon icon-dwn-dark"></span></a>

        </div>
            `)

        })
    })
    $('#model-filter').on('change', function () {
        var id = $(this).val()
        $('#filter').removeClass('d-none')
        $('.off-car-name').text($('#model-filter option:selected').text())
        if (id != 0) {
            $('#vehicles').find('.vehicle').show()
            $('#vehicles').find('.vehicle').filter(function () {
                return $(this).attr('data-model') != 'model_' + id
            }).hide()
        } else {
            $('#filter').addClass('d-none')
            // $('.off-car-name').text($('#model-filter option:selected').text())
            $('#vehicles').find('.vehicle').show()
        }
    })
})

function getOffer(id, cb) {
    $.ajax({
        type: 'post',
        url: url + '/offer',
        dataType: "json",
        data: { id: id },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            cb(data)
        },
        error: function (error) {
            console.log(error);
        }
    })
}