var url = $('[name="site_url"]').val()
$('#login-form').on('submit', function (e) {
    e.preventDefault()
    var formData = $(this).serializeArray()
    $.ajax({
        url: url + '/login',
        type: "POST",
        data: formData,
        success: function (res) {
            if (res.status) {
                $('#login').removeClass('btn-primary').addClass('btn-success')
                location.replace(url + '/' + res.url)
            } else {
                $('#message').html(`<div class="text-danger"><strong>Error!</strong> ${res.message}.</div>`)
            }
        },
        error: function () { }

    })
})