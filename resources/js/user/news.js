var url = $('[name="url"]').val()
var storage = $('[name="storage"]').val()
$(document).ready(function () {
    var limit = 0
    $('#load-more').on('click', function () {
        limit += 3;
        $.ajax({
            type: 'post',
            url: url + '/news',
            dataType: "json",
            data: { limit: limit },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (data) {
                data.forEach(element => {
                    $('#posts').append(`
                    <div class="col-sm-4">
                    <div class="news-block">
                        <a href="${url +'/news/' +element.slug}">
                            <div class= "news-block__image news__image" >
                                <img class="img-fluid" src="${storage + element.image}" alt="">
                            </div>
                            <div class="news__title">
                                <h5>${element.title}</h5>
                            </div>
                            <div class="news__details">
                                ${element.description}
                            </div>
                        </a>
                    </div>
                </div >
            `)
            });
                if (data.length == 0 || data.length < 3)
                    $('#load-more').hide()
            },
            error: function (error) {
                console.log(error);
            }
        })
    })
})