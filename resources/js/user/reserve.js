$(document).ready(function () {
    $('#reserve-form, #payment-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#reserve-form').validate({
        rules: {
            fname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            number: {
                required: true
            },
            otp: {
                required: true
            },
            address_1: {
                required: true
            },
            address_2: {
                required: true
            },
            password: {
                minlength: 5
            },
            password_confirm: {
                minlength: 5,
                equalTo: "#password"
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var formData = $(form).serializeArray()
            checkOtp(function () {
                $.ajax({
                    type: 'post',
                    url: url + '/order',
                    dataType: "json",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function (data) {
                        if (data.status) {
                            location.replace(data.data)
                        } else {
                            console.log(data.error);
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                })
            })
        }
    })
})

function checkOtp(cb) {
    var phone = $('#number').val()
    var otp = $('#otp').val()
    $.ajax({
        type: 'post',
        url: url + '/verify-otp',
        dataType: "json",
        data: { phone: phone, otp: otp },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (result) {
            if (result.status) {
                $('#message').text('Please wait...Redirecting to payment page')
                cb()
            } else {
                $('#message').text(result.message)
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function sendOtp() {
    var phone = $('#number').val()
    if (phone) {
        $('.helper-text').text('Please wait...')
        // phone = parseInt(phone)
        $.ajax({
            type: 'post',
            url: url + '/send-otp',
            dataType: "json",
            data: { phone: phone },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                if (result.status) {
                    $('.button__arrow').addClass('d-none')
                    var limit = 60
                    var intervel = setInterval(() => {
                        $('.helper-text').text(result.message + ', retry after ' + limit + ' seconds')
                        limit -= 1
                        if (limit == 0) {
                            clearInterval(intervel)
                            $('.button__arrow').removeClass('d-none')
                            $('.helper-text').text('Try again, if didn\'t recieved OTP')
                        }
                    }, 1000);
                } else {
                    console.log(result);
                }
            },
            error: function (error) {
                console.log(error);
            }
        })
    }else{
        $('.helper-text').text('Please enter a valid mobile number...')
    }
}