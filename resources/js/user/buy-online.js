$(function () {
    $('.car-colors span').on('click', function () {
        var color_id = $(this).data('id')
        $('[name="color_id"]').val(color_id)
    })
    setValue()
    setTerm()
    $('#loan-amount').on('change mousemove touchmove', function () {
        setValue()
    })
    $('#loan-duration').on('change mousemove touchmove', function () {
        setTerm()
    })
})

function setValue() {
    $('#down-payment').text($('#selected-amount').text())
    var totalLoan = (parseInt($('#total-price').text().replace(/AED|,| /g, '')) - parseInt($('#selected-amount').text().replace(/AED|,| /g, '')))
    $('#total-loan').text('AED ' + nC(totalLoan))
    calculate()
}

function setTerm() {
    $('#total-term').text($('#selected-duration').text() + ' Months')
    calculate()
}

function calculate() {
    var interest = parseFloat($('[name="interest"]').val())
    var term = parseInt($('#selected-duration').text())
    var price = parseInt($('#total-price').text().replace(/AED|,| /g, ''))
    var dp = parseInt($('#selected-amount').text().replace(/AED|,| /g, ''))
    var totalInterest = ((price - dp) * (term / 12) * (interest / 100)).toFixed(2)
    var totalPayable = ((price - dp) + parseFloat(totalInterest)).toFixed(2)
    var monthly = (parseFloat(totalPayable) / term).toFixed(2)
    $('#total-interest').text('AED ' + nC(totalInterest))
    $('#total-payable').text('AED ' + nC(totalPayable))
    $('#monthly-payment').text('AED ' + nC(monthly))
}

function nC(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}