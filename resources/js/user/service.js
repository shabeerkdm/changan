$(document).ready(function () {
    $('#service-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#service-form').validate({
        rules: {
            fname: { required: true },
            email: { required: true, email: true },
            phone: { required: true },
            model_id: { required: true },
            contcat_id: { required: true },
            plate_number: { required: true },
            date: { required: true }
        },
        ignore: "",
        submitHandler: function (form) {
            var formData = $(form).serializeArray();
            $('#message').html('Please wait...')
            $.ajax({
                type: 'post',
                url: url + '/service',
                dataType: "json",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (data) {
                    $('#message').html(`<span>Thank you for getting in touch! <br/>We appreciate you contacting us. One of our colleagues will get back in touch with you soon!<br/>Have a great day!</span>`)
                    setTimeout(() => {
                        location.replace(url+'/thank-you')
                    }, 3000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})