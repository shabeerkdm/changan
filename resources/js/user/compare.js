var storage = $('[name="storage"]').val()
var url = $('[name="site_url"]').val()
var type = $('[name="type"]').val()

var col = 1
var active = []
var total = $('[name="total"]').val()
$(document).ready(function () {
    init()
    $('#models').modal()
    if (type == 'models') {
        var car_1 = $('[name="car-1"]').val()
        if (car_1 != 0) {
            $('#grades').modal({ dismissible: false })
            $('#grades').modal('open')
        } else {
            $('#grades').modal()
        }
    } else {
        $('#grades').modal()
        var car_1 = $('[name="car-1"]').val()
        if (car_1 != '0')
            active.push(car_1)
        getAtts(car_1)
    }

    $('.choose-model').on('click', function () {
        var slug = $(this).attr('data-slug')
        $('#models').modal('close')
        getGrades(slug)
    })
})
var last;
function init() {
    $('.choose-grade').on('click', function () {
        var id = $(this).attr('data-id')
        if (active.findIndex(res => res == id) === -1) {
            if (last) {
                if (active.indexOf(last) !== -1)
                    active.splice(active.indexOf(last), 1);
                $('.cardetails').find('table tbody tr').find(`td:eq(${col})`).html('')
            }
            active.push(id)
            getAtts(id)
            var name = $(this).find('.exploremore__info h4').html().replace('span', 'small')
            $('#car-' + col).html(
                `<div class="compare-block__header--car">
                <img class="img-fluid add-${type}" data-id=${id} src="${$(this).find('img').attr('src')}" alt="">
            </div>
            <div class="compare-block__header--carname">
                ${name}
            </div>`
            ).removeClass('add-car')
        }
        $('#grades').modal('close')
    })
    $('.add-models').on('click', function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        e.preventDefault();
        col = $(this).closest('.vehicle').attr('id').replace('car-', '')
        if ($(this).closest('div').find('img').hasClass('img-fluid')) {
            last = $(this).attr('data-id')
        }
        $('#models').modal('open')
    })

    $('.add-grades').on('click', function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        e.preventDefault();
        col = $(this).closest('.vehicle').attr('id').replace('car-', '')
        if ($(this).closest('div').find('img').hasClass('img-fluid')) {
            last = $(this).attr('data-id')
        }
        if (active.length < parseInt(total)) {
            $('#grades').modal('open')
        } else {
            $(this).html('<span>No more grades available</span>')
        }
    })
}

function getAtts(id) {
    $.ajax({
        type: 'post',
        url: url + '/get-attributes',
        dataType: "json",
        data: { id: id },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            var atts = data.atts.map(res => {
                if (res.value.trim() == 'Yes') {
                    res.value = '&#x2713;'
                }
                if (res.value.trim() == 'No') {
                    res.value = '&#x2717;'
                }
                return res
            })
            setAtts(atts)
            if (active.length == 1) {
                var html = '<tr>'
                html += `<td></td>`
                for (var i = 1; i <= 3; i++) {
                    if (i == col)
                        html += `<td><div class="compare-block__footer--carname">
                       
                    </div>
                    <div class="loan__actions mt-1">
                        <ul>
                            <li class="w-100"> <a class="btn btn-primary"
                                    href="${url + '/book-test-drive?model=' + data.grade.slug}">BOOK A
                                    TEST DRIVE</a>
                            </li>
                            <li class="w-100 mt-1"> <a class="btn btn-primary"
                                    href="${url + '/vehicle/' + data.grade.slug}">Learn
                                    More</a></li>
                        </ul>
                    </div></td>`
                    else
                        html += '<td></td>'
                }
                html += '</tr>'
                $('.compare-block__footer').find('table tbody').html(html)
            } else {
                $('.compare-block__footer').find(`table tbody tr td:nth-child(${parseInt(col) + 1})`).html(`
                <div class="compare-block__footer--carname">
    
                    </div>
                    <div class="loan__actions mt-1">
                        <ul>
                            <li class="w-100"> <a class="btn btn-primary"
                                    href="${url + '/book-test-drive?model=' + data.grade.slug}">BOOK A
                                    TEST DRIVE</a>
                            </li>
                            <li class="w-100 mt-1"> <a class="btn btn-primary"
                                    href="${url + '/vehicle/' + data.grade.slug}">Learn
                                    More</a></li>
                        </ul>
                    </div>
                `)
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function setAtts(atts) {
    $('.cardetails').find(`table tbody tr td:nth-child(${col + 1})`).html('')
    if (active.length == 1) {
        $('.cardetails').find('table tbody').html('')
    }
    atts.forEach(element => {
        if (active.length == 1) {
            var html = '<tr>'
            html += `<td>${element.attribute}</td>`
            for (var i = 1; i <= 3; i++) {
                if (i == col)
                    html += `<td>${element.value}</td>`
                else
                    html += '<td></td>'
            }
            html += '</tr>'
            $('#group_' + element.group_id).find('table tbody').append(html)
        } else {
            $('#group_' + element.group_id).find('table tbody tr td:nth-child(1)').filter(function () {
                return $(this).text().trim() == element.attribute.trim();
            }).closest('tr').find(`td:nth-child(${parseInt(col) + 1})`).html(element.value);
        }
    });
    init()
    last = ''
}

function getGrades(slug) {
    $.ajax({
        type: 'post',
        url: url + '/get-grades',
        dataType: "json",
        data: { slug: slug },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            $('#grades-content').html('')
            data.forEach(res => {
                $('#grades-content').append(`
                    <div class="col-sm-4 choose-grade" data-id=${res.id}>
                    <div class="exploremore__car">
                        <img src="${storage + res.image}" alt="">
                    </div>
                    <div class="exploremore__info">
                        <h4>${res.model_name} <span>${res.name}</span></h4>
                    </div>
                </div>`)
            })
            $('#grades').modal('open')
            init()
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function nC(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}