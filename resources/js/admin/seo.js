$(document).ready(function () {
    $('#seo-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#seo-form').validate({
        rules: {
            page: {
                required: true
            },
            slug: {
                required: true
            },
            title: {
                required: true
            },
            description: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            var action = $(form).attr('action')
            $.ajax({
                type: 'post',
                url: action,
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    if (result.status) {
                        swal(result.message, {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.replace(url + '/admin/seo')
                        }, 2000);
                    } else {
                        var ul = document.createElement("ul");
                        ul.style.listStyleType = "none";
                        $.each(result.message, function (key, value) {
                            ul.innerHTML += `<li>${value}</li>`;
                        });
                        swal({
                            title: "An error occured!",
                            content: ul,
                            icon: "error",
                            buttons: true,
                            dangerMode: true,
                        });
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})