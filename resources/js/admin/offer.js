$(document).ready(function () {
    var method = $('[name="_method"]').val() == 'patch'
    $('#offer-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#offer-form').validate({
        rules: {
            title: {
                required: true
            },
            slug: {
                required: true
            },
            image: {
                required: !method
            },
            description: {
                required: function () {
                    return document.querySelector("#description").editorInstance.getData();
                }
            },
            body: {
                required: function () {
                    return document.querySelector("#body").editorInstance.getData();
                }
            }
        },
        ignore: [],
        onfocusout: function (element) {
            if ($(element).hasClass('ck-editor__editable')) {
                return false;
            }
            this.element(element);
        },
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            var action = $(form).attr('action')
            $.ajax({
                type: 'post',
                url: action,
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    swal(result.message, {
                        icon: "success",
                    });
                    setTimeout(() => {
                        location.replace(url + '/admin/offers')
                    }, 2000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})

function setGrade(self) {
    var model = self.value;
    $.ajax({
        type: 'get',
        url: url + '/admin/grades/' + model,
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            if (data.length > 0) {
                $('#grade').empty()
                data.forEach(element => {
                    $('#grade').append(new Option(element.name, element.id, element.id == model, true));
                });
                $('#grade').closest('.form-group').removeClass('d-none')
            } else {
                $('#grade').closest('.form-group').addClass('d-none')
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}