var specs = [], atts = [], clrs = [];
var is_edit = $('[name="is_edit"]').val()
$(document).ready(function () {
    $('#model-form, #add-grade-form,#specs-form, #att-form, #color-form, #gallery-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#model-form').validate({
        rules: {
            name: {
                required: true
            },
            slug: {
                required: true
            },
            title: {
                required: true
            },
            slogan: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($('#gallery-form')[0])
            var formData = $(form).serializeArray();
            formData.forEach((element) => {
                form_data.append(element.name, element.value);
            });
            form_data.append('view', $('#360_view').val())
            specs.forEach((res, i) => {
                form_data.append("spec_" + i + "_image", res.image);
            })
            clrs.forEach((res, i) => {
                form_data.append("clr_" + i + "_image", res.image);
            })
            form_data.append('specs', JSON.stringify(specs))
            form_data.append('atts', JSON.stringify(atts))
            form_data.append('clrs', JSON.stringify(clrs))
            var action = $(form).attr('action')
            $.ajax({
                type: 'post',
                url: action,
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    if (result.status) {
                        swal(result.message, {
                            icon: "success",
                        });
                        setTimeout(() => {
                            if (is_edit)
                                location.reload()
                            else
                                location.replace(url + '/admin/models/' + result.data)
                        }, 2000);
                    }
                    else
                        console.log(error);

                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    });
    $('#add-grade-form').validate({
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            form_data.append('features', Cdocument.querySelector("#features").editorInstance.getData());
            $.ajax({
                type: 'post',
                url: url + '/admin/grade',
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    swal(result.message, {
                        icon: "success",
                    });
                    setTimeout(() => {
                        location.reload()
                    }, 1000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
    $('.grade-edit').on('click', function () {
        var grade = $('#grade').val()
        $.ajax({
            type: 'get',
            url: url + '/admin/grade/' + grade,
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                $('#addGradeTitle').text('Edit Grade')
                $('#grade-name').val(result.name)
                $('#price').val(result.price)
                $('#intrest').val(result.intrest)
                $('#stock').attr('checked', result.stock == 1);
                document.querySelector("#features").editorInstance.setData(result.body)
                $('#add-grade-form').prepend(`<input type="hidden" name="grade_id" value="${grade}">`)
                $('#grade-image').attr('required', false)
                $('#addGrade').find('.modal-footer .btn-primary').text('Update')
                $('#addGrade').find('.modal-footer').prepend(`<button type="button" class="btn btn-danger delete" data-model="grade" data-id="${grade}">Delete</button>`)
                $('#addGrade').modal('show')
                $('.delete').on('click', function () {
                    deleteFn(this)
                })
            },
            error: function (error) {
                console.log(error);
            }
        })
    })
    $('#addGrade').on('hide.coreui.modal', function () {
        $('#addGradeTitle').text('Add Grade')
        $('#add-grade-form').find('[name="grade_id"]').remove()
        $('#grade-image').attr('required', true)
        $('#add-grade-form')[0].reset()
        document.querySelector("#features").editorInstance.setData('')
        $('#addGrade').find('.modal-footer').find('.delete').remove()
        $('#addGrade').find('.modal-footer .btn-primary').text('Save')
    })
    $('#specs-form').validate({
        rules: {
            title: {
                required: true
            },
            body: {
                required: function () {
                    return document.querySelector("#body").editorInstance.getData();
                }
            }
        },
        ignore: [],
        onfocusout: function (element) {
            if ($(element).hasClass('ck-editor__editable')) {
                return false;
            }
            this.element(element);
        },
        submitHandler: function (form) {
            var image = $(form).find('[type="file"]')[0].files[0]
            var specification = $(form).serializeArray()
            addSpecs({ id: '', index: specification[0].value, title: specification[1].value, type: specification[2].value, body: specification[3].value, image: image ? image : '', deleted: false })
            $(form)[0].reset()
            $('[name="index"]').val('')
            document.querySelector("#body").editorInstance.setData("");
        }
    })
    $('#att-form').validate({
        rules: {
            attribute: {
                required: true
            },
            value: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var attribute = $(form).serializeArray()
            attribute = { id: '', index: attribute[0].value, attribute: attribute[2].value, attribute_group_id: attribute[3].value, value: attribute[4].value, grade_id: attribute[1] ? attribute[1].value : 0, deleted: false }
            addAttribute(attribute)
            $(form)[0].reset()
            $('[name="index"]').val('')
            $('#attribute_group').val(attribute.attribute_group_id)
            $('#grade').val(attribute.grade_id)
        }
    })
    $('#color-form').validate({
        rules: {
            color: {
                required: true
            },
            image: {
                required: function () {
                    return document.querySelector("#body").editorInstance.getData();
                }
            }
        },
        ignore: [],
        onfocusout: function (element) {
            if ($(element).hasClass('ck-editor__editable')) {
                return false;
            }
            this.element(element);
        },
        submitHandler: function (form) {
            var image = $(form).find('[type="file"]')[0].files[0]
            var color = $(form).serializeArray()
            var stock = 0
            if (color.length > 3) {
                stock = color[3].value ? 1 : 0
            }
            addColor({ id: '', index: color[0].value, name: color[1].value, color: color[2].value, stock: stock, image: image ? image : '', deleted: false })
            $(form)[0].reset()
            $('[name="index"]').val('')
        }
    })

    if (is_edit) {
        edit()
    }
})


function addSpecs(specification) {
    if (specification.index == '' || specs.findIndex((res, i) => specification.index == i) === -1)
        specs.push(specification)
    else {
        var spec = specs.find((res, i) => specification.index == i)
        spec.title = specification.title
        spec.type = specification.type
        spec.body = specification.body
        spec.image = specification.image
    }
    setSpecs()
}

function setSpecs() {
    $('#model-specs tbody').html('')
    specs.forEach((res, i) => {
        if (!res.deleted)
            $('#model-specs tbody').append(
                `<tr>
                <td>${res.title}</td>
                <td>
                <button class="btn btn-warning action" onclick="editSpec(${i})"><i class="fa fa-edit fa-sm"></i></button>
                <button class="btn btn-danger action" onclick="del('s',${i})"><i class="fa fa-trash fa-sm"></i></button>
                </td>
            </tr>`
            )
    })
}

function addAttribute(attribute) {
    if (attribute.index == '' || atts.findIndex((res, i) => attribute.index == i) === -1)
        atts.push(attribute)
    else {
        var att = atts.find((res, i) => attribute.index == i)
        att.attribute = attribute.attribute
        att.value = attribute.value
        att.attribute_group_id = attribute.attribute_group_id
    }
    setAtts()
}

function setAtts() {
    $('#model-atts tbody').html('')
    var grade = $('#grade').val()
    if (parseInt(grade) != 0 && grade != null) {
        $('#grade-edit').removeClass('d-none')
    } else {
        $('#grade-edit').addClass('d-none')
    }
    atts.forEach((res, i) => {
        if (!res.deleted && res.grade_id == $('#grade').val())
            $('#model-atts tbody').append(
                `<tr>
                <td>${res.attribute}</td>
                <td>${res.value}</td>
                <td>
                    <button class="btn btn-warning action" onclick="editAtt(${i})"><i class="fa fa-edit fa-sm"></i></button>
                    <button class="btn btn-danger action" onclick="del('a',${i})"><i class="fa fa-trash fa-sm"></i></button>
                </td>
            </tr>`
            )
    })
}

function addColor(color) {
    if (color.index == '' || clrs.findIndex((res, i) => color.index == i || res.color == color.color) === -1)
        clrs.push(color)
    else {
        var clr = clrs.find((res, i) => color.index == i || res.name == color.name)
        clr.name = color.name
        clr.color = color.color
        clr.image = color.image
        clr.stock = color.stock
    }
    setColors()
}

function setColors() {
    $('#model-colors tbody').html('')
    clrs.forEach((res, i) => {
        if (!res.deleted)
            $('#model-colors tbody').append(
                `<tr>
                <td>${res.name}</td>
                <td>${res.color}</td>
                <td>${res.stock ? 'Yes' : 'No'}</td>
                <td>
                <button class="btn btn-warning action" onclick="editClr(${i})"><i class="fa fa-edit fa-sm"></i></button>
                    <button class="btn btn-danger action" onclick="del('c',${i})"><i class="fa fa-trash fa-sm"></i></button>
                </td>
            </tr>`
            )
    })
}

function edit() {
    $.ajax({
        type: 'get',
        url: url + '/admin/model/' + is_edit,
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            specs = data.specs.map(res => {
                res.deleted = false
                return res
            });
            atts = data.atts.map(res => {
                res.deleted = false
                return res
            });
            clrs = data.clrs.map(res => {
                res.deleted = false
                return res
            });
            setSpecs()
            setAtts()
            setColors()
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function editSpec(index) {
    var spec = specs[index]
    $('#spec-title').val(spec.title)
    $('#spec-type').val(spec.type)
    $('#specs-form').find('[name="index"]').val(index)
    document.querySelector("#body").editorInstance.setData(spec.body);
}

function editAtt(index) {
    var att = atts[index]
    $('#attribute').val(att.attribute)
    $('#attribute_group').val(att.attribute_group_id)
    $('#value').val(att.value)
    $('#att-form').find('[name="index"]').val(index)
}

function editClr(index) {
    var clr = clrs[index]
    $('#color-name').val(clr.name)
    $('#color').val(clr.color)
    $('#c_stock').attr('checked', clr.stock == 1)
    $('#color-form').find('[name="index"]').val(index)
}

function del(type, index) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((res) => {
        if (res) {
            if (type == 's') {
                specs[index].deleted = true
                setSpecs()
            }
            else if (type == 'a') {
                atts[index].deleted = true
                setAtts()
            } else if (type == 'c') {
                clrs[index].deleted = true
                setColors()
            }
        }
        else {
            swal("Not deleted");
        }
    });
}