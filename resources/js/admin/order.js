$(document).ready(function () {
    $('#orderPopup').on('show.coreui.modal', function (e) {
        var id = $(e.relatedTarget).data('id')
        $('[name="order_id"]').val(id)
        $.ajax({
            type: 'get',
            url: url + '/admin/orders/' + id,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                $('#orderPopup').find('.modal-body tbody').html('')
                Object.keys(result).forEach(key => {
                    if (key == 'Payment') {
                        result[key] = result[key] ? 'Success' : 'Failed'
                    }
                    if (key == 'Status') {
                        $('#status').val(result[key])
                    }
                    if (result[key]) {
                        $('#orderPopup').find('.modal-body tbody').append(`
                        <tr>
                            <td>${key}</td>
                            <td>${result[key]}</td>
                        </tr>
                    `)
                    }
                })
            },
            error: function () { }
        })
    })
    $('#change-status').on('submit', function (e) {
        e.preventDefault()
        var formData = $(this).serializeArray()
        var id = $('[name="order_id"]').val()
        $.ajax({
            type: 'post',
            url: url + '/admin/orders/' + id,
            data: formData,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) { 
                location.reload()
            },
            error: function () { }
        })
    })
})