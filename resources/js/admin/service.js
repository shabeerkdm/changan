var statuses = []
$(document).ready(function () {
    $('#status-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#servicePopup').on('show.coreui.modal', function (e) {
        var id = $(e.relatedTarget).data('id')
        $('[name="service_id"]').val(id)
        $.ajax({
            type: 'get',
            url: url + '/admin/services/' + id,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                $('#servicePopup').find('.modal-body tbody').html('')
                Object.keys(result).forEach(key => {
                    if (result[key] && key != 'status') {
                        $('#servicePopup').find('.modal-body tbody').append(`
                        <tr>
                            <td>${key}</td>
                            <td>${result[key]}</td>
                        </tr>
                    `)
                    }
                })
            },
            error: function () { }
        })
    })
    $('#serviceStatus').on('show.coreui.modal', function (e) {
        var id = $(e.relatedTarget).data('id')
        $('[name="service_id"]').val(id)
        $.ajax({
            type: 'get',
            url: url + '/admin/services/' + id,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                if (result.status)
                    statuses = JSON.parse(result.status)
                setStatus()
            },
            error: function () { }
        })
        $('#status-form').validate({
            rules: {
                date: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            ignore: "",
            submitHandler: function (form) {
                var data = $(form).serializeArray()
                $(form)[0].reset()
                $('[name="index"]').val('')
                var status = { index: data[0].value, date: data[1].value, status: data[2].value }
                addStatus(status)
            }
        })
    })
    $('#search-service').on('submit', function (e) {
        e.preventDefault()
        var search = $(this).find('[name="search"]').val()
        if (search != '') {
            location.replace(url + '/admin/services?search=' + search);
        }
    })
})

function addStatus(status) {
    if (status.index == '' || statuses.findIndex((res, i) => status.index == i) === -1)
        statuses.push(status)
    else {
        var stt = statuses.find((res, i) => status.index == i)
        stt.date = status.date
        stt.status = status.status
    }
    setStatus()
}

function setStatus() {
    $('#serviceStatus').find('.modal-body tbody').html('')
    if (statuses && statuses.length > 0)
        statuses.forEach((status, i) => {
            $('#serviceStatus').find('.modal-body tbody').append(`
            <tr>
                <td>${status.date}</td>
                <td>${status.status}</td>
                <td>
                <button class="btn btn-warning action" onclick="editStatus(${i})"><i class="fa fa-edit fa-sm"></i></button>
                    <button class="btn btn-danger action" onclick="del(${i})"><i class="fa fa-trash fa-sm"></i></button>
                </td>
            </tr>
        `)
        })
}

function editStatus(index) {
    var status = statuses[index]
    $('#date').val(status.date)
    $('#status').val(status.status)
    $('#status-form').find('[name="index"]').val(index)
}

function del(index) {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((res) => {
        if (res) {
            statuses.splice(index, 1)
            setStatus()
        }
        else {
            swal("Not deleted");
        }
    });
}

function update() {
    var id = $('[name="service_id"]').val()
    $.ajax({
        type: 'post',
        url: url + '/admin/services/' + id,
        data: { status: JSON.stringify(statuses), _method: 'patch' },
        dataType: "JSON",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (result) {
            location.reload()
        },
        error: function () { }
    })
}