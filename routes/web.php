<?php

use App\Http\Repositories\SeoRepository;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# -- AUTH -- #
Route::get('login', function () {
    return view('auth.login');
})->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::get('password/reset', 'AuthController@showLinkRequestForm');
Route::post('password/email', 'AuthController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'AuthController@showResetForm')->name('password.reset');
Route::post('password/reset', 'AuthController@reset');
# -- USER -- #
Route::middleware(['auth', 'role:USER'])->group(function () {
    Route::get('account', 'SiteController@account');
    Route::post('customer', 'SiteController@customer');
    Route::post('user', 'SiteController@user');
});
# -- ADMIN -- #
Route::prefix('admin')->group(function () {
    Route::get('/', 'AuthController@index');
    Route::group(["namespace" => "Admin"], function () {
        Route::middleware(['auth', 'role:ADMIN,SERVICE'])->group(function () {
            Route::get('dashboard', 'AdminController@dashboard');

            Route::get('leads', 'AdminController@leads');
            Route::get('leads/{id}', 'AdminController@lead');

            Route::get('services', 'AdminController@services');
            Route::get('services/{id}', 'AdminController@service');
            Route::patch('services/{id}', 'AdminController@serviceStatus');

            Route::get('orders', 'AdminController@orders');
            Route::get('orders/{id}', 'AdminController@order');
            Route::patch('orders/{id}', 'AdminController@orderStatus');

            Route::get('page/{slug}', 'AdminController@page');
            Route::post('page/{slug}', 'AdminController@savePage');

            Route::get('sliders', 'AdminController@sliders');
            Route::get('sliders-list', 'AdminController@getSliders');
            Route::get('sliders/{id}', 'AdminController@getSlides');
            Route::post('sliders', 'AdminController@saveSlider');
            Route::post('slide', 'AdminController@saveSlide');

            Route::get('posts', 'AdminController@posts');
            Route::get('posts/add', 'AdminController@postCreate');
            Route::get('posts/{id}', 'AdminController@postEdit');
            Route::get('post/{id}', 'AdminController@getPost');
            Route::post('posts', 'AdminController@savePost');
            Route::patch('posts/{id}', 'AdminController@updatePost');

            Route::get('offers', 'AdminController@offers');
            Route::get('offers/add', 'AdminController@offerCreate');
            Route::get('offers/{id}', 'AdminController@offerEdit');
            Route::get('offer/{id}', 'AdminController@getOffer');
            Route::post('offers', 'AdminController@saveOffer');
            Route::patch('offers/{id}', 'AdminController@updateOffer');

            Route::get('contact', 'AdminController@contact');
            Route::post('contact', 'AdminController@saveContact');

            Route::post('add-image', 'AdminController@saveImage');
            Route::post('get-images', 'AdminController@getImages');

            Route::get('models', 'VehicleController@index');
            Route::get('models/add', 'VehicleController@create');
            Route::get('models/{id}', 'VehicleController@edit');
            Route::get('model/{id}', 'VehicleController@getModel');
            Route::post('models', 'VehicleController@store');
            Route::patch('models/{id}', 'VehicleController@update');

            Route::get('grades/{id}', 'VehicleController@getGrades');
            Route::post('grade', 'VehicleController@addGrade');
            Route::get('grade/{id}', 'VehicleController@getGrade');

            Route::get('seo', 'AdminController@seo');
            Route::post('seo', 'AdminController@saveSeo');
            Route::get('seo/{id}', 'AdminController@editSeo');
            Route::patch('seo/{id}', 'AdminController@updateSeo');

            Route::get('settings', 'AdminController@settings');
            Route::post('options', 'AdminController@options');

            Route::delete('delete', 'AdminController@delete');
        });
        // Route::middleware(['auth', 'role:SERVICE'])->group(function () {
        //     Route::get('dashboard', 'AdminController@dashboard');
        //     Route::get('services', 'AdminController@services');
        //     Route::get('services/{id}', 'AdminController@service');
        //     Route::patch('services/{id}', 'AdminController@serviceStatus');
        // });
    });
});

#-- STATIC PAGES-- #

Route::get('history', function (SeoRepository $seo) {
    return view('static.history', ['seo' => $seo->getSeo('history', 1)]);
});
Route::get('innovations', function (SeoRepository $seo) {
    return view('static.innovations', ['seo' => $seo->getSeo('innovations', 1)]);
});
Route::get('intelligence', function (SeoRepository $seo) {
    return view('static.intelligence', ['seo' => $seo->getSeo('intelligence', 1)]);
});
Route::get('quality', function (SeoRepository $seo) {
    return view('static.quality', ['seo' => $seo->getSeo('quality', 1)]);
});
Route::get('design', function (SeoRepository $seo) {
    return view('static.design', ['seo' => $seo->getSeo('design', 1)]);
});
Route::get('sustainable', function (SeoRepository $seo) {
    return view('static.sustainable', ['seo' => $seo->getSeo('sustainable', 1)]);
});
Route::get('maintenance', function (SeoRepository $seo) {
    return view('static.maintenance', ['seo' => $seo->getSeo('maintenance', 1)]);
});
Route::get('terms', function (SeoRepository $seo) {
    return view('static.terms', ['seo' => $seo->getSeo('terms', 1)]);
});
Route::get('privacy', function (SeoRepository $seo) {
    return view('static.privacy', ['seo' => $seo->getSeo('privacy', 1)]);
});
Route::get('thank-you', function () {
    return view('static.thanks');
});
#-- DYNAMIC PAGES-- #

Route::get('/', 'SiteController@page');
Route::get('offers', 'SiteController@offers');
Route::get('sales/{slug}', 'SiteController@offer');
Route::post('offer', 'SiteController@getOffer');
Route::get('news', 'SiteController@news');
Route::get('news/{slug}', 'SiteController@post');
Route::post('news', 'SiteController@loadPosts');
Route::get('contact', 'SiteController@contact');
Route::get('compare', 'SiteController@compare');
Route::get('compare/{type}/{slug}', 'SiteController@compare');
Route::post('get-grades', 'SiteController@getGrades');
Route::post('get-attributes', 'SiteController@getAtts');
Route::get('car-online', 'SiteController@list');
Route::get('buy-online/{slug}', 'SiteController@buy');
Route::match(['GET', 'POST'], 'reserve', 'SiteController@reserve');
Route::post('contact', 'SiteController@lead');
Route::get('book-test-drive', 'SiteController@testDrive');
Route::get('enquire', 'SiteController@enquire');
Route::get('service-booking', 'SiteController@service');
Route::post('service', 'SiteController@saveService');
Route::get('tracking', 'SiteController@tracking');
Route::post('tracking', 'SiteController@getTracking');
Route::get('findlocation', 'SiteController@location');
Route::post('order', 'SiteController@createOrder');
Route::get('payment/{status}', 'SiteController@payment');
Route::post('send-otp', 'SiteController@sendOtp');
Route::post('verify-otp', 'SiteController@verifyOtp');
Route::get('{slug}', 'SiteController@page');
Route::get('vehicle/{slug}', 'SiteController@show');
