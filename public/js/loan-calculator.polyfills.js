/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

/*!
 * jQuery Loan Calculator 3.1.3
 *
 * Author: Jorge González <scrub.mx@gmail>
 * Released under the MIT license - https://opensource.org/licenses/MIT
 */
;
(function ($, window, document, undefined) {

  "use strict";

  /**
   * Table of credit rates for every score.
   * @type {Object}
   */

  var CREDIT_RATES = {
    'A': 5.32,
    'B': 8.18,
    'C': 12.29,
    'D': 15.61,
    'E': 18.25,
    'F': 21.99,
    'G': 26.77
  };

  /**
   * Table of allowed payment frequencies.
   * @type {Object}
   */
  var PAYMENT_FREQUENCIES = {
    'weekly': 52,
    'biweekly': 26,
    'monthly': 12
  };

  /**
   * The minimum allowed for a loan.
   * @type {Number}
   */
  var MINIMUM_LOAN = 1000;

  /**
   * The minimum duration in months.
   * @type {Number}
   */
  var MINIMUM_DURATION = 1;

  /**
   * Default options for the plugin.
   * @type {Object}
   */
  var defaults = {
    // default values for a loan
    loanAmount: 50000,
    loanDuration: 12,
    creditRates: CREDIT_RATES,
    creditScore: 'A',
    valueAddedTax: 0,
    serviceFee: 0,
    paymentFrequency: 'monthly',

    // inputs
    loanAmountSelector: '#loan-amount',
    loanDurationSelector: '#loan-duration',
    creditScoreSelector: '#credit-score',
    paymentFrequencySelector: '#payment-frequency',

    // display selected values
    selectedAmount: '#selected-amount',
    selectedDuration: '#selected-duration',
    selectedScore: '#selected-score',
    selectedPaymentFrequency: '#selected-payment-frequency',

    // display the results
    loanTotalSelector: '#loan-total',
    paymentSelector: '#payment',
    interestTotalSelector: '#interest-total',
    serviceFeeSelector: '#service-fee',
    taxTotalSelector: '#tax-total',
    totalAnnualCostSelector: '#total-annual-cost',
    loanGrandTotalSelector: '#grand-total'
  };

  /**
   * The actual plugin constructor
   * @param {Object} element
   * @param {Object} options
   */
  function Plugin(element, options) {
    this.$el = $(element);
    this._name = 'loanCalculator';
    this._defaults = defaults;
    this.settings = $.extend({}, defaults, options);
    this.attachListeners();
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {

    /**
     * Validates the data and shows the results.
     * @return {void}
     */
    init: function () {
      this.validate();
      this.render();
    },

    /**
     * Attach event listeners to the event handlers.
     * @return {void}
     */
    attachListeners: function () {
      var eventEmitters = [this.settings.loanAmountSelector, this.settings.loanDurationSelector, this.settings.creditScoreSelector, this.settings.paymentFrequencySelector];

      $(eventEmitters.join()).on({
        change: this.eventHandler.bind(this),
        mousemove: this.eventHandler.bind(this),
        touchmove: this.eventHandler.bind(this)
      });
    },

    /**
     * Handle events from the DOM.
     * @return {void}
     */
    eventHandler: function () {
      this.update({
        loanAmount: this.$el.find(this.settings.loanAmountSelector).val(),
        loanDuration: this.$el.find(this.settings.loanDurationSelector).val(),
        creditScore: this.$el.find(this.settings.creditScoreSelector).val(),
        paymentFrequency: this.$el.find(this.settings.paymentFrequencySelector).val()
      });
    },

    /**
     * Sanitize and validate the user input data.
     * @throws Error
     * @return {void}
     */
    validate: function () {
      if (typeof this.settings.loanAmount === 'string') {
        this.settings.loanAmount = this._toNumeric(this.settings.loanAmount);
      }

      if (typeof this.settings.serviceFee === 'string') {
        this.settings.serviceFee = this._toNumeric(this.settings.serviceFee);
      }

      if (!$.isPlainObject(this.settings.creditRates)) {
        throw new Error('The value provided for [creditRates] is not valid.');
      }

      for (var creditRate in this.settings.creditRates) {
        if (typeof this.settings.creditRates[creditRate] === 'string') {
          this.settings.creditRates[creditRate] = this._toNumeric(this.settings.creditRates[creditRate]);
        }

        if (!$.isNumeric(this.settings.creditRates[creditRate])) {
          throw new Error('The value provided for [creditRates] is not valid.');
        }

        if (this.settings.creditRates[creditRate] < 1) {
          this.settings.creditRates[creditRate] = this.settings.creditRates[creditRate] * 100;
        }
      }

      // Sanitize the input
      this.settings.loanAmount = parseFloat(this.settings.loanAmount);
      this.settings.loanDuration = parseFloat(this.settings.loanDuration);
      this.settings.serviceFee = parseFloat(this.settings.serviceFee);
      this.settings.creditScore = $.trim(this.settings.creditScore.toUpperCase());

      if (!PAYMENT_FREQUENCIES.hasOwnProperty(this.settings.paymentFrequency)) {
        throw new Error('The value provided for [paymentFrequency] is not valid.');
      }

      if (!this.settings.creditRates.hasOwnProperty(this.settings.creditScore)) {
        throw new Error('The value provided for [creditScore] is not valid.');
      }

      if (this.settings.loanAmount < MINIMUM_LOAN) {
        throw new Error('The value provided for [loanAmount] must me at least 1000.');
      }

      if (this.settings.loanDuration < MINIMUM_DURATION) {
        throw new Error('The value provided for [loanDuration] must me at least 1.');
      }

      if (!$.isNumeric(this.settings.serviceFee)) {
        throw new Error('The value provided for [serviceFee] is not valid.');
      }
    },

    /**
     * Show the results in the DOM.
     * @return {void}
     */
    render: function () {
      this._displaySelectedValues();
      this._displayResults();
    },

    /**
     * Show the selected values in the DOM.
     * @return {void}
     */
    _displaySelectedValues: function () {
      // Display the selected loan amount
      this.$el.find(this.settings.selectedAmount).html(this._toMoney(this.settings.loanAmount));

      // Display the selected loan duration
      this.$el.find(this.settings.selectedDuration).html(this.settings.loanDuration);

      // Display the selected credit score
      this.$el.find(this.settings.selectedScore).html(this.settings.creditScore);

      // Display the selected payment frequency
      this.$el.find(this.settings.selectedPaymentFrequency).html(this.settings.paymentFrequency);
    },

    /**
     * Display the results for the current values.
     * @return {void}
     */
    _displayResults: function () {
      // Display the loan total
      this.$el.find(this.settings.loanTotalSelector).html(this._toMoney(this._loanTotal()));

      // Display the loan periodic payment
      this.$el.find(this.settings.paymentSelector).html(this._toMoney(this._PMT()));

      // Display the interest total amount
      this.$el.find(this.settings.interestTotalSelector).html(this._toMoney(this._interestTotal()));

      // Display the tax total amount
      this.$el.find(this.settings.taxTotalSelector).html(this._toMoney(this._taxTotal()));

      // Display the annual total cost
      this.$el.find(this.settings.totalAnnualCostSelector).html(this._toPercentage(this._CAT()));

      // Display the service fee if any
      this.$el.find(this.settings.serviceFeeSelector).html(this._toMoney(this._serviceFeeWithVAT()));

      this.$el.find(this.settings.loanGrandTotalSelector).html(this._toMoney(this._grandTotal()));
    },

    /**
     * Run the init method again with the provided options.
     * @param {Object} args
     */
    update: function (args) {
      this.settings = $.extend({}, this._defaults, this.settings, args);
      this.init();
      this.$el.trigger('loan:update');
    },

    /**
     * Generate the results as an array of objects,
     * each object contains the values for each period.
     * @return {Array}
     */
    _results: function () {
      var balance = this.settings.loanAmount;
      var initial = this.settings.loanAmount;
      var interestRate = this._interestRate();
      var VAT = this._valueAddedTax();
      var payment = this._PMT();
      var numberOfPayments = this._numberOfPayments();
      var results = [];

      // We loop over the number of payments and each time
      // we extract the information to build the period
      // that will be appended to the results array.
      for (var paymentNumber = 0; paymentNumber < numberOfPayments; paymentNumber++) {
        var interest = balance * interestRate;
        var taxesPaid = balance * interestRate * VAT;
        var principal = payment - interest - taxesPaid;

        // update initial balance for next iteration
        initial = balance;

        // update final balance for the next iteration.
        balance = balance - principal;

        results.push({
          initial: initial,
          principal: principal,
          interest: interest,
          tax: taxesPaid,
          payment: payment,
          balance: balance
        });
      };

      return results;
    },

    /**
     * Generate the amortization schedule.
     * @return {Array}
     */
    schedule: function () {
      return $.map(this._results(), function (value) {
        return {
          initial: this._toMoney(value.initial),
          principal: this._toMoney(value.principal),
          interest: this._toMoney(value.interest),
          tax: this._toMoney(value.tax),
          payment: this._toMoney(value.payment),
          balance: this._toMoney(value.balance)
        };
      }.bind(this));
    },

    /**
     * Return the credit rates being used.
     * @return {Object}
     */
    creditRates: function () {
      return this.settings.creditRates;
    },

    /**
     * Get the credit rate corresponding to the current credit score.
     * @return {Number}
     */
    _annualInterestRate: function () {
      if (this.settings.hasOwnProperty('interestRate')) {
        if (this.settings.interestRate <= 1) {
          return this.settings.interestRate;
        }

        return this._toNumeric(this.settings.interestRate) / 100;
      }

      return this.settings.creditRates[this.settings.creditScore] / 100;
    },

    /**
     * Get the periodic interest rate.
     * @returns {Number}
     */
    _interestRate: function () {
      return this._annualInterestRate() / this._paymentFrequency();
    },

    /**
     * Returns the periodic payment frequency
     * @returns {Number}
     */
    _paymentFrequency: function () {
      return PAYMENT_FREQUENCIES[this.settings.paymentFrequency];
    },

    /**
     * Returns number of payments for the loan.
     * @returns {Number}
     */
    _numberOfPayments: function () {
      var durationInYears = this._toNumeric(this.settings.loanDuration) / 12;

      return Math.floor(durationInYears * PAYMENT_FREQUENCIES[this.settings.paymentFrequency]);
    },

    /**
     * Calculates the total cost of the loan.
     * @return {Number}
     */
    _loanTotal: function () {
      return this._PMT() * this._numberOfPayments();
    },

    /**
     * Calculate the monthly amortized loan payments.
     * @see https://en.wikipedia.org/wiki/Compound_interest#Monthly_amortized_loan_or_mortgage_payments
     * @return {Number}
     */
    _PMT: function () {
      var i = this._interestRate();
      var L = this.settings.loanAmount;
      var n = this._numberOfPayments();

      if (this.settings.valueAddedTax !== 0) {
        i = (1 + this._valueAddedTax()) * i; // interest rate with tax
      }

      return L * i / (1 - Math.pow(1 + i, -n));
    },

    /**
     * Calculate the total interest for the loan.
     * @returns {Number}
     */
    _interestTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.interest;
      }));
      return total;
    },

    /**
     * Calculate the value added tax total for the loan.
     * @returns {Number}
     */
    _taxTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.tax;
      }));
      return total;
    },

    /**
     * Return the loan fees and commissions total.
     * @return {Number}
     */
    _serviceFee: function () {
      var serviceFee = this.settings.serviceFee;

      // if the service fee is greater than 1 then the
      // value must be converted to decimals first.
      if (serviceFee > 1) {
        serviceFee = serviceFee / 100;
      }

      return this.settings.loanAmount * serviceFee;
    },

    /**
     * Return the loan fees and commissions total with VAT included.
     * @return {Number}
     */
    _serviceFeeWithVAT: function () {
      return this._serviceFee() * (this._valueAddedTax() + 1);
    },

    /**
     * Calculates the total cost of the loan including the service fee.
     * @return {Number}
     */
    _grandTotal: function () {
      return this._loanTotal() + this._serviceFeeWithVAT();
    },

    /**
     * Return the total annual cost (CAT)
     * @see http://www.banxico.org.mx/CAT
     * @return {Number}
     */
    _CAT: function () {
      var IRR = this._IRR(this._cashFlow());
      var periods = this._paymentFrequency();

      return Math.pow(1 + IRR, periods) - 1;
    },

    /**
     * Returns an array with a series of cash flows for the current loan.
     * @return {Array}
     */
    _cashFlow: function () {
      var results = this._results();
      var cashFlow = [this._serviceFee() - this.settings.loanAmount];

      $.each(results, function (index, period) {
        cashFlow.push(period.payment - period.tax);
      }.bind(this));

      return cashFlow;
    },

    /**
     * Returns the internal rate of return for a series of cash flows represented by the numbers in values.
     * @param  {Array} values
     * @param  {Number} guess
     * @return {Number}
     */
    _IRR: function (values, guess) {
      guess = guess || 0;

      // Calculates the resulting amount
      var irrResult = function (values, dates, rate) {
        var result = values[0];

        for (var i = 1; i < values.length; i++) {
          result += values[i] / Math.pow(rate + 1, (dates[i] - dates[0]) / 365);
        }

        return result;
      };

      // Calculates the first derivation
      var irrResultDerivative = function (values, dates, rate) {
        var result = 0;

        for (var i = 1; i < values.length; i++) {
          var frac = (dates[i] - dates[0]) / 365;
          result -= frac * values[i] / Math.pow(rate + 1, frac + 1);
        }

        return result;
      };

      // Initialize dates and check that values contains at
      // least one positive value and one negative value
      var dates = [];
      var positive = false;
      var negative = false;

      for (var i = 0; i < values.length; i++) {
        dates[i] = i === 0 ? 0 : dates[i - 1] + 365;
        if (values[i] > 0) positive = true;
        if (values[i] < 0) negative = true;
      }

      if (!positive || !negative) {
        throw new Error('Error the values does not contain at least one positive value and one negative value');
      }

      // Initialize guess and resultRate
      guess = guess === undefined ? 0.1 : guess;
      var resultRate = guess;

      // Set maximum epsilon for end of iteration
      var epsMax = 1e-10;

      // Implement Newton's method
      var newRate, epsRate, resultValue;
      var contLoop = true;

      do {
        resultValue = irrResult(values, dates, resultRate);
        newRate = resultRate - resultValue / irrResultDerivative(values, dates, resultRate);
        epsRate = Math.abs(newRate - resultRate);
        resultRate = newRate;
        contLoop = epsRate > epsMax && Math.abs(resultValue) > epsMax;
      } while (contLoop);

      // Return internal rate of return
      return resultRate;
    },

    /**
     * Return the value added tax in decimals.
     * @return {Number}
     */
    _valueAddedTax: function () {
      var tax = this._toNumeric(this.settings.valueAddedTax || 0);

      // if tax is greater than 1 means the value
      // must be converted to decimals first.
      return tax > 1 ? tax / 100 : tax;
    },

    /**
     * Convert numeric format to money format.
     * @param  {Number} numeric
     * @return {String}
     */
    _toMoney: function (numeric) {
      if (typeof numeric == 'string') {
        numeric = parseFloat(numeric);
      }

      return 'AED ' + numeric.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    },

    /**
     * Convert from money format to numeric format.
     * @param  {String} value
     * @return {Number}
     */
    _toNumeric: function (value) {
      return parseFloat(value.toString().replace(/[^0-9\.]+/g, ''));
    },

    /**
     * To convert the provided value to percent format.
     * @param {Number} numeric
     * @returns {String}
     */
    _toPercentage: function (numeric) {
      return (numeric * 100).toFixed(2) + '%';
    }

  });

  /**
   * Wrapper around the constructor to prevent multiple instantiations.
   */
  $.fn.loanCalculator = function (options, args) {
    if (options === 'schedule') {
      return this.data('plugin_loanCalculator').schedule();
    }

    if (options === 'rates') {
      return this.data('plugin_loanCalculator').creditRates();
    }

    return this.each((function () {
      var instance = $.data(this, 'plugin_loanCalculator');
      if (!instance) {
        $.data(this, 'plugin_loanCalculator', new Plugin(this, options));
      } else if (options === 'update') {
        return instance.update(args);
      }
    }));
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5sb2FuLWNhbGN1bGF0b3IuanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsImRvY3VtZW50IiwidW5kZWZpbmVkIiwiQ1JFRElUX1JBVEVTIiwiUEFZTUVOVF9GUkVRVUVOQ0lFUyIsIk1JTklNVU1fTE9BTiIsIk1JTklNVU1fRFVSQVRJT04iLCJkZWZhdWx0cyIsImxvYW5BbW91bnQiLCJsb2FuRHVyYXRpb24iLCJjcmVkaXRSYXRlcyIsImNyZWRpdFNjb3JlIiwidmFsdWVBZGRlZFRheCIsInNlcnZpY2VGZWUiLCJwYXltZW50RnJlcXVlbmN5IiwibG9hbkFtb3VudFNlbGVjdG9yIiwibG9hbkR1cmF0aW9uU2VsZWN0b3IiLCJjcmVkaXRTY29yZVNlbGVjdG9yIiwicGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yIiwic2VsZWN0ZWRBbW91bnQiLCJzZWxlY3RlZER1cmF0aW9uIiwic2VsZWN0ZWRTY29yZSIsInNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSIsImxvYW5Ub3RhbFNlbGVjdG9yIiwicGF5bWVudFNlbGVjdG9yIiwiaW50ZXJlc3RUb3RhbFNlbGVjdG9yIiwic2VydmljZUZlZVNlbGVjdG9yIiwidGF4VG90YWxTZWxlY3RvciIsInRvdGFsQW5udWFsQ29zdFNlbGVjdG9yIiwibG9hbkdyYW5kVG90YWxTZWxlY3RvciIsIlBsdWdpbiIsImVsZW1lbnQiLCJvcHRpb25zIiwiJGVsIiwiX25hbWUiLCJfZGVmYXVsdHMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImF0dGFjaExpc3RlbmVycyIsImluaXQiLCJwcm90b3R5cGUiLCJ2YWxpZGF0ZSIsInJlbmRlciIsImV2ZW50RW1pdHRlcnMiLCJqb2luIiwib24iLCJjaGFuZ2UiLCJldmVudEhhbmRsZXIiLCJiaW5kIiwibW91c2Vtb3ZlIiwidG91Y2htb3ZlIiwidXBkYXRlIiwiZmluZCIsInZhbCIsIl90b051bWVyaWMiLCJpc1BsYWluT2JqZWN0IiwiRXJyb3IiLCJjcmVkaXRSYXRlIiwiaXNOdW1lcmljIiwicGFyc2VGbG9hdCIsInRyaW0iLCJ0b1VwcGVyQ2FzZSIsImhhc093blByb3BlcnR5IiwiX2Rpc3BsYXlTZWxlY3RlZFZhbHVlcyIsIl9kaXNwbGF5UmVzdWx0cyIsImh0bWwiLCJfdG9Nb25leSIsIl9sb2FuVG90YWwiLCJfUE1UIiwiX2ludGVyZXN0VG90YWwiLCJfdGF4VG90YWwiLCJfdG9QZXJjZW50YWdlIiwiX0NBVCIsIl9zZXJ2aWNlRmVlV2l0aFZBVCIsIl9ncmFuZFRvdGFsIiwiYXJncyIsInRyaWdnZXIiLCJfcmVzdWx0cyIsImJhbGFuY2UiLCJpbml0aWFsIiwiaW50ZXJlc3RSYXRlIiwiX2ludGVyZXN0UmF0ZSIsIlZBVCIsIl92YWx1ZUFkZGVkVGF4IiwicGF5bWVudCIsIm51bWJlck9mUGF5bWVudHMiLCJfbnVtYmVyT2ZQYXltZW50cyIsInJlc3VsdHMiLCJwYXltZW50TnVtYmVyIiwiaW50ZXJlc3QiLCJ0YXhlc1BhaWQiLCJwcmluY2lwYWwiLCJwdXNoIiwidGF4Iiwic2NoZWR1bGUiLCJtYXAiLCJ2YWx1ZSIsIl9hbm51YWxJbnRlcmVzdFJhdGUiLCJfcGF5bWVudEZyZXF1ZW5jeSIsImR1cmF0aW9uSW5ZZWFycyIsIk1hdGgiLCJmbG9vciIsImkiLCJMIiwibiIsInBvdyIsInRvdGFsIiwiZWFjaCIsImluZGV4IiwiX3NlcnZpY2VGZWUiLCJJUlIiLCJfSVJSIiwiX2Nhc2hGbG93IiwicGVyaW9kcyIsImNhc2hGbG93IiwicGVyaW9kIiwidmFsdWVzIiwiZ3Vlc3MiLCJpcnJSZXN1bHQiLCJkYXRlcyIsInJhdGUiLCJyZXN1bHQiLCJsZW5ndGgiLCJpcnJSZXN1bHREZXJpdmF0aXZlIiwiZnJhYyIsInBvc2l0aXZlIiwibmVnYXRpdmUiLCJyZXN1bHRSYXRlIiwiZXBzTWF4IiwibmV3UmF0ZSIsImVwc1JhdGUiLCJyZXN1bHRWYWx1ZSIsImNvbnRMb29wIiwiYWJzIiwibnVtZXJpYyIsInRvRml4ZWQiLCJyZXBsYWNlIiwidG9TdHJpbmciLCJmbiIsImxvYW5DYWxjdWxhdG9yIiwiZGF0YSIsImluc3RhbmNlIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7Ozs7QUFNQTtBQUNBLENBQUEsVUFBQUEsQ0FBQSxFQUFBQyxNQUFBLEVBQUFDLFFBQUEsRUFBQUMsU0FBQSxFQUFBOztBQUVBOztBQUVBOzs7OztBQUlBLE1BQUFDLGVBQUE7QUFDQSxTQUFBLElBREE7QUFFQSxTQUFBLElBRkE7QUFHQSxTQUFBLEtBSEE7QUFJQSxTQUFBLEtBSkE7QUFLQSxTQUFBLEtBTEE7QUFNQSxTQUFBLEtBTkE7QUFPQSxTQUFBO0FBUEEsR0FBQTs7QUFVQTs7OztBQUlBLE1BQUFDLHNCQUFBO0FBQ0EsY0FBQSxFQURBO0FBRUEsZ0JBQUEsRUFGQTtBQUdBLGVBQUE7QUFIQSxHQUFBOztBQU1BOzs7O0FBSUEsTUFBQUMsZUFBQSxJQUFBOztBQUVBOzs7O0FBSUEsTUFBQUMsbUJBQUEsQ0FBQTs7QUFFQTs7OztBQUlBLE1BQUFDLFdBQUE7QUFDQTtBQUNBQyxnQkFBQSxLQUZBO0FBR0FDLGtCQUFBLEVBSEE7QUFJQUMsaUJBQUFQLFlBSkE7QUFLQVEsaUJBQUEsR0FMQTtBQU1BQyxtQkFBQSxDQU5BO0FBT0FDLGdCQUFBLENBUEE7QUFRQUMsc0JBQUEsU0FSQTs7QUFVQTtBQUNBQyx3QkFBQSxjQVhBO0FBWUFDLDBCQUFBLGdCQVpBO0FBYUFDLHlCQUFBLGVBYkE7QUFjQUMsOEJBQUEsb0JBZEE7O0FBZ0JBO0FBQ0FDLG9CQUFBLGtCQWpCQTtBQWtCQUMsc0JBQUEsb0JBbEJBO0FBbUJBQyxtQkFBQSxpQkFuQkE7QUFvQkFDLDhCQUFBLDZCQXBCQTs7QUFzQkE7QUFDQUMsdUJBQUEsYUF2QkE7QUF3QkFDLHFCQUFBLFVBeEJBO0FBeUJBQywyQkFBQSxpQkF6QkE7QUEwQkFDLHdCQUFBLGNBMUJBO0FBMkJBQyxzQkFBQSxZQTNCQTtBQTRCQUMsNkJBQUEsb0JBNUJBO0FBNkJBQyw0QkFBQTtBQTdCQSxHQUFBOztBQWdDQTs7Ozs7QUFLQSxXQUFBQyxNQUFBLENBQUFDLE9BQUEsRUFBQUMsT0FBQSxFQUFBO0FBQ0EsU0FBQUMsR0FBQSxHQUFBbEMsRUFBQWdDLE9BQUEsQ0FBQTtBQUNBLFNBQUFHLEtBQUEsR0FBQSxnQkFBQTtBQUNBLFNBQUFDLFNBQUEsR0FBQTVCLFFBQUE7QUFDQSxTQUFBNkIsUUFBQSxHQUFBckMsRUFBQXNDLE1BQUEsQ0FBQSxFQUFBLEVBQUE5QixRQUFBLEVBQUF5QixPQUFBLENBQUE7QUFDQSxTQUFBTSxlQUFBO0FBQ0EsU0FBQUMsSUFBQTtBQUNBOztBQUVBO0FBQ0F4QyxJQUFBc0MsTUFBQSxDQUFBUCxPQUFBVSxTQUFBLEVBQUE7O0FBRUE7Ozs7QUFJQUQsVUFBQSxZQUFBO0FBQ0EsV0FBQUUsUUFBQTtBQUNBLFdBQUFDLE1BQUE7QUFDQSxLQVRBOztBQVdBOzs7O0FBSUFKLHFCQUFBLFlBQUE7QUFDQSxVQUFBSyxnQkFBQSxDQUNBLEtBQUFQLFFBQUEsQ0FBQXJCLGtCQURBLEVBRUEsS0FBQXFCLFFBQUEsQ0FBQXBCLG9CQUZBLEVBR0EsS0FBQW9CLFFBQUEsQ0FBQW5CLG1CQUhBLEVBSUEsS0FBQW1CLFFBQUEsQ0FBQWxCLHdCQUpBLENBQUE7O0FBT0FuQixRQUFBNEMsY0FBQUMsSUFBQSxFQUFBLEVBQUFDLEVBQUEsQ0FBQTtBQUNBQyxnQkFBQSxLQUFBQyxZQUFBLENBQUFDLElBQUEsQ0FBQSxJQUFBLENBREE7QUFFQUMsbUJBQUEsS0FBQUYsWUFBQSxDQUFBQyxJQUFBLENBQUEsSUFBQSxDQUZBO0FBR0FFLG1CQUFBLEtBQUFILFlBQUEsQ0FBQUMsSUFBQSxDQUFBLElBQUE7QUFIQSxPQUFBO0FBS0EsS0E1QkE7O0FBOEJBOzs7O0FBSUFELGtCQUFBLFlBQUE7QUFDQSxXQUFBSSxNQUFBLENBQUE7QUFDQTNDLG9CQUFBLEtBQUF5QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXJCLGtCQUFBLEVBQUFzQyxHQUFBLEVBREE7QUFFQTVDLHNCQUFBLEtBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXBCLG9CQUFBLEVBQUFxQyxHQUFBLEVBRkE7QUFHQTFDLHFCQUFBLEtBQUFzQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQW5CLG1CQUFBLEVBQUFvQyxHQUFBLEVBSEE7QUFJQXZDLDBCQUFBLEtBQUFtQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWxCLHdCQUFBLEVBQUFtQyxHQUFBO0FBSkEsT0FBQTtBQU1BLEtBekNBOztBQTJDQTs7Ozs7QUFLQVosY0FBQSxZQUFBO0FBQ0EsVUFBQSxPQUFBLEtBQUFMLFFBQUEsQ0FBQTVCLFVBQUEsS0FBQSxRQUFBLEVBQUE7QUFDQSxhQUFBNEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBLEtBQUE4QyxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTVCLFVBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsT0FBQSxLQUFBNEIsUUFBQSxDQUFBdkIsVUFBQSxLQUFBLFFBQUEsRUFBQTtBQUNBLGFBQUF1QixRQUFBLENBQUF2QixVQUFBLEdBQUEsS0FBQXlDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBZCxFQUFBd0QsYUFBQSxDQUFBLEtBQUFuQixRQUFBLENBQUExQixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQThDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsV0FBQSxJQUFBQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsRUFBQTtBQUNBLFlBQUEsT0FBQSxLQUFBMEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLEtBQUEsUUFBQSxFQUFBO0FBQ0EsZUFBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxLQUFBSCxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsQ0FBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxDQUFBMUQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLENBQUEsRUFBQTtBQUNBLGdCQUFBLElBQUFELEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxJQUFBLENBQUEsRUFBQTtBQUNBLGVBQUFyQixRQUFBLENBQUExQixXQUFBLENBQUErQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxHQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBQUFyQixRQUFBLENBQUE1QixVQUFBLEdBQUFtRCxXQUFBLEtBQUF2QixRQUFBLENBQUE1QixVQUFBLENBQUE7QUFDQSxXQUFBNEIsUUFBQSxDQUFBM0IsWUFBQSxHQUFBa0QsV0FBQSxLQUFBdkIsUUFBQSxDQUFBM0IsWUFBQSxDQUFBO0FBQ0EsV0FBQTJCLFFBQUEsQ0FBQXZCLFVBQUEsR0FBQThDLFdBQUEsS0FBQXZCLFFBQUEsQ0FBQXZCLFVBQUEsQ0FBQTtBQUNBLFdBQUF1QixRQUFBLENBQUF6QixXQUFBLEdBQUFaLEVBQUE2RCxJQUFBLENBQUEsS0FBQXhCLFFBQUEsQ0FBQXpCLFdBQUEsQ0FBQWtELFdBQUEsRUFBQSxDQUFBOztBQUVBLFVBQUEsQ0FBQXpELG9CQUFBMEQsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF0QixnQkFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEwQyxLQUFBLENBQUEseURBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBb0QsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF6QixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQTZDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxLQUFBcEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBSCxZQUFBLEVBQUE7QUFDQSxjQUFBLElBQUFtRCxLQUFBLENBQUEsNERBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsS0FBQXBCLFFBQUEsQ0FBQTNCLFlBQUEsR0FBQUgsZ0JBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQWtELEtBQUEsQ0FBQSwyREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBekQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEyQyxLQUFBLENBQUEsbURBQUEsQ0FBQTtBQUNBO0FBQ0EsS0FwR0E7O0FBc0dBOzs7O0FBSUFkLFlBQUEsWUFBQTtBQUNBLFdBQUFxQixzQkFBQTtBQUNBLFdBQUFDLGVBQUE7QUFDQSxLQTdHQTs7QUErR0E7Ozs7QUFJQUQsNEJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQTlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBakIsY0FBQSxFQUFBOEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBOUIsUUFBQSxDQUFBNUIsVUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBaEIsZ0JBQUEsRUFBQTZDLElBQUEsQ0FDQSxLQUFBN0IsUUFBQSxDQUFBM0IsWUFEQTs7QUFJQTtBQUNBLFdBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWYsYUFBQSxFQUFBNEMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF6QixXQURBOztBQUlBO0FBQ0EsV0FBQXNCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBZCx3QkFBQSxFQUFBMkMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF0QixnQkFEQTtBQUdBLEtBdklBOztBQXlJQTs7OztBQUlBa0QscUJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQS9CLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBYixpQkFBQSxFQUFBMEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBQyxVQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFsQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVosZUFBQSxFQUFBeUMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBRSxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFuQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVgscUJBQUEsRUFBQXdDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQUcsY0FBQSxFQUFBLENBREE7O0FBSUE7QUFDQSxXQUFBcEMsR0FBQSxDQUFBbUIsSUFBQSxDQUFBLEtBQUFoQixRQUFBLENBQUFULGdCQUFBLEVBQUFzQyxJQUFBLENBQ0EsS0FBQUMsUUFBQSxDQUFBLEtBQUFJLFNBQUEsRUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXJDLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBUix1QkFBQSxFQUFBcUMsSUFBQSxDQUNBLEtBQUFNLGFBQUEsQ0FBQSxLQUFBQyxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUF2QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVYsa0JBQUEsRUFBQXVDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQU8sa0JBQUEsRUFBQSxDQURBOztBQUlBLFdBQUF4QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVAsc0JBQUEsRUFBQW9DLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQVEsV0FBQSxFQUFBLENBREE7QUFHQSxLQS9LQTs7QUFpTEE7Ozs7QUFJQXZCLFlBQUEsVUFBQXdCLElBQUEsRUFBQTtBQUNBLFdBQUF2QyxRQUFBLEdBQUFyQyxFQUFBc0MsTUFBQSxDQUFBLEVBQUEsRUFBQSxLQUFBRixTQUFBLEVBQUEsS0FBQUMsUUFBQSxFQUFBdUMsSUFBQSxDQUFBO0FBQ0EsV0FBQXBDLElBQUE7QUFDQSxXQUFBTixHQUFBLENBQUEyQyxPQUFBLENBQUEsYUFBQTtBQUNBLEtBekxBOztBQTJMQTs7Ozs7QUFLQUMsY0FBQSxZQUFBO0FBQ0EsVUFBQUMsVUFBQSxLQUFBMUMsUUFBQSxDQUFBNUIsVUFBQTtBQUNBLFVBQUF1RSxVQUFBLEtBQUEzQyxRQUFBLENBQUE1QixVQUFBO0FBQ0EsVUFBQXdFLGVBQUEsS0FBQUMsYUFBQSxFQUFBO0FBQ0EsVUFBQUMsTUFBQSxLQUFBQyxjQUFBLEVBQUE7QUFDQSxVQUFBQyxVQUFBLEtBQUFoQixJQUFBLEVBQUE7QUFDQSxVQUFBaUIsbUJBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLFVBQUFDLFVBQUEsRUFBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFBLElBQUFDLGdCQUFBLENBQUEsRUFBQUEsZ0JBQUFILGdCQUFBLEVBQUFHLGVBQUEsRUFBQTtBQUNBLFlBQUFDLFdBQUFYLFVBQUFFLFlBQUE7QUFDQSxZQUFBVSxZQUFBWixVQUFBRSxZQUFBLEdBQUFFLEdBQUE7QUFDQSxZQUFBUyxZQUFBUCxVQUFBSyxRQUFBLEdBQUFDLFNBQUE7O0FBRUE7QUFDQVgsa0JBQUFELE9BQUE7O0FBRUE7QUFDQUEsa0JBQUFBLFVBQUFhLFNBQUE7O0FBRUFKLGdCQUFBSyxJQUFBLENBQUE7QUFDQWIsbUJBQUFBLE9BREE7QUFFQVkscUJBQUFBLFNBRkE7QUFHQUYsb0JBQUFBLFFBSEE7QUFJQUksZUFBQUgsU0FKQTtBQUtBTixtQkFBQUEsT0FMQTtBQU1BTixtQkFBQUE7QUFOQSxTQUFBO0FBUUE7O0FBRUEsYUFBQVMsT0FBQTtBQUNBLEtBbE9BOztBQW9PQTs7OztBQUlBTyxjQUFBLFlBQUE7QUFDQSxhQUFBL0YsRUFBQWdHLEdBQUEsQ0FBQSxLQUFBbEIsUUFBQSxFQUFBLEVBQUEsVUFBQW1CLEtBQUEsRUFBQTtBQUNBLGVBQUE7QUFDQWpCLG1CQUFBLEtBQUFiLFFBQUEsQ0FBQThCLE1BQUFqQixPQUFBLENBREE7QUFFQVkscUJBQUEsS0FBQXpCLFFBQUEsQ0FBQThCLE1BQUFMLFNBQUEsQ0FGQTtBQUdBRixvQkFBQSxLQUFBdkIsUUFBQSxDQUFBOEIsTUFBQVAsUUFBQSxDQUhBO0FBSUFJLGVBQUEsS0FBQTNCLFFBQUEsQ0FBQThCLE1BQUFILEdBQUEsQ0FKQTtBQUtBVCxtQkFBQSxLQUFBbEIsUUFBQSxDQUFBOEIsTUFBQVosT0FBQSxDQUxBO0FBTUFOLG1CQUFBLEtBQUFaLFFBQUEsQ0FBQThCLE1BQUFsQixPQUFBO0FBTkEsU0FBQTtBQVFBLE9BVEEsQ0FTQTlCLElBVEEsQ0FTQSxJQVRBLENBQUEsQ0FBQTtBQVVBLEtBblBBOztBQXFQQTs7OztBQUlBdEMsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQTBCLFFBQUEsQ0FBQTFCLFdBQUE7QUFDQSxLQTNQQTs7QUE2UEE7Ozs7QUFJQXVGLHlCQUFBLFlBQUE7QUFDQSxVQUFBLEtBQUE3RCxRQUFBLENBQUEwQixjQUFBLENBQUEsY0FBQSxDQUFBLEVBQUE7QUFDQSxZQUFBLEtBQUExQixRQUFBLENBQUE0QyxZQUFBLElBQUEsQ0FBQSxFQUFBO0FBQ0EsaUJBQUEsS0FBQTVDLFFBQUEsQ0FBQTRDLFlBQUE7QUFDQTs7QUFFQSxlQUFBLEtBQUExQixVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTRDLFlBQUEsSUFBQSxHQUFBO0FBQ0E7O0FBRUEsYUFBQSxLQUFBNUMsUUFBQSxDQUFBMUIsV0FBQSxDQUFBLEtBQUEwQixRQUFBLENBQUF6QixXQUFBLElBQUEsR0FBQTtBQUNBLEtBM1FBOztBQTZRQTs7OztBQUlBc0UsbUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQWdCLG1CQUFBLEtBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLEtBblJBOztBQXNSQTs7OztBQUlBQSx1QkFBQSxZQUFBO0FBQ0EsYUFBQTlGLG9CQUFBLEtBQUFnQyxRQUFBLENBQUF0QixnQkFBQSxDQUFBO0FBQ0EsS0E1UkE7O0FBOFJBOzs7O0FBSUF3RSx1QkFBQSxZQUFBO0FBQ0EsVUFBQWEsa0JBQUEsS0FBQTdDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBM0IsWUFBQSxJQUFBLEVBQUE7O0FBRUEsYUFBQTJGLEtBQUFDLEtBQUEsQ0FBQUYsa0JBQUEvRixvQkFBQSxLQUFBZ0MsUUFBQSxDQUFBdEIsZ0JBQUEsQ0FBQSxDQUFBO0FBQ0EsS0F0U0E7O0FBd1NBOzs7O0FBSUFxRCxnQkFBQSxZQUFBO0FBQ0EsYUFBQSxLQUFBQyxJQUFBLEtBQUEsS0FBQWtCLGlCQUFBLEVBQUE7QUFDQSxLQTlTQTs7QUFnVEE7Ozs7O0FBS0FsQixVQUFBLFlBQUE7QUFDQSxVQUFBa0MsSUFBQSxLQUFBckIsYUFBQSxFQUFBO0FBQ0EsVUFBQXNCLElBQUEsS0FBQW5FLFFBQUEsQ0FBQTVCLFVBQUE7QUFDQSxVQUFBZ0csSUFBQSxLQUFBbEIsaUJBQUEsRUFBQTs7QUFFQSxVQUFBLEtBQUFsRCxRQUFBLENBQUF4QixhQUFBLEtBQUEsQ0FBQSxFQUFBO0FBQ0EwRixZQUFBLENBQUEsSUFBQSxLQUFBbkIsY0FBQSxFQUFBLElBQUFtQixDQUFBLENBREEsQ0FDQTtBQUNBOztBQUVBLGFBQUFDLElBQUFELENBQUEsSUFBQSxJQUFBRixLQUFBSyxHQUFBLENBQUEsSUFBQUgsQ0FBQSxFQUFBLENBQUFFLENBQUEsQ0FBQSxDQUFBO0FBQ0EsS0EvVEE7O0FBaVVBOzs7O0FBSUFuQyxvQkFBQSxZQUFBO0FBQ0EsVUFBQXFDLFFBQUEsQ0FBQTtBQUNBM0csUUFBQTRHLElBQUEsQ0FBQSxLQUFBOUIsUUFBQSxFQUFBLEVBQUEsVUFBQStCLEtBQUEsRUFBQVosS0FBQSxFQUFBO0FBQ0FVLGlCQUFBVixNQUFBUCxRQUFBO0FBQ0EsT0FGQTtBQUdBLGFBQUFpQixLQUFBO0FBQ0EsS0EzVUE7O0FBNlVBOzs7O0FBSUFwQyxlQUFBLFlBQUE7QUFDQSxVQUFBb0MsUUFBQSxDQUFBO0FBQ0EzRyxRQUFBNEcsSUFBQSxDQUFBLEtBQUE5QixRQUFBLEVBQUEsRUFBQSxVQUFBK0IsS0FBQSxFQUFBWixLQUFBLEVBQUE7QUFDQVUsaUJBQUFWLE1BQUFILEdBQUE7QUFDQSxPQUZBO0FBR0EsYUFBQWEsS0FBQTtBQUNBLEtBdlZBOztBQXlWQTs7OztBQUlBRyxpQkFBQSxZQUFBO0FBQ0EsVUFBQWhHLGFBQUEsS0FBQXVCLFFBQUEsQ0FBQXZCLFVBQUE7O0FBRUE7QUFDQTtBQUNBLFVBQUFBLGFBQUEsQ0FBQSxFQUFBO0FBQ0FBLHFCQUFBQSxhQUFBLEdBQUE7QUFDQTs7QUFFQSxhQUFBLEtBQUF1QixRQUFBLENBQUE1QixVQUFBLEdBQUFLLFVBQUE7QUFDQSxLQXZXQTs7QUF5V0E7Ozs7QUFJQTRELHdCQUFBLFlBQUE7QUFDQSxhQUFBLEtBQUFvQyxXQUFBLE1BQUEsS0FBQTFCLGNBQUEsS0FBQSxDQUFBLENBQUE7QUFDQSxLQS9XQTs7QUFpWEE7Ozs7QUFJQVQsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQVAsVUFBQSxLQUFBLEtBQUFNLGtCQUFBLEVBQUE7QUFDQSxLQXZYQTs7QUF5WEE7Ozs7O0FBS0FELFVBQUEsWUFBQTtBQUNBLFVBQUFzQyxNQUFBLEtBQUFDLElBQUEsQ0FBQSxLQUFBQyxTQUFBLEVBQUEsQ0FBQTtBQUNBLFVBQUFDLFVBQUEsS0FBQWYsaUJBQUEsRUFBQTs7QUFFQSxhQUFBRSxLQUFBSyxHQUFBLENBQUEsSUFBQUssR0FBQSxFQUFBRyxPQUFBLElBQUEsQ0FBQTtBQUNBLEtBbllBOztBQXFZQTs7OztBQUlBRCxlQUFBLFlBQUE7QUFDQSxVQUFBekIsVUFBQSxLQUFBVixRQUFBLEVBQUE7QUFDQSxVQUFBcUMsV0FBQSxDQUFBLEtBQUFMLFdBQUEsS0FBQSxLQUFBekUsUUFBQSxDQUFBNUIsVUFBQSxDQUFBOztBQUVBVCxRQUFBNEcsSUFBQSxDQUFBcEIsT0FBQSxFQUFBLFVBQUFxQixLQUFBLEVBQUFPLE1BQUEsRUFBQTtBQUNBRCxpQkFBQXRCLElBQUEsQ0FBQXVCLE9BQUEvQixPQUFBLEdBQUErQixPQUFBdEIsR0FBQTtBQUNBLE9BRkEsQ0FFQTdDLElBRkEsQ0FFQSxJQUZBLENBQUE7O0FBSUEsYUFBQWtFLFFBQUE7QUFDQSxLQWxaQTs7QUFvWkE7Ozs7OztBQU1BSCxVQUFBLFVBQUFLLE1BQUEsRUFBQUMsS0FBQSxFQUFBO0FBQ0FBLGNBQUFBLFNBQUEsQ0FBQTs7QUFFQTtBQUNBLFVBQUFDLFlBQUEsVUFBQUYsTUFBQSxFQUFBRyxLQUFBLEVBQUFDLElBQUEsRUFBQTtBQUNBLFlBQUFDLFNBQUFMLE9BQUEsQ0FBQSxDQUFBOztBQUVBLGFBQUEsSUFBQWQsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBbUIsb0JBQUFMLE9BQUFkLENBQUEsSUFBQUYsS0FBQUssR0FBQSxDQUFBZSxPQUFBLENBQUEsRUFBQSxDQUFBRCxNQUFBakIsQ0FBQSxJQUFBaUIsTUFBQSxDQUFBLENBQUEsSUFBQSxHQUFBLENBQUE7QUFDQTs7QUFFQSxlQUFBRSxNQUFBO0FBQ0EsT0FSQTs7QUFVQTtBQUNBLFVBQUFFLHNCQUFBLFVBQUFQLE1BQUEsRUFBQUcsS0FBQSxFQUFBQyxJQUFBLEVBQUE7QUFDQSxZQUFBQyxTQUFBLENBQUE7O0FBRUEsYUFBQSxJQUFBbkIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBLGNBQUFzQixPQUFBLENBQUFMLE1BQUFqQixDQUFBLElBQUFpQixNQUFBLENBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQUUsb0JBQUFHLE9BQUFSLE9BQUFkLENBQUEsQ0FBQSxHQUFBRixLQUFBSyxHQUFBLENBQUFlLE9BQUEsQ0FBQSxFQUFBSSxPQUFBLENBQUEsQ0FBQTtBQUNBOztBQUVBLGVBQUFILE1BQUE7QUFDQSxPQVRBOztBQVdBO0FBQ0E7QUFDQSxVQUFBRixRQUFBLEVBQUE7QUFDQSxVQUFBTSxXQUFBLEtBQUE7QUFDQSxVQUFBQyxXQUFBLEtBQUE7O0FBRUEsV0FBQSxJQUFBeEIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBaUIsY0FBQWpCLENBQUEsSUFBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxHQUFBaUIsTUFBQWpCLElBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQSxZQUFBYyxPQUFBZCxDQUFBLElBQUEsQ0FBQSxFQUFBdUIsV0FBQSxJQUFBO0FBQ0EsWUFBQVQsT0FBQWQsQ0FBQSxJQUFBLENBQUEsRUFBQXdCLFdBQUEsSUFBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQUQsUUFBQSxJQUFBLENBQUFDLFFBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQXRFLEtBQUEsQ0FDQSxzRkFEQSxDQUFBO0FBR0E7O0FBRUE7QUFDQTZELGNBQUFBLFVBQUFuSCxTQUFBLEdBQUEsR0FBQSxHQUFBbUgsS0FBQTtBQUNBLFVBQUFVLGFBQUFWLEtBQUE7O0FBRUE7QUFDQSxVQUFBVyxTQUFBLEtBQUE7O0FBRUE7QUFDQSxVQUFBQyxPQUFBLEVBQUFDLE9BQUEsRUFBQUMsV0FBQTtBQUNBLFVBQUFDLFdBQUEsSUFBQTs7QUFFQSxTQUFBO0FBQ0FELHNCQUFBYixVQUFBRixNQUFBLEVBQUFHLEtBQUEsRUFBQVEsVUFBQSxDQUFBO0FBQ0FFLGtCQUFBRixhQUFBSSxjQUFBUixvQkFBQVAsTUFBQSxFQUFBRyxLQUFBLEVBQUFRLFVBQUEsQ0FBQTtBQUNBRyxrQkFBQTlCLEtBQUFpQyxHQUFBLENBQUFKLFVBQUFGLFVBQUEsQ0FBQTtBQUNBQSxxQkFBQUUsT0FBQTtBQUNBRyxtQkFBQUYsVUFBQUYsTUFBQSxJQUFBNUIsS0FBQWlDLEdBQUEsQ0FBQUYsV0FBQSxJQUFBSCxNQUFBO0FBQ0EsT0FOQSxRQU1BSSxRQU5BOztBQVFBO0FBQ0EsYUFBQUwsVUFBQTtBQUNBLEtBM2RBOztBQTZkQTs7OztBQUlBNUMsb0JBQUEsWUFBQTtBQUNBLFVBQUFVLE1BQUEsS0FBQXZDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBeEIsYUFBQSxJQUFBLENBQUEsQ0FBQTs7QUFFQTtBQUNBO0FBQ0EsYUFBQWlGLE1BQUEsQ0FBQSxHQUFBQSxNQUFBLEdBQUEsR0FBQUEsR0FBQTtBQUNBLEtBdmVBOztBQXllQTs7Ozs7QUFLQTNCLGNBQUEsVUFBQW9FLE9BQUEsRUFBQTtBQUNBLFVBQUEsT0FBQUEsT0FBQSxJQUFBLFFBQUEsRUFBQTtBQUNBQSxrQkFBQTNFLFdBQUEyRSxPQUFBLENBQUE7QUFDQTs7QUFFQSxhQUFBLFNBQUFBLFFBQUFDLE9BQUEsQ0FBQSxDQUFBLEVBQUFDLE9BQUEsQ0FBQSxxQkFBQSxFQUFBLEtBQUEsQ0FBQTtBQUNBLEtBcGZBOztBQXNmQTs7Ozs7QUFLQWxGLGdCQUFBLFVBQUEwQyxLQUFBLEVBQUE7QUFDQSxhQUFBckMsV0FDQXFDLE1BQUF5QyxRQUFBLEdBQUFELE9BQUEsQ0FBQSxZQUFBLEVBQUEsRUFBQSxDQURBLENBQUE7QUFHQSxLQS9mQTs7QUFpZ0JBOzs7OztBQUtBakUsbUJBQUEsVUFBQStELE9BQUEsRUFBQTtBQUNBLGFBQUEsQ0FBQUEsVUFBQSxHQUFBLEVBQUFDLE9BQUEsQ0FBQSxDQUFBLElBQUEsR0FBQTtBQUNBOztBQXhnQkEsR0FBQTs7QUE0Z0JBOzs7QUFHQXhJLElBQUEySSxFQUFBLENBQUFDLGNBQUEsR0FBQSxVQUFBM0csT0FBQSxFQUFBMkMsSUFBQSxFQUFBO0FBQ0EsUUFBQTNDLFlBQUEsVUFBQSxFQUFBO0FBQ0EsYUFBQSxLQUFBNEcsSUFBQSxDQUFBLHVCQUFBLEVBQUE5QyxRQUFBLEVBQUE7QUFDQTs7QUFFQSxRQUFBOUQsWUFBQSxPQUFBLEVBQUE7QUFDQSxhQUFBLEtBQUE0RyxJQUFBLENBQUEsdUJBQUEsRUFBQWxJLFdBQUEsRUFBQTtBQUNBOztBQUVBLFdBQUEsS0FBQWlHLElBQUEsQ0FBQSxZQUFBO0FBQ0EsVUFBQWtDLFdBQUE5SSxFQUFBNkksSUFBQSxDQUFBLElBQUEsRUFBQSx1QkFBQSxDQUFBO0FBQ0EsVUFBQSxDQUFBQyxRQUFBLEVBQUE7QUFDQTlJLFVBQUE2SSxJQUFBLENBQUEsSUFBQSxFQUFBLHVCQUFBLEVBQUEsSUFBQTlHLE1BQUEsQ0FBQSxJQUFBLEVBQUFFLE9BQUEsQ0FBQTtBQUNBLE9BRkEsTUFFQSxJQUFBQSxZQUFBLFFBQUEsRUFBQTtBQUNBLGVBQUE2RyxTQUFBMUYsTUFBQSxDQUFBd0IsSUFBQSxDQUFBO0FBQ0E7QUFDQSxLQVBBLENBQUE7QUFRQSxHQWpCQTtBQW1CQSxDQTduQkEsRUE2bkJBbUUsTUE3bkJBLEVBNm5CQTlJLE1BN25CQSxFQTZuQkFDLFFBN25CQSIsImZpbGUiOiJsb2FuLWNhbGN1bGF0b3IucG9seWZpbGxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBqUXVlcnkgTG9hbiBDYWxjdWxhdG9yIDMuMS4zXG4gKlxuICogQXV0aG9yOiBKb3JnZSBHb256w6FsZXogPHNjcnViLm14QGdtYWlsPlxuICogUmVsZWFzZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIC0gaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9NSVRcbiAqL1xuO1xuKGZ1bmN0aW9uICgkLCB3aW5kb3csIGRvY3VtZW50LCB1bmRlZmluZWQpIHtcblxuICBcInVzZSBzdHJpY3RcIjtcblxuICAvKipcbiAgICogVGFibGUgb2YgY3JlZGl0IHJhdGVzIGZvciBldmVyeSBzY29yZS5cbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHZhciBDUkVESVRfUkFURVMgPSB7XG4gICAgJ0EnOiA1LjMyLFxuICAgICdCJzogOC4xOCxcbiAgICAnQyc6IDEyLjI5LFxuICAgICdEJzogMTUuNjEsXG4gICAgJ0UnOiAxOC4yNSxcbiAgICAnRic6IDIxLjk5LFxuICAgICdHJzogMjYuNzdcbiAgfTtcblxuICAvKipcbiAgICogVGFibGUgb2YgYWxsb3dlZCBwYXltZW50IGZyZXF1ZW5jaWVzLlxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKi9cbiAgdmFyIFBBWU1FTlRfRlJFUVVFTkNJRVMgPSB7XG4gICAgJ3dlZWtseSc6IDUyLFxuICAgICdiaXdlZWtseSc6IDI2LFxuICAgICdtb250aGx5JzogMTJcbiAgfTtcblxuICAvKipcbiAgICogVGhlIG1pbmltdW0gYWxsb3dlZCBmb3IgYSBsb2FuLlxuICAgKiBAdHlwZSB7TnVtYmVyfVxuICAgKi9cbiAgdmFyIE1JTklNVU1fTE9BTiA9IDEwMDA7XG5cbiAgLyoqXG4gICAqIFRoZSBtaW5pbXVtIGR1cmF0aW9uIGluIG1vbnRocy5cbiAgICogQHR5cGUge051bWJlcn1cbiAgICovXG4gIHZhciBNSU5JTVVNX0RVUkFUSU9OID0gMTtcblxuICAvKipcbiAgICogRGVmYXVsdCBvcHRpb25zIGZvciB0aGUgcGx1Z2luLlxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKi9cbiAgdmFyIGRlZmF1bHRzID0ge1xuICAgIC8vIGRlZmF1bHQgdmFsdWVzIGZvciBhIGxvYW5cbiAgICBsb2FuQW1vdW50OiA1MDAwMCxcbiAgICBsb2FuRHVyYXRpb246IDEyLFxuICAgIGNyZWRpdFJhdGVzOiBDUkVESVRfUkFURVMsXG4gICAgY3JlZGl0U2NvcmU6ICdBJyxcbiAgICB2YWx1ZUFkZGVkVGF4OiAwLFxuICAgIHNlcnZpY2VGZWU6IDAsXG4gICAgcGF5bWVudEZyZXF1ZW5jeTogJ21vbnRobHknLFxuXG4gICAgLy8gaW5wdXRzXG4gICAgbG9hbkFtb3VudFNlbGVjdG9yOiAnI2xvYW4tYW1vdW50JyxcbiAgICBsb2FuRHVyYXRpb25TZWxlY3RvcjogJyNsb2FuLWR1cmF0aW9uJyxcbiAgICBjcmVkaXRTY29yZVNlbGVjdG9yOiAnI2NyZWRpdC1zY29yZScsXG4gICAgcGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yOiAnI3BheW1lbnQtZnJlcXVlbmN5JyxcblxuICAgIC8vIGRpc3BsYXkgc2VsZWN0ZWQgdmFsdWVzXG4gICAgc2VsZWN0ZWRBbW91bnQ6ICcjc2VsZWN0ZWQtYW1vdW50JyxcbiAgICBzZWxlY3RlZER1cmF0aW9uOiAnI3NlbGVjdGVkLWR1cmF0aW9uJyxcbiAgICBzZWxlY3RlZFNjb3JlOiAnI3NlbGVjdGVkLXNjb3JlJyxcbiAgICBzZWxlY3RlZFBheW1lbnRGcmVxdWVuY3k6ICcjc2VsZWN0ZWQtcGF5bWVudC1mcmVxdWVuY3knLFxuXG4gICAgLy8gZGlzcGxheSB0aGUgcmVzdWx0c1xuICAgIGxvYW5Ub3RhbFNlbGVjdG9yOiAnI2xvYW4tdG90YWwnLFxuICAgIHBheW1lbnRTZWxlY3RvcjogJyNwYXltZW50JyxcbiAgICBpbnRlcmVzdFRvdGFsU2VsZWN0b3I6ICcjaW50ZXJlc3QtdG90YWwnLFxuICAgIHNlcnZpY2VGZWVTZWxlY3RvcjogJyNzZXJ2aWNlLWZlZScsXG4gICAgdGF4VG90YWxTZWxlY3RvcjogJyN0YXgtdG90YWwnLFxuICAgIHRvdGFsQW5udWFsQ29zdFNlbGVjdG9yOiAnI3RvdGFsLWFubnVhbC1jb3N0JyxcbiAgICBsb2FuR3JhbmRUb3RhbFNlbGVjdG9yOiAnI2dyYW5kLXRvdGFsJ1xuICB9O1xuXG4gIC8qKlxuICAgKiBUaGUgYWN0dWFsIHBsdWdpbiBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0ge09iamVjdH0gZWxlbWVudFxuICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgKi9cbiAgZnVuY3Rpb24gUGx1Z2luKGVsZW1lbnQsIG9wdGlvbnMpIHtcbiAgICB0aGlzLiRlbCA9ICQoZWxlbWVudCk7XG4gICAgdGhpcy5fbmFtZSA9ICdsb2FuQ2FsY3VsYXRvcic7XG4gICAgdGhpcy5fZGVmYXVsdHMgPSBkZWZhdWx0cztcbiAgICB0aGlzLnNldHRpbmdzID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcbiAgICB0aGlzLmF0dGFjaExpc3RlbmVycygpO1xuICAgIHRoaXMuaW5pdCgpO1xuICB9XG5cbiAgLy8gQXZvaWQgUGx1Z2luLnByb3RvdHlwZSBjb25mbGljdHNcbiAgJC5leHRlbmQoUGx1Z2luLnByb3RvdHlwZSwge1xuXG4gICAgLyoqXG4gICAgICogVmFsaWRhdGVzIHRoZSBkYXRhIGFuZCBzaG93cyB0aGUgcmVzdWx0cy5cbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMudmFsaWRhdGUoKTtcbiAgICAgIHRoaXMucmVuZGVyKCk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEF0dGFjaCBldmVudCBsaXN0ZW5lcnMgdG8gdGhlIGV2ZW50IGhhbmRsZXJzLlxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgYXR0YWNoTGlzdGVuZXJzOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgZXZlbnRFbWl0dGVycyA9IFtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50U2VsZWN0b3IsXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uU2VsZWN0b3IsXG4gICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVTZWxlY3RvcixcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5U2VsZWN0b3JcbiAgICAgIF07XG5cbiAgICAgICQoZXZlbnRFbWl0dGVycy5qb2luKCkpLm9uKHtcbiAgICAgICAgY2hhbmdlOiB0aGlzLmV2ZW50SGFuZGxlci5iaW5kKHRoaXMpLFxuICAgICAgICBtb3VzZW1vdmU6IHRoaXMuZXZlbnRIYW5kbGVyLmJpbmQodGhpcyksXG4gICAgICAgIHRvdWNobW92ZTogdGhpcy5ldmVudEhhbmRsZXIuYmluZCh0aGlzKVxuICAgICAgfSk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEhhbmRsZSBldmVudHMgZnJvbSB0aGUgRE9NLlxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgZXZlbnRIYW5kbGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLnVwZGF0ZSh7XG4gICAgICAgIGxvYW5BbW91bnQ6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50U2VsZWN0b3IpLnZhbCgpLFxuICAgICAgICBsb2FuRHVyYXRpb246IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb25TZWxlY3RvcikudmFsKCksXG4gICAgICAgIGNyZWRpdFNjb3JlOiB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVTZWxlY3RvcikudmFsKCksXG4gICAgICAgIHBheW1lbnRGcmVxdWVuY3k6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5U2VsZWN0b3IpLnZhbCgpXG4gICAgICB9KTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogU2FuaXRpemUgYW5kIHZhbGlkYXRlIHRoZSB1c2VyIGlucHV0IGRhdGEuXG4gICAgICogQHRocm93cyBFcnJvclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgdmFsaWRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh0eXBlb2YgdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50KTtcbiAgICAgIH1cblxuICAgICAgaWYgKHR5cGVvZiB0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUpO1xuICAgICAgfVxuXG4gICAgICBpZiAoISQuaXNQbGFpbk9iamVjdCh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzKSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2NyZWRpdFJhdGVzXSBpcyBub3QgdmFsaWQuJyk7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIGNyZWRpdFJhdGUgaW4gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlcykge1xuICAgICAgICBpZiAodHlwZW9mIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCEkLmlzTnVtZXJpYyh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdKSkge1xuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbY3JlZGl0UmF0ZXNdIGlzIG5vdCB2YWxpZC4nKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gPCAxKSB7XG4gICAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA9IHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gKiAxMDA7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gU2FuaXRpemUgdGhlIGlucHV0XG4gICAgICB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPSBwYXJzZUZsb2F0KHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCk7XG4gICAgICB0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvbiA9IHBhcnNlRmxvYXQodGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb24pO1xuICAgICAgdGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlID0gcGFyc2VGbG9hdCh0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUpO1xuICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZSA9ICQudHJpbSh0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlLnRvVXBwZXJDYXNlKCkpO1xuXG4gICAgICBpZiAoIVBBWU1FTlRfRlJFUVVFTkNJRVMuaGFzT3duUHJvcGVydHkodGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5KSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW3BheW1lbnRGcmVxdWVuY3ldIGlzIG5vdCB2YWxpZC4nKTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzLmhhc093blByb3BlcnR5KHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmUpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbY3JlZGl0U2NvcmVdIGlzIG5vdCB2YWxpZC4nKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCA8IE1JTklNVU1fTE9BTikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2xvYW5BbW91bnRdIG11c3QgbWUgYXQgbGVhc3QgMTAwMC4nKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uIDwgTUlOSU1VTV9EVVJBVElPTikge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2xvYW5EdXJhdGlvbl0gbXVzdCBtZSBhdCBsZWFzdCAxLicpO1xuICAgICAgfVxuXG4gICAgICBpZiAoISQuaXNOdW1lcmljKHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtzZXJ2aWNlRmVlXSBpcyBub3QgdmFsaWQuJyk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFNob3cgdGhlIHJlc3VsdHMgaW4gdGhlIERPTS5cbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5fZGlzcGxheVNlbGVjdGVkVmFsdWVzKCk7XG4gICAgICB0aGlzLl9kaXNwbGF5UmVzdWx0cygpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBTaG93IHRoZSBzZWxlY3RlZCB2YWx1ZXMgaW4gdGhlIERPTS5cbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIF9kaXNwbGF5U2VsZWN0ZWRWYWx1ZXM6IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIERpc3BsYXkgdGhlIHNlbGVjdGVkIGxvYW4gYW1vdW50XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWRBbW91bnQpLmh0bWwoXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50KVxuICAgICAgKTtcblxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgbG9hbiBkdXJhdGlvblxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkRHVyYXRpb24pLmh0bWwoXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBjcmVkaXQgc2NvcmVcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZWxlY3RlZFNjb3JlKS5odG1sKFxuICAgICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBwYXltZW50IGZyZXF1ZW5jeVxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSkuaHRtbChcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5XG4gICAgICApO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBEaXNwbGF5IHRoZSByZXN1bHRzIGZvciB0aGUgY3VycmVudCB2YWx1ZXMuXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICBfZGlzcGxheVJlc3VsdHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIERpc3BsYXkgdGhlIGxvYW4gdG90YWxcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuVG90YWxTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9sb2FuVG90YWwoKSlcbiAgICAgICk7XG5cbiAgICAgIC8vIERpc3BsYXkgdGhlIGxvYW4gcGVyaW9kaWMgcGF5bWVudFxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnBheW1lbnRTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9QTVQoKSlcbiAgICAgICk7XG5cbiAgICAgIC8vIERpc3BsYXkgdGhlIGludGVyZXN0IHRvdGFsIGFtb3VudFxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmludGVyZXN0VG90YWxTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9pbnRlcmVzdFRvdGFsKCkpXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSB0YXggdG90YWwgYW1vdW50XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MudGF4VG90YWxTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl90YXhUb3RhbCgpKVxuICAgICAgKTtcblxuICAgICAgLy8gRGlzcGxheSB0aGUgYW5udWFsIHRvdGFsIGNvc3RcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy50b3RhbEFubnVhbENvc3RTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9QZXJjZW50YWdlKHRoaXMuX0NBVCgpKVxuICAgICAgKTtcblxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VydmljZSBmZWUgaWYgYW55XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZVNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX3NlcnZpY2VGZWVXaXRoVkFUKCkpXG4gICAgICApO1xuXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MubG9hbkdyYW5kVG90YWxTZWxlY3RvcikuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9ncmFuZFRvdGFsKCkpXG4gICAgICApO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSdW4gdGhlIGluaXQgbWV0aG9kIGFnYWluIHdpdGggdGhlIHByb3ZpZGVkIG9wdGlvbnMuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGFyZ3NcbiAgICAgKi9cbiAgICB1cGRhdGU6IGZ1bmN0aW9uIChhcmdzKSB7XG4gICAgICB0aGlzLnNldHRpbmdzID0gJC5leHRlbmQoe30sIHRoaXMuX2RlZmF1bHRzLCB0aGlzLnNldHRpbmdzLCBhcmdzKTtcbiAgICAgIHRoaXMuaW5pdCgpO1xuICAgICAgdGhpcy4kZWwudHJpZ2dlcignbG9hbjp1cGRhdGUnKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2VuZXJhdGUgdGhlIHJlc3VsdHMgYXMgYW4gYXJyYXkgb2Ygb2JqZWN0cyxcbiAgICAgKiBlYWNoIG9iamVjdCBjb250YWlucyB0aGUgdmFsdWVzIGZvciBlYWNoIHBlcmlvZC5cbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cbiAgICAgKi9cbiAgICBfcmVzdWx0czogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGJhbGFuY2UgPSB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQ7XG4gICAgICB2YXIgaW5pdGlhbCA9IHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudDtcbiAgICAgIHZhciBpbnRlcmVzdFJhdGUgPSB0aGlzLl9pbnRlcmVzdFJhdGUoKTtcbiAgICAgIHZhciBWQVQgPSB0aGlzLl92YWx1ZUFkZGVkVGF4KCk7XG4gICAgICB2YXIgcGF5bWVudCA9IHRoaXMuX1BNVCgpO1xuICAgICAgdmFyIG51bWJlck9mUGF5bWVudHMgPSB0aGlzLl9udW1iZXJPZlBheW1lbnRzKCk7XG4gICAgICB2YXIgcmVzdWx0cyA9IFtdO1xuXG4gICAgICAvLyBXZSBsb29wIG92ZXIgdGhlIG51bWJlciBvZiBwYXltZW50cyBhbmQgZWFjaCB0aW1lXG4gICAgICAvLyB3ZSBleHRyYWN0IHRoZSBpbmZvcm1hdGlvbiB0byBidWlsZCB0aGUgcGVyaW9kXG4gICAgICAvLyB0aGF0IHdpbGwgYmUgYXBwZW5kZWQgdG8gdGhlIHJlc3VsdHMgYXJyYXkuXG4gICAgICBmb3IgKHZhciBwYXltZW50TnVtYmVyID0gMDsgcGF5bWVudE51bWJlciA8IG51bWJlck9mUGF5bWVudHM7IHBheW1lbnROdW1iZXIrKykge1xuICAgICAgICB2YXIgaW50ZXJlc3QgPSBiYWxhbmNlICogaW50ZXJlc3RSYXRlO1xuICAgICAgICB2YXIgdGF4ZXNQYWlkID0gYmFsYW5jZSAqIGludGVyZXN0UmF0ZSAqIFZBVDtcbiAgICAgICAgdmFyIHByaW5jaXBhbCA9IHBheW1lbnQgLSBpbnRlcmVzdCAtIHRheGVzUGFpZDtcblxuICAgICAgICAvLyB1cGRhdGUgaW5pdGlhbCBiYWxhbmNlIGZvciBuZXh0IGl0ZXJhdGlvblxuICAgICAgICBpbml0aWFsID0gYmFsYW5jZTtcblxuICAgICAgICAvLyB1cGRhdGUgZmluYWwgYmFsYW5jZSBmb3IgdGhlIG5leHQgaXRlcmF0aW9uLlxuICAgICAgICBiYWxhbmNlID0gYmFsYW5jZSAtIHByaW5jaXBhbDtcblxuICAgICAgICByZXN1bHRzLnB1c2goe1xuICAgICAgICAgIGluaXRpYWw6IGluaXRpYWwsXG4gICAgICAgICAgcHJpbmNpcGFsOiBwcmluY2lwYWwsXG4gICAgICAgICAgaW50ZXJlc3Q6IGludGVyZXN0LFxuICAgICAgICAgIHRheDogdGF4ZXNQYWlkLFxuICAgICAgICAgIHBheW1lbnQ6IHBheW1lbnQsXG4gICAgICAgICAgYmFsYW5jZTogYmFsYW5jZVxuICAgICAgICB9KVxuICAgICAgfTtcblxuICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdlbmVyYXRlIHRoZSBhbW9ydGl6YXRpb24gc2NoZWR1bGUuXG4gICAgICogQHJldHVybiB7QXJyYXl9XG4gICAgICovXG4gICAgc2NoZWR1bGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAkLm1hcCh0aGlzLl9yZXN1bHRzKCksIGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGluaXRpYWw6IHRoaXMuX3RvTW9uZXkodmFsdWUuaW5pdGlhbCksXG4gICAgICAgICAgcHJpbmNpcGFsOiB0aGlzLl90b01vbmV5KHZhbHVlLnByaW5jaXBhbCksXG4gICAgICAgICAgaW50ZXJlc3Q6IHRoaXMuX3RvTW9uZXkodmFsdWUuaW50ZXJlc3QpLFxuICAgICAgICAgIHRheDogdGhpcy5fdG9Nb25leSh2YWx1ZS50YXgpLFxuICAgICAgICAgIHBheW1lbnQ6IHRoaXMuX3RvTW9uZXkodmFsdWUucGF5bWVudCksXG4gICAgICAgICAgYmFsYW5jZTogdGhpcy5fdG9Nb25leSh2YWx1ZS5iYWxhbmNlKVxuICAgICAgICB9XG4gICAgICB9LmJpbmQodGhpcykpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIGNyZWRpdCByYXRlcyBiZWluZyB1c2VkLlxuICAgICAqIEByZXR1cm4ge09iamVjdH1cbiAgICAgKi9cbiAgICBjcmVkaXRSYXRlczogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXM7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgY3JlZGl0IHJhdGUgY29ycmVzcG9uZGluZyB0byB0aGUgY3VycmVudCBjcmVkaXQgc2NvcmUuXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9hbm51YWxJbnRlcmVzdFJhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLmhhc093blByb3BlcnR5KCdpbnRlcmVzdFJhdGUnKSkge1xuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5pbnRlcmVzdFJhdGUgPD0gMSkge1xuICAgICAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLmludGVyZXN0UmF0ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5pbnRlcmVzdFJhdGUpIC8gMTAwO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1t0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlXSAvIDEwMDtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0IHRoZSBwZXJpb2RpYyBpbnRlcmVzdCByYXRlLlxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX2ludGVyZXN0UmF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2FubnVhbEludGVyZXN0UmF0ZSgpIC8gdGhpcy5fcGF5bWVudEZyZXF1ZW5jeSgpO1xuICAgIH0sXG5cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIHBlcmlvZGljIHBheW1lbnQgZnJlcXVlbmN5XG4gICAgICogQHJldHVybnMge051bWJlcn1cbiAgICAgKi9cbiAgICBfcGF5bWVudEZyZXF1ZW5jeTogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIFBBWU1FTlRfRlJFUVVFTkNJRVNbdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5XTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyBudW1iZXIgb2YgcGF5bWVudHMgZm9yIHRoZSBsb2FuLlxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX251bWJlck9mUGF5bWVudHM6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBkdXJhdGlvbkluWWVhcnMgPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb24pIC8gMTI7XG5cbiAgICAgIHJldHVybiBNYXRoLmZsb29yKGR1cmF0aW9uSW5ZZWFycyAqIFBBWU1FTlRfRlJFUVVFTkNJRVNbdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5XSk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENhbGN1bGF0ZXMgdGhlIHRvdGFsIGNvc3Qgb2YgdGhlIGxvYW4uXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9sb2FuVG90YWw6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9QTVQoKSAqIHRoaXMuX251bWJlck9mUGF5bWVudHMoKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2FsY3VsYXRlIHRoZSBtb250aGx5IGFtb3J0aXplZCBsb2FuIHBheW1lbnRzLlxuICAgICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ29tcG91bmRfaW50ZXJlc3QjTW9udGhseV9hbW9ydGl6ZWRfbG9hbl9vcl9tb3J0Z2FnZV9wYXltZW50c1xuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfUE1UOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgaSA9IHRoaXMuX2ludGVyZXN0UmF0ZSgpO1xuICAgICAgdmFyIEwgPSB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQ7XG4gICAgICB2YXIgbiA9IHRoaXMuX251bWJlck9mUGF5bWVudHMoKTtcblxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MudmFsdWVBZGRlZFRheCAhPT0gMCkge1xuICAgICAgICBpID0gKDEgKyB0aGlzLl92YWx1ZUFkZGVkVGF4KCkpICogaTsgLy8gaW50ZXJlc3QgcmF0ZSB3aXRoIHRheFxuICAgICAgfVxuXG4gICAgICByZXR1cm4gKEwgKiBpKSAvICgxIC0gTWF0aC5wb3coMSArIGksIC1uKSk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENhbGN1bGF0ZSB0aGUgdG90YWwgaW50ZXJlc3QgZm9yIHRoZSBsb2FuLlxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX2ludGVyZXN0VG90YWw6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciB0b3RhbCA9IDA7XG4gICAgICAkLmVhY2godGhpcy5fcmVzdWx0cygpLCBmdW5jdGlvbiAoaW5kZXgsIHZhbHVlKSB7XG4gICAgICAgIHRvdGFsICs9IHZhbHVlLmludGVyZXN0O1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gdG90YWw7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENhbGN1bGF0ZSB0aGUgdmFsdWUgYWRkZWQgdGF4IHRvdGFsIGZvciB0aGUgbG9hbi5cbiAgICAgKiBAcmV0dXJucyB7TnVtYmVyfVxuICAgICAqL1xuICAgIF90YXhUb3RhbDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRvdGFsID0gMDtcbiAgICAgICQuZWFjaCh0aGlzLl9yZXN1bHRzKCksIGZ1bmN0aW9uIChpbmRleCwgdmFsdWUpIHtcbiAgICAgICAgdG90YWwgKz0gdmFsdWUudGF4O1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gdG90YWw7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJldHVybiB0aGUgbG9hbiBmZWVzIGFuZCBjb21taXNzaW9ucyB0b3RhbC5cbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX3NlcnZpY2VGZWU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBzZXJ2aWNlRmVlID0gdGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlO1xuXG4gICAgICAvLyBpZiB0aGUgc2VydmljZSBmZWUgaXMgZ3JlYXRlciB0aGFuIDEgdGhlbiB0aGVcbiAgICAgIC8vIHZhbHVlIG11c3QgYmUgY29udmVydGVkIHRvIGRlY2ltYWxzIGZpcnN0LlxuICAgICAgaWYgKHNlcnZpY2VGZWUgPiAxKSB7XG4gICAgICAgIHNlcnZpY2VGZWUgPSBzZXJ2aWNlRmVlIC8gMTAwO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50ICogc2VydmljZUZlZTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSBsb2FuIGZlZXMgYW5kIGNvbW1pc3Npb25zIHRvdGFsIHdpdGggVkFUIGluY2x1ZGVkLlxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfc2VydmljZUZlZVdpdGhWQVQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9zZXJ2aWNlRmVlKCkgKiAodGhpcy5fdmFsdWVBZGRlZFRheCgpICsgMSlcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2FsY3VsYXRlcyB0aGUgdG90YWwgY29zdCBvZiB0aGUgbG9hbiBpbmNsdWRpbmcgdGhlIHNlcnZpY2UgZmVlLlxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfZ3JhbmRUb3RhbDogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2xvYW5Ub3RhbCgpICsgdGhpcy5fc2VydmljZUZlZVdpdGhWQVQoKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSB0b3RhbCBhbm51YWwgY29zdCAoQ0FUKVxuICAgICAqIEBzZWUgaHR0cDovL3d3dy5iYW54aWNvLm9yZy5teC9DQVRcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX0NBVDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIElSUiA9IHRoaXMuX0lSUih0aGlzLl9jYXNoRmxvdygpKTtcbiAgICAgIHZhciBwZXJpb2RzID0gdGhpcy5fcGF5bWVudEZyZXF1ZW5jeSgpO1xuXG4gICAgICByZXR1cm4gTWF0aC5wb3coMSArIElSUiwgcGVyaW9kcykgLSAxO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIGFycmF5IHdpdGggYSBzZXJpZXMgb2YgY2FzaCBmbG93cyBmb3IgdGhlIGN1cnJlbnQgbG9hbi5cbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cbiAgICAgKi9cbiAgICBfY2FzaEZsb3c6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciByZXN1bHRzID0gdGhpcy5fcmVzdWx0cygpO1xuICAgICAgdmFyIGNhc2hGbG93ID0gW3RoaXMuX3NlcnZpY2VGZWUoKSAtIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudF07XG5cbiAgICAgICQuZWFjaChyZXN1bHRzLCBmdW5jdGlvbiAoaW5kZXgsIHBlcmlvZCkge1xuICAgICAgICBjYXNoRmxvdy5wdXNoKHBlcmlvZC5wYXltZW50IC0gcGVyaW9kLnRheCk7XG4gICAgICB9LmJpbmQodGhpcykpO1xuXG4gICAgICByZXR1cm4gY2FzaEZsb3c7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIGludGVybmFsIHJhdGUgb2YgcmV0dXJuIGZvciBhIHNlcmllcyBvZiBjYXNoIGZsb3dzIHJlcHJlc2VudGVkIGJ5IHRoZSBudW1iZXJzIGluIHZhbHVlcy5cbiAgICAgKiBAcGFyYW0gIHtBcnJheX0gdmFsdWVzXG4gICAgICogQHBhcmFtICB7TnVtYmVyfSBndWVzc1xuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfSVJSOiBmdW5jdGlvbiAodmFsdWVzLCBndWVzcykge1xuICAgICAgZ3Vlc3MgPSBndWVzcyB8fCAwO1xuXG4gICAgICAvLyBDYWxjdWxhdGVzIHRoZSByZXN1bHRpbmcgYW1vdW50XG4gICAgICB2YXIgaXJyUmVzdWx0ID0gZnVuY3Rpb24gKHZhbHVlcywgZGF0ZXMsIHJhdGUpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IHZhbHVlc1swXTtcblxuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIHJlc3VsdCArPSB2YWx1ZXNbaV0gLyBNYXRoLnBvdyhyYXRlICsgMSwgKGRhdGVzW2ldIC0gZGF0ZXNbMF0pIC8gMzY1KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICB9O1xuXG4gICAgICAvLyBDYWxjdWxhdGVzIHRoZSBmaXJzdCBkZXJpdmF0aW9uXG4gICAgICB2YXIgaXJyUmVzdWx0RGVyaXZhdGl2ZSA9IGZ1bmN0aW9uICh2YWx1ZXMsIGRhdGVzLCByYXRlKSB7XG4gICAgICAgIHZhciByZXN1bHQgPSAwO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgdmFyIGZyYWMgPSAoZGF0ZXNbaV0gLSBkYXRlc1swXSkgLyAzNjU7XG4gICAgICAgICAgcmVzdWx0IC09IGZyYWMgKiB2YWx1ZXNbaV0gLyBNYXRoLnBvdyhyYXRlICsgMSwgZnJhYyArIDEpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH07XG5cbiAgICAgIC8vIEluaXRpYWxpemUgZGF0ZXMgYW5kIGNoZWNrIHRoYXQgdmFsdWVzIGNvbnRhaW5zIGF0XG4gICAgICAvLyBsZWFzdCBvbmUgcG9zaXRpdmUgdmFsdWUgYW5kIG9uZSBuZWdhdGl2ZSB2YWx1ZVxuICAgICAgdmFyIGRhdGVzID0gW107XG4gICAgICB2YXIgcG9zaXRpdmUgPSBmYWxzZTtcbiAgICAgIHZhciBuZWdhdGl2ZSA9IGZhbHNlO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBkYXRlc1tpXSA9IChpID09PSAwKSA/IDAgOiBkYXRlc1tpIC0gMV0gKyAzNjU7XG4gICAgICAgIGlmICh2YWx1ZXNbaV0gPiAwKSBwb3NpdGl2ZSA9IHRydWU7XG4gICAgICAgIGlmICh2YWx1ZXNbaV0gPCAwKSBuZWdhdGl2ZSA9IHRydWU7XG4gICAgICB9XG5cbiAgICAgIGlmICghcG9zaXRpdmUgfHwgIW5lZ2F0aXZlKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICAnRXJyb3IgdGhlIHZhbHVlcyBkb2VzIG5vdCBjb250YWluIGF0IGxlYXN0IG9uZSBwb3NpdGl2ZSB2YWx1ZSBhbmQgb25lIG5lZ2F0aXZlIHZhbHVlJ1xuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICAvLyBJbml0aWFsaXplIGd1ZXNzIGFuZCByZXN1bHRSYXRlXG4gICAgICBndWVzcyA9IChndWVzcyA9PT0gdW5kZWZpbmVkKSA/IDAuMSA6IGd1ZXNzO1xuICAgICAgdmFyIHJlc3VsdFJhdGUgPSBndWVzcztcblxuICAgICAgLy8gU2V0IG1heGltdW0gZXBzaWxvbiBmb3IgZW5kIG9mIGl0ZXJhdGlvblxuICAgICAgdmFyIGVwc01heCA9IDFlLTEwO1xuXG4gICAgICAvLyBJbXBsZW1lbnQgTmV3dG9uJ3MgbWV0aG9kXG4gICAgICB2YXIgbmV3UmF0ZSwgZXBzUmF0ZSwgcmVzdWx0VmFsdWU7XG4gICAgICB2YXIgY29udExvb3AgPSB0cnVlO1xuXG4gICAgICBkbyB7XG4gICAgICAgIHJlc3VsdFZhbHVlID0gaXJyUmVzdWx0KHZhbHVlcywgZGF0ZXMsIHJlc3VsdFJhdGUpO1xuICAgICAgICBuZXdSYXRlID0gcmVzdWx0UmF0ZSAtIHJlc3VsdFZhbHVlIC8gaXJyUmVzdWx0RGVyaXZhdGl2ZSh2YWx1ZXMsIGRhdGVzLCByZXN1bHRSYXRlKTtcbiAgICAgICAgZXBzUmF0ZSA9IE1hdGguYWJzKG5ld1JhdGUgLSByZXN1bHRSYXRlKTtcbiAgICAgICAgcmVzdWx0UmF0ZSA9IG5ld1JhdGU7XG4gICAgICAgIGNvbnRMb29wID0gKGVwc1JhdGUgPiBlcHNNYXgpICYmIChNYXRoLmFicyhyZXN1bHRWYWx1ZSkgPiBlcHNNYXgpO1xuICAgICAgfSB3aGlsZSAoY29udExvb3ApO1xuXG4gICAgICAvLyBSZXR1cm4gaW50ZXJuYWwgcmF0ZSBvZiByZXR1cm5cbiAgICAgIHJldHVybiByZXN1bHRSYXRlO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIHZhbHVlIGFkZGVkIHRheCBpbiBkZWNpbWFscy5cbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX3ZhbHVlQWRkZWRUYXg6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciB0YXggPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy52YWx1ZUFkZGVkVGF4IHx8IDApO1xuXG4gICAgICAvLyBpZiB0YXggaXMgZ3JlYXRlciB0aGFuIDEgbWVhbnMgdGhlIHZhbHVlXG4gICAgICAvLyBtdXN0IGJlIGNvbnZlcnRlZCB0byBkZWNpbWFscyBmaXJzdC5cbiAgICAgIHJldHVybiAodGF4ID4gMSkgPyB0YXggLyAxMDAgOiB0YXg7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnQgbnVtZXJpYyBmb3JtYXQgdG8gbW9uZXkgZm9ybWF0LlxuICAgICAqIEBwYXJhbSAge051bWJlcn0gbnVtZXJpY1xuICAgICAqIEByZXR1cm4ge1N0cmluZ31cbiAgICAgKi9cbiAgICBfdG9Nb25leTogZnVuY3Rpb24gKG51bWVyaWMpIHtcbiAgICAgIGlmICh0eXBlb2YgbnVtZXJpYyA9PSAnc3RyaW5nJykge1xuICAgICAgICBudW1lcmljID0gcGFyc2VGbG9hdChudW1lcmljKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuICdBRUQgJyArIG51bWVyaWMudG9GaXhlZCgyKS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pK1xcLikvZywgJyQxLCcpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0IGZyb20gbW9uZXkgZm9ybWF0IHRvIG51bWVyaWMgZm9ybWF0LlxuICAgICAqIEBwYXJhbSAge1N0cmluZ30gdmFsdWVcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX3RvTnVtZXJpYzogZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICByZXR1cm4gcGFyc2VGbG9hdChcbiAgICAgICAgdmFsdWUudG9TdHJpbmcoKS5yZXBsYWNlKC9bXjAtOVxcLl0rL2csICcnKVxuICAgICAgKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogVG8gY29udmVydCB0aGUgcHJvdmlkZWQgdmFsdWUgdG8gcGVyY2VudCBmb3JtYXQuXG4gICAgICogQHBhcmFtIHtOdW1iZXJ9IG51bWVyaWNcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuICAgIF90b1BlcmNlbnRhZ2U6IGZ1bmN0aW9uIChudW1lcmljKSB7XG4gICAgICByZXR1cm4gKG51bWVyaWMgKiAxMDApLnRvRml4ZWQoMikgKyAnJSc7XG4gICAgfVxuXG4gIH0pO1xuXG4gIC8qKlxuICAgKiBXcmFwcGVyIGFyb3VuZCB0aGUgY29uc3RydWN0b3IgdG8gcHJldmVudCBtdWx0aXBsZSBpbnN0YW50aWF0aW9ucy5cbiAgICovXG4gICQuZm4ubG9hbkNhbGN1bGF0b3IgPSBmdW5jdGlvbiAob3B0aW9ucywgYXJncykge1xuICAgIGlmIChvcHRpb25zID09PSAnc2NoZWR1bGUnKSB7XG4gICAgICByZXR1cm4gdGhpcy5kYXRhKCdwbHVnaW5fbG9hbkNhbGN1bGF0b3InKS5zY2hlZHVsZSgpO1xuICAgIH1cblxuICAgIGlmIChvcHRpb25zID09PSAncmF0ZXMnKSB7XG4gICAgICByZXR1cm4gdGhpcy5kYXRhKCdwbHVnaW5fbG9hbkNhbGN1bGF0b3InKS5jcmVkaXRSYXRlcygpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGluc3RhbmNlID0gJC5kYXRhKHRoaXMsICdwbHVnaW5fbG9hbkNhbGN1bGF0b3InKTtcbiAgICAgIGlmICghaW5zdGFuY2UpIHtcbiAgICAgICAgJC5kYXRhKHRoaXMsICdwbHVnaW5fbG9hbkNhbGN1bGF0b3InLCBuZXcgUGx1Z2luKHRoaXMsIG9wdGlvbnMpKTtcbiAgICAgIH0gZWxzZSBpZiAob3B0aW9ucyA9PT0gJ3VwZGF0ZScpIHtcbiAgICAgICAgcmV0dXJuIGluc3RhbmNlLnVwZGF0ZShhcmdzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxufSkoalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50KTsiXX0=
