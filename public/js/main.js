var url = $('[name="site_url"]').val()
$(function () {
    var url = window.location.pathname,
        urlRegExp = new RegExp(url.replace(/\/$/, '') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
    // now grab every link from the navigation
    $('.navbar-nav li.nav-item a').each(function () {
        // and test its normalized href against the url pathname regexp
        if (url != '/' && urlRegExp.test(this.href.replace(/\/$/, ''))) {
            $("li.active").removeClass("active");
            $(this).closest('li').addClass('active');
        }else{
            
        }
    });
    // var str = location.href.toLowerCase();

    // $(".navbar-nav li.nav-item a.nav-link").each(function () {
    //     if (str.indexOf(this.href.toLowerCase()) > -1) {
    //         $("li.active").removeClass("active");
    //         $(this).parent().addClass("active");
    //     }
    // });
});
/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

// Footer Accordion
if ($(window).width() < 960) {
    $("#accordion h5").click((function () {
        if (false == $(this).next().is(':visible')) {
            $('#accordion ul').slideUp(300);
            $('#accordion h5').removeClass('active');
        }
        $(this).next().slideToggle(300);
        $(this).toggleClass('active');
    }));
    $('#accordion ul:eq(0)').show();
    $('#accordion h5:eq(0)').addClass('active');
};

$((function () {
    $('.row').each((function (i, elem) {
        $(elem).find('.creative-section__content') // Only children of this row
            .matchHeight({
                byRow: false
            }); // Row detection gets confused so disable it
    }));
}));

$(document).ready((function () {

    $('.timeline .owl-carousel').owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            }
        }
    }).find('.owl-item').each((function (i) {
        var attr = $(this).children().attr('data-year');
        var element = $('<span>' + attr + '</span>');
        $('.timeline .owl-carousel .owl-dot').eq(i).append(element);
    }));

    $('.carousel').carousel();

    $('.offer-popup').magnificPopup({
        // type: 'inline',
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function () {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
    $("#autoUpdate").hide();
    $('#checkbox1').change((function () {
        if (this.checked) {
            $('#autoUpdate').show(300);
        } else {
            $('#autoUpdate').hide(200);
        }
    }));

    $('#lightboxMoveFromTop').magnificPopup({
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function () {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

    // Materialize Starts Here
    M.updateTextFields();
    $('select').formSelect();
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();

    // Materialize Ends Here
    // 360 Color change functions
    $('.color-choose input').on('click', (function () {
        var headphonesColor = $(this).attr('data-image');
        $('.active').removeClass('active');
        $('.left-column img[data-image = ' + headphonesColor + ']').addClass('active');
        $(this).addClass('active');
    }));
    $('.exterior__360_action button').on('click', (function () {
        $('.exterior__360_action button.current').removeClass('current');
        $(this).addClass('current');
    }));
    // 360 Color change functions ends
    // Specifications Tabs
    $('ul.tabs li').click((function () {
        var tab_id = $(this).attr('data-tab');
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#" + tab_id).addClass('current');
    }));
    // magnificPopup
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    // scroll to top
    var scrollTop = $(".scrollTop");
    $(window).scroll((function () {
        var topPos = $(this).scrollTop();
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");
        } else {
            $(scrollTop).css("opacity", "0");
        }
    }));
    $(scrollTop).click((function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    }));
    // scroll to top Ends
}));
// Slick Slider Starts
$('.mainbanner').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    fade: true,
    arrows: false
});
$('.sales-slider').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    fade: true,
    arrows: false
});

$('.cars-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: false,
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }, {
        breakpoint: 480,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]
});
// Slick Slider Ends

// Menu Script starts here !!!
// Menu Script Ends here !!!
// Photo Gallery Starts here !!!
$('.gallery-link').on('click', (function () {
    $(this).next().magnificPopup('open');
}));
$('.gallery').each((function () {
    $(this).magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true
        },
        fixedContentPos: false
    });
}));
// Photo Gallery Ends here !!!
$(".car-colors span").click((function () {
    $(".car-colors span").removeClass("active");
    $(this).addClass("active");
    $(".car-button").css("color", $(this).attr("data-color"));
    $(".car-pic").css({ "background-image": 'url(' + $(this).attr("data-pic") + ')' });
}));

$('#cs35-white').click((function () {
    $('#cs35').reel('images', url + '/images/360/cs35/white/####.png');
}));
$('#cs35-red').click((function () {
    $('#cs35').reel('images', url + '/images/360/cs35/red/####.png');
}));
$('#cs35-blue').click((function () {
    $('#cs35').reel('images', url + '/images/360/cs35/blue/####.png');
}));
$('#cs35-grey').click((function () {
    $('#cs35').reel('images', url + '/images/360/cs35/grey/####.png');
}));
$('#cs35-brown').click((function () {
    $('#cs35').reel('images', url + '/images/360/cs35/brown/####.png');
}));
$.reel.def.indicator = 5;

// car details accordion

$(".cardetails-accordion .car-details__spec_head").click((function () {
    if (false == $(this).next().is(':visible')) {
        $('.cardetails-accordion .cardetails').slideUp(300);
        $('.car-details__spec_head').removeClass('active');
    }
    $(this).next().slideToggle(300);
    $(this).toggleClass('active');
}));
$('.cardetails-accordion .cardetails:eq(0)').addClass('active');
$('.car-details__spec_head:eq(0)').addClass('active');

(function ($) {

    // Accepts arguments as strings
    $calculator = $('#widget').loanCalculator({
        loanAmount: 'AED 1,000.00',
        loanDuration: '12',
        valueAddedTax: '0',
        serviceFee: '0',
        paymentFrequency: 'monthly'
    });
})(jQuery);

$((function () {
    var $el = $('.background__paralax');
    $(window).on('scroll', (function () {
        var scroll = $(document).scrollTop();
        $el.css({
            'background-position': '50% ' + -.06 * scroll + 'px'
        });
    }));
}));

const menu = document.querySelector('.mobile-menu');

menu.addEventListener('click', (function () {
    if (menu.classList.contains('open')) {
        menu.classList.remove('open');
        menu.classList.add('close');
    } else {
        menu.classList.remove('close');
        menu.classList.add('open');
    }
}));

var nonLinearSlider = document.getElementById('pricerange');
if (nonLinearSlider != null) {
    noUiSlider.create(nonLinearSlider, {
        connect: true,
        behaviour: 'tap',
        start: [parseInt(nonLinearSlider.getAttribute('data-min')), parseInt(nonLinearSlider.getAttribute('data-max'))],
        step: 1,
        range: {
            // Starting at 500, step the value by 500,
            // until 4000 is reached. From there, step by 1000.
            'min': [parseInt(nonLinearSlider.getAttribute('data-min'))],
            'max': [parseInt(nonLinearSlider.getAttribute('data-max'))]
        }
    });
    var nodes = [document.getElementById('lower-value'), // 0
    document.getElementById('upper-value') // 1
    ];

    // Display the slider value and how far the handle moved
    // from the left edge of the slider.
    nonLinearSlider.noUiSlider.on('update', (function (values, handle, unencoded, isTap, positions) {
        nodes[handle].innerHTML = values[handle] * 5 + 'k';
    }));
}

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsIndpZHRoIiwiY2xpY2siLCJuZXh0IiwiaXMiLCJzbGlkZVVwIiwicmVtb3ZlQ2xhc3MiLCJzbGlkZVRvZ2dsZSIsInRvZ2dsZUNsYXNzIiwic2hvdyIsImFkZENsYXNzIiwiZWFjaCIsImkiLCJlbGVtIiwiZmluZCIsIm1hdGNoSGVpZ2h0IiwiYnlSb3ciLCJkb2N1bWVudCIsInJlYWR5Iiwib3dsQ2Fyb3VzZWwiLCJsb29wIiwibWFyZ2luIiwibmF2IiwicmVzcG9uc2l2ZSIsIml0ZW1zIiwiYXR0ciIsImNoaWxkcmVuIiwiZWxlbWVudCIsImVxIiwiYXBwZW5kIiwiY2Fyb3VzZWwiLCJtYWduaWZpY1BvcHVwIiwicmVtb3ZhbERlbGF5IiwiY2FsbGJhY2tzIiwiYmVmb3JlT3BlbiIsInN0IiwibWFpbkNsYXNzIiwiZWwiLCJtaWRDbGljayIsImhpZGUiLCJjaGFuZ2UiLCJjaGVja2VkIiwiTSIsInVwZGF0ZVRleHRGaWVsZHMiLCJmb3JtU2VsZWN0IiwiZGF0ZXBpY2tlciIsInRpbWVwaWNrZXIiLCJvbiIsImhlYWRwaG9uZXNDb2xvciIsInRhYl9pZCIsImRpc2FibGVPbiIsInR5cGUiLCJwcmVsb2FkZXIiLCJmaXhlZENvbnRlbnRQb3MiLCJzY3JvbGxUb3AiLCJzY3JvbGwiLCJ0b3BQb3MiLCJjc3MiLCJhbmltYXRlIiwic2xpY2siLCJhdXRvcGxheSIsImF1dG9wbGF5U3BlZWQiLCJmYWRlIiwiYXJyb3dzIiwiaW5maW5pdGUiLCJzbGlkZXNUb1Nob3ciLCJzbGlkZXNUb1Njcm9sbCIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsImRvdHMiLCJkZWxlZ2F0ZSIsImdhbGxlcnkiLCJlbmFibGVkIiwibmF2aWdhdGVCeUltZ0NsaWNrIiwicmVlbCIsImRlZiIsImluZGljYXRvciIsIiRjYWxjdWxhdG9yIiwibG9hbkNhbGN1bGF0b3IiLCJsb2FuQW1vdW50IiwibG9hbkR1cmF0aW9uIiwidmFsdWVBZGRlZFRheCIsInNlcnZpY2VGZWUiLCJwYXltZW50RnJlcXVlbmN5IiwialF1ZXJ5IiwiJGVsIiwibWVudSIsInF1ZXJ5U2VsZWN0b3IiLCJhZGRFdmVudExpc3RlbmVyIiwiY2xhc3NMaXN0IiwiY29udGFpbnMiLCJyZW1vdmUiLCJhZGQiLCJub25MaW5lYXJTbGlkZXIiLCJnZXRFbGVtZW50QnlJZCIsIm5vVWlTbGlkZXIiLCJjcmVhdGUiLCJjb25uZWN0IiwiYmVoYXZpb3VyIiwic3RhcnQiLCJzdGVwIiwicmFuZ2UiLCJub2RlcyIsInZhbHVlcyIsImhhbmRsZSIsInVuZW5jb2RlZCIsImlzVGFwIiwicG9zaXRpb25zIiwiaW5uZXJIVE1MIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0EsSUFBQUEsRUFBQUMsTUFBQSxFQUFBQyxLQUFBLEtBQUEsR0FBQSxFQUFBO0FBQ0FGLE1BQUEsZUFBQSxFQUFBRyxLQUFBLENBQUEsWUFBQTtBQUNBLFlBQUEsU0FBQUgsRUFBQSxJQUFBLEVBQUFJLElBQUEsR0FBQUMsRUFBQSxDQUFBLFVBQUEsQ0FBQSxFQUFBO0FBQ0FMLGNBQUEsZUFBQSxFQUFBTSxPQUFBLENBQUEsR0FBQTtBQUNBTixjQUFBLGVBQUEsRUFBQU8sV0FBQSxDQUFBLFFBQUE7QUFDQTtBQUNBUCxVQUFBLElBQUEsRUFBQUksSUFBQSxHQUFBSSxXQUFBLENBQUEsR0FBQTtBQUNBUixVQUFBLElBQUEsRUFBQVMsV0FBQSxDQUFBLFFBQUE7QUFDQSxLQVBBO0FBUUFULE1BQUEscUJBQUEsRUFBQVUsSUFBQTtBQUNBVixNQUFBLHFCQUFBLEVBQUFXLFFBQUEsQ0FBQSxRQUFBO0FBQ0E7O0FBRUFYLEVBQUEsWUFBQTtBQUNBQSxNQUFBLE1BQUEsRUFBQVksSUFBQSxDQUFBLFVBQUFDLENBQUEsRUFBQUMsSUFBQSxFQUFBO0FBQ0FkLFVBQUFjLElBQUEsRUFDQUMsSUFEQSxDQUNBLDRCQURBLEVBQ0E7QUFEQSxTQUVBQyxXQUZBLENBRUE7QUFDQUMsbUJBQUE7QUFEQSxTQUZBLEVBREEsQ0FLQTtBQUNBLEtBTkE7QUFPQSxDQVJBOztBQVVBakIsRUFBQWtCLFFBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7O0FBRUFuQixNQUFBLHlCQUFBLEVBQUFvQixXQUFBLENBQUE7QUFDQUMsY0FBQSxLQURBO0FBRUFDLGdCQUFBLENBRkE7QUFHQUMsYUFBQSxLQUhBO0FBSUFDLG9CQUFBO0FBQ0EsZUFBQTtBQUNBQyx1QkFBQTtBQURBO0FBREE7QUFKQSxLQUFBLEVBU0FWLElBVEEsQ0FTQSxXQVRBLEVBVUFILElBVkEsQ0FVQSxVQUFBQyxDQUFBLEVBQUE7QUFDQSxZQUFBYSxPQUFBMUIsRUFBQSxJQUFBLEVBQUEyQixRQUFBLEdBQUFELElBQUEsQ0FBQSxXQUFBLENBQUE7QUFDQSxZQUFBRSxVQUFBNUIsRUFBQSxXQUFBMEIsSUFBQSxHQUFBLFNBQUEsQ0FBQTtBQUNBMUIsVUFBQSxrQ0FBQSxFQUFBNkIsRUFBQSxDQUFBaEIsQ0FBQSxFQUFBaUIsTUFBQSxDQUFBRixPQUFBO0FBQ0EsS0FkQTs7QUFtQkE1QixNQUFBLFdBQUEsRUFBQStCLFFBQUE7O0FBRUEvQixNQUFBLGNBQUEsRUFBQWdDLGFBQUEsQ0FBQTtBQUNBO0FBQ0FDLHNCQUFBLEdBRkEsRUFFQTtBQUNBQyxtQkFBQTtBQUNBQyx3QkFBQSxZQUFBO0FBQ0EscUJBQUFDLEVBQUEsQ0FBQUMsU0FBQSxHQUFBLEtBQUFELEVBQUEsQ0FBQUUsRUFBQSxDQUFBWixJQUFBLENBQUEsYUFBQSxDQUFBO0FBQ0E7QUFIQSxTQUhBO0FBUUFhLGtCQUFBLElBUkEsQ0FRQTtBQVJBLEtBQUE7QUFVQXZDLE1BQUEsYUFBQSxFQUFBd0MsSUFBQTtBQUNBeEMsTUFBQSxZQUFBLEVBQUF5QyxNQUFBLENBQUEsWUFBQTtBQUNBLFlBQUEsS0FBQUMsT0FBQSxFQUFBO0FBQ0ExQyxjQUFBLGFBQUEsRUFBQVUsSUFBQSxDQUFBLEdBQUE7QUFDQSxTQUZBLE1BRUE7QUFDQVYsY0FBQSxhQUFBLEVBQUF3QyxJQUFBLENBQUEsR0FBQTtBQUNBO0FBQ0EsS0FOQTs7QUFRQXhDLE1BQUEsc0JBQUEsRUFBQWdDLGFBQUEsQ0FBQTtBQUNBQyxzQkFBQSxHQURBLEVBQ0E7QUFDQUMsbUJBQUE7QUFDQUMsd0JBQUEsWUFBQTtBQUNBLHFCQUFBQyxFQUFBLENBQUFDLFNBQUEsR0FBQSxLQUFBRCxFQUFBLENBQUFFLEVBQUEsQ0FBQVosSUFBQSxDQUFBLGFBQUEsQ0FBQTtBQUNBO0FBSEEsU0FGQTtBQU9BYSxrQkFBQSxJQVBBLENBT0E7QUFQQSxLQUFBOztBQVVBO0FBQ0FJLE1BQUFDLGdCQUFBO0FBQ0E1QyxNQUFBLFFBQUEsRUFBQTZDLFVBQUE7QUFDQTdDLE1BQUEsYUFBQSxFQUFBOEMsVUFBQTtBQUNBOUMsTUFBQSxhQUFBLEVBQUErQyxVQUFBOztBQUdBO0FBQ0E7QUFDQS9DLE1BQUEscUJBQUEsRUFBQWdELEVBQUEsQ0FBQSxPQUFBLEVBQUEsWUFBQTtBQUNBLFlBQUFDLGtCQUFBakQsRUFBQSxJQUFBLEVBQUEwQixJQUFBLENBQUEsWUFBQSxDQUFBO0FBQ0ExQixVQUFBLFNBQUEsRUFBQU8sV0FBQSxDQUFBLFFBQUE7QUFDQVAsVUFBQSxtQ0FBQWlELGVBQUEsR0FBQSxHQUFBLEVBQUF0QyxRQUFBLENBQUEsUUFBQTtBQUNBWCxVQUFBLElBQUEsRUFBQVcsUUFBQSxDQUFBLFFBQUE7QUFDQSxLQUxBO0FBTUFYLE1BQUEsOEJBQUEsRUFBQWdELEVBQUEsQ0FBQSxPQUFBLEVBQUEsWUFBQTtBQUNBaEQsVUFBQSxzQ0FBQSxFQUFBTyxXQUFBLENBQUEsU0FBQTtBQUNBUCxVQUFBLElBQUEsRUFBQVcsUUFBQSxDQUFBLFNBQUE7QUFDQSxLQUhBO0FBSUE7QUFDQTtBQUNBWCxNQUFBLFlBQUEsRUFBQUcsS0FBQSxDQUFBLFlBQUE7QUFDQSxZQUFBK0MsU0FBQWxELEVBQUEsSUFBQSxFQUFBMEIsSUFBQSxDQUFBLFVBQUEsQ0FBQTtBQUNBMUIsVUFBQSxZQUFBLEVBQUFPLFdBQUEsQ0FBQSxTQUFBO0FBQ0FQLFVBQUEsY0FBQSxFQUFBTyxXQUFBLENBQUEsU0FBQTs7QUFFQVAsVUFBQSxJQUFBLEVBQUFXLFFBQUEsQ0FBQSxTQUFBO0FBQ0FYLFVBQUEsTUFBQWtELE1BQUEsRUFBQXZDLFFBQUEsQ0FBQSxTQUFBO0FBQ0EsS0FQQTtBQVFBO0FBQ0FYLE1BQUEsNENBQUEsRUFBQWdDLGFBQUEsQ0FBQTtBQUNBbUIsbUJBQUEsR0FEQTtBQUVBQyxjQUFBLFFBRkE7QUFHQWYsbUJBQUEsVUFIQTtBQUlBSixzQkFBQSxHQUpBO0FBS0FvQixtQkFBQSxLQUxBO0FBTUFDLHlCQUFBO0FBTkEsS0FBQTs7QUFTQTtBQUNBLFFBQUFDLFlBQUF2RCxFQUFBLFlBQUEsQ0FBQTtBQUNBQSxNQUFBQyxNQUFBLEVBQUF1RCxNQUFBLENBQUEsWUFBQTtBQUNBLFlBQUFDLFNBQUF6RCxFQUFBLElBQUEsRUFBQXVELFNBQUEsRUFBQTtBQUNBLFlBQUFFLFNBQUEsR0FBQSxFQUFBO0FBQ0F6RCxjQUFBdUQsU0FBQSxFQUFBRyxHQUFBLENBQUEsU0FBQSxFQUFBLEdBQUE7QUFDQSxTQUZBLE1BRUE7QUFDQTFELGNBQUF1RCxTQUFBLEVBQUFHLEdBQUEsQ0FBQSxTQUFBLEVBQUEsR0FBQTtBQUNBO0FBQ0EsS0FQQTtBQVFBMUQsTUFBQXVELFNBQUEsRUFBQXBELEtBQUEsQ0FBQSxZQUFBO0FBQ0FILFVBQUEsWUFBQSxFQUFBMkQsT0FBQSxDQUFBO0FBQ0FKLHVCQUFBO0FBREEsU0FBQSxFQUVBLEdBRkE7QUFHQSxlQUFBLEtBQUE7QUFDQSxLQUxBO0FBTUE7QUFDQSxDQTVHQTtBQTZHQTtBQUNBdkQsRUFBQSxhQUFBLEVBQUE0RCxLQUFBLENBQUE7QUFDQUMsY0FBQSxJQURBO0FBRUFDLG1CQUFBLElBRkE7QUFHQUMsVUFBQSxJQUhBO0FBSUFDLFlBQUE7QUFKQSxDQUFBO0FBTUFoRSxFQUFBLGVBQUEsRUFBQTRELEtBQUEsQ0FBQTtBQUNBQyxjQUFBLElBREE7QUFFQUMsbUJBQUEsSUFGQTtBQUdBQyxVQUFBLElBSEE7QUFJQUMsWUFBQTtBQUpBLENBQUE7O0FBUUFoRSxFQUFBLGNBQUEsRUFBQTRELEtBQUEsQ0FBQTtBQUNBSyxjQUFBLElBREE7QUFFQUMsa0JBQUEsQ0FGQTtBQUdBQyxvQkFBQSxDQUhBO0FBSUFILFlBQUEsS0FKQTtBQUtBeEMsZ0JBQUEsQ0FBQTtBQUNBNEMsb0JBQUEsSUFEQTtBQUVBQyxrQkFBQTtBQUNBSCwwQkFBQSxDQURBO0FBRUFDLDRCQUFBLENBRkE7QUFHQUYsc0JBQUEsSUFIQTtBQUlBSyxrQkFBQTtBQUpBO0FBRkEsS0FBQSxFQVNBO0FBQ0FGLG9CQUFBLEdBREE7QUFFQUMsa0JBQUE7QUFDQUgsMEJBQUEsQ0FEQTtBQUVBQyw0QkFBQTtBQUZBO0FBRkEsS0FUQSxFQWdCQTtBQUNBQyxvQkFBQSxHQURBO0FBRUFDLGtCQUFBO0FBQ0FILDBCQUFBLENBREE7QUFFQUMsNEJBQUE7QUFGQTtBQUZBLEtBaEJBO0FBTEEsQ0FBQTtBQThCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQW5FLEVBQUEsZUFBQSxFQUFBZ0QsRUFBQSxDQUFBLE9BQUEsRUFBQSxZQUFBO0FBQ0FoRCxNQUFBLElBQUEsRUFBQUksSUFBQSxHQUFBNEIsYUFBQSxDQUFBLE1BQUE7QUFDQSxDQUZBO0FBR0FoQyxFQUFBLFVBQUEsRUFBQVksSUFBQSxDQUFBLFlBQUE7QUFDQVosTUFBQSxJQUFBLEVBQUFnQyxhQUFBLENBQUE7QUFDQXVDLGtCQUFBLEdBREE7QUFFQW5CLGNBQUEsT0FGQTtBQUdBb0IsaUJBQUE7QUFDQUMscUJBQUEsSUFEQTtBQUVBQyxnQ0FBQTtBQUZBLFNBSEE7QUFPQXBCLHlCQUFBO0FBUEEsS0FBQTtBQVNBLENBVkE7QUFXQTtBQUNBdEQsRUFBQSxrQkFBQSxFQUFBRyxLQUFBLENBQUEsWUFBQTtBQUNBSCxNQUFBLGtCQUFBLEVBQUFPLFdBQUEsQ0FBQSxRQUFBO0FBQ0FQLE1BQUEsSUFBQSxFQUFBVyxRQUFBLENBQUEsUUFBQTtBQUNBWCxNQUFBLGFBQUEsRUFBQTBELEdBQUEsQ0FBQSxPQUFBLEVBQUExRCxFQUFBLElBQUEsRUFBQTBCLElBQUEsQ0FBQSxZQUFBLENBQUE7QUFDQTFCLE1BQUEsVUFBQSxFQUFBMEQsR0FBQSxDQUFBLGtCQUFBLEVBQUExRCxFQUFBLElBQUEsRUFBQTBCLElBQUEsQ0FBQSxVQUFBLENBQUE7QUFDQSxDQUxBOztBQU9BMUIsRUFBQSxhQUFBLEVBQUFHLEtBQUEsQ0FBQSxZQUFBO0FBQ0FILE1BQUEsT0FBQSxFQUFBMkUsSUFBQSxDQUFBLFFBQUEsRUFBQSxnQ0FBQTtBQUNBLENBRkE7QUFHQTNFLEVBQUEsV0FBQSxFQUFBRyxLQUFBLENBQUEsWUFBQTtBQUNBSCxNQUFBLE9BQUEsRUFBQTJFLElBQUEsQ0FBQSxRQUFBLEVBQUEsOEJBQUE7QUFDQSxDQUZBO0FBR0EzRSxFQUFBLFlBQUEsRUFBQUcsS0FBQSxDQUFBLFlBQUE7QUFDQUgsTUFBQSxPQUFBLEVBQUEyRSxJQUFBLENBQUEsUUFBQSxFQUFBLCtCQUFBO0FBQ0EsQ0FGQTtBQUdBM0UsRUFBQSxZQUFBLEVBQUFHLEtBQUEsQ0FBQSxZQUFBO0FBQ0FILE1BQUEsT0FBQSxFQUFBMkUsSUFBQSxDQUFBLFFBQUEsRUFBQSwrQkFBQTtBQUNBLENBRkE7QUFHQTNFLEVBQUEsYUFBQSxFQUFBRyxLQUFBLENBQUEsWUFBQTtBQUNBSCxNQUFBLE9BQUEsRUFBQTJFLElBQUEsQ0FBQSxRQUFBLEVBQUEsZ0NBQUE7QUFDQSxDQUZBO0FBR0EzRSxFQUFBMkUsSUFBQSxDQUFBQyxHQUFBLENBQUFDLFNBQUEsR0FBQSxDQUFBOztBQUdBOztBQUVBN0UsRUFBQSwrQ0FBQSxFQUFBRyxLQUFBLENBQUEsWUFBQTtBQUNBLFFBQUEsU0FBQUgsRUFBQSxJQUFBLEVBQUFJLElBQUEsR0FBQUMsRUFBQSxDQUFBLFVBQUEsQ0FBQSxFQUFBO0FBQ0FMLFVBQUEsbUNBQUEsRUFBQU0sT0FBQSxDQUFBLEdBQUE7QUFDQU4sVUFBQSx5QkFBQSxFQUFBTyxXQUFBLENBQUEsUUFBQTtBQUNBO0FBQ0FQLE1BQUEsSUFBQSxFQUFBSSxJQUFBLEdBQUFJLFdBQUEsQ0FBQSxHQUFBO0FBQ0FSLE1BQUEsSUFBQSxFQUFBUyxXQUFBLENBQUEsUUFBQTtBQUNBLENBUEE7QUFRQVQsRUFBQSx5Q0FBQSxFQUFBVyxRQUFBLENBQUEsUUFBQTtBQUNBWCxFQUFBLCtCQUFBLEVBQUFXLFFBQUEsQ0FBQSxRQUFBOztBQUdBLENBQUEsVUFBQVgsQ0FBQSxFQUFBOztBQUVBO0FBQ0E4RSxrQkFBQTlFLEVBQUEsU0FBQSxFQUFBK0UsY0FBQSxDQUFBO0FBQ0FDLG9CQUFBLGNBREE7QUFFQUMsc0JBQUEsSUFGQTtBQUdBQyx1QkFBQSxHQUhBO0FBSUFDLG9CQUFBLEdBSkE7QUFLQUMsMEJBQUE7QUFMQSxLQUFBLENBQUE7QUFRQSxDQVhBLEVBV0FDLE1BWEE7O0FBY0FyRixFQUFBLFlBQUE7QUFDQSxRQUFBc0YsTUFBQXRGLEVBQUEsc0JBQUEsQ0FBQTtBQUNBQSxNQUFBQyxNQUFBLEVBQUErQyxFQUFBLENBQUEsUUFBQSxFQUFBLFlBQUE7QUFDQSxZQUFBUSxTQUFBeEQsRUFBQWtCLFFBQUEsRUFBQXFDLFNBQUEsRUFBQTtBQUNBK0IsWUFBQTVCLEdBQUEsQ0FBQTtBQUNBLG1DQUFBLFNBQUEsQ0FBQSxHQUFBLEdBQUFGLE1BQUEsR0FBQTtBQURBLFNBQUE7QUFHQSxLQUxBO0FBTUEsQ0FSQTs7QUFjQSxNQUFBK0IsT0FBQXJFLFNBQUFzRSxhQUFBLENBQUEsY0FBQSxDQUFBOztBQUVBRCxLQUFBRSxnQkFBQSxDQUFBLE9BQUEsRUFBQSxZQUFBO0FBQ0EsUUFBQUYsS0FBQUcsU0FBQSxDQUFBQyxRQUFBLENBQUEsTUFBQSxDQUFBLEVBQUE7QUFDQUosYUFBQUcsU0FBQSxDQUFBRSxNQUFBLENBQUEsTUFBQTtBQUNBTCxhQUFBRyxTQUFBLENBQUFHLEdBQUEsQ0FBQSxPQUFBO0FBQ0EsS0FIQSxNQUdBO0FBQ0FOLGFBQUFHLFNBQUEsQ0FBQUUsTUFBQSxDQUFBLE9BQUE7QUFDQUwsYUFBQUcsU0FBQSxDQUFBRyxHQUFBLENBQUEsTUFBQTtBQUNBO0FBQ0EsQ0FSQTs7QUFZQSxJQUFBQyxrQkFBQTVFLFNBQUE2RSxjQUFBLENBQUEsWUFBQSxDQUFBOztBQUVBQyxXQUFBQyxNQUFBLENBQUFILGVBQUEsRUFBQTtBQUNBSSxhQUFBLElBREE7QUFFQUMsZUFBQSxLQUZBO0FBR0FDLFdBQUEsQ0FBQSxDQUFBLEVBQUEsRUFBQSxDQUhBO0FBSUFDLFVBQUEsQ0FKQTtBQUtBQyxXQUFBO0FBQ0E7QUFDQTtBQUNBLGVBQUEsQ0FBQSxDQUFBLENBSEE7QUFJQSxlQUFBLENBQUEsRUFBQTtBQUpBO0FBTEEsQ0FBQTs7QUFhQSxJQUFBQyxRQUFBLENBQ0FyRixTQUFBNkUsY0FBQSxDQUFBLGFBQUEsQ0FEQSxFQUNBO0FBQ0E3RSxTQUFBNkUsY0FBQSxDQUFBLGFBQUEsQ0FGQSxDQUVBO0FBRkEsQ0FBQTs7QUFLQTtBQUNBO0FBQ0FELGdCQUFBRSxVQUFBLENBQUFoRCxFQUFBLENBQUEsUUFBQSxFQUFBLFVBQUF3RCxNQUFBLEVBQUFDLE1BQUEsRUFBQUMsU0FBQSxFQUFBQyxLQUFBLEVBQUFDLFNBQUEsRUFBQTtBQUNBTCxVQUFBRSxNQUFBLEVBQUFJLFNBQUEsR0FBQUwsT0FBQUMsTUFBQSxJQUFBLEdBQUE7QUFDQSxDQUZBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBGb290ZXIgQWNjb3JkaW9uXG5pZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA5NjApIHtcbiAgICAkKFwiI2FjY29yZGlvbiBoNVwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChmYWxzZSA9PSAkKHRoaXMpLm5leHQoKS5pcygnOnZpc2libGUnKSkge1xuICAgICAgICAgICAgJCgnI2FjY29yZGlvbiB1bCcpLnNsaWRlVXAoMzAwKTtcbiAgICAgICAgICAgICQoJyNhY2NvcmRpb24gaDUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgIH1cbiAgICAgICAgJCh0aGlzKS5uZXh0KCkuc2xpZGVUb2dnbGUoMzAwKTtcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG4gICAgfSk7XG4gICAgJCgnI2FjY29yZGlvbiB1bDplcSgwKScpLnNob3coKTtcbiAgICAkKCcjYWNjb3JkaW9uIGg1OmVxKDApJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xufTtcblxuJChmdW5jdGlvbiAoKSB7XG4gICAgJCgnLnJvdycpLmVhY2goZnVuY3Rpb24gKGksIGVsZW0pIHtcbiAgICAgICAgJChlbGVtKVxuICAgICAgICAgICAgLmZpbmQoJy5jcmVhdGl2ZS1zZWN0aW9uX19jb250ZW50JykgLy8gT25seSBjaGlsZHJlbiBvZiB0aGlzIHJvd1xuICAgICAgICAgICAgLm1hdGNoSGVpZ2h0KHtcbiAgICAgICAgICAgICAgICBieVJvdzogZmFsc2VcbiAgICAgICAgICAgIH0pOyAvLyBSb3cgZGV0ZWN0aW9uIGdldHMgY29uZnVzZWQgc28gZGlzYWJsZSBpdFxuICAgIH0pO1xufSk7XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgICQoJy50aW1lbGluZSAub3dsLWNhcm91c2VsJykub3dsQ2Fyb3VzZWwoe1xuICAgICAgICAgICAgbG9vcDogZmFsc2UsXG4gICAgICAgICAgICBtYXJnaW46IDAsXG4gICAgICAgICAgICBuYXY6IGZhbHNlLFxuICAgICAgICAgICAgcmVzcG9uc2l2ZToge1xuICAgICAgICAgICAgICAgIDA6IHtcbiAgICAgICAgICAgICAgICAgICAgaXRlbXM6IDFcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pLmZpbmQoJy5vd2wtaXRlbScpXG4gICAgICAgIC5lYWNoKGZ1bmN0aW9uIChpKSB7XG4gICAgICAgICAgICB2YXIgYXR0ciA9ICQodGhpcykuY2hpbGRyZW4oKS5hdHRyKCdkYXRhLXllYXInKTtcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gJCgnPHNwYW4+JyArIGF0dHIgKyAnPC9zcGFuPicpO1xuICAgICAgICAgICAgJCgnLnRpbWVsaW5lIC5vd2wtY2Fyb3VzZWwgLm93bC1kb3QnKS5lcShpKS5hcHBlbmQoZWxlbWVudCk7XG4gICAgICAgIH0pO1xuXG5cblxuXG4gICAgJCgnLmNhcm91c2VsJykuY2Fyb3VzZWwoKTtcblxuICAgICQoJy5vZmZlci1wb3B1cCcpLm1hZ25pZmljUG9wdXAoe1xuICAgICAgICAvLyB0eXBlOiAnaW5saW5lJyxcbiAgICAgICAgcmVtb3ZhbERlbGF5OiA1MDAsIC8vZGVsYXkgcmVtb3ZhbCBieSBYIHRvIGFsbG93IG91dC1hbmltYXRpb25cbiAgICAgICAgY2FsbGJhY2tzOiB7XG4gICAgICAgICAgICBiZWZvcmVPcGVuOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdC5tYWluQ2xhc3MgPSB0aGlzLnN0LmVsLmF0dHIoJ2RhdGEtZWZmZWN0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIG1pZENsaWNrOiB0cnVlIC8vIGFsbG93IG9wZW5pbmcgcG9wdXAgb24gbWlkZGxlIG1vdXNlIGNsaWNrLiBBbHdheXMgc2V0IGl0IHRvIHRydWUgaWYgeW91IGRvbid0IHByb3ZpZGUgYWx0ZXJuYXRpdmUgc291cmNlLlxuICAgIH0pO1xuICAgICQoXCIjYXV0b1VwZGF0ZVwiKS5oaWRlKCk7XG4gICAgJCgnI2NoZWNrYm94MScpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICQoJyNhdXRvVXBkYXRlJykuc2hvdygzMDApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgJCgnI2F1dG9VcGRhdGUnKS5oaWRlKDIwMCk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgICQoJyNsaWdodGJveE1vdmVGcm9tVG9wJykubWFnbmlmaWNQb3B1cCh7XG4gICAgICAgIHJlbW92YWxEZWxheTogNTAwLCAvL2RlbGF5IHJlbW92YWwgYnkgWCB0byBhbGxvdyBvdXQtYW5pbWF0aW9uXG4gICAgICAgIGNhbGxiYWNrczoge1xuICAgICAgICAgICAgYmVmb3JlT3BlbjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3QubWFpbkNsYXNzID0gdGhpcy5zdC5lbC5hdHRyKCdkYXRhLWVmZmVjdCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBtaWRDbGljazogdHJ1ZSAvLyBhbGxvdyBvcGVuaW5nIHBvcHVwIG9uIG1pZGRsZSBtb3VzZSBjbGljay4gQWx3YXlzIHNldCBpdCB0byB0cnVlIGlmIHlvdSBkb24ndCBwcm92aWRlIGFsdGVybmF0aXZlIHNvdXJjZS5cbiAgICB9KTtcblxuICAgIC8vIE1hdGVyaWFsaXplIFN0YXJ0cyBIZXJlXG4gICAgTS51cGRhdGVUZXh0RmllbGRzKCk7XG4gICAgJCgnc2VsZWN0JykuZm9ybVNlbGVjdCgpO1xuICAgICQoJy5kYXRlcGlja2VyJykuZGF0ZXBpY2tlcigpO1xuICAgICQoJy50aW1lcGlja2VyJykudGltZXBpY2tlcigpO1xuXG5cbiAgICAvLyBNYXRlcmlhbGl6ZSBFbmRzIEhlcmVcbiAgICAvLyAzNjAgQ29sb3IgY2hhbmdlIGZ1bmN0aW9uc1xuICAgICQoJy5jb2xvci1jaG9vc2UgaW5wdXQnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBoZWFkcGhvbmVzQ29sb3IgPSAkKHRoaXMpLmF0dHIoJ2RhdGEtaW1hZ2UnKTtcbiAgICAgICAgJCgnLmFjdGl2ZScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgJCgnLmxlZnQtY29sdW1uIGltZ1tkYXRhLWltYWdlID0gJyArIGhlYWRwaG9uZXNDb2xvciArICddJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICB9KTtcbiAgICAkKCcuZXh0ZXJpb3JfXzM2MF9hY3Rpb24gYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCcuZXh0ZXJpb3JfXzM2MF9hY3Rpb24gYnV0dG9uLmN1cnJlbnQnKS5yZW1vdmVDbGFzcygnY3VycmVudCcpO1xuICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdjdXJyZW50Jyk7XG4gICAgfSk7XG4gICAgLy8gMzYwIENvbG9yIGNoYW5nZSBmdW5jdGlvbnMgZW5kc1xuICAgIC8vIFNwZWNpZmljYXRpb25zIFRhYnNcbiAgICAkKCd1bC50YWJzIGxpJykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgdGFiX2lkID0gJCh0aGlzKS5hdHRyKCdkYXRhLXRhYicpO1xuICAgICAgICAkKCd1bC50YWJzIGxpJykucmVtb3ZlQ2xhc3MoJ2N1cnJlbnQnKTtcbiAgICAgICAgJCgnLnRhYi1jb250ZW50JykucmVtb3ZlQ2xhc3MoJ2N1cnJlbnQnKTtcblxuICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdjdXJyZW50Jyk7XG4gICAgICAgICQoXCIjXCIgKyB0YWJfaWQpLmFkZENsYXNzKCdjdXJyZW50Jyk7XG4gICAgfSk7XG4gICAgLy8gbWFnbmlmaWNQb3B1cFxuICAgICQoJy5wb3B1cC15b3V0dWJlLCAucG9wdXAtdmltZW8sIC5wb3B1cC1nbWFwcycpLm1hZ25pZmljUG9wdXAoe1xuICAgICAgICBkaXNhYmxlT246IDcwMCxcbiAgICAgICAgdHlwZTogJ2lmcmFtZScsXG4gICAgICAgIG1haW5DbGFzczogJ21mcC1mYWRlJyxcbiAgICAgICAgcmVtb3ZhbERlbGF5OiAxNjAsXG4gICAgICAgIHByZWxvYWRlcjogZmFsc2UsXG4gICAgICAgIGZpeGVkQ29udGVudFBvczogZmFsc2VcbiAgICB9KTtcblxuICAgIC8vIHNjcm9sbCB0byB0b3BcbiAgICB2YXIgc2Nyb2xsVG9wID0gJChcIi5zY3JvbGxUb3BcIik7XG4gICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciB0b3BQb3MgPSAkKHRoaXMpLnNjcm9sbFRvcCgpO1xuICAgICAgICBpZiAodG9wUG9zID4gMTAwKSB7XG4gICAgICAgICAgICAkKHNjcm9sbFRvcCkuY3NzKFwib3BhY2l0eVwiLCBcIjFcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKHNjcm9sbFRvcCkuY3NzKFwib3BhY2l0eVwiLCBcIjBcIik7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAkKHNjcm9sbFRvcCkuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICBzY3JvbGxUb3A6IDBcbiAgICAgICAgfSwgODAwKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xuICAgIC8vIHNjcm9sbCB0byB0b3AgRW5kc1xufSk7XG4vLyBTbGljayBTbGlkZXIgU3RhcnRzXG4kKCcubWFpbmJhbm5lcicpLnNsaWNrKHtcbiAgICBhdXRvcGxheTogdHJ1ZSxcbiAgICBhdXRvcGxheVNwZWVkOiAyMDAwLFxuICAgIGZhZGU6IHRydWUsXG4gICAgYXJyb3dzOiBmYWxzZVxufSk7XG4kKCcuc2FsZXMtc2xpZGVyJykuc2xpY2soe1xuICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgIGF1dG9wbGF5U3BlZWQ6IDIwMDAsXG4gICAgZmFkZTogdHJ1ZSxcbiAgICBhcnJvd3M6IGZhbHNlXG59KTtcblxuXG4kKCcuY2Fycy1zbGlkZXInKS5zbGljayh7XG4gICAgaW5maW5pdGU6IHRydWUsXG4gICAgc2xpZGVzVG9TaG93OiA0LFxuICAgIHNsaWRlc1RvU2Nyb2xsOiA0LFxuICAgIGFycm93czogZmFsc2UsXG4gICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDEwMjQsXG4gICAgICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogMyxcbiAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMyxcbiAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBkb3RzOiB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDYwMCxcbiAgICAgICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAyXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDQ4MCxcbiAgICAgICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICBdXG59KTtcbi8vIFNsaWNrIFNsaWRlciBFbmRzXG5cbi8vIE1lbnUgU2NyaXB0IHN0YXJ0cyBoZXJlICEhIVxuLy8gTWVudSBTY3JpcHQgRW5kcyBoZXJlICEhIVxuLy8gUGhvdG8gR2FsbGVyeSBTdGFydHMgaGVyZSAhISFcbiQoJy5nYWxsZXJ5LWxpbmsnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgJCh0aGlzKS5uZXh0KCkubWFnbmlmaWNQb3B1cCgnb3BlbicpO1xufSk7XG4kKCcuZ2FsbGVyeScpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICQodGhpcykubWFnbmlmaWNQb3B1cCh7XG4gICAgICAgIGRlbGVnYXRlOiAnYScsXG4gICAgICAgIHR5cGU6ICdpbWFnZScsXG4gICAgICAgIGdhbGxlcnk6IHtcbiAgICAgICAgICAgIGVuYWJsZWQ6IHRydWUsXG4gICAgICAgICAgICBuYXZpZ2F0ZUJ5SW1nQ2xpY2s6IHRydWVcbiAgICAgICAgfSxcbiAgICAgICAgZml4ZWRDb250ZW50UG9zOiBmYWxzZVxuICAgIH0pO1xufSk7XG4vLyBQaG90byBHYWxsZXJ5IEVuZHMgaGVyZSAhISFcbiQoXCIuY2FyLWNvbG9ycyBzcGFuXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKFwiLmNhci1jb2xvcnMgc3BhblwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAkKHRoaXMpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICQoXCIuY2FyLWJ1dHRvblwiKS5jc3MoXCJjb2xvclwiLCAkKHRoaXMpLmF0dHIoXCJkYXRhLWNvbG9yXCIpKTtcbiAgICAkKFwiLmNhci1waWNcIikuY3NzKFwiYmFja2dyb3VuZC1pbWFnZVwiLCAkKHRoaXMpLmF0dHIoXCJkYXRhLXBpY1wiKSk7XG59KTtcblxuJCgnI2NzMzUtd2hpdGUnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgJCgnI2NzMzUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczM1L3doaXRlLyMjIyMucG5nJyk7XG59KTtcbiQoJyNjczM1LXJlZCcpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvcmVkLyMjIyMucG5nJyk7XG59KTtcbiQoJyNjczM1LWJsdWUnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgJCgnI2NzMzUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczM1L2JsdWUvIyMjIy5wbmcnKTtcbn0pO1xuJCgnI2NzMzUtZ3JleScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvZ3JleS8jIyMjLnBuZycpO1xufSk7XG4kKCcjY3MzNS1icm93bicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvYnJvd24vIyMjIy5wbmcnKTtcbn0pO1xuJC5yZWVsLmRlZi5pbmRpY2F0b3IgPSA1O1xuXG5cbi8vIGNhciBkZXRhaWxzIGFjY29yZGlvblxuXG4kKFwiLmNhcmRldGFpbHMtYWNjb3JkaW9uIC5jYXItZGV0YWlsc19fc3BlY19oZWFkXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoZmFsc2UgPT0gJCh0aGlzKS5uZXh0KCkuaXMoJzp2aXNpYmxlJykpIHtcbiAgICAgICAgJCgnLmNhcmRldGFpbHMtYWNjb3JkaW9uIC5jYXJkZXRhaWxzJykuc2xpZGVVcCgzMDApO1xuICAgICAgICAkKCcuY2FyLWRldGFpbHNfX3NwZWNfaGVhZCcpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICB9XG4gICAgJCh0aGlzKS5uZXh0KCkuc2xpZGVUb2dnbGUoMzAwKTtcbiAgICAkKHRoaXMpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbn0pO1xuJCgnLmNhcmRldGFpbHMtYWNjb3JkaW9uIC5jYXJkZXRhaWxzOmVxKDApJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuJCgnLmNhci1kZXRhaWxzX19zcGVjX2hlYWQ6ZXEoMCknKS5hZGRDbGFzcygnYWN0aXZlJyk7XG5cblxuKGZ1bmN0aW9uICgkKSB7XG5cbiAgICAvLyBBY2NlcHRzIGFyZ3VtZW50cyBhcyBzdHJpbmdzXG4gICAgJGNhbGN1bGF0b3IgPSAkKCcjd2lkZ2V0JykubG9hbkNhbGN1bGF0b3Ioe1xuICAgICAgICBsb2FuQW1vdW50OiAnQUVEIDEsMDAwLjAwJyxcbiAgICAgICAgbG9hbkR1cmF0aW9uOiAnMTInLFxuICAgICAgICB2YWx1ZUFkZGVkVGF4OiAnMCcsXG4gICAgICAgIHNlcnZpY2VGZWU6ICcwJyxcbiAgICAgICAgcGF5bWVudEZyZXF1ZW5jeTogJ21vbnRobHknXG4gICAgfSk7XG5cbn0pKGpRdWVyeSk7XG5cblxuJChmdW5jdGlvbiAoKSB7XG4gICAgdmFyICRlbCA9ICQoJy5iYWNrZ3JvdW5kX19wYXJhbGF4Jyk7XG4gICAgJCh3aW5kb3cpLm9uKCdzY3JvbGwnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBzY3JvbGwgPSAkKGRvY3VtZW50KS5zY3JvbGxUb3AoKTtcbiAgICAgICAgJGVsLmNzcyh7XG4gICAgICAgICAgICAnYmFja2dyb3VuZC1wb3NpdGlvbic6ICc1MCUgJyArICgtLjA2ICogc2Nyb2xsKSArICdweCdcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KTtcblxuXG5cblxuXG5jb25zdCBtZW51ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm1vYmlsZS1tZW51Jyk7XG5cbm1lbnUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKG1lbnUuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJykpIHtcbiAgICAgICAgbWVudS5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJyk7XG4gICAgICAgIG1lbnUuY2xhc3NMaXN0LmFkZCgnY2xvc2UnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBtZW51LmNsYXNzTGlzdC5yZW1vdmUoJ2Nsb3NlJyk7XG4gICAgICAgIG1lbnUuY2xhc3NMaXN0LmFkZCgnb3BlbicpO1xuICAgIH1cbn0pXG5cblxuXG52YXIgbm9uTGluZWFyU2xpZGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3ByaWNlcmFuZ2UnKTtcblxubm9VaVNsaWRlci5jcmVhdGUobm9uTGluZWFyU2xpZGVyLCB7XG4gICAgY29ubmVjdDogdHJ1ZSxcbiAgICBiZWhhdmlvdXI6ICd0YXAnLFxuICAgIHN0YXJ0OiBbNSwgMTVdLFxuICAgIHN0ZXA6IDEsXG4gICAgcmFuZ2U6IHtcbiAgICAgICAgLy8gU3RhcnRpbmcgYXQgNTAwLCBzdGVwIHRoZSB2YWx1ZSBieSA1MDAsXG4gICAgICAgIC8vIHVudGlsIDQwMDAgaXMgcmVhY2hlZC4gRnJvbSB0aGVyZSwgc3RlcCBieSAxMDAwLlxuICAgICAgICAnbWluJzogWzFdLFxuICAgICAgICAnbWF4JzogWzIwXVxuICAgIH1cbn0pO1xuXG52YXIgbm9kZXMgPSBbXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvd2VyLXZhbHVlJyksIC8vIDBcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXBwZXItdmFsdWUnKSAvLyAxXG5dO1xuXG4vLyBEaXNwbGF5IHRoZSBzbGlkZXIgdmFsdWUgYW5kIGhvdyBmYXIgdGhlIGhhbmRsZSBtb3ZlZFxuLy8gZnJvbSB0aGUgbGVmdCBlZGdlIG9mIHRoZSBzbGlkZXIuXG5ub25MaW5lYXJTbGlkZXIubm9VaVNsaWRlci5vbigndXBkYXRlJywgZnVuY3Rpb24gKHZhbHVlcywgaGFuZGxlLCB1bmVuY29kZWQsIGlzVGFwLCBwb3NpdGlvbnMpIHtcbiAgICBub2Rlc1toYW5kbGVdLmlubmVySFRNTCA9IHZhbHVlc1toYW5kbGVdICsgJ2snO1xufSk7Il19
