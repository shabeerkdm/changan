/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

/*!
 * jQuery Loan Calculator 3.1.3
 *
 * Author: Jorge González <scrub.mx@gmail>
 * Released under the MIT license - https://opensource.org/licenses/MIT
 */
;
(function ($, window, document, undefined) {

  "use strict";

  /**
   * Table of credit rates for every score.
   * @type {Object}
   */

  var CREDIT_RATES = {
    'A': 5.32,
    'B': 8.18,
    'C': 12.29,
    'D': 15.61,
    'E': 18.25,
    'F': 21.99,
    'G': 26.77
  };

  /**
   * Table of allowed payment frequencies.
   * @type {Object}
   */
  var PAYMENT_FREQUENCIES = {
    'weekly': 52,
    'biweekly': 26,
    'monthly': 12
  };

  /**
   * The minimum allowed for a loan.
   * @type {Number}
   */
  var MINIMUM_LOAN = 1000;

  /**
   * The minimum duration in months.
   * @type {Number}
   */
  var MINIMUM_DURATION = 1;

  /**
   * Default options for the plugin.
   * @type {Object}
   */
  var defaults = {
    // default values for a loan
    loanAmount: 50000,
    loanDuration: 12,
    creditRates: CREDIT_RATES,
    creditScore: 'A',
    valueAddedTax: 0,
    serviceFee: 0,
    paymentFrequency: 'monthly',

    // inputs
    loanAmountSelector: '#loan-amount',
    loanDurationSelector: '#loan-duration',
    creditScoreSelector: '#credit-score',
    paymentFrequencySelector: '#payment-frequency',

    // display selected values
    selectedAmount: '#selected-amount',
    selectedDuration: '#selected-duration',
    selectedScore: '#selected-score',
    selectedPaymentFrequency: '#selected-payment-frequency',

    // display the results
    loanTotalSelector: '#loan-total',
    paymentSelector: '#payment',
    interestTotalSelector: '#interest-total',
    serviceFeeSelector: '#service-fee',
    taxTotalSelector: '#tax-total',
    totalAnnualCostSelector: '#total-annual-cost',
    loanGrandTotalSelector: '#grand-total'
  };

  /**
   * The actual plugin constructor
   * @param {Object} element
   * @param {Object} options
   */
  function Plugin(element, options) {
    this.$el = $(element);
    this._name = 'loanCalculator';
    this._defaults = defaults;
    this.settings = $.extend({}, defaults, options);
    this.attachListeners();
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {

    /**
     * Validates the data and shows the results.
     * @return {void}
     */
    init: function () {
      this.validate();
      this.render();
    },

    /**
     * Attach event listeners to the event handlers.
     * @return {void}
     */
    attachListeners: function () {
      var eventEmitters = [this.settings.loanAmountSelector, this.settings.loanDurationSelector, this.settings.creditScoreSelector, this.settings.paymentFrequencySelector];

      $(eventEmitters.join()).on({
        change: this.eventHandler.bind(this),
        mousemove: this.eventHandler.bind(this),
        touchmove: this.eventHandler.bind(this)
      });
    },

    /**
     * Handle events from the DOM.
     * @return {void}
     */
    eventHandler: function () {
      this.update({
        loanAmount: this.$el.find(this.settings.loanAmountSelector).val(),
        loanDuration: this.$el.find(this.settings.loanDurationSelector).val(),
        creditScore: this.$el.find(this.settings.creditScoreSelector).val(),
        paymentFrequency: this.$el.find(this.settings.paymentFrequencySelector).val()
      });
    },

    /**
     * Sanitize and validate the user input data.
     * @throws Error
     * @return {void}
     */
    validate: function () {
      if (typeof this.settings.loanAmount === 'string') {
        this.settings.loanAmount = this._toNumeric(this.settings.loanAmount);
      }

      if (typeof this.settings.serviceFee === 'string') {
        this.settings.serviceFee = this._toNumeric(this.settings.serviceFee);
      }

      if (!$.isPlainObject(this.settings.creditRates)) {
        throw new Error('The value provided for [creditRates] is not valid.');
      }

      for (var creditRate in this.settings.creditRates) {
        if (typeof this.settings.creditRates[creditRate] === 'string') {
          this.settings.creditRates[creditRate] = this._toNumeric(this.settings.creditRates[creditRate]);
        }

        if (!$.isNumeric(this.settings.creditRates[creditRate])) {
          throw new Error('The value provided for [creditRates] is not valid.');
        }

        if (this.settings.creditRates[creditRate] < 1) {
          this.settings.creditRates[creditRate] = this.settings.creditRates[creditRate] * 100;
        }
      }

      // Sanitize the input
      this.settings.loanAmount = parseFloat(this.settings.loanAmount);
      this.settings.loanDuration = parseFloat(this.settings.loanDuration);
      this.settings.serviceFee = parseFloat(this.settings.serviceFee);
      this.settings.creditScore = $.trim(this.settings.creditScore.toUpperCase());

      if (!PAYMENT_FREQUENCIES.hasOwnProperty(this.settings.paymentFrequency)) {
        throw new Error('The value provided for [paymentFrequency] is not valid.');
      }

      if (!this.settings.creditRates.hasOwnProperty(this.settings.creditScore)) {
        throw new Error('The value provided for [creditScore] is not valid.');
      }

      if (this.settings.loanAmount < MINIMUM_LOAN) {
        throw new Error('The value provided for [loanAmount] must me at least 1000.');
      }

      if (this.settings.loanDuration < MINIMUM_DURATION) {
        throw new Error('The value provided for [loanDuration] must me at least 1.');
      }

      if (!$.isNumeric(this.settings.serviceFee)) {
        throw new Error('The value provided for [serviceFee] is not valid.');
      }
    },

    /**
     * Show the results in the DOM.
     * @return {void}
     */
    render: function () {
      this._displaySelectedValues();
      this._displayResults();
    },

    /**
     * Show the selected values in the DOM.
     * @return {void}
     */
    _displaySelectedValues: function () {
      // Display the selected loan amount
      this.$el.find(this.settings.selectedAmount).html(this._toMoney(this.settings.loanAmount));

      // Display the selected loan duration
      this.$el.find(this.settings.selectedDuration).html(this.settings.loanDuration);

      // Display the selected credit score
      this.$el.find(this.settings.selectedScore).html(this.settings.creditScore);

      // Display the selected payment frequency
      this.$el.find(this.settings.selectedPaymentFrequency).html(this.settings.paymentFrequency);
    },

    /**
     * Display the results for the current values.
     * @return {void}
     */
    _displayResults: function () {
      // Display the loan total
      this.$el.find(this.settings.loanTotalSelector).html(this._toMoney(this._loanTotal()));

      // Display the loan periodic payment
      this.$el.find(this.settings.paymentSelector).html(this._toMoney(this._PMT()));

      // Display the interest total amount
      this.$el.find(this.settings.interestTotalSelector).html(this._toMoney(this._interestTotal()));

      // Display the tax total amount
      this.$el.find(this.settings.taxTotalSelector).html(this._toMoney(this._taxTotal()));

      // Display the annual total cost
      this.$el.find(this.settings.totalAnnualCostSelector).html(this._toPercentage(this._CAT()));

      // Display the service fee if any
      this.$el.find(this.settings.serviceFeeSelector).html(this._toMoney(this._serviceFeeWithVAT()));

      this.$el.find(this.settings.loanGrandTotalSelector).html(this._toMoney(this._grandTotal()));
    },

    /**
     * Run the init method again with the provided options.
     * @param {Object} args
     */
    update: function (args) {
      this.settings = $.extend({}, this._defaults, this.settings, args);
      this.init();
      this.$el.trigger('loan:update');
    },

    /**
     * Generate the results as an array of objects,
     * each object contains the values for each period.
     * @return {Array}
     */
    _results: function () {
      var balance = this.settings.loanAmount;
      var initial = this.settings.loanAmount;
      var interestRate = this._interestRate();
      var VAT = this._valueAddedTax();
      var payment = this._PMT();
      var numberOfPayments = this._numberOfPayments();
      var results = [];

      // We loop over the number of payments and each time
      // we extract the information to build the period
      // that will be appended to the results array.
      for (var paymentNumber = 0; paymentNumber < numberOfPayments; paymentNumber++) {
        var interest = balance * interestRate;
        var taxesPaid = balance * interestRate * VAT;
        var principal = payment - interest - taxesPaid;

        // update initial balance for next iteration
        initial = balance;

        // update final balance for the next iteration.
        balance = balance - principal;

        results.push({
          initial: initial,
          principal: principal,
          interest: interest,
          tax: taxesPaid,
          payment: payment,
          balance: balance
        });
      };

      return results;
    },

    /**
     * Generate the amortization schedule.
     * @return {Array}
     */
    schedule: function () {
      return $.map(this._results(), function (value) {
        return {
          initial: this._toMoney(value.initial),
          principal: this._toMoney(value.principal),
          interest: this._toMoney(value.interest),
          tax: this._toMoney(value.tax),
          payment: this._toMoney(value.payment),
          balance: this._toMoney(value.balance)
        };
      }.bind(this));
    },

    /**
     * Return the credit rates being used.
     * @return {Object}
     */
    creditRates: function () {
      return this.settings.creditRates;
    },

    /**
     * Get the credit rate corresponding to the current credit score.
     * @return {Number}
     */
    _annualInterestRate: function () {
      if (this.settings.hasOwnProperty('interestRate')) {
        if (this.settings.interestRate <= 1) {
          return this.settings.interestRate;
        }

        return this._toNumeric(this.settings.interestRate) / 100;
      }

      return this.settings.creditRates[this.settings.creditScore] / 100;
    },

    /**
     * Get the periodic interest rate.
     * @returns {Number}
     */
    _interestRate: function () {
      return this._annualInterestRate() / this._paymentFrequency();
    },

    /**
     * Returns the periodic payment frequency
     * @returns {Number}
     */
    _paymentFrequency: function () {
      return PAYMENT_FREQUENCIES[this.settings.paymentFrequency];
    },

    /**
     * Returns number of payments for the loan.
     * @returns {Number}
     */
    _numberOfPayments: function () {
      var durationInYears = this._toNumeric(this.settings.loanDuration) / 12;

      return Math.floor(durationInYears * PAYMENT_FREQUENCIES[this.settings.paymentFrequency]);
    },

    /**
     * Calculates the total cost of the loan.
     * @return {Number}
     */
    _loanTotal: function () {
      return this._PMT() * this._numberOfPayments();
    },

    /**
     * Calculate the monthly amortized loan payments.
     * @see https://en.wikipedia.org/wiki/Compound_interest#Monthly_amortized_loan_or_mortgage_payments
     * @return {Number}
     */
    _PMT: function () {
      var i = this._interestRate();
      var L = this.settings.loanAmount;
      var n = this._numberOfPayments();

      if (this.settings.valueAddedTax !== 0) {
        i = (1 + this._valueAddedTax()) * i; // interest rate with tax
      }

      return L * i / (1 - Math.pow(1 + i, -n));
    },

    /**
     * Calculate the total interest for the loan.
     * @returns {Number}
     */
    _interestTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.interest;
      }));
      return total;
    },

    /**
     * Calculate the value added tax total for the loan.
     * @returns {Number}
     */
    _taxTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.tax;
      }));
      return total;
    },

    /**
     * Return the loan fees and commissions total.
     * @return {Number}
     */
    _serviceFee: function () {
      var serviceFee = this.settings.serviceFee;

      // if the service fee is greater than 1 then the
      // value must be converted to decimals first.
      if (serviceFee > 1) {
        serviceFee = serviceFee / 100;
      }

      return this.settings.loanAmount * serviceFee;
    },

    /**
     * Return the loan fees and commissions total with VAT included.
     * @return {Number}
     */
    _serviceFeeWithVAT: function () {
      return this._serviceFee() * (this._valueAddedTax() + 1);
    },

    /**
     * Calculates the total cost of the loan including the service fee.
     * @return {Number}
     */
    _grandTotal: function () {
      return this._loanTotal() + this._serviceFeeWithVAT();
    },

    /**
     * Return the total annual cost (CAT)
     * @see http://www.banxico.org.mx/CAT
     * @return {Number}
     */
    _CAT: function () {
      var IRR = this._IRR(this._cashFlow());
      var periods = this._paymentFrequency();

      return Math.pow(1 + IRR, periods) - 1;
    },

    /**
     * Returns an array with a series of cash flows for the current loan.
     * @return {Array}
     */
    _cashFlow: function () {
      var results = this._results();
      var cashFlow = [this._serviceFee() - this.settings.loanAmount];

      $.each(results, function (index, period) {
        cashFlow.push(period.payment - period.tax);
      }.bind(this));

      return cashFlow;
    },

    /**
     * Returns the internal rate of return for a series of cash flows represented by the numbers in values.
     * @param  {Array} values
     * @param  {Number} guess
     * @return {Number}
     */
    _IRR: function (values, guess) {
      guess = guess || 0;

      // Calculates the resulting amount
      var irrResult = function (values, dates, rate) {
        var result = values[0];

        for (var i = 1; i < values.length; i++) {
          result += values[i] / Math.pow(rate + 1, (dates[i] - dates[0]) / 365);
        }

        return result;
      };

      // Calculates the first derivation
      var irrResultDerivative = function (values, dates, rate) {
        var result = 0;

        for (var i = 1; i < values.length; i++) {
          var frac = (dates[i] - dates[0]) / 365;
          result -= frac * values[i] / Math.pow(rate + 1, frac + 1);
        }

        return result;
      };

      // Initialize dates and check that values contains at
      // least one positive value and one negative value
      var dates = [];
      var positive = false;
      var negative = false;

      for (var i = 0; i < values.length; i++) {
        dates[i] = i === 0 ? 0 : dates[i - 1] + 365;
        if (values[i] > 0) positive = true;
        if (values[i] < 0) negative = true;
      }

      if (!positive || !negative) {
        throw new Error('Error the values does not contain at least one positive value and one negative value');
      }

      // Initialize guess and resultRate
      guess = guess === undefined ? 0.1 : guess;
      var resultRate = guess;

      // Set maximum epsilon for end of iteration
      var epsMax = 1e-10;

      // Implement Newton's method
      var newRate, epsRate, resultValue;
      var contLoop = true;

      do {
        resultValue = irrResult(values, dates, resultRate);
        newRate = resultRate - resultValue / irrResultDerivative(values, dates, resultRate);
        epsRate = Math.abs(newRate - resultRate);
        resultRate = newRate;
        contLoop = epsRate > epsMax && Math.abs(resultValue) > epsMax;
      } while (contLoop);

      // Return internal rate of return
      return resultRate;
    },

    /**
     * Return the value added tax in decimals.
     * @return {Number}
     */
    _valueAddedTax: function () {
      var tax = this._toNumeric(this.settings.valueAddedTax || 0);

      // if tax is greater than 1 means the value
      // must be converted to decimals first.
      return tax > 1 ? tax / 100 : tax;
    },

    /**
     * Convert numeric format to money format.
     * @param  {Number} numeric
     * @return {String}
     */
    _toMoney: function (numeric) {
      if (typeof numeric == 'string') {
        numeric = parseFloat(numeric);
      }

      return 'AED ' + numeric.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    },

    /**
     * Convert from money format to numeric format.
     * @param  {String} value
     * @return {Number}
     */
    _toNumeric: function (value) {
      return parseFloat(value.toString().replace(/[^0-9\.]+/g, ''));
    },

    /**
     * To convert the provided value to percent format.
     * @param {Number} numeric
     * @returns {String}
     */
    _toPercentage: function (numeric) {
      return (numeric * 100).toFixed(2) + '%';
    }

  });

  /**
   * Wrapper around the constructor to prevent multiple instantiations.
   */
  $.fn.loanCalculator = function (options, args) {
    if (options === 'schedule') {
      return this.data('plugin_loanCalculator').schedule();
    }

    if (options === 'rates') {
      return this.data('plugin_loanCalculator').creditRates();
    }

    return this.each((function () {
      var instance = $.data(this, 'plugin_loanCalculator');
      if (!instance) {
        $.data(this, 'plugin_loanCalculator', new Plugin(this, options));
      } else if (options === 'update') {
        return instance.update(args);
      }
    }));
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5sb2FuLWNhbGN1bGF0b3IuanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsImRvY3VtZW50IiwidW5kZWZpbmVkIiwiQ1JFRElUX1JBVEVTIiwiUEFZTUVOVF9GUkVRVUVOQ0lFUyIsIk1JTklNVU1fTE9BTiIsIk1JTklNVU1fRFVSQVRJT04iLCJkZWZhdWx0cyIsImxvYW5BbW91bnQiLCJsb2FuRHVyYXRpb24iLCJjcmVkaXRSYXRlcyIsImNyZWRpdFNjb3JlIiwidmFsdWVBZGRlZFRheCIsInNlcnZpY2VGZWUiLCJwYXltZW50RnJlcXVlbmN5IiwibG9hbkFtb3VudFNlbGVjdG9yIiwibG9hbkR1cmF0aW9uU2VsZWN0b3IiLCJjcmVkaXRTY29yZVNlbGVjdG9yIiwicGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yIiwic2VsZWN0ZWRBbW91bnQiLCJzZWxlY3RlZER1cmF0aW9uIiwic2VsZWN0ZWRTY29yZSIsInNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSIsImxvYW5Ub3RhbFNlbGVjdG9yIiwicGF5bWVudFNlbGVjdG9yIiwiaW50ZXJlc3RUb3RhbFNlbGVjdG9yIiwic2VydmljZUZlZVNlbGVjdG9yIiwidGF4VG90YWxTZWxlY3RvciIsInRvdGFsQW5udWFsQ29zdFNlbGVjdG9yIiwibG9hbkdyYW5kVG90YWxTZWxlY3RvciIsIlBsdWdpbiIsImVsZW1lbnQiLCJvcHRpb25zIiwiJGVsIiwiX25hbWUiLCJfZGVmYXVsdHMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImF0dGFjaExpc3RlbmVycyIsImluaXQiLCJwcm90b3R5cGUiLCJ2YWxpZGF0ZSIsInJlbmRlciIsImV2ZW50RW1pdHRlcnMiLCJqb2luIiwib24iLCJjaGFuZ2UiLCJldmVudEhhbmRsZXIiLCJiaW5kIiwibW91c2Vtb3ZlIiwidG91Y2htb3ZlIiwidXBkYXRlIiwiZmluZCIsInZhbCIsIl90b051bWVyaWMiLCJpc1BsYWluT2JqZWN0IiwiRXJyb3IiLCJjcmVkaXRSYXRlIiwiaXNOdW1lcmljIiwicGFyc2VGbG9hdCIsInRyaW0iLCJ0b1VwcGVyQ2FzZSIsImhhc093blByb3BlcnR5IiwiX2Rpc3BsYXlTZWxlY3RlZFZhbHVlcyIsIl9kaXNwbGF5UmVzdWx0cyIsImh0bWwiLCJfdG9Nb25leSIsIl9sb2FuVG90YWwiLCJfUE1UIiwiX2ludGVyZXN0VG90YWwiLCJfdGF4VG90YWwiLCJfdG9QZXJjZW50YWdlIiwiX0NBVCIsIl9zZXJ2aWNlRmVlV2l0aFZBVCIsIl9ncmFuZFRvdGFsIiwiYXJncyIsInRyaWdnZXIiLCJfcmVzdWx0cyIsImJhbGFuY2UiLCJpbml0aWFsIiwiaW50ZXJlc3RSYXRlIiwiX2ludGVyZXN0UmF0ZSIsIlZBVCIsIl92YWx1ZUFkZGVkVGF4IiwicGF5bWVudCIsIm51bWJlck9mUGF5bWVudHMiLCJfbnVtYmVyT2ZQYXltZW50cyIsInJlc3VsdHMiLCJwYXltZW50TnVtYmVyIiwiaW50ZXJlc3QiLCJ0YXhlc1BhaWQiLCJwcmluY2lwYWwiLCJwdXNoIiwidGF4Iiwic2NoZWR1bGUiLCJtYXAiLCJ2YWx1ZSIsIl9hbm51YWxJbnRlcmVzdFJhdGUiLCJfcGF5bWVudEZyZXF1ZW5jeSIsImR1cmF0aW9uSW5ZZWFycyIsIk1hdGgiLCJmbG9vciIsImkiLCJMIiwibiIsInBvdyIsInRvdGFsIiwiZWFjaCIsImluZGV4IiwiX3NlcnZpY2VGZWUiLCJJUlIiLCJfSVJSIiwiX2Nhc2hGbG93IiwicGVyaW9kcyIsImNhc2hGbG93IiwicGVyaW9kIiwidmFsdWVzIiwiZ3Vlc3MiLCJpcnJSZXN1bHQiLCJkYXRlcyIsInJhdGUiLCJyZXN1bHQiLCJsZW5ndGgiLCJpcnJSZXN1bHREZXJpdmF0aXZlIiwiZnJhYyIsInBvc2l0aXZlIiwibmVnYXRpdmUiLCJyZXN1bHRSYXRlIiwiZXBzTWF4IiwibmV3UmF0ZSIsImVwc1JhdGUiLCJyZXN1bHRWYWx1ZSIsImNvbnRMb29wIiwiYWJzIiwibnVtZXJpYyIsInRvRml4ZWQiLCJyZXBsYWNlIiwidG9TdHJpbmciLCJmbiIsImxvYW5DYWxjdWxhdG9yIiwiZGF0YSIsImluc3RhbmNlIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7Ozs7QUFNQTtBQUNBLENBQUEsVUFBQUEsQ0FBQSxFQUFBQyxNQUFBLEVBQUFDLFFBQUEsRUFBQUMsU0FBQSxFQUFBOztBQUVBOztBQUVBOzs7OztBQUlBLE1BQUFDLGVBQUE7QUFDQSxTQUFBLElBREE7QUFFQSxTQUFBLElBRkE7QUFHQSxTQUFBLEtBSEE7QUFJQSxTQUFBLEtBSkE7QUFLQSxTQUFBLEtBTEE7QUFNQSxTQUFBLEtBTkE7QUFPQSxTQUFBO0FBUEEsR0FBQTs7QUFVQTs7OztBQUlBLE1BQUFDLHNCQUFBO0FBQ0EsY0FBQSxFQURBO0FBRUEsZ0JBQUEsRUFGQTtBQUdBLGVBQUE7QUFIQSxHQUFBOztBQU1BOzs7O0FBSUEsTUFBQUMsZUFBQSxJQUFBOztBQUVBOzs7O0FBSUEsTUFBQUMsbUJBQUEsQ0FBQTs7QUFFQTs7OztBQUlBLE1BQUFDLFdBQUE7QUFDQTtBQUNBQyxnQkFBQSxLQUZBO0FBR0FDLGtCQUFBLEVBSEE7QUFJQUMsaUJBQUFQLFlBSkE7QUFLQVEsaUJBQUEsR0FMQTtBQU1BQyxtQkFBQSxDQU5BO0FBT0FDLGdCQUFBLENBUEE7QUFRQUMsc0JBQUEsU0FSQTs7QUFVQTtBQUNBQyx3QkFBQSxjQVhBO0FBWUFDLDBCQUFBLGdCQVpBO0FBYUFDLHlCQUFBLGVBYkE7QUFjQUMsOEJBQUEsb0JBZEE7O0FBZ0JBO0FBQ0FDLG9CQUFBLGtCQWpCQTtBQWtCQUMsc0JBQUEsb0JBbEJBO0FBbUJBQyxtQkFBQSxpQkFuQkE7QUFvQkFDLDhCQUFBLDZCQXBCQTs7QUFzQkE7QUFDQUMsdUJBQUEsYUF2QkE7QUF3QkFDLHFCQUFBLFVBeEJBO0FBeUJBQywyQkFBQSxpQkF6QkE7QUEwQkFDLHdCQUFBLGNBMUJBO0FBMkJBQyxzQkFBQSxZQTNCQTtBQTRCQUMsNkJBQUEsb0JBNUJBO0FBNkJBQyw0QkFBQTtBQTdCQSxHQUFBOztBQWdDQTs7Ozs7QUFLQSxXQUFBQyxNQUFBLENBQUFDLE9BQUEsRUFBQUMsT0FBQSxFQUFBO0FBQ0EsU0FBQUMsR0FBQSxHQUFBbEMsRUFBQWdDLE9BQUEsQ0FBQTtBQUNBLFNBQUFHLEtBQUEsR0FBQSxnQkFBQTtBQUNBLFNBQUFDLFNBQUEsR0FBQTVCLFFBQUE7QUFDQSxTQUFBNkIsUUFBQSxHQUFBckMsRUFBQXNDLE1BQUEsQ0FBQSxFQUFBLEVBQUE5QixRQUFBLEVBQUF5QixPQUFBLENBQUE7QUFDQSxTQUFBTSxlQUFBO0FBQ0EsU0FBQUMsSUFBQTtBQUNBOztBQUVBO0FBQ0F4QyxJQUFBc0MsTUFBQSxDQUFBUCxPQUFBVSxTQUFBLEVBQUE7O0FBRUE7Ozs7QUFJQUQsVUFBQSxZQUFBO0FBQ0EsV0FBQUUsUUFBQTtBQUNBLFdBQUFDLE1BQUE7QUFDQSxLQVRBOztBQVdBOzs7O0FBSUFKLHFCQUFBLFlBQUE7QUFDQSxVQUFBSyxnQkFBQSxDQUNBLEtBQUFQLFFBQUEsQ0FBQXJCLGtCQURBLEVBRUEsS0FBQXFCLFFBQUEsQ0FBQXBCLG9CQUZBLEVBR0EsS0FBQW9CLFFBQUEsQ0FBQW5CLG1CQUhBLEVBSUEsS0FBQW1CLFFBQUEsQ0FBQWxCLHdCQUpBLENBQUE7O0FBT0FuQixRQUFBNEMsY0FBQUMsSUFBQSxFQUFBLEVBQUFDLEVBQUEsQ0FBQTtBQUNBQyxnQkFBQSxLQUFBQyxZQUFBLENBQUFDLElBQUEsQ0FBQSxJQUFBLENBREE7QUFFQUMsbUJBQUEsS0FBQUYsWUFBQSxDQUFBQyxJQUFBLENBQUEsSUFBQSxDQUZBO0FBR0FFLG1CQUFBLEtBQUFILFlBQUEsQ0FBQUMsSUFBQSxDQUFBLElBQUE7QUFIQSxPQUFBO0FBS0EsS0E1QkE7O0FBOEJBOzs7O0FBSUFELGtCQUFBLFlBQUE7QUFDQSxXQUFBSSxNQUFBLENBQUE7QUFDQTNDLG9CQUFBLEtBQUF5QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXJCLGtCQUFBLEVBQUFzQyxHQUFBLEVBREE7QUFFQTVDLHNCQUFBLEtBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXBCLG9CQUFBLEVBQUFxQyxHQUFBLEVBRkE7QUFHQTFDLHFCQUFBLEtBQUFzQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQW5CLG1CQUFBLEVBQUFvQyxHQUFBLEVBSEE7QUFJQXZDLDBCQUFBLEtBQUFtQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWxCLHdCQUFBLEVBQUFtQyxHQUFBO0FBSkEsT0FBQTtBQU1BLEtBekNBOztBQTJDQTs7Ozs7QUFLQVosY0FBQSxZQUFBO0FBQ0EsVUFBQSxPQUFBLEtBQUFMLFFBQUEsQ0FBQTVCLFVBQUEsS0FBQSxRQUFBLEVBQUE7QUFDQSxhQUFBNEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBLEtBQUE4QyxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTVCLFVBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsT0FBQSxLQUFBNEIsUUFBQSxDQUFBdkIsVUFBQSxLQUFBLFFBQUEsRUFBQTtBQUNBLGFBQUF1QixRQUFBLENBQUF2QixVQUFBLEdBQUEsS0FBQXlDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBZCxFQUFBd0QsYUFBQSxDQUFBLEtBQUFuQixRQUFBLENBQUExQixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQThDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsV0FBQSxJQUFBQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsRUFBQTtBQUNBLFlBQUEsT0FBQSxLQUFBMEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLEtBQUEsUUFBQSxFQUFBO0FBQ0EsZUFBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxLQUFBSCxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsQ0FBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxDQUFBMUQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLENBQUEsRUFBQTtBQUNBLGdCQUFBLElBQUFELEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxJQUFBLENBQUEsRUFBQTtBQUNBLGVBQUFyQixRQUFBLENBQUExQixXQUFBLENBQUErQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxHQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBQUFyQixRQUFBLENBQUE1QixVQUFBLEdBQUFtRCxXQUFBLEtBQUF2QixRQUFBLENBQUE1QixVQUFBLENBQUE7QUFDQSxXQUFBNEIsUUFBQSxDQUFBM0IsWUFBQSxHQUFBa0QsV0FBQSxLQUFBdkIsUUFBQSxDQUFBM0IsWUFBQSxDQUFBO0FBQ0EsV0FBQTJCLFFBQUEsQ0FBQXZCLFVBQUEsR0FBQThDLFdBQUEsS0FBQXZCLFFBQUEsQ0FBQXZCLFVBQUEsQ0FBQTtBQUNBLFdBQUF1QixRQUFBLENBQUF6QixXQUFBLEdBQUFaLEVBQUE2RCxJQUFBLENBQUEsS0FBQXhCLFFBQUEsQ0FBQXpCLFdBQUEsQ0FBQWtELFdBQUEsRUFBQSxDQUFBOztBQUVBLFVBQUEsQ0FBQXpELG9CQUFBMEQsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF0QixnQkFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEwQyxLQUFBLENBQUEseURBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBb0QsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF6QixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQTZDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxLQUFBcEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBSCxZQUFBLEVBQUE7QUFDQSxjQUFBLElBQUFtRCxLQUFBLENBQUEsNERBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsS0FBQXBCLFFBQUEsQ0FBQTNCLFlBQUEsR0FBQUgsZ0JBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQWtELEtBQUEsQ0FBQSwyREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBekQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEyQyxLQUFBLENBQUEsbURBQUEsQ0FBQTtBQUNBO0FBQ0EsS0FwR0E7O0FBc0dBOzs7O0FBSUFkLFlBQUEsWUFBQTtBQUNBLFdBQUFxQixzQkFBQTtBQUNBLFdBQUFDLGVBQUE7QUFDQSxLQTdHQTs7QUErR0E7Ozs7QUFJQUQsNEJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQTlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBakIsY0FBQSxFQUFBOEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBOUIsUUFBQSxDQUFBNUIsVUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBaEIsZ0JBQUEsRUFBQTZDLElBQUEsQ0FDQSxLQUFBN0IsUUFBQSxDQUFBM0IsWUFEQTs7QUFJQTtBQUNBLFdBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWYsYUFBQSxFQUFBNEMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF6QixXQURBOztBQUlBO0FBQ0EsV0FBQXNCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBZCx3QkFBQSxFQUFBMkMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF0QixnQkFEQTtBQUdBLEtBdklBOztBQXlJQTs7OztBQUlBa0QscUJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQS9CLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBYixpQkFBQSxFQUFBMEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBQyxVQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFsQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVosZUFBQSxFQUFBeUMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBRSxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFuQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVgscUJBQUEsRUFBQXdDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQUcsY0FBQSxFQUFBLENBREE7O0FBSUE7QUFDQSxXQUFBcEMsR0FBQSxDQUFBbUIsSUFBQSxDQUFBLEtBQUFoQixRQUFBLENBQUFULGdCQUFBLEVBQUFzQyxJQUFBLENBQ0EsS0FBQUMsUUFBQSxDQUFBLEtBQUFJLFNBQUEsRUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXJDLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBUix1QkFBQSxFQUFBcUMsSUFBQSxDQUNBLEtBQUFNLGFBQUEsQ0FBQSxLQUFBQyxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUF2QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVYsa0JBQUEsRUFBQXVDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQU8sa0JBQUEsRUFBQSxDQURBOztBQUlBLFdBQUF4QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVAsc0JBQUEsRUFBQW9DLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQVEsV0FBQSxFQUFBLENBREE7QUFHQSxLQS9LQTs7QUFpTEE7Ozs7QUFJQXZCLFlBQUEsVUFBQXdCLElBQUEsRUFBQTtBQUNBLFdBQUF2QyxRQUFBLEdBQUFyQyxFQUFBc0MsTUFBQSxDQUFBLEVBQUEsRUFBQSxLQUFBRixTQUFBLEVBQUEsS0FBQUMsUUFBQSxFQUFBdUMsSUFBQSxDQUFBO0FBQ0EsV0FBQXBDLElBQUE7QUFDQSxXQUFBTixHQUFBLENBQUEyQyxPQUFBLENBQUEsYUFBQTtBQUNBLEtBekxBOztBQTJMQTs7Ozs7QUFLQUMsY0FBQSxZQUFBO0FBQ0EsVUFBQUMsVUFBQSxLQUFBMUMsUUFBQSxDQUFBNUIsVUFBQTtBQUNBLFVBQUF1RSxVQUFBLEtBQUEzQyxRQUFBLENBQUE1QixVQUFBO0FBQ0EsVUFBQXdFLGVBQUEsS0FBQUMsYUFBQSxFQUFBO0FBQ0EsVUFBQUMsTUFBQSxLQUFBQyxjQUFBLEVBQUE7QUFDQSxVQUFBQyxVQUFBLEtBQUFoQixJQUFBLEVBQUE7QUFDQSxVQUFBaUIsbUJBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLFVBQUFDLFVBQUEsRUFBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFBLElBQUFDLGdCQUFBLENBQUEsRUFBQUEsZ0JBQUFILGdCQUFBLEVBQUFHLGVBQUEsRUFBQTtBQUNBLFlBQUFDLFdBQUFYLFVBQUFFLFlBQUE7QUFDQSxZQUFBVSxZQUFBWixVQUFBRSxZQUFBLEdBQUFFLEdBQUE7QUFDQSxZQUFBUyxZQUFBUCxVQUFBSyxRQUFBLEdBQUFDLFNBQUE7O0FBRUE7QUFDQVgsa0JBQUFELE9BQUE7O0FBRUE7QUFDQUEsa0JBQUFBLFVBQUFhLFNBQUE7O0FBRUFKLGdCQUFBSyxJQUFBLENBQUE7QUFDQWIsbUJBQUFBLE9BREE7QUFFQVkscUJBQUFBLFNBRkE7QUFHQUYsb0JBQUFBLFFBSEE7QUFJQUksZUFBQUgsU0FKQTtBQUtBTixtQkFBQUEsT0FMQTtBQU1BTixtQkFBQUE7QUFOQSxTQUFBO0FBUUE7O0FBRUEsYUFBQVMsT0FBQTtBQUNBLEtBbE9BOztBQW9PQTs7OztBQUlBTyxjQUFBLFlBQUE7QUFDQSxhQUFBL0YsRUFBQWdHLEdBQUEsQ0FBQSxLQUFBbEIsUUFBQSxFQUFBLEVBQUEsVUFBQW1CLEtBQUEsRUFBQTtBQUNBLGVBQUE7QUFDQWpCLG1CQUFBLEtBQUFiLFFBQUEsQ0FBQThCLE1BQUFqQixPQUFBLENBREE7QUFFQVkscUJBQUEsS0FBQXpCLFFBQUEsQ0FBQThCLE1BQUFMLFNBQUEsQ0FGQTtBQUdBRixvQkFBQSxLQUFBdkIsUUFBQSxDQUFBOEIsTUFBQVAsUUFBQSxDQUhBO0FBSUFJLGVBQUEsS0FBQTNCLFFBQUEsQ0FBQThCLE1BQUFILEdBQUEsQ0FKQTtBQUtBVCxtQkFBQSxLQUFBbEIsUUFBQSxDQUFBOEIsTUFBQVosT0FBQSxDQUxBO0FBTUFOLG1CQUFBLEtBQUFaLFFBQUEsQ0FBQThCLE1BQUFsQixPQUFBO0FBTkEsU0FBQTtBQVFBLE9BVEEsQ0FTQTlCLElBVEEsQ0FTQSxJQVRBLENBQUEsQ0FBQTtBQVVBLEtBblBBOztBQXFQQTs7OztBQUlBdEMsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQTBCLFFBQUEsQ0FBQTFCLFdBQUE7QUFDQSxLQTNQQTs7QUE2UEE7Ozs7QUFJQXVGLHlCQUFBLFlBQUE7QUFDQSxVQUFBLEtBQUE3RCxRQUFBLENBQUEwQixjQUFBLENBQUEsY0FBQSxDQUFBLEVBQUE7QUFDQSxZQUFBLEtBQUExQixRQUFBLENBQUE0QyxZQUFBLElBQUEsQ0FBQSxFQUFBO0FBQ0EsaUJBQUEsS0FBQTVDLFFBQUEsQ0FBQTRDLFlBQUE7QUFDQTs7QUFFQSxlQUFBLEtBQUExQixVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTRDLFlBQUEsSUFBQSxHQUFBO0FBQ0E7O0FBRUEsYUFBQSxLQUFBNUMsUUFBQSxDQUFBMUIsV0FBQSxDQUFBLEtBQUEwQixRQUFBLENBQUF6QixXQUFBLElBQUEsR0FBQTtBQUNBLEtBM1FBOztBQTZRQTs7OztBQUlBc0UsbUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQWdCLG1CQUFBLEtBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLEtBblJBOztBQXNSQTs7OztBQUlBQSx1QkFBQSxZQUFBO0FBQ0EsYUFBQTlGLG9CQUFBLEtBQUFnQyxRQUFBLENBQUF0QixnQkFBQSxDQUFBO0FBQ0EsS0E1UkE7O0FBOFJBOzs7O0FBSUF3RSx1QkFBQSxZQUFBO0FBQ0EsVUFBQWEsa0JBQUEsS0FBQTdDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBM0IsWUFBQSxJQUFBLEVBQUE7O0FBRUEsYUFBQTJGLEtBQUFDLEtBQUEsQ0FBQUYsa0JBQUEvRixvQkFBQSxLQUFBZ0MsUUFBQSxDQUFBdEIsZ0JBQUEsQ0FBQSxDQUFBO0FBQ0EsS0F0U0E7O0FBd1NBOzs7O0FBSUFxRCxnQkFBQSxZQUFBO0FBQ0EsYUFBQSxLQUFBQyxJQUFBLEtBQUEsS0FBQWtCLGlCQUFBLEVBQUE7QUFDQSxLQTlTQTs7QUFnVEE7Ozs7O0FBS0FsQixVQUFBLFlBQUE7QUFDQSxVQUFBa0MsSUFBQSxLQUFBckIsYUFBQSxFQUFBO0FBQ0EsVUFBQXNCLElBQUEsS0FBQW5FLFFBQUEsQ0FBQTVCLFVBQUE7QUFDQSxVQUFBZ0csSUFBQSxLQUFBbEIsaUJBQUEsRUFBQTs7QUFFQSxVQUFBLEtBQUFsRCxRQUFBLENBQUF4QixhQUFBLEtBQUEsQ0FBQSxFQUFBO0FBQ0EwRixZQUFBLENBQUEsSUFBQSxLQUFBbkIsY0FBQSxFQUFBLElBQUFtQixDQUFBLENBREEsQ0FDQTtBQUNBOztBQUVBLGFBQUFDLElBQUFELENBQUEsSUFBQSxJQUFBRixLQUFBSyxHQUFBLENBQUEsSUFBQUgsQ0FBQSxFQUFBLENBQUFFLENBQUEsQ0FBQSxDQUFBO0FBQ0EsS0EvVEE7O0FBaVVBOzs7O0FBSUFuQyxvQkFBQSxZQUFBO0FBQ0EsVUFBQXFDLFFBQUEsQ0FBQTtBQUNBM0csUUFBQTRHLElBQUEsQ0FBQSxLQUFBOUIsUUFBQSxFQUFBLEVBQUEsVUFBQStCLEtBQUEsRUFBQVosS0FBQSxFQUFBO0FBQ0FVLGlCQUFBVixNQUFBUCxRQUFBO0FBQ0EsT0FGQTtBQUdBLGFBQUFpQixLQUFBO0FBQ0EsS0EzVUE7O0FBNlVBOzs7O0FBSUFwQyxlQUFBLFlBQUE7QUFDQSxVQUFBb0MsUUFBQSxDQUFBO0FBQ0EzRyxRQUFBNEcsSUFBQSxDQUFBLEtBQUE5QixRQUFBLEVBQUEsRUFBQSxVQUFBK0IsS0FBQSxFQUFBWixLQUFBLEVBQUE7QUFDQVUsaUJBQUFWLE1BQUFILEdBQUE7QUFDQSxPQUZBO0FBR0EsYUFBQWEsS0FBQTtBQUNBLEtBdlZBOztBQXlWQTs7OztBQUlBRyxpQkFBQSxZQUFBO0FBQ0EsVUFBQWhHLGFBQUEsS0FBQXVCLFFBQUEsQ0FBQXZCLFVBQUE7O0FBRUE7QUFDQTtBQUNBLFVBQUFBLGFBQUEsQ0FBQSxFQUFBO0FBQ0FBLHFCQUFBQSxhQUFBLEdBQUE7QUFDQTs7QUFFQSxhQUFBLEtBQUF1QixRQUFBLENBQUE1QixVQUFBLEdBQUFLLFVBQUE7QUFDQSxLQXZXQTs7QUF5V0E7Ozs7QUFJQTRELHdCQUFBLFlBQUE7QUFDQSxhQUFBLEtBQUFvQyxXQUFBLE1BQUEsS0FBQTFCLGNBQUEsS0FBQSxDQUFBLENBQUE7QUFDQSxLQS9XQTs7QUFpWEE7Ozs7QUFJQVQsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQVAsVUFBQSxLQUFBLEtBQUFNLGtCQUFBLEVBQUE7QUFDQSxLQXZYQTs7QUF5WEE7Ozs7O0FBS0FELFVBQUEsWUFBQTtBQUNBLFVBQUFzQyxNQUFBLEtBQUFDLElBQUEsQ0FBQSxLQUFBQyxTQUFBLEVBQUEsQ0FBQTtBQUNBLFVBQUFDLFVBQUEsS0FBQWYsaUJBQUEsRUFBQTs7QUFFQSxhQUFBRSxLQUFBSyxHQUFBLENBQUEsSUFBQUssR0FBQSxFQUFBRyxPQUFBLElBQUEsQ0FBQTtBQUNBLEtBbllBOztBQXFZQTs7OztBQUlBRCxlQUFBLFlBQUE7QUFDQSxVQUFBekIsVUFBQSxLQUFBVixRQUFBLEVBQUE7QUFDQSxVQUFBcUMsV0FBQSxDQUFBLEtBQUFMLFdBQUEsS0FBQSxLQUFBekUsUUFBQSxDQUFBNUIsVUFBQSxDQUFBOztBQUVBVCxRQUFBNEcsSUFBQSxDQUFBcEIsT0FBQSxFQUFBLFVBQUFxQixLQUFBLEVBQUFPLE1BQUEsRUFBQTtBQUNBRCxpQkFBQXRCLElBQUEsQ0FBQXVCLE9BQUEvQixPQUFBLEdBQUErQixPQUFBdEIsR0FBQTtBQUNBLE9BRkEsQ0FFQTdDLElBRkEsQ0FFQSxJQUZBLENBQUE7O0FBSUEsYUFBQWtFLFFBQUE7QUFDQSxLQWxaQTs7QUFvWkE7Ozs7OztBQU1BSCxVQUFBLFVBQUFLLE1BQUEsRUFBQUMsS0FBQSxFQUFBO0FBQ0FBLGNBQUFBLFNBQUEsQ0FBQTs7QUFFQTtBQUNBLFVBQUFDLFlBQUEsVUFBQUYsTUFBQSxFQUFBRyxLQUFBLEVBQUFDLElBQUEsRUFBQTtBQUNBLFlBQUFDLFNBQUFMLE9BQUEsQ0FBQSxDQUFBOztBQUVBLGFBQUEsSUFBQWQsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBbUIsb0JBQUFMLE9BQUFkLENBQUEsSUFBQUYsS0FBQUssR0FBQSxDQUFBZSxPQUFBLENBQUEsRUFBQSxDQUFBRCxNQUFBakIsQ0FBQSxJQUFBaUIsTUFBQSxDQUFBLENBQUEsSUFBQSxHQUFBLENBQUE7QUFDQTs7QUFFQSxlQUFBRSxNQUFBO0FBQ0EsT0FSQTs7QUFVQTtBQUNBLFVBQUFFLHNCQUFBLFVBQUFQLE1BQUEsRUFBQUcsS0FBQSxFQUFBQyxJQUFBLEVBQUE7QUFDQSxZQUFBQyxTQUFBLENBQUE7O0FBRUEsYUFBQSxJQUFBbkIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBLGNBQUFzQixPQUFBLENBQUFMLE1BQUFqQixDQUFBLElBQUFpQixNQUFBLENBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQUUsb0JBQUFHLE9BQUFSLE9BQUFkLENBQUEsQ0FBQSxHQUFBRixLQUFBSyxHQUFBLENBQUFlLE9BQUEsQ0FBQSxFQUFBSSxPQUFBLENBQUEsQ0FBQTtBQUNBOztBQUVBLGVBQUFILE1BQUE7QUFDQSxPQVRBOztBQVdBO0FBQ0E7QUFDQSxVQUFBRixRQUFBLEVBQUE7QUFDQSxVQUFBTSxXQUFBLEtBQUE7QUFDQSxVQUFBQyxXQUFBLEtBQUE7O0FBRUEsV0FBQSxJQUFBeEIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBaUIsY0FBQWpCLENBQUEsSUFBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxHQUFBaUIsTUFBQWpCLElBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQSxZQUFBYyxPQUFBZCxDQUFBLElBQUEsQ0FBQSxFQUFBdUIsV0FBQSxJQUFBO0FBQ0EsWUFBQVQsT0FBQWQsQ0FBQSxJQUFBLENBQUEsRUFBQXdCLFdBQUEsSUFBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQUQsUUFBQSxJQUFBLENBQUFDLFFBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQXRFLEtBQUEsQ0FDQSxzRkFEQSxDQUFBO0FBR0E7O0FBRUE7QUFDQTZELGNBQUFBLFVBQUFuSCxTQUFBLEdBQUEsR0FBQSxHQUFBbUgsS0FBQTtBQUNBLFVBQUFVLGFBQUFWLEtBQUE7O0FBRUE7QUFDQSxVQUFBVyxTQUFBLEtBQUE7O0FBRUE7QUFDQSxVQUFBQyxPQUFBLEVBQUFDLE9BQUEsRUFBQUMsV0FBQTtBQUNBLFVBQUFDLFdBQUEsSUFBQTs7QUFFQSxTQUFBO0FBQ0FELHNCQUFBYixVQUFBRixNQUFBLEVBQUFHLEtBQUEsRUFBQVEsVUFBQSxDQUFBO0FBQ0FFLGtCQUFBRixhQUFBSSxjQUFBUixvQkFBQVAsTUFBQSxFQUFBRyxLQUFBLEVBQUFRLFVBQUEsQ0FBQTtBQUNBRyxrQkFBQTlCLEtBQUFpQyxHQUFBLENBQUFKLFVBQUFGLFVBQUEsQ0FBQTtBQUNBQSxxQkFBQUUsT0FBQTtBQUNBRyxtQkFBQUYsVUFBQUYsTUFBQSxJQUFBNUIsS0FBQWlDLEdBQUEsQ0FBQUYsV0FBQSxJQUFBSCxNQUFBO0FBQ0EsT0FOQSxRQU1BSSxRQU5BOztBQVFBO0FBQ0EsYUFBQUwsVUFBQTtBQUNBLEtBM2RBOztBQTZkQTs7OztBQUlBNUMsb0JBQUEsWUFBQTtBQUNBLFVBQUFVLE1BQUEsS0FBQXZDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBeEIsYUFBQSxJQUFBLENBQUEsQ0FBQTs7QUFFQTtBQUNBO0FBQ0EsYUFBQWlGLE1BQUEsQ0FBQSxHQUFBQSxNQUFBLEdBQUEsR0FBQUEsR0FBQTtBQUNBLEtBdmVBOztBQXllQTs7Ozs7QUFLQTNCLGNBQUEsVUFBQW9FLE9BQUEsRUFBQTtBQUNBLFVBQUEsT0FBQUEsT0FBQSxJQUFBLFFBQUEsRUFBQTtBQUNBQSxrQkFBQTNFLFdBQUEyRSxPQUFBLENBQUE7QUFDQTs7QUFFQSxhQUFBLFNBQUFBLFFBQUFDLE9BQUEsQ0FBQSxDQUFBLEVBQUFDLE9BQUEsQ0FBQSxxQkFBQSxFQUFBLEtBQUEsQ0FBQTtBQUNBLEtBcGZBOztBQXNmQTs7Ozs7QUFLQWxGLGdCQUFBLFVBQUEwQyxLQUFBLEVBQUE7QUFDQSxhQUFBckMsV0FDQXFDLE1BQUF5QyxRQUFBLEdBQUFELE9BQUEsQ0FBQSxZQUFBLEVBQUEsRUFBQSxDQURBLENBQUE7QUFHQSxLQS9mQTs7QUFpZ0JBOzs7OztBQUtBakUsbUJBQUEsVUFBQStELE9BQUEsRUFBQTtBQUNBLGFBQUEsQ0FBQUEsVUFBQSxHQUFBLEVBQUFDLE9BQUEsQ0FBQSxDQUFBLElBQUEsR0FBQTtBQUNBOztBQXhnQkEsR0FBQTs7QUE0Z0JBOzs7QUFHQXhJLElBQUEySSxFQUFBLENBQUFDLGNBQUEsR0FBQSxVQUFBM0csT0FBQSxFQUFBMkMsSUFBQSxFQUFBO0FBQ0EsUUFBQTNDLFlBQUEsVUFBQSxFQUFBO0FBQ0EsYUFBQSxLQUFBNEcsSUFBQSxDQUFBLHVCQUFBLEVBQUE5QyxRQUFBLEVBQUE7QUFDQTs7QUFFQSxRQUFBOUQsWUFBQSxPQUFBLEVBQUE7QUFDQSxhQUFBLEtBQUE0RyxJQUFBLENBQUEsdUJBQUEsRUFBQWxJLFdBQUEsRUFBQTtBQUNBOztBQUVBLFdBQUEsS0FBQWlHLElBQUEsQ0FBQSxZQUFBO0FBQ0EsVUFBQWtDLFdBQUE5SSxFQUFBNkksSUFBQSxDQUFBLElBQUEsRUFBQSx1QkFBQSxDQUFBO0FBQ0EsVUFBQSxDQUFBQyxRQUFBLEVBQUE7QUFDQTlJLFVBQUE2SSxJQUFBLENBQUEsSUFBQSxFQUFBLHVCQUFBLEVBQUEsSUFBQTlHLE1BQUEsQ0FBQSxJQUFBLEVBQUFFLE9BQUEsQ0FBQTtBQUNBLE9BRkEsTUFFQSxJQUFBQSxZQUFBLFFBQUEsRUFBQTtBQUNBLGVBQUE2RyxTQUFBMUYsTUFBQSxDQUFBd0IsSUFBQSxDQUFBO0FBQ0E7QUFDQSxLQVBBLENBQUE7QUFRQSxHQWpCQTtBQW1CQSxDQTduQkEsRUE2bkJBbUUsTUE3bkJBLEVBNm5CQTlJLE1BN25CQSxFQTZuQkFDLFFBN25CQSIsImZpbGUiOiJsb2FuLWNhbGN1bGF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqIGpRdWVyeSBMb2FuIENhbGN1bGF0b3IgMy4xLjNcbiAqXG4gKiBBdXRob3I6IEpvcmdlIEdvbnrDoWxleiA8c2NydWIubXhAZ21haWw+XG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgLSBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICovXG47XG4oZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xuXG4gIFwidXNlIHN0cmljdFwiO1xuXG4gIC8qKlxuICAgKiBUYWJsZSBvZiBjcmVkaXQgcmF0ZXMgZm9yIGV2ZXJ5IHNjb3JlLlxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKi9cbiAgdmFyIENSRURJVF9SQVRFUyA9IHtcbiAgICAnQSc6IDUuMzIsXG4gICAgJ0InOiA4LjE4LFxuICAgICdDJzogMTIuMjksXG4gICAgJ0QnOiAxNS42MSxcbiAgICAnRSc6IDE4LjI1LFxuICAgICdGJzogMjEuOTksXG4gICAgJ0cnOiAyNi43N1xuICB9O1xuXG4gIC8qKlxuICAgKiBUYWJsZSBvZiBhbGxvd2VkIHBheW1lbnQgZnJlcXVlbmNpZXMuXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICB2YXIgUEFZTUVOVF9GUkVRVUVOQ0lFUyA9IHtcbiAgICAnd2Vla2x5JzogNTIsXG4gICAgJ2Jpd2Vla2x5JzogMjYsXG4gICAgJ21vbnRobHknOiAxMlxuICB9O1xuXG4gIC8qKlxuICAgKiBUaGUgbWluaW11bSBhbGxvd2VkIGZvciBhIGxvYW4uXG4gICAqIEB0eXBlIHtOdW1iZXJ9XG4gICAqL1xuICB2YXIgTUlOSU1VTV9MT0FOID0gMTAwMDtcblxuICAvKipcbiAgICogVGhlIG1pbmltdW0gZHVyYXRpb24gaW4gbW9udGhzLlxuICAgKiBAdHlwZSB7TnVtYmVyfVxuICAgKi9cbiAgdmFyIE1JTklNVU1fRFVSQVRJT04gPSAxO1xuXG4gIC8qKlxuICAgKiBEZWZhdWx0IG9wdGlvbnMgZm9yIHRoZSBwbHVnaW4uXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICB2YXIgZGVmYXVsdHMgPSB7XG4gICAgLy8gZGVmYXVsdCB2YWx1ZXMgZm9yIGEgbG9hblxuICAgIGxvYW5BbW91bnQ6IDUwMDAwLFxuICAgIGxvYW5EdXJhdGlvbjogMTIsXG4gICAgY3JlZGl0UmF0ZXM6IENSRURJVF9SQVRFUyxcbiAgICBjcmVkaXRTY29yZTogJ0EnLFxuICAgIHZhbHVlQWRkZWRUYXg6IDAsXG4gICAgc2VydmljZUZlZTogMCxcbiAgICBwYXltZW50RnJlcXVlbmN5OiAnbW9udGhseScsXG5cbiAgICAvLyBpbnB1dHNcbiAgICBsb2FuQW1vdW50U2VsZWN0b3I6ICcjbG9hbi1hbW91bnQnLFxuICAgIGxvYW5EdXJhdGlvblNlbGVjdG9yOiAnI2xvYW4tZHVyYXRpb24nLFxuICAgIGNyZWRpdFNjb3JlU2VsZWN0b3I6ICcjY3JlZGl0LXNjb3JlJyxcbiAgICBwYXltZW50RnJlcXVlbmN5U2VsZWN0b3I6ICcjcGF5bWVudC1mcmVxdWVuY3knLFxuXG4gICAgLy8gZGlzcGxheSBzZWxlY3RlZCB2YWx1ZXNcbiAgICBzZWxlY3RlZEFtb3VudDogJyNzZWxlY3RlZC1hbW91bnQnLFxuICAgIHNlbGVjdGVkRHVyYXRpb246ICcjc2VsZWN0ZWQtZHVyYXRpb24nLFxuICAgIHNlbGVjdGVkU2NvcmU6ICcjc2VsZWN0ZWQtc2NvcmUnLFxuICAgIHNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeTogJyNzZWxlY3RlZC1wYXltZW50LWZyZXF1ZW5jeScsXG5cbiAgICAvLyBkaXNwbGF5IHRoZSByZXN1bHRzXG4gICAgbG9hblRvdGFsU2VsZWN0b3I6ICcjbG9hbi10b3RhbCcsXG4gICAgcGF5bWVudFNlbGVjdG9yOiAnI3BheW1lbnQnLFxuICAgIGludGVyZXN0VG90YWxTZWxlY3RvcjogJyNpbnRlcmVzdC10b3RhbCcsXG4gICAgc2VydmljZUZlZVNlbGVjdG9yOiAnI3NlcnZpY2UtZmVlJyxcbiAgICB0YXhUb3RhbFNlbGVjdG9yOiAnI3RheC10b3RhbCcsXG4gICAgdG90YWxBbm51YWxDb3N0U2VsZWN0b3I6ICcjdG90YWwtYW5udWFsLWNvc3QnLFxuICAgIGxvYW5HcmFuZFRvdGFsU2VsZWN0b3I6ICcjZ3JhbmQtdG90YWwnXG4gIH07XG5cbiAgLyoqXG4gICAqIFRoZSBhY3R1YWwgcGx1Z2luIGNvbnN0cnVjdG9yXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBlbGVtZW50XG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAqL1xuICBmdW5jdGlvbiBQbHVnaW4oZWxlbWVudCwgb3B0aW9ucykge1xuICAgIHRoaXMuJGVsID0gJChlbGVtZW50KTtcbiAgICB0aGlzLl9uYW1lID0gJ2xvYW5DYWxjdWxhdG9yJztcbiAgICB0aGlzLl9kZWZhdWx0cyA9IGRlZmF1bHRzO1xuICAgIHRoaXMuc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSwgZGVmYXVsdHMsIG9wdGlvbnMpO1xuICAgIHRoaXMuYXR0YWNoTGlzdGVuZXJzKCk7XG4gICAgdGhpcy5pbml0KCk7XG4gIH1cblxuICAvLyBBdm9pZCBQbHVnaW4ucHJvdG90eXBlIGNvbmZsaWN0c1xuICAkLmV4dGVuZChQbHVnaW4ucHJvdG90eXBlLCB7XG5cbiAgICAvKipcbiAgICAgKiBWYWxpZGF0ZXMgdGhlIGRhdGEgYW5kIHNob3dzIHRoZSByZXN1bHRzLlxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy52YWxpZGF0ZSgpO1xuICAgICAgdGhpcy5yZW5kZXIoKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQXR0YWNoIGV2ZW50IGxpc3RlbmVycyB0byB0aGUgZXZlbnQgaGFuZGxlcnMuXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICBhdHRhY2hMaXN0ZW5lcnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBldmVudEVtaXR0ZXJzID0gW1xuICAgICAgICB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnRTZWxlY3RvcixcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb25TZWxlY3RvcixcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZVNlbGVjdG9yLFxuICAgICAgICB0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3lTZWxlY3RvclxuICAgICAgXTtcblxuICAgICAgJChldmVudEVtaXR0ZXJzLmpvaW4oKSkub24oe1xuICAgICAgICBjaGFuZ2U6IHRoaXMuZXZlbnRIYW5kbGVyLmJpbmQodGhpcyksXG4gICAgICAgIG1vdXNlbW92ZTogdGhpcy5ldmVudEhhbmRsZXIuYmluZCh0aGlzKSxcbiAgICAgICAgdG91Y2htb3ZlOiB0aGlzLmV2ZW50SGFuZGxlci5iaW5kKHRoaXMpXG4gICAgICB9KTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogSGFuZGxlIGV2ZW50cyBmcm9tIHRoZSBET00uXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICBldmVudEhhbmRsZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMudXBkYXRlKHtcbiAgICAgICAgbG9hbkFtb3VudDogdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnRTZWxlY3RvcikudmFsKCksXG4gICAgICAgIGxvYW5EdXJhdGlvbjogdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvblNlbGVjdG9yKS52YWwoKSxcbiAgICAgICAgY3JlZGl0U2NvcmU6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZVNlbGVjdG9yKS52YWwoKSxcbiAgICAgICAgcGF5bWVudEZyZXF1ZW5jeTogdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3lTZWxlY3RvcikudmFsKClcbiAgICAgIH0pO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBTYW5pdGl6ZSBhbmQgdmFsaWRhdGUgdGhlIHVzZXIgaW5wdXQgZGF0YS5cbiAgICAgKiBAdGhyb3dzIEVycm9yXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICB2YWxpZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHR5cGVvZiB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQpO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlID0gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSk7XG4gICAgICB9XG5cbiAgICAgIGlmICghJC5pc1BsYWluT2JqZWN0KHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXMpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbY3JlZGl0UmF0ZXNdIGlzIG5vdCB2YWxpZC4nKTtcbiAgICAgIH1cblxuICAgICAgZm9yICh2YXIgY3JlZGl0UmF0ZSBpbiB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzKSB7XG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdID0gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0pXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoISQuaXNOdW1lcmljKHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0pKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtjcmVkaXRSYXRlc10gaXMgbm90IHZhbGlkLicpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA8IDEpIHtcbiAgICAgICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdID0gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSAqIDEwMDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBTYW5pdGl6ZSB0aGUgaW5wdXRcbiAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCA9IHBhcnNlRmxvYXQodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50KTtcbiAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uID0gcGFyc2VGbG9hdCh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvbik7XG4gICAgICB0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUgPSBwYXJzZUZsb2F0KHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSk7XG4gICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlID0gJC50cmltKHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmUudG9VcHBlckNhc2UoKSk7XG5cbiAgICAgIGlmICghUEFZTUVOVF9GUkVRVUVOQ0lFUy5oYXNPd25Qcm9wZXJ0eSh0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3kpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbcGF5bWVudEZyZXF1ZW5jeV0gaXMgbm90IHZhbGlkLicpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIXRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXMuaGFzT3duUHJvcGVydHkodGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtjcmVkaXRTY29yZV0gaXMgbm90IHZhbGlkLicpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50IDwgTUlOSU1VTV9MT0FOKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbbG9hbkFtb3VudF0gbXVzdCBtZSBhdCBsZWFzdCAxMDAwLicpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb24gPCBNSU5JTVVNX0RVUkFUSU9OKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbbG9hbkR1cmF0aW9uXSBtdXN0IG1lIGF0IGxlYXN0IDEuJyk7XG4gICAgICB9XG5cbiAgICAgIGlmICghJC5pc051bWVyaWModGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlKSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW3NlcnZpY2VGZWVdIGlzIG5vdCB2YWxpZC4nKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogU2hvdyB0aGUgcmVzdWx0cyBpbiB0aGUgRE9NLlxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLl9kaXNwbGF5U2VsZWN0ZWRWYWx1ZXMoKTtcbiAgICAgIHRoaXMuX2Rpc3BsYXlSZXN1bHRzKCk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFNob3cgdGhlIHNlbGVjdGVkIHZhbHVlcyBpbiB0aGUgRE9NLlxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgX2Rpc3BsYXlTZWxlY3RlZFZhbHVlczogZnVuY3Rpb24gKCkge1xuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgbG9hbiBhbW91bnRcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZWxlY3RlZEFtb3VudCkuaHRtbChcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQpXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBsb2FuIGR1cmF0aW9uXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWREdXJhdGlvbikuaHRtbChcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb25cbiAgICAgICk7XG5cbiAgICAgIC8vIERpc3BsYXkgdGhlIHNlbGVjdGVkIGNyZWRpdCBzY29yZVxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkU2NvcmUpLmh0bWwoXG4gICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVcbiAgICAgICk7XG5cbiAgICAgIC8vIERpc3BsYXkgdGhlIHNlbGVjdGVkIHBheW1lbnQgZnJlcXVlbmN5XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWRQYXltZW50RnJlcXVlbmN5KS5odG1sKFxuICAgICAgICB0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3lcbiAgICAgICk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIERpc3BsYXkgdGhlIHJlc3VsdHMgZm9yIHRoZSBjdXJyZW50IHZhbHVlcy5cbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIF9kaXNwbGF5UmVzdWx0czogZnVuY3Rpb24gKCkge1xuICAgICAgLy8gRGlzcGxheSB0aGUgbG9hbiB0b3RhbFxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmxvYW5Ub3RhbFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX2xvYW5Ub3RhbCgpKVxuICAgICAgKTtcblxuICAgICAgLy8gRGlzcGxheSB0aGUgbG9hbiBwZXJpb2RpYyBwYXltZW50XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MucGF5bWVudFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX1BNVCgpKVxuICAgICAgKTtcblxuICAgICAgLy8gRGlzcGxheSB0aGUgaW50ZXJlc3QgdG90YWwgYW1vdW50XG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MuaW50ZXJlc3RUb3RhbFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX2ludGVyZXN0VG90YWwoKSlcbiAgICAgICk7XG5cbiAgICAgIC8vIERpc3BsYXkgdGhlIHRheCB0b3RhbCBhbW91bnRcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy50YXhUb3RhbFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX3RheFRvdGFsKCkpXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSBhbm51YWwgdG90YWwgY29zdFxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnRvdGFsQW5udWFsQ29zdFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b1BlcmNlbnRhZ2UodGhpcy5fQ0FUKCkpXG4gICAgICApO1xuXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZXJ2aWNlIGZlZSBpZiBhbnlcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlU2VsZWN0b3IpLmh0bWwoXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fc2VydmljZUZlZVdpdGhWQVQoKSlcbiAgICAgICk7XG5cbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuR3JhbmRUb3RhbFNlbGVjdG9yKS5odG1sKFxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX2dyYW5kVG90YWwoKSlcbiAgICAgICk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJ1biB0aGUgaW5pdCBtZXRob2QgYWdhaW4gd2l0aCB0aGUgcHJvdmlkZWQgb3B0aW9ucy5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gYXJnc1xuICAgICAqL1xuICAgIHVwZGF0ZTogZnVuY3Rpb24gKGFyZ3MpIHtcbiAgICAgIHRoaXMuc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSwgdGhpcy5fZGVmYXVsdHMsIHRoaXMuc2V0dGluZ3MsIGFyZ3MpO1xuICAgICAgdGhpcy5pbml0KCk7XG4gICAgICB0aGlzLiRlbC50cmlnZ2VyKCdsb2FuOnVwZGF0ZScpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZW5lcmF0ZSB0aGUgcmVzdWx0cyBhcyBhbiBhcnJheSBvZiBvYmplY3RzLFxuICAgICAqIGVhY2ggb2JqZWN0IGNvbnRhaW5zIHRoZSB2YWx1ZXMgZm9yIGVhY2ggcGVyaW9kLlxuICAgICAqIEByZXR1cm4ge0FycmF5fVxuICAgICAqL1xuICAgIF9yZXN1bHRzOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgYmFsYW5jZSA9IHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudDtcbiAgICAgIHZhciBpbml0aWFsID0gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50O1xuICAgICAgdmFyIGludGVyZXN0UmF0ZSA9IHRoaXMuX2ludGVyZXN0UmF0ZSgpO1xuICAgICAgdmFyIFZBVCA9IHRoaXMuX3ZhbHVlQWRkZWRUYXgoKTtcbiAgICAgIHZhciBwYXltZW50ID0gdGhpcy5fUE1UKCk7XG4gICAgICB2YXIgbnVtYmVyT2ZQYXltZW50cyA9IHRoaXMuX251bWJlck9mUGF5bWVudHMoKTtcbiAgICAgIHZhciByZXN1bHRzID0gW107XG5cbiAgICAgIC8vIFdlIGxvb3Agb3ZlciB0aGUgbnVtYmVyIG9mIHBheW1lbnRzIGFuZCBlYWNoIHRpbWVcbiAgICAgIC8vIHdlIGV4dHJhY3QgdGhlIGluZm9ybWF0aW9uIHRvIGJ1aWxkIHRoZSBwZXJpb2RcbiAgICAgIC8vIHRoYXQgd2lsbCBiZSBhcHBlbmRlZCB0byB0aGUgcmVzdWx0cyBhcnJheS5cbiAgICAgIGZvciAodmFyIHBheW1lbnROdW1iZXIgPSAwOyBwYXltZW50TnVtYmVyIDwgbnVtYmVyT2ZQYXltZW50czsgcGF5bWVudE51bWJlcisrKSB7XG4gICAgICAgIHZhciBpbnRlcmVzdCA9IGJhbGFuY2UgKiBpbnRlcmVzdFJhdGU7XG4gICAgICAgIHZhciB0YXhlc1BhaWQgPSBiYWxhbmNlICogaW50ZXJlc3RSYXRlICogVkFUO1xuICAgICAgICB2YXIgcHJpbmNpcGFsID0gcGF5bWVudCAtIGludGVyZXN0IC0gdGF4ZXNQYWlkO1xuXG4gICAgICAgIC8vIHVwZGF0ZSBpbml0aWFsIGJhbGFuY2UgZm9yIG5leHQgaXRlcmF0aW9uXG4gICAgICAgIGluaXRpYWwgPSBiYWxhbmNlO1xuXG4gICAgICAgIC8vIHVwZGF0ZSBmaW5hbCBiYWxhbmNlIGZvciB0aGUgbmV4dCBpdGVyYXRpb24uXG4gICAgICAgIGJhbGFuY2UgPSBiYWxhbmNlIC0gcHJpbmNpcGFsO1xuXG4gICAgICAgIHJlc3VsdHMucHVzaCh7XG4gICAgICAgICAgaW5pdGlhbDogaW5pdGlhbCxcbiAgICAgICAgICBwcmluY2lwYWw6IHByaW5jaXBhbCxcbiAgICAgICAgICBpbnRlcmVzdDogaW50ZXJlc3QsXG4gICAgICAgICAgdGF4OiB0YXhlc1BhaWQsXG4gICAgICAgICAgcGF5bWVudDogcGF5bWVudCxcbiAgICAgICAgICBiYWxhbmNlOiBiYWxhbmNlXG4gICAgICAgIH0pXG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gcmVzdWx0cztcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2VuZXJhdGUgdGhlIGFtb3J0aXphdGlvbiBzY2hlZHVsZS5cbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cbiAgICAgKi9cbiAgICBzY2hlZHVsZTogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuICQubWFwKHRoaXMuX3Jlc3VsdHMoKSwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgaW5pdGlhbDogdGhpcy5fdG9Nb25leSh2YWx1ZS5pbml0aWFsKSxcbiAgICAgICAgICBwcmluY2lwYWw6IHRoaXMuX3RvTW9uZXkodmFsdWUucHJpbmNpcGFsKSxcbiAgICAgICAgICBpbnRlcmVzdDogdGhpcy5fdG9Nb25leSh2YWx1ZS5pbnRlcmVzdCksXG4gICAgICAgICAgdGF4OiB0aGlzLl90b01vbmV5KHZhbHVlLnRheCksXG4gICAgICAgICAgcGF5bWVudDogdGhpcy5fdG9Nb25leSh2YWx1ZS5wYXltZW50KSxcbiAgICAgICAgICBiYWxhbmNlOiB0aGlzLl90b01vbmV5KHZhbHVlLmJhbGFuY2UpXG4gICAgICAgIH1cbiAgICAgIH0uYmluZCh0aGlzKSk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJldHVybiB0aGUgY3JlZGl0IHJhdGVzIGJlaW5nIHVzZWQuXG4gICAgICogQHJldHVybiB7T2JqZWN0fVxuICAgICAqL1xuICAgIGNyZWRpdFJhdGVzOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlcztcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogR2V0IHRoZSBjcmVkaXQgcmF0ZSBjb3JyZXNwb25kaW5nIHRvIHRoZSBjdXJyZW50IGNyZWRpdCBzY29yZS5cbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX2FubnVhbEludGVyZXN0UmF0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MuaGFzT3duUHJvcGVydHkoJ2ludGVyZXN0UmF0ZScpKSB7XG4gICAgICAgIGlmICh0aGlzLnNldHRpbmdzLmludGVyZXN0UmF0ZSA8PSAxKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuaW50ZXJlc3RSYXRlO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmludGVyZXN0UmF0ZSkgLyAxMDA7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW3RoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVdIC8gMTAwO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBHZXQgdGhlIHBlcmlvZGljIGludGVyZXN0IHJhdGUuXG4gICAgICogQHJldHVybnMge051bWJlcn1cbiAgICAgKi9cbiAgICBfaW50ZXJlc3RSYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fYW5udWFsSW50ZXJlc3RSYXRlKCkgLyB0aGlzLl9wYXltZW50RnJlcXVlbmN5KCk7XG4gICAgfSxcblxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgcGVyaW9kaWMgcGF5bWVudCBmcmVxdWVuY3lcbiAgICAgKiBAcmV0dXJucyB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9wYXltZW50RnJlcXVlbmN5OiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gUEFZTUVOVF9GUkVRVUVOQ0lFU1t0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3ldO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIG51bWJlciBvZiBwYXltZW50cyBmb3IgdGhlIGxvYW4uXG4gICAgICogQHJldHVybnMge051bWJlcn1cbiAgICAgKi9cbiAgICBfbnVtYmVyT2ZQYXltZW50czogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGR1cmF0aW9uSW5ZZWFycyA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvbikgLyAxMjtcblxuICAgICAgcmV0dXJuIE1hdGguZmxvb3IoZHVyYXRpb25JblllYXJzICogUEFZTUVOVF9GUkVRVUVOQ0lFU1t0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3ldKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2FsY3VsYXRlcyB0aGUgdG90YWwgY29zdCBvZiB0aGUgbG9hbi5cbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX2xvYW5Ub3RhbDogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuX1BNVCgpICogdGhpcy5fbnVtYmVyT2ZQYXltZW50cygpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBDYWxjdWxhdGUgdGhlIG1vbnRobHkgYW1vcnRpemVkIGxvYW4gcGF5bWVudHMuXG4gICAgICogQHNlZSBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9Db21wb3VuZF9pbnRlcmVzdCNNb250aGx5X2Ftb3J0aXplZF9sb2FuX29yX21vcnRnYWdlX3BheW1lbnRzXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9QTVQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBpID0gdGhpcy5faW50ZXJlc3RSYXRlKCk7XG4gICAgICB2YXIgTCA9IHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudDtcbiAgICAgIHZhciBuID0gdGhpcy5fbnVtYmVyT2ZQYXltZW50cygpO1xuXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy52YWx1ZUFkZGVkVGF4ICE9PSAwKSB7XG4gICAgICAgIGkgPSAoMSArIHRoaXMuX3ZhbHVlQWRkZWRUYXgoKSkgKiBpOyAvLyBpbnRlcmVzdCByYXRlIHdpdGggdGF4XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAoTCAqIGkpIC8gKDEgLSBNYXRoLnBvdygxICsgaSwgLW4pKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2FsY3VsYXRlIHRoZSB0b3RhbCBpbnRlcmVzdCBmb3IgdGhlIGxvYW4uXG4gICAgICogQHJldHVybnMge051bWJlcn1cbiAgICAgKi9cbiAgICBfaW50ZXJlc3RUb3RhbDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRvdGFsID0gMDtcbiAgICAgICQuZWFjaCh0aGlzLl9yZXN1bHRzKCksIGZ1bmN0aW9uIChpbmRleCwgdmFsdWUpIHtcbiAgICAgICAgdG90YWwgKz0gdmFsdWUuaW50ZXJlc3Q7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0b3RhbDtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ2FsY3VsYXRlIHRoZSB2YWx1ZSBhZGRlZCB0YXggdG90YWwgZm9yIHRoZSBsb2FuLlxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XG4gICAgICovXG4gICAgX3RheFRvdGFsOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgdG90YWwgPSAwO1xuICAgICAgJC5lYWNoKHRoaXMuX3Jlc3VsdHMoKSwgZnVuY3Rpb24gKGluZGV4LCB2YWx1ZSkge1xuICAgICAgICB0b3RhbCArPSB2YWx1ZS50YXg7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0b3RhbDtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmV0dXJuIHRoZSBsb2FuIGZlZXMgYW5kIGNvbW1pc3Npb25zIHRvdGFsLlxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfc2VydmljZUZlZTogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHNlcnZpY2VGZWUgPSB0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWU7XG5cbiAgICAgIC8vIGlmIHRoZSBzZXJ2aWNlIGZlZSBpcyBncmVhdGVyIHRoYW4gMSB0aGVuIHRoZVxuICAgICAgLy8gdmFsdWUgbXVzdCBiZSBjb252ZXJ0ZWQgdG8gZGVjaW1hbHMgZmlyc3QuXG4gICAgICBpZiAoc2VydmljZUZlZSA+IDEpIHtcbiAgICAgICAgc2VydmljZUZlZSA9IHNlcnZpY2VGZWUgLyAxMDA7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgKiBzZXJ2aWNlRmVlO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIGxvYW4gZmVlcyBhbmQgY29tbWlzc2lvbnMgdG90YWwgd2l0aCBWQVQgaW5jbHVkZWQuXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9zZXJ2aWNlRmVlV2l0aFZBVDogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuX3NlcnZpY2VGZWUoKSAqICh0aGlzLl92YWx1ZUFkZGVkVGF4KCkgKyAxKVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBDYWxjdWxhdGVzIHRoZSB0b3RhbCBjb3N0IG9mIHRoZSBsb2FuIGluY2x1ZGluZyB0aGUgc2VydmljZSBmZWUuXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9ncmFuZFRvdGFsOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fbG9hblRvdGFsKCkgKyB0aGlzLl9zZXJ2aWNlRmVlV2l0aFZBVCgpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm4gdGhlIHRvdGFsIGFubnVhbCBjb3N0IChDQVQpXG4gICAgICogQHNlZSBodHRwOi8vd3d3LmJhbnhpY28ub3JnLm14L0NBVFxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfQ0FUOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgSVJSID0gdGhpcy5fSVJSKHRoaXMuX2Nhc2hGbG93KCkpO1xuICAgICAgdmFyIHBlcmlvZHMgPSB0aGlzLl9wYXltZW50RnJlcXVlbmN5KCk7XG5cbiAgICAgIHJldHVybiBNYXRoLnBvdygxICsgSVJSLCBwZXJpb2RzKSAtIDE7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gYXJyYXkgd2l0aCBhIHNlcmllcyBvZiBjYXNoIGZsb3dzIGZvciB0aGUgY3VycmVudCBsb2FuLlxuICAgICAqIEByZXR1cm4ge0FycmF5fVxuICAgICAqL1xuICAgIF9jYXNoRmxvdzogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHJlc3VsdHMgPSB0aGlzLl9yZXN1bHRzKCk7XG4gICAgICB2YXIgY2FzaEZsb3cgPSBbdGhpcy5fc2VydmljZUZlZSgpIC0gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50XTtcblxuICAgICAgJC5lYWNoKHJlc3VsdHMsIGZ1bmN0aW9uIChpbmRleCwgcGVyaW9kKSB7XG4gICAgICAgIGNhc2hGbG93LnB1c2gocGVyaW9kLnBheW1lbnQgLSBwZXJpb2QudGF4KTtcbiAgICAgIH0uYmluZCh0aGlzKSk7XG5cbiAgICAgIHJldHVybiBjYXNoRmxvdztcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgaW50ZXJuYWwgcmF0ZSBvZiByZXR1cm4gZm9yIGEgc2VyaWVzIG9mIGNhc2ggZmxvd3MgcmVwcmVzZW50ZWQgYnkgdGhlIG51bWJlcnMgaW4gdmFsdWVzLlxuICAgICAqIEBwYXJhbSAge0FycmF5fSB2YWx1ZXNcbiAgICAgKiBAcGFyYW0gIHtOdW1iZXJ9IGd1ZXNzXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxuICAgICAqL1xuICAgIF9JUlI6IGZ1bmN0aW9uICh2YWx1ZXMsIGd1ZXNzKSB7XG4gICAgICBndWVzcyA9IGd1ZXNzIHx8IDA7XG5cbiAgICAgIC8vIENhbGN1bGF0ZXMgdGhlIHJlc3VsdGluZyBhbW91bnRcbiAgICAgIHZhciBpcnJSZXN1bHQgPSBmdW5jdGlvbiAodmFsdWVzLCBkYXRlcywgcmF0ZSkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gdmFsdWVzWzBdO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgcmVzdWx0ICs9IHZhbHVlc1tpXSAvIE1hdGgucG93KHJhdGUgKyAxLCAoZGF0ZXNbaV0gLSBkYXRlc1swXSkgLyAzNjUpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH07XG5cbiAgICAgIC8vIENhbGN1bGF0ZXMgdGhlIGZpcnN0IGRlcml2YXRpb25cbiAgICAgIHZhciBpcnJSZXN1bHREZXJpdmF0aXZlID0gZnVuY3Rpb24gKHZhbHVlcywgZGF0ZXMsIHJhdGUpIHtcbiAgICAgICAgdmFyIHJlc3VsdCA9IDA7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCB2YWx1ZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICB2YXIgZnJhYyA9IChkYXRlc1tpXSAtIGRhdGVzWzBdKSAvIDM2NTtcbiAgICAgICAgICByZXN1bHQgLT0gZnJhYyAqIHZhbHVlc1tpXSAvIE1hdGgucG93KHJhdGUgKyAxLCBmcmFjICsgMSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfTtcblxuICAgICAgLy8gSW5pdGlhbGl6ZSBkYXRlcyBhbmQgY2hlY2sgdGhhdCB2YWx1ZXMgY29udGFpbnMgYXRcbiAgICAgIC8vIGxlYXN0IG9uZSBwb3NpdGl2ZSB2YWx1ZSBhbmQgb25lIG5lZ2F0aXZlIHZhbHVlXG4gICAgICB2YXIgZGF0ZXMgPSBbXTtcbiAgICAgIHZhciBwb3NpdGl2ZSA9IGZhbHNlO1xuICAgICAgdmFyIG5lZ2F0aXZlID0gZmFsc2U7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGRhdGVzW2ldID0gKGkgPT09IDApID8gMCA6IGRhdGVzW2kgLSAxXSArIDM2NTtcbiAgICAgICAgaWYgKHZhbHVlc1tpXSA+IDApIHBvc2l0aXZlID0gdHJ1ZTtcbiAgICAgICAgaWYgKHZhbHVlc1tpXSA8IDApIG5lZ2F0aXZlID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgaWYgKCFwb3NpdGl2ZSB8fCAhbmVnYXRpdmUpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgICdFcnJvciB0aGUgdmFsdWVzIGRvZXMgbm90IGNvbnRhaW4gYXQgbGVhc3Qgb25lIHBvc2l0aXZlIHZhbHVlIGFuZCBvbmUgbmVnYXRpdmUgdmFsdWUnXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIC8vIEluaXRpYWxpemUgZ3Vlc3MgYW5kIHJlc3VsdFJhdGVcbiAgICAgIGd1ZXNzID0gKGd1ZXNzID09PSB1bmRlZmluZWQpID8gMC4xIDogZ3Vlc3M7XG4gICAgICB2YXIgcmVzdWx0UmF0ZSA9IGd1ZXNzO1xuXG4gICAgICAvLyBTZXQgbWF4aW11bSBlcHNpbG9uIGZvciBlbmQgb2YgaXRlcmF0aW9uXG4gICAgICB2YXIgZXBzTWF4ID0gMWUtMTA7XG5cbiAgICAgIC8vIEltcGxlbWVudCBOZXd0b24ncyBtZXRob2RcbiAgICAgIHZhciBuZXdSYXRlLCBlcHNSYXRlLCByZXN1bHRWYWx1ZTtcbiAgICAgIHZhciBjb250TG9vcCA9IHRydWU7XG5cbiAgICAgIGRvIHtcbiAgICAgICAgcmVzdWx0VmFsdWUgPSBpcnJSZXN1bHQodmFsdWVzLCBkYXRlcywgcmVzdWx0UmF0ZSk7XG4gICAgICAgIG5ld1JhdGUgPSByZXN1bHRSYXRlIC0gcmVzdWx0VmFsdWUgLyBpcnJSZXN1bHREZXJpdmF0aXZlKHZhbHVlcywgZGF0ZXMsIHJlc3VsdFJhdGUpO1xuICAgICAgICBlcHNSYXRlID0gTWF0aC5hYnMobmV3UmF0ZSAtIHJlc3VsdFJhdGUpO1xuICAgICAgICByZXN1bHRSYXRlID0gbmV3UmF0ZTtcbiAgICAgICAgY29udExvb3AgPSAoZXBzUmF0ZSA+IGVwc01heCkgJiYgKE1hdGguYWJzKHJlc3VsdFZhbHVlKSA+IGVwc01heCk7XG4gICAgICB9IHdoaWxlIChjb250TG9vcCk7XG5cbiAgICAgIC8vIFJldHVybiBpbnRlcm5hbCByYXRlIG9mIHJldHVyblxuICAgICAgcmV0dXJuIHJlc3VsdFJhdGU7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIFJldHVybiB0aGUgdmFsdWUgYWRkZWQgdGF4IGluIGRlY2ltYWxzLlxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfdmFsdWVBZGRlZFRheDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRheCA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLnZhbHVlQWRkZWRUYXggfHwgMCk7XG5cbiAgICAgIC8vIGlmIHRheCBpcyBncmVhdGVyIHRoYW4gMSBtZWFucyB0aGUgdmFsdWVcbiAgICAgIC8vIG11c3QgYmUgY29udmVydGVkIHRvIGRlY2ltYWxzIGZpcnN0LlxuICAgICAgcmV0dXJuICh0YXggPiAxKSA/IHRheCAvIDEwMCA6IHRheDtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ29udmVydCBudW1lcmljIGZvcm1hdCB0byBtb25leSBmb3JtYXQuXG4gICAgICogQHBhcmFtICB7TnVtYmVyfSBudW1lcmljXG4gICAgICogQHJldHVybiB7U3RyaW5nfVxuICAgICAqL1xuICAgIF90b01vbmV5OiBmdW5jdGlvbiAobnVtZXJpYykge1xuICAgICAgaWYgKHR5cGVvZiBudW1lcmljID09ICdzdHJpbmcnKSB7XG4gICAgICAgIG51bWVyaWMgPSBwYXJzZUZsb2F0KG51bWVyaWMpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gJ0FFRCAnICsgbnVtZXJpYy50b0ZpeGVkKDIpLnJlcGxhY2UoLyhcXGQpKD89KFxcZHszfSkrXFwuKS9nLCAnJDEsJyk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIENvbnZlcnQgZnJvbSBtb25leSBmb3JtYXQgdG8gbnVtZXJpYyBmb3JtYXQuXG4gICAgICogQHBhcmFtICB7U3RyaW5nfSB2YWx1ZVxuICAgICAqIEByZXR1cm4ge051bWJlcn1cbiAgICAgKi9cbiAgICBfdG9OdW1lcmljOiBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgIHJldHVybiBwYXJzZUZsb2F0KFxuICAgICAgICB2YWx1ZS50b1N0cmluZygpLnJlcGxhY2UoL1teMC05XFwuXSsvZywgJycpXG4gICAgICApO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBUbyBjb252ZXJ0IHRoZSBwcm92aWRlZCB2YWx1ZSB0byBwZXJjZW50IGZvcm1hdC5cbiAgICAgKiBAcGFyYW0ge051bWJlcn0gbnVtZXJpY1xuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAgICovXG4gICAgX3RvUGVyY2VudGFnZTogZnVuY3Rpb24gKG51bWVyaWMpIHtcbiAgICAgIHJldHVybiAobnVtZXJpYyAqIDEwMCkudG9GaXhlZCgyKSArICclJztcbiAgICB9XG5cbiAgfSk7XG5cbiAgLyoqXG4gICAqIFdyYXBwZXIgYXJvdW5kIHRoZSBjb25zdHJ1Y3RvciB0byBwcmV2ZW50IG11bHRpcGxlIGluc3RhbnRpYXRpb25zLlxuICAgKi9cbiAgJC5mbi5sb2FuQ2FsY3VsYXRvciA9IGZ1bmN0aW9uIChvcHRpb25zLCBhcmdzKSB7XG4gICAgaWYgKG9wdGlvbnMgPT09ICdzY2hlZHVsZScpIHtcbiAgICAgIHJldHVybiB0aGlzLmRhdGEoJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicpLnNjaGVkdWxlKCk7XG4gICAgfVxuXG4gICAgaWYgKG9wdGlvbnMgPT09ICdyYXRlcycpIHtcbiAgICAgIHJldHVybiB0aGlzLmRhdGEoJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicpLmNyZWRpdFJhdGVzKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgaW5zdGFuY2UgPSAkLmRhdGEodGhpcywgJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicpO1xuICAgICAgaWYgKCFpbnN0YW5jZSkge1xuICAgICAgICAkLmRhdGEodGhpcywgJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicsIG5ldyBQbHVnaW4odGhpcywgb3B0aW9ucykpO1xuICAgICAgfSBlbHNlIGlmIChvcHRpb25zID09PSAndXBkYXRlJykge1xuICAgICAgICByZXR1cm4gaW5zdGFuY2UudXBkYXRlKGFyZ3MpO1xuICAgICAgfVxuICAgIH0pO1xuICB9O1xuXG59KShqUXVlcnksIHdpbmRvdywgZG9jdW1lbnQpOyJdfQ==
