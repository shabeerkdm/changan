$(document).ready(function () {
    $('#vehicles').find('.vehicle').show()
    var slug = $('#select-model').val()
    var grade = $('#select-grade').val(0)
    $('#select-model').on('change', function () {
        var slug = $(this).val()
        if (slug == 0) {
            $('#select-grade').attr('disabled', true)
        } else {
            getGrades(slug);
        }
        var min = (parseInt($('#lower-value').text())/5).toFixed(2)
        var max = (parseInt($('#upper-value').text())/5).toFixed(2)
        checkPRice([min, max])
    })

    $('#select-grade').on('change', function () {
        var min = (parseInt($('#lower-value').text())/5).toFixed(2)
        var max = (parseInt($('#upper-value').text())/5).toFixed(2)
        checkPRice([min, max])
    })
})

function getGrades(slug) {
    $.ajax({
        type: 'post',
        url: url + '/get-grades',
        dataType: "json",
        data: { slug: slug },
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            if (data.length > 0) {
                $('#select-grade').html(new Option('All Grades', 0))
                $('#select-grade').attr('disabled', false)
                data.forEach(element => {
                    $('#select-grade').append(new Option(element.name, element.id))
                });
                $('#select-grade option[value=0]').attr('selected','selected').change();
            } else {
                $('#select-grade').html(new Option('All Grades', 0))
                $('#select-grade').attr('disabled', true)
            }
        },
        error: function (error) {
            console.log(error);
        }
    })
}

nonLinearSlider.noUiSlider.on('update', (function (values, handle, unencoded, isTap, positions) {
    checkPRice(values)
}));

function checkPRice(values) {
    var slug = $('#select-model').val()
    var grade = $('#select-grade').val()
    $('#vehicles').find('.vehicle').hide()
    $('#vehicles').find('.vehicle').filter(function () {
        var price = parseFloat($(this).attr('data-price').replace(',', ''))
        var min = parseFloat(values[0]) * 5000
        var max = parseFloat(values[1]) * 5000
        if (!isNaN(price)) {
            var m_status = true;
            var g_status = true;
            if (slug != 0)
                m_status = $(this).attr('data-model') == slug
            if (grade != 0)
                g_status = $(this).attr('data-grade') == grade
            return min <= price && price <= max && m_status && g_status;
        } else {
            return true
        }
    }).show()
}
