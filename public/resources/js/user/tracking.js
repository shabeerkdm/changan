$(document).ready(function () {
    $('#tracking-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#tracking-form').validate({
        rules: {
            plate_number: {
                required: true
            },
            phone: {
                required: true
            }
        },
        ignore: "",
        submitHandler: function (form) {
            var formData = $(form).serializeArray()
            $.ajax({
                type: 'post',
                url: url + '/tracking',
                dataType: "json",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    if (result.status) {
                        $('#status').html('')
                        var statuses = JSON.parse(result.data)
                        statuses.sort((a, b) => {
                            return new Date(b.date) - new Date(a.date);
                        });
                        statuses.forEach(element => {
                            $('#status').append(`
                            <div class="tracking-history ">
                                <div class="tracking-history__date">${moment(element.date, 'YY-MM-DD').format('DD-MMM-YYYY')}</div>
                                <div class="tracking-history__conent">${element.status}</div>
                            </div>`)
                        });
                        $('section.status').removeClass('d-none')
                    } else {
                        $('#status').html('')
                        $('#status').html(`<span class="text-center">${result.message}</span>`)
                        $('section.status').removeClass('d-none')
                        setTimeout(() => {
                            $('section.status').addClass('d-none')
                        }, 5000);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})