$(document).ready(function () {
    $('#account-form, #password-form').on('submit', function (e) {
        e.preventDefault();
    })
    $('#account-form').validate({
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true
            },
            phone: {
                required: true
            }
        },
        ignore: "",
        submitHandler: function (form) {
            var formData = $(form).serializeArray()
            $.ajax({
                type: 'post',
                url: url + '/customer',
                dataType: "json",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (data) {
                    if (data.status) {
                        location.reload()
                    } else {
                        console.log(data.error);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
    $('#password-form').validate({
        rules: {
            password: {
                minlength: 5
            },
            password_confirm: {
                minlength: 5,
                equalTo: "#password"
            }
        },
        ignore: "",
        submitHandler: function (form) {
            var formData = $(form).serializeArray()
            $.ajax({
                type: 'post',
                url: url + '/user',
                dataType: "json",
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (data) {
                    if (data.status) {
                        $('#message').text(data.message)
                        setTimeout(() => {
                            location.reload()
                        }, 3000);
                    } else {
                        console.log(data.error);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})
