var url = $('[name="site_url"]').val()
$('#login-form').on('submit', function (e) {
    e.preventDefault()
    var formData = new FormData($(this)[0])
    $.ajax({
        url: url + '/login',
        type: "POST",
        dataType: "JSON",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (res.status) {
                $('#login').removeClass('btn-primary').addClass('btn-success')
                location.replace(url + '/admin/dashboard')
            } else {
                $('#login').removeClass('btn-primary').addClass('btn-danger')
                $('#message').html(`<div class="text-danger"><strong>Error!</strong> ${res.message}.</div>`)
                setTimeout(() => {
                    $('#login').removeClass('btn-danger').addClass('btn-primary')
                    $('#message').html('')
                }, 3000);
            }
        },
        error: function () { }

    })
})