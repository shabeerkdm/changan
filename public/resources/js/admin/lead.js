$(document).ready(function () {
    $('#leadPopup').on('show.coreui.modal', function (e) {
        var id = $(e.relatedTarget).data('id')
        $.ajax({
            type: 'get',
            url: url + '/admin/leads/' + id,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                $('#leadPopup').find('.modal-body tbody').html('')
                Object.keys(result).forEach(key => {
                    if (result[key])
                        $('#leadPopup').find('.modal-body tbody').append(`
                        <tr>
                            <td>${key}</td>
                            <td>${result[key]}</td>
                        </tr>
                    `)
                })
            },
            error: function () { }
        })
    })
    $('#filter').on('change', function () {
        var filter = $(this).val()
        if (filter != 0)
            location.replace(url + '/admin/leads?filter=' + filter)
        else
            location.replace(url + '/admin/leads')
    })
})