$(document).ready(function () {
    $('#site_options').on('submit', function (e) {
        e.preventDefault()
        var formData = new FormData($(this)[0])
        $.ajax({
            type: 'post',
            url: url + '/admin/options',
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                if (result.status) {
                    swal(result.message, {
                        icon: "success",
                    }).then(() => {
                        location.reload()
                    });
                }
                else
                    console.log(error);
            },
            error: function (error) {
                console.log(error);
            }
        })
    })
})