$(document).ready(function () {
    $('#contact-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#contact-form').validate({
        rules: {
            location: {
                required: true
            },
            address: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            $.ajax({
                type: 'post',
                url: url + '/admin/contact',
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    swal(result.message, {
                        icon: "success",
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})
$('.delete').on('click', function () {
    deleteFn(this)
})

function editContact(contact) {
    $('#location').val(contact.location)
    $('#description').val(contact.description)
    $('#address').val(contact.address)
    $('#phone').val(contact.phone)
    $('#email').val(contact.email)
    $('#timing_sales').val(contact.timing_sales)
    $('#timing_services').val(contact.timing_services)
    $('#email').val(contact.email)
    $('#contact-form').append(`<input type="hidden" name="contact_id" value="${contact.id}">`)
    $('#submit').text('Update')
}