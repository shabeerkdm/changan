$(document).ready(function () {
    getImages()
    $('#add-image').on('submit', function (e) {
        e.preventDefault()
    })
    $('#add-image').validate({
        rules: {
            image: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            $.ajax({
                type: 'post',
                url: url + '/admin/add-image',
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    if (result.status) {
                        swal(result.message, {
                            icon: "success",
                        });
                        getImages()
                    }
                    else
                        console.log(error);

                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})

function getImages() {
    var reference_id = $('[name="reference_id"]').val()
    $.ajax({
        type: 'post',
        url: url + '/admin/get-images',
        data: { section_id: $('[name="section_id"]').val(), reference_id: reference_id },
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                "content"
            )
        },
        success: function (data) {
            $('#image-media tbody').html('')
            var btn = reference_id ? '<button class="btn btn-danger" onclick="deleteImage(this)"><i class="fa fa-trash fa-sm"></i></button>' : '<button class="btn btn-warning" onclick="copy(this)"><i class="fa fa-copy fa-sm"></i></button><button class="btn btn-danger ml-1" onclick="deleteImage(this)"><i class="fa fa-trash fa-sm"></i></button>'
            data.forEach(element => {
                $('#image-media tbody').append(
                    `<tr>
                        <td><img src="${storage + element.image}" width="50%"></td>
                        <td data-id="${element.id}">
                            ${btn}
                        </td>
                    </tr>`
                )
            });
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function copy(self) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(self).closest('tr').find('img').attr('src')).select();
    document.execCommand("copy");
    $temp.remove();
}

function deleteImage(self) {
    var model = 'image';
    var id = $(self).closest('td').data('id');
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this " + model + '!',
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((res) => {
            if (res) {
                $.ajax({
                    type: 'DELETE',
                    url: url + '/admin/delete',
                    data: { id: id, model: model },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function (result) {
                        swal(model + " has been deleted!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload()
                        }, 1500);
                    },
                    error: function () { }
                })
            } else {
                swal(model + " is safe!");
            }
        });
}