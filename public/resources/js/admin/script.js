var url = $('[name="site_url"]').val()
var storage = $('[name="storage"]').val()
$(document).ready(function () {
    $('#master-form, #slide-form').on('submit', function (e) {
        e.preventDefault()
    })

    $('input[type="file"]').on('change', function (e) {
        var fileName = e.target.files[0].name;
        $(this).next('.custom-file-label').html(fileName);
    })
})

function deleteFn(self) {
    var model = $(self).data('model')
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this " + model + '!',
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((res) => {
            if (res) {
                $.ajax({
                    type: 'DELETE',
                    url: url + '/admin/delete',
                    data: { id: $(self).data('id'), model: model },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        )
                    },
                    success: function (result) {
                        swal(model + " has been deleted!", {
                            icon: "success",
                        });
                        setTimeout(() => {
                            location.reload()
                        }, 1500);
                    },
                    error: function () { }
                })
            } else {
                swal(model+" is safe!");
            }
        });
}
