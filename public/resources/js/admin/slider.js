$(document).ready(function () {
    $('#master-form').validate({
        rules: {
            name: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            $.ajax({
                type: 'post',
                url: $(form).attr('action'),
                dataType: "JSON",
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    location.reload()
                    $('[name="slider_id"]').remove()
                },
                error: function (error) { }
            })
        }
    })
    $('#slide-form').validate({
        rules: {
            slide_id: {
                required: true
            },
            image: {
                required: true
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            $.ajax({
                type: 'post',
                url: $(form).attr('action'),
                dataType: "JSON",
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.status) {
                        swal(result.message, {
                            icon: 'success'
                        })
                        setTimeout(() => {
                            location.reload()
                        }, 1000);
                    }
                },
                error: function (error) { }
            })
        }
    })
    $('.edit').on('click', function () {
        var id = $(this).data('id')
        $('#master-form').prepend(`<input type="hidden" name="slider_id" value="${id}">`)
        $('#master-form').find('#name').val($(this).closest('tr').find('td:first-child').text())
    })
    $('#slidesPopup').on('show.coreui.modal', function (e) {
        var id = $(e.relatedTarget).data('id')
        $.ajax({
            type: 'get',
            url: url + '/admin/sliders/' + id,
            dataType: "JSON",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                    "content"
                )
            },
            success: function (result) {
                $('#slides').find('tbody').html('')
                result.data.forEach(slide => {
                    $('#slides').find('tbody').append(`
                        <tr>
                            <td><img src="${storage + slide.image}" width="100px"></td>
                            <td>
                                <button class="btn btn-danger action delete" onclick="deleteFn(this)" data-id="${slide.id}" data-model="slide"><i
                                    class="fa fa-trash fa-sm"></i></button>
                            </td>
                        </tr>
                    `)
                });
            },
            error: function () { }
        })
    })
})
$('.delete').on('click', function () {
    deleteFn(this)
})

