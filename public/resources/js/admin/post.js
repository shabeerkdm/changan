$(document).ready(function () {
    var method = $('[name="_method"]').val() == 'patch'
    $('#post-form').on('submit', function (e) {
        e.preventDefault()
    })
    $('#post-form').validate({
        rules: {
            title: {
                required: true
            },
            slug: {
                required: true
            },
            image: {
                required: !method
            },
            description: {
                required: function () {
                    return document.querySelector("#description").editorInstance.getData();
                }
            },
            body: {
                required: function () {
                    return document.querySelector("#body").editorInstance.getData();
                }
            }
        },
        messages:
        {
            description: {
                required: "Please enter Text"
            }
        },
        ignore: [],
        submitHandler: function (form) {
            var form_data = new FormData($(form)[0])
            var action = $(form).attr('action')
            $.ajax({
                type: 'post',
                url: action,
                data: form_data,
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                        "content"
                    )
                },
                success: function (result) {
                    swal(result.message, {
                        icon: "success",
                    });
                    setTimeout(() => {
                        location.replace(url + '/admin/posts')
                    }, 2000);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    })
})