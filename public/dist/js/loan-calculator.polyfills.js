/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

/*!
 * jQuery Loan Calculator 3.1.3
 *
 * Author: Jorge González <scrub.mx@gmail>
 * Released under the MIT license - https://opensource.org/licenses/MIT
 */
;
(function ($, window, document, undefined) {

  "use strict";

  /**
   * Table of credit rates for every score.
   * @type {Object}
   */

  var CREDIT_RATES = {
    'A': 5.32,
    'B': 8.18,
    'C': 12.29,
    'D': 15.61,
    'E': 18.25,
    'F': 21.99,
    'G': 26.77
  };

  /**
   * Table of allowed payment frequencies.
   * @type {Object}
   */
  var PAYMENT_FREQUENCIES = {
    'weekly': 52,
    'biweekly': 26,
    'monthly': 12
  };

  /**
   * The minimum allowed for a loan.
   * @type {Number}
   */
  var MINIMUM_LOAN = 1000;

  /**
   * The minimum duration in months.
   * @type {Number}
   */
  var MINIMUM_DURATION = 1;

  /**
   * Default options for the plugin.
   * @type {Object}
   */
  var defaults = {
    // default values for a loan
    loanAmount: 50000,
    loanDuration: 12,
    creditRates: CREDIT_RATES,
    creditScore: 'A',
    valueAddedTax: 0,
    serviceFee: 0,
    paymentFrequency: 'monthly',

    // inputs
    loanAmountSelector: '#loan-amount',
    loanDurationSelector: '#loan-duration',
    creditScoreSelector: '#credit-score',
    paymentFrequencySelector: '#payment-frequency',

    // display selected values
    selectedAmount: '#selected-amount',
    selectedDuration: '#selected-duration',
    selectedScore: '#selected-score',
    selectedPaymentFrequency: '#selected-payment-frequency',

    // display the results
    loanTotalSelector: '#loan-total',
    paymentSelector: '#payment',
    interestTotalSelector: '#interest-total',
    serviceFeeSelector: '#service-fee',
    taxTotalSelector: '#tax-total',
    totalAnnualCostSelector: '#total-annual-cost',
    loanGrandTotalSelector: '#grand-total'
  };

  /**
   * The actual plugin constructor
   * @param {Object} element
   * @param {Object} options
   */
  function Plugin(element, options) {
    this.$el = $(element);
    this._name = 'loanCalculator';
    this._defaults = defaults;
    this.settings = $.extend({}, defaults, options);
    this.attachListeners();
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {

    /**
     * Validates the data and shows the results.
     * @return {void}
     */
    init: function () {
      this.validate();
      this.render();
    },

    /**
     * Attach event listeners to the event handlers.
     * @return {void}
     */
    attachListeners: function () {
      var eventEmitters = [this.settings.loanAmountSelector, this.settings.loanDurationSelector, this.settings.creditScoreSelector, this.settings.paymentFrequencySelector];

      $(eventEmitters.join()).on({
        change: this.eventHandler.bind(this),
        mousemove: this.eventHandler.bind(this),
        touchmove: this.eventHandler.bind(this)
      });
    },

    /**
     * Handle events from the DOM.
     * @return {void}
     */
    eventHandler: function () {
      this.update({
        loanAmount: this.$el.find(this.settings.loanAmountSelector).val(),
        loanDuration: this.$el.find(this.settings.loanDurationSelector).val(),
        creditScore: this.$el.find(this.settings.creditScoreSelector).val(),
        paymentFrequency: this.$el.find(this.settings.paymentFrequencySelector).val()
      });
    },

    /**
     * Sanitize and validate the user input data.
     * @throws Error
     * @return {void}
     */
    validate: function () {
      if (typeof this.settings.loanAmount === 'string') {
        this.settings.loanAmount = this._toNumeric(this.settings.loanAmount);
      }

      if (typeof this.settings.serviceFee === 'string') {
        this.settings.serviceFee = this._toNumeric(this.settings.serviceFee);
      }

      if (!$.isPlainObject(this.settings.creditRates)) {
        throw new Error('The value provided for [creditRates] is not valid.');
      }

      for (var creditRate in this.settings.creditRates) {
        if (typeof this.settings.creditRates[creditRate] === 'string') {
          this.settings.creditRates[creditRate] = this._toNumeric(this.settings.creditRates[creditRate]);
        }

        if (!$.isNumeric(this.settings.creditRates[creditRate])) {
          throw new Error('The value provided for [creditRates] is not valid.');
        }

        if (this.settings.creditRates[creditRate] < 1) {
          this.settings.creditRates[creditRate] = this.settings.creditRates[creditRate] * 100;
        }
      }

      // Sanitize the input
      this.settings.loanAmount = parseFloat(this.settings.loanAmount);
      this.settings.loanDuration = parseFloat(this.settings.loanDuration);
      this.settings.serviceFee = parseFloat(this.settings.serviceFee);
      this.settings.creditScore = $.trim(this.settings.creditScore.toUpperCase());

      if (!PAYMENT_FREQUENCIES.hasOwnProperty(this.settings.paymentFrequency)) {
        throw new Error('The value provided for [paymentFrequency] is not valid.');
      }

      if (!this.settings.creditRates.hasOwnProperty(this.settings.creditScore)) {
        throw new Error('The value provided for [creditScore] is not valid.');
      }

      if (this.settings.loanAmount < MINIMUM_LOAN) {
        throw new Error('The value provided for [loanAmount] must me at least 1000.');
      }

      if (this.settings.loanDuration < MINIMUM_DURATION) {
        throw new Error('The value provided for [loanDuration] must me at least 1.');
      }

      if (!$.isNumeric(this.settings.serviceFee)) {
        throw new Error('The value provided for [serviceFee] is not valid.');
      }
    },

    /**
     * Show the results in the DOM.
     * @return {void}
     */
    render: function () {
      this._displaySelectedValues();
      this._displayResults();
    },

    /**
     * Show the selected values in the DOM.
     * @return {void}
     */
    _displaySelectedValues: function () {
      // Display the selected loan amount
      this.$el.find(this.settings.selectedAmount).html(this._toMoney(this.settings.loanAmount));

      // Display the selected loan duration
      this.$el.find(this.settings.selectedDuration).html(this.settings.loanDuration);

      // Display the selected credit score
      this.$el.find(this.settings.selectedScore).html(this.settings.creditScore);

      // Display the selected payment frequency
      this.$el.find(this.settings.selectedPaymentFrequency).html(this.settings.paymentFrequency);
    },

    /**
     * Display the results for the current values.
     * @return {void}
     */
    _displayResults: function () {
      // Display the loan total
      this.$el.find(this.settings.loanTotalSelector).html(this._toMoney(this._loanTotal()));

      // Display the loan periodic payment
      this.$el.find(this.settings.paymentSelector).html(this._toMoney(this._PMT()));

      // Display the interest total amount
      this.$el.find(this.settings.interestTotalSelector).html(this._toMoney(this._interestTotal()));

      // Display the tax total amount
      this.$el.find(this.settings.taxTotalSelector).html(this._toMoney(this._taxTotal()));

      // Display the annual total cost
      this.$el.find(this.settings.totalAnnualCostSelector).html(this._toPercentage(this._CAT()));

      // Display the service fee if any
      this.$el.find(this.settings.serviceFeeSelector).html(this._toMoney(this._serviceFeeWithVAT()));

      this.$el.find(this.settings.loanGrandTotalSelector).html(this._toMoney(this._grandTotal()));
    },

    /**
     * Run the init method again with the provided options.
     * @param {Object} args
     */
    update: function (args) {
      this.settings = $.extend({}, this._defaults, this.settings, args);
      this.init();
      this.$el.trigger('loan:update');
    },

    /**
     * Generate the results as an array of objects,
     * each object contains the values for each period.
     * @return {Array}
     */
    _results: function () {
      var balance = this.settings.loanAmount;
      var initial = this.settings.loanAmount;
      var interestRate = this._interestRate();
      var VAT = this._valueAddedTax();
      var payment = this._PMT();
      var numberOfPayments = this._numberOfPayments();
      var results = [];

      // We loop over the number of payments and each time
      // we extract the information to build the period
      // that will be appended to the results array.
      for (var paymentNumber = 0; paymentNumber < numberOfPayments; paymentNumber++) {
        var interest = balance * interestRate;
        var taxesPaid = balance * interestRate * VAT;
        var principal = payment - interest - taxesPaid;

        // update initial balance for next iteration
        initial = balance;

        // update final balance for the next iteration.
        balance = balance - principal;

        results.push({
          initial: initial,
          principal: principal,
          interest: interest,
          tax: taxesPaid,
          payment: payment,
          balance: balance
        });
      };

      return results;
    },

    /**
     * Generate the amortization schedule.
     * @return {Array}
     */
    schedule: function () {
      return $.map(this._results(), function (value) {
        return {
          initial: this._toMoney(value.initial),
          principal: this._toMoney(value.principal),
          interest: this._toMoney(value.interest),
          tax: this._toMoney(value.tax),
          payment: this._toMoney(value.payment),
          balance: this._toMoney(value.balance)
        };
      }.bind(this));
    },

    /**
     * Return the credit rates being used.
     * @return {Object}
     */
    creditRates: function () {
      return this.settings.creditRates;
    },

    /**
     * Get the credit rate corresponding to the current credit score.
     * @return {Number}
     */
    _annualInterestRate: function () {
      if (this.settings.hasOwnProperty('interestRate')) {
        if (this.settings.interestRate <= 1) {
          return this.settings.interestRate;
        }

        return this._toNumeric(this.settings.interestRate) / 100;
      }

      return this.settings.creditRates[this.settings.creditScore] / 100;
    },

    /**
     * Get the periodic interest rate.
     * @returns {Number}
     */
    _interestRate: function () {
      return this._annualInterestRate() / this._paymentFrequency();
    },

    /**
     * Returns the periodic payment frequency
     * @returns {Number}
     */
    _paymentFrequency: function () {
      return PAYMENT_FREQUENCIES[this.settings.paymentFrequency];
    },

    /**
     * Returns number of payments for the loan.
     * @returns {Number}
     */
    _numberOfPayments: function () {
      var durationInYears = this._toNumeric(this.settings.loanDuration) / 12;

      return Math.floor(durationInYears * PAYMENT_FREQUENCIES[this.settings.paymentFrequency]);
    },

    /**
     * Calculates the total cost of the loan.
     * @return {Number}
     */
    _loanTotal: function () {
      return this._PMT() * this._numberOfPayments();
    },

    /**
     * Calculate the monthly amortized loan payments.
     * @see https://en.wikipedia.org/wiki/Compound_interest#Monthly_amortized_loan_or_mortgage_payments
     * @return {Number}
     */
    _PMT: function () {
      var i = this._interestRate();
      var L = this.settings.loanAmount;
      var n = this._numberOfPayments();

      if (this.settings.valueAddedTax !== 0) {
        i = (1 + this._valueAddedTax()) * i; // interest rate with tax
      }

      return L * i / (1 - Math.pow(1 + i, -n));
    },

    /**
     * Calculate the total interest for the loan.
     * @returns {Number}
     */
    _interestTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.interest;
      }));
      return total;
    },

    /**
     * Calculate the value added tax total for the loan.
     * @returns {Number}
     */
    _taxTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.tax;
      }));
      return total;
    },

    /**
     * Return the loan fees and commissions total.
     * @return {Number}
     */
    _serviceFee: function () {
      var serviceFee = this.settings.serviceFee;

      // if the service fee is greater than 1 then the
      // value must be converted to decimals first.
      if (serviceFee > 1) {
        serviceFee = serviceFee / 100;
      }

      return this.settings.loanAmount * serviceFee;
    },

    /**
     * Return the loan fees and commissions total with VAT included.
     * @return {Number}
     */
    _serviceFeeWithVAT: function () {
      return this._serviceFee() * (this._valueAddedTax() + 1);
    },

    /**
     * Calculates the total cost of the loan including the service fee.
     * @return {Number}
     */
    _grandTotal: function () {
      return this._loanTotal() + this._serviceFeeWithVAT();
    },

    /**
     * Return the total annual cost (CAT)
     * @see http://www.banxico.org.mx/CAT
     * @return {Number}
     */
    _CAT: function () {
      var IRR = this._IRR(this._cashFlow());
      var periods = this._paymentFrequency();

      return Math.pow(1 + IRR, periods) - 1;
    },

    /**
     * Returns an array with a series of cash flows for the current loan.
     * @return {Array}
     */
    _cashFlow: function () {
      var results = this._results();
      var cashFlow = [this._serviceFee() - this.settings.loanAmount];

      $.each(results, function (index, period) {
        cashFlow.push(period.payment - period.tax);
      }.bind(this));

      return cashFlow;
    },

    /**
     * Returns the internal rate of return for a series of cash flows represented by the numbers in values.
     * @param  {Array} values
     * @param  {Number} guess
     * @return {Number}
     */
    _IRR: function (values, guess) {
      guess = guess || 0;

      // Calculates the resulting amount
      var irrResult = function (values, dates, rate) {
        var result = values[0];

        for (var i = 1; i < values.length; i++) {
          result += values[i] / Math.pow(rate + 1, (dates[i] - dates[0]) / 365);
        }

        return result;
      };

      // Calculates the first derivation
      var irrResultDerivative = function (values, dates, rate) {
        var result = 0;

        for (var i = 1; i < values.length; i++) {
          var frac = (dates[i] - dates[0]) / 365;
          result -= frac * values[i] / Math.pow(rate + 1, frac + 1);
        }

        return result;
      };

      // Initialize dates and check that values contains at
      // least one positive value and one negative value
      var dates = [];
      var positive = false;
      var negative = false;

      for (var i = 0; i < values.length; i++) {
        dates[i] = i === 0 ? 0 : dates[i - 1] + 365;
        if (values[i] > 0) positive = true;
        if (values[i] < 0) negative = true;
      }

      if (!positive || !negative) {
        throw new Error('Error the values does not contain at least one positive value and one negative value');
      }

      // Initialize guess and resultRate
      guess = guess === undefined ? 0.1 : guess;
      var resultRate = guess;

      // Set maximum epsilon for end of iteration
      var epsMax = 1e-10;

      // Implement Newton's method
      var newRate, epsRate, resultValue;
      var contLoop = true;

      do {
        resultValue = irrResult(values, dates, resultRate);
        newRate = resultRate - resultValue / irrResultDerivative(values, dates, resultRate);
        epsRate = Math.abs(newRate - resultRate);
        resultRate = newRate;
        contLoop = epsRate > epsMax && Math.abs(resultValue) > epsMax;
      } while (contLoop);

      // Return internal rate of return
      return resultRate;
    },

    /**
     * Return the value added tax in decimals.
     * @return {Number}
     */
    _valueAddedTax: function () {
      var tax = this._toNumeric(this.settings.valueAddedTax || 0);

      // if tax is greater than 1 means the value
      // must be converted to decimals first.
      return tax > 1 ? tax / 100 : tax;
    },

    /**
     * Convert numeric format to money format.
     * @param  {Number} numeric
     * @return {String}
     */
    _toMoney: function (numeric) {
      if (typeof numeric == 'string') {
        numeric = parseFloat(numeric);
      }

      return 'AED ' + numeric.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    },

    /**
     * Convert from money format to numeric format.
     * @param  {String} value
     * @return {Number}
     */
    _toNumeric: function (value) {
      return parseFloat(value.toString().replace(/[^0-9\.]+/g, ''));
    },

    /**
     * To convert the provided value to percent format.
     * @param {Number} numeric
     * @returns {String}
     */
    _toPercentage: function (numeric) {
      return (numeric * 100).toFixed(2) + '%';
    }

  });

  /**
   * Wrapper around the constructor to prevent multiple instantiations.
   */
  $.fn.loanCalculator = function (options, args) {
    if (options === 'schedule') {
      return this.data('plugin_loanCalculator').schedule();
    }

    if (options === 'rates') {
      return this.data('plugin_loanCalculator').creditRates();
    }

    return this.each((function () {
      var instance = $.data(this, 'plugin_loanCalculator');
      if (!instance) {
        $.data(this, 'plugin_loanCalculator', new Plugin(this, options));
      } else if (options === 'update') {
        return instance.update(args);
      }
    }));
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5sb2FuLWNhbGN1bGF0b3IuanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsImRvY3VtZW50IiwidW5kZWZpbmVkIiwiQ1JFRElUX1JBVEVTIiwiUEFZTUVOVF9GUkVRVUVOQ0lFUyIsIk1JTklNVU1fTE9BTiIsIk1JTklNVU1fRFVSQVRJT04iLCJkZWZhdWx0cyIsImxvYW5BbW91bnQiLCJsb2FuRHVyYXRpb24iLCJjcmVkaXRSYXRlcyIsImNyZWRpdFNjb3JlIiwidmFsdWVBZGRlZFRheCIsInNlcnZpY2VGZWUiLCJwYXltZW50RnJlcXVlbmN5IiwibG9hbkFtb3VudFNlbGVjdG9yIiwibG9hbkR1cmF0aW9uU2VsZWN0b3IiLCJjcmVkaXRTY29yZVNlbGVjdG9yIiwicGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yIiwic2VsZWN0ZWRBbW91bnQiLCJzZWxlY3RlZER1cmF0aW9uIiwic2VsZWN0ZWRTY29yZSIsInNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSIsImxvYW5Ub3RhbFNlbGVjdG9yIiwicGF5bWVudFNlbGVjdG9yIiwiaW50ZXJlc3RUb3RhbFNlbGVjdG9yIiwic2VydmljZUZlZVNlbGVjdG9yIiwidGF4VG90YWxTZWxlY3RvciIsInRvdGFsQW5udWFsQ29zdFNlbGVjdG9yIiwibG9hbkdyYW5kVG90YWxTZWxlY3RvciIsIlBsdWdpbiIsImVsZW1lbnQiLCJvcHRpb25zIiwiJGVsIiwiX25hbWUiLCJfZGVmYXVsdHMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImF0dGFjaExpc3RlbmVycyIsImluaXQiLCJwcm90b3R5cGUiLCJ2YWxpZGF0ZSIsInJlbmRlciIsImV2ZW50RW1pdHRlcnMiLCJqb2luIiwib24iLCJjaGFuZ2UiLCJldmVudEhhbmRsZXIiLCJiaW5kIiwibW91c2Vtb3ZlIiwidG91Y2htb3ZlIiwidXBkYXRlIiwiZmluZCIsInZhbCIsIl90b051bWVyaWMiLCJpc1BsYWluT2JqZWN0IiwiRXJyb3IiLCJjcmVkaXRSYXRlIiwiaXNOdW1lcmljIiwicGFyc2VGbG9hdCIsInRyaW0iLCJ0b1VwcGVyQ2FzZSIsImhhc093blByb3BlcnR5IiwiX2Rpc3BsYXlTZWxlY3RlZFZhbHVlcyIsIl9kaXNwbGF5UmVzdWx0cyIsImh0bWwiLCJfdG9Nb25leSIsIl9sb2FuVG90YWwiLCJfUE1UIiwiX2ludGVyZXN0VG90YWwiLCJfdGF4VG90YWwiLCJfdG9QZXJjZW50YWdlIiwiX0NBVCIsIl9zZXJ2aWNlRmVlV2l0aFZBVCIsIl9ncmFuZFRvdGFsIiwiYXJncyIsInRyaWdnZXIiLCJfcmVzdWx0cyIsImJhbGFuY2UiLCJpbml0aWFsIiwiaW50ZXJlc3RSYXRlIiwiX2ludGVyZXN0UmF0ZSIsIlZBVCIsIl92YWx1ZUFkZGVkVGF4IiwicGF5bWVudCIsIm51bWJlck9mUGF5bWVudHMiLCJfbnVtYmVyT2ZQYXltZW50cyIsInJlc3VsdHMiLCJwYXltZW50TnVtYmVyIiwiaW50ZXJlc3QiLCJ0YXhlc1BhaWQiLCJwcmluY2lwYWwiLCJwdXNoIiwidGF4Iiwic2NoZWR1bGUiLCJtYXAiLCJ2YWx1ZSIsIl9hbm51YWxJbnRlcmVzdFJhdGUiLCJfcGF5bWVudEZyZXF1ZW5jeSIsImR1cmF0aW9uSW5ZZWFycyIsIk1hdGgiLCJmbG9vciIsImkiLCJMIiwibiIsInBvdyIsInRvdGFsIiwiZWFjaCIsImluZGV4IiwiX3NlcnZpY2VGZWUiLCJJUlIiLCJfSVJSIiwiX2Nhc2hGbG93IiwicGVyaW9kcyIsImNhc2hGbG93IiwicGVyaW9kIiwidmFsdWVzIiwiZ3Vlc3MiLCJpcnJSZXN1bHQiLCJkYXRlcyIsInJhdGUiLCJyZXN1bHQiLCJsZW5ndGgiLCJpcnJSZXN1bHREZXJpdmF0aXZlIiwiZnJhYyIsInBvc2l0aXZlIiwibmVnYXRpdmUiLCJyZXN1bHRSYXRlIiwiZXBzTWF4IiwibmV3UmF0ZSIsImVwc1JhdGUiLCJyZXN1bHRWYWx1ZSIsImNvbnRMb29wIiwiYWJzIiwibnVtZXJpYyIsInRvRml4ZWQiLCJyZXBsYWNlIiwidG9TdHJpbmciLCJmbiIsImxvYW5DYWxjdWxhdG9yIiwiZGF0YSIsImluc3RhbmNlIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7Ozs7QUFNQTtBQUNBLENBQUEsVUFBQUEsQ0FBQSxFQUFBQyxNQUFBLEVBQUFDLFFBQUEsRUFBQUMsU0FBQSxFQUFBOztBQUVBOztBQUVBOzs7OztBQUlBLE1BQUFDLGVBQUE7QUFDQSxTQUFBLElBREE7QUFFQSxTQUFBLElBRkE7QUFHQSxTQUFBLEtBSEE7QUFJQSxTQUFBLEtBSkE7QUFLQSxTQUFBLEtBTEE7QUFNQSxTQUFBLEtBTkE7QUFPQSxTQUFBO0FBUEEsR0FBQTs7QUFVQTs7OztBQUlBLE1BQUFDLHNCQUFBO0FBQ0EsY0FBQSxFQURBO0FBRUEsZ0JBQUEsRUFGQTtBQUdBLGVBQUE7QUFIQSxHQUFBOztBQU1BOzs7O0FBSUEsTUFBQUMsZUFBQSxJQUFBOztBQUVBOzs7O0FBSUEsTUFBQUMsbUJBQUEsQ0FBQTs7QUFFQTs7OztBQUlBLE1BQUFDLFdBQUE7QUFDQTtBQUNBQyxnQkFBQSxLQUZBO0FBR0FDLGtCQUFBLEVBSEE7QUFJQUMsaUJBQUFQLFlBSkE7QUFLQVEsaUJBQUEsR0FMQTtBQU1BQyxtQkFBQSxDQU5BO0FBT0FDLGdCQUFBLENBUEE7QUFRQUMsc0JBQUEsU0FSQTs7QUFVQTtBQUNBQyx3QkFBQSxjQVhBO0FBWUFDLDBCQUFBLGdCQVpBO0FBYUFDLHlCQUFBLGVBYkE7QUFjQUMsOEJBQUEsb0JBZEE7O0FBZ0JBO0FBQ0FDLG9CQUFBLGtCQWpCQTtBQWtCQUMsc0JBQUEsb0JBbEJBO0FBbUJBQyxtQkFBQSxpQkFuQkE7QUFvQkFDLDhCQUFBLDZCQXBCQTs7QUFzQkE7QUFDQUMsdUJBQUEsYUF2QkE7QUF3QkFDLHFCQUFBLFVBeEJBO0FBeUJBQywyQkFBQSxpQkF6QkE7QUEwQkFDLHdCQUFBLGNBMUJBO0FBMkJBQyxzQkFBQSxZQTNCQTtBQTRCQUMsNkJBQUEsb0JBNUJBO0FBNkJBQyw0QkFBQTtBQTdCQSxHQUFBOztBQWdDQTs7Ozs7QUFLQSxXQUFBQyxNQUFBLENBQUFDLE9BQUEsRUFBQUMsT0FBQSxFQUFBO0FBQ0EsU0FBQUMsR0FBQSxHQUFBbEMsRUFBQWdDLE9BQUEsQ0FBQTtBQUNBLFNBQUFHLEtBQUEsR0FBQSxnQkFBQTtBQUNBLFNBQUFDLFNBQUEsR0FBQTVCLFFBQUE7QUFDQSxTQUFBNkIsUUFBQSxHQUFBckMsRUFBQXNDLE1BQUEsQ0FBQSxFQUFBLEVBQUE5QixRQUFBLEVBQUF5QixPQUFBLENBQUE7QUFDQSxTQUFBTSxlQUFBO0FBQ0EsU0FBQUMsSUFBQTtBQUNBOztBQUVBO0FBQ0F4QyxJQUFBc0MsTUFBQSxDQUFBUCxPQUFBVSxTQUFBLEVBQUE7O0FBRUE7Ozs7QUFJQUQsVUFBQSxZQUFBO0FBQ0EsV0FBQUUsUUFBQTtBQUNBLFdBQUFDLE1BQUE7QUFDQSxLQVRBOztBQVdBOzs7O0FBSUFKLHFCQUFBLFlBQUE7QUFDQSxVQUFBSyxnQkFBQSxDQUNBLEtBQUFQLFFBQUEsQ0FBQXJCLGtCQURBLEVBRUEsS0FBQXFCLFFBQUEsQ0FBQXBCLG9CQUZBLEVBR0EsS0FBQW9CLFFBQUEsQ0FBQW5CLG1CQUhBLEVBSUEsS0FBQW1CLFFBQUEsQ0FBQWxCLHdCQUpBLENBQUE7O0FBT0FuQixRQUFBNEMsY0FBQUMsSUFBQSxFQUFBLEVBQUFDLEVBQUEsQ0FBQTtBQUNBQyxnQkFBQSxLQUFBQyxZQUFBLENBQUFDLElBQUEsQ0FBQSxJQUFBLENBREE7QUFFQUMsbUJBQUEsS0FBQUYsWUFBQSxDQUFBQyxJQUFBLENBQUEsSUFBQSxDQUZBO0FBR0FFLG1CQUFBLEtBQUFILFlBQUEsQ0FBQUMsSUFBQSxDQUFBLElBQUE7QUFIQSxPQUFBO0FBS0EsS0E1QkE7O0FBOEJBOzs7O0FBSUFELGtCQUFBLFlBQUE7QUFDQSxXQUFBSSxNQUFBLENBQUE7QUFDQTNDLG9CQUFBLEtBQUF5QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXJCLGtCQUFBLEVBQUFzQyxHQUFBLEVBREE7QUFFQTVDLHNCQUFBLEtBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXBCLG9CQUFBLEVBQUFxQyxHQUFBLEVBRkE7QUFHQTFDLHFCQUFBLEtBQUFzQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQW5CLG1CQUFBLEVBQUFvQyxHQUFBLEVBSEE7QUFJQXZDLDBCQUFBLEtBQUFtQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWxCLHdCQUFBLEVBQUFtQyxHQUFBO0FBSkEsT0FBQTtBQU1BLEtBekNBOztBQTJDQTs7Ozs7QUFLQVosY0FBQSxZQUFBO0FBQ0EsVUFBQSxPQUFBLEtBQUFMLFFBQUEsQ0FBQTVCLFVBQUEsS0FBQSxRQUFBLEVBQUE7QUFDQSxhQUFBNEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBLEtBQUE4QyxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTVCLFVBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsT0FBQSxLQUFBNEIsUUFBQSxDQUFBdkIsVUFBQSxLQUFBLFFBQUEsRUFBQTtBQUNBLGFBQUF1QixRQUFBLENBQUF2QixVQUFBLEdBQUEsS0FBQXlDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBZCxFQUFBd0QsYUFBQSxDQUFBLEtBQUFuQixRQUFBLENBQUExQixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQThDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsV0FBQSxJQUFBQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsRUFBQTtBQUNBLFlBQUEsT0FBQSxLQUFBMEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLEtBQUEsUUFBQSxFQUFBO0FBQ0EsZUFBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxLQUFBSCxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsQ0FBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxDQUFBMUQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLENBQUEsRUFBQTtBQUNBLGdCQUFBLElBQUFELEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxJQUFBLENBQUEsRUFBQTtBQUNBLGVBQUFyQixRQUFBLENBQUExQixXQUFBLENBQUErQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxHQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBQUFyQixRQUFBLENBQUE1QixVQUFBLEdBQUFtRCxXQUFBLEtBQUF2QixRQUFBLENBQUE1QixVQUFBLENBQUE7QUFDQSxXQUFBNEIsUUFBQSxDQUFBM0IsWUFBQSxHQUFBa0QsV0FBQSxLQUFBdkIsUUFBQSxDQUFBM0IsWUFBQSxDQUFBO0FBQ0EsV0FBQTJCLFFBQUEsQ0FBQXZCLFVBQUEsR0FBQThDLFdBQUEsS0FBQXZCLFFBQUEsQ0FBQXZCLFVBQUEsQ0FBQTtBQUNBLFdBQUF1QixRQUFBLENBQUF6QixXQUFBLEdBQUFaLEVBQUE2RCxJQUFBLENBQUEsS0FBQXhCLFFBQUEsQ0FBQXpCLFdBQUEsQ0FBQWtELFdBQUEsRUFBQSxDQUFBOztBQUVBLFVBQUEsQ0FBQXpELG9CQUFBMEQsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF0QixnQkFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEwQyxLQUFBLENBQUEseURBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBb0QsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF6QixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQTZDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxLQUFBcEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBSCxZQUFBLEVBQUE7QUFDQSxjQUFBLElBQUFtRCxLQUFBLENBQUEsNERBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsS0FBQXBCLFFBQUEsQ0FBQTNCLFlBQUEsR0FBQUgsZ0JBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQWtELEtBQUEsQ0FBQSwyREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBekQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEyQyxLQUFBLENBQUEsbURBQUEsQ0FBQTtBQUNBO0FBQ0EsS0FwR0E7O0FBc0dBOzs7O0FBSUFkLFlBQUEsWUFBQTtBQUNBLFdBQUFxQixzQkFBQTtBQUNBLFdBQUFDLGVBQUE7QUFDQSxLQTdHQTs7QUErR0E7Ozs7QUFJQUQsNEJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQTlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBakIsY0FBQSxFQUFBOEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBOUIsUUFBQSxDQUFBNUIsVUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBaEIsZ0JBQUEsRUFBQTZDLElBQUEsQ0FDQSxLQUFBN0IsUUFBQSxDQUFBM0IsWUFEQTs7QUFJQTtBQUNBLFdBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWYsYUFBQSxFQUFBNEMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF6QixXQURBOztBQUlBO0FBQ0EsV0FBQXNCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBZCx3QkFBQSxFQUFBMkMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF0QixnQkFEQTtBQUdBLEtBdklBOztBQXlJQTs7OztBQUlBa0QscUJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQS9CLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBYixpQkFBQSxFQUFBMEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBQyxVQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFsQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVosZUFBQSxFQUFBeUMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBRSxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFuQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVgscUJBQUEsRUFBQXdDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQUcsY0FBQSxFQUFBLENBREE7O0FBSUE7QUFDQSxXQUFBcEMsR0FBQSxDQUFBbUIsSUFBQSxDQUFBLEtBQUFoQixRQUFBLENBQUFULGdCQUFBLEVBQUFzQyxJQUFBLENBQ0EsS0FBQUMsUUFBQSxDQUFBLEtBQUFJLFNBQUEsRUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXJDLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBUix1QkFBQSxFQUFBcUMsSUFBQSxDQUNBLEtBQUFNLGFBQUEsQ0FBQSxLQUFBQyxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUF2QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVYsa0JBQUEsRUFBQXVDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQU8sa0JBQUEsRUFBQSxDQURBOztBQUlBLFdBQUF4QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVAsc0JBQUEsRUFBQW9DLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQVEsV0FBQSxFQUFBLENBREE7QUFHQSxLQS9LQTs7QUFpTEE7Ozs7QUFJQXZCLFlBQUEsVUFBQXdCLElBQUEsRUFBQTtBQUNBLFdBQUF2QyxRQUFBLEdBQUFyQyxFQUFBc0MsTUFBQSxDQUFBLEVBQUEsRUFBQSxLQUFBRixTQUFBLEVBQUEsS0FBQUMsUUFBQSxFQUFBdUMsSUFBQSxDQUFBO0FBQ0EsV0FBQXBDLElBQUE7QUFDQSxXQUFBTixHQUFBLENBQUEyQyxPQUFBLENBQUEsYUFBQTtBQUNBLEtBekxBOztBQTJMQTs7Ozs7QUFLQUMsY0FBQSxZQUFBO0FBQ0EsVUFBQUMsVUFBQSxLQUFBMUMsUUFBQSxDQUFBNUIsVUFBQTtBQUNBLFVBQUF1RSxVQUFBLEtBQUEzQyxRQUFBLENBQUE1QixVQUFBO0FBQ0EsVUFBQXdFLGVBQUEsS0FBQUMsYUFBQSxFQUFBO0FBQ0EsVUFBQUMsTUFBQSxLQUFBQyxjQUFBLEVBQUE7QUFDQSxVQUFBQyxVQUFBLEtBQUFoQixJQUFBLEVBQUE7QUFDQSxVQUFBaUIsbUJBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLFVBQUFDLFVBQUEsRUFBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFBLElBQUFDLGdCQUFBLENBQUEsRUFBQUEsZ0JBQUFILGdCQUFBLEVBQUFHLGVBQUEsRUFBQTtBQUNBLFlBQUFDLFdBQUFYLFVBQUFFLFlBQUE7QUFDQSxZQUFBVSxZQUFBWixVQUFBRSxZQUFBLEdBQUFFLEdBQUE7QUFDQSxZQUFBUyxZQUFBUCxVQUFBSyxRQUFBLEdBQUFDLFNBQUE7O0FBRUE7QUFDQVgsa0JBQUFELE9BQUE7O0FBRUE7QUFDQUEsa0JBQUFBLFVBQUFhLFNBQUE7O0FBRUFKLGdCQUFBSyxJQUFBLENBQUE7QUFDQWIsbUJBQUFBLE9BREE7QUFFQVkscUJBQUFBLFNBRkE7QUFHQUYsb0JBQUFBLFFBSEE7QUFJQUksZUFBQUgsU0FKQTtBQUtBTixtQkFBQUEsT0FMQTtBQU1BTixtQkFBQUE7QUFOQSxTQUFBO0FBUUE7O0FBRUEsYUFBQVMsT0FBQTtBQUNBLEtBbE9BOztBQW9PQTs7OztBQUlBTyxjQUFBLFlBQUE7QUFDQSxhQUFBL0YsRUFBQWdHLEdBQUEsQ0FBQSxLQUFBbEIsUUFBQSxFQUFBLEVBQUEsVUFBQW1CLEtBQUEsRUFBQTtBQUNBLGVBQUE7QUFDQWpCLG1CQUFBLEtBQUFiLFFBQUEsQ0FBQThCLE1BQUFqQixPQUFBLENBREE7QUFFQVkscUJBQUEsS0FBQXpCLFFBQUEsQ0FBQThCLE1BQUFMLFNBQUEsQ0FGQTtBQUdBRixvQkFBQSxLQUFBdkIsUUFBQSxDQUFBOEIsTUFBQVAsUUFBQSxDQUhBO0FBSUFJLGVBQUEsS0FBQTNCLFFBQUEsQ0FBQThCLE1BQUFILEdBQUEsQ0FKQTtBQUtBVCxtQkFBQSxLQUFBbEIsUUFBQSxDQUFBOEIsTUFBQVosT0FBQSxDQUxBO0FBTUFOLG1CQUFBLEtBQUFaLFFBQUEsQ0FBQThCLE1BQUFsQixPQUFBO0FBTkEsU0FBQTtBQVFBLE9BVEEsQ0FTQTlCLElBVEEsQ0FTQSxJQVRBLENBQUEsQ0FBQTtBQVVBLEtBblBBOztBQXFQQTs7OztBQUlBdEMsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQTBCLFFBQUEsQ0FBQTFCLFdBQUE7QUFDQSxLQTNQQTs7QUE2UEE7Ozs7QUFJQXVGLHlCQUFBLFlBQUE7QUFDQSxVQUFBLEtBQUE3RCxRQUFBLENBQUEwQixjQUFBLENBQUEsY0FBQSxDQUFBLEVBQUE7QUFDQSxZQUFBLEtBQUExQixRQUFBLENBQUE0QyxZQUFBLElBQUEsQ0FBQSxFQUFBO0FBQ0EsaUJBQUEsS0FBQTVDLFFBQUEsQ0FBQTRDLFlBQUE7QUFDQTs7QUFFQSxlQUFBLEtBQUExQixVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTRDLFlBQUEsSUFBQSxHQUFBO0FBQ0E7O0FBRUEsYUFBQSxLQUFBNUMsUUFBQSxDQUFBMUIsV0FBQSxDQUFBLEtBQUEwQixRQUFBLENBQUF6QixXQUFBLElBQUEsR0FBQTtBQUNBLEtBM1FBOztBQTZRQTs7OztBQUlBc0UsbUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQWdCLG1CQUFBLEtBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLEtBblJBOztBQXNSQTs7OztBQUlBQSx1QkFBQSxZQUFBO0FBQ0EsYUFBQTlGLG9CQUFBLEtBQUFnQyxRQUFBLENBQUF0QixnQkFBQSxDQUFBO0FBQ0EsS0E1UkE7O0FBOFJBOzs7O0FBSUF3RSx1QkFBQSxZQUFBO0FBQ0EsVUFBQWEsa0JBQUEsS0FBQTdDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBM0IsWUFBQSxJQUFBLEVBQUE7O0FBRUEsYUFBQTJGLEtBQUFDLEtBQUEsQ0FBQUYsa0JBQUEvRixvQkFBQSxLQUFBZ0MsUUFBQSxDQUFBdEIsZ0JBQUEsQ0FBQSxDQUFBO0FBQ0EsS0F0U0E7O0FBd1NBOzs7O0FBSUFxRCxnQkFBQSxZQUFBO0FBQ0EsYUFBQSxLQUFBQyxJQUFBLEtBQUEsS0FBQWtCLGlCQUFBLEVBQUE7QUFDQSxLQTlTQTs7QUFnVEE7Ozs7O0FBS0FsQixVQUFBLFlBQUE7QUFDQSxVQUFBa0MsSUFBQSxLQUFBckIsYUFBQSxFQUFBO0FBQ0EsVUFBQXNCLElBQUEsS0FBQW5FLFFBQUEsQ0FBQTVCLFVBQUE7QUFDQSxVQUFBZ0csSUFBQSxLQUFBbEIsaUJBQUEsRUFBQTs7QUFFQSxVQUFBLEtBQUFsRCxRQUFBLENBQUF4QixhQUFBLEtBQUEsQ0FBQSxFQUFBO0FBQ0EwRixZQUFBLENBQUEsSUFBQSxLQUFBbkIsY0FBQSxFQUFBLElBQUFtQixDQUFBLENBREEsQ0FDQTtBQUNBOztBQUVBLGFBQUFDLElBQUFELENBQUEsSUFBQSxJQUFBRixLQUFBSyxHQUFBLENBQUEsSUFBQUgsQ0FBQSxFQUFBLENBQUFFLENBQUEsQ0FBQSxDQUFBO0FBQ0EsS0EvVEE7O0FBaVVBOzs7O0FBSUFuQyxvQkFBQSxZQUFBO0FBQ0EsVUFBQXFDLFFBQUEsQ0FBQTtBQUNBM0csUUFBQTRHLElBQUEsQ0FBQSxLQUFBOUIsUUFBQSxFQUFBLEVBQUEsVUFBQStCLEtBQUEsRUFBQVosS0FBQSxFQUFBO0FBQ0FVLGlCQUFBVixNQUFBUCxRQUFBO0FBQ0EsT0FGQTtBQUdBLGFBQUFpQixLQUFBO0FBQ0EsS0EzVUE7O0FBNlVBOzs7O0FBSUFwQyxlQUFBLFlBQUE7QUFDQSxVQUFBb0MsUUFBQSxDQUFBO0FBQ0EzRyxRQUFBNEcsSUFBQSxDQUFBLEtBQUE5QixRQUFBLEVBQUEsRUFBQSxVQUFBK0IsS0FBQSxFQUFBWixLQUFBLEVBQUE7QUFDQVUsaUJBQUFWLE1BQUFILEdBQUE7QUFDQSxPQUZBO0FBR0EsYUFBQWEsS0FBQTtBQUNBLEtBdlZBOztBQXlWQTs7OztBQUlBRyxpQkFBQSxZQUFBO0FBQ0EsVUFBQWhHLGFBQUEsS0FBQXVCLFFBQUEsQ0FBQXZCLFVBQUE7O0FBRUE7QUFDQTtBQUNBLFVBQUFBLGFBQUEsQ0FBQSxFQUFBO0FBQ0FBLHFCQUFBQSxhQUFBLEdBQUE7QUFDQTs7QUFFQSxhQUFBLEtBQUF1QixRQUFBLENBQUE1QixVQUFBLEdBQUFLLFVBQUE7QUFDQSxLQXZXQTs7QUF5V0E7Ozs7QUFJQTRELHdCQUFBLFlBQUE7QUFDQSxhQUFBLEtBQUFvQyxXQUFBLE1BQUEsS0FBQTFCLGNBQUEsS0FBQSxDQUFBLENBQUE7QUFDQSxLQS9XQTs7QUFpWEE7Ozs7QUFJQVQsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQVAsVUFBQSxLQUFBLEtBQUFNLGtCQUFBLEVBQUE7QUFDQSxLQXZYQTs7QUF5WEE7Ozs7O0FBS0FELFVBQUEsWUFBQTtBQUNBLFVBQUFzQyxNQUFBLEtBQUFDLElBQUEsQ0FBQSxLQUFBQyxTQUFBLEVBQUEsQ0FBQTtBQUNBLFVBQUFDLFVBQUEsS0FBQWYsaUJBQUEsRUFBQTs7QUFFQSxhQUFBRSxLQUFBSyxHQUFBLENBQUEsSUFBQUssR0FBQSxFQUFBRyxPQUFBLElBQUEsQ0FBQTtBQUNBLEtBbllBOztBQXFZQTs7OztBQUlBRCxlQUFBLFlBQUE7QUFDQSxVQUFBekIsVUFBQSxLQUFBVixRQUFBLEVBQUE7QUFDQSxVQUFBcUMsV0FBQSxDQUFBLEtBQUFMLFdBQUEsS0FBQSxLQUFBekUsUUFBQSxDQUFBNUIsVUFBQSxDQUFBOztBQUVBVCxRQUFBNEcsSUFBQSxDQUFBcEIsT0FBQSxFQUFBLFVBQUFxQixLQUFBLEVBQUFPLE1BQUEsRUFBQTtBQUNBRCxpQkFBQXRCLElBQUEsQ0FBQXVCLE9BQUEvQixPQUFBLEdBQUErQixPQUFBdEIsR0FBQTtBQUNBLE9BRkEsQ0FFQTdDLElBRkEsQ0FFQSxJQUZBLENBQUE7O0FBSUEsYUFBQWtFLFFBQUE7QUFDQSxLQWxaQTs7QUFvWkE7Ozs7OztBQU1BSCxVQUFBLFVBQUFLLE1BQUEsRUFBQUMsS0FBQSxFQUFBO0FBQ0FBLGNBQUFBLFNBQUEsQ0FBQTs7QUFFQTtBQUNBLFVBQUFDLFlBQUEsVUFBQUYsTUFBQSxFQUFBRyxLQUFBLEVBQUFDLElBQUEsRUFBQTtBQUNBLFlBQUFDLFNBQUFMLE9BQUEsQ0FBQSxDQUFBOztBQUVBLGFBQUEsSUFBQWQsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBbUIsb0JBQUFMLE9BQUFkLENBQUEsSUFBQUYsS0FBQUssR0FBQSxDQUFBZSxPQUFBLENBQUEsRUFBQSxDQUFBRCxNQUFBakIsQ0FBQSxJQUFBaUIsTUFBQSxDQUFBLENBQUEsSUFBQSxHQUFBLENBQUE7QUFDQTs7QUFFQSxlQUFBRSxNQUFBO0FBQ0EsT0FSQTs7QUFVQTtBQUNBLFVBQUFFLHNCQUFBLFVBQUFQLE1BQUEsRUFBQUcsS0FBQSxFQUFBQyxJQUFBLEVBQUE7QUFDQSxZQUFBQyxTQUFBLENBQUE7O0FBRUEsYUFBQSxJQUFBbkIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBLGNBQUFzQixPQUFBLENBQUFMLE1BQUFqQixDQUFBLElBQUFpQixNQUFBLENBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQUUsb0JBQUFHLE9BQUFSLE9BQUFkLENBQUEsQ0FBQSxHQUFBRixLQUFBSyxHQUFBLENBQUFlLE9BQUEsQ0FBQSxFQUFBSSxPQUFBLENBQUEsQ0FBQTtBQUNBOztBQUVBLGVBQUFILE1BQUE7QUFDQSxPQVRBOztBQVdBO0FBQ0E7QUFDQSxVQUFBRixRQUFBLEVBQUE7QUFDQSxVQUFBTSxXQUFBLEtBQUE7QUFDQSxVQUFBQyxXQUFBLEtBQUE7O0FBRUEsV0FBQSxJQUFBeEIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBaUIsY0FBQWpCLENBQUEsSUFBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxHQUFBaUIsTUFBQWpCLElBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQSxZQUFBYyxPQUFBZCxDQUFBLElBQUEsQ0FBQSxFQUFBdUIsV0FBQSxJQUFBO0FBQ0EsWUFBQVQsT0FBQWQsQ0FBQSxJQUFBLENBQUEsRUFBQXdCLFdBQUEsSUFBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQUQsUUFBQSxJQUFBLENBQUFDLFFBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQXRFLEtBQUEsQ0FDQSxzRkFEQSxDQUFBO0FBR0E7O0FBRUE7QUFDQTZELGNBQUFBLFVBQUFuSCxTQUFBLEdBQUEsR0FBQSxHQUFBbUgsS0FBQTtBQUNBLFVBQUFVLGFBQUFWLEtBQUE7O0FBRUE7QUFDQSxVQUFBVyxTQUFBLEtBQUE7O0FBRUE7QUFDQSxVQUFBQyxPQUFBLEVBQUFDLE9BQUEsRUFBQUMsV0FBQTtBQUNBLFVBQUFDLFdBQUEsSUFBQTs7QUFFQSxTQUFBO0FBQ0FELHNCQUFBYixVQUFBRixNQUFBLEVBQUFHLEtBQUEsRUFBQVEsVUFBQSxDQUFBO0FBQ0FFLGtCQUFBRixhQUFBSSxjQUFBUixvQkFBQVAsTUFBQSxFQUFBRyxLQUFBLEVBQUFRLFVBQUEsQ0FBQTtBQUNBRyxrQkFBQTlCLEtBQUFpQyxHQUFBLENBQUFKLFVBQUFGLFVBQUEsQ0FBQTtBQUNBQSxxQkFBQUUsT0FBQTtBQUNBRyxtQkFBQUYsVUFBQUYsTUFBQSxJQUFBNUIsS0FBQWlDLEdBQUEsQ0FBQUYsV0FBQSxJQUFBSCxNQUFBO0FBQ0EsT0FOQSxRQU1BSSxRQU5BOztBQVFBO0FBQ0EsYUFBQUwsVUFBQTtBQUNBLEtBM2RBOztBQTZkQTs7OztBQUlBNUMsb0JBQUEsWUFBQTtBQUNBLFVBQUFVLE1BQUEsS0FBQXZDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBeEIsYUFBQSxJQUFBLENBQUEsQ0FBQTs7QUFFQTtBQUNBO0FBQ0EsYUFBQWlGLE1BQUEsQ0FBQSxHQUFBQSxNQUFBLEdBQUEsR0FBQUEsR0FBQTtBQUNBLEtBdmVBOztBQXllQTs7Ozs7QUFLQTNCLGNBQUEsVUFBQW9FLE9BQUEsRUFBQTtBQUNBLFVBQUEsT0FBQUEsT0FBQSxJQUFBLFFBQUEsRUFBQTtBQUNBQSxrQkFBQTNFLFdBQUEyRSxPQUFBLENBQUE7QUFDQTs7QUFFQSxhQUFBLFNBQUFBLFFBQUFDLE9BQUEsQ0FBQSxDQUFBLEVBQUFDLE9BQUEsQ0FBQSxxQkFBQSxFQUFBLEtBQUEsQ0FBQTtBQUNBLEtBcGZBOztBQXNmQTs7Ozs7QUFLQWxGLGdCQUFBLFVBQUEwQyxLQUFBLEVBQUE7QUFDQSxhQUFBckMsV0FDQXFDLE1BQUF5QyxRQUFBLEdBQUFELE9BQUEsQ0FBQSxZQUFBLEVBQUEsRUFBQSxDQURBLENBQUE7QUFHQSxLQS9mQTs7QUFpZ0JBOzs7OztBQUtBakUsbUJBQUEsVUFBQStELE9BQUEsRUFBQTtBQUNBLGFBQUEsQ0FBQUEsVUFBQSxHQUFBLEVBQUFDLE9BQUEsQ0FBQSxDQUFBLElBQUEsR0FBQTtBQUNBOztBQXhnQkEsR0FBQTs7QUE0Z0JBOzs7QUFHQXhJLElBQUEySSxFQUFBLENBQUFDLGNBQUEsR0FBQSxVQUFBM0csT0FBQSxFQUFBMkMsSUFBQSxFQUFBO0FBQ0EsUUFBQTNDLFlBQUEsVUFBQSxFQUFBO0FBQ0EsYUFBQSxLQUFBNEcsSUFBQSxDQUFBLHVCQUFBLEVBQUE5QyxRQUFBLEVBQUE7QUFDQTs7QUFFQSxRQUFBOUQsWUFBQSxPQUFBLEVBQUE7QUFDQSxhQUFBLEtBQUE0RyxJQUFBLENBQUEsdUJBQUEsRUFBQWxJLFdBQUEsRUFBQTtBQUNBOztBQUVBLFdBQUEsS0FBQWlHLElBQUEsQ0FBQSxZQUFBO0FBQ0EsVUFBQWtDLFdBQUE5SSxFQUFBNkksSUFBQSxDQUFBLElBQUEsRUFBQSx1QkFBQSxDQUFBO0FBQ0EsVUFBQSxDQUFBQyxRQUFBLEVBQUE7QUFDQTlJLFVBQUE2SSxJQUFBLENBQUEsSUFBQSxFQUFBLHVCQUFBLEVBQUEsSUFBQTlHLE1BQUEsQ0FBQSxJQUFBLEVBQUFFLE9BQUEsQ0FBQTtBQUNBLE9BRkEsTUFFQSxJQUFBQSxZQUFBLFFBQUEsRUFBQTtBQUNBLGVBQUE2RyxTQUFBMUYsTUFBQSxDQUFBd0IsSUFBQSxDQUFBO0FBQ0E7QUFDQSxLQVBBLENBQUE7QUFRQSxHQWpCQTtBQW1CQSxDQTduQkEsRUE2bkJBbUUsTUE3bkJBLEVBNm5CQTlJLE1BN25CQSxFQTZuQkFDLFFBN25CQSIsImZpbGUiOiJsb2FuLWNhbGN1bGF0b3IucG9seWZpbGxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIGpRdWVyeSBMb2FuIENhbGN1bGF0b3IgMy4xLjNcclxuICpcclxuICogQXV0aG9yOiBKb3JnZSBHb256w6FsZXogPHNjcnViLm14QGdtYWlsPlxyXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgLSBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxyXG4gKi9cclxuO1xyXG4oZnVuY3Rpb24gKCQsIHdpbmRvdywgZG9jdW1lbnQsIHVuZGVmaW5lZCkge1xyXG5cclxuICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgLyoqXHJcbiAgICogVGFibGUgb2YgY3JlZGl0IHJhdGVzIGZvciBldmVyeSBzY29yZS5cclxuICAgKiBAdHlwZSB7T2JqZWN0fVxyXG4gICAqL1xyXG4gIHZhciBDUkVESVRfUkFURVMgPSB7XHJcbiAgICAnQSc6IDUuMzIsXHJcbiAgICAnQic6IDguMTgsXHJcbiAgICAnQyc6IDEyLjI5LFxyXG4gICAgJ0QnOiAxNS42MSxcclxuICAgICdFJzogMTguMjUsXHJcbiAgICAnRic6IDIxLjk5LFxyXG4gICAgJ0cnOiAyNi43N1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIFRhYmxlIG9mIGFsbG93ZWQgcGF5bWVudCBmcmVxdWVuY2llcy5cclxuICAgKiBAdHlwZSB7T2JqZWN0fVxyXG4gICAqL1xyXG4gIHZhciBQQVlNRU5UX0ZSRVFVRU5DSUVTID0ge1xyXG4gICAgJ3dlZWtseSc6IDUyLFxyXG4gICAgJ2Jpd2Vla2x5JzogMjYsXHJcbiAgICAnbW9udGhseSc6IDEyXHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIG1pbmltdW0gYWxsb3dlZCBmb3IgYSBsb2FuLlxyXG4gICAqIEB0eXBlIHtOdW1iZXJ9XHJcbiAgICovXHJcbiAgdmFyIE1JTklNVU1fTE9BTiA9IDEwMDA7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBtaW5pbXVtIGR1cmF0aW9uIGluIG1vbnRocy5cclxuICAgKiBAdHlwZSB7TnVtYmVyfVxyXG4gICAqL1xyXG4gIHZhciBNSU5JTVVNX0RVUkFUSU9OID0gMTtcclxuXHJcbiAgLyoqXHJcbiAgICogRGVmYXVsdCBvcHRpb25zIGZvciB0aGUgcGx1Z2luLlxyXG4gICAqIEB0eXBlIHtPYmplY3R9XHJcbiAgICovXHJcbiAgdmFyIGRlZmF1bHRzID0ge1xyXG4gICAgLy8gZGVmYXVsdCB2YWx1ZXMgZm9yIGEgbG9hblxyXG4gICAgbG9hbkFtb3VudDogNTAwMDAsXHJcbiAgICBsb2FuRHVyYXRpb246IDEyLFxyXG4gICAgY3JlZGl0UmF0ZXM6IENSRURJVF9SQVRFUyxcclxuICAgIGNyZWRpdFNjb3JlOiAnQScsXHJcbiAgICB2YWx1ZUFkZGVkVGF4OiAwLFxyXG4gICAgc2VydmljZUZlZTogMCxcclxuICAgIHBheW1lbnRGcmVxdWVuY3k6ICdtb250aGx5JyxcclxuXHJcbiAgICAvLyBpbnB1dHNcclxuICAgIGxvYW5BbW91bnRTZWxlY3RvcjogJyNsb2FuLWFtb3VudCcsXHJcbiAgICBsb2FuRHVyYXRpb25TZWxlY3RvcjogJyNsb2FuLWR1cmF0aW9uJyxcclxuICAgIGNyZWRpdFNjb3JlU2VsZWN0b3I6ICcjY3JlZGl0LXNjb3JlJyxcclxuICAgIHBheW1lbnRGcmVxdWVuY3lTZWxlY3RvcjogJyNwYXltZW50LWZyZXF1ZW5jeScsXHJcblxyXG4gICAgLy8gZGlzcGxheSBzZWxlY3RlZCB2YWx1ZXNcclxuICAgIHNlbGVjdGVkQW1vdW50OiAnI3NlbGVjdGVkLWFtb3VudCcsXHJcbiAgICBzZWxlY3RlZER1cmF0aW9uOiAnI3NlbGVjdGVkLWR1cmF0aW9uJyxcclxuICAgIHNlbGVjdGVkU2NvcmU6ICcjc2VsZWN0ZWQtc2NvcmUnLFxyXG4gICAgc2VsZWN0ZWRQYXltZW50RnJlcXVlbmN5OiAnI3NlbGVjdGVkLXBheW1lbnQtZnJlcXVlbmN5JyxcclxuXHJcbiAgICAvLyBkaXNwbGF5IHRoZSByZXN1bHRzXHJcbiAgICBsb2FuVG90YWxTZWxlY3RvcjogJyNsb2FuLXRvdGFsJyxcclxuICAgIHBheW1lbnRTZWxlY3RvcjogJyNwYXltZW50JyxcclxuICAgIGludGVyZXN0VG90YWxTZWxlY3RvcjogJyNpbnRlcmVzdC10b3RhbCcsXHJcbiAgICBzZXJ2aWNlRmVlU2VsZWN0b3I6ICcjc2VydmljZS1mZWUnLFxyXG4gICAgdGF4VG90YWxTZWxlY3RvcjogJyN0YXgtdG90YWwnLFxyXG4gICAgdG90YWxBbm51YWxDb3N0U2VsZWN0b3I6ICcjdG90YWwtYW5udWFsLWNvc3QnLFxyXG4gICAgbG9hbkdyYW5kVG90YWxTZWxlY3RvcjogJyNncmFuZC10b3RhbCdcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgYWN0dWFsIHBsdWdpbiBjb25zdHJ1Y3RvclxyXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBlbGVtZW50XHJcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcclxuICAgKi9cclxuICBmdW5jdGlvbiBQbHVnaW4oZWxlbWVudCwgb3B0aW9ucykge1xyXG4gICAgdGhpcy4kZWwgPSAkKGVsZW1lbnQpO1xyXG4gICAgdGhpcy5fbmFtZSA9ICdsb2FuQ2FsY3VsYXRvcic7XHJcbiAgICB0aGlzLl9kZWZhdWx0cyA9IGRlZmF1bHRzO1xyXG4gICAgdGhpcy5zZXR0aW5ncyA9ICQuZXh0ZW5kKHt9LCBkZWZhdWx0cywgb3B0aW9ucyk7XHJcbiAgICB0aGlzLmF0dGFjaExpc3RlbmVycygpO1xyXG4gICAgdGhpcy5pbml0KCk7XHJcbiAgfVxyXG5cclxuICAvLyBBdm9pZCBQbHVnaW4ucHJvdG90eXBlIGNvbmZsaWN0c1xyXG4gICQuZXh0ZW5kKFBsdWdpbi5wcm90b3R5cGUsIHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFZhbGlkYXRlcyB0aGUgZGF0YSBhbmQgc2hvd3MgdGhlIHJlc3VsdHMuXHJcbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxyXG4gICAgICovXHJcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHRoaXMudmFsaWRhdGUoKTtcclxuICAgICAgdGhpcy5yZW5kZXIoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBdHRhY2ggZXZlbnQgbGlzdGVuZXJzIHRvIHRoZSBldmVudCBoYW5kbGVycy5cclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgKi9cclxuICAgIGF0dGFjaExpc3RlbmVyczogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgZXZlbnRFbWl0dGVycyA9IFtcclxuICAgICAgICB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnRTZWxlY3RvcixcclxuICAgICAgICB0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvblNlbGVjdG9yLFxyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVTZWxlY3RvcixcclxuICAgICAgICB0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3lTZWxlY3RvclxyXG4gICAgICBdO1xyXG5cclxuICAgICAgJChldmVudEVtaXR0ZXJzLmpvaW4oKSkub24oe1xyXG4gICAgICAgIGNoYW5nZTogdGhpcy5ldmVudEhhbmRsZXIuYmluZCh0aGlzKSxcclxuICAgICAgICBtb3VzZW1vdmU6IHRoaXMuZXZlbnRIYW5kbGVyLmJpbmQodGhpcyksXHJcbiAgICAgICAgdG91Y2htb3ZlOiB0aGlzLmV2ZW50SGFuZGxlci5iaW5kKHRoaXMpXHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEhhbmRsZSBldmVudHMgZnJvbSB0aGUgRE9NLlxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgZXZlbnRIYW5kbGVyOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHRoaXMudXBkYXRlKHtcclxuICAgICAgICBsb2FuQW1vdW50OiB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudFNlbGVjdG9yKS52YWwoKSxcclxuICAgICAgICBsb2FuRHVyYXRpb246IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb25TZWxlY3RvcikudmFsKCksXHJcbiAgICAgICAgY3JlZGl0U2NvcmU6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZVNlbGVjdG9yKS52YWwoKSxcclxuICAgICAgICBwYXltZW50RnJlcXVlbmN5OiB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MucGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yKS52YWwoKVxyXG4gICAgICB9KTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTYW5pdGl6ZSBhbmQgdmFsaWRhdGUgdGhlIHVzZXIgaW5wdXQgZGF0YS5cclxuICAgICAqIEB0aHJvd3MgRXJyb3JcclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgKi9cclxuICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICh0eXBlb2YgdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodHlwZW9mIHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICB0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUgPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCEkLmlzUGxhaW5PYmplY3QodGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlcykpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2NyZWRpdFJhdGVzXSBpcyBub3QgdmFsaWQuJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZvciAodmFyIGNyZWRpdFJhdGUgaW4gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlcykge1xyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSlcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghJC5pc051bWVyaWModGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSkpIHtcclxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbY3JlZGl0UmF0ZXNdIGlzIG5vdCB2YWxpZC4nKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gPCAxKSB7XHJcbiAgICAgICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdID0gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSAqIDEwMDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFNhbml0aXplIHRoZSBpbnB1dFxyXG4gICAgICB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPSBwYXJzZUZsb2F0KHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCk7XHJcbiAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uID0gcGFyc2VGbG9hdCh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvbik7XHJcbiAgICAgIHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSA9IHBhcnNlRmxvYXQodGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlKTtcclxuICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZSA9ICQudHJpbSh0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlLnRvVXBwZXJDYXNlKCkpO1xyXG5cclxuICAgICAgaWYgKCFQQVlNRU5UX0ZSRVFVRU5DSUVTLmhhc093blByb3BlcnR5KHRoaXMuc2V0dGluZ3MucGF5bWVudEZyZXF1ZW5jeSkpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW3BheW1lbnRGcmVxdWVuY3ldIGlzIG5vdCB2YWxpZC4nKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCF0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzLmhhc093blByb3BlcnR5KHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmUpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtjcmVkaXRTY29yZV0gaXMgbm90IHZhbGlkLicpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50IDwgTUlOSU1VTV9MT0FOKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtsb2FuQW1vdW50XSBtdXN0IG1lIGF0IGxlYXN0IDEwMDAuJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvbiA8IE1JTklNVU1fRFVSQVRJT04pIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2xvYW5EdXJhdGlvbl0gbXVzdCBtZSBhdCBsZWFzdCAxLicpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoISQuaXNOdW1lcmljKHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSkpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW3NlcnZpY2VGZWVdIGlzIG5vdCB2YWxpZC4nKTtcclxuICAgICAgfVxyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNob3cgdGhlIHJlc3VsdHMgaW4gdGhlIERPTS5cclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgKi9cclxuICAgIHJlbmRlcjogZnVuY3Rpb24gKCkge1xyXG4gICAgICB0aGlzLl9kaXNwbGF5U2VsZWN0ZWRWYWx1ZXMoKTtcclxuICAgICAgdGhpcy5fZGlzcGxheVJlc3VsdHMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTaG93IHRoZSBzZWxlY3RlZCB2YWx1ZXMgaW4gdGhlIERPTS5cclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgKi9cclxuICAgIF9kaXNwbGF5U2VsZWN0ZWRWYWx1ZXM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgbG9hbiBhbW91bnRcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkQW1vdW50KS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50KVxyXG4gICAgICApO1xyXG5cclxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgbG9hbiBkdXJhdGlvblxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWREdXJhdGlvbikuaHRtbChcclxuICAgICAgICB0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvblxyXG4gICAgICApO1xyXG5cclxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgY3JlZGl0IHNjb3JlXHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZWxlY3RlZFNjb3JlKS5odG1sKFxyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIHNlbGVjdGVkIHBheW1lbnQgZnJlcXVlbmN5XHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZWxlY3RlZFBheW1lbnRGcmVxdWVuY3kpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5XHJcbiAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzcGxheSB0aGUgcmVzdWx0cyBmb3IgdGhlIGN1cnJlbnQgdmFsdWVzLlxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgX2Rpc3BsYXlSZXN1bHRzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIGxvYW4gdG90YWxcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmxvYW5Ub3RhbFNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fbG9hblRvdGFsKCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBsb2FuIHBlcmlvZGljIHBheW1lbnRcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnBheW1lbnRTZWxlY3RvcikuaHRtbChcclxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX1BNVCgpKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgLy8gRGlzcGxheSB0aGUgaW50ZXJlc3QgdG90YWwgYW1vdW50XHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5pbnRlcmVzdFRvdGFsU2VsZWN0b3IpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9pbnRlcmVzdFRvdGFsKCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSB0YXggdG90YWwgYW1vdW50XHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy50YXhUb3RhbFNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fdGF4VG90YWwoKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIGFubnVhbCB0b3RhbCBjb3N0XHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy50b3RhbEFubnVhbENvc3RTZWxlY3RvcikuaHRtbChcclxuICAgICAgICB0aGlzLl90b1BlcmNlbnRhZ2UodGhpcy5fQ0FUKCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZXJ2aWNlIGZlZSBpZiBhbnlcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWVTZWxlY3RvcikuaHRtbChcclxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX3NlcnZpY2VGZWVXaXRoVkFUKCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MubG9hbkdyYW5kVG90YWxTZWxlY3RvcikuaHRtbChcclxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX2dyYW5kVG90YWwoKSlcclxuICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSdW4gdGhlIGluaXQgbWV0aG9kIGFnYWluIHdpdGggdGhlIHByb3ZpZGVkIG9wdGlvbnMuXHJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gYXJnc1xyXG4gICAgICovXHJcbiAgICB1cGRhdGU6IGZ1bmN0aW9uIChhcmdzKSB7XHJcbiAgICAgIHRoaXMuc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSwgdGhpcy5fZGVmYXVsdHMsIHRoaXMuc2V0dGluZ3MsIGFyZ3MpO1xyXG4gICAgICB0aGlzLmluaXQoKTtcclxuICAgICAgdGhpcy4kZWwudHJpZ2dlcignbG9hbjp1cGRhdGUnKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZW5lcmF0ZSB0aGUgcmVzdWx0cyBhcyBhbiBhcnJheSBvZiBvYmplY3RzLFxyXG4gICAgICogZWFjaCBvYmplY3QgY29udGFpbnMgdGhlIHZhbHVlcyBmb3IgZWFjaCBwZXJpb2QuXHJcbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cclxuICAgICAqL1xyXG4gICAgX3Jlc3VsdHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIGJhbGFuY2UgPSB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQ7XHJcbiAgICAgIHZhciBpbml0aWFsID0gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50O1xyXG4gICAgICB2YXIgaW50ZXJlc3RSYXRlID0gdGhpcy5faW50ZXJlc3RSYXRlKCk7XHJcbiAgICAgIHZhciBWQVQgPSB0aGlzLl92YWx1ZUFkZGVkVGF4KCk7XHJcbiAgICAgIHZhciBwYXltZW50ID0gdGhpcy5fUE1UKCk7XHJcbiAgICAgIHZhciBudW1iZXJPZlBheW1lbnRzID0gdGhpcy5fbnVtYmVyT2ZQYXltZW50cygpO1xyXG4gICAgICB2YXIgcmVzdWx0cyA9IFtdO1xyXG5cclxuICAgICAgLy8gV2UgbG9vcCBvdmVyIHRoZSBudW1iZXIgb2YgcGF5bWVudHMgYW5kIGVhY2ggdGltZVxyXG4gICAgICAvLyB3ZSBleHRyYWN0IHRoZSBpbmZvcm1hdGlvbiB0byBidWlsZCB0aGUgcGVyaW9kXHJcbiAgICAgIC8vIHRoYXQgd2lsbCBiZSBhcHBlbmRlZCB0byB0aGUgcmVzdWx0cyBhcnJheS5cclxuICAgICAgZm9yICh2YXIgcGF5bWVudE51bWJlciA9IDA7IHBheW1lbnROdW1iZXIgPCBudW1iZXJPZlBheW1lbnRzOyBwYXltZW50TnVtYmVyKyspIHtcclxuICAgICAgICB2YXIgaW50ZXJlc3QgPSBiYWxhbmNlICogaW50ZXJlc3RSYXRlO1xyXG4gICAgICAgIHZhciB0YXhlc1BhaWQgPSBiYWxhbmNlICogaW50ZXJlc3RSYXRlICogVkFUO1xyXG4gICAgICAgIHZhciBwcmluY2lwYWwgPSBwYXltZW50IC0gaW50ZXJlc3QgLSB0YXhlc1BhaWQ7XHJcblxyXG4gICAgICAgIC8vIHVwZGF0ZSBpbml0aWFsIGJhbGFuY2UgZm9yIG5leHQgaXRlcmF0aW9uXHJcbiAgICAgICAgaW5pdGlhbCA9IGJhbGFuY2U7XHJcblxyXG4gICAgICAgIC8vIHVwZGF0ZSBmaW5hbCBiYWxhbmNlIGZvciB0aGUgbmV4dCBpdGVyYXRpb24uXHJcbiAgICAgICAgYmFsYW5jZSA9IGJhbGFuY2UgLSBwcmluY2lwYWw7XHJcblxyXG4gICAgICAgIHJlc3VsdHMucHVzaCh7XHJcbiAgICAgICAgICBpbml0aWFsOiBpbml0aWFsLFxyXG4gICAgICAgICAgcHJpbmNpcGFsOiBwcmluY2lwYWwsXHJcbiAgICAgICAgICBpbnRlcmVzdDogaW50ZXJlc3QsXHJcbiAgICAgICAgICB0YXg6IHRheGVzUGFpZCxcclxuICAgICAgICAgIHBheW1lbnQ6IHBheW1lbnQsXHJcbiAgICAgICAgICBiYWxhbmNlOiBiYWxhbmNlXHJcbiAgICAgICAgfSlcclxuICAgICAgfTtcclxuXHJcbiAgICAgIHJldHVybiByZXN1bHRzO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyYXRlIHRoZSBhbW9ydGl6YXRpb24gc2NoZWR1bGUuXHJcbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cclxuICAgICAqL1xyXG4gICAgc2NoZWR1bGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuICQubWFwKHRoaXMuX3Jlc3VsdHMoKSwgZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgIGluaXRpYWw6IHRoaXMuX3RvTW9uZXkodmFsdWUuaW5pdGlhbCksXHJcbiAgICAgICAgICBwcmluY2lwYWw6IHRoaXMuX3RvTW9uZXkodmFsdWUucHJpbmNpcGFsKSxcclxuICAgICAgICAgIGludGVyZXN0OiB0aGlzLl90b01vbmV5KHZhbHVlLmludGVyZXN0KSxcclxuICAgICAgICAgIHRheDogdGhpcy5fdG9Nb25leSh2YWx1ZS50YXgpLFxyXG4gICAgICAgICAgcGF5bWVudDogdGhpcy5fdG9Nb25leSh2YWx1ZS5wYXltZW50KSxcclxuICAgICAgICAgIGJhbGFuY2U6IHRoaXMuX3RvTW9uZXkodmFsdWUuYmFsYW5jZSlcclxuICAgICAgICB9XHJcbiAgICAgIH0uYmluZCh0aGlzKSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSBjcmVkaXQgcmF0ZXMgYmVpbmcgdXNlZC5cclxuICAgICAqIEByZXR1cm4ge09iamVjdH1cclxuICAgICAqL1xyXG4gICAgY3JlZGl0UmF0ZXM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXM7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBjcmVkaXQgcmF0ZSBjb3JyZXNwb25kaW5nIHRvIHRoZSBjdXJyZW50IGNyZWRpdCBzY29yZS5cclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX2FubnVhbEludGVyZXN0UmF0ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy5oYXNPd25Qcm9wZXJ0eSgnaW50ZXJlc3RSYXRlJykpIHtcclxuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5pbnRlcmVzdFJhdGUgPD0gMSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuaW50ZXJlc3RSYXRlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmludGVyZXN0UmF0ZSkgLyAxMDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW3RoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmVdIC8gMTAwO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0aGUgcGVyaW9kaWMgaW50ZXJlc3QgcmF0ZS5cclxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9pbnRlcmVzdFJhdGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuX2FubnVhbEludGVyZXN0UmF0ZSgpIC8gdGhpcy5fcGF5bWVudEZyZXF1ZW5jeSgpO1xyXG4gICAgfSxcclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIHRoZSBwZXJpb2RpYyBwYXltZW50IGZyZXF1ZW5jeVxyXG4gICAgICogQHJldHVybnMge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3BheW1lbnRGcmVxdWVuY3k6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIFBBWU1FTlRfRlJFUVVFTkNJRVNbdGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5XTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIG51bWJlciBvZiBwYXltZW50cyBmb3IgdGhlIGxvYW4uXHJcbiAgICAgKiBAcmV0dXJucyB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfbnVtYmVyT2ZQYXltZW50czogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgZHVyYXRpb25JblllYXJzID0gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uKSAvIDEyO1xyXG5cclxuICAgICAgcmV0dXJuIE1hdGguZmxvb3IoZHVyYXRpb25JblllYXJzICogUEFZTUVOVF9GUkVRVUVOQ0lFU1t0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3ldKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxjdWxhdGVzIHRoZSB0b3RhbCBjb3N0IG9mIHRoZSBsb2FuLlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfbG9hblRvdGFsOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9QTVQoKSAqIHRoaXMuX251bWJlck9mUGF5bWVudHMoKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxjdWxhdGUgdGhlIG1vbnRobHkgYW1vcnRpemVkIGxvYW4gcGF5bWVudHMuXHJcbiAgICAgKiBAc2VlIGh0dHBzOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0NvbXBvdW5kX2ludGVyZXN0I01vbnRobHlfYW1vcnRpemVkX2xvYW5fb3JfbW9ydGdhZ2VfcGF5bWVudHNcclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX1BNVDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgaSA9IHRoaXMuX2ludGVyZXN0UmF0ZSgpO1xyXG4gICAgICB2YXIgTCA9IHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudDtcclxuICAgICAgdmFyIG4gPSB0aGlzLl9udW1iZXJPZlBheW1lbnRzKCk7XHJcblxyXG4gICAgICBpZiAodGhpcy5zZXR0aW5ncy52YWx1ZUFkZGVkVGF4ICE9PSAwKSB7XHJcbiAgICAgICAgaSA9ICgxICsgdGhpcy5fdmFsdWVBZGRlZFRheCgpKSAqIGk7IC8vIGludGVyZXN0IHJhdGUgd2l0aCB0YXhcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIChMICogaSkgLyAoMSAtIE1hdGgucG93KDEgKyBpLCAtbikpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZSB0aGUgdG90YWwgaW50ZXJlc3QgZm9yIHRoZSBsb2FuLlxyXG4gICAgICogQHJldHVybnMge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX2ludGVyZXN0VG90YWw6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIHRvdGFsID0gMDtcclxuICAgICAgJC5lYWNoKHRoaXMuX3Jlc3VsdHMoKSwgZnVuY3Rpb24gKGluZGV4LCB2YWx1ZSkge1xyXG4gICAgICAgIHRvdGFsICs9IHZhbHVlLmludGVyZXN0O1xyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIHRvdGFsO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZSB0aGUgdmFsdWUgYWRkZWQgdGF4IHRvdGFsIGZvciB0aGUgbG9hbi5cclxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF90YXhUb3RhbDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgdG90YWwgPSAwO1xyXG4gICAgICAkLmVhY2godGhpcy5fcmVzdWx0cygpLCBmdW5jdGlvbiAoaW5kZXgsIHZhbHVlKSB7XHJcbiAgICAgICAgdG90YWwgKz0gdmFsdWUudGF4O1xyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIHRvdGFsO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgbG9hbiBmZWVzIGFuZCBjb21taXNzaW9ucyB0b3RhbC5cclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3NlcnZpY2VGZWU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIHNlcnZpY2VGZWUgPSB0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWU7XHJcblxyXG4gICAgICAvLyBpZiB0aGUgc2VydmljZSBmZWUgaXMgZ3JlYXRlciB0aGFuIDEgdGhlbiB0aGVcclxuICAgICAgLy8gdmFsdWUgbXVzdCBiZSBjb252ZXJ0ZWQgdG8gZGVjaW1hbHMgZmlyc3QuXHJcbiAgICAgIGlmIChzZXJ2aWNlRmVlID4gMSkge1xyXG4gICAgICAgIHNlcnZpY2VGZWUgPSBzZXJ2aWNlRmVlIC8gMTAwO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50ICogc2VydmljZUZlZTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIGxvYW4gZmVlcyBhbmQgY29tbWlzc2lvbnMgdG90YWwgd2l0aCBWQVQgaW5jbHVkZWQuXHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9zZXJ2aWNlRmVlV2l0aFZBVDogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fc2VydmljZUZlZSgpICogKHRoaXMuX3ZhbHVlQWRkZWRUYXgoKSArIDEpXHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsY3VsYXRlcyB0aGUgdG90YWwgY29zdCBvZiB0aGUgbG9hbiBpbmNsdWRpbmcgdGhlIHNlcnZpY2UgZmVlLlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfZ3JhbmRUb3RhbDogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fbG9hblRvdGFsKCkgKyB0aGlzLl9zZXJ2aWNlRmVlV2l0aFZBVCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgdG90YWwgYW5udWFsIGNvc3QgKENBVClcclxuICAgICAqIEBzZWUgaHR0cDovL3d3dy5iYW54aWNvLm9yZy5teC9DQVRcclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX0NBVDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgSVJSID0gdGhpcy5fSVJSKHRoaXMuX2Nhc2hGbG93KCkpO1xyXG4gICAgICB2YXIgcGVyaW9kcyA9IHRoaXMuX3BheW1lbnRGcmVxdWVuY3koKTtcclxuXHJcbiAgICAgIHJldHVybiBNYXRoLnBvdygxICsgSVJSLCBwZXJpb2RzKSAtIDE7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhbiBhcnJheSB3aXRoIGEgc2VyaWVzIG9mIGNhc2ggZmxvd3MgZm9yIHRoZSBjdXJyZW50IGxvYW4uXHJcbiAgICAgKiBAcmV0dXJuIHtBcnJheX1cclxuICAgICAqL1xyXG4gICAgX2Nhc2hGbG93OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciByZXN1bHRzID0gdGhpcy5fcmVzdWx0cygpO1xyXG4gICAgICB2YXIgY2FzaEZsb3cgPSBbdGhpcy5fc2VydmljZUZlZSgpIC0gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50XTtcclxuXHJcbiAgICAgICQuZWFjaChyZXN1bHRzLCBmdW5jdGlvbiAoaW5kZXgsIHBlcmlvZCkge1xyXG4gICAgICAgIGNhc2hGbG93LnB1c2gocGVyaW9kLnBheW1lbnQgLSBwZXJpb2QudGF4KTtcclxuICAgICAgfS5iaW5kKHRoaXMpKTtcclxuXHJcbiAgICAgIHJldHVybiBjYXNoRmxvdztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIHRoZSBpbnRlcm5hbCByYXRlIG9mIHJldHVybiBmb3IgYSBzZXJpZXMgb2YgY2FzaCBmbG93cyByZXByZXNlbnRlZCBieSB0aGUgbnVtYmVycyBpbiB2YWx1ZXMuXHJcbiAgICAgKiBAcGFyYW0gIHtBcnJheX0gdmFsdWVzXHJcbiAgICAgKiBAcGFyYW0gIHtOdW1iZXJ9IGd1ZXNzXHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9JUlI6IGZ1bmN0aW9uICh2YWx1ZXMsIGd1ZXNzKSB7XHJcbiAgICAgIGd1ZXNzID0gZ3Vlc3MgfHwgMDtcclxuXHJcbiAgICAgIC8vIENhbGN1bGF0ZXMgdGhlIHJlc3VsdGluZyBhbW91bnRcclxuICAgICAgdmFyIGlyclJlc3VsdCA9IGZ1bmN0aW9uICh2YWx1ZXMsIGRhdGVzLCByYXRlKSB7XHJcbiAgICAgICAgdmFyIHJlc3VsdCA9IHZhbHVlc1swXTtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCB2YWx1ZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgIHJlc3VsdCArPSB2YWx1ZXNbaV0gLyBNYXRoLnBvdyhyYXRlICsgMSwgKGRhdGVzW2ldIC0gZGF0ZXNbMF0pIC8gMzY1KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAgIH07XHJcblxyXG4gICAgICAvLyBDYWxjdWxhdGVzIHRoZSBmaXJzdCBkZXJpdmF0aW9uXHJcbiAgICAgIHZhciBpcnJSZXN1bHREZXJpdmF0aXZlID0gZnVuY3Rpb24gKHZhbHVlcywgZGF0ZXMsIHJhdGUpIHtcclxuICAgICAgICB2YXIgcmVzdWx0ID0gMDtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCB2YWx1ZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgIHZhciBmcmFjID0gKGRhdGVzW2ldIC0gZGF0ZXNbMF0pIC8gMzY1O1xyXG4gICAgICAgICAgcmVzdWx0IC09IGZyYWMgKiB2YWx1ZXNbaV0gLyBNYXRoLnBvdyhyYXRlICsgMSwgZnJhYyArIDEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgfTtcclxuXHJcbiAgICAgIC8vIEluaXRpYWxpemUgZGF0ZXMgYW5kIGNoZWNrIHRoYXQgdmFsdWVzIGNvbnRhaW5zIGF0XHJcbiAgICAgIC8vIGxlYXN0IG9uZSBwb3NpdGl2ZSB2YWx1ZSBhbmQgb25lIG5lZ2F0aXZlIHZhbHVlXHJcbiAgICAgIHZhciBkYXRlcyA9IFtdO1xyXG4gICAgICB2YXIgcG9zaXRpdmUgPSBmYWxzZTtcclxuICAgICAgdmFyIG5lZ2F0aXZlID0gZmFsc2U7XHJcblxyXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGRhdGVzW2ldID0gKGkgPT09IDApID8gMCA6IGRhdGVzW2kgLSAxXSArIDM2NTtcclxuICAgICAgICBpZiAodmFsdWVzW2ldID4gMCkgcG9zaXRpdmUgPSB0cnVlO1xyXG4gICAgICAgIGlmICh2YWx1ZXNbaV0gPCAwKSBuZWdhdGl2ZSA9IHRydWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghcG9zaXRpdmUgfHwgIW5lZ2F0aXZlKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxyXG4gICAgICAgICAgJ0Vycm9yIHRoZSB2YWx1ZXMgZG9lcyBub3QgY29udGFpbiBhdCBsZWFzdCBvbmUgcG9zaXRpdmUgdmFsdWUgYW5kIG9uZSBuZWdhdGl2ZSB2YWx1ZSdcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBJbml0aWFsaXplIGd1ZXNzIGFuZCByZXN1bHRSYXRlXHJcbiAgICAgIGd1ZXNzID0gKGd1ZXNzID09PSB1bmRlZmluZWQpID8gMC4xIDogZ3Vlc3M7XHJcbiAgICAgIHZhciByZXN1bHRSYXRlID0gZ3Vlc3M7XHJcblxyXG4gICAgICAvLyBTZXQgbWF4aW11bSBlcHNpbG9uIGZvciBlbmQgb2YgaXRlcmF0aW9uXHJcbiAgICAgIHZhciBlcHNNYXggPSAxZS0xMDtcclxuXHJcbiAgICAgIC8vIEltcGxlbWVudCBOZXd0b24ncyBtZXRob2RcclxuICAgICAgdmFyIG5ld1JhdGUsIGVwc1JhdGUsIHJlc3VsdFZhbHVlO1xyXG4gICAgICB2YXIgY29udExvb3AgPSB0cnVlO1xyXG5cclxuICAgICAgZG8ge1xyXG4gICAgICAgIHJlc3VsdFZhbHVlID0gaXJyUmVzdWx0KHZhbHVlcywgZGF0ZXMsIHJlc3VsdFJhdGUpO1xyXG4gICAgICAgIG5ld1JhdGUgPSByZXN1bHRSYXRlIC0gcmVzdWx0VmFsdWUgLyBpcnJSZXN1bHREZXJpdmF0aXZlKHZhbHVlcywgZGF0ZXMsIHJlc3VsdFJhdGUpO1xyXG4gICAgICAgIGVwc1JhdGUgPSBNYXRoLmFicyhuZXdSYXRlIC0gcmVzdWx0UmF0ZSk7XHJcbiAgICAgICAgcmVzdWx0UmF0ZSA9IG5ld1JhdGU7XHJcbiAgICAgICAgY29udExvb3AgPSAoZXBzUmF0ZSA+IGVwc01heCkgJiYgKE1hdGguYWJzKHJlc3VsdFZhbHVlKSA+IGVwc01heCk7XHJcbiAgICAgIH0gd2hpbGUgKGNvbnRMb29wKTtcclxuXHJcbiAgICAgIC8vIFJldHVybiBpbnRlcm5hbCByYXRlIG9mIHJldHVyblxyXG4gICAgICByZXR1cm4gcmVzdWx0UmF0ZTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIHZhbHVlIGFkZGVkIHRheCBpbiBkZWNpbWFscy5cclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3ZhbHVlQWRkZWRUYXg6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIHRheCA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLnZhbHVlQWRkZWRUYXggfHwgMCk7XHJcblxyXG4gICAgICAvLyBpZiB0YXggaXMgZ3JlYXRlciB0aGFuIDEgbWVhbnMgdGhlIHZhbHVlXHJcbiAgICAgIC8vIG11c3QgYmUgY29udmVydGVkIHRvIGRlY2ltYWxzIGZpcnN0LlxyXG4gICAgICByZXR1cm4gKHRheCA+IDEpID8gdGF4IC8gMTAwIDogdGF4O1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgbnVtZXJpYyBmb3JtYXQgdG8gbW9uZXkgZm9ybWF0LlxyXG4gICAgICogQHBhcmFtICB7TnVtYmVyfSBudW1lcmljXHJcbiAgICAgKiBAcmV0dXJuIHtTdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIF90b01vbmV5OiBmdW5jdGlvbiAobnVtZXJpYykge1xyXG4gICAgICBpZiAodHlwZW9mIG51bWVyaWMgPT0gJ3N0cmluZycpIHtcclxuICAgICAgICBudW1lcmljID0gcGFyc2VGbG9hdChudW1lcmljKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuICdBRUQgJyArIG51bWVyaWMudG9GaXhlZCgyKS5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pK1xcLikvZywgJyQxLCcpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnZlcnQgZnJvbSBtb25leSBmb3JtYXQgdG8gbnVtZXJpYyBmb3JtYXQuXHJcbiAgICAgKiBAcGFyYW0gIHtTdHJpbmd9IHZhbHVlXHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF90b051bWVyaWM6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICByZXR1cm4gcGFyc2VGbG9hdChcclxuICAgICAgICB2YWx1ZS50b1N0cmluZygpLnJlcGxhY2UoL1teMC05XFwuXSsvZywgJycpXHJcbiAgICAgICk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVG8gY29udmVydCB0aGUgcHJvdmlkZWQgdmFsdWUgdG8gcGVyY2VudCBmb3JtYXQuXHJcbiAgICAgKiBAcGFyYW0ge051bWJlcn0gbnVtZXJpY1xyXG4gICAgICogQHJldHVybnMge1N0cmluZ31cclxuICAgICAqL1xyXG4gICAgX3RvUGVyY2VudGFnZTogZnVuY3Rpb24gKG51bWVyaWMpIHtcclxuICAgICAgcmV0dXJuIChudW1lcmljICogMTAwKS50b0ZpeGVkKDIpICsgJyUnO1xyXG4gICAgfVxyXG5cclxuICB9KTtcclxuXHJcbiAgLyoqXHJcbiAgICogV3JhcHBlciBhcm91bmQgdGhlIGNvbnN0cnVjdG9yIHRvIHByZXZlbnQgbXVsdGlwbGUgaW5zdGFudGlhdGlvbnMuXHJcbiAgICovXHJcbiAgJC5mbi5sb2FuQ2FsY3VsYXRvciA9IGZ1bmN0aW9uIChvcHRpb25zLCBhcmdzKSB7XHJcbiAgICBpZiAob3B0aW9ucyA9PT0gJ3NjaGVkdWxlJykge1xyXG4gICAgICByZXR1cm4gdGhpcy5kYXRhKCdwbHVnaW5fbG9hbkNhbGN1bGF0b3InKS5zY2hlZHVsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChvcHRpb25zID09PSAncmF0ZXMnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRhdGEoJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicpLmNyZWRpdFJhdGVzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBpbnN0YW5jZSA9ICQuZGF0YSh0aGlzLCAncGx1Z2luX2xvYW5DYWxjdWxhdG9yJyk7XHJcbiAgICAgIGlmICghaW5zdGFuY2UpIHtcclxuICAgICAgICAkLmRhdGEodGhpcywgJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicsIG5ldyBQbHVnaW4odGhpcywgb3B0aW9ucykpO1xyXG4gICAgICB9IGVsc2UgaWYgKG9wdGlvbnMgPT09ICd1cGRhdGUnKSB7XHJcbiAgICAgICAgcmV0dXJuIGluc3RhbmNlLnVwZGF0ZShhcmdzKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbn0pKGpRdWVyeSwgd2luZG93LCBkb2N1bWVudCk7Il19
