/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

/*!
 * jQuery Loan Calculator 3.1.3
 *
 * Author: Jorge González <scrub.mx@gmail>
 * Released under the MIT license - https://opensource.org/licenses/MIT
 */
;
(function ($, window, document, undefined) {

  "use strict";

  /**
   * Table of credit rates for every score.
   * @type {Object}
   */

  var CREDIT_RATES = {
    'A': 5.32,
    'B': 8.18,
    'C': 12.29,
    'D': 15.61,
    'E': 18.25,
    'F': 21.99,
    'G': 26.77
  };

  /**
   * Table of allowed payment frequencies.
   * @type {Object}
   */
  var PAYMENT_FREQUENCIES = {
    'weekly': 52,
    'biweekly': 26,
    'monthly': 12
  };

  /**
   * The minimum allowed for a loan.
   * @type {Number}
   */
  var MINIMUM_LOAN = 1000;

  /**
   * The minimum duration in months.
   * @type {Number}
   */
  var MINIMUM_DURATION = 1;

  /**
   * Default options for the plugin.
   * @type {Object}
   */
  var defaults = {
    // default values for a loan
    loanAmount: 50000,
    loanDuration: 12,
    creditRates: CREDIT_RATES,
    creditScore: 'A',
    valueAddedTax: 0,
    serviceFee: 0,
    paymentFrequency: 'monthly',

    // inputs
    loanAmountSelector: '#loan-amount',
    loanDurationSelector: '#loan-duration',
    creditScoreSelector: '#credit-score',
    paymentFrequencySelector: '#payment-frequency',

    // display selected values
    selectedAmount: '#selected-amount',
    selectedDuration: '#selected-duration',
    selectedScore: '#selected-score',
    selectedPaymentFrequency: '#selected-payment-frequency',

    // display the results
    loanTotalSelector: '#loan-total',
    paymentSelector: '#payment',
    interestTotalSelector: '#interest-total',
    serviceFeeSelector: '#service-fee',
    taxTotalSelector: '#tax-total',
    totalAnnualCostSelector: '#total-annual-cost',
    loanGrandTotalSelector: '#grand-total'
  };

  /**
   * The actual plugin constructor
   * @param {Object} element
   * @param {Object} options
   */
  function Plugin(element, options) {
    this.$el = $(element);
    this._name = 'loanCalculator';
    this._defaults = defaults;
    this.settings = $.extend({}, defaults, options);
    this.attachListeners();
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {

    /**
     * Validates the data and shows the results.
     * @return {void}
     */
    init: function () {
      this.validate();
      this.render();
    },

    /**
     * Attach event listeners to the event handlers.
     * @return {void}
     */
    attachListeners: function () {
      var eventEmitters = [this.settings.loanAmountSelector, this.settings.loanDurationSelector, this.settings.creditScoreSelector, this.settings.paymentFrequencySelector];

      $(eventEmitters.join()).on({
        change: this.eventHandler.bind(this),
        mousemove: this.eventHandler.bind(this),
        touchmove: this.eventHandler.bind(this)
      });
    },

    /**
     * Handle events from the DOM.
     * @return {void}
     */
    eventHandler: function () {
      this.update({
        loanAmount: this.$el.find(this.settings.loanAmountSelector).val(),
        loanDuration: this.$el.find(this.settings.loanDurationSelector).val(),
        creditScore: this.$el.find(this.settings.creditScoreSelector).val(),
        paymentFrequency: this.$el.find(this.settings.paymentFrequencySelector).val()
      });
    },

    /**
     * Sanitize and validate the user input data.
     * @throws Error
     * @return {void}
     */
    validate: function () {
      if (typeof this.settings.loanAmount === 'string') {
        this.settings.loanAmount = this._toNumeric(this.settings.loanAmount);
      }

      if (typeof this.settings.serviceFee === 'string') {
        this.settings.serviceFee = this._toNumeric(this.settings.serviceFee);
      }

      if (!$.isPlainObject(this.settings.creditRates)) {
        throw new Error('The value provided for [creditRates] is not valid.');
      }

      for (var creditRate in this.settings.creditRates) {
        if (typeof this.settings.creditRates[creditRate] === 'string') {
          this.settings.creditRates[creditRate] = this._toNumeric(this.settings.creditRates[creditRate]);
        }

        if (!$.isNumeric(this.settings.creditRates[creditRate])) {
          throw new Error('The value provided for [creditRates] is not valid.');
        }

        if (this.settings.creditRates[creditRate] < 1) {
          this.settings.creditRates[creditRate] = this.settings.creditRates[creditRate] * 100;
        }
      }

      // Sanitize the input
      this.settings.loanAmount = parseFloat(this.settings.loanAmount);
      this.settings.loanDuration = parseFloat(this.settings.loanDuration);
      this.settings.serviceFee = parseFloat(this.settings.serviceFee);
      this.settings.creditScore = $.trim(this.settings.creditScore.toUpperCase());

      if (!PAYMENT_FREQUENCIES.hasOwnProperty(this.settings.paymentFrequency)) {
        throw new Error('The value provided for [paymentFrequency] is not valid.');
      }

      if (!this.settings.creditRates.hasOwnProperty(this.settings.creditScore)) {
        throw new Error('The value provided for [creditScore] is not valid.');
      }

      if (this.settings.loanAmount < MINIMUM_LOAN) {
        throw new Error('The value provided for [loanAmount] must me at least 1000.');
      }

      if (this.settings.loanDuration < MINIMUM_DURATION) {
        throw new Error('The value provided for [loanDuration] must me at least 1.');
      }

      if (!$.isNumeric(this.settings.serviceFee)) {
        throw new Error('The value provided for [serviceFee] is not valid.');
      }
    },

    /**
     * Show the results in the DOM.
     * @return {void}
     */
    render: function () {
      this._displaySelectedValues();
      this._displayResults();
    },

    /**
     * Show the selected values in the DOM.
     * @return {void}
     */
    _displaySelectedValues: function () {
      // Display the selected loan amount
      this.$el.find(this.settings.selectedAmount).html(this._toMoney(this.settings.loanAmount));

      // Display the selected loan duration
      this.$el.find(this.settings.selectedDuration).html(this.settings.loanDuration);

      // Display the selected credit score
      this.$el.find(this.settings.selectedScore).html(this.settings.creditScore);

      // Display the selected payment frequency
      this.$el.find(this.settings.selectedPaymentFrequency).html(this.settings.paymentFrequency);
    },

    /**
     * Display the results for the current values.
     * @return {void}
     */
    _displayResults: function () {
      // Display the loan total
      this.$el.find(this.settings.loanTotalSelector).html(this._toMoney(this._loanTotal()));

      // Display the loan periodic payment
      this.$el.find(this.settings.paymentSelector).html(this._toMoney(this._PMT()));

      // Display the interest total amount
      this.$el.find(this.settings.interestTotalSelector).html(this._toMoney(this._interestTotal()));

      // Display the tax total amount
      this.$el.find(this.settings.taxTotalSelector).html(this._toMoney(this._taxTotal()));

      // Display the annual total cost
      this.$el.find(this.settings.totalAnnualCostSelector).html(this._toPercentage(this._CAT()));

      // Display the service fee if any
      this.$el.find(this.settings.serviceFeeSelector).html(this._toMoney(this._serviceFeeWithVAT()));

      this.$el.find(this.settings.loanGrandTotalSelector).html(this._toMoney(this._grandTotal()));
    },

    /**
     * Run the init method again with the provided options.
     * @param {Object} args
     */
    update: function (args) {
      this.settings = $.extend({}, this._defaults, this.settings, args);
      this.init();
      this.$el.trigger('loan:update');
    },

    /**
     * Generate the results as an array of objects,
     * each object contains the values for each period.
     * @return {Array}
     */
    _results: function () {
      var balance = this.settings.loanAmount;
      var initial = this.settings.loanAmount;
      var interestRate = this._interestRate();
      var VAT = this._valueAddedTax();
      var payment = this._PMT();
      var numberOfPayments = this._numberOfPayments();
      var results = [];

      // We loop over the number of payments and each time
      // we extract the information to build the period
      // that will be appended to the results array.
      for (var paymentNumber = 0; paymentNumber < numberOfPayments; paymentNumber++) {
        var interest = balance * interestRate;
        var taxesPaid = balance * interestRate * VAT;
        var principal = payment - interest - taxesPaid;

        // update initial balance for next iteration
        initial = balance;

        // update final balance for the next iteration.
        balance = balance - principal;

        results.push({
          initial: initial,
          principal: principal,
          interest: interest,
          tax: taxesPaid,
          payment: payment,
          balance: balance
        });
      };

      return results;
    },

    /**
     * Generate the amortization schedule.
     * @return {Array}
     */
    schedule: function () {
      return $.map(this._results(), function (value) {
        return {
          initial: this._toMoney(value.initial),
          principal: this._toMoney(value.principal),
          interest: this._toMoney(value.interest),
          tax: this._toMoney(value.tax),
          payment: this._toMoney(value.payment),
          balance: this._toMoney(value.balance)
        };
      }.bind(this));
    },

    /**
     * Return the credit rates being used.
     * @return {Object}
     */
    creditRates: function () {
      return this.settings.creditRates;
    },

    /**
     * Get the credit rate corresponding to the current credit score.
     * @return {Number}
     */
    _annualInterestRate: function () {
      if (this.settings.hasOwnProperty('interestRate')) {
        if (this.settings.interestRate <= 1) {
          return this.settings.interestRate;
        }

        return this._toNumeric(this.settings.interestRate) / 100;
      }

      return this.settings.creditRates[this.settings.creditScore] / 100;
    },

    /**
     * Get the periodic interest rate.
     * @returns {Number}
     */
    _interestRate: function () {
      return this._annualInterestRate() / this._paymentFrequency();
    },

    /**
     * Returns the periodic payment frequency
     * @returns {Number}
     */
    _paymentFrequency: function () {
      return PAYMENT_FREQUENCIES[this.settings.paymentFrequency];
    },

    /**
     * Returns number of payments for the loan.
     * @returns {Number}
     */
    _numberOfPayments: function () {
      var durationInYears = this._toNumeric(this.settings.loanDuration) / 12;

      return Math.floor(durationInYears * PAYMENT_FREQUENCIES[this.settings.paymentFrequency]);
    },

    /**
     * Calculates the total cost of the loan.
     * @return {Number}
     */
    _loanTotal: function () {
      return this._PMT() * this._numberOfPayments();
    },

    /**
     * Calculate the monthly amortized loan payments.
     * @see https://en.wikipedia.org/wiki/Compound_interest#Monthly_amortized_loan_or_mortgage_payments
     * @return {Number}
     */
    _PMT: function () {
      var i = this._interestRate();
      var L = this.settings.loanAmount;
      var n = this._numberOfPayments();

      if (this.settings.valueAddedTax !== 0) {
        i = (1 + this._valueAddedTax()) * i; // interest rate with tax
      }

      return L * i / (1 - Math.pow(1 + i, -n));
    },

    /**
     * Calculate the total interest for the loan.
     * @returns {Number}
     */
    _interestTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.interest;
      }));
      return total;
    },

    /**
     * Calculate the value added tax total for the loan.
     * @returns {Number}
     */
    _taxTotal: function () {
      var total = 0;
      $.each(this._results(), (function (index, value) {
        total += value.tax;
      }));
      return total;
    },

    /**
     * Return the loan fees and commissions total.
     * @return {Number}
     */
    _serviceFee: function () {
      var serviceFee = this.settings.serviceFee;

      // if the service fee is greater than 1 then the
      // value must be converted to decimals first.
      if (serviceFee > 1) {
        serviceFee = serviceFee / 100;
      }

      return this.settings.loanAmount * serviceFee;
    },

    /**
     * Return the loan fees and commissions total with VAT included.
     * @return {Number}
     */
    _serviceFeeWithVAT: function () {
      return this._serviceFee() * (this._valueAddedTax() + 1);
    },

    /**
     * Calculates the total cost of the loan including the service fee.
     * @return {Number}
     */
    _grandTotal: function () {
      return this._loanTotal() + this._serviceFeeWithVAT();
    },

    /**
     * Return the total annual cost (CAT)
     * @see http://www.banxico.org.mx/CAT
     * @return {Number}
     */
    _CAT: function () {
      var IRR = this._IRR(this._cashFlow());
      var periods = this._paymentFrequency();

      return Math.pow(1 + IRR, periods) - 1;
    },

    /**
     * Returns an array with a series of cash flows for the current loan.
     * @return {Array}
     */
    _cashFlow: function () {
      var results = this._results();
      var cashFlow = [this._serviceFee() - this.settings.loanAmount];

      $.each(results, function (index, period) {
        cashFlow.push(period.payment - period.tax);
      }.bind(this));

      return cashFlow;
    },

    /**
     * Returns the internal rate of return for a series of cash flows represented by the numbers in values.
     * @param  {Array} values
     * @param  {Number} guess
     * @return {Number}
     */
    _IRR: function (values, guess) {
      guess = guess || 0;

      // Calculates the resulting amount
      var irrResult = function (values, dates, rate) {
        var result = values[0];

        for (var i = 1; i < values.length; i++) {
          result += values[i] / Math.pow(rate + 1, (dates[i] - dates[0]) / 365);
        }

        return result;
      };

      // Calculates the first derivation
      var irrResultDerivative = function (values, dates, rate) {
        var result = 0;

        for (var i = 1; i < values.length; i++) {
          var frac = (dates[i] - dates[0]) / 365;
          result -= frac * values[i] / Math.pow(rate + 1, frac + 1);
        }

        return result;
      };

      // Initialize dates and check that values contains at
      // least one positive value and one negative value
      var dates = [];
      var positive = false;
      var negative = false;

      for (var i = 0; i < values.length; i++) {
        dates[i] = i === 0 ? 0 : dates[i - 1] + 365;
        if (values[i] > 0) positive = true;
        if (values[i] < 0) negative = true;
      }

      if (!positive || !negative) {
        throw new Error('Error the values does not contain at least one positive value and one negative value');
      }

      // Initialize guess and resultRate
      guess = guess === undefined ? 0.1 : guess;
      var resultRate = guess;

      // Set maximum epsilon for end of iteration
      var epsMax = 1e-10;

      // Implement Newton's method
      var newRate, epsRate, resultValue;
      var contLoop = true;

      do {
        resultValue = irrResult(values, dates, resultRate);
        newRate = resultRate - resultValue / irrResultDerivative(values, dates, resultRate);
        epsRate = Math.abs(newRate - resultRate);
        resultRate = newRate;
        contLoop = epsRate > epsMax && Math.abs(resultValue) > epsMax;
      } while (contLoop);

      // Return internal rate of return
      return resultRate;
    },

    /**
     * Return the value added tax in decimals.
     * @return {Number}
     */
    _valueAddedTax: function () {
      var tax = this._toNumeric(this.settings.valueAddedTax || 0);

      // if tax is greater than 1 means the value
      // must be converted to decimals first.
      return tax > 1 ? tax / 100 : tax;
    },

    /**
     * Convert numeric format to money format.
     * @param  {Number} numeric
     * @return {String}
     */
    _toMoney: function (numeric) {
      if (typeof numeric == 'string') {
        numeric = parseFloat(numeric);
      }

      return 'AED ' + numeric.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    },

    /**
     * Convert from money format to numeric format.
     * @param  {String} value
     * @return {Number}
     */
    _toNumeric: function (value) {
      return parseFloat(value.toString().replace(/[^0-9\.]+/g, ''));
    },

    /**
     * To convert the provided value to percent format.
     * @param {Number} numeric
     * @returns {String}
     */
    _toPercentage: function (numeric) {
      return (numeric * 100).toFixed(2) + '%';
    }

  });

  /**
   * Wrapper around the constructor to prevent multiple instantiations.
   */
  $.fn.loanCalculator = function (options, args) {
    if (options === 'schedule') {
      return this.data('plugin_loanCalculator').schedule();
    }

    if (options === 'rates') {
      return this.data('plugin_loanCalculator').creditRates();
    }

    return this.each((function () {
      var instance = $.data(this, 'plugin_loanCalculator');
      if (!instance) {
        $.data(this, 'plugin_loanCalculator', new Plugin(this, options));
      } else if (options === 'update') {
        return instance.update(args);
      }
    }));
  };
})(jQuery, window, document);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5sb2FuLWNhbGN1bGF0b3IuanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsImRvY3VtZW50IiwidW5kZWZpbmVkIiwiQ1JFRElUX1JBVEVTIiwiUEFZTUVOVF9GUkVRVUVOQ0lFUyIsIk1JTklNVU1fTE9BTiIsIk1JTklNVU1fRFVSQVRJT04iLCJkZWZhdWx0cyIsImxvYW5BbW91bnQiLCJsb2FuRHVyYXRpb24iLCJjcmVkaXRSYXRlcyIsImNyZWRpdFNjb3JlIiwidmFsdWVBZGRlZFRheCIsInNlcnZpY2VGZWUiLCJwYXltZW50RnJlcXVlbmN5IiwibG9hbkFtb3VudFNlbGVjdG9yIiwibG9hbkR1cmF0aW9uU2VsZWN0b3IiLCJjcmVkaXRTY29yZVNlbGVjdG9yIiwicGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yIiwic2VsZWN0ZWRBbW91bnQiLCJzZWxlY3RlZER1cmF0aW9uIiwic2VsZWN0ZWRTY29yZSIsInNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSIsImxvYW5Ub3RhbFNlbGVjdG9yIiwicGF5bWVudFNlbGVjdG9yIiwiaW50ZXJlc3RUb3RhbFNlbGVjdG9yIiwic2VydmljZUZlZVNlbGVjdG9yIiwidGF4VG90YWxTZWxlY3RvciIsInRvdGFsQW5udWFsQ29zdFNlbGVjdG9yIiwibG9hbkdyYW5kVG90YWxTZWxlY3RvciIsIlBsdWdpbiIsImVsZW1lbnQiLCJvcHRpb25zIiwiJGVsIiwiX25hbWUiLCJfZGVmYXVsdHMiLCJzZXR0aW5ncyIsImV4dGVuZCIsImF0dGFjaExpc3RlbmVycyIsImluaXQiLCJwcm90b3R5cGUiLCJ2YWxpZGF0ZSIsInJlbmRlciIsImV2ZW50RW1pdHRlcnMiLCJqb2luIiwib24iLCJjaGFuZ2UiLCJldmVudEhhbmRsZXIiLCJiaW5kIiwibW91c2Vtb3ZlIiwidG91Y2htb3ZlIiwidXBkYXRlIiwiZmluZCIsInZhbCIsIl90b051bWVyaWMiLCJpc1BsYWluT2JqZWN0IiwiRXJyb3IiLCJjcmVkaXRSYXRlIiwiaXNOdW1lcmljIiwicGFyc2VGbG9hdCIsInRyaW0iLCJ0b1VwcGVyQ2FzZSIsImhhc093blByb3BlcnR5IiwiX2Rpc3BsYXlTZWxlY3RlZFZhbHVlcyIsIl9kaXNwbGF5UmVzdWx0cyIsImh0bWwiLCJfdG9Nb25leSIsIl9sb2FuVG90YWwiLCJfUE1UIiwiX2ludGVyZXN0VG90YWwiLCJfdGF4VG90YWwiLCJfdG9QZXJjZW50YWdlIiwiX0NBVCIsIl9zZXJ2aWNlRmVlV2l0aFZBVCIsIl9ncmFuZFRvdGFsIiwiYXJncyIsInRyaWdnZXIiLCJfcmVzdWx0cyIsImJhbGFuY2UiLCJpbml0aWFsIiwiaW50ZXJlc3RSYXRlIiwiX2ludGVyZXN0UmF0ZSIsIlZBVCIsIl92YWx1ZUFkZGVkVGF4IiwicGF5bWVudCIsIm51bWJlck9mUGF5bWVudHMiLCJfbnVtYmVyT2ZQYXltZW50cyIsInJlc3VsdHMiLCJwYXltZW50TnVtYmVyIiwiaW50ZXJlc3QiLCJ0YXhlc1BhaWQiLCJwcmluY2lwYWwiLCJwdXNoIiwidGF4Iiwic2NoZWR1bGUiLCJtYXAiLCJ2YWx1ZSIsIl9hbm51YWxJbnRlcmVzdFJhdGUiLCJfcGF5bWVudEZyZXF1ZW5jeSIsImR1cmF0aW9uSW5ZZWFycyIsIk1hdGgiLCJmbG9vciIsImkiLCJMIiwibiIsInBvdyIsInRvdGFsIiwiZWFjaCIsImluZGV4IiwiX3NlcnZpY2VGZWUiLCJJUlIiLCJfSVJSIiwiX2Nhc2hGbG93IiwicGVyaW9kcyIsImNhc2hGbG93IiwicGVyaW9kIiwidmFsdWVzIiwiZ3Vlc3MiLCJpcnJSZXN1bHQiLCJkYXRlcyIsInJhdGUiLCJyZXN1bHQiLCJsZW5ndGgiLCJpcnJSZXN1bHREZXJpdmF0aXZlIiwiZnJhYyIsInBvc2l0aXZlIiwibmVnYXRpdmUiLCJyZXN1bHRSYXRlIiwiZXBzTWF4IiwibmV3UmF0ZSIsImVwc1JhdGUiLCJyZXN1bHRWYWx1ZSIsImNvbnRMb29wIiwiYWJzIiwibnVtZXJpYyIsInRvRml4ZWQiLCJyZXBsYWNlIiwidG9TdHJpbmciLCJmbiIsImxvYW5DYWxjdWxhdG9yIiwiZGF0YSIsImluc3RhbmNlIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOzs7Ozs7QUFNQTtBQUNBLENBQUEsVUFBQUEsQ0FBQSxFQUFBQyxNQUFBLEVBQUFDLFFBQUEsRUFBQUMsU0FBQSxFQUFBOztBQUVBOztBQUVBOzs7OztBQUlBLE1BQUFDLGVBQUE7QUFDQSxTQUFBLElBREE7QUFFQSxTQUFBLElBRkE7QUFHQSxTQUFBLEtBSEE7QUFJQSxTQUFBLEtBSkE7QUFLQSxTQUFBLEtBTEE7QUFNQSxTQUFBLEtBTkE7QUFPQSxTQUFBO0FBUEEsR0FBQTs7QUFVQTs7OztBQUlBLE1BQUFDLHNCQUFBO0FBQ0EsY0FBQSxFQURBO0FBRUEsZ0JBQUEsRUFGQTtBQUdBLGVBQUE7QUFIQSxHQUFBOztBQU1BOzs7O0FBSUEsTUFBQUMsZUFBQSxJQUFBOztBQUVBOzs7O0FBSUEsTUFBQUMsbUJBQUEsQ0FBQTs7QUFFQTs7OztBQUlBLE1BQUFDLFdBQUE7QUFDQTtBQUNBQyxnQkFBQSxLQUZBO0FBR0FDLGtCQUFBLEVBSEE7QUFJQUMsaUJBQUFQLFlBSkE7QUFLQVEsaUJBQUEsR0FMQTtBQU1BQyxtQkFBQSxDQU5BO0FBT0FDLGdCQUFBLENBUEE7QUFRQUMsc0JBQUEsU0FSQTs7QUFVQTtBQUNBQyx3QkFBQSxjQVhBO0FBWUFDLDBCQUFBLGdCQVpBO0FBYUFDLHlCQUFBLGVBYkE7QUFjQUMsOEJBQUEsb0JBZEE7O0FBZ0JBO0FBQ0FDLG9CQUFBLGtCQWpCQTtBQWtCQUMsc0JBQUEsb0JBbEJBO0FBbUJBQyxtQkFBQSxpQkFuQkE7QUFvQkFDLDhCQUFBLDZCQXBCQTs7QUFzQkE7QUFDQUMsdUJBQUEsYUF2QkE7QUF3QkFDLHFCQUFBLFVBeEJBO0FBeUJBQywyQkFBQSxpQkF6QkE7QUEwQkFDLHdCQUFBLGNBMUJBO0FBMkJBQyxzQkFBQSxZQTNCQTtBQTRCQUMsNkJBQUEsb0JBNUJBO0FBNkJBQyw0QkFBQTtBQTdCQSxHQUFBOztBQWdDQTs7Ozs7QUFLQSxXQUFBQyxNQUFBLENBQUFDLE9BQUEsRUFBQUMsT0FBQSxFQUFBO0FBQ0EsU0FBQUMsR0FBQSxHQUFBbEMsRUFBQWdDLE9BQUEsQ0FBQTtBQUNBLFNBQUFHLEtBQUEsR0FBQSxnQkFBQTtBQUNBLFNBQUFDLFNBQUEsR0FBQTVCLFFBQUE7QUFDQSxTQUFBNkIsUUFBQSxHQUFBckMsRUFBQXNDLE1BQUEsQ0FBQSxFQUFBLEVBQUE5QixRQUFBLEVBQUF5QixPQUFBLENBQUE7QUFDQSxTQUFBTSxlQUFBO0FBQ0EsU0FBQUMsSUFBQTtBQUNBOztBQUVBO0FBQ0F4QyxJQUFBc0MsTUFBQSxDQUFBUCxPQUFBVSxTQUFBLEVBQUE7O0FBRUE7Ozs7QUFJQUQsVUFBQSxZQUFBO0FBQ0EsV0FBQUUsUUFBQTtBQUNBLFdBQUFDLE1BQUE7QUFDQSxLQVRBOztBQVdBOzs7O0FBSUFKLHFCQUFBLFlBQUE7QUFDQSxVQUFBSyxnQkFBQSxDQUNBLEtBQUFQLFFBQUEsQ0FBQXJCLGtCQURBLEVBRUEsS0FBQXFCLFFBQUEsQ0FBQXBCLG9CQUZBLEVBR0EsS0FBQW9CLFFBQUEsQ0FBQW5CLG1CQUhBLEVBSUEsS0FBQW1CLFFBQUEsQ0FBQWxCLHdCQUpBLENBQUE7O0FBT0FuQixRQUFBNEMsY0FBQUMsSUFBQSxFQUFBLEVBQUFDLEVBQUEsQ0FBQTtBQUNBQyxnQkFBQSxLQUFBQyxZQUFBLENBQUFDLElBQUEsQ0FBQSxJQUFBLENBREE7QUFFQUMsbUJBQUEsS0FBQUYsWUFBQSxDQUFBQyxJQUFBLENBQUEsSUFBQSxDQUZBO0FBR0FFLG1CQUFBLEtBQUFILFlBQUEsQ0FBQUMsSUFBQSxDQUFBLElBQUE7QUFIQSxPQUFBO0FBS0EsS0E1QkE7O0FBOEJBOzs7O0FBSUFELGtCQUFBLFlBQUE7QUFDQSxXQUFBSSxNQUFBLENBQUE7QUFDQTNDLG9CQUFBLEtBQUF5QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXJCLGtCQUFBLEVBQUFzQyxHQUFBLEVBREE7QUFFQTVDLHNCQUFBLEtBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQXBCLG9CQUFBLEVBQUFxQyxHQUFBLEVBRkE7QUFHQTFDLHFCQUFBLEtBQUFzQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQW5CLG1CQUFBLEVBQUFvQyxHQUFBLEVBSEE7QUFJQXZDLDBCQUFBLEtBQUFtQixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWxCLHdCQUFBLEVBQUFtQyxHQUFBO0FBSkEsT0FBQTtBQU1BLEtBekNBOztBQTJDQTs7Ozs7QUFLQVosY0FBQSxZQUFBO0FBQ0EsVUFBQSxPQUFBLEtBQUFMLFFBQUEsQ0FBQTVCLFVBQUEsS0FBQSxRQUFBLEVBQUE7QUFDQSxhQUFBNEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBLEtBQUE4QyxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTVCLFVBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsT0FBQSxLQUFBNEIsUUFBQSxDQUFBdkIsVUFBQSxLQUFBLFFBQUEsRUFBQTtBQUNBLGFBQUF1QixRQUFBLENBQUF2QixVQUFBLEdBQUEsS0FBQXlDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBZCxFQUFBd0QsYUFBQSxDQUFBLEtBQUFuQixRQUFBLENBQUExQixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQThDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsV0FBQSxJQUFBQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsRUFBQTtBQUNBLFlBQUEsT0FBQSxLQUFBMEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLEtBQUEsUUFBQSxFQUFBO0FBQ0EsZUFBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxLQUFBSCxVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsQ0FBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxDQUFBMUQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxDQUFBLENBQUEsRUFBQTtBQUNBLGdCQUFBLElBQUFELEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsWUFBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBK0MsVUFBQSxJQUFBLENBQUEsRUFBQTtBQUNBLGVBQUFyQixRQUFBLENBQUExQixXQUFBLENBQUErQyxVQUFBLElBQUEsS0FBQXJCLFFBQUEsQ0FBQTFCLFdBQUEsQ0FBQStDLFVBQUEsSUFBQSxHQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBQUFyQixRQUFBLENBQUE1QixVQUFBLEdBQUFtRCxXQUFBLEtBQUF2QixRQUFBLENBQUE1QixVQUFBLENBQUE7QUFDQSxXQUFBNEIsUUFBQSxDQUFBM0IsWUFBQSxHQUFBa0QsV0FBQSxLQUFBdkIsUUFBQSxDQUFBM0IsWUFBQSxDQUFBO0FBQ0EsV0FBQTJCLFFBQUEsQ0FBQXZCLFVBQUEsR0FBQThDLFdBQUEsS0FBQXZCLFFBQUEsQ0FBQXZCLFVBQUEsQ0FBQTtBQUNBLFdBQUF1QixRQUFBLENBQUF6QixXQUFBLEdBQUFaLEVBQUE2RCxJQUFBLENBQUEsS0FBQXhCLFFBQUEsQ0FBQXpCLFdBQUEsQ0FBQWtELFdBQUEsRUFBQSxDQUFBOztBQUVBLFVBQUEsQ0FBQXpELG9CQUFBMEQsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF0QixnQkFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEwQyxLQUFBLENBQUEseURBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQSxLQUFBcEIsUUFBQSxDQUFBMUIsV0FBQSxDQUFBb0QsY0FBQSxDQUFBLEtBQUExQixRQUFBLENBQUF6QixXQUFBLENBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQTZDLEtBQUEsQ0FBQSxvREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxLQUFBcEIsUUFBQSxDQUFBNUIsVUFBQSxHQUFBSCxZQUFBLEVBQUE7QUFDQSxjQUFBLElBQUFtRCxLQUFBLENBQUEsNERBQUEsQ0FBQTtBQUNBOztBQUVBLFVBQUEsS0FBQXBCLFFBQUEsQ0FBQTNCLFlBQUEsR0FBQUgsZ0JBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQWtELEtBQUEsQ0FBQSwyREFBQSxDQUFBO0FBQ0E7O0FBRUEsVUFBQSxDQUFBekQsRUFBQTJELFNBQUEsQ0FBQSxLQUFBdEIsUUFBQSxDQUFBdkIsVUFBQSxDQUFBLEVBQUE7QUFDQSxjQUFBLElBQUEyQyxLQUFBLENBQUEsbURBQUEsQ0FBQTtBQUNBO0FBQ0EsS0FwR0E7O0FBc0dBOzs7O0FBSUFkLFlBQUEsWUFBQTtBQUNBLFdBQUFxQixzQkFBQTtBQUNBLFdBQUFDLGVBQUE7QUFDQSxLQTdHQTs7QUErR0E7Ozs7QUFJQUQsNEJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQTlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBakIsY0FBQSxFQUFBOEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBOUIsUUFBQSxDQUFBNUIsVUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXlCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBaEIsZ0JBQUEsRUFBQTZDLElBQUEsQ0FDQSxLQUFBN0IsUUFBQSxDQUFBM0IsWUFEQTs7QUFJQTtBQUNBLFdBQUF3QixHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQWYsYUFBQSxFQUFBNEMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF6QixXQURBOztBQUlBO0FBQ0EsV0FBQXNCLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBZCx3QkFBQSxFQUFBMkMsSUFBQSxDQUNBLEtBQUE3QixRQUFBLENBQUF0QixnQkFEQTtBQUdBLEtBdklBOztBQXlJQTs7OztBQUlBa0QscUJBQUEsWUFBQTtBQUNBO0FBQ0EsV0FBQS9CLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBYixpQkFBQSxFQUFBMEMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBQyxVQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFsQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVosZUFBQSxFQUFBeUMsSUFBQSxDQUNBLEtBQUFDLFFBQUEsQ0FBQSxLQUFBRSxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUFuQyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVgscUJBQUEsRUFBQXdDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQUcsY0FBQSxFQUFBLENBREE7O0FBSUE7QUFDQSxXQUFBcEMsR0FBQSxDQUFBbUIsSUFBQSxDQUFBLEtBQUFoQixRQUFBLENBQUFULGdCQUFBLEVBQUFzQyxJQUFBLENBQ0EsS0FBQUMsUUFBQSxDQUFBLEtBQUFJLFNBQUEsRUFBQSxDQURBOztBQUlBO0FBQ0EsV0FBQXJDLEdBQUEsQ0FBQW1CLElBQUEsQ0FBQSxLQUFBaEIsUUFBQSxDQUFBUix1QkFBQSxFQUFBcUMsSUFBQSxDQUNBLEtBQUFNLGFBQUEsQ0FBQSxLQUFBQyxJQUFBLEVBQUEsQ0FEQTs7QUFJQTtBQUNBLFdBQUF2QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVYsa0JBQUEsRUFBQXVDLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQU8sa0JBQUEsRUFBQSxDQURBOztBQUlBLFdBQUF4QyxHQUFBLENBQUFtQixJQUFBLENBQUEsS0FBQWhCLFFBQUEsQ0FBQVAsc0JBQUEsRUFBQW9DLElBQUEsQ0FDQSxLQUFBQyxRQUFBLENBQUEsS0FBQVEsV0FBQSxFQUFBLENBREE7QUFHQSxLQS9LQTs7QUFpTEE7Ozs7QUFJQXZCLFlBQUEsVUFBQXdCLElBQUEsRUFBQTtBQUNBLFdBQUF2QyxRQUFBLEdBQUFyQyxFQUFBc0MsTUFBQSxDQUFBLEVBQUEsRUFBQSxLQUFBRixTQUFBLEVBQUEsS0FBQUMsUUFBQSxFQUFBdUMsSUFBQSxDQUFBO0FBQ0EsV0FBQXBDLElBQUE7QUFDQSxXQUFBTixHQUFBLENBQUEyQyxPQUFBLENBQUEsYUFBQTtBQUNBLEtBekxBOztBQTJMQTs7Ozs7QUFLQUMsY0FBQSxZQUFBO0FBQ0EsVUFBQUMsVUFBQSxLQUFBMUMsUUFBQSxDQUFBNUIsVUFBQTtBQUNBLFVBQUF1RSxVQUFBLEtBQUEzQyxRQUFBLENBQUE1QixVQUFBO0FBQ0EsVUFBQXdFLGVBQUEsS0FBQUMsYUFBQSxFQUFBO0FBQ0EsVUFBQUMsTUFBQSxLQUFBQyxjQUFBLEVBQUE7QUFDQSxVQUFBQyxVQUFBLEtBQUFoQixJQUFBLEVBQUE7QUFDQSxVQUFBaUIsbUJBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLFVBQUFDLFVBQUEsRUFBQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFBLElBQUFDLGdCQUFBLENBQUEsRUFBQUEsZ0JBQUFILGdCQUFBLEVBQUFHLGVBQUEsRUFBQTtBQUNBLFlBQUFDLFdBQUFYLFVBQUFFLFlBQUE7QUFDQSxZQUFBVSxZQUFBWixVQUFBRSxZQUFBLEdBQUFFLEdBQUE7QUFDQSxZQUFBUyxZQUFBUCxVQUFBSyxRQUFBLEdBQUFDLFNBQUE7O0FBRUE7QUFDQVgsa0JBQUFELE9BQUE7O0FBRUE7QUFDQUEsa0JBQUFBLFVBQUFhLFNBQUE7O0FBRUFKLGdCQUFBSyxJQUFBLENBQUE7QUFDQWIsbUJBQUFBLE9BREE7QUFFQVkscUJBQUFBLFNBRkE7QUFHQUYsb0JBQUFBLFFBSEE7QUFJQUksZUFBQUgsU0FKQTtBQUtBTixtQkFBQUEsT0FMQTtBQU1BTixtQkFBQUE7QUFOQSxTQUFBO0FBUUE7O0FBRUEsYUFBQVMsT0FBQTtBQUNBLEtBbE9BOztBQW9PQTs7OztBQUlBTyxjQUFBLFlBQUE7QUFDQSxhQUFBL0YsRUFBQWdHLEdBQUEsQ0FBQSxLQUFBbEIsUUFBQSxFQUFBLEVBQUEsVUFBQW1CLEtBQUEsRUFBQTtBQUNBLGVBQUE7QUFDQWpCLG1CQUFBLEtBQUFiLFFBQUEsQ0FBQThCLE1BQUFqQixPQUFBLENBREE7QUFFQVkscUJBQUEsS0FBQXpCLFFBQUEsQ0FBQThCLE1BQUFMLFNBQUEsQ0FGQTtBQUdBRixvQkFBQSxLQUFBdkIsUUFBQSxDQUFBOEIsTUFBQVAsUUFBQSxDQUhBO0FBSUFJLGVBQUEsS0FBQTNCLFFBQUEsQ0FBQThCLE1BQUFILEdBQUEsQ0FKQTtBQUtBVCxtQkFBQSxLQUFBbEIsUUFBQSxDQUFBOEIsTUFBQVosT0FBQSxDQUxBO0FBTUFOLG1CQUFBLEtBQUFaLFFBQUEsQ0FBQThCLE1BQUFsQixPQUFBO0FBTkEsU0FBQTtBQVFBLE9BVEEsQ0FTQTlCLElBVEEsQ0FTQSxJQVRBLENBQUEsQ0FBQTtBQVVBLEtBblBBOztBQXFQQTs7OztBQUlBdEMsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQTBCLFFBQUEsQ0FBQTFCLFdBQUE7QUFDQSxLQTNQQTs7QUE2UEE7Ozs7QUFJQXVGLHlCQUFBLFlBQUE7QUFDQSxVQUFBLEtBQUE3RCxRQUFBLENBQUEwQixjQUFBLENBQUEsY0FBQSxDQUFBLEVBQUE7QUFDQSxZQUFBLEtBQUExQixRQUFBLENBQUE0QyxZQUFBLElBQUEsQ0FBQSxFQUFBO0FBQ0EsaUJBQUEsS0FBQTVDLFFBQUEsQ0FBQTRDLFlBQUE7QUFDQTs7QUFFQSxlQUFBLEtBQUExQixVQUFBLENBQUEsS0FBQWxCLFFBQUEsQ0FBQTRDLFlBQUEsSUFBQSxHQUFBO0FBQ0E7O0FBRUEsYUFBQSxLQUFBNUMsUUFBQSxDQUFBMUIsV0FBQSxDQUFBLEtBQUEwQixRQUFBLENBQUF6QixXQUFBLElBQUEsR0FBQTtBQUNBLEtBM1FBOztBQTZRQTs7OztBQUlBc0UsbUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQWdCLG1CQUFBLEtBQUEsS0FBQUMsaUJBQUEsRUFBQTtBQUNBLEtBblJBOztBQXNSQTs7OztBQUlBQSx1QkFBQSxZQUFBO0FBQ0EsYUFBQTlGLG9CQUFBLEtBQUFnQyxRQUFBLENBQUF0QixnQkFBQSxDQUFBO0FBQ0EsS0E1UkE7O0FBOFJBOzs7O0FBSUF3RSx1QkFBQSxZQUFBO0FBQ0EsVUFBQWEsa0JBQUEsS0FBQTdDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBM0IsWUFBQSxJQUFBLEVBQUE7O0FBRUEsYUFBQTJGLEtBQUFDLEtBQUEsQ0FBQUYsa0JBQUEvRixvQkFBQSxLQUFBZ0MsUUFBQSxDQUFBdEIsZ0JBQUEsQ0FBQSxDQUFBO0FBQ0EsS0F0U0E7O0FBd1NBOzs7O0FBSUFxRCxnQkFBQSxZQUFBO0FBQ0EsYUFBQSxLQUFBQyxJQUFBLEtBQUEsS0FBQWtCLGlCQUFBLEVBQUE7QUFDQSxLQTlTQTs7QUFnVEE7Ozs7O0FBS0FsQixVQUFBLFlBQUE7QUFDQSxVQUFBa0MsSUFBQSxLQUFBckIsYUFBQSxFQUFBO0FBQ0EsVUFBQXNCLElBQUEsS0FBQW5FLFFBQUEsQ0FBQTVCLFVBQUE7QUFDQSxVQUFBZ0csSUFBQSxLQUFBbEIsaUJBQUEsRUFBQTs7QUFFQSxVQUFBLEtBQUFsRCxRQUFBLENBQUF4QixhQUFBLEtBQUEsQ0FBQSxFQUFBO0FBQ0EwRixZQUFBLENBQUEsSUFBQSxLQUFBbkIsY0FBQSxFQUFBLElBQUFtQixDQUFBLENBREEsQ0FDQTtBQUNBOztBQUVBLGFBQUFDLElBQUFELENBQUEsSUFBQSxJQUFBRixLQUFBSyxHQUFBLENBQUEsSUFBQUgsQ0FBQSxFQUFBLENBQUFFLENBQUEsQ0FBQSxDQUFBO0FBQ0EsS0EvVEE7O0FBaVVBOzs7O0FBSUFuQyxvQkFBQSxZQUFBO0FBQ0EsVUFBQXFDLFFBQUEsQ0FBQTtBQUNBM0csUUFBQTRHLElBQUEsQ0FBQSxLQUFBOUIsUUFBQSxFQUFBLEVBQUEsVUFBQStCLEtBQUEsRUFBQVosS0FBQSxFQUFBO0FBQ0FVLGlCQUFBVixNQUFBUCxRQUFBO0FBQ0EsT0FGQTtBQUdBLGFBQUFpQixLQUFBO0FBQ0EsS0EzVUE7O0FBNlVBOzs7O0FBSUFwQyxlQUFBLFlBQUE7QUFDQSxVQUFBb0MsUUFBQSxDQUFBO0FBQ0EzRyxRQUFBNEcsSUFBQSxDQUFBLEtBQUE5QixRQUFBLEVBQUEsRUFBQSxVQUFBK0IsS0FBQSxFQUFBWixLQUFBLEVBQUE7QUFDQVUsaUJBQUFWLE1BQUFILEdBQUE7QUFDQSxPQUZBO0FBR0EsYUFBQWEsS0FBQTtBQUNBLEtBdlZBOztBQXlWQTs7OztBQUlBRyxpQkFBQSxZQUFBO0FBQ0EsVUFBQWhHLGFBQUEsS0FBQXVCLFFBQUEsQ0FBQXZCLFVBQUE7O0FBRUE7QUFDQTtBQUNBLFVBQUFBLGFBQUEsQ0FBQSxFQUFBO0FBQ0FBLHFCQUFBQSxhQUFBLEdBQUE7QUFDQTs7QUFFQSxhQUFBLEtBQUF1QixRQUFBLENBQUE1QixVQUFBLEdBQUFLLFVBQUE7QUFDQSxLQXZXQTs7QUF5V0E7Ozs7QUFJQTRELHdCQUFBLFlBQUE7QUFDQSxhQUFBLEtBQUFvQyxXQUFBLE1BQUEsS0FBQTFCLGNBQUEsS0FBQSxDQUFBLENBQUE7QUFDQSxLQS9XQTs7QUFpWEE7Ozs7QUFJQVQsaUJBQUEsWUFBQTtBQUNBLGFBQUEsS0FBQVAsVUFBQSxLQUFBLEtBQUFNLGtCQUFBLEVBQUE7QUFDQSxLQXZYQTs7QUF5WEE7Ozs7O0FBS0FELFVBQUEsWUFBQTtBQUNBLFVBQUFzQyxNQUFBLEtBQUFDLElBQUEsQ0FBQSxLQUFBQyxTQUFBLEVBQUEsQ0FBQTtBQUNBLFVBQUFDLFVBQUEsS0FBQWYsaUJBQUEsRUFBQTs7QUFFQSxhQUFBRSxLQUFBSyxHQUFBLENBQUEsSUFBQUssR0FBQSxFQUFBRyxPQUFBLElBQUEsQ0FBQTtBQUNBLEtBbllBOztBQXFZQTs7OztBQUlBRCxlQUFBLFlBQUE7QUFDQSxVQUFBekIsVUFBQSxLQUFBVixRQUFBLEVBQUE7QUFDQSxVQUFBcUMsV0FBQSxDQUFBLEtBQUFMLFdBQUEsS0FBQSxLQUFBekUsUUFBQSxDQUFBNUIsVUFBQSxDQUFBOztBQUVBVCxRQUFBNEcsSUFBQSxDQUFBcEIsT0FBQSxFQUFBLFVBQUFxQixLQUFBLEVBQUFPLE1BQUEsRUFBQTtBQUNBRCxpQkFBQXRCLElBQUEsQ0FBQXVCLE9BQUEvQixPQUFBLEdBQUErQixPQUFBdEIsR0FBQTtBQUNBLE9BRkEsQ0FFQTdDLElBRkEsQ0FFQSxJQUZBLENBQUE7O0FBSUEsYUFBQWtFLFFBQUE7QUFDQSxLQWxaQTs7QUFvWkE7Ozs7OztBQU1BSCxVQUFBLFVBQUFLLE1BQUEsRUFBQUMsS0FBQSxFQUFBO0FBQ0FBLGNBQUFBLFNBQUEsQ0FBQTs7QUFFQTtBQUNBLFVBQUFDLFlBQUEsVUFBQUYsTUFBQSxFQUFBRyxLQUFBLEVBQUFDLElBQUEsRUFBQTtBQUNBLFlBQUFDLFNBQUFMLE9BQUEsQ0FBQSxDQUFBOztBQUVBLGFBQUEsSUFBQWQsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBbUIsb0JBQUFMLE9BQUFkLENBQUEsSUFBQUYsS0FBQUssR0FBQSxDQUFBZSxPQUFBLENBQUEsRUFBQSxDQUFBRCxNQUFBakIsQ0FBQSxJQUFBaUIsTUFBQSxDQUFBLENBQUEsSUFBQSxHQUFBLENBQUE7QUFDQTs7QUFFQSxlQUFBRSxNQUFBO0FBQ0EsT0FSQTs7QUFVQTtBQUNBLFVBQUFFLHNCQUFBLFVBQUFQLE1BQUEsRUFBQUcsS0FBQSxFQUFBQyxJQUFBLEVBQUE7QUFDQSxZQUFBQyxTQUFBLENBQUE7O0FBRUEsYUFBQSxJQUFBbkIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBLGNBQUFzQixPQUFBLENBQUFMLE1BQUFqQixDQUFBLElBQUFpQixNQUFBLENBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQUUsb0JBQUFHLE9BQUFSLE9BQUFkLENBQUEsQ0FBQSxHQUFBRixLQUFBSyxHQUFBLENBQUFlLE9BQUEsQ0FBQSxFQUFBSSxPQUFBLENBQUEsQ0FBQTtBQUNBOztBQUVBLGVBQUFILE1BQUE7QUFDQSxPQVRBOztBQVdBO0FBQ0E7QUFDQSxVQUFBRixRQUFBLEVBQUE7QUFDQSxVQUFBTSxXQUFBLEtBQUE7QUFDQSxVQUFBQyxXQUFBLEtBQUE7O0FBRUEsV0FBQSxJQUFBeEIsSUFBQSxDQUFBLEVBQUFBLElBQUFjLE9BQUFNLE1BQUEsRUFBQXBCLEdBQUEsRUFBQTtBQUNBaUIsY0FBQWpCLENBQUEsSUFBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxHQUFBaUIsTUFBQWpCLElBQUEsQ0FBQSxJQUFBLEdBQUE7QUFDQSxZQUFBYyxPQUFBZCxDQUFBLElBQUEsQ0FBQSxFQUFBdUIsV0FBQSxJQUFBO0FBQ0EsWUFBQVQsT0FBQWQsQ0FBQSxJQUFBLENBQUEsRUFBQXdCLFdBQUEsSUFBQTtBQUNBOztBQUVBLFVBQUEsQ0FBQUQsUUFBQSxJQUFBLENBQUFDLFFBQUEsRUFBQTtBQUNBLGNBQUEsSUFBQXRFLEtBQUEsQ0FDQSxzRkFEQSxDQUFBO0FBR0E7O0FBRUE7QUFDQTZELGNBQUFBLFVBQUFuSCxTQUFBLEdBQUEsR0FBQSxHQUFBbUgsS0FBQTtBQUNBLFVBQUFVLGFBQUFWLEtBQUE7O0FBRUE7QUFDQSxVQUFBVyxTQUFBLEtBQUE7O0FBRUE7QUFDQSxVQUFBQyxPQUFBLEVBQUFDLE9BQUEsRUFBQUMsV0FBQTtBQUNBLFVBQUFDLFdBQUEsSUFBQTs7QUFFQSxTQUFBO0FBQ0FELHNCQUFBYixVQUFBRixNQUFBLEVBQUFHLEtBQUEsRUFBQVEsVUFBQSxDQUFBO0FBQ0FFLGtCQUFBRixhQUFBSSxjQUFBUixvQkFBQVAsTUFBQSxFQUFBRyxLQUFBLEVBQUFRLFVBQUEsQ0FBQTtBQUNBRyxrQkFBQTlCLEtBQUFpQyxHQUFBLENBQUFKLFVBQUFGLFVBQUEsQ0FBQTtBQUNBQSxxQkFBQUUsT0FBQTtBQUNBRyxtQkFBQUYsVUFBQUYsTUFBQSxJQUFBNUIsS0FBQWlDLEdBQUEsQ0FBQUYsV0FBQSxJQUFBSCxNQUFBO0FBQ0EsT0FOQSxRQU1BSSxRQU5BOztBQVFBO0FBQ0EsYUFBQUwsVUFBQTtBQUNBLEtBM2RBOztBQTZkQTs7OztBQUlBNUMsb0JBQUEsWUFBQTtBQUNBLFVBQUFVLE1BQUEsS0FBQXZDLFVBQUEsQ0FBQSxLQUFBbEIsUUFBQSxDQUFBeEIsYUFBQSxJQUFBLENBQUEsQ0FBQTs7QUFFQTtBQUNBO0FBQ0EsYUFBQWlGLE1BQUEsQ0FBQSxHQUFBQSxNQUFBLEdBQUEsR0FBQUEsR0FBQTtBQUNBLEtBdmVBOztBQXllQTs7Ozs7QUFLQTNCLGNBQUEsVUFBQW9FLE9BQUEsRUFBQTtBQUNBLFVBQUEsT0FBQUEsT0FBQSxJQUFBLFFBQUEsRUFBQTtBQUNBQSxrQkFBQTNFLFdBQUEyRSxPQUFBLENBQUE7QUFDQTs7QUFFQSxhQUFBLFNBQUFBLFFBQUFDLE9BQUEsQ0FBQSxDQUFBLEVBQUFDLE9BQUEsQ0FBQSxxQkFBQSxFQUFBLEtBQUEsQ0FBQTtBQUNBLEtBcGZBOztBQXNmQTs7Ozs7QUFLQWxGLGdCQUFBLFVBQUEwQyxLQUFBLEVBQUE7QUFDQSxhQUFBckMsV0FDQXFDLE1BQUF5QyxRQUFBLEdBQUFELE9BQUEsQ0FBQSxZQUFBLEVBQUEsRUFBQSxDQURBLENBQUE7QUFHQSxLQS9mQTs7QUFpZ0JBOzs7OztBQUtBakUsbUJBQUEsVUFBQStELE9BQUEsRUFBQTtBQUNBLGFBQUEsQ0FBQUEsVUFBQSxHQUFBLEVBQUFDLE9BQUEsQ0FBQSxDQUFBLElBQUEsR0FBQTtBQUNBOztBQXhnQkEsR0FBQTs7QUE0Z0JBOzs7QUFHQXhJLElBQUEySSxFQUFBLENBQUFDLGNBQUEsR0FBQSxVQUFBM0csT0FBQSxFQUFBMkMsSUFBQSxFQUFBO0FBQ0EsUUFBQTNDLFlBQUEsVUFBQSxFQUFBO0FBQ0EsYUFBQSxLQUFBNEcsSUFBQSxDQUFBLHVCQUFBLEVBQUE5QyxRQUFBLEVBQUE7QUFDQTs7QUFFQSxRQUFBOUQsWUFBQSxPQUFBLEVBQUE7QUFDQSxhQUFBLEtBQUE0RyxJQUFBLENBQUEsdUJBQUEsRUFBQWxJLFdBQUEsRUFBQTtBQUNBOztBQUVBLFdBQUEsS0FBQWlHLElBQUEsQ0FBQSxZQUFBO0FBQ0EsVUFBQWtDLFdBQUE5SSxFQUFBNkksSUFBQSxDQUFBLElBQUEsRUFBQSx1QkFBQSxDQUFBO0FBQ0EsVUFBQSxDQUFBQyxRQUFBLEVBQUE7QUFDQTlJLFVBQUE2SSxJQUFBLENBQUEsSUFBQSxFQUFBLHVCQUFBLEVBQUEsSUFBQTlHLE1BQUEsQ0FBQSxJQUFBLEVBQUFFLE9BQUEsQ0FBQTtBQUNBLE9BRkEsTUFFQSxJQUFBQSxZQUFBLFFBQUEsRUFBQTtBQUNBLGVBQUE2RyxTQUFBMUYsTUFBQSxDQUFBd0IsSUFBQSxDQUFBO0FBQ0E7QUFDQSxLQVBBLENBQUE7QUFRQSxHQWpCQTtBQW1CQSxDQTduQkEsRUE2bkJBbUUsTUE3bkJBLEVBNm5CQTlJLE1BN25CQSxFQTZuQkFDLFFBN25CQSIsImZpbGUiOiJsb2FuLWNhbGN1bGF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogalF1ZXJ5IExvYW4gQ2FsY3VsYXRvciAzLjEuM1xyXG4gKlxyXG4gKiBBdXRob3I6IEpvcmdlIEdvbnrDoWxleiA8c2NydWIubXhAZ21haWw+XHJcbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSAtIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXHJcbiAqL1xyXG47XHJcbihmdW5jdGlvbiAoJCwgd2luZG93LCBkb2N1bWVudCwgdW5kZWZpbmVkKSB7XHJcblxyXG4gIFwidXNlIHN0cmljdFwiO1xyXG5cclxuICAvKipcclxuICAgKiBUYWJsZSBvZiBjcmVkaXQgcmF0ZXMgZm9yIGV2ZXJ5IHNjb3JlLlxyXG4gICAqIEB0eXBlIHtPYmplY3R9XHJcbiAgICovXHJcbiAgdmFyIENSRURJVF9SQVRFUyA9IHtcclxuICAgICdBJzogNS4zMixcclxuICAgICdCJzogOC4xOCxcclxuICAgICdDJzogMTIuMjksXHJcbiAgICAnRCc6IDE1LjYxLFxyXG4gICAgJ0UnOiAxOC4yNSxcclxuICAgICdGJzogMjEuOTksXHJcbiAgICAnRyc6IDI2Ljc3XHJcbiAgfTtcclxuXHJcbiAgLyoqXHJcbiAgICogVGFibGUgb2YgYWxsb3dlZCBwYXltZW50IGZyZXF1ZW5jaWVzLlxyXG4gICAqIEB0eXBlIHtPYmplY3R9XHJcbiAgICovXHJcbiAgdmFyIFBBWU1FTlRfRlJFUVVFTkNJRVMgPSB7XHJcbiAgICAnd2Vla2x5JzogNTIsXHJcbiAgICAnYml3ZWVrbHknOiAyNixcclxuICAgICdtb250aGx5JzogMTJcclxuICB9O1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgbWluaW11bSBhbGxvd2VkIGZvciBhIGxvYW4uXHJcbiAgICogQHR5cGUge051bWJlcn1cclxuICAgKi9cclxuICB2YXIgTUlOSU1VTV9MT0FOID0gMTAwMDtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIG1pbmltdW0gZHVyYXRpb24gaW4gbW9udGhzLlxyXG4gICAqIEB0eXBlIHtOdW1iZXJ9XHJcbiAgICovXHJcbiAgdmFyIE1JTklNVU1fRFVSQVRJT04gPSAxO1xyXG5cclxuICAvKipcclxuICAgKiBEZWZhdWx0IG9wdGlvbnMgZm9yIHRoZSBwbHVnaW4uXHJcbiAgICogQHR5cGUge09iamVjdH1cclxuICAgKi9cclxuICB2YXIgZGVmYXVsdHMgPSB7XHJcbiAgICAvLyBkZWZhdWx0IHZhbHVlcyBmb3IgYSBsb2FuXHJcbiAgICBsb2FuQW1vdW50OiA1MDAwMCxcclxuICAgIGxvYW5EdXJhdGlvbjogMTIsXHJcbiAgICBjcmVkaXRSYXRlczogQ1JFRElUX1JBVEVTLFxyXG4gICAgY3JlZGl0U2NvcmU6ICdBJyxcclxuICAgIHZhbHVlQWRkZWRUYXg6IDAsXHJcbiAgICBzZXJ2aWNlRmVlOiAwLFxyXG4gICAgcGF5bWVudEZyZXF1ZW5jeTogJ21vbnRobHknLFxyXG5cclxuICAgIC8vIGlucHV0c1xyXG4gICAgbG9hbkFtb3VudFNlbGVjdG9yOiAnI2xvYW4tYW1vdW50JyxcclxuICAgIGxvYW5EdXJhdGlvblNlbGVjdG9yOiAnI2xvYW4tZHVyYXRpb24nLFxyXG4gICAgY3JlZGl0U2NvcmVTZWxlY3RvcjogJyNjcmVkaXQtc2NvcmUnLFxyXG4gICAgcGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yOiAnI3BheW1lbnQtZnJlcXVlbmN5JyxcclxuXHJcbiAgICAvLyBkaXNwbGF5IHNlbGVjdGVkIHZhbHVlc1xyXG4gICAgc2VsZWN0ZWRBbW91bnQ6ICcjc2VsZWN0ZWQtYW1vdW50JyxcclxuICAgIHNlbGVjdGVkRHVyYXRpb246ICcjc2VsZWN0ZWQtZHVyYXRpb24nLFxyXG4gICAgc2VsZWN0ZWRTY29yZTogJyNzZWxlY3RlZC1zY29yZScsXHJcbiAgICBzZWxlY3RlZFBheW1lbnRGcmVxdWVuY3k6ICcjc2VsZWN0ZWQtcGF5bWVudC1mcmVxdWVuY3knLFxyXG5cclxuICAgIC8vIGRpc3BsYXkgdGhlIHJlc3VsdHNcclxuICAgIGxvYW5Ub3RhbFNlbGVjdG9yOiAnI2xvYW4tdG90YWwnLFxyXG4gICAgcGF5bWVudFNlbGVjdG9yOiAnI3BheW1lbnQnLFxyXG4gICAgaW50ZXJlc3RUb3RhbFNlbGVjdG9yOiAnI2ludGVyZXN0LXRvdGFsJyxcclxuICAgIHNlcnZpY2VGZWVTZWxlY3RvcjogJyNzZXJ2aWNlLWZlZScsXHJcbiAgICB0YXhUb3RhbFNlbGVjdG9yOiAnI3RheC10b3RhbCcsXHJcbiAgICB0b3RhbEFubnVhbENvc3RTZWxlY3RvcjogJyN0b3RhbC1hbm51YWwtY29zdCcsXHJcbiAgICBsb2FuR3JhbmRUb3RhbFNlbGVjdG9yOiAnI2dyYW5kLXRvdGFsJ1xyXG4gIH07XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBhY3R1YWwgcGx1Z2luIGNvbnN0cnVjdG9yXHJcbiAgICogQHBhcmFtIHtPYmplY3R9IGVsZW1lbnRcclxuICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xyXG4gICAqL1xyXG4gIGZ1bmN0aW9uIFBsdWdpbihlbGVtZW50LCBvcHRpb25zKSB7XHJcbiAgICB0aGlzLiRlbCA9ICQoZWxlbWVudCk7XHJcbiAgICB0aGlzLl9uYW1lID0gJ2xvYW5DYWxjdWxhdG9yJztcclxuICAgIHRoaXMuX2RlZmF1bHRzID0gZGVmYXVsdHM7XHJcbiAgICB0aGlzLnNldHRpbmdzID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcclxuICAgIHRoaXMuYXR0YWNoTGlzdGVuZXJzKCk7XHJcbiAgICB0aGlzLmluaXQoKTtcclxuICB9XHJcblxyXG4gIC8vIEF2b2lkIFBsdWdpbi5wcm90b3R5cGUgY29uZmxpY3RzXHJcbiAgJC5leHRlbmQoUGx1Z2luLnByb3RvdHlwZSwge1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhdGVzIHRoZSBkYXRhIGFuZCBzaG93cyB0aGUgcmVzdWx0cy5cclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XHJcbiAgICAgKi9cclxuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdGhpcy52YWxpZGF0ZSgpO1xyXG4gICAgICB0aGlzLnJlbmRlcigpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEF0dGFjaCBldmVudCBsaXN0ZW5lcnMgdG8gdGhlIGV2ZW50IGhhbmRsZXJzLlxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgYXR0YWNoTGlzdGVuZXJzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBldmVudEVtaXR0ZXJzID0gW1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudFNlbGVjdG9yLFxyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uU2VsZWN0b3IsXHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZVNlbGVjdG9yLFxyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MucGF5bWVudEZyZXF1ZW5jeVNlbGVjdG9yXHJcbiAgICAgIF07XHJcblxyXG4gICAgICAkKGV2ZW50RW1pdHRlcnMuam9pbigpKS5vbih7XHJcbiAgICAgICAgY2hhbmdlOiB0aGlzLmV2ZW50SGFuZGxlci5iaW5kKHRoaXMpLFxyXG4gICAgICAgIG1vdXNlbW92ZTogdGhpcy5ldmVudEhhbmRsZXIuYmluZCh0aGlzKSxcclxuICAgICAgICB0b3VjaG1vdmU6IHRoaXMuZXZlbnRIYW5kbGVyLmJpbmQodGhpcylcclxuICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSGFuZGxlIGV2ZW50cyBmcm9tIHRoZSBET00uXHJcbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxyXG4gICAgICovXHJcbiAgICBldmVudEhhbmRsZXI6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdGhpcy51cGRhdGUoe1xyXG4gICAgICAgIGxvYW5BbW91bnQ6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50U2VsZWN0b3IpLnZhbCgpLFxyXG4gICAgICAgIGxvYW5EdXJhdGlvbjogdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmxvYW5EdXJhdGlvblNlbGVjdG9yKS52YWwoKSxcclxuICAgICAgICBjcmVkaXRTY29yZTogdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlU2VsZWN0b3IpLnZhbCgpLFxyXG4gICAgICAgIHBheW1lbnRGcmVxdWVuY3k6IHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5U2VsZWN0b3IpLnZhbCgpXHJcbiAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNhbml0aXplIGFuZCB2YWxpZGF0ZSB0aGUgdXNlciBpbnB1dCBkYXRhLlxyXG4gICAgICogQHRocm93cyBFcnJvclxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgdmFsaWRhdGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKHR5cGVvZiB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50ID0gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0eXBlb2YgdGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgIHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZSA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoISQuaXNQbGFpbk9iamVjdCh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzKSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbY3JlZGl0UmF0ZXNdIGlzIG5vdCB2YWxpZC4nKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgZm9yICh2YXIgY3JlZGl0UmF0ZSBpbiB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzKSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA9IHRoaXMuX3RvTnVtZXJpYyh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCEkLmlzTnVtZXJpYyh0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdKSkge1xyXG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdUaGUgdmFsdWUgcHJvdmlkZWQgZm9yIFtjcmVkaXRSYXRlc10gaXMgbm90IHZhbGlkLicpXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlc1tjcmVkaXRSYXRlXSA8IDEpIHtcclxuICAgICAgICAgIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbY3JlZGl0UmF0ZV0gPSB0aGlzLnNldHRpbmdzLmNyZWRpdFJhdGVzW2NyZWRpdFJhdGVdICogMTAwO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gU2FuaXRpemUgdGhlIGlucHV0XHJcbiAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudCA9IHBhcnNlRmxvYXQodGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50KTtcclxuICAgICAgdGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb24gPSBwYXJzZUZsb2F0KHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uKTtcclxuICAgICAgdGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlID0gcGFyc2VGbG9hdCh0aGlzLnNldHRpbmdzLnNlcnZpY2VGZWUpO1xyXG4gICAgICB0aGlzLnNldHRpbmdzLmNyZWRpdFNjb3JlID0gJC50cmltKHRoaXMuc2V0dGluZ3MuY3JlZGl0U2NvcmUudG9VcHBlckNhc2UoKSk7XHJcblxyXG4gICAgICBpZiAoIVBBWU1FTlRfRlJFUVVFTkNJRVMuaGFzT3duUHJvcGVydHkodGhpcy5zZXR0aW5ncy5wYXltZW50RnJlcXVlbmN5KSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbcGF5bWVudEZyZXF1ZW5jeV0gaXMgbm90IHZhbGlkLicpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoIXRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXMuaGFzT3duUHJvcGVydHkodGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZSkpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2NyZWRpdFNjb3JlXSBpcyBub3QgdmFsaWQuJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgPCBNSU5JTVVNX0xPQU4pIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZSB2YWx1ZSBwcm92aWRlZCBmb3IgW2xvYW5BbW91bnRdIG11c3QgbWUgYXQgbGVhc3QgMTAwMC4nKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uIDwgTUlOSU1VTV9EVVJBVElPTikge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbbG9hbkR1cmF0aW9uXSBtdXN0IG1lIGF0IGxlYXN0IDEuJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghJC5pc051bWVyaWModGhpcy5zZXR0aW5ncy5zZXJ2aWNlRmVlKSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVGhlIHZhbHVlIHByb3ZpZGVkIGZvciBbc2VydmljZUZlZV0gaXMgbm90IHZhbGlkLicpO1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2hvdyB0aGUgcmVzdWx0cyBpbiB0aGUgRE9NLlxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHRoaXMuX2Rpc3BsYXlTZWxlY3RlZFZhbHVlcygpO1xyXG4gICAgICB0aGlzLl9kaXNwbGF5UmVzdWx0cygpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNob3cgdGhlIHNlbGVjdGVkIHZhbHVlcyBpbiB0aGUgRE9NLlxyXG4gICAgICogQHJldHVybiB7dm9pZH1cclxuICAgICAqL1xyXG4gICAgX2Rpc3BsYXlTZWxlY3RlZFZhbHVlczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBsb2FuIGFtb3VudFxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VsZWN0ZWRBbW91bnQpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBsb2FuIGR1cmF0aW9uXHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5zZWxlY3RlZER1cmF0aW9uKS5odG1sKFxyXG4gICAgICAgIHRoaXMuc2V0dGluZ3MubG9hbkR1cmF0aW9uXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBzZWxlY3RlZCBjcmVkaXQgc2NvcmVcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkU2NvcmUpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZVxyXG4gICAgICApO1xyXG5cclxuICAgICAgLy8gRGlzcGxheSB0aGUgc2VsZWN0ZWQgcGF5bWVudCBmcmVxdWVuY3lcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnNlbGVjdGVkUGF5bWVudEZyZXF1ZW5jeSkuaHRtbChcclxuICAgICAgICB0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3lcclxuICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNwbGF5IHRoZSByZXN1bHRzIGZvciB0aGUgY3VycmVudCB2YWx1ZXMuXHJcbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxyXG4gICAgICovXHJcbiAgICBfZGlzcGxheVJlc3VsdHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgLy8gRGlzcGxheSB0aGUgbG9hbiB0b3RhbFxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MubG9hblRvdGFsU2VsZWN0b3IpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl9sb2FuVG90YWwoKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIGxvYW4gcGVyaW9kaWMgcGF5bWVudFxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3MucGF5bWVudFNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fUE1UKCkpXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBEaXNwbGF5IHRoZSBpbnRlcmVzdCB0b3RhbCBhbW91bnRcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLmludGVyZXN0VG90YWxTZWxlY3RvcikuaHRtbChcclxuICAgICAgICB0aGlzLl90b01vbmV5KHRoaXMuX2ludGVyZXN0VG90YWwoKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIHRheCB0b3RhbCBhbW91bnRcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnRheFRvdGFsU2VsZWN0b3IpLmh0bWwoXHJcbiAgICAgICAgdGhpcy5fdG9Nb25leSh0aGlzLl90YXhUb3RhbCgpKVxyXG4gICAgICApO1xyXG5cclxuICAgICAgLy8gRGlzcGxheSB0aGUgYW5udWFsIHRvdGFsIGNvc3RcclxuICAgICAgdGhpcy4kZWwuZmluZCh0aGlzLnNldHRpbmdzLnRvdGFsQW5udWFsQ29zdFNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvUGVyY2VudGFnZSh0aGlzLl9DQVQoKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIERpc3BsYXkgdGhlIHNlcnZpY2UgZmVlIGlmIGFueVxyXG4gICAgICB0aGlzLiRlbC5maW5kKHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZVNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fc2VydmljZUZlZVdpdGhWQVQoKSlcclxuICAgICAgKTtcclxuXHJcbiAgICAgIHRoaXMuJGVsLmZpbmQodGhpcy5zZXR0aW5ncy5sb2FuR3JhbmRUb3RhbFNlbGVjdG9yKS5odG1sKFxyXG4gICAgICAgIHRoaXMuX3RvTW9uZXkodGhpcy5fZ3JhbmRUb3RhbCgpKVxyXG4gICAgICApO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJ1biB0aGUgaW5pdCBtZXRob2QgYWdhaW4gd2l0aCB0aGUgcHJvdmlkZWQgb3B0aW9ucy5cclxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBhcmdzXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZTogZnVuY3Rpb24gKGFyZ3MpIHtcclxuICAgICAgdGhpcy5zZXR0aW5ncyA9ICQuZXh0ZW5kKHt9LCB0aGlzLl9kZWZhdWx0cywgdGhpcy5zZXR0aW5ncywgYXJncyk7XHJcbiAgICAgIHRoaXMuaW5pdCgpO1xyXG4gICAgICB0aGlzLiRlbC50cmlnZ2VyKCdsb2FuOnVwZGF0ZScpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyYXRlIHRoZSByZXN1bHRzIGFzIGFuIGFycmF5IG9mIG9iamVjdHMsXHJcbiAgICAgKiBlYWNoIG9iamVjdCBjb250YWlucyB0aGUgdmFsdWVzIGZvciBlYWNoIHBlcmlvZC5cclxuICAgICAqIEByZXR1cm4ge0FycmF5fVxyXG4gICAgICovXHJcbiAgICBfcmVzdWx0czogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgYmFsYW5jZSA9IHRoaXMuc2V0dGluZ3MubG9hbkFtb3VudDtcclxuICAgICAgdmFyIGluaXRpYWwgPSB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQ7XHJcbiAgICAgIHZhciBpbnRlcmVzdFJhdGUgPSB0aGlzLl9pbnRlcmVzdFJhdGUoKTtcclxuICAgICAgdmFyIFZBVCA9IHRoaXMuX3ZhbHVlQWRkZWRUYXgoKTtcclxuICAgICAgdmFyIHBheW1lbnQgPSB0aGlzLl9QTVQoKTtcclxuICAgICAgdmFyIG51bWJlck9mUGF5bWVudHMgPSB0aGlzLl9udW1iZXJPZlBheW1lbnRzKCk7XHJcbiAgICAgIHZhciByZXN1bHRzID0gW107XHJcblxyXG4gICAgICAvLyBXZSBsb29wIG92ZXIgdGhlIG51bWJlciBvZiBwYXltZW50cyBhbmQgZWFjaCB0aW1lXHJcbiAgICAgIC8vIHdlIGV4dHJhY3QgdGhlIGluZm9ybWF0aW9uIHRvIGJ1aWxkIHRoZSBwZXJpb2RcclxuICAgICAgLy8gdGhhdCB3aWxsIGJlIGFwcGVuZGVkIHRvIHRoZSByZXN1bHRzIGFycmF5LlxyXG4gICAgICBmb3IgKHZhciBwYXltZW50TnVtYmVyID0gMDsgcGF5bWVudE51bWJlciA8IG51bWJlck9mUGF5bWVudHM7IHBheW1lbnROdW1iZXIrKykge1xyXG4gICAgICAgIHZhciBpbnRlcmVzdCA9IGJhbGFuY2UgKiBpbnRlcmVzdFJhdGU7XHJcbiAgICAgICAgdmFyIHRheGVzUGFpZCA9IGJhbGFuY2UgKiBpbnRlcmVzdFJhdGUgKiBWQVQ7XHJcbiAgICAgICAgdmFyIHByaW5jaXBhbCA9IHBheW1lbnQgLSBpbnRlcmVzdCAtIHRheGVzUGFpZDtcclxuXHJcbiAgICAgICAgLy8gdXBkYXRlIGluaXRpYWwgYmFsYW5jZSBmb3IgbmV4dCBpdGVyYXRpb25cclxuICAgICAgICBpbml0aWFsID0gYmFsYW5jZTtcclxuXHJcbiAgICAgICAgLy8gdXBkYXRlIGZpbmFsIGJhbGFuY2UgZm9yIHRoZSBuZXh0IGl0ZXJhdGlvbi5cclxuICAgICAgICBiYWxhbmNlID0gYmFsYW5jZSAtIHByaW5jaXBhbDtcclxuXHJcbiAgICAgICAgcmVzdWx0cy5wdXNoKHtcclxuICAgICAgICAgIGluaXRpYWw6IGluaXRpYWwsXHJcbiAgICAgICAgICBwcmluY2lwYWw6IHByaW5jaXBhbCxcclxuICAgICAgICAgIGludGVyZXN0OiBpbnRlcmVzdCxcclxuICAgICAgICAgIHRheDogdGF4ZXNQYWlkLFxyXG4gICAgICAgICAgcGF5bWVudDogcGF5bWVudCxcclxuICAgICAgICAgIGJhbGFuY2U6IGJhbGFuY2VcclxuICAgICAgICB9KVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgcmV0dXJuIHJlc3VsdHM7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2VuZXJhdGUgdGhlIGFtb3J0aXphdGlvbiBzY2hlZHVsZS5cclxuICAgICAqIEByZXR1cm4ge0FycmF5fVxyXG4gICAgICovXHJcbiAgICBzY2hlZHVsZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gJC5tYXAodGhpcy5fcmVzdWx0cygpLCBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgaW5pdGlhbDogdGhpcy5fdG9Nb25leSh2YWx1ZS5pbml0aWFsKSxcclxuICAgICAgICAgIHByaW5jaXBhbDogdGhpcy5fdG9Nb25leSh2YWx1ZS5wcmluY2lwYWwpLFxyXG4gICAgICAgICAgaW50ZXJlc3Q6IHRoaXMuX3RvTW9uZXkodmFsdWUuaW50ZXJlc3QpLFxyXG4gICAgICAgICAgdGF4OiB0aGlzLl90b01vbmV5KHZhbHVlLnRheCksXHJcbiAgICAgICAgICBwYXltZW50OiB0aGlzLl90b01vbmV5KHZhbHVlLnBheW1lbnQpLFxyXG4gICAgICAgICAgYmFsYW5jZTogdGhpcy5fdG9Nb25leSh2YWx1ZS5iYWxhbmNlKVxyXG4gICAgICAgIH1cclxuICAgICAgfS5iaW5kKHRoaXMpKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gdGhlIGNyZWRpdCByYXRlcyBiZWluZyB1c2VkLlxyXG4gICAgICogQHJldHVybiB7T2JqZWN0fVxyXG4gICAgICovXHJcbiAgICBjcmVkaXRSYXRlczogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5jcmVkaXRSYXRlcztcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIGNyZWRpdCByYXRlIGNvcnJlc3BvbmRpbmcgdG8gdGhlIGN1cnJlbnQgY3JlZGl0IHNjb3JlLlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfYW5udWFsSW50ZXJlc3RSYXRlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLmhhc093blByb3BlcnR5KCdpbnRlcmVzdFJhdGUnKSkge1xyXG4gICAgICAgIGlmICh0aGlzLnNldHRpbmdzLmludGVyZXN0UmF0ZSA8PSAxKSB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5zZXR0aW5ncy5pbnRlcmVzdFJhdGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3MuaW50ZXJlc3RSYXRlKSAvIDEwMDtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXMuc2V0dGluZ3MuY3JlZGl0UmF0ZXNbdGhpcy5zZXR0aW5ncy5jcmVkaXRTY29yZV0gLyAxMDA7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHRoZSBwZXJpb2RpYyBpbnRlcmVzdCByYXRlLlxyXG4gICAgICogQHJldHVybnMge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX2ludGVyZXN0UmF0ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fYW5udWFsSW50ZXJlc3RSYXRlKCkgLyB0aGlzLl9wYXltZW50RnJlcXVlbmN5KCk7XHJcbiAgICB9LFxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIHBlcmlvZGljIHBheW1lbnQgZnJlcXVlbmN5XHJcbiAgICAgKiBAcmV0dXJucyB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfcGF5bWVudEZyZXF1ZW5jeTogZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gUEFZTUVOVF9GUkVRVUVOQ0lFU1t0aGlzLnNldHRpbmdzLnBheW1lbnRGcmVxdWVuY3ldO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgbnVtYmVyIG9mIHBheW1lbnRzIGZvciB0aGUgbG9hbi5cclxuICAgICAqIEByZXR1cm5zIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9udW1iZXJPZlBheW1lbnRzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBkdXJhdGlvbkluWWVhcnMgPSB0aGlzLl90b051bWVyaWModGhpcy5zZXR0aW5ncy5sb2FuRHVyYXRpb24pIC8gMTI7XHJcblxyXG4gICAgICByZXR1cm4gTWF0aC5mbG9vcihkdXJhdGlvbkluWWVhcnMgKiBQQVlNRU5UX0ZSRVFVRU5DSUVTW3RoaXMuc2V0dGluZ3MucGF5bWVudEZyZXF1ZW5jeV0pO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZXMgdGhlIHRvdGFsIGNvc3Qgb2YgdGhlIGxvYW4uXHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9sb2FuVG90YWw6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuX1BNVCgpICogdGhpcy5fbnVtYmVyT2ZQYXltZW50cygpO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZSB0aGUgbW9udGhseSBhbW9ydGl6ZWQgbG9hbiBwYXltZW50cy5cclxuICAgICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQ29tcG91bmRfaW50ZXJlc3QjTW9udGhseV9hbW9ydGl6ZWRfbG9hbl9vcl9tb3J0Z2FnZV9wYXltZW50c1xyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfUE1UOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBpID0gdGhpcy5faW50ZXJlc3RSYXRlKCk7XHJcbiAgICAgIHZhciBMID0gdGhpcy5zZXR0aW5ncy5sb2FuQW1vdW50O1xyXG4gICAgICB2YXIgbiA9IHRoaXMuX251bWJlck9mUGF5bWVudHMoKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLnNldHRpbmdzLnZhbHVlQWRkZWRUYXggIT09IDApIHtcclxuICAgICAgICBpID0gKDEgKyB0aGlzLl92YWx1ZUFkZGVkVGF4KCkpICogaTsgLy8gaW50ZXJlc3QgcmF0ZSB3aXRoIHRheFxyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gKEwgKiBpKSAvICgxIC0gTWF0aC5wb3coMSArIGksIC1uKSk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsY3VsYXRlIHRoZSB0b3RhbCBpbnRlcmVzdCBmb3IgdGhlIGxvYW4uXHJcbiAgICAgKiBAcmV0dXJucyB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfaW50ZXJlc3RUb3RhbDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgdG90YWwgPSAwO1xyXG4gICAgICAkLmVhY2godGhpcy5fcmVzdWx0cygpLCBmdW5jdGlvbiAoaW5kZXgsIHZhbHVlKSB7XHJcbiAgICAgICAgdG90YWwgKz0gdmFsdWUuaW50ZXJlc3Q7XHJcbiAgICAgIH0pO1xyXG4gICAgICByZXR1cm4gdG90YWw7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsY3VsYXRlIHRoZSB2YWx1ZSBhZGRlZCB0YXggdG90YWwgZm9yIHRoZSBsb2FuLlxyXG4gICAgICogQHJldHVybnMge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3RheFRvdGFsOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciB0b3RhbCA9IDA7XHJcbiAgICAgICQuZWFjaCh0aGlzLl9yZXN1bHRzKCksIGZ1bmN0aW9uIChpbmRleCwgdmFsdWUpIHtcclxuICAgICAgICB0b3RhbCArPSB2YWx1ZS50YXg7XHJcbiAgICAgIH0pO1xyXG4gICAgICByZXR1cm4gdG90YWw7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSBsb2FuIGZlZXMgYW5kIGNvbW1pc3Npb25zIHRvdGFsLlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfc2VydmljZUZlZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgc2VydmljZUZlZSA9IHRoaXMuc2V0dGluZ3Muc2VydmljZUZlZTtcclxuXHJcbiAgICAgIC8vIGlmIHRoZSBzZXJ2aWNlIGZlZSBpcyBncmVhdGVyIHRoYW4gMSB0aGVuIHRoZVxyXG4gICAgICAvLyB2YWx1ZSBtdXN0IGJlIGNvbnZlcnRlZCB0byBkZWNpbWFscyBmaXJzdC5cclxuICAgICAgaWYgKHNlcnZpY2VGZWUgPiAxKSB7XHJcbiAgICAgICAgc2VydmljZUZlZSA9IHNlcnZpY2VGZWUgLyAxMDA7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnQgKiBzZXJ2aWNlRmVlO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgbG9hbiBmZWVzIGFuZCBjb21taXNzaW9ucyB0b3RhbCB3aXRoIFZBVCBpbmNsdWRlZC5cclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3NlcnZpY2VGZWVXaXRoVkFUOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9zZXJ2aWNlRmVlKCkgKiAodGhpcy5fdmFsdWVBZGRlZFRheCgpICsgMSlcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxjdWxhdGVzIHRoZSB0b3RhbCBjb3N0IG9mIHRoZSBsb2FuIGluY2x1ZGluZyB0aGUgc2VydmljZSBmZWUuXHJcbiAgICAgKiBAcmV0dXJuIHtOdW1iZXJ9XHJcbiAgICAgKi9cclxuICAgIF9ncmFuZFRvdGFsOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl9sb2FuVG90YWwoKSArIHRoaXMuX3NlcnZpY2VGZWVXaXRoVkFUKCk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSB0b3RhbCBhbm51YWwgY29zdCAoQ0FUKVxyXG4gICAgICogQHNlZSBodHRwOi8vd3d3LmJhbnhpY28ub3JnLm14L0NBVFxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfQ0FUOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHZhciBJUlIgPSB0aGlzLl9JUlIodGhpcy5fY2FzaEZsb3coKSk7XHJcbiAgICAgIHZhciBwZXJpb2RzID0gdGhpcy5fcGF5bWVudEZyZXF1ZW5jeSgpO1xyXG5cclxuICAgICAgcmV0dXJuIE1hdGgucG93KDEgKyBJUlIsIHBlcmlvZHMpIC0gMTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGFuIGFycmF5IHdpdGggYSBzZXJpZXMgb2YgY2FzaCBmbG93cyBmb3IgdGhlIGN1cnJlbnQgbG9hbi5cclxuICAgICAqIEByZXR1cm4ge0FycmF5fVxyXG4gICAgICovXHJcbiAgICBfY2FzaEZsb3c6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIHJlc3VsdHMgPSB0aGlzLl9yZXN1bHRzKCk7XHJcbiAgICAgIHZhciBjYXNoRmxvdyA9IFt0aGlzLl9zZXJ2aWNlRmVlKCkgLSB0aGlzLnNldHRpbmdzLmxvYW5BbW91bnRdO1xyXG5cclxuICAgICAgJC5lYWNoKHJlc3VsdHMsIGZ1bmN0aW9uIChpbmRleCwgcGVyaW9kKSB7XHJcbiAgICAgICAgY2FzaEZsb3cucHVzaChwZXJpb2QucGF5bWVudCAtIHBlcmlvZC50YXgpO1xyXG4gICAgICB9LmJpbmQodGhpcykpO1xyXG5cclxuICAgICAgcmV0dXJuIGNhc2hGbG93O1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIGludGVybmFsIHJhdGUgb2YgcmV0dXJuIGZvciBhIHNlcmllcyBvZiBjYXNoIGZsb3dzIHJlcHJlc2VudGVkIGJ5IHRoZSBudW1iZXJzIGluIHZhbHVlcy5cclxuICAgICAqIEBwYXJhbSAge0FycmF5fSB2YWx1ZXNcclxuICAgICAqIEBwYXJhbSAge051bWJlcn0gZ3Vlc3NcclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX0lSUjogZnVuY3Rpb24gKHZhbHVlcywgZ3Vlc3MpIHtcclxuICAgICAgZ3Vlc3MgPSBndWVzcyB8fCAwO1xyXG5cclxuICAgICAgLy8gQ2FsY3VsYXRlcyB0aGUgcmVzdWx0aW5nIGFtb3VudFxyXG4gICAgICB2YXIgaXJyUmVzdWx0ID0gZnVuY3Rpb24gKHZhbHVlcywgZGF0ZXMsIHJhdGUpIHtcclxuICAgICAgICB2YXIgcmVzdWx0ID0gdmFsdWVzWzBdO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgcmVzdWx0ICs9IHZhbHVlc1tpXSAvIE1hdGgucG93KHJhdGUgKyAxLCAoZGF0ZXNbaV0gLSBkYXRlc1swXSkgLyAzNjUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgfTtcclxuXHJcbiAgICAgIC8vIENhbGN1bGF0ZXMgdGhlIGZpcnN0IGRlcml2YXRpb25cclxuICAgICAgdmFyIGlyclJlc3VsdERlcml2YXRpdmUgPSBmdW5jdGlvbiAodmFsdWVzLCBkYXRlcywgcmF0ZSkge1xyXG4gICAgICAgIHZhciByZXN1bHQgPSAwO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgdmFyIGZyYWMgPSAoZGF0ZXNbaV0gLSBkYXRlc1swXSkgLyAzNjU7XHJcbiAgICAgICAgICByZXN1bHQgLT0gZnJhYyAqIHZhbHVlc1tpXSAvIE1hdGgucG93KHJhdGUgKyAxLCBmcmFjICsgMSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgLy8gSW5pdGlhbGl6ZSBkYXRlcyBhbmQgY2hlY2sgdGhhdCB2YWx1ZXMgY29udGFpbnMgYXRcclxuICAgICAgLy8gbGVhc3Qgb25lIHBvc2l0aXZlIHZhbHVlIGFuZCBvbmUgbmVnYXRpdmUgdmFsdWVcclxuICAgICAgdmFyIGRhdGVzID0gW107XHJcbiAgICAgIHZhciBwb3NpdGl2ZSA9IGZhbHNlO1xyXG4gICAgICB2YXIgbmVnYXRpdmUgPSBmYWxzZTtcclxuXHJcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdmFsdWVzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgZGF0ZXNbaV0gPSAoaSA9PT0gMCkgPyAwIDogZGF0ZXNbaSAtIDFdICsgMzY1O1xyXG4gICAgICAgIGlmICh2YWx1ZXNbaV0gPiAwKSBwb3NpdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgaWYgKHZhbHVlc1tpXSA8IDApIG5lZ2F0aXZlID0gdHJ1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKCFwb3NpdGl2ZSB8fCAhbmVnYXRpdmUpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXHJcbiAgICAgICAgICAnRXJyb3IgdGhlIHZhbHVlcyBkb2VzIG5vdCBjb250YWluIGF0IGxlYXN0IG9uZSBwb3NpdGl2ZSB2YWx1ZSBhbmQgb25lIG5lZ2F0aXZlIHZhbHVlJ1xyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIEluaXRpYWxpemUgZ3Vlc3MgYW5kIHJlc3VsdFJhdGVcclxuICAgICAgZ3Vlc3MgPSAoZ3Vlc3MgPT09IHVuZGVmaW5lZCkgPyAwLjEgOiBndWVzcztcclxuICAgICAgdmFyIHJlc3VsdFJhdGUgPSBndWVzcztcclxuXHJcbiAgICAgIC8vIFNldCBtYXhpbXVtIGVwc2lsb24gZm9yIGVuZCBvZiBpdGVyYXRpb25cclxuICAgICAgdmFyIGVwc01heCA9IDFlLTEwO1xyXG5cclxuICAgICAgLy8gSW1wbGVtZW50IE5ld3RvbidzIG1ldGhvZFxyXG4gICAgICB2YXIgbmV3UmF0ZSwgZXBzUmF0ZSwgcmVzdWx0VmFsdWU7XHJcbiAgICAgIHZhciBjb250TG9vcCA9IHRydWU7XHJcblxyXG4gICAgICBkbyB7XHJcbiAgICAgICAgcmVzdWx0VmFsdWUgPSBpcnJSZXN1bHQodmFsdWVzLCBkYXRlcywgcmVzdWx0UmF0ZSk7XHJcbiAgICAgICAgbmV3UmF0ZSA9IHJlc3VsdFJhdGUgLSByZXN1bHRWYWx1ZSAvIGlyclJlc3VsdERlcml2YXRpdmUodmFsdWVzLCBkYXRlcywgcmVzdWx0UmF0ZSk7XHJcbiAgICAgICAgZXBzUmF0ZSA9IE1hdGguYWJzKG5ld1JhdGUgLSByZXN1bHRSYXRlKTtcclxuICAgICAgICByZXN1bHRSYXRlID0gbmV3UmF0ZTtcclxuICAgICAgICBjb250TG9vcCA9IChlcHNSYXRlID4gZXBzTWF4KSAmJiAoTWF0aC5hYnMocmVzdWx0VmFsdWUpID4gZXBzTWF4KTtcclxuICAgICAgfSB3aGlsZSAoY29udExvb3ApO1xyXG5cclxuICAgICAgLy8gUmV0dXJuIGludGVybmFsIHJhdGUgb2YgcmV0dXJuXHJcbiAgICAgIHJldHVybiByZXN1bHRSYXRlO1xyXG4gICAgfSxcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiB0aGUgdmFsdWUgYWRkZWQgdGF4IGluIGRlY2ltYWxzLlxyXG4gICAgICogQHJldHVybiB7TnVtYmVyfVxyXG4gICAgICovXHJcbiAgICBfdmFsdWVBZGRlZFRheDogZnVuY3Rpb24gKCkge1xyXG4gICAgICB2YXIgdGF4ID0gdGhpcy5fdG9OdW1lcmljKHRoaXMuc2V0dGluZ3MudmFsdWVBZGRlZFRheCB8fCAwKTtcclxuXHJcbiAgICAgIC8vIGlmIHRheCBpcyBncmVhdGVyIHRoYW4gMSBtZWFucyB0aGUgdmFsdWVcclxuICAgICAgLy8gbXVzdCBiZSBjb252ZXJ0ZWQgdG8gZGVjaW1hbHMgZmlyc3QuXHJcbiAgICAgIHJldHVybiAodGF4ID4gMSkgPyB0YXggLyAxMDAgOiB0YXg7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBudW1lcmljIGZvcm1hdCB0byBtb25leSBmb3JtYXQuXHJcbiAgICAgKiBAcGFyYW0gIHtOdW1iZXJ9IG51bWVyaWNcclxuICAgICAqIEByZXR1cm4ge1N0cmluZ31cclxuICAgICAqL1xyXG4gICAgX3RvTW9uZXk6IGZ1bmN0aW9uIChudW1lcmljKSB7XHJcbiAgICAgIGlmICh0eXBlb2YgbnVtZXJpYyA9PSAnc3RyaW5nJykge1xyXG4gICAgICAgIG51bWVyaWMgPSBwYXJzZUZsb2F0KG51bWVyaWMpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gJ0FFRCAnICsgbnVtZXJpYy50b0ZpeGVkKDIpLnJlcGxhY2UoLyhcXGQpKD89KFxcZHszfSkrXFwuKS9nLCAnJDEsJyk7XHJcbiAgICB9LFxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydCBmcm9tIG1vbmV5IGZvcm1hdCB0byBudW1lcmljIGZvcm1hdC5cclxuICAgICAqIEBwYXJhbSAge1N0cmluZ30gdmFsdWVcclxuICAgICAqIEByZXR1cm4ge051bWJlcn1cclxuICAgICAqL1xyXG4gICAgX3RvTnVtZXJpYzogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgIHJldHVybiBwYXJzZUZsb2F0KFxyXG4gICAgICAgIHZhbHVlLnRvU3RyaW5nKCkucmVwbGFjZSgvW14wLTlcXC5dKy9nLCAnJylcclxuICAgICAgKTtcclxuICAgIH0sXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUbyBjb252ZXJ0IHRoZSBwcm92aWRlZCB2YWx1ZSB0byBwZXJjZW50IGZvcm1hdC5cclxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBudW1lcmljXHJcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBfdG9QZXJjZW50YWdlOiBmdW5jdGlvbiAobnVtZXJpYykge1xyXG4gICAgICByZXR1cm4gKG51bWVyaWMgKiAxMDApLnRvRml4ZWQoMikgKyAnJSc7XHJcbiAgICB9XHJcblxyXG4gIH0pO1xyXG5cclxuICAvKipcclxuICAgKiBXcmFwcGVyIGFyb3VuZCB0aGUgY29uc3RydWN0b3IgdG8gcHJldmVudCBtdWx0aXBsZSBpbnN0YW50aWF0aW9ucy5cclxuICAgKi9cclxuICAkLmZuLmxvYW5DYWxjdWxhdG9yID0gZnVuY3Rpb24gKG9wdGlvbnMsIGFyZ3MpIHtcclxuICAgIGlmIChvcHRpb25zID09PSAnc2NoZWR1bGUnKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmRhdGEoJ3BsdWdpbl9sb2FuQ2FsY3VsYXRvcicpLnNjaGVkdWxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKG9wdGlvbnMgPT09ICdyYXRlcycpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZGF0YSgncGx1Z2luX2xvYW5DYWxjdWxhdG9yJykuY3JlZGl0UmF0ZXMoKTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgdmFyIGluc3RhbmNlID0gJC5kYXRhKHRoaXMsICdwbHVnaW5fbG9hbkNhbGN1bGF0b3InKTtcclxuICAgICAgaWYgKCFpbnN0YW5jZSkge1xyXG4gICAgICAgICQuZGF0YSh0aGlzLCAncGx1Z2luX2xvYW5DYWxjdWxhdG9yJywgbmV3IFBsdWdpbih0aGlzLCBvcHRpb25zKSk7XHJcbiAgICAgIH0gZWxzZSBpZiAob3B0aW9ucyA9PT0gJ3VwZGF0ZScpIHtcclxuICAgICAgICByZXR1cm4gaW5zdGFuY2UudXBkYXRlKGFyZ3MpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9O1xyXG5cclxufSkoalF1ZXJ5LCB3aW5kb3csIGRvY3VtZW50KTsiXX0=
