/*!
 * project-name v0.0.1
 * A description for your project.
 * (c) 2020 rth
 * MIT License
 * http://link-to-your-git-repo.com
 */

// alsvin

$('#alsvin-white').click((function () {
    $('#alsvin').reel('images', '../public/images/360/alsvin/white/####.png');
}));
$('#alsvin-red').click((function () {
    $('#alsvin').reel('images', '../public/images/360/alsvin/red/####.png');
}));
$('#alsvin-blue').click((function () {
    $('#alsvin').reel('images', '../public/images/360/alsvin/blue/####.png');
}));
$('#alsvin-grey').click((function () {
    $('#alsvin').reel('images', '../public/images/360/alsvin/grey/####.png');
}));
$('#alsvin-black').click((function () {
    $('#alsvin').reel('images', '../public/images/360/alsvin/black/####.png');
}));

// cs35
$('#cs35-white').click((function () {
    $('#cs35').reel('images', '../public/images/360/cs35/white/####.png');
}));
$('#cs35-red').click((function () {
    $('#cs35').reel('images', '../public/images/360/cs35/red/####.png');
}));
$('#cs35-blue').click((function () {
    $('#cs35').reel('images', '../public/images/360/cs35/blue/####.png');
}));
$('#cs35-grey').click((function () {
    $('#cs35').reel('images', '../public/images/360/cs35/grey/####.png');
}));
$('#cs35-brown').click((function () {
    $('#cs35').reel('images', '../public/images/360/cs35/brown/####.png');
}));

// cs85


$('#cs85-white').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/white/####.png');
}));
$('#cs85-red').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/red/####.png');
}));
$('#cs85-purple').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/purple/####.png');
}));
$('#cs85-blue').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/blue/####.png');
}));
$('#cs85-grey').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/grey/####.png');
}));
$('#cs85-black').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/black/####.png');
}));
$('#cs85-black').click((function () {
    $('#cs85').reel('images', '../public/images/360/cs85/black/####.png');
}));

// cs95
$('#cs95-white').click((function () {
    $('#cs95').reel('images', '../public/images/360/cs95/white/####.png');
}));

$('#cs95-grey').click((function () {
    $('#cs95').reel('images', '../public/images/360/cs95/grey/####.png');
}));
$('#cs95-black').click((function () {
    $('#cs95').reel('images', '../public/images/360/cs95/black/####.png');
}));
$('#cs95-brown').click((function () {
    $('#cs95').reel('images', '../public/images/360/cs95/brown/####.png');
}));

$.reel.def.indicator = 5;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIjM2MC5qcyJdLCJuYW1lcyI6WyIkIiwiY2xpY2siLCJyZWVsIiwiZGVmIiwiaW5kaWNhdG9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBOztBQUVBQSxFQUFBLGVBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxTQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsa0NBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsYUFBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLFNBQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSxnQ0FBQTtBQUNBLENBRkE7QUFHQUYsRUFBQSxjQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsU0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLGlDQUFBO0FBQ0EsQ0FGQTtBQUdBRixFQUFBLGNBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxTQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsaUNBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsZUFBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLFNBQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSxrQ0FBQTtBQUNBLENBRkE7O0FBSUE7QUFDQUYsRUFBQSxhQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLGdDQUFBO0FBQ0EsQ0FGQTtBQUdBRixFQUFBLFdBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsOEJBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsWUFBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLE9BQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSwrQkFBQTtBQUNBLENBRkE7QUFHQUYsRUFBQSxZQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLCtCQUFBO0FBQ0EsQ0FGQTtBQUdBRixFQUFBLGFBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsZ0NBQUE7QUFDQSxDQUZBOztBQUtBOzs7QUFHQUYsRUFBQSxhQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLGdDQUFBO0FBQ0EsQ0FGQTtBQUdBRixFQUFBLFdBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsOEJBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsY0FBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLE9BQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSxpQ0FBQTtBQUNBLENBRkE7QUFHQUYsRUFBQSxZQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLCtCQUFBO0FBQ0EsQ0FGQTtBQUdBRixFQUFBLFlBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsK0JBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsYUFBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLE9BQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSxnQ0FBQTtBQUNBLENBRkE7QUFHQUYsRUFBQSxhQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLGdDQUFBO0FBQ0EsQ0FGQTs7QUFNQTtBQUNBRixFQUFBLGFBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsZ0NBQUE7QUFDQSxDQUZBOztBQUlBRixFQUFBLFlBQUEsRUFBQUMsS0FBQSxDQUFBLFlBQUE7QUFDQUQsTUFBQSxPQUFBLEVBQUFFLElBQUEsQ0FBQSxRQUFBLEVBQUEsK0JBQUE7QUFDQSxDQUZBO0FBR0FGLEVBQUEsYUFBQSxFQUFBQyxLQUFBLENBQUEsWUFBQTtBQUNBRCxNQUFBLE9BQUEsRUFBQUUsSUFBQSxDQUFBLFFBQUEsRUFBQSxnQ0FBQTtBQUNBLENBRkE7QUFHQUYsRUFBQSxhQUFBLEVBQUFDLEtBQUEsQ0FBQSxZQUFBO0FBQ0FELE1BQUEsT0FBQSxFQUFBRSxJQUFBLENBQUEsUUFBQSxFQUFBLGdDQUFBO0FBQ0EsQ0FGQTs7QUFNQUYsRUFBQUUsSUFBQSxDQUFBQyxHQUFBLENBQUFDLFNBQUEsR0FBQSxDQUFBIiwiZmlsZSI6IjM2MC5wb2x5ZmlsbHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhbHN2aW5cclxuXHJcbiQoJyNhbHN2aW4td2hpdGUnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjYWxzdmluJykucmVlbCgnaW1hZ2VzJywgJ2ltYWdlcy8zNjAvYWxzdmluL3doaXRlLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjYWxzdmluLXJlZCcpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNhbHN2aW4nKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9hbHN2aW4vcmVkLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjYWxzdmluLWJsdWUnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjYWxzdmluJykucmVlbCgnaW1hZ2VzJywgJ2ltYWdlcy8zNjAvYWxzdmluL2JsdWUvIyMjIy5wbmcnKTtcclxufSk7XHJcbiQoJyNhbHN2aW4tZ3JleScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNhbHN2aW4nKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9hbHN2aW4vZ3JleS8jIyMjLnBuZycpO1xyXG59KTtcclxuJCgnI2Fsc3Zpbi1ibGFjaycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNhbHN2aW4nKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9hbHN2aW4vYmxhY2svIyMjIy5wbmcnKTtcclxufSk7XHJcblxyXG4vLyBjczM1XHJcbiQoJyNjczM1LXdoaXRlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzMzUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczM1L3doaXRlLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3MzNS1yZWQnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvcmVkLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3MzNS1ibHVlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzMzUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczM1L2JsdWUvIyMjIy5wbmcnKTtcclxufSk7XHJcbiQoJyNjczM1LWdyZXknKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvZ3JleS8jIyMjLnBuZycpO1xyXG59KTtcclxuJCgnI2NzMzUtYnJvd24nKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3MzNScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzMzUvYnJvd24vIyMjIy5wbmcnKTtcclxufSk7XHJcblxyXG5cclxuLy8gY3M4NVxyXG5cclxuXHJcbiQoJyNjczg1LXdoaXRlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzODUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczg1L3doaXRlLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3M4NS1yZWQnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3M4NScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzODUvcmVkLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3M4NS1wdXJwbGUnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3M4NScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzODUvcHVycGxlLyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3M4NS1ibHVlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzODUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczg1L2JsdWUvIyMjIy5wbmcnKTtcclxufSk7XHJcbiQoJyNjczg1LWdyZXknKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3M4NScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzODUvZ3JleS8jIyMjLnBuZycpO1xyXG59KTtcclxuJCgnI2NzODUtYmxhY2snKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3M4NScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzODUvYmxhY2svIyMjIy5wbmcnKTtcclxufSk7XHJcbiQoJyNjczg1LWJsYWNrJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzODUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczg1L2JsYWNrLyMjIyMucG5nJyk7XHJcbn0pO1xyXG5cclxuXHJcblxyXG4vLyBjczk1XHJcbiQoJyNjczk1LXdoaXRlJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2NzOTUnKS5yZWVsKCdpbWFnZXMnLCAnaW1hZ2VzLzM2MC9jczk1L3doaXRlLyMjIyMucG5nJyk7XHJcbn0pO1xyXG5cclxuJCgnI2NzOTUtZ3JleScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNjczk1JykucmVlbCgnaW1hZ2VzJywgJ2ltYWdlcy8zNjAvY3M5NS9ncmV5LyMjIyMucG5nJyk7XHJcbn0pO1xyXG4kKCcjY3M5NS1ibGFjaycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICQoJyNjczk1JykucmVlbCgnaW1hZ2VzJywgJ2ltYWdlcy8zNjAvY3M5NS9ibGFjay8jIyMjLnBuZycpO1xyXG59KTtcclxuJCgnI2NzOTUtYnJvd24nKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjY3M5NScpLnJlZWwoJ2ltYWdlcycsICdpbWFnZXMvMzYwL2NzOTUvYnJvd24vIyMjIy5wbmcnKTtcclxufSk7XHJcblxyXG5cclxuXHJcbiQucmVlbC5kZWYuaW5kaWNhdG9yID0gNTsiXX0=
