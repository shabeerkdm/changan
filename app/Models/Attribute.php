<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public function Group()
    {
        return $this->belongsTo(AttributeGroup::class);
    }
}
