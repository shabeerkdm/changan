<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function Customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function Grade()
    {
        return $this->belongsTo(Grade::class);
    }
}
