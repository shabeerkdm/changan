<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FieldPage extends Model
{
    protected $table = 'field_page';
}
