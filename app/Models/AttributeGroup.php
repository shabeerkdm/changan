<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    public function Atts()
    {
        return $this->hasMany(Attribute::class);
    }
}
