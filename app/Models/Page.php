<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;

    public function fields()
    {
        return $this->belongsToMany(Field::class)->withPivot('name', 'value', 'class', 'label', 'validation', 'options');
    }
}
