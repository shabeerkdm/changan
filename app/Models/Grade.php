<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as MModel;

class Grade extends MModel
{

    public function Model()
    {
        return $this->belongsTo(Model::class);
    }

    public function Clrs()
    {
        return $this->hasMany(Color::class, 'model_id', 'model_id');
    }
}
