<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $hidden = ['model_id','updated_at','created_at'];
}
