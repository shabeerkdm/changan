<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as MModel;

class Model extends MModel
{
    public function Specs()
    {
        return $this->hasMany(Specification::class);
    }

    public function Atts()
    {
        return $this->hasMany(Attribute::class);
    }

    public function Clrs()
    {
        return $this->hasMany(Color::class);
    }

    public function AttGroups()
    {
        return $this->hasMany(AttributeGroup::class);
    }

    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }
}
