<?php

function admin_js($path)
{
    return url('/') . "/resources/js/admin/" . $path;
}
function user_js($path)
{
    return url('/') . "/resources/js/user/" . $path;
}

function storage($path)
{
    return url('/') . "/storage/app/" . $path;
}

function OrderID($order_id, $length = 30)
{
    $first = 'C';
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $first . $order_id . $randomString;
}

function generateOTP($length = 30)
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
