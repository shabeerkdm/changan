<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('layouts.menu', 'App\Http\View\Composers\MenuComposer');
        View::composer('layouts.footer', 'App\Http\View\Composers\FooterComposer');
        View::composer('layouts.contact', 'App\Http\View\Composers\ContactComposer');
    }
}
