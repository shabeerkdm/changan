<?php

namespace App\Http\Controllers;

use App\Http\Repositories\SeoRepository;
use App\Http\Repositories\SiteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{

    public function __construct(SiteRepository $siteRepository, SeoRepository $seoRepository)
    {
        $this->site_repo = $siteRepository;
        $this->seo_repo = $seoRepository;
    }

    public function account()
    {
        return view('auth.account', ['customer' => $this->site_repo->getCustomer(Auth::user()->id)]);
    }

    public function customer(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$this->site_repo->updateCustomer($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "", 'error' => 0]);
        }
    }

    public function user(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'password' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$this->site_repo->updateUser($request, Auth::user()->id)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "Password changed successfully", 'error' => 0]);
        }
    }

    public function page($slug = 'home')
    {
        $news = '';
        $models = '';
        if ($slug == 'home') {
            $news = $this->site_repo->getRecentPost();
            $models = $this->site_repo->getModels();
        }
        $page = $this->site_repo->getPage($slug);
        $seo = $this->seo_repo->getSeo($slug, 1);
        return view($page->type . '.' . $slug, ['page' => $page, 'news' => $news, 'models' => $models, 'seo' => $seo]);
    }

    public function show($slug)
    {
        return view('pages.vehicle', [
            'model' => $this->site_repo->getModel($slug),
            'models' => $this->site_repo->getModels(),
            'grades' => $this->site_repo->getGrades($slug),
            'specs' => $this->site_repo->getSpecs($slug),
            'gallery' => $this->site_repo->getImages($slug),
            'seo' => $this->seo_repo->getSeo($slug, 2)
        ]);
    }

    public function offers()
    {
        return view('pages.offers', [
            'offers' => $this->site_repo->getOffers(),
            'models' => $this->site_repo->getModels(),
            'seo' => $this->seo_repo->getSeo('offers', 1)
        ]);
    }

    public function offer($slug)
    {
        return view('pages.offer', [
            'offer' => $this->site_repo->getOffer($slug),
            'models' => $this->site_repo->getModels(),
            'locations' => $this->site_repo->getLocations(),
            'seo' => $this->seo_repo->getSeo($slug, 4)
        ]);
    }

    public function getOffer(Request $request)
    {
        return response()->json($this->site_repo->getOfferByID($request->id));
    }

    public function news()
    {
        return view('pages.news', ['posts' => $this->site_repo->getPosts(), 'seo' => $this->seo_repo->getSeo('news', 1)]);
    }

    public function post($slug)
    {
        return view('pages.post', [
            'post' => $this->site_repo->getPost($slug),
            'recent_posts' => $this->site_repo->getRecentPost(),
            'seo' => $this->seo_repo->getSeo($slug, 3)
        ]);
    }

    public function loadPosts(Request $request)
    {
        return response()->json($this->site_repo->loadPosts($request));
    }

    public function contact()
    {
        return view('pages.contact', [
            'contacts' => $this->site_repo->getLocations(),
            'models' => $this->site_repo->getModels(),
            'options' => $this->site_repo->getOptions(),
            'seo' => $this->seo_repo->getSeo('contact', 1)
        ]);
    }

    public function location()
    {
        return view('static.location', [
            'contacts' => $this->site_repo->getLocations(),
            'seo' => $this->seo_repo->getSeo('findlocation', 1)
        ]);
    }

    public function compare($type = 'models', $slug = '')
    {   
        $models = [];
        $data = [];
        if ($type == 'grades') {
            $data = $this->site_repo->getGrades($slug);
        } else if ($type == 'models') {
            if ($slug != '') {
                $data = $this->site_repo->getGrades($slug);
            }
            $models = $this->site_repo->getModels();
        }
        return view('pages.compare', [
            'groups' => $this->site_repo->getGroups(),
            'type' => $type,
            'models' => $models,
            'data' => $data,
            'seo' => $this->seo_repo->getSeo($slug ? $slug : 'compare', 1)
        ]);
    }

    public function list()
    {
        return view('pages.car-online', [
            'models' => $this->site_repo->getModels(),
            'grades' => $this->site_repo->getGrades(),
            'range' => $this->site_repo->getRange(),
            'seo' => $this->seo_repo->getSeo('car-online', 1)
        ]);
    }

    public function buy(Request $request, $slug)
    {
        $grade_id = $request->has('grade') ? $request->get('grade') : 0;
        $grade = [];
        if ($grade_id == 0) {
            $grades = $this->site_repo->getGrades($slug);
            $grade = $grades[0];
        } else {
            $grade = $this->site_repo->getGrade($grade_id);
        }
        $groups = $this->site_repo->getGroups($grade_id, $slug);
        return view('pages.buyonline', ['grade' => $grade, 'groups' => $groups, 'seo' => $this->seo_repo->getSeo('buy-online', 1)]);
    }

    public function reserve(Request $request)
    {
        if ($request->method() == 'GET') {
            return redirect(url('/'));
        }
        $grade = $this->site_repo->getGrade($request->grade_id);
        $color = $this->site_repo->getColor($request->color_id);
        return view('pages.reserve', ['grade' => $grade, 'color' => $color, 'seo' => $this->seo_repo->getSeo('reserve', 1)]);
    }

    public function testDrive(Request $request)
    {
        $model_id = '';
        $seo = '';
        // if ($request->has('grade')) {
        //     $grade = $this->site_repo->getGrade($request->get('grade'));
        //     $model_id = $grade->model_id;
        // }
        if ($request->has('model')) {
            $model = $this->site_repo->getModel($request->get('model'));
            $model_id = $model->id;
            $seo = $this->seo_repo->getSeo($model->slug, 1);
        }else{
            $seo = $this->seo_repo->getSeo('book-test-drive', 1);
        }

        return view('static.booktestdrive', [
            'model_id' => $model_id,
            'models' => $this->site_repo->getModels(),
            'locations' => $this->site_repo->getLocations(),
            'seo' => $seo
        ]);
    }

    public function enquire(Request $request)
    {
        $model_id = '';
        if ($request->has('grade')) {
            $grade = $this->site_repo->getGrade($request->get('grade'));
            $model_id = $grade->model_id;
        }
        if ($request->has('model')) {
            $model = $this->site_repo->getModel($request->get('model'));
            $model_id = $model->id;
        }

        return view('static.enquire', [
            'model_id' => $model_id,
            'models' => $this->site_repo->getModels(),
            'locations' => $this->site_repo->getLocations(),
            'seo' => $this->seo_repo->getSeo('enquire', 1)
        ]);
    }

    public function service()
    {
        return view('pages.service-booking', [
            'models' => $this->site_repo->getAllModels(),
            'locations' => $this->site_repo->getLocations(),
            'seo' => $this->seo_repo->getSeo('service-booking', 1)
        ]);
    }

    public function saveService(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'fname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'plate_number' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$this->site_repo->saveService($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "Service created", 'error' => 0]);
        }
    }

    public function tracking()
    {
        return view('pages.tracking',['seo' => $this->seo_repo->getSeo('tracking', 1)]);
    }

    public function getTracking(Request $request)
    {
        if ($data = $this->site_repo->tracking($request)) {
            return response()->json(['status' => true, 'data' => $data, 'error' => 0]);
        } else {
            return response()->json(['status' => false,  'message' => "No service detailes found for this vehicle!", 'error' => 1]);
        }
    }

    public function getAtts(Request $request)
    {
        return response()->json($this->site_repo->getAtts($request->id));
    }

    public function getGrades(Request $request)
    {
        return response()->json($this->site_repo->getGrades($request->slug));
    }

    public function lead(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$this->site_repo->saveLead($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "Lead created", 'error' => 0]);
        }
    }

    public function createOrder(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'password' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->site_repo->saveOrder($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data,  'message' => "Order created", 'error' => 0]);
        }
    }

    public function payment(Request $request, $status)
    {
        if ($order_id = $request->get('order_id')) {
            $order = $this->site_repo->getOrder($order_id);
            if ($status == 'success') {
                $_status = $this->site_repo->checkOrder($order_id);
                if ($_status['code'] == 3)
                    $status = 'success';
                else
                    return view('static.error', ['status' => $status,'seo'=>'']);
            }
            return view('static.' . $status, ['order' => $order,'seo'=>'']);
        } else {
            return redirect('/');
        }
    }

    public function sendOtp(Request $request)
    {
        $request = $request->only('phone');
        if (!$this->site_repo->sendOtp($request['phone'])) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "OTP sent successfully", 'error' => 0]);
        }
    }

    public function verifyOtp(Request $request)
    {
        $request = $request->only('phone', 'otp');
        if (!$this->site_repo->checkOtp($request)) {
            return response()->json(['status' => false, 'message' => 'OTP is not matching', 'error' => 1]);
        } else {
            return response()->json(['status' => true,  'message' => "OTP verified successfully", 'error' => 0]);
        }
    }
}
