<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\VehicleRepository;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    private $vehicle_repo;

    public function __construct(VehicleRepository $vehicleRepository)
    {
        $this->vehicle_repo = $vehicleRepository;
    }

    public function index()
    {
        return view('admin.models.index', ['models' => $this->vehicle_repo->getModelsList()]);
    }

    public function create()
    {
        return view('admin.models.model', [
            'sliders' => $this->vehicle_repo->getSlidersList(),
            'att_groups' => $this->vehicle_repo->getAttGroupsList(),
            'model' => ''
        ]);
    }

    public function edit($id)
    {
        return view('admin.models.model', [
            'sliders' => $this->vehicle_repo->getSlidersList(),
            'att_groups' => $this->vehicle_repo->getAttGroupsList(),
            'grades' => $this->vehicle_repo->getGradesList($id),
            'model' => $this->vehicle_repo->getModel($id)
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'name' => 'required',
            'slider_id' => 'required',
            'slug' => 'required',
            'title' => 'required',
            'slogan' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$id = $this->vehicle_repo->storeVehicle($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $id, 'message' =>   "Model created successfully", 'error' => 0]);
        }
    }

    public function update(Request $request, $id)
    {
        $validatedData = \Validator::make($request->all(), [
            'name' => 'required',
            'slider_id' => 'required',
            'slug' => 'required',
            'title' => 'required',
            'slogan' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$this->vehicle_repo->updateVehicle($request, $id)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'message' =>   "Model updated successfully", 'error' => 0]);
        }
    }

    public function addGrade(Request $request)
    {
        if (!$this->vehicle_repo->storeGrade($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            $message = $request->has('grade_id') ? 'Updated':'Created';
            return response()->json(['status' => true, 'message' =>   "Grade $message successfully", 'error' => 0]);
        }
    }

    public function getGrades($id)
    {
        return response()->json($this->vehicle_repo->getGradesList($id));
    }

    public function getGrade($id)
    {
        return response()->json($this->vehicle_repo->getGrade($id));
    }

    public function getModel($id)
    {
        $model = $this->vehicle_repo->getModelDetails($id);
        return response()->json($model);
    }
}
