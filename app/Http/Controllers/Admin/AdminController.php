<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\AdminRepository;
use App\Http\Repositories\SeoRepository;
use App\Http\Repositories\VehicleRepository;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct(AdminRepository $adminRepository, VehicleRepository $vehicleRepository, SeoRepository $seoRepository)
    {
        $this->admin_repo = $adminRepository;
        $this->vehicle_repo = $vehicleRepository;
        $this->seo_repo = $seoRepository;
    }

    public function dashboard()
    {
        $counts = $this->admin_repo->getCounts();
        $recent_leads = $this->admin_repo->getRecentLeads();
        $recent_services = $this->admin_repo->getRecentServices();
        $recent_orders = $this->admin_repo->getRecentOrders();
        return view('admin.dashboard', ['counts' => $counts, 'leads' => $recent_leads, 'services' => $recent_services, 'orders' => $recent_orders]);
    }

    public function leads(Request $request)
    {
        $filters = ['contact-us' => 'Contact Us', 'book-test-drive' => 'Book Test Drive', 'enquire' => 'Enquire', 'contact-page' => 'Contact Page','offer-page' => 'Offer Page'];
        return view('admin.leads.index', ['leads' => $this->admin_repo->getLeads($request), 'filters' => $filters, 'filter' => $request->get('filter')]);
    }

    public function lead($id)
    {
        return response()->json($this->admin_repo->getLead($id));
    }

    public function services(Request $request)
    {
        $search = $request->get('search');
        return view('admin.services.index', ['services' => $this->admin_repo->getServices($search), 'search' => $search]);
    }
    public function service($id)
    {
        return response()->json($this->admin_repo->getService($id));
    }
    public function serviceStatus(Request $request, $id)
    {
        return response()->json($this->admin_repo->serviceStatus($request, $id));
    }

    public function orders()
    {
        return view('admin.orders.index', ['orders' => $this->admin_repo->getOrders()]);
    }

    public function order($id)
    {
        return response()->json($this->admin_repo->getOrder($id));
    }
    public function orderStatus(Request $request, $id)
    {
        return response()->json($this->admin_repo->orderStatus($request, $id));
    }

    public function page($slug)
    {
        return view('admin.pages.page', ['page' => $this->admin_repo->getPage($slug)]);
    }

    public function savePage(Request $request)
    {

        if (!$data = $this->admin_repo->savePage($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Successfully updated", 'error' => 0]);
        }
    }

    public function sliders()
    {
        $sliders = $this->admin_repo->getSliders();
        return view('admin.sliders.index', ['sliders' => $sliders]);
    }

    public function getSliders()
    {
        if ($data = $this->admin_repo->getSlidersList()) {
            return response()->json(['status' => true, 'data' => $data, 'error' => 0]);
        } else {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        }
    }

    public function getSlides($id)
    {
        if ($data = $this->admin_repo->getSlides($id)) {
            return response()->json(['status' => true, 'data' => $data, 'error' => 0]);
        } else {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        }
    }

    public function saveSlider(Request $request)
    {

        $validatedData = \Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->saveSlider($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Successfully updated", 'error' => 0]);
        }
    }

    public function saveSlide(Request $request)
    {

        $validatedData = \Validator::make($request->all(), [
            'slider_id' => 'required',
            'image' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->saveSlide($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Successfully updated", 'error' => 0]);
        }
    }

    public function posts()
    {
        return view('admin.posts.index', ['posts' => $this->admin_repo->getPosts()]);
    }

    public function postCreate()
    {
        return view('admin.posts.post', ['post' => '']);
    }

    public function postEdit($id)
    {
        return view('admin.posts.post', ['post' => $this->admin_repo->getPost($id)]);
    }

    public function settings()
    {
        return view('admin.settings.settings', ['options' => $this->admin_repo->getOptions()]);
    }

    public function savePost(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
            'body' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->savePost($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "News created", 'error' => 0]);
        }
    }

    public function updatePost(Request $request, $id)
    {
        $validatedData = \Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'body' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->updatePost($request, $id)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "News updated", 'error' => 0]);
        }
    }

    public function offers()
    {
        return view('admin.offers.index', ['offers' => $this->admin_repo->getOffers()]);
    }

    public function offerCreate()
    {
        return view('admin.offers.offer', [
            'offer' => '',
            'models' => $this->vehicle_repo->getModelsList(),
            'grades' => []
        ]);
    }

    public function offerEdit($id)
    {
        $offer = $this->admin_repo->getOffer($id);
        return view('admin.offers.offer', [
            'offer' => $offer,
            'models' => $this->vehicle_repo->getModelsList(),
            'grades' => $this->vehicle_repo->getGradesList($offer->model_id)
        ]);
    }

    public function saveOffer(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'required',
            'body' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->saveOffer($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Offer created", 'error' => 0]);
        }
    }

    public function updateOffer(Request $request, $id)
    {
        $validatedData = \Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->updateOffer($request, $id)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Offer updated", 'error' => 0]);
        }
    }

    public function contact()
    {
        return view('admin.contact.index', ['contacts' => $this->admin_repo->getContacts()]);
    }

    public function saveContact(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'company' => 'required',
            'location' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->saveContact($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            $message = $request->has('contact_id') ? 'updated' : 'created';
            return response()->json(['status' => true, 'data' => $data, 'message' => "Contact " . $message, 'error' => 0]);
        }
    }


    public function getImages(Request $request)
    {
        return response()->json($this->admin_repo->getImagesList($request));
    }

    public function saveImage(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'section_id' => 'required',
            'image' => 'required'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->admin_repo->saveImage($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Image uploaded successfully", 'error' => 0]);
        }
    }

    public function options(Request $request)
    {
        if (!$this->admin_repo->saveOptions($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'message' => "Settings updated successfully", 'error' => 0]);
        }
    }

    public function delete(Request $request)
    {
        if (!$this->admin_repo->delete($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'message' => "Successfully delete", 'error' => 0]);
        }
    }

    public function seo()
    {
        return view('admin.settings.seo', ['seo' => '', 'seos' => $this->seo_repo->getSeos()]);
    }

    public function saveSeo(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'slug' => 'required|unique:seos,slug'
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->seo_repo->saveSeo($request)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Seo saved", 'error' => 0]);
        }
    }

    public function editSeo($id)
    {
        $seo = $this->seo_repo->getSeo($id);
        return view('admin.settings.seo', ['seo' => $seo, 'seos' => $this->seo_repo->getSeos()]);
    }

    public function updateSeo(Request $request, $id)
    {
        $validatedData = \Validator::make($request->all(), [
            'slug' => 'required|unique:seos,slug,' . $id,
        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if (!$data = $this->seo_repo->updateSeo($request, $id)) {
            return response()->json(['status' => false, 'message' => 'Technical error! Please try again', 'error' => 1]);
        } else {
            return response()->json(['status' => true, 'data' => $data, 'message' => "Seo updated", 'error' => 0]);
        }
    }
}
