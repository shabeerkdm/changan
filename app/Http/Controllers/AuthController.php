<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Repositories\AuthRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{

    private $auth_repo;

    public function __construct(AuthRepository $authRepository)
    {
        $this->middleware('guest', ['except' => ['logout']]);
        $this->auth_repo = $authRepository;
    }

    public function index()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => 'Invalid Credentials', 'error' => 0]);
        }

        if (!$this->auth_repo->checkEmail($credentials['email']))
            return response()->json(['status' => false, 'message' => 'Invalid Credentials', 'error' => 1]);
        if (!$this->auth_repo->isActiveUser($credentials['email'])) {
            return response()->json(['status' => false, 'message' => 'Inactive User', 'error' => 1]);
        }
        if (Auth::attempt($credentials)) {
            Auth::logoutOtherDevices($credentials['password']);
            $url = Auth::user()->role()->first()->role == 'USER' ? 'account' : 'admin/dashboard';
            return response()->json(['status' => true, 'url' => $url, 'message' => 'User authenticated', 'error' => 0]);
        } else {
            return response()->json(['status' => false, 'message' => 'Invalid Credentials', 'error' => 1]);
        }
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'email' => 'required|email',

        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if ($this->auth_repo->checkEmail($request->email)) {
            $response = Password::broker()->sendResetLink($request->only('email'));
            if ($response == Password::RESET_LINK_SENT) {
                return response()->json(['status' => true, 'message' => $response, 'error' => 0]);
            } else {
                return response()->json(['status' => false, 'message' => $response, 'error' => 0]);
            }
        } else {
            return response()->json(['status' => false, 'error' => 1, 'message' => 'The email you entered does not exist!']);
        }
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'token' => 'required'

        ]);
        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }
        if ($this->auth_repo->checkEmail($request->email)) {
            $response = Password::broker()->reset(
                $request->only('email', 'password', 'password_confirmation', 'token'),
                function ($user, $password) {
                    $user->password = Hash::make($password);
                    $user->save();
                }
            );
            if ($response == Password::PASSWORD_RESET) {
                return response()->json(['status' => true, 'message' => $response, 'error' => 0]);
            } else {
                return response()->json(['status' => false, 'message' => $response, 'error' => 0]);
            }
        } else {
            return response()->json(['status' => false, 'error' => 1, 'message' => 'The email you entered does not exist!']);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
