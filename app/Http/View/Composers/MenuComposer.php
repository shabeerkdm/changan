<?php

namespace App\Http\View\Composers;

use App\Http\Repositories\SiteRepository;
use Illuminate\View\View;

class MenuComposer
{

    protected $models;

    public function __construct(SiteRepository $models)
    {
        // Dependencies automatically resolved by service container...
        $this->models = $models;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('models', $this->models->getModels());
    }
}