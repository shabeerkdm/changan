<?php

namespace App\Http\View\Composers;

use App\Http\Repositories\SiteRepository;
use Illuminate\View\View;

class FooterComposer
{

    protected $options;

    public function __construct(SiteRepository $options)
    {
        $this->options = $options;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('options', $this->options->getOptions());
    }
}
