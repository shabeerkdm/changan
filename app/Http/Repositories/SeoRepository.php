<?php


namespace App\Http\Repositories;

use App\Models\Seo;

class SeoRepository
{

    public function getSeos()
    {
        try {
            return Seo::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSeo($id, $type = '')
    {
        try {
            $query = Seo::where('id', $id)
                ->orWhere('slug', $id);
            if ($type != '') {
                $query->where('type', $type);
            }
            return $query->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveSeo($request)
    {
        try {
            $seo = new Seo();
            $seo->page = $request->page;
            $seo->slug = $request->slug;
            $seo->type = $request->type;
            $seo->title = $request->title;
            $seo->description = $request->description;
            $seo->key_words = $request->key_words;
            $seo->save();
            return $seo->id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateSeo($request, $id)
    {
        try {
            $seo = Seo::findOrFail($id);
            $seo->page = $request->page;
            $seo->slug = $request->slug;
            $seo->type = $request->type;
            $seo->title = $request->title;
            $seo->description = $request->description;
            $seo->key_words = $request->key_words;
            $seo->save();
            return $seo->id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
