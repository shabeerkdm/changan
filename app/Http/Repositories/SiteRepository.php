<?php

namespace App\Http\Repositories;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\Color;
use App\Models\Contact;
use App\Models\Customer;
use App\Models\FieldPage;
use App\Models\Grade;
use App\Models\Image;
use App\Models\Lead;
use App\Models\Model;
use App\Models\Offer;
use App\Models\Option;
use App\Models\Order;
use App\Models\Page;
use App\Models\Phone;
use App\Models\Post;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Specification;
use App\Models\User;
use App\Notifications\AccountCreated;
use App\Notifications\Contact as NotificationsContact;
use App\Traits\SmsTrait;
use Notification;

class SiteRepository
{

    use SmsTrait;

    public function getPage($slug)
    {
        try {
            $query = Page::with('fields')
                ->where('slug', $slug);
            $page = $query->first();
            $slider_id = FieldPage::where(['page_id' => $page->id, 'name' => 'slider_id'])->first();
            if ($slider_id && $slider_id->value)
                $page->slider = SLider::findOrFail($slider_id->value);
            else
                $page->slider = [];
            return $page;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getModels()
    {
        try {
            return Model::select('id', 'name', 'image', 'slug', 'category')->where('engine', '!=', 'false')->where('active', 1)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getAllModels()
    {
        try {
            $collection = Model::select('id', 'name', 'image', 'slug')->where('active', 1)->get();
            $ids = [1, 5, 2, 6, 7, 3, 4];

            $sorted = $collection->sortBy(function ($model) use ($ids) {
                return array_search($model->getKey(), $ids);
            });
            return $sorted;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }


    public function getModel($slug)
    {
        try {
            return Model::where('slug', $slug)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getGrades($slug = '')
    {
        try {
            $query = Grade::select(
                'grades.*',
                'models.slug as slug',
                'models.name as model_name'
            )
                ->leftJoin('models', 'models.id', '=', 'grades.model_id')
                ->where(['models.active' => 1, 'grades.stock' => 1])
                ->with('Clrs');
            if ($slug)
                $query->where('slug', $slug);
            $grades = $query->get();
            return $grades;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function getGrade($id)
    {
        try {
            $grade = Grade::select(
                'grades.*',
                'models.slug as slug',
                'models.name as model_name'
            )
                ->leftJoin('models', 'models.id', '=', 'grades.model_id')
                ->with('Clrs')
                ->where('grades.id', $id)->first();
            return $grade;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getRange()
    {
        try {
            return Grade::selectRaw('MIN(price) as min, MAX(price) as max')->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSpecs($slug)
    {
        try {
            $model = Model::where('slug', $slug)->first();
            $specs['power'] = Specification::select('title', 'body', 'image')
                ->where(['model_id' => $model->id, 'type' => 'power'])
                ->get();
            $specs['exterior'] = Specification::select('title', 'body', 'image')
                ->where(['model_id' => $model->id, 'type' => 'exterior'])
                ->get();
            $specs['interior'] = Specification::select('title', 'body', 'image')
                ->where(['model_id' => $model->id, 'type' => 'interior'])
                ->get();
            $specs['safety'] = Specification::select('title', 'body', 'image')
                ->where(['model_id' => $model->id, 'type' => 'safety'])
                ->get();
            return $specs;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getImages($slug)
    {
        try {
            $model = Model::where('slug', $slug)->first();
            $images = Image::where(['section_id' => 2, 'reference_id' => $model->id])->get();
            return $images;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOffers()
    {
        try {
            return Offer::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOffer($slug)
    {
        try {
            return Offer::where('slug', $slug)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOfferByID($id)
    {
        try {
            return Offer::select(
                'offers.*',
                'models.slug as m_slug',
                'models.name as m_name'
            )
                ->leftJoin('models', 'models.id', '=', 'offers.model_id')
                ->where('offers.id', $id)
                ->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getPosts()
    {
        try {
            return Post::latest()->take(3)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getPost($slug)
    {
        try {
            return Post::where('slug', $slug)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getRecentPost()
    {
        try {
            return Post::take(3)->latest()->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function loadPosts($request)
    {
        try {
            $limit = $request->limit;
            return Post::latest()->skip($limit)->take(3)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getLocations()
    {
        try {
            return Contact::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getGroups($grade = false, $slug = false)
    {
        try {
            if (!$grade && !$slug) {
                return AttributeGroup::all();
            } else if ($slug && $grade) {
                $model_id = Model::where('slug', $slug)->first()->id;
                return AttributeGroup::with(['Atts' => function ($q) use ($grade, $model_id) {
                    $q->where('model_id', $model_id);
                    $q->where('grade_id', $grade);
                }])->get();
            } else {
                return AttributeGroup::with(['Atts' => function ($q) use ($grade) {
                    $q->where('grade_id', $grade);
                }])->get();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getAtts($id)
    {
        try {
            $grade = $this->getGrade($id);
            if ($id) {
                $query = Attribute::select(
                    'attribute',
                    'value',
                    'attribute_groups.id as group_id'
                );
                $query->leftJoin('attribute_groups', 'attribute_groups.id', '=', 'attributes.attribute_group_id');
                $query->where('grade_id', $id);
                return ['grade' => $grade, 'atts' => $query->get()];
            } else {
                return [];
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getColor($id)
    {
        try {
            return Color::findOrFail($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveLead($request)
    {
        try {
            $lead = Lead::where(['email' => $request->email = $request->email, 'phone' => $request->phone])->first();
            if ($lead == '')
                $lead = new Lead();
            $lead->source = $request->source;
            $lead->fname = $request->fname;
            $lead->lname = $request->lname;
            $lead->name = $request->has('name') ? $request->name : $request->fname . ' ' . $request->lname;
            $lead->type = $request->type;
            $lead->contact_id = $request->contact_id;
            $lead->address = $request->address;
            $lead->phone = $request->phone;
            $lead->email = $request->email;
            $lead->subject = $request->subject;
            $lead->message = $request->message;
            if ($request->has('date'))
                $lead->date = date('Y-m-d', strtotime($request->date));
            if ($request->has('time'))
                $lead->time = date('H:m:i', strtotime($request->time));
            $lead->model_id = $request->model_id;
            $lead->is_subscribe = $request->is_subscribe ? 1 : 0;
            $lead->save();
            $details = [
                'greeting' => 'Hi ' . $request->fname . ' ' . $request->lname,
                'body' => 'ThankYou for showing interest in changan, our representative will contact you soon...',
            ];
            $model = Model::select('name')->where('id', $request->model_id)->first();
            Notification::route('mail', $request->email)
                ->notify(new NotificationsContact($details));
            $details = [
                'source' => $request->source,
                'fname' => $request->has('name') ? $request->name : $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'subject' => $request->subject,
                'message' => $request->message,
                'model' => $model->name
            ];
            Notification::route('mail', ['unionmotors-dubaihq@eskimosolutions.com' => 'Union Motors', 'info@changan.ae' => 'Info'])
                ->notify(new NotificationsContact($details, true));
            return $this->sentToAMS($lead);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function sentToAMS($lead)
    {
        try {
            $model = '';
            if ($lead->model_id)
                $model = Model::findOrFail($lead->model_id)->name;
            if ($lead->fname == null) {
                $lead->fname = $lead->name;
                $lead->lname = $lead->name;
            }
            $params = array(
                'SystemType'  => 'Website',
                'RefId'   => $lead->id,
                'DealerIdentifier' => env('AMS_DEALER_ID'),
                'DealerName'    => env('AMS_DEALER_NAME'),
                'Customer'    => array(
                    'FirstName' => $lead->fname,
                    'LastName' => $lead->lname,
                    'Phone' => $lead->phone,
                    'Email' => $lead->email,
                ),
                "Make" => "Changan",
                "Model" => $model,
                "Notes" => $lead->source,
                "Origin" => "Dealer Website"
            );
            $postData = json_encode($params);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, env('AMS_SERVICE_URL', "https://leads-staging.amsfusionsd.com/api/generic/processlead/"));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Accept:application/json'));
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD,  env('AMS_USERNAME', 'amstest') . ":" . env('AMS_PASSWORD', 'Ams#2018'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            $results = curl_exec($ch);
            curl_close($ch);
            list($header, $body) = explode("\r\n\r\n", $results, 2);
            return $body;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveService($request)
    {
        try {
            $service = Service::where(['email' => $request->email = $request->email, 'phone' => $request->phone])->first();
            if ($service == '')
                $service = new Service();
            $service->fname = $request->fname;
            $service->lname = $request->lname;
            $service->email = $request->email;
            $service->phone = $request->phone;
            $service->types = $request->types;
            $service->date = date('Y-m-d', strtotime($request->date));
            $service->plate_number = $request->plate_number;
            $service->comments = $request->comments;
            $service->status = 'Processing...';
            $service->model_id = $request->model_id;
            $service->contact_id = $request->contact_id;
            return $service->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function tracking($request)
    {
        try {
            $service = Service::select('status')->where(['plate_number' => $request->plate_number, 'phone' => $request->phone])->first();
            if ($service)
                return $service->status;
            else
                return false;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOptions()
    {
        try {
            $_options = Option::all()->toArray();
            $options = [];
            foreach ($_options as $option) {
                $options[$option['option']] = $option['value'];
            }
            return $options;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveOrder($request)
    {
        try {
            $customer = Customer::firstOrNew(['email' => $request->email, 'contact' => $request->phone]);
            $customer->fname = $request->fname;
            $customer->lname = $request->lname;
            $customer->address_1 = $request->address_1;
            $customer->address_2 = $request->address_2;
            $customer->password = $request->password;
            $customer->save();
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->grade_id = $request->grade_id;
            $order->color_id = $request->color_id;
            $order->save();
            $order_id = OrderID($order->id, 7);
            Order::where('id', $order->id)->update(['order_id' => $order_id]);
            return $this->createOrder($order_id, $order);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getCustomer($user_id)
    {
        try {
            return Customer::where('user_id', $user_id)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateCustomer($request)
    {
        try {
            $customer =  Customer::where('id', $request->customer_id)->first();
            $customer->fname = $request->fname;
            $customer->lname = $request->lname;
            $customer->email = $request->email;
            $customer->contact = $request->phone;
            return $customer->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateUser($request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->password = bcrypt($request->password);
            return $user->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }


    public function getOrder($order_id)
    {
        try {
            $order = Order::where('order_id', $order_id)->first();
            return $order;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function checkOrder($order_id)
    {
        try {
            $order = $this->getOrder($order_id);
            return $this->getStatus($order->reference_id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function createOrder($order_id, $order)
    {
        try {
            $params = array(
                'ivp_method'  => 'create',
                'ivp_store'   => env('TELR_STORE_ID'),
                'ivp_authkey' => env('TELR_KEY'),
                'ivp_cart'    => $order_id,
                'ivp_test'    => '0',
                'ivp_amount'  => '1000.00',
                'ivp_currency' => 'AED',
                'ivp_desc'    => $order->grade->model->name . ' ' . $order->grade->name,
                'return_auth' => env('APP_URL') . 'payment/success?order_id=' . $order_id,
                'return_can'  => env('APP_URL') . 'payment/cancelled?order_id=' . $order_id,
                'return_decl' => env('APP_URL') . 'payment/declined?order_id=' . $order_id
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, env('TELR_URL', "https://secure.telr.com/gateway/order.json"));
            curl_setopt($ch, CURLOPT_POST, count($params));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            $results = curl_exec($ch);
            curl_close($ch);
            $results = json_decode($results, true);
            $ref = trim($results['order']['ref']);
            $url = trim($results['order']['url']);
            if (empty($ref) || empty($url)) {
                return false;
            } else {
                Order::where('order_id', $order_id)->update(['reference_id' => $ref]);
                return $url;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function getStatus($referens_id)
    {
        try {
            $params = array(
                'ivp_method'  => 'check',
                'ivp_store'   => env('TELR_STORE_ID'),
                'ivp_authkey' => env('TELR_KEY'),
                'order_ref' => $referens_id
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, env('TELR_URL', "https://secure.telr.com/gateway/order.json"));
            curl_setopt($ch, CURLOPT_POST, count($params));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            $results = curl_exec($ch);
            curl_close($ch);
            $results = json_decode($results, true);
            $status =  $results['order']['status'];
            if (empty($status)) {
                return false;
            } else {
                if ($status['code'] == 3) {
                    $this->orderPlaced($referens_id);
                }
                return $status;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function orderPlaced($referens_id)
    {
        try {
            $order = Order::where('reference_id', $referens_id)->first();
            $order->is_success = 1;
            $order->save();
            $customer = Customer::where('id', $order->customer_id)->first();
            $user =  User::firstOrNew(['email' => $customer->email]);
            if (!$user->exists) {
                $user->name = $customer->fname . ' ' . $customer->lname;
                $user->password = bcrypt($customer->password);
                $user->role_id = 3;
                $user->save();
                $customer->user_id = $user->id;
                $customer->save();
                $customer->notify(new AccountCreated($customer));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function sendOtp($_phone)
    {
        try {
            $otp = generateOTP(4);
            $phone = Phone::where('phone', $_phone)->first();
            if ($phone == '')
                $phone = new Phone();
            $phone->phone = $_phone;
            $phone->otp = $otp;
            $phone->is_verified = 0;
            $phone->save();
            $result = $this->to($_phone)
                ->message(Option::select('value')->where('option', 'otp_sms')->first()->value, array('OTP' => $otp))->response();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function checkOtp($request)
    {
        try {
            $phone = Phone::where(['phone' => $request['phone'], 'otp' => $request['otp']])->first();
            return $phone != '';
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
