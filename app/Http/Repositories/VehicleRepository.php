<?php

namespace App\Http\Repositories;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\Color;
use App\Models\Grade;
use App\Models\Model;
use App\Models\Slider;
use App\Models\Specification;
use Illuminate\Support\Facades\Storage;

class VehicleRepository
{
    public function getModelsList()
    {
        try {
            return Model::where('engine', '!=', 'false')->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getModel($id)
    {
        try {
            return Model::findOrFail($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSlidersList()
    {
        try {
            return Slider::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getAttGroupsList()
    {
        try {
            return AttributeGroup::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function storeVehicle($request)
    {
        try {

            $vehicle = new Model();
            $vehicle->name = $request->name;
            $vehicle->slider_id = $request->slider_id;
            $vehicle->slug = $request->slug;
            $vehicle->title = $request->title;
            $vehicle->slogan = $request->slogan;
            $vehicle->engine = $request->engine;
            $vehicle->transmission = $request->transmission;
            $vehicle->torque = $request->torque;
            $vehicle->active = $request->has('active') ? 1 : 0;
            $vehicle->category = $request->category;
            $vehicle->save();
            $vehicle_id = $vehicle->id;

            return $vehicle_id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateVehicle($request, $id)
    {
        try {
            $vehicle = Model::findOrFail($id);
            if ($request->file('video_bg')) {
                if ($vehicle->video_bg != '')
                    Storage::delete($vehicle->video_bg);
                $video_bg = $request->file('video_bg')->store('vehicle');
            } else {
                $video_bg = $vehicle->video_bg;
            }
            if ($request->file('gallery_bg')) {
                if ($vehicle->gallery_bg != '')
                    Storage::delete($vehicle->gallery_bg);
                $gallery_bg = $request->file('gallery_bg')->store('vehicle');
            } else {
                $gallery_bg = $vehicle->gallery_bg;
            }
            if ($request->file('image')) {
                if ($vehicle->image != '')
                    Storage::delete($vehicle->image);
                $image = $request->file('image')->store('vehicle');
            } else {
                $image = $vehicle->image;
            }
            if ($request->file('brochure')) {
                if ($vehicle->brochure != '')
                    Storage::delete($vehicle->brochure);
                $brochure = $request->file('brochure')->store('brochure');
            } else {
                $brochure = $vehicle->brochure;
            }
            $vehicle->name = $request->name;
            $vehicle->slider_id = $request->slider_id;
            $vehicle->slug = $request->slug;
            $vehicle->title = $request->title;
            $vehicle->slogan = $request->slogan;
            $vehicle->engine = $request->engine;
            $vehicle->transmission = $request->transmission;
            $vehicle->torque = $request->torque;
            $vehicle->video_bg = $video_bg;
            $vehicle->video_1 = $request->video_1;
            $vehicle->gallery_bg = $gallery_bg;
            $vehicle->image = $image;
            $vehicle->brochure = $brochure;
            $vehicle->view = $request->view;
            $vehicle->active = $request->has('active') ? 1 : 0;
            $vehicle->category = $request->category;
            $vehicle->save();
            $vehicle_id = $vehicle->id;

            $specs = json_decode($request->specs);
            if (!empty($specs)) {
                foreach ($specs as $x => $spec) {
                    $specification = $spec->id ? Specification::findOrFail($spec->id) : new Specification();
                    $image = '';
                    if ($request->file('spec_' . $x . '_image') != null) {
                        if ($spec->id && $specification->image != "")
                            Storage::delete($specification->image);
                        $filename = $request->file('spec_' . $x . '_image')->store('specs');
                        $image = $filename;
                    } else {
                        $image = $specification->image;
                    }
                    if ($spec->id && $spec->deleted)
                        Specification::findOrFail($spec->id)->delete();
                    else {
                        $specification->model_id = $vehicle_id;
                        $specification->type = $spec->type;
                        $specification->title = $spec->title;
                        $specification->body = $spec->body;
                        $specification->image = $image;
                        $specification->save();
                    }
                }
            }

            $atts = json_decode($request->atts);
            if (!empty($atts)) {
                foreach ($atts as $att) {
                    if ($att->id && $att->deleted)
                        Attribute::findOrFail($att->id)->delete();
                    else {
                        $attribute = $att->id ? Attribute::findOrFail($att->id) : new Attribute();
                        $attribute->attribute_group_id = $att->attribute_group_id;
                        $attribute->model_id = $vehicle_id;
                        $attribute->grade_id = $att->grade_id;
                        $attribute->attribute = $att->attribute;
                        $attribute->value = $att->value;
                        $attribute->save();
                    }
                }
            }

            $colors = json_decode($request->clrs);
            if (!empty($colors)) {
                foreach ($colors as $x => $clr) {
                    $color = $clr->id ? Color::findOrFail($clr->id) : new Color();
                    $image = '';
                    if ($request->file('clr_' . $x . '_image') != null) {
                        if ($clr->id && $color->image != "")
                            Storage::delete($color->image);
                        $filename = $request->file('clr_' . $x . '_image')->store('colors');
                        $image = $filename;
                    } else {
                        $image = $color->image;
                    }
                    if ($clr->id && $clr->deleted)
                        Color::findOrFail($clr->id)->delete();
                    else {
                        $color->model_id = $vehicle_id;
                        $color->name = $clr->name;
                        $color->color = $clr->color;
                        $color->stock = $clr->stock ? 1 : 0;
                        $color->image = $image;
                        $color->save();
                    }
                }
            }


            return $vehicle_id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function storeGrade($request)
    {
        try {
            if ($request->has('grade_id')) {
                $grade = Grade::findOrFail($request->grade_id);
                $image = $grade->image;
            } else {
                $grade = new Grade();
            }
            if ($request->file('image') != null) {
                if ($grade->image != '')
                    Storage::delete($grade->image);
                $filename = $request->file('image')->store('grade');
                $image = $filename;
            }
            $grade->model_id = $request->model_id;
            $grade->name = $request->name;
            $grade->image = $image;
            $grade->price = $request->price;
            $grade->intrest = $request->intrest;
            $grade->stock = $request->stock ? 1 : 0;
            $grade->body = $request->features;
            return $grade->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getGradesList($id)
    {
        try {
            return Grade::where('model_id', $id)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getGrade($id)
    {
        try {
            return Grade::findOrFail($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getModelDetails($id)
    {
        try {
            return Model::where('id', $id)
                ->with('Specs')
                ->with('Atts')
                ->with('Clrs')
                ->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
