<?php

namespace App\Http\Repositories;

use App\Models\Contact;
use App\Models\FieldPage;
use App\Models\Grade;
use App\Models\Image;
use App\Models\Lead;
use App\Models\Offer;
use App\Models\Option;
use App\Models\Order;
use App\Models\Page;
use App\Models\Post;
use App\Models\Service;
use App\Models\Slide;
use App\Models\Slider;
use Illuminate\Support\Facades\Storage;

class AdminRepository
{
    public function getCounts()
    {
        try {
            $leads = Lead::count();
            $orders = Order::count();
            $services = Service::count();
            $posts = Post::count();
            return ['Leads' => $leads, 'Orders' => $orders, 'Services' => $services, 'Posts' => $posts];
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function getRecentLeads()
    {
        try {
            return Lead::latest()->take(10)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function getRecentServices()
    {
        try {
            return Service::latest()->take(10)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
    public function getRecentOrders()
    {
        try {
            return Order::latest()->take(10)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getPage($slug)
    {
        try {
            return Page::where('slug', $slug)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function savePage($request)
    {
        try {
            $page_id = $request->id;
            unset($request['id']);
            unset($request['_token']);
            foreach ($request->all() as $key => $value) {
                $field = FieldPage::where('page_id', $page_id)->where('name', $key)->first();
                if ($request->file($key) != null) {
                    if ($field->value != '')
                        Storage::delete($field->value);
                    $filename = $request->file($key)->store('page');
                    $value = $filename;
                }
                $field->value = $value;
                $field->save();
            }
            return $page_id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSliders()
    {
        try {
            return Slider::paginate(10);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSlidersList()
    {
        try {
            return Slider::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getSlides($id)
    {
        try {
            return Slide::where('slider_id', $id)->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveSlider($request)
    {
        try {
            $slider = $request->slider_id ? SLider::findOrFail($request->slider_id) : new Slider();
            $slider->name = $request->name;
            return $slider->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveSlide($request)
    {
        try {
            if ($request->file('image') != null) {
                $image = $request->file('image')->store('slider');
            }
            $m_image = '';
            if ($request->file('m_image') != null) {
                $m_image = $request->file('m_image')->store('slider');
            }
            $slide = new Slide();
            $slide->slider_id = $request->slider_id;
            $slide->image = $image;
            $slide->m_image = $m_image;
            return $slide->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getPosts()
    {
        try {
            return Post::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getPost($id)
    {
        try {
            return Post::findorFail($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function savePost($request)
    {
        try {
            if ($request->file('image') != null) {
                $image = $request->file('image')->store('posts');
            }
            $post = new Post();
            $post->slug = $request->slug;
            $post->title = $request->title;
            $post->image = $image;
            $post->description = $request->description;
            $post->body = $request->body;
            return $post->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updatePost($request, $id)
    {
        try {
            $post = Post::findOrFail($id);
            if ($request->file('image') != null) {
                if ($post->image != '')
                    Storage::delete($post->image);
                $image = $request->file('image')->store('posts');
            } else {
                $image = $post->image;
            }
            $post->slug = $request->slug;
            $post->title = $request->title;
            $post->image = $image;
            $post->description = $request->description;
            $post->body = $request->body;
            return $post->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOffers()
    {
        try {
            return Offer::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOffer($id)
    {
        try {
            return Offer::findorFail($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveOffer($request)
    {
        try {
            if ($request->file('image') != null) {
                $image = $request->file('image')->store('offers');
            }
            if ($request->file('image_1') != null) {
                $image_1 = $request->file('image_1')->store('offers');
            }
            $offer = new Offer();
            $offer->slug = $request->slug;
            $offer->title = $request->title;
            $offer->type = $request->type;
            $offer->model_id = $request->model_id;
            $offer->grade_id = $this->checkGrade($request->grade_id, $request->model_id);
            $offer->image = $image;
            $offer->image_1 = $image_1;
            $offer->description = $request->description;
            $offer->body = $request->body;
            return $offer->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateOffer($request, $id)
    {
        try {
            $offer = Offer::findOrFail($id);

            if ($request->file('image') != null) {
                if ($offer->image != '')
                    Storage::delete($offer->image);
                $image = $request->file('image')->store('offers');
            } else {
                $image = $offer->image;
            }
            if ($request->file('image_1') != null) {
                if ($offer->image_1 != '')
                    Storage::delete($offer->image_1);
                $image_1 = $request->file('image_1')->store('offers');
            } else {
                $image_1 = $offer->image_1;
            }
            $offer->slug = $request->slug;
            $offer->title = $request->title;
            $offer->type = $request->type;
            $offer->model_id = $request->model_id;
            $offer->grade_id = $this->checkGrade($request->grade_id, $request->model_id);
            $offer->image = $image;
            $offer->image_1 = $image_1;
            $offer->description = $request->description;
            $offer->body = $request->body;
            return $offer->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getContacts()
    {
        try {
            return Contact::all();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveContact($request)
    {
        try {
            $contact = $request->has('contact_id') ? Contact::findOrFail($request->contact_id) : new Contact();
            $contact->company = $request->company;
            $contact->location = $request->location;
            $contact->description = $request->description;
            $contact->address = $request->address;
            $contact->po_box = $request->po_box;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->timing_sales = $request->timing_sales;
            $contact->timing_services = $request->timing_services;
            $contact->save();
            return $contact->id;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    private function checkGrade($grade_id, $model_id)
    {
        $grade = Grade::where(['id' => $grade_id, 'model_id' => $model_id])->first();
        return $grade ? $grade->id : null;
    }

    public function getImagesList($request)
    {
        try {
            $section_id = $request->section_id;
            $reference_id = $request->reference_id;
            return Image::where(['section_id' => $section_id, 'reference_id' => $reference_id])->get();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveImage($request)
    {
        try {
            if ($request->file('image') != null) {
                $filename = $request->file('image')->store('media');
            }
            $image = new Image();
            $image->section_id = $request->section_id;
            $image->reference_id = $request->reference_id;
            $image->image = $filename;
            return $image->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function delete($request)
    {
        try {
            $id = $request->id;
            $model = 'App\\Models\\' . ucfirst($request->model);
            $model::where('id', $id)->delete();
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getLeads($request)
    {
        try {
            $query = Lead::select('*');
            if ($filter = $request->get('filter')) {
                $query->where('source', $filter);
            }
            return $query->latest()->paginate(25);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getLead($id)
    {
        try {
            return Lead::select(
                'leads.source as Source',
                'leads.name as Name',
                'leads.phone as Phone',
                'leads.email as Email',
                'leads.message as message',
                'models.name as Model Name',
                'date as Date',
                'time as Time',
                'leads.created_at as Created At',
                'contacts.address as Address'
            )
                ->leftJoin('models', 'models.id', '=', 'leads.model_id')
                ->leftJoin('contacts','contacts.id','=','leads.contact_id')
                ->where('leads.id', $id)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getServices($search)
    {
        try {
            $query = Service::latest();
            if ($search) {
                $query->where('plate_number', 'like', "%$search%");
            }
            return $query->paginate(25);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getService($id)
    {
        try {
            return Service::select(
                'services.fname as First Name',
                'services.lname as Last Name',
                'services.email as Email',
                'services.phone as Phone',
                'services.types as Types of Services',
                'services.plate_number as Types of Plate Number',
                'services.date as Types of Date',
                'services.comments as Comments',
                'services.status as status',
                'models.name as Model Name'
            )
                ->leftJoin('models', 'models.id', '=', 'services.model_id')
                ->where('services.id', $id)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function serviceStatus($request, $id)
    {
        try {
            $service = Service::findOrFail($id);
            $service->status = $request->status;
            return $service->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOrders()
    {
        try {
            return Order::latest()->paginate(25);
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function getOrder($id)
    {
        try {
            return Order::select(
                'orders.order_id as Order ID',
                'orders.is_success as Payment',
                'orders.status as Status',
                'customers.fname as First Name',
                'customers.lname as Last Name',
                'customers.email as Email',
                'customers.contact as Phone',
                'grades.name as Grade Name',
                'models.name as Model Name',
                'colors.name as Color name'
            )->leftJoin('grades', 'grades.id', '=', 'orders.grade_id')
                ->leftJoin('models', 'models.id', '=', 'grades.model_id')
                ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
                ->leftJoin('colors', 'colors.id', '=', 'orders.color_id')
                ->where('orders.id', $id)->first();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function orderStatus($request, $id)
    {
        try {
            $order = Order::findOrFail($id);
            $order->status = $request->status;
            return $order->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function saveOptions($request)
    {
        try {
            if ($request->file('image_r') != null) {
                $image_r = Option::where('option', 'image_r')->first();
                if ($image_r != null)
                    Storage::delete($image_r);
                $image_r = $request->file('image_r')->store('options');
                Option::updateOrCreate(
                    ['option' => 'image_r'],
                    ['value' =>  $image_r]
                );
            };
            if ($request->file('image_l') != null) {
                $image_l = option::where('option', 'image_l')->first();
                if ($image_l != null)
                    Storage::delete($image_l);
                $image_l = $request->file('image_l')->store('options');
                Option::updateOrCreate(
                    ['option' => 'image_l'],
                    ['value' =>  $image_l]
                );
            };
            $social = $request->only(['facebook', 'twitter', 'youtube', 'instagram', 'otp_sms', 'company', 'pobox']);
            foreach ($social as $key => $value) {
                Option::updateOrCreate(
                    ['option' => $key],
                    ['value' => $value]
                );
            }
            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
            dd();
            return false;
        }
    }

    public function getOptions()
    {
        try {
            $_options = Option::all()->toArray();
            $options = [];
            foreach ($_options as $option) {
                $options[$option['option']] = $option['value'];
            }
            return $options;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
