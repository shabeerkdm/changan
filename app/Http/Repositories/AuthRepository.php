<?php

namespace App\Http\Repositories;

use App\Models\User;

class AuthRepository
{

    public function checkEmail($email = NULL)
    {
        if (!is_null($email)) {
            try {
                return User::where('email', $email)->first();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
    }

    public function isActiveUser($email)
    {
        if (!is_null($email)) {
            try {
                return User::where('email', $email)->where('is_active', 1)->first();
            } catch (Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }
    }
}
