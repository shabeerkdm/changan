<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Contact extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details, $response = false)
    {
        $this->details = $details;
        $this->response = $response;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->response) {
            $subject = 'New Contact Request';
            if ($this->details['source'] == 'book-test-drive') {
                $subject = 'Test Drive Request';
            }
            return (new MailMessage)
                ->subject($subject)
                ->line('Source :' . $this->details['source'])
                ->line('Name :' . $this->details['fname'] . ' ' . $this->details['lname'])
                ->line('Email :' . $this->details['email'])
                ->line('Phone :' . $this->details['phone'])
                ->line('Address :' . $this->details['address'])
                ->line('Subject :' . $this->details['subject'])
                ->line('Message :' . $this->details['message'])
                ->replyTo($this->details['email'], $this->details['fname'] . ' ' . $this->details['lname']);
        } else {
            return (new MailMessage)
                ->subject('Changan')
                ->greeting($this->details['greeting'])
                ->line($this->details['body']);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
