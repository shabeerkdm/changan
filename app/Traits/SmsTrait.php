<?php

namespace App\Traits;

trait SmsTrait
{
    private $_url;
    private $_data;
    private $_to;
    private $_message;

    public function to($to = null)
    {
        $this->_to = preg_replace('/^(?:\+971|971|0)?/','971', urldecode($to));
        return $this;
    }

    public function message($template = null, $data = array())
    {
        $message = $template;
        if (is_array($data) && sizeof($data) > 0) {
            $message = $this->_template($template, $data);
        }
        $this->_message = $message;
        return $this;
    }

    private function _template($template = null, $data = array())
    {
        foreach ($data as $key => $value) {
            $template = str_replace("[$key]", $value, $template);
        }

        return str_replace(' ', '+', $template);
    }

    private function setProvider()
    {
        $this->_data = array(
            'User' => env('SMS_USERNAME','Changan'),
            'passwd'   => env('SMS_PASSWORD','Changan@5622'),
            'message'      => $this->_message,
            'mobilenumber'       => $this->_to,
            'mtype' => 'N',
            'DR' => 'Y'
        );
        $this->_url = env('SMS_URL','http://api.smscountry.com/SMSCwebservice_bulk.aspx');
    }

    public function response()
    {
        $this->setProvider();
        return $this->_getRequest($this->_url);
    }

    private function _getRequest()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, urldecode($this->_url . "?" . http_build_query($this->_data)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
